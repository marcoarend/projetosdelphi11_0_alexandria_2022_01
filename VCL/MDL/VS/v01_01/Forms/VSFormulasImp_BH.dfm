object FmVSFormulasImp_BH: TFmVSFormulasImp_BH
  Left = 363
  Top = 167
  Caption = 'QUI-RECEI-003 :: Impress'#227'o de Receitas de Ribeira'
  ClientHeight = 660
  ClientWidth = 947
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 947
    Height = 492
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 947
      Height = 73
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object RGImpRecRib: TRadioGroup
        Left = 4
        Top = 0
        Width = 700
        Height = 65
        Caption = ' Apresenta'#231#227'o: '
        Columns = 2
        ItemIndex = 3
        Items.Strings = (
          'Procure em:'
          ''
          'FmPrincipal.VAR_RecImpApresentaCol'
          'FmPrincipal.VAR_RecImpApresentaRol')
        TabOrder = 0
      end
      object GBkgTon: TGroupBox
        Left = 706
        Top = 0
        Width = 95
        Height = 65
        Caption = ' Grandeza: '
        TabOrder = 1
        object RBTon: TRadioButton
          Left = 8
          Top = 16
          Width = 50
          Height = 17
          Caption = 'Ton'
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object RBkg: TRadioButton
          Left = 8
          Top = 36
          Width = 50
          Height = 17
          Caption = 'kg'
          TabOrder = 1
        end
      end
      object CkMatricial: TCheckBox
        Left = 808
        Top = 8
        Width = 97
        Height = 17
        Caption = 'Novo (Matricial)'
        TabOrder = 2
      end
      object CkGrade: TCheckBox
        Left = 808
        Top = 28
        Width = 125
        Height = 17
        Caption = 'Ver grade (Matricial)'
        TabOrder = 3
      end
      object CkImpVS: TCheckBox
        Left = 808
        Top = 48
        Width = 169
        Height = 17
        Caption = 'Imprimir artigo e mat'#233'ria-prima.'
        Checked = True
        Enabled = False
        State = cbChecked
        TabOrder = 4
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 73
      Width = 947
      Height = 419
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Dados b'#225'sicos'
        object PainelDados: TPanel
          Left = 0
          Top = 0
          Width = 939
          Height = 391
          Align = alClient
          BevelOuter = bvSpace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object PainelReceita: TPanel
            Left = 1
            Top = 1
            Width = 937
            Height = 140
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 48
              Width = 50
              Height = 16
              Caption = 'Receita:'
            end
            object Label11: TLabel
              Left = 8
              Top = 92
              Width = 247
              Height = 16
              Caption = 'Cliente interno (dono do produto qu'#237'mico):'
            end
            object Label2: TLabel
              Left = 8
              Top = 2
              Width = 38
              Height = 16
              Caption = 'Artigo:'
              FocusControl = DBEdit1
            end
            object Label7: TLabel
              Left = 564
              Top = -6
              Width = 35
              Height = 16
              Caption = 'Pe'#231'a:'
              Visible = False
            end
            object SpeedButton1: TSpeedButton
              Left = 820
              Top = 12
              Width = 24
              Height = 24
              Caption = '...'
              Visible = False
              OnClick = SpeedButton1Click
            end
            object Label14: TLabel
              Left = 544
              Top = 48
              Width = 114
              Height = 16
              Caption = 'Grupo de emiss'#227'o:'
            end
            object EdReceita: TdmkEditCB
              Left = 8
              Top = 64
              Width = 72
              Height = 24
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdReceitaChange
              DBLookupComboBox = CBReceita
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBReceita: TdmkDBLookupComboBox
              Left = 82
              Top = 64
              Width = 459
              Height = 24
              Color = clWhite
              KeyField = 'Numero'
              ListField = 'Nome'
              ListSource = DsFormulas
              TabOrder = 3
              dmkEditCB = EdReceita
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdCliInt: TdmkEditCB
              Left = 8
              Top = 108
              Width = 72
              Height = 24
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCliInt
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCliInt: TdmkDBLookupComboBox
              Left = 82
              Top = 108
              Width = 843
              Height = 24
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMECI'
              ListSource = DsCliInt
              TabOrder = 7
              dmkEditCB = EdCliInt
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object DBEdit1: TDBEdit
              Left = 8
              Top = 20
              Width = 80
              Height = 24
              DataField = 'GraGruX'
              DataSource = DsVMIAtu
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 92
              Top = 20
              Width = 837
              Height = 24
              DataField = 'NO_PRD_TAM_COR'
              DataSource = DsVMIAtu
              TabOrder = 1
            end
            object CBPeca: TdmkDBLookupComboBox
              Left = 616
              Top = 12
              Width = 205
              Height = 24
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsDefPecas
              TabOrder = 8
              Visible = False
              dmkEditCB = EdPeca
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdPeca: TdmkEditCB
              Left = 564
              Top = 12
              Width = 53
              Height = 24
              Alignment = taRightJustify
              TabOrder = 9
              Visible = False
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdQtdeChange
              DBLookupComboBox = CBPeca
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdEmitGru: TdmkEditCB
              Left = 544
              Top = 64
              Width = 56
              Height = 24
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdReceitaChange
              DBLookupComboBox = CBEmitGru
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEmitGru: TdmkDBLookupComboBox
              Left = 600
              Top = 64
              Width = 325
              Height = 24
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsEmitGru
              TabOrder = 5
              dmkEditCB = EdEmitGru
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
          object PainelEscolhas: TPanel
            Left = 1
            Top = 141
            Width = 937
            Height = 50
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Label10: TLabel
              Left = 280
              Top = -2
              Width = 61
              Height = 16
              Caption = 'Peso [F4]:'
            end
            object Label3: TLabel
              Left = 400
              Top = -2
              Width = 73
              Height = 16
              Caption = 'Quantidade:'
            end
            object Label12: TLabel
              Left = 504
              Top = -2
              Width = 54
              Height = 16
              Caption = 'kg/Pe'#231'a:'
            end
            object Label9: TLabel
              Left = 8
              Top = -2
              Width = 37
              Height = 16
              Caption = 'Ful'#227'o:'
            end
            object LaData: TLabel
              Left = 88
              Top = -2
              Width = 32
              Height = 16
              Caption = 'Data:'
              Visible = False
            end
            object LaHora: TLabel
              Left = 206
              Top = -2
              Width = 67
              Height = 16
              Caption = 'Hora in'#237'cio:'
            end
            object EdPeso: TdmkEdit
              Left = 280
              Top = 16
              Width = 117
              Height = 24
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdPesoChange
              OnKeyDown = EdPesoKeyDown
            end
            object EdQtde: TdmkEdit
              Left = 400
              Top = 16
              Width = 101
              Height = 24
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdQtdeChange
            end
            object EdMedia: TdmkEdit
              Left = 504
              Top = 16
              Width = 61
              Height = 24
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdFulao: TdmkEdit
              Left = 8
              Top = 16
              Width = 77
              Height = 24
              MaxLength = 5
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdPesoChange
            end
            object TPDataP: TdmkEditDateTimePicker
              Left = 88
              Top = 16
              Width = 113
              Height = 24
              Date = 38795.000000000000000000
              Time = 0.975709085701964800
              TabOrder = 1
              Visible = False
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpInsumMovimMin
            end
            object EdHoraIni: TdmkEdit
              Left = 206
              Top = 16
              Width = 71
              Height = 24
              TabOrder = 2
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfLong
              Texto = '00:00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object CkDtCorrApo: TCheckBox
              Left = 572
              Top = 21
              Width = 233
              Height = 17
              Caption = #201' corre'#231#227'o de apontamento. Data:'
              TabOrder = 6
              OnClick = CkDtCorrApoClick
            end
            object TPDtCorrApo: TdmkEditDateTimePicker
              Left = 804
              Top = 17
              Width = 129
              Height = 21
              Date = 44660.000000000000000000
              Time = 0.833253726850671200
              Enabled = False
              TabOrder = 7
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpSPED_EFD_MIN_MAX
            end
          end
          object PainelEscolhe: TPanel
            Left = 1
            Top = 191
            Width = 937
            Height = 199
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 101
              Height = 199
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label6: TLabel
                Left = 4
                Top = 4
                Width = 79
                Height = 13
                Caption = 'Lotes de couros:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
            end
            object DBGrid1: TDBGrid
              Left = 101
              Top = 0
              Width = 611
              Height = 199
              Align = alClient
              DataSource = DsVMIOriIMEI
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_SerieFch'
                  Title.Caption = 'S'#233'rie'
                  Width = 111
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ficha'
                  Title.Caption = 'Ficha RMP'
                  Width = 60
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QtdGerPeso'
                  Title.Caption = 'Peso kg'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QtdGerPeca'
                  Title.Caption = 'Pe'#231'as'
                  Width = 76
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QtdGerArM2'
                  Title.Caption = 'Area m'#178
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Marca'
                  Width = 103
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Kg baixado'
                  Visible = True
                end>
            end
            object PageControl2: TPageControl
              Left = 712
              Top = 0
              Width = 225
              Height = 199
              ActivePage = TabSheet4
              Align = alRight
              MultiLine = True
              TabOrder = 2
              TabPosition = tpLeft
              object TabSheet4: TTabSheet
                Caption = 'Observa'#231#245'es'
                object EdMemo: TMemo
                  Left = 0
                  Top = 0
                  Width = 197
                  Height = 191
                  TabStop = False
                  Align = alClient
                  Color = clWhite
                  TabOrder = 0
                end
              end
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Configura'#231#245'es'
        ImageIndex = 2
        object PainelConfig: TPanel
          Left = 0
          Top = 0
          Width = 939
          Height = 391
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object RGTipoPreco: TRadioGroup
            Left = 12
            Top = 6
            Width = 297
            Height = 47
            Caption = ' Origem pre'#231'os: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'Estoque'
              'Cadastro'
              'A definir')
            TabOrder = 0
          end
          object RGImprime: TRadioGroup
            Left = 12
            Top = 53
            Width = 393
            Height = 47
            Caption = ' Op'#231#245'es: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'Visualizar'
              'Imprimir'
              'Matricial'
              'Arquivo')
            TabOrder = 1
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 947
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 899
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 193
      Height = 48
      Align = alLeft
      TabOrder = 1
      object LaSP_A: TLabel
        Left = 7
        Top = 9
        Width = 135
        Height = 32
        Caption = '000000000'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaSP_B: TLabel
        Left = 7
        Top = 11
        Width = 135
        Height = 32
        Caption = '000000000'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaSP_C: TLabel
        Left = 7
        Top = 10
        Width = 135
        Height = 32
        Caption = '000000000'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_M: TGroupBox
      Left = 193
      Top = 0
      Width = 706
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 412
        Height = 32
        Caption = 'Impress'#227'o de Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 412
        Height = 32
        Caption = 'Impress'#227'o de Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 412
        Height = 32
        Caption = 'Impress'#227'o de Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 540
    Width = 947
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 943
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 22
        Width = 943
        Height = 17
        Align = alBottom
        TabOrder = 0
        Visible = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 596
    Width = 947
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 943
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label13: TLabel
        Left = 148
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object PnSaiDesis: TPanel
        Left = 799
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtCancela: TBitBtn
          Tag = 15
          Left = 4
          Top = 1
          Width = 120
          Height = 40
          Hint = 'Cancela exibi'#231#227'o do cadastro de senhas'
          Caption = '&Desiste'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtCancelaClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Hint = 'Confirma a senha digitada'
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object EdEmpresa: TdmkEditCB
        Left = 148
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 204
        Top = 16
        Width = 437
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * From DefPecas')
    Left = 436
    Top = 384
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.defpecas.Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.defpecas.Nome'
      Size = 6
    end
    object QrDefPecasGrandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'DBMBWET.defpecas.Grandeza'
    end
    object QrDefPecasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.defpecas.Lk'
    end
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 436
    Top = 432
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 368
    Top = 432
  end
  object QrFormulas: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFormulasCalcFields
    SQL.Strings = (
      'SELECT * FROM formulas'
      'ORDER BY Nome')
    Left = 372
    Top = 384
    object QrFormulasHHMM_P: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_P'
      Size = 10
      Calculated = True
    end
    object QrFormulasHHMM_R: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_R'
      Size = 10
      Calculated = True
    end
    object QrFormulasHHMM_T: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_T'
      Size = 10
      Calculated = True
    end
    object QrFormulasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFormulasClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrFormulasTipificacao: TIntegerField
      FieldName = 'Tipificacao'
    end
    object QrFormulasDataI: TDateField
      FieldName = 'DataI'
    end
    object QrFormulasDataA: TDateField
      FieldName = 'DataA'
    end
    object QrFormulasTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrFormulasTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrFormulasTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrFormulasTempoT: TIntegerField
      FieldName = 'TempoT'
    end
    object QrFormulasHorasR: TIntegerField
      FieldName = 'HorasR'
    end
    object QrFormulasHorasP: TIntegerField
      FieldName = 'HorasP'
    end
    object QrFormulasHorasT: TIntegerField
      FieldName = 'HorasT'
    end
    object QrFormulasHidrica: TIntegerField
      FieldName = 'Hidrica'
    end
    object QrFormulasLinhaTE: TIntegerField
      FieldName = 'LinhaTE'
    end
    object QrFormulasCaldeira: TIntegerField
      FieldName = 'Caldeira'
    end
    object QrFormulasSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrFormulasEspessura: TIntegerField
      FieldName = 'Espessura'
    end
    object QrFormulasPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrFormulasQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrFormulasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulasRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
    object QrFormulasBxaEstqVS: TSmallintField
      FieldName = 'BxaEstqVS'
    end
    object QrFormulasGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECI, ci.Codigo'
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"')
    Left = 252
    Top = 384
    object QrCliIntNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 252
    Top = 432
  end
  object QrLotes: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLotesAfterOpen
    SQL.Strings = (
      'SELECT emc.Controle, emc.VSMovIts, emc.Peso, emc.Pecas,   '
      'vmi.SerieFch, vmi.Ficha, vmi.Marca,fch.Nome NO_SerieFch  '
      'FROM emitcus emc '
      'LEFT JOIN vsmovits vmi ON vmi.Controle=emc.VSMovIts '
      'LEFT JOIN vsserfch fch ON fch.Codigo=vmi.SerieFch '
      'WHERE emc.Codigo>0 ')
    Left = 312
    Top = 385
    object QrLotesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLotesVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrLotesPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrLotesPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrLotesSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrLotesFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrLotesMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrLotesNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
  end
  object DsLotes: TDataSource
    DataSet = QrLotes
    Left = 312
    Top = 433
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Peso) Peso, '
      'SUM(Pecas) Pecas'
      'FROM emitcus'
      'WHERE Codigo=:P0')
    Left = 568
    Top = 385
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSomaPecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object DsVMIAtu: TDataSource
    DataSet = QrVMIAtu
    Left = 100
    Top = 433
  end
  object QrVMIAtu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 100
    Top = 385
    object QrVMIAtuCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVMIAtuControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVMIAtuMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVMIAtuMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVMIAtuMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVMIAtuEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVMIAtuTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVMIAtuCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVMIAtuMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVMIAtuDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVMIAtuPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVMIAtuGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVMIAtuPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAtuPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAtuAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVMIAtuSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVMIAtuSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVMIAtuSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVMIAtuSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMIAtuSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAtuSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVMIAtuSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVMIAtuFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVMIAtuMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVMIAtuFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVMIAtuCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVMIAtuCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVMIAtuCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVMIAtuDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVMIAtuDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVMIAtuDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVMIAtuQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMIAtuQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAtuQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMIAtuQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAtuQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVMIAtuNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVMIAtuNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVMIAtuNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVMIAtuID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVMIAtuNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVMIAtuReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVMIAtuCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVMIAtuCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVMIAtuNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVMIAtuMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVMIAtuPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVMIAtuStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVMIAtuNO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
    object QrVMIAtuNO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrVMIAtuClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVMIAtuCouNiv1: TIntegerField
      FieldName = 'CouNiv1'
    end
    object QrVMIAtuCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
  end
  object QrVMIOriIMEI: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVMIOriIMEICalcFields
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 184
    Top = 385
    object QrVMIOriIMEICodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVMIOriIMEIControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVMIOriIMEIMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVMIOriIMEIMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVMIOriIMEIMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVMIOriIMEIEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVMIOriIMEITerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVMIOriIMEICliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVMIOriIMEIMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVMIOriIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVMIOriIMEIPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVMIOriIMEIGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVMIOriIMEIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriIMEIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriIMEIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEISrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVMIOriIMEISrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVMIOriIMEISrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVMIOriIMEISrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVMIOriIMEISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMIOriIMEISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriIMEISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVMIOriIMEISerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVMIOriIMEIFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVMIOriIMEIMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVMIOriIMEIFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVMIOriIMEICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVMIOriIMEIDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVMIOriIMEIDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVMIOriIMEIDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVMIOriIMEIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMIOriIMEIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriIMEIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMIOriIMEIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriIMEIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVMIOriIMEINO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVMIOriIMEINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVMIOriIMEINO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVMIOriIMEIID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVMIOriIMEINO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVMIOriIMEINO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVMIOriIMEIReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVMIOriIMEIVSMovIts: TLargeintField
      FieldKind = fkCalculated
      FieldName = 'VSMovIts'
      Calculated = True
    end
  end
  object DsVMIOriIMEI: TDataSource
    DataSet = QrVMIOriIMEI
    Left = 184
    Top = 433
  end
  object QrEmitGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM emitgru'
      'WHERE Ativo=1'
      'ORDER BY Nome'
      '')
    Left = 432
    Top = 113
    object QrEmitGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEmitGru: TDataSource
    DataSet = QrEmitGru
    Left = 432
    Top = 157
  end
end
