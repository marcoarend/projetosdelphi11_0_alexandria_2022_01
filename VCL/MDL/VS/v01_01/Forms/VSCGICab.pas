unit VSCGICab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditDateTimePicker, dmkDBLookupComboBox,
  dmkEditCB, Vcl.ComCtrls, dmkDBGridZTO, UnProjGroup_Consts;

type
  TFmVSCGICab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrVSCGICab: TmySQLQuery;
    DsVSCGICab: TDataSource;
    QrVSCGIIts: TmySQLQuery;
    DsVSCGIIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrVSCGICabNO_EMP: TWideStringField;
    QrVSCGICabCodigo: TIntegerField;
    QrVSCGICabNome: TWideStringField;
    QrVSCGICabEmpresa: TIntegerField;
    QrVSCGICabDtHrIni: TDateTimeField;
    QrVSCGICabDtHrFim: TDateTimeField;
    QrVSCGICabLk: TIntegerField;
    QrVSCGICabDataCad: TDateField;
    QrVSCGICabDataAlt: TDateField;
    QrVSCGICabUserCad: TIntegerField;
    QrVSCGICabUserAlt: TIntegerField;
    QrVSCGICabAlterWeb: TSmallintField;
    QrVSCGICabAtivo: TSmallintField;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label52: TLabel;
    TPDtHrIni: TdmkEditDateTimePicker;
    EdDtHrIni: TdmkEdit;
    TPDtHrFim: TdmkEditDateTimePicker;
    EdDtHrFim: TdmkEdit;
    DGDados: TdmkDBGridZTO;
    QrVSCGIItsNO_SerieFch: TWideStringField;
    QrVSCGIItsNO_FORNECE: TWideStringField;
    QrVSCGIItsNO_PRD_TAM_COR: TWideStringField;
    QrVSCGIItsCodigo: TLargeintField;
    QrVSCGIItsControle: TLargeintField;
    QrVSCGIItsVSMovIts: TLargeintField;
    QrVSCGIItsFicha: TLargeintField;
    QrVSCGIItsID_TTW: TLargeintField;
    QrVSCGIItsNO_TTW: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSCGICabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSCGICabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSCGICabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSCGICabBeforeClose(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSCGIIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSCGIIts(Controle: Integer);

  end;

var
  FmVSCGICab: TFmVSCGICab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSCGIIts, ModuleGeral, UnVS_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSCGICab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSCGICab.MostraFormVSCGIIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSCGIIts, FmVSCGIIts, afmoNegarComAviso) then
  begin
    FmVSCGIIts.ImgTipo.SQLType := SQLType;
    FmVSCGIIts.FQrCab := QrVSCGICab;
    FmVSCGIIts.FDsCab := DsVSCGICab;
    FmVSCGIIts.FQrIts := QrVSCGIIts;
    if SQLType = stIns then

    else
    begin
      //FmVSCGIIts.EdControle.ValueVariant := QrVSCGIItsControle.Value;
      //
    end;
    FmVSCGIIts.ShowModal;
    FmVSCGIIts.Destroy;
  end;
end;

procedure TFmVSCGICab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSCGICab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSCGICab, QrVSCGIIts);
end;

procedure TFmVSCGICab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSCGICab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSCGIIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSCGIIts);
end;

procedure TFmVSCGICab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSCGICabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSCGICab.DefParams;
begin
  VAR_GOTOTABELA := 'vscgicab';
  VAR_GOTOMYSQLTABLE := QrVSCGICab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMP, vcc.*  ');
  VAR_SQLx.Add('FROM vscgicab vcc ');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=vcc.Empresa ');
  VAR_SQLx.Add('WHERE vcc.Codigo > 0 ');
  //
  VAR_SQL1.Add('AND vcc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vcc.Nome Like :P0');
  //
end;

procedure TFmVSCGICab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSCGIIts(stUpd);
end;

procedure TFmVSCGICab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSCGICab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSCGICab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSCGICab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI('Confirma a remo��o do IME-I selecionado?',
  'VSCGIIts', 'Controle', QrVSCGIItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSCGIIts,
      TIntegerField(QrVSCGIItsControle), QrVSCGIItsControle.Value);
    ReopenVSCGIIts(Controle);
  end;
end;

procedure TFmVSCGICab.ReopenVSCGIIts(Controle: Integer);
  function GeraSQLVSMovItx(Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
  begin
    if VS_PF.GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
      Result := Geral.ATS([
      'SELECT ' + VS_PF.TabMovVS_Fld_IMEI(tab),
      'CAST(vci.Codigo AS SIGNED) Codigo, ',
      'CAST(vci.Controle AS SIGNED) Controle, ',
      'CAST(vci.VSMovIts AS SIGNED) VSMovIts, ',
      'CAST(vsf.Nome AS CHAR) NO_SerieFch, ',
      'CAST(vmi.Ficha AS SIGNED) Ficha, ',
      'CAST(IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) AS CHAR) NO_FORNECE, ',
      'CAST(CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)) ',
      ') AS CHAR) NO_PRD_TAM_COR ',
      'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
      'LEFT JOIN vscgiits vci ON vmi.Controle=vci.VSMovIts ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch  ',
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
      'WHERE vci.Codigo=' + Geral.FF0(QrVSCGICabCodigo.Value),
      Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
      '']);
  end;
const
  TemIMEIMrt = 1;
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCGIIts, Dmod.MyDB, [
  'SELECT vci.*, vsf.Nome NO_SerieFch, vmi.Ficha, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)) ',
  ') NO_PRD_TAM_COR ',
  'FROM vscgiits vci ',
  'LEFT JOIN v s m o v i t s vmi ON vmi.Controle=vci.VSMovIts ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch  ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'WHERE vci.Codigo=' + Geral.FF0(QrVSCGICabCodigo.Value),
  'ORDER BY VSMovIts ',
  '']);
*)
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCGIIts, Dmod.MyDB, [
  GeraSQLVSMovItx(ttwB, TemIMEIMrt),
  GeraSQLVSMovItx(ttwA),
  '',
  'ORDER BY VSMovIts ',
  '']);
  //
  //Geral.MB_SQL(nil, QrVSCGIIts);
  QrVSCGIIts.Locate('Controle', Controle, []);
end;


procedure TFmVSCGICab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSCGICab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSCGICab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSCGICab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSCGICab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSCGICab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCGICab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSCGICabCodigo.Value;
  Close;
end;

procedure TFmVSCGICab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSCGIIts(stIns);
end;

procedure TFmVSCGICab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSCGICab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vscgicab');
end;

procedure TFmVSCGICab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtHrIni, DtHrFim: String;
  Codigo, Empresa: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtHrIni        := Geral.FDT_TP_Ed(TPDtHrIni.Date, EdDtHrIni.Text);
  DtHrFim        := Geral.FDT_TP_Ed(TPDtHrFim.Date, EdDtHrFim.Text);
  Nome           := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('vscgicab', 'Codigo', '', '', tsPos,
    ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vscgicab', False, [
  'Nome', 'Empresa', 'DtHrIni',
  'DtHrFim'], [
  'Codigo'], [
  Nome, Empresa, DtHrIni,
  DtHrFim], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSCGICab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vscgicab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vscgicab', 'Codigo');
end;

procedure TFmVSCGICab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSCGICab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSCGICab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmVSCGICab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSCGICabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSCGICab.SbImprimeClick(Sender: TObject);
var
  IMEIs, Mortos: array of Integer;
  I, Controle, K, M, IMEI: Integer;
begin
  QrVSCGIIts.DisableControls;
  try
    Controle := QrVSCGIItsControle.Value;
    K := 0;
    M := 0;
    QrVSCGIIts.First;
    while not QrVSCGIIts.Eof do
    begin
      IMEI := QrVSCGIItsVSMovIts.Value;
      if IMEI <> 0 then
      begin
        K := K + 1;
        SetLength(IMEIs, K);
        IMEIs[K - 1] := IMEI;
      end else
        Geral.MB_Aviso('O item "' + Geral.FF0(QrVSCGIItsControle.Value) +
        '" n�o possui um n�mero de IME-I v�lido e n�o ser� pesquisado!');
      //
      if QrVSCGIItsID_TTW.Value = 1 then
      begin
        M := M + 1;
        SetLength(Mortos, M);
        Mortos[M - 1] := IMEI;
      end;
      //
      QrVSCGIIts.Next;
    end;
    VS_PF.ImprimeClassIMEIs(IMEIs, Mortos, True);
    QrVSCGIIts.Locate('Controle', Controle, []);
  finally
    QrVSCGIIts.EnableControls;
  end;
end;

procedure TFmVSCGICab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSCGICab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSCGICabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSCGICab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSCGICab.QrVSCGICabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSCGICab.QrVSCGICabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSCGIIts(0);
end;

procedure TFmVSCGICab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSCGICabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSCGICab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSCGICabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vscgicab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSCGICab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCGICab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSCGICab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vscgicab');
  Agora := DModG.ObtemAgora();
  TPDtHrIni.Date := Agora;
  EdDtHrIni.ValueVariant := Agora;
end;

procedure TFmVSCGICab.QrVSCGICabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSCGIIts.Close;
end;

procedure TFmVSCGICab.QrVSCGICabBeforeOpen(DataSet: TDataSet);
begin
  QrVSCGICabCodigo.DisplayFormat := FFormatFloat;
end;

end.

