object FmVSMovCab: TFmVSMovCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-160 :: IME-Cs'
  ClientHeight = 505
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 52
    Width = 784
    Height = 453
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitTop = 96
    ExplicitHeight = 409
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 390
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 346
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 52
    Width = 784
    Height = 453
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitTop = 96
    ExplicitHeight = 409
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        FocusControl = DBEdCodigo
      end
      object Label3: TLabel
        Left = 76
        Top = 16
        Width = 50
        Height = 13
        Caption = 'ID C'#243'digo:'
        FocusControl = dmkDBEdit1
      end
      object Label4: TLabel
        Left = 136
        Top = 16
        Width = 147
        Height = 13
        Caption = 'C'#243'digo / descri'#231#227'o movimento:'
        FocusControl = DBEdit1
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSMovCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 176
        Top = 32
        Width = 601
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsVSMovCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'CodigoID'
        DataSource = DsVSMovCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 136
        Top = 32
        Width = 38
        Height = 21
        DataField = 'MovimID'
        DataSource = DsVSMovCab
        TabOrder = 3
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 389
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 345
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'IME-&C'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'IME-&I'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 61
      Width = 784
      Height = 148
      Align = alTop
      DataSource = DsVSMovIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_TTW'
          Title.Caption = 'Tabela'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'IME-I'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data/Hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MovimNiv'
          Title.Caption = 'Nivel mov.'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Empresa'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGru1'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Nome produto'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pallet'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_LOC_CEN'
          Title.Caption = 'Centro / local'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Peso Kg'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaP2'
          Title.Caption = #193'rea ft'#178
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorT'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoKg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoM2'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SerieFch'
          Title.Caption = 'S'#233'rie'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SerieFch'
          Title.Caption = 'S'#233'rie'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NFeSer'
          Title.Caption = 'S'#233'rie NFe'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NFeNum'
          Title.Caption = 'N'#186' NFe'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeca'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeso'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtArM2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Fornecedor'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ClientMO'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CliVenda'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoMOKg'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoMOM2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoMOTot'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoPQ'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdAntArM2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorMP'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdAntArP2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdAntPeca'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdAntPeso'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdAntPeso'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DstGGX'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DstMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DstNivel1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DstNivel2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtCorrApo'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FornecMO'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GGXRcl'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_TTW'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ItemNFe'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'JmpGGX'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'JmpMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'JmpNivel1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'JmpNivel2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LnkNivXtr1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LnkNivXtr2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Misturou'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MovimTwn'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_DstMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_EstqMovimID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PALLET'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SrcMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaMPAG'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PedItsFin'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PedItsLib'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PedItsVda'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ReqMovEstq'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RmsGGX'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RmsMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RmsNivel1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RmsNivel2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcGGX'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcMovID'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcNivel1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcNivel1'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrcNivel2'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StqCenLoc'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Terceiro'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VSMulFrnCab'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VSMulNFeCab'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Observ'
          Width = 62
          Visible = True
        end>
    end
    object GBAvisos1: TGroupBox
      Left = 0
      Top = 336
      Width = 784
      Height = 53
      Align = alBottom
      Caption = ' Avisos: '
      TabOrder = 3
      ExplicitLeft = 28
      ExplicitTop = 256
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 36
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 38
        ExplicitTop = 3
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object PB1: TProgressBar
          Left = 0
          Top = 19
          Width = 780
          Height = 17
          Align = alBottom
          TabOrder = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 92
        Height = 32
        Caption = 'IME-Cs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 92
        Height = 32
        Caption = 'IME-Cs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 92
        Height = 32
        Caption = 'IME-Cs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSMovCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSMovCabBeforeOpen
    AfterOpen = QrVSMovCabAfterOpen
    BeforeClose = QrVSMovCabBeforeClose
    AfterScroll = QrVSMovCabAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM vsmovcab'
      'WHERE Codigo > 0')
    Left = 92
    Top = 233
    object QrVSMovCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovCabMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovCabCodigoID: TIntegerField
      FieldName = 'CodigoID'
    end
    object QrVSMovCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSMovCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSMovCab: TDataSource
    DataSet = QrVSMovCab
    Left = 92
    Top = 277
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 436
    Top = 400
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 320
    Top = 400
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      Visible = False
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      Visible = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      Visible = False
      OnClick = CabExclui1Click
    end
    object Irparajaneladomovimento1: TMenuItem
      Caption = '&Ir para janela do movimento'
      OnClick = Irparajaneladomovimento1Click
    end
    object AtualizaquantidadesdosIMEIs1: TMenuItem
      Caption = '&Atualiza quantidades dos IME-Is'
      OnClick = AtualizaquantidadesdosIMEIs1Click
    end
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVSMovItsCalcFields
    SQL.Strings = (
      ''
      'SELECT  "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, '
      'CAST(vmi.Codigo AS SIGNED) Codigo, '
      'CAST(vmi.Controle AS SIGNED) Controle, '
      'CAST(vmi.MovimCod AS SIGNED) MovimCod, '
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, '
      'CAST(vmi.MovimTwn AS SIGNED) MovimTwn, '
      'CAST(vmi.Empresa AS SIGNED) Empresa, '
      'CAST(vmi.ClientMO AS SIGNED) ClientMO, '
      'CAST(vmi.Terceiro AS SIGNED) Terceiro, '
      'CAST(vmi.CliVenda AS SIGNED) CliVenda, '
      'CAST(vmi.MovimID AS SIGNED) MovimID, '
      'CAST(vmi.DataHora AS DATETIME) DataHora, '
      'CAST(vmi.Pallet AS SIGNED) Pallet, '
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, '
      'CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas, '
      'CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg, '
      'CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2, '
      'CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2, '
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, '
      'CAST(vmi.SrcMovID AS SIGNED) SrcMovID, '
      'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1, '
      'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2, '
      'CAST(vmi.SrcGGX AS SIGNED) SrcGGX, '
      'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca, '
      'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso, '
      'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2, '
      'CAST(vmi.Observ AS CHAR) Observ, '
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, '
      'CAST(vmi.Ficha AS SIGNED) Ficha, '
      'CAST(vmi.Misturou AS UNSIGNED) Misturou, '
      'CAST(vmi.FornecMO AS SIGNED) FornecMO, '
      'CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg, '
      'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, '
      'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP, '
      'CAST(vmi.CustoPQ AS DECIMAL (15,4)) CustoPQ, '
      'CAST(vmi.DstMovID AS SIGNED) DstMovID, '
      'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1, '
      'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2, '
      'CAST(vmi.DstGGX AS SIGNED) DstGGX, '
      'CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca, '
      'CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, '
      'CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, '
      'CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, '
      'CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca, '
      'CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso, '
      'CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2, '
      'CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2, '
      'CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG, '
      'CAST(vmi.Marca AS CHAR) Marca, '
      'CAST(vmi.PedItsLib AS SIGNED) PedItsLib, '
      'CAST(vmi.PedItsFin AS SIGNED) PedItsFin, '
      'CAST(vmi.PedItsVda AS SIGNED) PedItsVda, '
      'CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2, '
      'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq, '
      'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc, '
      'CAST(vmi.ItemNFe AS SIGNED) ItemNFe, '
      'CAST(vmi.VSMulFrnCab AS SIGNED) VSMulFrnCab, '
      'CAST(vmi.NFeSer + 0.000 AS SIGNED) NFeSer, '
      'CAST(vmi.NFeNum AS SIGNED) NFeNum, '
      'CAST(vmi.VSMulNFeCab AS SIGNED) VSMulNFeCab, '
      'CAST(vmi.JmpMovID AS SIGNED) JmpMovID, '
      'CAST(vmi.JmpNivel1 AS SIGNED) JmpNivel1, '
      'CAST(vmi.JmpNivel2 AS SIGNED) JmpNivel2, '
      'CAST(vmi.RmsMovID AS SIGNED) RmsMovID, '
      'CAST(vmi.RmsNivel1 AS SIGNED) RmsNivel1, '
      'CAST(vmi.RmsNivel2 AS SIGNED) RmsNivel2, '
      'CAST(vmi.GGXRcl AS SIGNED) GGXRcl, '
      'CAST(vmi.RmsGGX AS SIGNED) RmsGGX, '
      'CAST(vmi.JmpGGX AS SIGNED) JmpGGX, '
      'CAST(vmi.RmsGGX AS SIGNED) RmsGGX, '
      'CAST(vmi.DtCorrApo AS DATETIME) DtCorrApo, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      ''
      'CAST(ggx.GraGru1 AS SIGNED) GraGru1, '
      'CAST(vmi.LnkNivXtr1 AS SIGNED) LnkNivXtr1,'
      'CAST(vmi.LnkNivXtr2 AS SIGNED) LnkNivXtr2, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, '
      'CAST(vsf.Nome AS CHAR) NO_SerieFch, '
      'CAST(vsp.Nome AS CHAR) NO_Pallet, '
      'CAST(CONCAT(scl.Nome, " (", scc.Nome, ")") AS CHAR) NO_LOC_CEN '
      ''
      ''
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      ''
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro'
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc '
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo '
      ''
      ''
      'WHERE vmi.Controle > 0'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'AND vmi.Controle=:P0'
      ''
      '')
    Left = 156
    Top = 233
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMovItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSMovItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSMovItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSMovItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSMovItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSMovItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSMovItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSMovItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSMovItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSMovItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSMovItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSMovItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSMovItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSMovItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSMovItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSMovItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSMovItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSMovItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSMovItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Origin = 'vspalleta.Nome'
      Size = 60
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSMovItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Origin = 'vsserfch.Nome'
      Size = 60
    end
    object QrVSMovItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovItsNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_DstMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DstMovID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_SrcMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_SrcMovID'
      Size = 255
      Calculated = True
    end
    object QrVSMovItsNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 103
    end
    object QrVSMovItsGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrVSMovItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrVSMovItsPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrVSMovItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrVSMovItsItemNFe: TLargeintField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrVSMovItsLnkNivXtr1: TLargeintField
      FieldName = 'LnkNivXtr1'
      Required = True
    end
    object QrVSMovItsLnkNivXtr2: TLargeintField
      FieldName = 'LnkNivXtr2'
      Required = True
    end
    object QrVSMovItsClientMO: TLargeintField
      FieldName = 'ClientMO'
      Required = True
    end
    object QrVSMovItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
    end
    object QrVSMovItsVSMulFrnCab: TLargeintField
      FieldName = 'VSMulFrnCab'
      Required = True
    end
    object QrVSMovItsNFeSer: TLargeintField
      FieldName = 'NFeSer'
      Required = True
    end
    object QrVSMovItsNFeNum: TLargeintField
      FieldName = 'NFeNum'
      Required = True
    end
    object QrVSMovItsVSMulNFeCab: TLargeintField
      FieldName = 'VSMulNFeCab'
      Required = True
    end
    object QrVSMovItsJmpMovID: TLargeintField
      FieldName = 'JmpMovID'
      Required = True
    end
    object QrVSMovItsJmpNivel1: TLargeintField
      FieldName = 'JmpNivel1'
      Required = True
    end
    object QrVSMovItsJmpNivel2: TLargeintField
      FieldName = 'JmpNivel2'
      Required = True
    end
    object QrVSMovItsRmsMovID: TLargeintField
      FieldName = 'RmsMovID'
      Required = True
    end
    object QrVSMovItsRmsNivel1: TLargeintField
      FieldName = 'RmsNivel1'
      Required = True
    end
    object QrVSMovItsRmsNivel2: TLargeintField
      FieldName = 'RmsNivel2'
      Required = True
    end
    object QrVSMovItsGGXRcl: TLargeintField
      FieldName = 'GGXRcl'
      Required = True
    end
    object QrVSMovItsRmsGGX: TLargeintField
      FieldName = 'RmsGGX'
      Required = True
    end
    object QrVSMovItsJmpGGX: TLargeintField
      FieldName = 'JmpGGX'
      Required = True
    end
    object QrVSMovItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSMovItsCustoM2: TFloatField
      FieldName = 'CustoM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSMovItsCustoKg: TFloatField
      FieldName = 'CustoKg'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 156
    Top = 281
  end
  object QrVMIsOri: TMySQLQuery
    Database = Dmod.MyDB
    Left = 328
    Top = 241
    object QrVMIsOriSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object QrVMI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 324
    Top = 293
    object QrVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
end
