unit VSPWEAltOriIMEI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.Grids, Vcl.DBGrids,
  UnProjGroup_Consts, dmkDBGridZTO, Vcl.Menus, UnAppEnums;

type
  TFmVSPWEAltOriIMEI = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label8: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdCliVenda: TdmkDBEdit;
    BtReabre: TBitBtn;
    Panel9: TPanel;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Panel10: TPanel;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdControle: TdmkEdit;
    EdPecas: TdmkEdit;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    EdValorT: TdmkEdit;
    LaValorT: TLabel;
    SbValorT: TSpeedButton;
    EdMediaM2Pc: TdmkEdit;
    Label18: TLabel;
    QrVMI: TmySQLQuery;
    QrVMINO_Pallet: TWideStringField;
    QrVMICodigo: TIntegerField;
    QrVMIControle: TIntegerField;
    QrVMIMovimCod: TIntegerField;
    QrVMIMovimNiv: TIntegerField;
    QrVMIEmpresa: TIntegerField;
    QrVMITerceiro: TIntegerField;
    QrVMIMovimID: TIntegerField;
    QrVMIDataHora: TDateTimeField;
    QrVMIPallet: TIntegerField;
    QrVMIGraGruX: TIntegerField;
    QrVMIPecas: TFloatField;
    QrVMIPesoKg: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIAreaP2: TFloatField;
    QrVMISrcMovID: TIntegerField;
    QrVMISrcNivel1: TIntegerField;
    QrVMISrcNivel2: TIntegerField;
    QrVMILk: TIntegerField;
    QrVMIDataCad: TDateField;
    QrVMIDataAlt: TDateField;
    QrVMIUserCad: TIntegerField;
    QrVMIUserAlt: TIntegerField;
    QrVMIAlterWeb: TSmallintField;
    QrVMIAtivo: TSmallintField;
    QrVMISdoVrtPeca: TFloatField;
    QrVMINO_FORNECE: TWideStringField;
    QrVMISdoVrtArM2: TFloatField;
    QrVMIValorT: TFloatField;
    QrVMIFicha: TIntegerField;
    QrVMIMisturou: TIntegerField;
    QrVMISerieFch: TIntegerField;
    QrVMIMovimTwn: TIntegerField;
    QrVMICliVenda: TIntegerField;
    QrVMILnkNivXtr1: TIntegerField;
    QrVMILnkNivXtr2: TIntegerField;
    QrVMISdoVrtPeso: TFloatField;
    QrVMIObserv: TWideStringField;
    QrVMIFornecMO: TIntegerField;
    QrVMICustoMOKg: TFloatField;
    QrVMICustoMOTot: TFloatField;
    QrVMIValorMP: TFloatField;
    QrVMIDstMovID: TIntegerField;
    QrVMIDstNivel1: TIntegerField;
    QrVMIDstNivel2: TIntegerField;
    QrVMIQtdGerPeca: TFloatField;
    QrVMIQtdGerPeso: TFloatField;
    QrVMIQtdGerArM2: TFloatField;
    QrVMIQtdGerArP2: TFloatField;
    QrVMIQtdAntPeca: TFloatField;
    QrVMIQtdAntPeso: TFloatField;
    QrVMIQtdAntArM2: TFloatField;
    QrVMIQtdAntArP2: TFloatField;
    QrVMIAptoUso: TSmallintField;
    QrVMINotaMPAG: TFloatField;
    QrVMISrcGGX: TIntegerField;
    QrVMIDstGGX: TIntegerField;
    QrVMIMarca: TWideStringField;
    QrVMIPedItsLib: TIntegerField;
    QrVMIPedItsFin: TIntegerField;
    QrVMIPedItsVda: TIntegerField;
    QrVMIMediaM2: TFloatField;
    QrVMIVSMulFrnCab: TIntegerField;
    QrVMIStqCenLoc: TIntegerField;
    QrVMINO_PRD_TAM_COR: TWideStringField;
    QrVMIClientMO: TIntegerField;
    Panel1: TPanel;
    QrVMIGraGruY: TIntegerField;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Label7: TLabel;
    DBEdDataHora: TdmkDBEdit;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label4: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    DBEdAntPecas: TDBEdit;
    DBEdAntPesoKg: TDBEdit;
    DBEdAntAreaM2: TDBEdit;
    EdMaxPecas: TdmkEdit;
    EdMaxAreaM2: TdmkEditCalc;
    EdMaxPesoKg: TdmkEdit;
    DBEdAntValorT: TDBEdit;
    Label10: TLabel;
    EdMaxValorT: TdmkEdit;
    Label11: TLabel;
    QrVMIItemNFe: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPecasRedefinido(Sender: TObject);
    procedure EdValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAreaM2Redefinido(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbValorTClick(Sender: TObject);
    procedure EdControleRedefinido(Sender: TObject);
    procedure EdPesoKgRedefinido(Sender: TObject);
    procedure EdMaxPecasRedefinido(Sender: TObject);
    procedure EdMaxPesoKgRedefinido(Sender: TObject);
    procedure EdMaxAreaM2Redefinido(Sender: TObject);
    procedure EdMaxValorTRedefinido(Sender: TObject);
    procedure EdAreaM2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    //
    procedure CalculaParcialPecas();
    procedure CalculaParcialArea();
    procedure CopiaTotaisIMEI();
    procedure ReopenVSInnIts(Controle: Integer);
    procedure ReopenGGXY();
    procedure ReopenVMI();
    procedure CalculaValorT();

  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab, FDsIts: TDataSource;
  end;

  var
  FmVSPWEAltOriIMEI: TFmVSPWEAltOriIMEI;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  CalcParc4Val, VSPWECab, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSPWEAltOriIMEI.BtOKClick(Sender: TObject);
const
  Observ = '';
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  MovimTwn   = 0;
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  EdFicha    = nil;
  //EdPesoKg   = nil;
  CustoMOKg   = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  ValorMP     = 0;
  QtdGerPeca  = 0;
  QtdGerPeso  = 0;
  QtdGerArM2  = 0;
  QtdGerArP2  = 0;
  AptoUso     = 0; // Eh venda!
  NotaMPAG    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib   = 0;
  PedItsFin   = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, CliVenda,
  SerieFch, Ficha, (*Misturou,*) DstNivel1, DstNivel2, GraGruY, SrcGGX,
  DstGGX, PedItsVda, ItemNFe, VSMulFrnCab, ClientMO, FornecMO, StqCenLoc,
  GGXRcl: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
begin
  if MyObjects.FIC(ImgTipo.SQLType <> stUpd, nil,
    'Status SQL deve ser altera��o!') then
      Exit;
  SrcMovID       := QrVMISrcMovID.Value;
  SrcNivel1      := QrVMISrcNivel1.Value;
  SrcNivel2      := QrVMISrcNivel2.Value;
  SrcGGX         := QrVMISrcGGX.Value;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClientMO       := QrVMIClientMO.Value;
  FornecMO       := QrVMIFornecMO.Value;
  StqCenLoc      := QrVMIStqCenLoc.Value;
  Terceiro       := QrVMITerceiro.Value;
  VSMulFrnCab    := QrVMIVSMulFrnCab.Value;
  CliVenda       := Geral.IMV(DBEdCliVenda.Text);
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := TEstqMovimID(QrVMIMovimID.Value);
  MovimNiv       := TEstqMovimNiv(QrVMIMovimNiv.Value);
  Pallet         := QrVMIPallet.Value;
  GraGruX        := QrVMIGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not VS_CRC_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  Pecas          := -EdPecas.ValueVariant;
  PesoKg         := -EdPesoKg.ValueVariant;
  AreaM2         := -EdAreaM2.ValueVariant;
  AreaP2         := -EdAreaP2.ValueVariant;
  ValorT         := -EdValorT.ValueVariant;
  //
  SerieFch       := QrVMISerieFch.Value;
  Ficha          := QrVMIFicha.Value;
  Marca          := QrVMIMarca.Value;
  //Misturou       := QrVMIMisturou.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
//  GraGruY        := VS_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex);
  GraGruY        := QrVMIGraGruY.Value;
  if QrVMIPedItsFin.Value <> 0 then
    PedItsVda := QrVMIPedItsFin.Value
  else
    PedItsVda := QrVMIPedItsLib.Value;
  //
  if VS_CRC_PF.VSFic(GraGruX, Empresa, 0, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, nil, nil(*EdPallet*), EdFicha, EdPecas,
  nil, nil, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca,
  nil) then
    Exit;
  if MyObjects.FIC((QrVMISdoVrtArM2.Value > 0) and (AreaM2 = 0),
  EdAreaM2, 'Informe a �rea!') then
    Exit else
  if MyObjects.FIC((QrVMISdoVrtPeso.Value > 0) and (PesoKg = 0),
  EdPesoKg, 'Informe o peso!') then
    Exit;
  //
  if MyObjects.FIC(GGXRcl = 0, EdGGXRcl,
  'Informe o material usado para emitir NFe!') then
    Exit;
  //
  ItemNFe     := QrVMIItemNFe.Value;
(*
  if MyObjects.FIC(ItemNFe = 0, EdItemNFe,
  'Informe o item da NFe!') then
    Exit;
*)
  if MyObjects.FIC(Pecas > EdMaxPecas.ValueVariant, EdMaxPecas,
    'Quantidade de pe�as excede o m�ximo!') then
      Exit;
  if MyObjects.FIC(PesoKg > EdMaxPesoKg.ValueVariant, EdMaxPesoKg,
    'Quantidade de peso kg excede o m�ximo!') then
      Exit;
  if MyObjects.FIC(AreaM2 > EdMaxAreaM2.ValueVariant, EdMaxAreaM2,
    'Quantidade de �rea m� excede o m�ximo!') then
      Exit;
(*
  if Dmod.VSFic(GraGruX, Empresa, Terceiro, Pallet, Pecas, AreaM2, PesoKg, ValorT,
  EdGraGruX, EdPallet, EdPecas, EdAreaM2, EdValorT) then
    Exit;
*)


  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);

  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei072(*Alera��o de baixa de couro em processo por IME-I*)) then
  begin
    VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    //VS_CRC_PF.AtualizaTotaisVSXxxCab('vspwecab', MovimCod);
    VS_CRC_PF.AtualizaTotaisVSPWECab(MovimCod);
    //VS_CRC_PF.AtualizaVSPedIts_Vda(PedItsVda);
    FmVSPWECab.AtualizaNFeItens((*MovimCod*));
    FmVSPWECab.LocCod(Codigo, Codigo);
    ReopenVSInnIts(Controle);
    Close;
  end;
end;

procedure TFmVSPWEAltOriIMEI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPWEAltOriIMEI.CalculaParcialArea();
var
  TxtArea: String;
  Fator, TotPecas, TotPesoKg, TotAreaM2, NewPecas, NewPesoKg, NewAreaM2: Double;
begin
  TotPecas  := EdMaxPecas.ValueVariant;
  TotPesoKg := EdMaxPesoKg.ValueVariant;
  TotAreaM2 := EdMaxAreaM2.ValueVariant;
  if TotAreaM2 = 0 then
  begin
    Geral.MB_Aviso('Total de �rea zerada!');
    Exit;
  end;
  TxtArea := '0';
  if InputQuery('Total de �rea', 'Informe o total da �rea:', TxtArea) then
  begin
    NewAreaM2  := Geral.DMV(TxtArea);
    //
    Fator     := NewAreaM2 / TotAreaM2;
    //
    NewPesoKg  := Round(Fator * TotPesoKg);
    NewPecas   := Trunc((Fator * TotPecas) * 2) / 2;
    //
    EdPecas.ValueVariant  := NewPecas;
    EdPesoKg.ValueVariant := NewPesoKg;
    EdAreaM2.ValueVariant := NewAreaM2;
  end;
end;

procedure TFmVSPWEAltOriIMEI.CalculaParcialPecas();
var
  TxtPecas: String;
  Fator, TotPecas, TotPesoKg, TotAreaM2, NewPecas, NewPesoKg, NewAreaM2: Double;
begin
  TotPecas  := EdMaxPecas.ValueVariant;
  TotPesoKg := EdMaxPesoKg.ValueVariant;
  TotAreaM2 := EdMaxAreaM2.ValueVariant;
  if TotPecas = 0 then
  begin
    Geral.MB_Aviso('Total de pe�as zerada!');
    Exit;
  end;
  TxtPecas := '0';
  if InputQuery('Total de pe�as', 'Informe o total de pe�as:', TxtPecas) then
  begin
    NewPecas  := Geral.DMV(TxtPecas);
    //
    Fator     := NewPecas / TotPecas;
    //
    NewPesoKg  := Fator * TotPesoKg;
    NewAreaM2  := Fator * TotAreaM2;
    //
    EdPecas.ValueVariant  := NewPecas;
    EdPesoKg.ValueVariant := NewPesoKg;
    EdAreaM2.ValueVariant := NewAreaM2;
  end;
end;

procedure TFmVSPWEAltOriIMEI.CalculaValorT();
var
  MaxPecas, MaxAreaM2, MaxPesoKg, MaxValorT,
  Pecas, AreaM2, PesoKg, Valor, Media: Double;
begin
  begin
    Pecas     := EdPecas.ValueVariant;
    AreaM2    := EdAreaM2.ValueVariant;
    PesoKg    := EdPesoKg.ValueVariant;
    MaxPecas  := EdMaxPecas.ValueVariant;
    MaxAreaM2 := EdMaxAreaM2.ValueVariant;
    MaxPesoKg := EdMaxPesoKg.ValueVariant;
    MaxValorT := EdMaxValorT.ValueVariant;
    //
    Valor := 0;
    if (MaxAreaM2 > 0) and (AreaM2 > 0) then
      Valor := AreaM2 * (MaxValorT / MaxAreaM2)
    else
    if (MaxPesoKg > 0) and (PesoKg > 0) then
      Valor := PesoKg * (MaxValorT / MaxPesoKg)
    else
    if (MaxPecas > 0) and (Pecas > 0) then
      Valor := Pecas * (MaxValorT / MaxPecas);
    //
    EdValorT.ValueVariant := Valor;
    //
    if Pecas > 0 then
      Media := AreaM2 / Pecas
    else
      Media := 0;
    EdMediaM2Pc.ValueVariant := Media;
  end;
end;

procedure TFmVSPWEAltOriIMEI.CopiaTotaisIMEI();
begin
  EdPecas.ValueVariant   := EdMaxPecas.ValueVariant;
  EdPesoKg.ValueVariant  := EdMaxPesoKg.ValueVariant;
  EdAreaM2.ValueVariant  := EdMaxAreaM2.ValueVariant;
  EdValorT.ValueVariant  := EdMaxValorT.ValueVariant;
end;

procedure TFmVSPWEAltOriIMEI.EdValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    CalculaValorT();
end;

procedure TFmVSPWEAltOriIMEI.EdAreaM2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    CalculaParcialArea();
end;

procedure TFmVSPWEAltOriIMEI.EdAreaM2Redefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSPWEAltOriIMEI.EdControleRedefinido(Sender: TObject);
begin
  ReopenVMI();
end;

procedure TFmVSPWEAltOriIMEI.EdMaxAreaM2Redefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSPWEAltOriIMEI.EdMaxPecasRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSPWEAltOriIMEI.EdMaxPesoKgRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSPWEAltOriIMEI.EdMaxValorTRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSPWEAltOriIMEI.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaTotaisIMEI();
  if Key = VK_F3 then
    CalculaParcialPecas();
end;

procedure TFmVSPWEAltOriIMEI.EdPecasRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSPWEAltOriIMEI.EdPesoKgRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSPWEAltOriIMEI.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEmpresa.DataSource    := FDsCab;
  DBEdCliVenda.DataSource   := FDsCab;
  //
  DBEdDataHora.DataSource   := FDsIts;
  DBEdAntPecas.DataSource   := FDsIts;
  DBEdAntPesoKg.DataSource  := FDsIts;
  DBEdAntValorT.DataSource  := FDsIts;
  DBEdAntAreaM2.DataSource  := FDsIts;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSPWEAltOriIMEI.FormCreate(Sender: TObject);
const
  Colunas = 2;
  Default = 2;
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenGGXY();
  //
end;

procedure TFmVSPWEAltOriIMEI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPWEAltOriIMEI.ReopenGGXY();
begin
  VS_CRC_PF.AbreGraGruXY(QrGGXRcl, '');
end;

procedure TFmVSPWEAltOriIMEI.ReopenVMI();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
  'SELECT ggx.GraGruY, pal.Nome NO_Pallet, vmi.*,',
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(vmi.SdoVrtPeca=0, 0, vmi.SdoVrtArM2 / vmi.SdoVrtPeca) MediaM2, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  pal ON pal.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.Controle=' + Geral.FF0(EdControle.ValueVariant),
  '']);
end;

procedure TFmVSPWEAltOriIMEI.ReopenVSInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSPWEAltOriIMEI.SbValorTClick(Sender: TObject);
begin
  EdValorT.Enabled := True;
  LaValorT.Enabled := True;
  EdValorT.SetFocus;
end;

end.
