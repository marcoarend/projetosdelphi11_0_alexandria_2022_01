unit VSMOEnvEnv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditCalc, mySQLDbTables, UnProjGroup_Vars, dmkDBLookupComboBox, dmkEditCB,
  dmkDBGridZTO, dmkEditDateTimePicker, dmkMemo;
type
  TFmVSMOEnvEnv = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdNFEMP_FatID: TdmkEdit;
    EdNFEMP_FatNum: TdmkEdit;
    EdNFEMP_Empresa: TdmkEdit;
    EdNFEMP_SerNF: TdmkEdit;
    Label9: TLabel;
    EdNFEMP_nNF: TdmkEdit;
    Label12: TLabel;
    EdNFEMP_nItem: TdmkEdit;
    Label13: TLabel;
    LaPecas: TLabel;
    EdNFEMP_Pecas: TdmkEdit;
    EdNFEMP_AreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    EdNFEMP_AreaP2: TdmkEditCalc;
    EdNFEMP_PesoKg: TdmkEdit;
    LaPeso: TLabel;
    Label14: TLabel;
    EdNFEMP_ValorT: TdmkEdit;
    SbRMP: TSpeedButton;
    Label31: TLabel;
    BtMONaoCobr: TBitBtn;
    BtContinoaCobranca: TBitBtn;
    GroupBox5: TGroupBox;
    Panel6: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    SbCF: TSpeedButton;
    Label43: TLabel;
    Label44: TLabel;
    EdCFTMP_FatID: TdmkEdit;
    EdCFTMP_FatNum: TdmkEdit;
    EdCFTMP_Empresa: TdmkEdit;
    EdCFTMP_SerCT: TdmkEdit;
    EdCFTMP_nCT: TdmkEdit;
    EdCFTMP_nItem: TdmkEdit;
    EdCFTMP_Pecas: TdmkEdit;
    EdCFTMP_AreaM2: TdmkEditCalc;
    EdCFTMP_AreaP2: TdmkEditCalc;
    EdCFTMP_PesoKg: TdmkEdit;
    EdCFTMP_ValorT: TdmkEdit;
    EdCFTMP_PesTrKg: TdmkEdit;
    EdCFTMP_CusTrKg: TdmkEdit;
    QrTransporta: TmySQLQuery;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    DsTransporta: TDataSource;
    Label45: TLabel;
    EdCFTMP_Terceiro: TdmkEditCB;
    CBCFTMP_Terceiro: TdmkDBLookupComboBox;
    EdNFEMP_Terceiro: TdmkEditCB;
    CBNFEMP_Terceiro: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    QrVMI: TmySQLQuery;
    DsVMI: TDataSource;
    DBGIMEIs: TdmkDBGridZTO;
    QrVMIControle: TIntegerField;
    QrVMIGraGruX: TIntegerField;
    QrVMIPecas: TFloatField;
    QrVMIPesoKg: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIAreaP2: TFloatField;
    QrVMINO_PRDA_TAM_COR: TWideStringField;
    QrVMIAtivo: TSmallintField;
    Panel5: TPanel;
    Panel8: TPanel;
    BtItem: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    QrSum: TmySQLQuery;
    QrSumPecas: TFloatField;
    QrSumPesoKg: TFloatField;
    QrSumAreaM2: TFloatField;
    QrSumAreaP2: TFloatField;
    QrSumAreaKg: TFloatField;
    EdVSVMI_MovimCod: TdmkEdit;
    Label2: TLabel;
    QrVMIValorT: TFloatField;
    QrSumValorT: TFloatField;
    QrVMIIDItem: TIntegerField;
    QrVMIPallet: TIntegerField;
    QrVMIMovimCod: TIntegerField;
    Label4: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    LaHora: TLabel;
    Label3: TLabel;
    MeNome: TdmkMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbRMPClick(Sender: TObject);
    procedure BtMONaoCobrClick(Sender: TObject);
    procedure BtContinoaCobrancaClick(Sender: TObject);
    procedure EdCFTMP_PesTrKgRedefinido(Sender: TObject);
    procedure EdCFTMP_CusTrKgRedefinido(Sender: TObject);
    procedure BtItemClick(Sender: TObject);
    procedure QrVMIAfterOpen(DataSet: TDataSet);
    procedure DBGIMEIsAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure DBGIMEIsExit(Sender: TObject);
    procedure EdCFTMP_nCTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MeNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    function  IncluiVSMOEnvEVMIs(): Boolean;
    procedure ValorTotalPeloPeso();

  public
    { Public declarations }
    FCodigo: Integer;
    FTabVMIQtdEnvEnv, FSQL_WHERE_VMI: String;
    procedure ReopenVMI(Controle: Integer);
  end;

  var
  FmVSMOEnvEnv: TFmVSMOEnvEnv;

implementation

uses UnMyObjects, UMySQLModule, Module, MyDBCheck, DmkDAC_PF, NFe_PF, ModVS_CRC,
  ModuleGeral, VSMOEnvEVMI, UnDmkProcFunc;

{$R *.DFM}

procedure TFmVSMOEnvEnv.BtContinoaCobrancaClick(Sender: TObject);
begin
(*
  EdNFCMO_SerNF.ValueVariant := VAR_NFCMO_SerNF;
  EdNFCMO_nNF.ValueVariant   := VAR_NFCMO_nNF;
  EdNFEMP_SerNF.ValueVariant := VAR_NFRMP_SerNF;
  EdNFEMP_nNF.ValueVariant   := VAR_NFRMP_nNF;
  if VAR_NFRMP_nNF > 0 then
    SbRMPClick(Self);
  if VAR_NFCMO_nNF > 0 then
    SbCMOClick(Self);
*)
end;

procedure TFmVSMOEnvEnv.BtItemClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrVMIControle.Value;
  if DBCheck.CriaFm(TFmVSMOEnvEVMI, FmVSMOEnvEVMI, afmoNegarComAviso) then
  begin
    FmVSMOEnvEVMI.ImgTipo.SQLType  := stUpd;
    FmVSMOEnvEVMI.FTabVMIQtdEnvEnv := FTabVMIQtdEnvEnv;
    FmVSMOEnvEVMI.EdControle.ValueVariant := Controle;
    FmVSMOEnvEVMI.EdPallet.ValueVariant   := QrVMIPallet.Value;
    FmVSMOEnvEVMI.EdGraGruX.ValueVariant  := QrVMIGraGruX.Value;
    FmVSMOEnvEVMI.EdNO_PRD_TAM_COR.Text   := QrVMINO_PRDA_TAM_COR.Value;
    FmVSMOEnvEVMI.EdPecas.ValueVariant    := QrVMIPecas.Value;
    FmVSMOEnvEVMI.EdPesoKg.ValueVariant   := QrVMIPesoKg.Value;
    FmVSMOEnvEVMI.EdAreaM2.ValueVariant   := QrVMIAreaM2.Value;
    FmVSMOEnvEVMI.EdAreaP2.ValueVariant   := QrVMIAreaP2.Value;
    //
    FmVSMOEnvEVMI.ShowModal;
    //
    FmVSMOEnvEVMI.Destroy;
    //
    ReopenVMI(Controle);
  end;
end;

procedure TFmVSMOEnvEnv.BtMONaoCobrClick(Sender: TObject);
begin
(*
  EdNFEMP_FatID.ValueVariant  := -1;
  EdNFEMP_FatNum.ValueVariant := -1;
  EdNFEMP_nNF.ValueVariant    := -1;
  //
  EdNFCMO_FatID.ValueVariant  := -1;
  EdNFCMO_FatNum.ValueVariant := -1;
  EdNFCMO_nNF.ValueVariant    := -1;
  //
  BtOKClick(Self);
*)
end;

procedure TFmVSMOEnvEnv.BtOKClick(Sender: TObject);
var
  Codigo, NFEMP_FatID, NFEMP_FatNum, NFEMP_Empresa, NFEMP_Terceiro, NFEMP_nItem,
  NFEMP_SerNF, NFEMP_nNF,
  (*
  VSVMI_Controle, VSVMI_Codigo, VSVMI_MovimID,
  VSVMI_MovimNiv, VSVMI_Empresa, VSVMI_SerNF, VSVMI_nNF,
  *)
  VSVMI_MovimCod,
  CFTMP_FatID, CFTMP_FatNum, CFTMP_Empresa, CFTMP_Terceiro, CFTMP_nItem,
  CFTMP_SerCT, CFTMP_nCT: Integer;
  NFEMP_Pecas, NFEMP_PesoKg, NFEMP_AreaM2, NFEMP_AreaP2, NFEMP_ValorT,
  CFTMP_Pecas, CFTMP_PesoKg, CFTMP_AreaM2, CFTMP_AreaP2, CFTMP_PesTrKg,
  CFTMP_CusTrKg, CFTMP_ValorT: Double;
  Nome, DataHora: String;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := dmkPF.LimpaSeSohCRLF(MeNome.Text);
  NFEMP_FatID    := EdNFEMP_FatID.ValueVariant;
  NFEMP_FatNum   := EdNFEMP_FatNum.ValueVariant;
  NFEMP_Empresa  := EdNFEMP_Empresa.ValueVariant;
  NFEMP_Terceiro := EdNFEMP_Terceiro.ValueVariant;
  NFEMP_nItem    := EdNFEMP_nItem.ValueVariant;
  NFEMP_SerNF    := EdNFEMP_SerNF.ValueVariant;
  NFEMP_nNF      := EdNFEMP_nNF.ValueVariant;
  NFEMP_Pecas    := EdNFEMP_Pecas.ValueVariant;
  NFEMP_PesoKg   := EdNFEMP_PesoKg.ValueVariant;
  NFEMP_AreaM2   := EdNFEMP_AreaM2.ValueVariant;
  NFEMP_AreaP2   := EdNFEMP_AreaP2.ValueVariant;
  NFEMP_ValorT   := EdNFEMP_ValorT.ValueVariant;
(*
  VSVMI_Controle := EdVSVMI_Controle.ValueVariant;
  VSVMI_Codigo   := EdVSVMI_Codigo.ValueVariant;
  VSVMI_MovimID  := EdVSVMI_MovimID.ValueVariant;
  VSVMI_MovimNiv := EdVSVMI_MovimNiv.ValueVariant;
*)
  VSVMI_MovimCod := EdVSVMI_MovimCod.ValueVariant;
(*VSVMI_Empresa  := EdVSVMI_Empresa.ValueVariant;
  VSVMI_SerNF    := EdVSVMI_SerNF.ValueVariant;
  VSVMI_nNF      := EdVSVMI_nNF.ValueVariant;
*)
  CFTMP_FatID    := EdCFTMP_FatID.ValueVariant;
  CFTMP_FatNum   := EdCFTMP_FatNum.ValueVariant;
  CFTMP_Empresa  := EdCFTMP_Empresa.ValueVariant;
  CFTMP_Terceiro := EdCFTMP_Terceiro.ValueVariant;
  CFTMP_nItem    := EdCFTMP_nItem.ValueVariant;
  CFTMP_SerCT    := EdCFTMP_SerCT.ValueVariant;
  CFTMP_nCT      := EdCFTMP_nCT.ValueVariant;
  CFTMP_Pecas    := EdCFTMP_Pecas.ValueVariant;
  CFTMP_PesoKg   := EdCFTMP_PesoKg.ValueVariant;
  CFTMP_AreaM2   := EdCFTMP_AreaM2.ValueVariant;
  CFTMP_AreaP2   := EdCFTMP_AreaP2.ValueVariant;
  CFTMP_PesTrKg  := EdCFTMP_PesTrKg.ValueVariant;
  CFTMP_CusTrKg  := EdCFTMP_CusTrKg.ValueVariant;
  CFTMP_ValorT   := EdCFTMP_ValorT.ValueVariant;
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  //
  if MyObjects.FIC(TPData.Date < 2, TPData, 'Data/hora n�o definida!')
  //
  //if MyObjects.FIC(VSVMI_SerNF = 0, EdVSVMI_SerNF, 'Informe a s�rie da NF m�e do envio da mat�ria-prima!')
  //if MyObjects.FIC(VSVMI_nNF = 0, EdVSVMI_nNF, 'Informe o n�mero da NF m�e do envio da mat�ria-prima!')
  //or MyObjects.FIC(NFEMP_SerNF = 0, EdNFEMP_SerNF, 'Informe a s�rie da NF do envio da mat�ria-prima!')
  or MyObjects.FIC(NFEMP_nNF = 0, EdNFEMP_nNF, 'Informe o n�mero da NF do envio da mat�ria-prima!')
  or MyObjects.FIC(NFEMP_Terceiro = 0, EdNFEMP_Terceiro, 'Informe o Fornecedor de MO!')
  or MyObjects.FIC(NFEMP_Empresa = 0, EdNFEMP_Empresa, 'Informe a empresa do envio da mat�ria-prima!')
  or MyObjects.FIC(NFEMP_FatID = 0, EdNFEMP_FatID, 'Informe o ID do envio da mat�ria-prima!')
  or MyObjects.FIC(NFEMP_FatNum = 0, EdNFEMP_FatNum, 'Informe o c�digo do envio da mat�ria-prima!')
  //
  //or MyObjects.FIC(CFTMP_SerCT = 0, EdCFTMP_SerCT, 'Informe a s�rie do CT do envio da mat�ria-prima!')
  or MyObjects.FIC(CFTMP_nCT = 0, EdCFTMP_nCT, 'Informe o n�mero do CT do envio da mat�ria-prima!')
  or MyObjects.FIC(CFTMP_Empresa = 0, EdCFTMP_Empresa, 'Informe a empresa para o frete do couro!')
  or MyObjects.FIC(CFTMP_FatID = 0, EdCFTMP_FatID, 'Informe o ID do frete do couro!')
  or MyObjects.FIC(CFTMP_FatNum = 0, EdCFTMP_FatNum, 'Informe o c�digo do frete do couro!')
  or MyObjects.FIC(CFTMP_Terceiro = 0, EdCFTMP_Terceiro, 'Informe o transportador do couro!')
  then Exit;
  //
  if Codigo = 0 then
  begin
    Codigo := UMyMod.BPGS1I32('vsmoenvenv', 'Codigo', '', '', tsPos, SQLType, Codigo);
    EdCodigo.ValueVariant := Codigo;
  end;
  //
  if not IncluiVSMOEnvEVMIs() then exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmoenvenv', False, [
  'Nome', 'DataHora',
  'NFEMP_FatID', 'NFEMP_FatNum', 'NFEMP_Empresa',
  'NFEMP_Terceiro', 'NFEMP_nItem', 'NFEMP_SerNF',
  'NFEMP_nNF', 'NFEMP_Pecas', 'NFEMP_PesoKg',
  'NFEMP_AreaM2', 'NFEMP_AreaP2', 'NFEMP_ValorT',
  (*
  'VSVMI_Controle', 'VSVMI_Codigo', 'VSVMI_MovimID',
  'VSVMI_MovimNiv',
  *)
  'VSVMI_MovimCod',
  (*
  'VSVMI_Empresa',
  'VSVMI_SerNF', 'VSVMI_nNF',
  *)
  'CFTMP_FatID',
  'CFTMP_FatNum', 'CFTMP_Empresa', 'CFTMP_Terceiro',
  'CFTMP_nItem', 'CFTMP_SerCT', 'CFTMP_nCT',
  'CFTMP_Pecas', 'CFTMP_PesoKg', 'CFTMP_AreaM2',
  'CFTMP_AreaP2', 'CFTMP_PesTrKg', 'CFTMP_CusTrKg',
  'CFTMP_ValorT'], [
  'Codigo'], [
  Nome, DataHora,
  NFEMP_FatID, NFEMP_FatNum, NFEMP_Empresa,
  NFEMP_Terceiro, NFEMP_nItem, NFEMP_SerNF,
  NFEMP_nNF, NFEMP_Pecas, NFEMP_PesoKg,
  NFEMP_AreaM2, NFEMP_AreaP2, NFEMP_ValorT,
  (*
  VSVMI_Controle, VSVMI_Codigo, VSVMI_MovimID,
  VSVMI_MovimNiv,
  *)
  VSVMI_MovimCod,
  (*
  VSVMI_Empresa,
  VSVMI_SerNF, VSVMI_nNF,
  *)
  CFTMP_FatID,
  CFTMP_FatNum, CFTMP_Empresa, CFTMP_Terceiro,
  CFTMP_nItem, CFTMP_SerCT, CFTMP_nCT,
  CFTMP_Pecas, CFTMP_PesoKg, CFTMP_AreaM2,
  CFTMP_AreaP2, CFTMP_PesTrKg, CFTMP_CusTrKg,
  CFTMP_ValorT], [
  Codigo], True) then
  begin
    FCodigo := Codigo;
    DmModVS_CRC.AtualizaSaldoVSMOEnvEnv(Codigo);
    //
    DmModVS_CRC.CalculaFreteVMIdeMOEnvEnv(VSVMI_MovimCod);
    //
    DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(VSVMI_MovimCod);
    //
    Close;
  end;
end;

procedure TFmVSMOEnvEnv.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMOEnvEnv.DBGIMEIsAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
var
  Ativo, Controle: Integer;
begin
  Controle := QrVMIControle.Value;
  //
  if DBGIMEIs.SelectedRows.CurrentRowSelected = True then
    Ativo := 1
  else
    Ativo := 0;
  //
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FTabVMIQtdEnvEnv, False, [
  'Ativo'], [
  'Controle'], [
  Ativo], [
  Controle], False) then
    ReopenVMI(Controle);
end;

procedure TFmVSMOEnvEnv.DBGIMEIsExit(Sender: TObject);
begin
  ReopenVMI(QrVMIControle.Value);
end;

procedure TFmVSMOEnvEnv.EdCFTMP_CusTrKgRedefinido(Sender: TObject);
begin
  ValorTotalPeloPeso();
end;

procedure TFmVSMOEnvEnv.EdCFTMP_nCTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nCTe: Integer;
begin
  if Key = VK_F4 then
  begin
    EdCFTMP_nCT.ValueVariant := DmModVS_CRC.ObtemProximoCTe(
      EdCFTMP_Terceiro.ValueVariant, EdCFTMP_SerCT.ValueVariant);
    if nCTe > 0 then
      EdCFTMP_nCT.ValueVariant := nCTe;
  end;
end;

procedure TFmVSMOEnvEnv.EdCFTMP_PesTrKgRedefinido(Sender: TObject);
begin
  ValorTotalPeloPeso();
end;

procedure TFmVSMOEnvEnv.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMOEnvEnv.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  FCodigo := 0;
  FSQL_WHERE_VMI := '';
  //
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
end;

procedure TFmVSMOEnvEnv.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmVSMOEnvEnv.IncluiVSMOEnvEVMIs(): Boolean;
var
  VSMOEnvEnv: Integer;
  FListaIMECs: TMyGrlArrInt;

  //
{
  procedure IncrementaListaIMECs(MovimCod: Integer);
  var
    I: Integer;
    Achou: Boolean;
  begin
    Achou := False;
    for I := Low(FListaIMECs) to High(FListaIMECs) do
    begin
      if FListaIMECs[I] = MovimCod then
      begin
        Achou := True;
        Break;
      end;
    end;
    if Achou = False then
    begin
      I := Length(FListaIMECs);
      SetLength(FListaIMECs, I + 1);
      FListaIMECs[I] := MovimCod;
    end;
  end;
}
  //
  procedure IncluiAtual();
  var
    Codigo, VSMovIts: Integer;
    ValorFrete, Pecas, PesoKg, AreaM2, AreaP2: Double;
    SQLType: TSQLType;
  begin
    if QrVMIAtivo.Value = 0 then
      Exit;
    SQLType        := stIns;
    Codigo         := QrVMIIDItem.Value;
    VSMovIts       := QrVMIControle.Value;
    Pecas          := QrVMIPecas.Value;
    PesoKg         := QrVMIPesoKg.Value;
    AreaM2         := QrVMIAreaM2.Value;
    AreaP2         := QrVMIAreaP2.Value;
    if QrSumAreaKg.Value = 0 then
      ValorFrete     := 0
    else
      ValorFrete := ((AreaM2 * 4) + PesoKg) / QrSumAreaKg.Value *
        EdCFTMP_ValorT.ValueVariant;
    //
    if Codigo = 0 then
      Codigo := UMyMod.BPGS1I32('vsmoenvevmi', 'Codigo', '', '', tsPos, stIns, Codigo);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmoenvevmi', False, [
    'VSMOEnvEnv', 'VSMovIts', 'ValorFrete',
    'Pecas', 'PesoKg', 'AreaM2',
    'AreaP2'], [
    'Codigo'], [
    VSMOEnvEnv, VSMovIts, ValorFrete,
    Pecas, PesoKg, AreaM2,
    AreaP2], [
    Codigo], True);
  end;
const
  sProcName = 'TFmVSMOEnvEnv.IncluiVSMOEnvEVMIs()';
var
  J: Integer;
begin
  Result := False;
  SetLength(FListaIMECs, 0);
  VSMOEnvEnv     := EdCodigo.ValueVariant;
  Dmod.MyDB.Execute('DELETE FROM vsmoenvevmi WHERE VSMOEnvEnv=' + Geral.FF0(VSMOEnvEnv));
  QrVMI.First;
  while not QrVMI.Eof do
  begin
    IncluiAtual();
    DmModVS_CRC.AtualizaFreteVMIdeMOEnvEnv(QrVMIControle.Value);
    dmkPF.IncrementaListaDeInteiros(FListaIMECs, QrVMIMovimCod.Value);
    //
    QrVMI.Next;
  end;
  //
  for J := Low(FListaIMECs) to High(FListaIMECs) do
    DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(FListaIMECs[J]);
  //
  Result := True;
end;

procedure TFmVSMOEnvEnv.MeNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    Key := 0;
    BtOK.SetFocus;
  end;
end;

procedure TFmVSMOEnvEnv.QrVMIAfterOpen(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, DModG.MyPID_DB, [
  'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
  'SUM(ValorT) ValorT, SUM((AreaM2*4) + PesoKg) AreaKg ',
  'FROM ' + FTabVMIQtdEnvEnv,
  'WHERE Ativo=1',
  '']);
  //
  EdNFEMP_Pecas.ValueVariant  := QrSumPecas.Value;
  EdNFEMP_PesoKg.ValueVariant := QrSumPesoKg.Value;
  EdNFEMP_AreaM2.ValueVariant := QrSumAreaM2.Value;
  EdNFEMP_AreaP2.ValueVariant := QrSumAreaP2.Value;
  EdNFEMP_ValorT.ValueVariant := QrSumValorT.Value;
  //
  EdCFTMP_Pecas.ValueVariant  := QrSumPecas.Value;
  EdCFTMP_PesoKg.ValueVariant := QrSumPesoKg.Value;
  EdCFTMP_AreaM2.ValueVariant := QrSumAreaM2.Value;
  EdCFTMP_AreaP2.ValueVariant := QrSumAreaP2.Value;
  //
  QrVMI.DisableControls;
  try
    MyObjects.SetaItensBookmarkAtivos(Self, TDBGrid(DBGIMEIs), 'Ativo');
  finally
    QrVMI.EnableControls;
  end;
end;

procedure TFmVSMOEnvEnv.ReopenVMI(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, DModG.MyPID_DB, [
  'SELECT * FROM ' + FTabVMIQtdEnvEnv,
  '']);
  if Controle <> 0 then
    QrVMI.Locate('Controle', Controle, []);
end;

procedure TFmVSMOEnvEnv.SbRMPClick(Sender: TObject);
begin
  DmModVS_CRC.PesquisaNFe(EdNFEMP_FatID, EdNFEMP_FatNum, EdNFEMP_Empresa, EdNFEMP_Terceiro,
  EdNFEMP_SerNF, EdNFEMP_nNF);
end;

procedure TFmVSMOEnvEnv.ValorTotalPeloPeso();
var
  CFTPA_CusTrKg, CFTPA_PesTrKg: Double;
begin
  CFTPA_CusTrKg := EdCFTMP_CusTrKg.ValueVariant;
  CFTPA_PesTrKg := EdCFTMP_PesTrKg.ValueVariant;
  if (CFTPA_CusTrKg >= 0.000001) and (CFTPA_PesTrKg >= 0.001) then
  begin
    //EdNFCMO_CusMOM2.ValueVariant := 0;
    EdCFTMP_ValorT.ValueVariant := CFTPA_PesTrKg * CFTPA_CusTrKg;
  end;
end;

{
object GroupBox2: TGroupBox
  Left = 0
  Top = 45
  Width = 651
  Height = 72
  Align = alTop
  Caption = ' Item produzido: '
  TabOrder = 1
  object Panel5: TPanel
    Left = 2
    Top = 15
    Width = 647
    Height = 55
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    object Label2: TLabel
      Left = 72
      Top = 0
      Width = 14
      Height = 13
      Caption = 'ID:'
      Enabled = False
    end
    object Label3: TLabel
      Left = 104
      Top = 0
      Width = 29
      Height = 13
      Caption = 'N'#237'vel:'
      Enabled = False
    end
    object Label4: TLabel
      Left = 136
      Top = 0
      Width = 28
      Height = 13
      Caption = 'IME-I:'
      Enabled = False
    end
    object Label5: TLabel
      Left = 212
      Top = 0
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      Enabled = False
    end
    object Label6: TLabel
      Left = 288
      Top = 0
      Width = 32
      Height = 13
      Caption = 'IME-C:'
      Enabled = False
    end
    object Label8: TLabel
      Left = 12
      Top = 0
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      Enabled = False
    end
    object Label26: TLabel
      Left = 364
      Top = 0
      Width = 44
      Height = 13
      Caption = 'S'#233'rie NF:'
    end
    object Label27: TLabel
      Left = 416
      Top = 0
      Width = 42
      Height = 13
      Caption = 'Num NF:'
    end
    object EdVSVMI_MovimID: TdmkEdit
      Left = 72
      Top = 16
      Width = 29
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_MovimNiv: TdmkEdit
      Left = 104
      Top = 16
      Width = 29
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_Controle: TdmkEdit
      Left = 136
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_Codigo: TdmkEdit
      Left = 212
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_MovimCod: TdmkEdit
      Left = 288
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_Empresa: TdmkEdit
      Left = 12
      Top = 16
      Width = 56
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdVSVMI_SerNF: TdmkEdit
      Left = 363
      Top = 16
      Width = 48
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '-1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = -1
      ValWarn = False
    end
    object EdVSVMI_nNF: TdmkEdit
      Left = 415
      Top = 16
      Width = 72
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object Memo1: TMemo
      Left = 496
      Top = 0
      Width = 149
      Height = 53
      Enabled = False
      Lines.Strings = (
        '<-- S'#233'rie e NF do cabe'#231'alho.'
        '<-- Deprecado!'
        '<-- Apenas Informativo.')
      ReadOnly = True
      TabOrder = 8
    end
  end
end
}
end.
