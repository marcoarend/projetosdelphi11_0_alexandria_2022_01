object FmVSCalSubPrdItsMul: TFmVSCalSubPrdItsMul
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-274 :: Sub Produto Processado (M'#250'ltiplo)'
  ClientHeight = 622
  ClientWidth = 619
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 501
    Width = 619
    Height = 7
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 619
    Height = 180
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    TabOrder = 0
    object DGDadosOri: TDBGrid
      Left = 2
      Top = 15
      Width = 615
      Height = 163
      Align = alClient
      DataSource = DsVSCalOriIMEI
      Enabled = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_TTW'
          Title.Caption = 'Arquivo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ReqMovEstq'
          Title.Caption = 'N'#176' RME'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'IME-I'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pallet'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SerieFch'
          Title.Caption = 'S'#233'rie RMP'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Artigos origem da opera'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaMPAG'
          Title.Caption = 'Nota MPAG'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Peso kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Fornecedor'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdGerPeca'
          Title.Caption = 'P'#231' gerado'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdGerPeso'
          Title.Caption = 'Kg gerado'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoPQ'
          Title.Caption = 'Custo PQ'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoMOKg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoMOTot'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Observ'
          Title.Caption = 'Observa'#231#245'es'
          Visible = True
        end>
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 228
    Width = 619
    Height = 183
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label1: TLabel
      Left = 96
      Top = 16
      Width = 151
      Height = 13
      Caption = 'Sub Produto (F4 - '#250'ltima usada):'
    end
    object LaPecas: TLabel
      Left = 12
      Top = 56
      Width = 33
      Height = 13
      Caption = 'Pe'#231'as:'
      Enabled = False
    end
    object LaAreaM2: TLabel
      Left = 104
      Top = 56
      Width = 39
      Height = 13
      Caption = #193'rea m'#178':'
      Enabled = False
    end
    object LaAreaP2: TLabel
      Left = 200
      Top = 56
      Width = 37
      Height = 13
      Caption = #193'rea ft'#178':'
      Enabled = False
    end
    object LaPeso: TLabel
      Left = 304
      Top = 56
      Width = 27
      Height = 13
      Caption = 'Peso:'
    end
    object Label7: TLabel
      Left = 504
      Top = 96
      Width = 36
      Height = 13
      Caption = '$ Total:'
      Enabled = False
    end
    object Label13: TLabel
      Left = 12
      Top = 96
      Width = 68
      Height = 13
      Caption = '$ total M-obra:'
    end
    object Label14: TLabel
      Left = 116
      Top = 96
      Width = 240
      Height = 13
      Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
    end
    object Label16: TLabel
      Left = 12
      Top = 136
      Width = 61
      Height = 13
      Caption = 'Observa'#231#227'o:'
    end
    object Label8: TLabel
      Left = 400
      Top = 56
      Width = 76
      Height = 13
      Caption = '$ Total M-prima:'
      Enabled = False
    end
    object Label12: TLabel
      Left = 504
      Top = 56
      Width = 62
      Height = 13
      Caption = '$/kg M-obra:'
    end
    object Label53: TLabel
      Left = 457
      Top = 137
      Width = 58
      Height = 13
      Caption = 'Data / hora:'
      Color = clBtnFace
      ParentColor = False
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 152
      Top = 32
      Width = 453
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGraGruX
      TabOrder = 2
      OnKeyDown = CBGraGruXKeyDown
      dmkEditCB = EdGraGruX
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdGraGruX: TdmkEditCB
      Left = 96
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdGraGruXKeyDown
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnFormActivate
    end
    object EdPecas: TdmkEdit
      Left = 12
      Top = 72
      Width = 88
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdAreaM2: TdmkEditCalc
      Left = 104
      Top = 72
      Width = 92
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaM2'
      UpdCampo = 'AreaM2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      dmkEditCalcA = EdAreaP2
      CalcType = ctM2toP2
      CalcFrac = cfQuarto
    end
    object EdAreaP2: TdmkEditCalc
      Left = 200
      Top = 72
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaP2'
      UpdCampo = 'AreaP2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      dmkEditCalcA = EdAreaM2
      CalcType = ctP2toM2
      CalcFrac = cfCento
    end
    object EdPesoKg: TdmkEdit
      Left = 304
      Top = 72
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'PesoKg'
      UpdCampo = 'PesoKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdPesoKgChange
    end
    object EdValorT: TdmkEdit
      Left = 504
      Top = 112
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 12
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorT'
      UpdCampo = 'ValorT'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdCustoMOTot: TdmkEdit
      Left = 12
      Top = 112
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'CustoMOTot'
      UpdCampo = 'CustoMOTot'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdValorMPChange
    end
    object EdObserv: TdmkEdit
      Left = 12
      Top = 152
      Width = 441
      Height = 21
      TabOrder = 13
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Observ'
      UpdCampo = 'Observ'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdValorMP: TdmkEdit
      Left = 400
      Top = 72
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'ValorMP'
      UpdCampo = 'ValorMP'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdValorMPChange
    end
    object EdCustoMOKg: TdmkEdit
      Left = 504
      Top = 72
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'CustoMOKg'
      UpdCampo = 'CustoMOKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdCustoMOKgChange
    end
    object EdFornecMO: TdmkEditCB
      Left = 116
      Top = 112
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'FornecMO'
      UpdCampo = 'FornecMO'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBFornecMO
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnFormActivate
    end
    object CBFornecMO: TdmkDBLookupComboBox
      Left = 172
      Top = 112
      Width = 329
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsPrestador
      TabOrder = 11
      OnKeyDown = CBGraGruXKeyDown
      dmkEditCB = EdFornecMO
      QryCampo = 'Pallet'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object TPData: TdmkEditDateTimePicker
      Left = 457
      Top = 152
      Width = 108
      Height = 21
      Date = 45064.000000000000000000
      Time = 0.639644131944805800
      TabOrder = 14
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'DtHrAberto'
      UpdCampo = 'DtHrAberto'
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdHora: TdmkEdit
      Left = 565
      Top = 152
      Width = 40
      Height = 21
      TabOrder = 15
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00:00'
      QryName = 'QrVSGerArt'
      QryCampo = 'DtHrAberto'
      UpdCampo = 'DtHrAberto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 619
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GB_R: TGroupBox
      Left = 571
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 523
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 421
        Height = 32
        Caption = 'Sub Produto Processado (M'#250'ltiplo)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 421
        Height = 32
        Caption = 'Sub Produto Processado (M'#250'ltiplo)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 421
        Height = 32
        Caption = 'Sub Produto Processado (M'#250'ltiplo)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 508
    Width = 619
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 5
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 615
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 552
    Width = 619
    Height = 70
    Align = alBottom
    TabOrder = 6
    object PnSaiDesis: TPanel
      Left = 473
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 471
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 411
    Width = 619
    Height = 90
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label49: TLabel
      Left = 12
      Top = 3
      Width = 60
      Height = 13
      Caption = 'Localiza'#231#227'o:'
    end
    object Label50: TLabel
      Left = 510
      Top = 3
      Width = 74
      Height = 13
      Caption = 'Req.Mov.Estq.:'
      Color = clBtnFace
      ParentColor = False
    end
    object SbStqCenLoc: TSpeedButton
      Left = 489
      Top = 19
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbStqCenLocClick
    end
    object Label62: TLabel
      Left = 12
      Top = 45
      Width = 139
      Height = 13
      Caption = 'Quem fica com o subproduto:'
    end
    object Label20: TLabel
      Left = 296
      Top = 45
      Width = 29
      Height = 13
      Caption = 'Pallet:'
    end
    object LaVSRibCla: TLabel
      Left = 354
      Top = 45
      Width = 76
      Height = 13
      Caption = 'Nome do Artigo:'
    end
    object SbPallet1: TSpeedButton
      Left = 584
      Top = 60
      Width = 23
      Height = 23
      Caption = '...'
      OnClick = SbPallet1Click
    end
    object EdStqCenLoc: TdmkEditCB
      Left = 12
      Top = 19
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'StqCenLoc'
      UpdCampo = 'StqCenLoc'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStqCenLoc
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnFormActivate
    end
    object CBStqCenLoc: TdmkDBLookupComboBox
      Left = 67
      Top = 19
      Width = 421
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_LOC_CEN'
      ListSource = DsStqCenLoc
      TabOrder = 1
      dmkEditCB = EdStqCenLoc
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdReqMovEstq: TdmkEdit
      Left = 511
      Top = 19
      Width = 93
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ReqMovEstq'
      UpdCampo = 'ReqMovEstq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdClientMO: TdmkEditCB
      Left = 12
      Top = 61
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBClientMO
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnFormActivate
    end
    object CBClientMO: TdmkDBLookupComboBox
      Left = 71
      Top = 61
      Width = 222
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsClientMO
      TabOrder = 4
      dmkEditCB = EdClientMO
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPallet: TdmkEditCB
      Left = 296
      Top = 61
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPallet
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPallet: TdmkDBLookupComboBox
      Left = 354
      Top = 61
      Width = 227
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsVSPallet
      TabOrder = 6
      dmkEditCB = EdPallet
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 408
    Top = 56
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 408
    Top = 104
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 472
    Top = 56
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 472
    Top = 104
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 344
    Top = 56
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 344
    Top = 104
  end
  object QrVSCalOriIMEI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 32
    Top = 105
    object QrVSCalOriIMEICodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSCalOriIMEIControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSCalOriIMEIMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSCalOriIMEIMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSCalOriIMEIMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSCalOriIMEIEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSCalOriIMEITerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSCalOriIMEICliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSCalOriIMEIMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSCalOriIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSCalOriIMEIPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSCalOriIMEIGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSCalOriIMEIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriIMEIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriIMEIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEISrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSCalOriIMEISrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSCalOriIMEISrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSCalOriIMEISrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSCalOriIMEISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSCalOriIMEISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriIMEISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSCalOriIMEISerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSCalOriIMEIFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSCalOriIMEIMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSCalOriIMEIFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSCalOriIMEICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSCalOriIMEIDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSCalOriIMEIDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSCalOriIMEIDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSCalOriIMEIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSCalOriIMEIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriIMEIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSCalOriIMEIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSCalOriIMEIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSCalOriIMEINO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSCalOriIMEINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCalOriIMEINO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSCalOriIMEIID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSCalOriIMEINO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSCalOriIMEINO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSCalOriIMEIReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSCalOriIMEICustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSCalOriIMEIVSMulFrnCab: TLargeintField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSCalOriIMEIClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSCalOriIMEIMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object DsVSCalOriIMEI: TDataSource
    DataSet = QrVSCalOriIMEI
    Left = 32
    Top = 153
  end
  object QrSum: TMySQLQuery
    Database = Dmod.MyDB
    Left = 124
    Top = 108
    object QrSumPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrClientMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 252
    Top = 80
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 252
    Top = 128
  end
  object QrVSPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspalleta let'
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 556
    Top = 105
    object QrVSPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPalletLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPalletDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPalletDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPalletUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPalletUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPalletAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPalletAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPalletEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPalletStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPalletCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPalletNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPalletNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet: TDataSource
    DataSet = QrVSPallet
    Left = 556
    Top = 149
  end
  object PMPallet: TPopupMenu
    Left = 519
    Top = 472
    object Criar1: TMenuItem
      Caption = '&Criar Novo'
      OnClick = Criar1Click
    end
    object Gerenciamento1: TMenuItem
      Caption = '&Gerenciamento'
      OnClick = Gerenciamento1Click
    end
  end
end
