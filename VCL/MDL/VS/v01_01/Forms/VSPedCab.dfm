object FmVSPedCab: TFmVSPedCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-091 :: Pedido de Venda'
  ClientHeight = 691
  ClientWidth = 1022
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1022
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 532
      Width = 1022
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 882
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1022
      Height = 245
      Align = alTop
      TabOrder = 1
      object PnEdit: TPanel
        Left = 2
        Top = 15
        Width = 1018
        Height = 228
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 8
          Top = 8
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label10: TLabel
          Left = 368
          Top = 8
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object Label5: TLabel
          Left = 8
          Top = 48
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label22: TLabel
          Left = 124
          Top = 48
          Width = 49
          Height = 13
          Caption = 'Vendedor:'
        end
        object Label38: TLabel
          Left = 8
          Top = 92
          Width = 293
          Height = 13
          Caption = 'Observa'#231#245'es internas de uso da empresa (n'#227'o ser'#225' impresso):'
        end
        object Label19: TLabel
          Left = 696
          Top = 8
          Width = 75
          Height = 13
          Caption = 'Transportadora:'
        end
        object LaCondicaoPG: TLabel
          Left = 404
          Top = 48
          Width = 119
          Height = 13
          Caption = 'Condi'#231#227'o de pagamento:'
        end
        object Label23: TLabel
          Left = 8
          Top = 164
          Width = 330
          Height = 13
          Caption = 
            'OC - Ordem de compra (ou ordem de presta'#231#227'o de servi'#231'o) do clien' +
            'te:'
        end
        object Label8: TLabel
          Left = 80
          Top = 8
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label3: TLabel
          Left = 564
          Top = 164
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label14: TLabel
          Left = 672
          Top = 48
          Width = 134
          Height = 13
          Caption = 'Local da entrega (Entidade):'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 24
          Width = 69
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCliente: TdmkEditCB
          Left = 368
          Top = 24
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cliente'
          UpdCampo = 'Cliente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 420
          Top = 24
          Width = 273
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsClientes
          TabOrder = 4
          dmkEditCB = EdCliente
          QryCampo = 'Cliente'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object TPDataF: TdmkEditDateTimePicker
          Left = 8
          Top = 64
          Width = 112
          Height = 21
          Date = 38579.000000000000000000
          Time = 0.624225613399175900
          TabOrder = 7
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataF'
          UpdCampo = 'DataF'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdVendedor: TdmkEditCB
          Left = 124
          Top = 64
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Vendedor'
          UpdCampo = 'Vendedor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBVendedor
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBVendedor: TdmkDBLookupComboBox
          Left = 176
          Top = 64
          Width = 225
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsVendedores
          TabOrder = 9
          dmkEditCB = EdVendedor
          QryCampo = 'Vendedor'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object MeObz: TdmkMemo
          Left = 8
          Top = 108
          Width = 989
          Height = 51
          MaxLength = 254
          TabOrder = 12
          QryCampo = 'Obz'
          UpdCampo = 'Obz'
          UpdType = utYes
        end
        object EdTransp: TdmkEditCB
          Left = 696
          Top = 24
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Transp'
          UpdCampo = 'Transp'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBTransp
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBTransp: TdmkDBLookupComboBox
          Left = 751
          Top = 24
          Width = 246
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsTransp
          TabOrder = 6
          dmkEditCB = EdTransp
          QryCampo = 'Transp'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBCondicaoPG: TdmkDBLookupComboBox
          Left = 460
          Top = 64
          Width = 209
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsPediPrzCab
          TabOrder = 11
          dmkEditCB = EdCondicaoPG
          QryCampo = 'CondicaoPg'
          UpdType = utNil
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCondicaoPG: TdmkEditCB
          Left = 404
          Top = 64
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CondicaoPg'
          UpdCampo = 'CondicaoPg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCondicaoPG
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdPedidCli: TdmkEdit
          Left = 8
          Top = 180
          Width = 552
          Height = 21
          TabOrder = 13
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'PedidCli'
          UpdCampo = 'PedidCli'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEmpresa: TdmkEditCB
          Left = 80
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Empresa'
          UpdCampo = 'Empresa'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 136
          Top = 24
          Width = 229
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 2
          dmkEditCB = EdEmpresa
          QryCampo = 'Empresa'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNome: TdmkEdit
          Left = 564
          Top = 180
          Width = 433
          Height = 21
          TabOrder = 14
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEntregaEnti: TdmkEditCB
          Left = 672
          Top = 64
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 15
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'EntregaEnti'
          UpdCampo = 'EntregaEnti'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEntregaEnti
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEntregaEnti: TdmkDBLookupComboBox
          Left = 724
          Top = 64
          Width = 273
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsClientes
          TabOrder = 16
          dmkEditCB = EdEntregaEnti
          QryCampo = 'EntregaEnti'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1022
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 531
      Width = 1022
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 47
        Align = alLeft
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 204
        Top = 15
        Width = 816
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 683
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1022
      Height = 137
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 296
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdNome
      end
      object Label6: TLabel
        Left = 504
        Top = 44
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit02
      end
      object Label18: TLabel
        Left = 400
        Top = 44
        Width = 27
        Height = 13
        Caption = 'Valor:'
        FocusControl = DBEdit2
      end
      object Label20: TLabel
        Left = 832
        Top = 4
        Width = 69
        Height = 13
        Caption = 'Transportador:'
        FocusControl = DBEdit3
      end
      object Label21: TLabel
        Left = 572
        Top = 44
        Width = 119
        Height = 13
        Caption = 'Condi'#231#227'o de pagamento:'
        FocusControl = DBEdit4
      end
      object Label24: TLabel
        Left = 840
        Top = 44
        Width = 85
        Height = 13
        Caption = 'Pedido do cliente:'
        FocusControl = DBEdit5
      end
      object Label4: TLabel
        Left = 72
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit6
      end
      object Label7: TLabel
        Left = 4
        Top = 44
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        FocusControl = DBEdit9
      end
      object Label11: TLabel
        Left = 80
        Top = 44
        Width = 43
        Height = 13
        Caption = 'Peso Kg:'
        FocusControl = DBEdit10
      end
      object Label12: TLabel
        Left = 184
        Top = 44
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        FocusControl = DBEdit11
      end
      object Label13: TLabel
        Left = 288
        Top = 44
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        FocusControl = DBEdit12
      end
      object Label15: TLabel
        Left = 612
        Top = 4
        Width = 134
        Height = 13
        Caption = 'Local da entrega (Entidade):'
        FocusControl = DBEdit13
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 65
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSPedCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 352
        Top = 20
        Width = 257
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMECLIENTE'
        DataSource = DsVSPedCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit02: TDBEdit
        Left = 504
        Top = 60
        Width = 65
        Height = 21
        Color = clWhite
        DataField = 'DataF'
        DataSource = DsVSPedCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 400
        Top = 60
        Width = 100
        Height = 21
        DataField = 'Valor'
        DataSource = DsVSPedCab
        TabOrder = 3
      end
      object DBMemo2: TDBMemo
        Left = 0
        Top = 86
        Width = 1022
        Height = 51
        Align = alBottom
        DataField = 'Obz'
        DataSource = DsVSPedCab
        TabOrder = 4
      end
      object DBEdit3: TDBEdit
        Left = 888
        Top = 20
        Width = 129
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMETRANSP'
        DataSource = DsVSPedCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
      end
      object DBEdit4: TDBEdit
        Left = 572
        Top = 60
        Width = 265
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NO_CONDICAOPG'
        DataSource = DsVSPedCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
      end
      object DBEdit5: TDBEdit
        Left = 840
        Top = 60
        Width = 176
        Height = 21
        DataField = 'PedidCli'
        DataSource = DsVSPedCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
      end
      object DBEdit6: TDBEdit
        Left = 72
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSPedCab
        TabOrder = 8
      end
      object DBEdit7: TDBEdit
        Left = 296
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsVSPedCab
        TabOrder = 9
      end
      object DBEdit8: TDBEdit
        Left = 832
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Transp'
        DataSource = DsVSPedCab
        TabOrder = 10
      end
      object DBEdit9: TDBEdit
        Left = 4
        Top = 60
        Width = 72
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSPedCab
        TabOrder = 11
      end
      object DBEdit10: TDBEdit
        Left = 80
        Top = 60
        Width = 100
        Height = 21
        DataField = 'PesoKg'
        DataSource = DsVSPedCab
        TabOrder = 12
      end
      object DBEdit11: TDBEdit
        Left = 184
        Top = 60
        Width = 100
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVSPedCab
        TabOrder = 13
      end
      object DBEdit12: TDBEdit
        Left = 288
        Top = 60
        Width = 108
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVSPedCab
        TabOrder = 14
      end
      object DBEdit1: TDBEdit
        Left = 128
        Top = 20
        Width = 165
        Height = 21
        DataField = 'NOMEEMP'
        DataSource = DsVSPedCab
        TabOrder = 15
      end
      object DBEdit13: TDBEdit
        Left = 612
        Top = 20
        Width = 56
        Height = 21
        DataField = 'EntregaEnti'
        DataSource = DsVSPedCab
        TabOrder = 16
      end
      object DBEdit14: TDBEdit
        Left = 668
        Top = 20
        Width = 161
        Height = 21
        DataField = 'NOMEEntregaEnti'
        DataSource = DsVSPedCab
        TabOrder = 17
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 426
      Width = 1022
      Height = 105
      Align = alBottom
      Caption = '     Informa'#231#245'es que ser'#227'o impressas na ordem de servi'#231'o: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object DBMemo1: TDBMemo
        Left = 2
        Top = 15
        Width = 609
        Height = 88
        Align = alLeft
        DataField = 'Observ'
        DataSource = DsVSPedIts
        TabOrder = 0
      end
    end
    object DBGVsPedIts: TDBGrid
      Left = 13
      Top = 165
      Width = 832
      Height = 64
      DataSource = DsVSPedIts
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'OS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Status'
          Title.Caption = 'Status'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VSMovIts'
          Title.Caption = 'IME-I'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Artigo'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Peso Kg'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaP2'
          Title.Caption = #193'rea ft'#178
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VdaPecas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VdaPesoKg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VdaAreaM2'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VdaAreaP2'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoTipo'
          Title.Caption = 'Tipo Pre'#231'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TipoSaldo_TXT'
          Title.Caption = 'Ger.sdo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Saldo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Simbolo'
          Title.Caption = 'Moeda'
          Width = 37
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoVal'
          Title.Caption = 'Pre'#231'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Title.Caption = 'Valor T.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Title.Caption = 'Desconto $'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SubTo'
          Title.Caption = 'Sub Total $'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Entrega'
          Title.Caption = 'Entregar em'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFLUXO'
          Title.Caption = 'Fluxo'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoPQ'
          Title.Caption = 'Custo PQ'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoWB'
          Title.Caption = 'Custo couro'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ReceiRecu'
          Title.Caption = 'Receita de recurtimento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ReceiAcab'
          Title.Caption = 'Receita de acabamento'
          Width = 184
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ReceiRefu'
          Title.Caption = 'Receita de refulonagem'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Descri'#231#227'o'
          Width = 232
          Visible = True
        end>
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 342
      Width = 1022
      Height = 84
      Align = alBottom
      DataSource = DsEmitCus
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Pesagem'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID Custo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataEmis'
          Title.Caption = 'Emiss'#227'o'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESETOR'
          Title.Caption = 'Setor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Numero'
          Title.Caption = 'Receita'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME'
          Title.Caption = 'Descri'#231#227'o da receita'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Title.Caption = 'Peso kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Custo'
          Title.Caption = 'Custo BRL'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PercTotCus'
          Title.Caption = '% da receita'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Setor'
          Title.Caption = 'C'#243'd. setor'
          Visible = True
        end>
    end
    object Panel7: TPanel
      Left = 0
      Top = 322
      Width = 1022
      Height = 20
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'Receitas utilizadas nos setores de recurtimento e acabamento'
      TabOrder = 5
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 248
      Width = 1022
      Height = 74
      Align = alBottom
      DataSource = DsWBMovIts
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Pallet'
          Title.Caption = 'ID Pallet'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PALLET'
          Title.Caption = 'Pallet'
          Width = 95
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea m'#178
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorT'
          Title.Caption = 'Valor total'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'IME-I'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LnkNivXtr1'
          Title.Caption = 'Pesagem (SP)'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LnkNivXtr2'
          Title.Caption = 'Item custo SP'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Observ'
          Title.Caption = 'Observa'#231#245'es'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaP2'
          Title.Caption = #193'rea ft'#178
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoKg'
          Title.Caption = 'Peso kg'
          Width = 72
          Visible = True
        end>
    end
    object Panel6: TPanel
      Left = 0
      Top = 228
      Width = 1022
      Height = 20
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'Mat'#233'ria-prima utilizada no setor de recurtimento'
      TabOrder = 7
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1022
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 974
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 758
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 205
        Height = 32
        Caption = 'Pedido de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 205
        Height = 32
        Caption = 'Pedido de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 205
        Height = 32
        Caption = 'Pedido de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1022
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1018
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 144
    Top = 44
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 664
    Top = 584
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 532
    Top = 584
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrMPs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM artigosgrupos'
      'ORDER BY Nome'
      '')
    Left = 281
    Top = 29
    object QrMPsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMPsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMPs: TDataSource
    DataSet = QrMPs
    Left = 281
    Top = 73
  end
  object QrFluxosIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ope.Nome NOMEOPERACAO, fli.*'
      'FROM fluxosits fli'
      'LEFT JOIN operacoes ope ON ope.Codigo=fli.Operacao'
      'WHERE fli.Codigo=:P0'
      'ORDER BY fli.Ordem, fli.Controle DESC')
    Left = 456
    Top = 65529
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFluxosItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFluxosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFluxosItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFluxosItsOperacao: TIntegerField
      FieldName = 'Operacao'
    end
    object QrFluxosItsAcao1: TWideStringField
      FieldName = 'Acao1'
      Size = 30
    end
    object QrFluxosItsAcao2: TWideStringField
      FieldName = 'Acao2'
      Size = 30
    end
    object QrFluxosItsAcao3: TWideStringField
      FieldName = 'Acao3'
      Size = 30
    end
    object QrFluxosItsAcao4: TWideStringField
      FieldName = 'Acao4'
      Size = 30
    end
    object QrFluxosItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFluxosItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFluxosItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFluxosItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFluxosItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFluxosItsNOMEOPERACAO: TWideStringField
      FieldName = 'NOMEOPERACAO'
      Size = 30
    end
    object QrFluxosItsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object DsFluxosIts: TDataSource
    DataSet = QrFluxosIts
    Left = 456
    Top = 37
  end
  object frxDsFluxosIts: TfrxDBDataset
    UserName = 'frxDsFluxosIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Ordem=Ordem'
      'Operacao=Operacao'
      'Acao1=Acao1'
      'Acao2=Acao2'
      'Acao3=Acao3'
      'Acao4=Acao4'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'NOMEOPERACAO=NOMEOPERACAO'
      'SEQ=SEQ')
    DataSet = QrFluxosIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 456
    Top = 80
  end
  object QrVSPedCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSPedCabBeforeOpen
    AfterScroll = QrVSPedCabAfterScroll
    SQL.Strings = (
      'SELECT'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMEVENDEDOR,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,'
      'IF(tr.Tipo=0, tr.RazaoSocial, tr.Nome) NOMETRANSP,'
      'pc.Nome NO_CONDICAOPG,'
      'vpc.*'
      'FROM vspedcab vpc'
      'LEFT JOIN entidades emp ON emp.Codigo=vpc.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=vpc.Cliente'
      'LEFT JOIN entidades ve ON ve.Codigo=vpc.Vendedor'
      'LEFT JOIN entidades tr ON tr.Codigo=vpc.Transp'
      'LEFT JOIN pediprzcab pc ON pc.Codigo=vpc.CondicaoPg'
      'WHERE vpc.Codigo > 0'
      '')
    Left = 524
    Top = 65529
    object QrVSPedCabNOMEVENDEDOR: TWideStringField
      FieldName = 'NOMEVENDEDOR'
      Size = 100
    end
    object QrVSPedCabNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrVSPedCabNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrVSPedCabNO_CONDICAOPG: TWideStringField
      FieldName = 'NO_CONDICAOPG'
      Size = 50
    end
    object QrVSPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPedCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPedCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSPedCabVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrVSPedCabDataF: TDateField
      FieldName = 'DataF'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrVSPedCabValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSPedCabObz: TWideStringField
      FieldName = 'Obz'
      Size = 255
    end
    object QrVSPedCabTransp: TIntegerField
      FieldName = 'Transp'
    end
    object QrVSPedCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrVSPedCabPedidCli: TWideStringField
      FieldName = 'PedidCli'
      Size = 60
    end
    object QrVSPedCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSPedCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPedCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPedCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPedCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPedCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPedCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPedCabPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSPedCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrVSPedCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSPedCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSPedCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPedCabNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrVSPedCabEntregaEnti: TIntegerField
      FieldName = 'EntregaEnti'
    end
    object QrVSPedCabNOMEEntregaEnti: TWideStringField
      FieldName = 'NOMEEntregaEnti'
      Size = 120
    end
  end
  object DsVSPedCab: TDataSource
    DataSet = QrVSPedCab
    Left = 524
    Top = 37
  end
  object frxDsVSPedCab: TfrxDBDataset
    UserName = 'frxDsVSPedCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEVENDEDOR=NOMEVENDEDOR'
      'NOMECLIENTE=NOMECLIENTE'
      'NOMETRANSP=NOMETRANSP'
      'NO_CONDICAOPG=NO_CONDICAOPG'
      'Codigo=Codigo'
      'Empresa=Empresa'
      'Cliente=Cliente'
      'Vendedor=Vendedor'
      'DataF=DataF'
      'Qtde=Qtde'
      'Valor=Valor'
      'Obz=Obz'
      'Transp=Transp'
      'CondicaoPg=CondicaoPg'
      'PedidCli=PedidCli'
      'Nome=Nome'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrVSPedCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 525
    Top = 81
  end
  object QrVSPedIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPedItsBeforeClose
    AfterScroll = QrVSPedItsAfterScroll
    OnCalcFields = QrVSPedItsCalcFields
    SQL.Strings = (
      'SELECT vmi.*,'
      'flu.Nome NOMEFLUXO, '
      'flu.Nome NO_Fluxo, fo1.Nome NO_ReceiRecu, '
      'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,  '
      'CONCAT(gg1.Nome, '
      
        '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.N' +
        'ome)), '
      '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR,'
      'CASE vmi.PrecoTipo'
      '  WHEN 1 THEN "Pe'#231'as"'
      '  WHEN 2 THEN "Peso kg"'
      '  WHEN 3 THEN "'#193'rea m2"'
      '  WHEN 4 THEN "'#193'rea ft2"'
      '  ELSE "? ? ?" '
      'END TipoSaldo_TXT, '
      'IF(Status <> 3, 0.000, '
      '  CASE vmi.PrecoTipo'
      '    WHEN 1 THEN vmi.Pecas-vmi.VdaPecas'
      '    WHEN 2 THEN vmi.PesoKg-vmi.VdaPesoKg'
      '    WHEN 3 THEN vmi.AreaM2-vmi.VdaAreaM2'
      '    WHEN 4 THEN vmi.AreaP2-vmi.AreaP2'
      '    ELSE 0.00 '
      '  END'
      ') Saldo, cmd.Sigla, cmd.Simbolo '
      'FROM vspedits vmi '
      'LEFT JOIN fluxos     flu ON flu.Codigo=vmi.Fluxo '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN formulas   fo1 ON fo1.Numero=vmi.ReceiRecu '
      'LEFT JOIN formulas   fo2 ON fo2.Numero=vmi.ReceiRefu '
      'LEFT JOIN tintascab  ti1 ON ti1.Numero=vmi.ReceiAcab '
      'LEFT JOIN cambiomda  cmd ON cmd.Codigo=vmi.PrecoMoeda')
    Left = 580
    Top = 65529
    object QrVSPedItsSubTo: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SubTo'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
    object QrVSPedItsPRONTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PRONTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrVSPedItsNOMETIPOPROD: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOPROD'
      Size = 15
      Calculated = True
    end
    object QrVSPedItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPedItsVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSPedItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrVSPedItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPedItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPedItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPedItsPrecoTipo: TSmallintField
      FieldName = 'PrecoTipo'
    end
    object QrVSPedItsPrecoMoeda: TIntegerField
      FieldName = 'PrecoMoeda'
    end
    object QrVSPedItsPrecoVal: TFloatField
      FieldName = 'PrecoVal'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPedItsPercDesco: TFloatField
      FieldName = 'PercDesco'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrVSPedItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPedItsTexto: TWideStringField
      FieldName = 'Texto'
      Size = 50
    end
    object QrVSPedItsEntrega: TDateField
      FieldName = 'Entrega'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrVSPedItsPronto: TDateField
      FieldName = 'Pronto'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrVSPedItsStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPedItsFluxo: TIntegerField
      FieldName = 'Fluxo'
    end
    object QrVSPedItsObserv: TWideMemoField
      FieldName = 'Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrVSPedItsTipoProd: TSmallintField
      FieldName = 'TipoProd'
    end
    object QrVSPedItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSPedItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPedItsReceiRecu: TIntegerField
      FieldName = 'ReceiRecu'
    end
    object QrVSPedItsReceiRefu: TIntegerField
      FieldName = 'ReceiRefu'
    end
    object QrVSPedItsReceiAcab: TIntegerField
      FieldName = 'ReceiAcab'
    end
    object QrVSPedItsCustoWB: TFloatField
      FieldName = 'CustoWB'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPedItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPedItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPedItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPedItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPedItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPedItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPedItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPedItsNOMEFLUXO: TWideStringField
      FieldName = 'NOMEFLUXO'
      Size = 100
    end
    object QrVSPedItsNO_Fluxo: TWideStringField
      FieldName = 'NO_Fluxo'
      Size = 100
    end
    object QrVSPedItsNO_ReceiRecu: TWideStringField
      FieldName = 'NO_ReceiRecu'
      Size = 30
    end
    object QrVSPedItsNO_ReceiRefu: TWideStringField
      FieldName = 'NO_ReceiRefu'
      Size = 30
    end
    object QrVSPedItsNO_ReceiAcab: TWideStringField
      FieldName = 'NO_ReceiAcab'
      Size = 100
    end
    object QrVSPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPedItsPerctrib: TFloatField
      FieldName = 'Perctrib'
    end
    object QrVSPedItsLibPecas: TFloatField
      FieldName = 'LibPecas'
      Required = True
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrVSPedItsLibPesoKg: TFloatField
      FieldName = 'LibPesoKg'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPedItsLibAreaM2: TFloatField
      FieldName = 'LibAreaM2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPedItsFinPecas: TFloatField
      FieldName = 'FinPecas'
      Required = True
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrVSPedItsLibAreaP2: TFloatField
      FieldName = 'LibAreaP2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPedItsFinPesoKg: TFloatField
      FieldName = 'FinPesoKg'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPedItsFinAreaM2: TFloatField
      FieldName = 'FinAreaM2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPedItsFinAreaP2: TFloatField
      FieldName = 'FinAreaP2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPedItsVdaPecas: TFloatField
      FieldName = 'VdaPecas'
      Required = True
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrVSPedItsVdaPesoKg: TFloatField
      FieldName = 'VdaPesoKg'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPedItsVdaAreaM2: TFloatField
      FieldName = 'VdaAreaM2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPedItsVdaAreaP2: TFloatField
      FieldName = 'VdaAreaP2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPedItsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrVSPedItsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrVSPedItsTipoSaldo_TXT: TWideStringField
      FieldName = 'TipoSaldo_TXT'
      Required = True
      Size = 8
    end
    object QrVSPedItsSaldo: TFloatField
      FieldName = 'Saldo'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPedItsSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 5
    end
    object QrVSPedItsSimbolo: TWideStringField
      FieldName = 'Simbolo'
      Size = 15
    end
    object QrVSPedItsNO_Status: TWideStringField
      FieldName = 'NO_Status'
      Size = 30
    end
    object QrVSPedItsNaoFinaliza: TSmallintField
      FieldName = 'NaoFinaliza'
    end
  end
  object DsVSPedIts: TDataSource
    DataSet = QrVSPedIts
    Left = 580
    Top = 41
  end
  object frxDsVSPedIts: TfrxDBDataset
    UserName = 'frxDsVSPedIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SubTo=SubTo'
      'PRONTO_TXT=PRONTO_TXT'
      'NOMETIPOPROD=NOMETIPOPROD'
      'Codigo=Codigo'
      'Controle=Controle'
      'VSWetEnd=VSWetEnd'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'PrecoTipo=PrecoTipo'
      'PrecoMoeda=PrecoMoeda'
      'PrecoVal=PrecoVal'
      'Desco=Desco'
      'Valor=Valor'
      'Texto=Texto'
      'Entrega=Entrega'
      'Pronto=Pronto'
      'Status=Status'
      'Fluxo=Fluxo'
      'Observ=Observ'
      'Descricao=Descricao'
      'Complementacao=Complementacao'
      'TipoProd=TipoProd'
      'CustoPQ=CustoPQ'
      'GraGruX=GraGruX'
      'ReceiRecu=ReceiRecu'
      'ReceiRefu=ReceiRefu'
      'ReceiAcab=ReceiAcab'
      'TxtMPs=TxtMPs'
      'CustoWB=CustoWB'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NOMEFLUXO=NOMEFLUXO'
      'NO_Fluxo=NO_Fluxo'
      'NO_ReceiRecu=NO_ReceiRecu'
      'NO_ReceiRefu=NO_ReceiRefu'
      'NO_ReceiAcab=NO_ReceiAcab'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR')
    DataSet = QrVSPedIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 581
    Top = 81
  end
  object QrPesqV: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mpvits'
      'WHERE Codigo<>0'
      'AND Pedido=:P0')
    Left = 641
    Top = 65529
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrVendedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece6="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 705
    Top = 65529
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 705
    Top = 37
  end
  object frxOS_3: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39496.531362662000000000
    ReportOptions.LastChange = 39496.531362662000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  Page2.Visible := <VAR_OSImpInfWB>;                            ' +
        '                     '
      'end.')
    OnGetValue = frxOS_2GetValue
    Left = 645
    Top = 81
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
      end
      item
        DataSet = frxDsVSPedCab
        DataSetName = 'frxDsVSPedCab'
      end
      item
        DataSet = frxDsVSPedIts
        DataSetName = 'frxDsVSPedIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 449.764040710000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Width = 366.614410000000000000
          Height = 30.236240000000000000
          DataSet = frxDsVSPedIts
          DataSetName = 'frxDsVSPedIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            
              'OS: [frxDsVSPedIts."Controle"]    [frxDsVSPedIts."NOMETIPOPROD"]' +
              ' ')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 79.370130000000000000
          Width = 64.251968500000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 253.228510000000000000
          Top = 79.370130000000000000
          Width = 427.086724020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo / espessura / cor:')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 79.370130000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 98.267780000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsVSPedIts."Pe' +
              'cas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 98.267780000000000000
          Width = 64.251968500000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsVSPedIts."Pe' +
              'soKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 253.228510000000000000
          Top = 98.267780000000000000
          Width = 427.086724020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 98.267780000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.##0.00;-#,###,###.##0.00; '#39',<frxDsVSPedI' +
              'ts."AreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 117.165430000000000000
          Width = 661.417750000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedIts."Observ"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 188.976500000000000000
          Width = 113.385900000000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data da'
            'entrega: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 188.976500000000000000
          Width = 566.929499999999900000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPedIts."Entrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 170.078850000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 170.078850000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '  [FormatFloat('#39'000'#39',<frxDsVSPedIts."Codigo">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236239999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 30.236239999999990000
          Width = 627.401726140000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLIENTE'
          DataSet = frxDsVSPedCab
          DataSetName = 'frxDsVSPedCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedCab."NOMECLIENTE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 117.165430000000000000
          Width = 18.897650000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          Rotation = 90
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Top = 170.078850000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Dta pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 170.078850000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedCab."DataF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 49.133890000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Ordem de')
          ParentFont = False
          WordWrap = False
        end
        object frxDsFluxosCodigo: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 170.078850000000000000
          Width = 351.496290000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDsFluxos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[FormatFloat('#39'000'#39', <frxDsVSPedIts."Fluxo">)] - [frxDsVSPedIts."' +
              'NOMEFLUXO"]'
            '')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 170.078850000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fluxo:')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 226.771800000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita recurtimento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPedIts."ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 226.771800000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita acabamento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPedIts."ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 226.771800000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedIts."NO_ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 226.771800000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedIts."NO_ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Top = 321.260050000000000000
          Width = 680.314960629921300000
          Height = 45.354330710000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 283.464750000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 283.464750000000000000
          Width = 196.535560000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 283.464750000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo136: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 283.464750000000000000
          Width = 177.637910000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          AllowVectorExport = True
          Top = 283.464750000000000000
          Width = 215.433210000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Entrada Wet Blue')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Top = 404.409710000000000000
          Width = 680.314960629921300000
          Height = 45.354330710000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 366.614410000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 366.614410000000000000
          Width = 45.354360000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 366.614410000000000000
          Width = 177.637910000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Top = 366.614410000000000000
          Width = 215.433210000000000000
          Height = 37.795275590000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Entrada no Acabamento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Top = 49.133890000000010000
          Width = 98.267780000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Transportador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 49.133890000000010000
          Width = 347.716506140000000000
          Height = 15.118110236220470000
          DataSet = frxDsVSPedCab
          DataSetName = 'frxDsVSPedCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedCab."NOMETRANSP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 98.267780000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Condi'#231#227'o pagto.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 64.252010000000000000
          Width = 347.716760000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedCab."NO_CONDICAOPG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 64.252010000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Compra:')
          ParentFont = False
          WordWrap = False
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 49.133890000000000000
          Width = 177.637910000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedCab."PedidCli"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 313.700941180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 79.370130000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 98.267780000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.##0.00;-#,###,###.##0.00; '#39',<frxDsVSPedI' +
              'ts."AreaP2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 54.803174020000000000
        Top = 566.929500000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFluxosIts
        DataSetName = 'frxDsFluxosIts'
        RowCount = 0
        object frxDsFluxosItsOrdem: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 17.007874020000000000
          DataField = 'Ordem'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Ordem"]')
          ParentFont = False
        end
        object frxDsFluxosItsNOMEOPERACAO: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 219.212740000000000000
          Height = 17.007874020000000000
          DataField = 'NOMEOPERACAO'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."NOMEOPERACAO"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao1: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao1'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao1"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao2: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao2'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao2"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao3: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao3'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao3"]')
          ParentFont = False
        end
        object frxDsFluxosItsAcao4: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826771650000000000
          Height = 17.007874020000000000
          DataField = 'Acao4'
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFluxosIts."Acao4"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 17.007874020000030000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data entrada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 17.007874020000030000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 17.007874020000030000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora entrada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 17.007874020000030000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 17.007874020000030000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 17.007874020000030000
          Width = 257.007874020000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 34.015770000000310000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data t'#233'rmino:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 34.015770000000310000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 34.015770000000310000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora t'#233'rmino:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 34.015770000000310000
          Width = 98.267780000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 34.015770000000310000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 34.015770000000310000
          Width = 257.007874020000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 491.338900000000000000
        Width = 680.315400000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Opera'#231#227'o / Processo')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 1')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 2')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 3')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsFluxosIts
          DataSetName = 'frxDsFluxosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle 4')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle2: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 260.787555350000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 238.110390000000000000
          Width = 139.842585590000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e Nome do Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 238.110390000000000000
          Width = 79.370098270000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 238.110390000000000000
          Width = 71.811038270000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 238.110390000000000000
          Width = 52.913400470000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 196.535560000000000000
          Width = 75.590556060000000000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Top = 238.110390000000000000
          Width = 158.740223390000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e Nome do fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Width = 328.818865910000000000
          Height = 18.897650000000000000
          DataField = 'Em'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em: [Date] [Time]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Top = 52.913420000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 52.913420000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 52.913420000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 52.913420000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 52.913420000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Espes.')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 52.913420000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 52.913420000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            ''
            ''
            ''
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39',<frxDsVSPedIts."Pe' +
              'cas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 71.811070000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            ''
            ''
            ''
            '[frxDsVSPedIts."NOMEUNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 71.811070000000000000
          Width = 257.007874020000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedIts."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 71.811070000000000000
          Width = 139.842519690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedIts."CorTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 71.811070000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPedIts."EspesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 71.811070000000000000
          Width = 71.811040710000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedIts."Classe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 71.811070000000000000
          Width = 68.031540000000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSPedIts."M2Pedido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 90.708720000000000000
          Width = 661.417750000000000000
          Height = 52.913385830000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -29
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'FICHA DE BAIXA DO ESTOQUE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Top = 162.519790000000000000
          Width = 113.385900000000000000
          Height = 34.015770000000010000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data da'
            'entrega: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 162.519790000000000000
          Width = 566.929499999999900000
          Height = 34.015770000000010000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPedIts."Entrega"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 143.622140000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 143.622140000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '  [FormatFloat('#39'000'#39',<frxDsVSPedIts."Pedido">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236239999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cliente: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 30.236239999999990000
          Width = 393.070866140000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLIENTE'
          DataSet = frxDsVSPedCab
          DataSetName = 'frxDsVSPedCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedCab."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708720000000000000
          Width = 18.897650000000000000
          Height = 52.913385830000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          Rotation = 90
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Top = 143.622140000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Dta pedido: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 143.622140000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedCab."DataF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 30.236239999999990000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 30.236239999999990000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedIts."NOMEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 143.622140000000000000
          Width = 351.496290000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDsFluxos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[FormatFloat('#39'000'#39', <frxDsVSPedIts."Fluxo">)] - [frxDsVSPedIts."' +
              'NOMEFLUXO"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 143.622140000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fluxo:')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Top = 196.535560000000000000
          Width = 45.354176930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 196.535560000000000000
          Width = 445.984356930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedIts."GraGruX"] - [frxDsVSPedIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Top = 215.433210000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita recurtimento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 215.433210000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPedIts."ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 215.433210000000000000
          Width = 105.826656930000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Receita acabamento:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 215.433210000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPedIts."ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 215.433210000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedIts."NO_ReceiRecu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 215.433210000000000000
          Width = 181.417322830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSPedIts."NO_ReceiAcab"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 351.496290000000000000
          Height = 30.236240000000000000
          DataSet = frxDsVSPedIts
          DataSetName = 'frxDsVSPedIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            
              'OS: [frxDsVSPedIts."Controle"]    [frxDsVSPedIts."NOMETIPOPROD"]' +
              ' ')
          ParentFont = False
          WordWrap = False
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 196.535560000000000000
          Width = 113.385856060000000000
          Height = 18.897632910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 238.110390000000000000
          Width = 177.637866060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID e nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692910000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        RowCount = 10
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Width = 60.472455590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Width = 79.370098270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Width = 71.811038270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 52.913400470000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Width = 117.165381180000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913388270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 105.826803390000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Width = 56.692925590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 83.149611180000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrPediPrzCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 780
    Top = 65532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 780
    Top = 40
  end
  object QrTransp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 929
    Top = 273
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransp: TDataSource
    DataSet = QrTransp
    Left = 929
    Top = 317
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFornecedorCalcFields
    SQL.Strings = (
      'SELECT '
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOME,'
      'CASE WHEN fo.Tipo=0 THEN fo.ERua'
      'ELSE fo.PRua END RUA,'
      'CASE WHEN fo.Tipo=0 THEN fo.ENumero'
      'ELSE fo.PNumero END NUMERO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECompl'
      'ELSE fo.PCompl END COMPL,'
      'CASE WHEN fo.Tipo=0 THEN fo.EBairro'
      'ELSE fo.PBairro END BAIRRO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECidade'
      'ELSE fo.PCidade END CIDADE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EPais'
      'ELSE fo.PPais END PAIS,'
      'CASE WHEN fo.Tipo=0 THEN fo.ETe1'
      'ELSE fo.PTe1 END TELEFONE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EFax'
      'ELSE fo.PPais END FAX,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECel'
      'ELSE fo.PCel END Celular,'
      'CASE WHEN fo.Tipo=0 THEN fo.CNPJ'
      'ELSE fo.CPF END CNPJ,'
      'CASE WHEN fo.Tipo=0 THEN fo.IE'
      'ELSE fo.RG END IE,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECEP'
      'ELSE fo.PCEP END CEP,'
      'CASE WHEN fo.Tipo=0 THEN fo.EContato'
      'ELSE fo.PContato END Contato,'
      ''
      'CASE WHEN fo.Tipo=0 THEN fo.EUF'
      'ELSE fo.PUF END UF,'
      'CASE WHEN fo.Tipo=0 THEN uf1.Nome'
      'ELSE uf2.Nome END NOMEUF,'
      'EEMail, PEmail,'
      ''
      'CASE WHEN fo.Tipo=0 THEN ll1.Nome'
      'ELSE ll2.Nome END LOGRAD'
      ''
      'FROM entidades fo'
      'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf'
      'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf'
      'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd'
      'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd'
      'WHERE fo.Codigo=:P0')
    Left = 768
    Top = 140
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFornecedorFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrFornecedorCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Calculated = True
    end
    object QrFornecedorCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Calculated = True
    end
    object QrFornecedorTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrFornecedorCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrFornecedorNOME: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME'
      Size = 100
    end
    object QrFornecedorRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 30
    end
    object QrFornecedorNUMERO: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'NUMERO'
    end
    object QrFornecedorCOMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 30
    end
    object QrFornecedorBAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrFornecedorCIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrFornecedorPAIS: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'PAIS'
    end
    object QrFornecedorTELEFONE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TELEFONE'
    end
    object QrFornecedorFAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrFornecedorCelular: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Celular'
    end
    object QrFornecedorCNPJ: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrFornecedorIE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'IE'
    end
    object QrFornecedorCEP: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'CEP'
    end
    object QrFornecedorContato: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Contato'
      Size = 60
    end
    object QrFornecedorUF: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'UF'
    end
    object QrFornecedorNOMEUF: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrFornecedorEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrFornecedorPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrFornecedorNUMEROTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMEROTXT'
      Size = 40
      Calculated = True
    end
    object QrFornecedorENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrFornecedorLOGRAD: TWideStringField
      FieldName = 'LOGRAD'
      Size = 10
    end
  end
  object QrTransportador: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTransportadorCalcFields
    SQL.Strings = (
      'SELECT '
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOME,'
      'CASE WHEN fo.Tipo=0 THEN fo.ERua'
      'ELSE fo.PRua END RUA,'
      'CASE WHEN fo.Tipo=0 THEN fo.ENumero'
      'ELSE fo.PNumero END NUMERO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECompl'
      'ELSE fo.PCompl END COMPL,'
      'CASE WHEN fo.Tipo=0 THEN fo.EBairro'
      'ELSE fo.PBairro END BAIRRO,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECidade'
      'ELSE fo.PCidade END CIDADE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EPais'
      'ELSE fo.PPais END PAIS,'
      'CASE WHEN fo.Tipo=0 THEN fo.ETe1'
      'ELSE fo.PTe1 END TELEFONE,'
      'CASE WHEN fo.Tipo=0 THEN fo.EFax'
      'ELSE fo.PPais END FAX,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECel'
      'ELSE fo.PCel END Celular,'
      'CASE WHEN fo.Tipo=0 THEN fo.CNPJ'
      'ELSE fo.CPF END CNPJ,'
      'CASE WHEN fo.Tipo=0 THEN fo.IE'
      'ELSE fo.RG END IE,'
      'CASE WHEN fo.Tipo=0 THEN fo.ECEP'
      'ELSE fo.PCEP END CEP,'
      'CASE WHEN fo.Tipo=0 THEN fo.EContato'
      'ELSE fo.PContato END Contato,'
      ''
      'CASE WHEN fo.Tipo=0 THEN fo.EUF'
      'ELSE fo.PUF END UF,'
      'CASE WHEN fo.Tipo=0 THEN uf1.Nome'
      'ELSE uf2.Nome END NOMEUF,'
      'EEMail, PEmail,'
      ''
      'CASE WHEN fo.Tipo=0 THEN ll1.Nome'
      'ELSE ll2.Nome END LOGRAD'
      ''
      'FROM entidades fo'
      'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf'
      'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf'
      'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd'
      'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd'
      'WHERE fo.Codigo=:P0')
    Left = 768
    Top = 228
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTransportadorTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrTransportadorCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Calculated = True
    end
    object QrTransportadorCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Calculated = True
    end
    object QrTransportadorFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrTransportadorCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrTransportadorNOME: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME'
      Size = 100
    end
    object QrTransportadorRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 30
    end
    object QrTransportadorNUMERO: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'NUMERO'
    end
    object QrTransportadorCOMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 30
    end
    object QrTransportadorBAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrTransportadorCIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrTransportadorPAIS: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'PAIS'
    end
    object QrTransportadorTELEFONE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TELEFONE'
    end
    object QrTransportadorFAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrTransportadorCelular: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Celular'
    end
    object QrTransportadorCNPJ: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTransportadorIE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'IE'
    end
    object QrTransportadorCEP: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'CEP'
    end
    object QrTransportadorContato: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Contato'
      Size = 60
    end
    object QrTransportadorUF: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'UF'
    end
    object QrTransportadorNOMEUF: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrTransportadorNUMEROTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMEROTXT'
      Size = 40
      Calculated = True
    end
    object QrTransportadorENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrTransportadorEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrTransportadorPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrTransportadorLOGRAD: TWideStringField
      FieldName = 'LOGRAD'
      Size = 10
    end
  end
  object QrTotais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(M2Pedido) Qtde, SUM(ValorPed) Valor'
      'FROM mpvits'
      'WHERE Pedido=:P0')
    Left = 769
    Top = 273
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotaisQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrTotaisValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE (Cliente1="V" OR Cliente2="V")'
      'ORDER BY NOMEENTIDADE')
    Left = 645
    Top = 229
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 645
    Top = 273
  end
  object QrWBMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM wbmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 396
    Top = 265
    object QrWBMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWBMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrWBMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrWBMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrWBMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrWBMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrWBMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWBMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWBMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWBMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWBMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWBMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWBMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWBMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrWBMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrWBMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrWBMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrWBMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrWBMovItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrWBMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrWBMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrWBMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrWBMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrWBMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
  end
  object DsWBMovIts: TDataSource
    DataSet = QrWBMovIts
    Left = 396
    Top = 309
  end
  object QrEmitCus: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emt.Codigo, emt.DataEmis, emt.Numero, emt.Setor,'
      'emt.NOMESETOR, emt.NOME, cus.Controle, cus.Custo,'
      'cus.Pecas, cus.Peso, cus.AreaM2, cus.PercTotCus'
      'FROM emitcus cus'
      'LEFT JOIN emit emt ON emt.Codigo=cus.Codigo'
      'WHERE cus.MPVIts=0')
    Left = 200
    Top = 264
    object QrEmitCusCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmitCusDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrEmitCusNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrEmitCusSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrEmitCusNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrEmitCusNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrEmitCusControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmitCusCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEmitCusPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEmitCusPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitCusAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEmitCusPercTotCus: TFloatField
      FieldName = 'PercTotCus'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsEmitCus: TDataSource
    DataSet = QrEmitCus
    Left = 200
    Top = 312
  end
  object QrEntregaEnti: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE NOME END NOMEENTIDADE'
      'FROM entidades'
      'WHERE (Cliente1="V" OR Cliente2="V")'
      'ORDER BY NOMEENTIDADE')
    Left = 649
    Top = 325
    object QrEntregaEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntregaEntiNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEntregaEnti: TDataSource
    DataSet = QrEntregaEnti
    Left = 649
    Top = 369
  end
end
