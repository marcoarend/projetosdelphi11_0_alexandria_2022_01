object FmVSFormulasImp_FI: TFmVSFormulasImp_FI
  Left = 339
  Top = 185
  Caption = 'QUI-RECEI-105 :: Impress'#227'o de Receita de Acabamento'
  ClientHeight = 749
  ClientWidth = 868
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnIUser: TPanel
    Left = 0
    Top = 48
    Width = 868
    Height = 580
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 868
      Height = 181
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 42
        Width = 117
        Height = 13
        Caption = 'Receita de acabamento:'
      end
      object Label2: TLabel
        Left = 16
        Top = 82
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label8: TLabel
        Left = 16
        Top = 2
        Width = 30
        Height = 13
        Caption = 'Artigo:'
        FocusControl = DBEdit1
      end
      object Label14: TLabel
        Left = 396
        Top = 82
        Width = 88
        Height = 13
        Caption = 'Grupo de emiss'#227'o:'
        Enabled = False
      end
      object Label21: TLabel
        Left = 16
        Top = 120
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdTintasCab: TdmkEditCB
        Left = 16
        Top = 58
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTintasCabChange
        DBLookupComboBox = CBTintasCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTintasCab: TdmkDBLookupComboBox
        Left = 72
        Top = 58
        Width = 781
        Height = 21
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsTintasCab
        TabOrder = 3
        dmkEditCB = EdTintasCab
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCliInt: TdmkEditCB
        Left = 16
        Top = 98
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 72
        Top = 98
        Width = 321
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECI'
        ListSource = DsCliInt
        TabOrder = 5
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 18
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'GraGruX'
        DataSource = DsVMIAtu
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 98
        Top = 18
        Width = 756
        Height = 21
        TabStop = False
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVMIAtu
        TabOrder = 1
      end
      object EdEmitGru: TdmkEditCB
        Left = 396
        Top = 98
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmitGru
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmitGru: TdmkDBLookupComboBox
        Left = 453
        Top = 98
        Width = 401
        Height = 21
        Color = clWhite
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEmitGru
        TabOrder = 7
        dmkEditCB = EdEmitGru
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmpresa: TdmkEditCB
        Left = 16
        Top = 136
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 72
        Top = 136
        Width = 437
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 9
        TabStop = False
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 181
      Width = 868
      Height = 351
      Align = alClient
      BevelOuter = bvNone
      Caption = 'Panel4'
      TabOrder = 1
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 868
        Height = 196
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 868
          Height = 45
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LaData: TLabel
            Left = 264
            Top = 0
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object LaHora: TLabel
            Left = 389
            Top = 0
            Width = 55
            Height = 13
            Caption = 'Hora in'#237'cio:'
          end
          object BtTodos: TBitBtn
            Tag = 127
            Left = 13
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Todos'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtTodosClick
          end
          object BtNenhum: TBitBtn
            Tag = 128
            Left = 137
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Nenhum'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtNenhumClick
          end
          object RGTipoPreco: TRadioGroup
            Left = 622
            Top = 0
            Width = 246
            Height = 45
            Align = alRight
            Caption = ' Origem pre'#231'os: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'Estoque'
              'Cadastro'
              'A definir')
            TabOrder = 5
          end
          object RGModeloImp: TRadioGroup
            Left = 450
            Top = 0
            Width = 172
            Height = 45
            Align = alRight
            Caption = ' Modelo de impress'#227'o: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Pesagem'
              'Custos')
            TabOrder = 4
          end
          object TPDataP: TdmkEditDateTimePicker
            Left = 264
            Top = 16
            Width = 122
            Height = 21
            Date = 38795.000000000000000000
            Time = 0.975709085701964800
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpInsumMovimMin
          end
          object EdHoraIni: TdmkEdit
            Left = 388
            Top = 16
            Width = 57
            Height = 21
            TabOrder = 3
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object DBGImpriFlu: TDBGrid
          Left = 0
          Top = 45
          Width = 868
          Height = 151
          Align = alClient
          DataSource = DsImpriFlu
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGImpriFluCellClick
          OnColEnter = DBGImpriFluColEnter
          OnColExit = DBGImpriFluColExit
          OnDrawColumnCell = DBGImpriFluDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              ReadOnly = True
              Title.Caption = 'ok'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdemFlu'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomeFlu'
              Title.Caption = 'Fluxo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomeTin'
              Title.Caption = 'Processo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GramasM2'
              Title.Caption = 'g/m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoKg'
              Title.Caption = '$/kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoTo'
              Title.Caption = '$ total'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GramasTo'
              Title.Caption = 'Peso g'
              Visible = True
            end>
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 196
        Width = 868
        Height = 155
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 101
          Height = 155
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label6: TLabel
            Left = 4
            Top = 4
            Width = 79
            Height = 13
            Caption = 'Lotes de couros:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
        end
        object PageControl2: TPageControl
          Left = 643
          Top = 0
          Width = 225
          Height = 155
          ActivePage = TabSheet4
          Align = alRight
          MultiLine = True
          TabOrder = 1
          TabPosition = tpLeft
          object TabSheet4: TTabSheet
            Caption = 'Observa'#231#245'es'
            object EdMemo: TMemo
              Left = 0
              Top = 0
              Width = 198
              Height = 147
              TabStop = False
              Align = alClient
              Color = clWhite
              TabOrder = 0
            end
          end
        end
        object DBGrid1: TDBGrid
          Left = 101
          Top = 0
          Width = 542
          Height = 155
          Align = alClient
          DataSource = DsVMIOriIMEI
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Pallet'
              Width = 78
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PECAS_POSIT'
              Title.Caption = 'Pe'#231'as'
              Width = 55
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AREAM2_POSIT'
              Title.Caption = #193'rea m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Marca'
              Width = 103
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie'
              Width = 111
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESOKG_POSIT'
              Title.Caption = 'Peso Kg'
              Visible = True
            end>
        end
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 532
      Width = 868
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object Label29: TLabel
        Left = 8
        Top = 4
        Width = 45
        Height = 13
        Caption = #193'rea (m'#178'):'
      end
      object Label30: TLabel
        Left = 84
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label3: TLabel
        Left = 152
        Top = 4
        Width = 28
        Height = 13
        Caption = 'Pe'#231'a:'
      end
      object Label24: TLabel
        Left = 352
        Top = 4
        Width = 52
        Height = 13
        Caption = 'Espessura:'
      end
      object EdAreaM2: TdmkEdit
        Left = 8
        Top = 20
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPecas: TdmkEdit
        Left = 84
        Top = 20
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 1
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdDefPeca: TdmkEditCB
        Left = 152
        Top = 20
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBDefPeca
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBDefPeca: TdmkDBLookupComboBox
        Left = 192
        Top = 20
        Width = 156
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsDefPecas
        TabOrder = 3
        dmkEditCB = EdDefPeca
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEspessura: TdmkEditCB
        Left = 352
        Top = 20
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEspessura
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEspessura: TdmkDBLookupComboBox
        Left = 392
        Top = 20
        Width = 145
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Linhas'
        ListSource = DsEspessuras
        TabOrder = 5
        dmkEditCB = EdEspessura
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkDtCorrApo: TCheckBox
        Left = 544
        Top = 23
        Width = 185
        Height = 17
        Caption = #201' corre'#231#227'o de apontamento. Data:'
        TabOrder = 6
        OnClick = CkDtCorrApoClick
      end
      object TPDtCorrApo: TdmkEditDateTimePicker
        Left = 732
        Top = 19
        Width = 129
        Height = 21
        Date = 45292.000000000000000000
        Time = 0.833253726850671200
        Enabled = False
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN_MAX
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 868
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 820
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 772
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 463
        Height = 32
        Caption = 'Impress'#227'o de Receita de Acabamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 463
        Height = 32
        Caption = 'Impress'#227'o de Receita de Acabamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 463
        Height = 32
        Caption = 'Impress'#227'o de Receita de Acabamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 628
    Width = 868
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 864
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 864
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 685
    Width = 868
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 864
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 720
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrTintasCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, Cli.RazaoSocial, cli.Nome)'
      'NOMECLII, lse.Nome NOMESETOR, esp.LINHAS, cab.*'
      'FROM tintascab cab'
      'LEFT JOIN entidades cli ON cli.Codigo=cab.ClienteI'#10
      'LEFT JOIN listasetores lse ON lse.Codigo=cab.Setor'#10
      'LEFT JOIN espessuras esp ON esp.Codigo=cab.Espessura'#10
      'WHERE cab.Numero>0'
      '')
    Left = 20
    Top = 196
    object QrTintasCabNOMECLII: TWideStringField
      FieldName = 'NOMECLII'
      Size = 100
    end
    object QrTintasCabNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrTintasCabLINHAS: TWideStringField
      FieldName = 'LINHAS'
      Size = 7
    end
    object QrTintasCabNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrTintasCabTxtUsu: TWideStringField
      FieldName = 'TxtUsu'
      Required = True
      Size = 10
    end
    object QrTintasCabNome: TWideStringField
      DisplayWidth = 100
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTintasCabClienteI: TIntegerField
      FieldName = 'ClienteI'
      Required = True
    end
    object QrTintasCabTipificacao: TIntegerField
      FieldName = 'Tipificacao'
      Required = True
    end
    object QrTintasCabDataI: TDateField
      FieldName = 'DataI'
    end
    object QrTintasCabDataA: TDateField
      FieldName = 'DataA'
    end
    object QrTintasCabTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrTintasCabSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrTintasCabEspessura: TIntegerField
      FieldName = 'Espessura'
    end
    object QrTintasCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTintasCabQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0'
    end
    object QrTintasCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTintasCabDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTintasCabDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTintasCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTintasCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTintasCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrTintasCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsTintasCab: TDataSource
    DataSet = QrTintasCab
    Left = 20
    Top = 244
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECI, ci.Codigo'
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"')
    Left = 256
    Top = 356
    object QrCliIntNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 284
    Top = 356
  end
  object QrEspessuras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Linhas, EMCM'
      'FROM espessuras'
      'ORDER BY EMCM')
    Left = 256
    Top = 260
    object QrEspessurasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEspessurasLinhas: TWideStringField
      FieldName = 'Linhas'
      Size = 7
    end
    object QrEspessurasEMCM: TFloatField
      FieldName = 'EMCM'
    end
  end
  object DsEspessuras: TDataSource
    DataSet = QrEspessuras
    Left = 284
    Top = 260
  end
  object DsImpriFlu: TDataSource
    DataSet = QrImpriFlu
    Left = 148
    Top = 244
  end
  object QrImpriIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Ativo, pq.Nome, pcl.PQ PQCI, pcl.CustoPadrao, '
      'pcl.MoedaPadrao, pcl.Controle PRODUTOCI, fi.* '
      'FROM _tintas_impri_its_ fi'#9
      'LEFT JOIN bluederm.PQ    pq  ON pq.Codigo=fi.Produto'
      'LEFT JOIN bluederm.PQCli pcl ON pcl.PQ=fi.Produto AND pcl.CI=:P0')
    Left = 52
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrImpriItsOrdemFlu: TIntegerField
      FieldName = 'OrdemFlu'
    end
    object QrImpriItsOrdemIts: TIntegerField
      FieldName = 'OrdemIts'
    end
    object QrImpriItsTintas: TIntegerField
      FieldName = 'Tintas'
    end
    object QrImpriItsProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrImpriItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrImpriItsGramasTi: TFloatField
      FieldName = 'GramasTi'
    end
    object QrImpriItsGramasKg: TFloatField
      FieldName = 'GramasKg'
    end
    object QrImpriItsGramasTo: TFloatField
      FieldName = 'GramasTo'
    end
    object QrImpriItsPrecoMo_Kg: TFloatField
      FieldName = 'PrecoMo_Kg'
    end
    object QrImpriItsCotacao_Mo: TFloatField
      FieldName = 'Cotacao_Mo'
    end
    object QrImpriItsCustoRS_Kg: TFloatField
      FieldName = 'CustoRS_Kg'
    end
    object QrImpriItsCustoRS_To: TFloatField
      FieldName = 'CustoRS_To'
    end
    object QrImpriItsSiglaMoeda: TWideStringField
      FieldName = 'SiglaMoeda'
      Size = 10
    end
    object QrImpriItsPQCI: TIntegerField
      FieldName = 'PQCI'
    end
    object QrImpriItsCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
    end
    object QrImpriItsMoedaPadrao: TSmallintField
      FieldName = 'MoedaPadrao'
    end
    object QrImpriItsPRODUTOCI: TIntegerField
      FieldName = 'PRODUTOCI'
      Required = True
    end
    object QrImpriItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrImpriItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrImpriItsCustoMedio: TFloatField
      FieldName = 'CustoMedio'
    end
  end
  object QrTintasFlu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tin.Nome NOMEPROCESSO, Tin.Ordem ORDEMTIN, flu.*'
      'FROM tintasflu flu'
      'LEFT JOIN tintastin tin ON tin.Codigo=flu.TintasTin'
      'WHERE flu.Numero=:P0'
      'ORDER BY flu.Ordem')
    Left = 88
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTintasFluNOMEPROCESSO: TWideStringField
      FieldName = 'NOMEPROCESSO'
      Size = 30
    end
    object QrTintasFluOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrTintasFluReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrTintasFluNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrTintasFluCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTintasFluNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrTintasFluTintasTin: TIntegerField
      FieldName = 'TintasTin'
    end
    object QrTintasFluGramasM2: TFloatField
      FieldName = 'GramasM2'
      DisplayFormat = '#,###,##0.000'
    end
    object QrTintasFluORDEMTIN: TIntegerField
      FieldName = 'ORDEMTIN'
    end
    object QrTintasFluInfoCargM2: TFloatField
      FieldName = 'InfoCargM2'
    end
    object QrTintasFluDescri: TWideStringField
      FieldName = 'Descri'
      Size = 255
    end
    object QrTintasFluOpProc: TSmallintField
      FieldName = 'OpProc'
    end
  end
  object QrImpriFlu: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrImpriFluAfterScroll
    RequestLive = True
    Left = 148
    Top = 196
    object QrImpriFluAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrImpriFluOrdemTin: TIntegerField
      FieldName = 'OrdemTin'
      MaxValue = 1
    end
    object QrImpriFluCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImpriFluNomeFlu: TWideStringField
      FieldName = 'NomeFlu'
      Size = 100
    end
    object QrImpriFluNomeTin: TWideStringField
      FieldName = 'NomeTin'
      Size = 30
    end
    object QrImpriFluTintasTin: TIntegerField
      FieldName = 'TintasTin'
    end
    object QrImpriFluOrdemFlu: TIntegerField
      FieldName = 'OrdemFlu'
      MaxValue = 1
    end
    object QrImpriFluGramasM2: TFloatField
      FieldName = 'GramasM2'
      DisplayFormat = '#,###,##0.000'
    end
    object QrImpriFluGramasTo: TFloatField
      FieldName = 'GramasTo'
      DisplayFormat = '#,###,##0'
    end
    object QrImpriFluCustoTo: TFloatField
      FieldName = 'CustoTo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrImpriFluCustoKg: TFloatField
      FieldName = 'CustoKg'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrImpriFluAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrImpriFluCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
    object QrImpriFluCusUSM2: TFloatField
      FieldName = 'CusUSM2'
    end
    object QrImpriFluInfoCargM2: TFloatField
      FieldName = 'InfoCargM2'
    end
    object QrImpriFluDescriFlu: TWideStringField
      FieldName = 'DescriFlu'
      Size = 255
    end
    object QrImpriFluOpProc: TSmallintField
      FieldName = 'OpProc'
    end
  end
  object QrTintasIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTintasItsCalcFields
    SQL.Strings = (
      'SELECT pq_.Nome NOMEPQ,'
      ''
      'IF(pqc.CustoPadrao > 0, pqc.CustoPadrao,'
      'pqc.Valor/pqc.Peso) CustoPadrao,'
      'IF((pqc.Valor/pqc.Peso>0) AND (pqc.Valor>0) AND (pqc.Peso>0),'
      'pqc.Valor/pqc.Peso, pqc.CustoPadrao) CustoMedio,'
      ''
      ''
      'pqc.MoedaPadrao, tii.Numero TintasCab,'
      'tii.Codigo TintasTin, tii.Ordem ORDEMITS,'
      'tii.Controle, tii.Produto, tii.GramasTi,'
      'tii.Gramaskg, tii.Obs, tii.Ativo'
      'FROM tintasits tii'
      ''
      'LEFT JOIN pq pq_ ON pq_.Codigo=tii.Produto'
      'LEFT JOIN pqcli pqc ON pqc.pq=pq_.Codigo'
      'WHERE tii.Codigo=:P0'
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 24
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTintasItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrTintasItsCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
    end
    object QrTintasItsCustoMedio: TFloatField
      FieldName = 'CustoMedio'
    end
    object QrTintasItsMoedaPadrao: TSmallintField
      FieldName = 'MoedaPadrao'
    end
    object QrTintasItsTintasCab: TIntegerField
      FieldName = 'TintasCab'
      Required = True
    end
    object QrTintasItsTintasTin: TIntegerField
      FieldName = 'TintasTin'
      Required = True
    end
    object QrTintasItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTintasItsProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrTintasItsGramasTi: TFloatField
      FieldName = 'GramasTi'
    end
    object QrTintasItsGramaskg: TFloatField
      FieldName = 'Gramaskg'
    end
    object QrTintasItsObs: TWideStringField
      FieldName = 'Obs'
      Size = 30
    end
    object QrTintasItsORDEMITS: TIntegerField
      FieldName = 'ORDEMITS'
      Required = True
    end
    object QrTintasItsSIGLAMOEDA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SIGLAMOEDA'
      Size = 10
      Calculated = True
    end
    object QrTintasItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsImpriIts: TDataSource
    DataSet = QrImpriIts
    Left = 80
    Top = 300
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 284
    Top = 308
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * From DefPecas')
    Left = 256
    Top = 308
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.defpecas.Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.defpecas.Nome'
      Size = 6
    end
    object QrDefPecasGrandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'DBMBWET.defpecas.Grandeza'
    end
  end
  object QrLotes: TMySQLQuery
    Database = Dmod.MyDB
    Left = 668
    Top = 272
  end
  object QrPreuso: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Ativo, ems.Codigo, ems.Controle, '
      'pqc.MoedaPadrao, ems.ProdutoCI,'
      'SUM(ems.Peso_PQ) Peso_PQ, ems.Produto, ems.PQCI,'
      'ems.NomePQ, pqc.Peso, (pqc.Peso+999999999999) EXISTE,'
      ''
      'IF(pqc.CustoPadrao > 0, pqc.CustoPadrao,'
      'pqc.Valor/pqc.Peso) CustoPadrao,'
      ''
      'IF((pqc.Valor/pqc.Peso>0) AND (pqc.Valor>0) AND (pqc.Peso>0),'
      'pqc.Valor/pqc.Peso, pqc.CustoPadrao) CustoMedio,'
      ''
      'ems.Cli_Orig, pqc.Peso-SUM(ems.Peso_PQ) PESOFUTURO, '
      'IF(cli.Tipo=0, cli.RazaoSocial,cli.Nome) NOMECLI_ORIG'
      'FROM emitits ems'
      'LEFT JOIN PQCli     pqc ON pqc.Controle=ems.ProdutoCI'
      'LEFT JOIN PQ     pq ON pq.Codigo=pqc.PQ'
      'LEFT JOIN Entidades cli ON cli.Codigo=ems.Cli_Orig'
      'WHERE ems.Ativo=2'
      'AND ems.Codigo=:P0'
      'GROUP BY ems.Produto, Cli_Orig'
      ''
      '')
    Left = 668
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPreusoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPreusoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPreusoPeso_PQ: TFloatField
      FieldName = 'Peso_PQ'
    end
    object QrPreusoProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrPreusoPQCI: TIntegerField
      FieldName = 'PQCI'
      Required = True
    end
    object QrPreusoProdutoCI: TIntegerField
      FieldName = 'ProdutoCI'
      Required = True
    end
    object QrPreusoNomePQ: TWideStringField
      FieldName = 'NomePQ'
      Size = 50
    end
    object QrPreusoPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPreusoPESOFUTURO: TFloatField
      FieldName = 'PESOFUTURO'
    end
    object QrPreusoCli_Orig: TIntegerField
      FieldName = 'Cli_Orig'
      Required = True
    end
    object QrPreusoNOMECLI_ORIG: TWideStringField
      FieldName = 'NOMECLI_ORIG'
      Size = 100
    end
    object QrPreusoDEFASAGEM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DEFASAGEM'
      Calculated = True
    end
    object QrPreusoCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
    end
    object QrPreusoMoedaPadrao: TSmallintField
      FieldName = 'MoedaPadrao'
    end
    object QrPreusoCustoMedio: TFloatField
      FieldName = 'CustoMedio'
    end
    object QrPreusoEXISTE: TFloatField
      FieldName = 'EXISTE'
    end
    object QrPreusoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrSumCus: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(CustoTo) CustoTo, '
      'SUM(CustoKg) CustoKg, '
      'SUM(CustoM2) CustoM2, '
      'SUM(CusUSM2) CusUSM2'
      'FROM _tintas_impri_flu_')
    Left = 452
    Top = 296
    object QrSumCusCustoKg: TFloatField
      FieldName = 'CustoKg'
    end
    object QrSumCusCustoTo: TFloatField
      FieldName = 'CustoTo'
    end
    object QrSumCusCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
    object QrSumCusCusUSM2: TFloatField
      FieldName = 'CusUSM2'
    end
  end
  object QrVal: TMySQLQuery
    Database = Dmod.MyDB
    Left = 456
    Top = 200
    object QrValCustoRS_To: TFloatField
      FieldName = 'CustoRS_To'
    end
  end
  object QrSqr: TMySQLQuery
    Database = Dmod.MyDB
    Left = 456
    Top = 248
    object QrSqrAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object DsVMIAtu: TDataSource
    DataSet = QrVMIAtu
    Left = 552
    Top = 413
  end
  object QrVMIAtu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 552
    Top = 365
    object QrVMIAtuCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVMIAtuControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVMIAtuMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVMIAtuMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVMIAtuMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVMIAtuEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVMIAtuTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVMIAtuCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVMIAtuMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVMIAtuDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVMIAtuPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVMIAtuGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVMIAtuPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAtuPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAtuAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVMIAtuSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVMIAtuSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVMIAtuSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVMIAtuSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMIAtuSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAtuSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVMIAtuSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVMIAtuFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVMIAtuMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVMIAtuFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVMIAtuCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVMIAtuCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVMIAtuCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVMIAtuDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVMIAtuDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVMIAtuDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVMIAtuQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMIAtuQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAtuQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMIAtuQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAtuQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAtuNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVMIAtuNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVMIAtuNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVMIAtuNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVMIAtuID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVMIAtuNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVMIAtuReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVMIAtuCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVMIAtuCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVMIAtuNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVMIAtuMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVMIAtuPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVMIAtuStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVMIAtuNO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
    object QrVMIAtuNO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrVMIAtuClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
  end
  object QrEmitGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM emitgru'
      'WHERE Ativo=1'
      'ORDER BY Nome'
      '')
    Left = 576
    Top = 273
    object QrEmitGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEmitGru: TDataSource
    DataSet = QrEmitGru
    Left = 576
    Top = 317
  end
  object QrVMIOriIMEI: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVMIOriIMEICalcFields
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 368
    Top = 261
    object QrVMIOriIMEICodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVMIOriIMEIControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVMIOriIMEIMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVMIOriIMEIMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVMIOriIMEIMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVMIOriIMEIEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVMIOriIMEITerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVMIOriIMEICliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVMIOriIMEIMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVMIOriIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVMIOriIMEIPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVMIOriIMEIGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVMIOriIMEIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriIMEIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriIMEIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEISrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVMIOriIMEISrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVMIOriIMEISrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVMIOriIMEISrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVMIOriIMEISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMIOriIMEISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriIMEISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVMIOriIMEISerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVMIOriIMEIFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVMIOriIMEIMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVMIOriIMEIFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVMIOriIMEICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVMIOriIMEIDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVMIOriIMEIDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVMIOriIMEIDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVMIOriIMEIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMIOriIMEIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriIMEIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMIOriIMEIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIOriIMEIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIOriIMEINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVMIOriIMEINO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVMIOriIMEINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVMIOriIMEINO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVMIOriIMEIID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVMIOriIMEINO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVMIOriIMEINO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVMIOriIMEIReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVMIOriIMEIVSMovIts: TLargeintField
      FieldKind = fkCalculated
      FieldName = 'VSMovIts'
      Calculated = True
    end
    object QrVMIOriIMEIPECAS_POSIT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PECAS_POSIT'
      Calculated = True
    end
    object QrVMIOriIMEIAREAM2_POSIT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AREAM2_POSIT'
      Calculated = True
    end
    object QrVMIOriIMEIPESOKG_POSIT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PESOKG_POSIT'
      Calculated = True
    end
  end
  object DsVMIOriIMEI: TDataSource
    DataSet = QrVMIOriIMEI
    Left = 368
    Top = 309
  end
  object QrEmitCus: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT(mpi.Texto, " ", mpi.CorTxt) TextoECor,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CLI,'
      'ecu.* '
      'FROM emitcus ecu'
      'LEFT JOIN mpvits mpi ON mpi.Controle=ecu.MPVIts'
      'LEFT JOIN mpp mpp ON mpp.Codigo=mpi.Pedido '
      'LEFT JOIN entidades ent ON ent.Codigo=mpp.Cliente '
      'WHERE ecu.Controle>0 ')
    Left = 744
    Top = 269
    object QrEmitCusTextoECor: TWideStringField
      FieldName = 'TextoECor'
      Size = 81
    end
    object QrEmitCusNOME_CLI: TWideStringField
      FieldName = 'NOME_CLI'
      Size = 100
    end
    object QrEmitCusCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitCusControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmitCusMPIn: TIntegerField
      FieldName = 'MPIn'
    end
    object QrEmitCusFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrEmitCusDataEmis: TDateTimeField
      FieldName = 'DataEmis'
    end
    object QrEmitCusPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitCusCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrEmitCusPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEmitCusAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitCusMPVIts: TIntegerField
      FieldName = 'MPVIts'
    end
    object QrEmitCusAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEmitCusAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrEmitCusPercTotCus: TFloatField
      FieldName = 'PercTotCus'
    end
  end
  object DsEmitCus: TDataSource
    DataSet = QrEmitCus
    Left = 772
    Top = 269
  end
end
