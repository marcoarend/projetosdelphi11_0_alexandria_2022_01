unit VSRMPPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, mySQLDbTables, AppListas, UnDmkProcFunc, dmkDBGridZTO,
  dmkCheckBox, UnProjGroup_Consts, UnProjGroup_Vars, UnAppEnums,
  dmkEditDateTimePicker, frxClass, frxDBSet;

type
  TFmVSRMPPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DsVSSerFch: TDataSource;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqControle: TIntegerField;
    QrPesqNO_SerieFch: TWideStringField;
    QrPesqFicha: TIntegerField;
    QrPesqMarca: TWideStringField;
    QrPesqCodigo: TIntegerField;
    DGDados: TDBGrid;
    QrPesqSerieFch: TIntegerField;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    Panel6: TPanel;
    Panel5: TPanel;
    Label11: TLabel;
    Label4: TLabel;
    Label12: TLabel;
    Label6: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label22: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    CBSerieFch: TdmkDBLookupComboBox;
    EdSerieFch: TdmkEditCB;
    EdFicha: TdmkEdit;
    EdMarca: TdmkEdit;
    EdControle: TdmkEdit;
    BtReabre: TBitBtn;
    EdPallet: TdmkEdit;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    EdMotorista: TdmkEditCB;
    CBMotorista: TdmkDBLookupComboBox;
    EdSrcNivel2: TdmkEdit;
    EdDstNivel2: TdmkEdit;
    EdObserv: TdmkEdit;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    DBGMovimNiv: TdmkDBGridZTO;
    QrMovimNiv: TmySQLQuery;
    DsMovimNiv: TDataSource;
    QrMovimNivNO_MovimNiv: TWideStringField;
    QrPesqNO_MovimNiv: TWideStringField;
    CkSdoPositivo: TdmkCheckBox;
    QrPesqNO_FORNECE: TWideStringField;
    RGTabela: TRadioGroup;
    QrMovimNivMovimNiv: TLargeintField;
    QrPesqMovimCod: TIntegerField;
    QrPesqCliVenda: TIntegerField;
    QrPesqCustoMOKg: TFloatField;
    QrPesqCustoMOM2: TFloatField;
    QrPesqDataHora: TDateTimeField;
    QrPesqNO_PRD_TAM_COR: TWideStringField;
    BtDefIMEI: TBitBtn;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    EdPecas: TdmkEdit;
    CkPecas: TCheckBox;
    CkAreaM2: TCheckBox;
    EdAreaM2: TdmkEdit;
    EdAreaP2: TdmkEdit;
    CkAreaP2: TCheckBox;
    EdPesoKg: TdmkEdit;
    CkPesoKg: TCheckBox;
    EdNFeNum: TdmkEdit;
    Label10: TLabel;
    CkNFeSer: TCheckBox;
    EdNFeSer: TdmkEdit;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    Label13: TLabel;
    Label14: TLabel;
    BtImprime: TBitBtn;
    QrPesqPecas: TFloatField;
    QrPesqPesoKg: TFloatField;
    QrPesqAreaM2: TFloatField;
    QrPesqAreaP2: TFloatField;
    QrPesqValorT: TFloatField;
    frxWET_CURTI_037: TfrxReport;
    frxDsPesq: TfrxDBDataset;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure BtDefIMEIClick(Sender: TObject);
    procedure frxWET_CURTI_037GetValue(const VarName: string;
      var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
  private
    { Private declarations }
    procedure LocalizaRegistro;
    procedure ReabrePesquisa();
  public
    { Public declarations }
    FOnlySelIMEI: Boolean;
    FMovimID: TEstqMovimID;
    FCodigo, FControle, FSerieFch, FFicha: Integer;
    //
    procedure ReopenMovimNiv(MovimID: TEstqMovimID);
  end;

  var
  FmVSRMPPsq: TFmVSRMPPsq;

implementation

uses UnMyObjects, Module, UnVS_CRC_PF, DmkDAC_PF, ModVS_CRC, ModuleGeral;

{$R *.DFM}

procedure TFmVSRMPPsq.BtDefIMEIClick(Sender: TObject);
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    FCodigo   := QrPesqCodigo.Value;
    FControle := QrPesqControle.Value;
    FSerieFch := QrPesqSerieFch.Value;
    FFicha    := QrPesqFicha.Value;
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS_CRC.QrIMEI, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE Controle=' + Geral.FF0(QrPesqControle.Value),
    '']);
    //
    Close;
  end;
end;

procedure TFmVSRMPPsq.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxWET_CURTI_037, [
  DModG.frxDsDono,
  frxDsPesq
  ]);
  MyObjects.frxMostra(frxWET_CURTI_037, 'Pesquisa de IME-Is');
end;

procedure TFmVSRMPPsq.BtOKClick(Sender: TObject);
begin
  LocalizaRegistro;
end;

procedure TFmVSRMPPsq.BtReabreClick(Sender: TObject);
begin
  ReabrePesquisa();
  if (QrPesq.RecordCount = 0) and (RGTabela.Itemindex = 0) then
  begin
    RGTabela.Itemindex := 1;
    ReabrePesquisa();
    RGTabela.Itemindex := 0;
  end;
end;

procedure TFmVSRMPPsq.BtSaidaClick(Sender: TObject);
begin
  FCodigo   := 0;
  FControle := 0;
  FSerieFch := 0;
  FFicha    := 0;
  DmModVS_CRC.QrIMEI.Close;
  Close;
end;

procedure TFmVSRMPPsq.DGDadosDblClick(Sender: TObject);
begin
  case ImgTipo.SQLType of
    stPsq:
    begin
      if FOnlySelIMEI then
      begin
        VAR_IMEI_SEL := QrPesqControle.Value;
        Close;
      end else
        LocalizaRegistro;
    end;
    stCpy: BtDefIMEIClick(Self);
  end;
end;

procedure TFmVSRMPPsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSRMPPsq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FOnlySelIMEI := False;
  //
  FCodigo   := 0;
  FControle := 0;
  FSerieFch := 0;
  FFicha    := 0;
  DmModVS_CRC.QrIMEI.Close;
  //
  VS_CRC_PF.AbreVSSerFch(QrVSSerFch);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  //
  TPIni.Date := 0; //Date - 90;
  TPFim.Date := 0; //Date;
end;

procedure TFmVSRMPPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRMPPsq.FormShow(Sender: TObject);
begin
  EdControle.SetFocus;
end;

procedure TFmVSRMPPsq.frxWET_CURTI_037GetValue(const VarName: string;
  var Value: Variant);
begin
(*
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
*)
  if VarName ='VARF_DATA' then
    Value := Now()

end;

procedure TFmVSRMPPsq.LocalizaRegistro;
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    FCodigo   := QrPesqCodigo.Value;
    FControle := QrPesqControle.Value;
    FSerieFch := QrPesqSerieFch.Value;
    FFicha    := QrPesqFicha.Value;
    Close;
  end;
end;

procedure TFmVSRMPPsq.QrPesqAfterOpen(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Registros encontrados: ' +
  Geral.FF0(QrPesq.RecordCount));
end;

procedure TFmVSRMPPsq.QrPesqBeforeClose(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, '...');
end;

procedure TFmVSRMPPsq.ReabrePesquisa();
var
  Observ, Marca, SQL1, SQL2, SQL3, SQL4, SQL5, SQL6, SQL7, SQL8, SQL9, SQL0,
  SQLA, SQLB, SQLC, SQLD, SQLE, SQLF, SQLG, SQLH, SQLI, SQLJ, SQLK, SQL_MovimID,
  MovimNivs: String;
  Controle, SerieFch, Ficha, Pallet, Terceiro, Motorista, Fornecedor, SrcNivel2,
  DstNivel2, CliVenda, FornecMO, NFeNum: Integer;
  ATT_MovimNiv, Tabela: String;
  DtIni, DtFim: TDateTime;
  CkIni, CkFim: Boolean;
  // ini 2022-04-09
  TabMaeNome, SQL_TabMaeLeftJoin, SQL_TabMaeAND: String;
begin
  case RGTabela.ItemIndex of
    0: Tabela := CO_SEL_TAB_VMI;
    1: Tabela := CO_TAB_VMB;
    else Tabela := 'vsmovit?';
  end;
  ATT_MovimNiv := dmkPF.ArrayToTexto('MovimNiv', 'NO_MovimNiv', pvNo, True,
    sEstqMovimNiv);
  Controle := EdControle.ValueVariant;
  SerieFch := EdSerieFch.ValueVariant;
  Ficha    := EdFicha.ValueVariant;
  Marca    := EdMarca.Text;
  Pallet   := EdPallet.ValueVariant;
  Fornecedor  := EdFornecedor.ValueVariant;
  CliVenda    := EdCliente.ValueVariant;
  FornecMO    := EdFornecMO.ValueVariant;
  Motorista   := EdMotorista.ValueVariant;
  SrcNivel2   := EdSrcNivel2.ValueVariant;
  DstNivel2   := EdDstNivel2.ValueVariant;
  Observ      := EdObserv.Text;
  MovimNivs   := VS_CRC_PF.DefineIDs_Str(DBGMovimNiv, QrMovimNiv, 'MovimNiv');
  NFeNum      := EdNFeNum.ValueVariant;
  //
  if FMovimID <> emidAjuste then
    SQL_MovimID := 'WHERE vmi.MovimID=' + Geral.FF0(Integer(FMovimID))
  else
    SQL_MovimID := 'WHERE vmi.MovimID >= 0';
  //
  if Controle <> 0 then
    SQL1 := 'AND vmi.Controle=' + Geral.FF0(Controle)
  else
    SQL1 := '';
  //
  if SerieFch <> 0 then
    SQL2 := 'AND vmi.SerieFch=' + Geral.FF0(SerieFch)
  else
    SQL2 := '';
  //
  if Ficha <> 0 then
    SQL3 := 'AND vmi.Ficha=' + Geral.FF0(Ficha)
  else
    SQL3 := '';
  //
  if Marca <> '' then
  begin
    if Pos('%', Marca) > 0 then
      SQL4 := 'AND vmi.Marca LIKE "' + Marca + '"'
    else
      SQL4 := 'AND vmi.Marca="' + Marca + '"'
  end else
    SQL4 := '';
  //
  if Pallet <> 0 then
    SQL5 := 'AND vmi.Pallet=' + Geral.FF0(Pallet)
  else
    SQL5 := '';
  //
  if Fornecedor <> 0 then
    SQL6 := 'AND vmi.Terceiro=' + Geral.FF0(Fornecedor)
  else
    SQL6 := '';
  //
  if Motorista <> 0 then
    SQL7 := Geral.ATS([
    'AND vmi.MovimCod IN (',
    '  SELECT MovimCod',
    '  FROM vsinncab',
    '  WHERE Motorista=' + Geral.FF0(Motorista),
    '  )'])
  else
    SQL7 := '';
  if SrcNivel2 <> 0 then
    SQL8 := 'AND vmi.SrcNivel2=' + Geral.FF0(SrcNivel2)
  else
    SQL8 := '';
  //
  if DstNivel2 <> 0 then
    SQL9 := 'AND vmi.DstNivel2=' + Geral.FF0(DstNivel2)
  else
    SQL9 := '';
  //
  if Observ <> '' then
    SQL0 := 'AND vmi.Observ LIKE ' + '"%' + Observ + '%"'
  else
    SQL0 := '';
  //
  if CliVenda <> 0 then
    SQLA := 'AND vmi.CliVenda=' + Geral.FF0(CliVenda)
  else
    SQLA := '';
  //
  if FornecMO <> 0 then
    SQLB := 'AND vmi.FornecMO=' + Geral.FF0(FornecMO)
  else
    SQLB := '';
  //
  if MovimNivs <> '' then
    SQLC := 'AND vmi.MovimNiv IN (' + MovimNivs + ')'
  else
    SQLC := '';
  if CkSdoPositivo.Checked then
    SQLD := 'AND vmi.SdoVrtPeca > 0'
  else
    SQLD := '';
  //
  SQLE := Geral.FFT_Dot(EdPecas.ValueVariant, 3, siPositivo);
  if CkPecas.Checked then
    SQLE := 'AND (vmi.Pecas = ' + SQLE + ' OR vmi.Pecas = -' + SQLE +
    ' OR vmi.SdoVrtPeca=' + SQLE + ') '
  else
    SQLE := '';
  //
  SQLF := Geral.FFT_Dot(EdAreaM2.ValueVariant, 3, siPositivo);
  if CkAreaM2.Checked then
    SQLF := 'AND (vmi.AreaM2 = ' + SQLF + ' OR vmi.AreaM2 = -' + SQLF +
    ' OR vmi.SdoVrtArM2=' + SQLF + ') '
  else
    SQLF := '';
  //
  SQLG := Geral.FFT_Dot(EdAreaP2.ValueVariant, 3, siPositivo);
  if CkAreaP2.Checked then
    SQLG := 'AND (vmi.AreaP2 = ' + SQLG + ' OR vmi.AreaP2 = -' + SQLG +
    ' OR vmi.SdoVrtArP2=' + SQLG + ') '
  else
    SQLG := '';
  //
  SQLH := Geral.FFT_Dot(EdPesoKg.ValueVariant, 3, siPositivo);
  if CkPesoKg.Checked then
    SQLH := 'AND (vmi.PesoKg = ' + SQLH + ' OR vmi.PesoKg = -' + SQLH +
    ' OR vmi.SdoVrtPeso=' + SQLH + ') '
  else
    SQLH := '';
  // ini 2022-04-09
  //if EdNFeNum.ValueVariant <> 0 then
  //  SQLI := 'AND vmi.NFeNum = ' + Geral.FF0(EdNFeNum.ValueVariant)
  //else
  //  SQLI := '';
  //
  SQL_TabMaeLeftJoin := '';
  SQL_TabMaeAND := '';
  TabMaeNome := VS_CRC_PF.ObtemNomeTabelaVSXxxCab(FMovimID, False);
  if NFeNum > 0 then
  begin
    if TabMaeNome <> CO_FIVE_ASKS then
    begin
      case FMovimID of
        (*01*)emidCompra,                 // 'vsinncab';
        (*16*)emidEntradaPlC,             // 'vsplccab';
        (*21*)emidDevolucao,              // 'vsdvlcab';
        (*22*)emidRetrabalho,             // 'vsrtbcab';
        (*36*)emidInnSemCob:              // 'vsesccab';
        begin
          SQL_TabMaeAND := 'AND (' +
            'vmi.NFeNum = ' + Geral.FF0(NFeNum) +
            ' OR tmn.ide_nNF=' + Geral.FF0(NFeNum) +
            ' OR tmn.emi_nNF=' + Geral.FF0(NFeNum) +
            ')';
        end;
        (*11*)emidEmOperacao,             // 'vsopecab';
        (*14*)emidClassArtXXMul,          // 'vspamulcaba';
        (*19*)emidEmProcWE,               // 'vspwecab';
        (*20*)emidFinished,               // 'vspwecab';
        (*24*)emidReclasXXMul,            // 'vspamulcabr';
        (*26*)emidEmProcCal,              // 'vscalcab';
        (*27*)emidEmProcCur,              // 'vscurcab';
        (*30*)emidEmRibPDA,               // 'vscalpda';
        (*31*)emidEmRibDTA,              // 'vscaldta';
        (*33*)emidEmReprRM,               // 'vsrrmcab';
        (*34*)emidCurtido,                // 'vscurjmp';
        (*32*)emidEmProcSP:               // 'vspspcab';
        begin
          SQL_TabMaeAND := 'AND (' +
            'vmi.NFeNum = ' + Geral.FF0(NFeNum) +
            ' OR tmn.NFeRem=' + Geral.FF0(NFeNum) +
            ')';
        end;
       (*25*)emidTransfLoc,              // 'vstrfloccab';
       (*28*)emidDesclasse:              // 'vsdsccab';
       begin
          SQL_TabMaeAND := 'AND ( ' +
            'vmi.NFeNum = ' + Geral.FF0(NFeNum) +
            ' OR tmn.ide_nNF=' + Geral.FF0(NFeNum) +
            ')';
       end;
      end;
    end;
    if SQL_TabMaeAND <> EmptyStr then
      SQL_TabMaeLeftJoin := 'LEFT JOIN ' + TabMaeNome + ' tmn ON tmn.MovimCod=vmi.MovimCod';
  end;
  //
  if CkNFeSer.Checked then
    SQLJ := 'AND vmi.NFeSer = ' + Geral.FF0(EdNFeSer.ValueVariant)
  else
    SQLJ := '';
  //
  if (TPIni.Date > 2) or (TPFim.Date > 2) then
  begin
    DtIni := Trunc(TPIni.Date);
    DtFim := Trunc(TPFim.Date);
    CkIni := DtIni > 2;
    CkFim := DtFim > 2;
    //
    SQLK := dmkPF.SQL_Periodo('AND vmi.DataHora ', DtIni, DtFim, CkIni, CkFim);
  end else
    SQLK := '';

  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    // ini 2023-03-30
    //'SELECT DISTINCT ',
    'SELECT ABS(vmi.Pecas) Pecas, ABS(vmi.PesoKg) PesoKg,   ',
    'ABS(vmi.AreaM2) AreaM2, ABS(vmi.AreaP2) AreaP2,    ',
    'ABS(vmi.ValorT) ValorT,  ',
    // fim 2023-03-30
    VS_CRC_PF.SQL_NO_GGX(),
    VS_CRC_PF.SQL_NO_FRN(),
    'vmi.Codigo, vmi.MovimCod, vmi.Controle, vsf.Nome NO_SerieFch, ',
    'vmi.Ficha, vmi.Marca, vmi.SerieFch, ',
    'vmi.DataHora, vmi.CliVenda, vmi.CustoMOKg, vmi.CustoMOM2, ',
    ATT_MovimNiv,
    'FROM ' + Tabela + ' vmi ',
    'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    VS_CRC_PF.SQL_LJ_GGX(),
    VS_CRC_PF.SQL_LJ_FRN(),
    SQL_TabMaeLeftJoin,
    //'WHERE vmi.Ativo = 1 ',
    SQL_MovimID,
    SQL1,
    SQL2,
    SQL3,
    SQL4,
    SQL5,
    SQL6,
    SQL7,
    SQL8,
    SQL9,
    SQL0,
    SQLA,
    SQLB,
    SQLC,
    SQLD,
    SQLE,
    SQLF,
    SQLG,
    SQLH,
    SQLI,
    SQLJ,
    SQLK,
    //
    SQL_TabMaeAND,
    //
    'ORDER BY vmi.Controle DESC ',
    '']);
  //Geral.MB_Teste(QrPesq.SQL.Text);
end;

procedure TFmVSRMPPsq.ReopenMovimNiv(MovimID: TEstqMovimID);
var
  ATT_MovimNiv: String;
begin
  ATT_MovimNiv := dmkPF.ArrayToTexto('MovimNiv', 'NO_MovimNiv', pvNo, True,
    sEstqMovimNiv);
  UnDmkDAC_PF.AbreMySQLQuery0(QrMovimNiv, Dmod.MyDB, [
  'SELECT DISTINCT CAST(MovimNiv AS SIGNED) MovimNiv, ',
  ATT_MovimNiv,
  'FROM ' + CO_TAB_VMB,
  'WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
  '',
  'UNION ',
  '',
  'SELECT DISTINCT CAST(MovimNiv AS SIGNED) MovimNiv, ',
  ATT_MovimNiv,
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
  '']);
end;

end.
