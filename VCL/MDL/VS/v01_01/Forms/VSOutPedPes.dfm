object FmVSOutPedPes: TFmVSOutPedPes
  Left = 339
  Top = 185
  Caption = 'FAT-PEDID-007 :: Pesquisa de Pedidos Pendentes'
  ClientHeight = 577
  ClientWidth = 775
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 775
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object Label2: TLabel
      Left = 12
      Top = 44
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object SpeedButton1: TSpeedButton
      Left = 69
      Top = 60
      Width = 22
      Height = 21
      Caption = '?'
      OnClick = SpeedButton1Click
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 68
      Top = 20
      Width = 500
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 1
      dmkEditCB = EdEmpresa
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdEmpresa: TdmkEditCB
      Left = 12
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEmpresaChange
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdCliente: TdmkEditCB
      Left = 12
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdClienteChange
      DBLookupComboBox = CBCliente
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 92
      Top = 60
      Width = 476
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENT'
      ListSource = DsClientes
      TabOrder = 3
      dmkEditCB = EdCliente
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object DBGItens: TdmkDBGrid
    Left = 0
    Top = 137
    Width = 775
    Height = 332
    Align = alClient
    Columns = <
      item
        Expanded = False
        FieldName = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Title.Caption = 'C'#243'digo'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataF'
        Title.Caption = 'Emiss'#227'o'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PedidCli'
        Title.Caption = 'Pedido cliente'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEVENDEDOR'
        Title.Caption = 'Vendedor'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pecas'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PesoKg'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AreaM2'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AreaP2'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Valor'
        Width = 80
        Visible = True
      end>
    Color = clWindow
    DataSource = DsVSPedCab
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGItensDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Title.Caption = 'C'#243'digo'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataF'
        Title.Caption = 'Emiss'#227'o'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PedidCli'
        Title.Caption = 'Pedido cliente'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEVENDEDOR'
        Title.Caption = 'Vendedor'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pecas'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PesoKg'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AreaM2'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AreaP2'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Valor'
        Width = 80
        Visible = True
      end>
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 775
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 727
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 679
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 391
        Height = 32
        Caption = 'Pesquisa de Pedidos Pendentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 391
        Height = 32
        Caption = 'Pesquisa de Pedidos Pendentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 391
        Height = 32
        Caption = 'Pesquisa de Pedidos Pendentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 469
    Width = 775
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 771
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 513
    Width = 775
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 771
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 627
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT cli.Codigo, '
      'IF(cli.Tipo=0,RazaoSocial,Cli.Nome) NOMEENT '
      'FROM pedivda pvd'
      'LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente'
      'WHERE pvd.Codigo IN (    '
      '     SELECT DISTINCT Codigo '
      '     FROM pedivdaits'
      '     WHERE QuantP-QuantC-QuantV > 0'
      ')'
      'ORDER BY NOMEENT')
    Left = 184
    Top = 220
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 184
    Top = 268
  end
  object QrVSPedCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMEVENDEDOR,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,'
      'IF(tr.Tipo=0, tr.RazaoSocial, tr.Nome) NOMETRANSP,'
      'pc.Nome NO_CONDICAOPG,'
      'vpc.*'
      'FROM vspedcab vpc'
      'LEFT JOIN entidades emp ON emp.Codigo=vpc.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=vpc.Cliente'
      'LEFT JOIN entidades ve ON ve.Codigo=vpc.Vendedor'
      'LEFT JOIN entidades tr ON tr.Codigo=vpc.Transp'
      'LEFT JOIN pediprzcab pc ON pc.Codigo=vpc.CondicaoPg'
      'WHERE vpc.Codigo > 0'
      '')
    Left = 88
    Top = 221
    object QrVSPedCabNOMEVENDEDOR: TWideStringField
      FieldName = 'NOMEVENDEDOR'
      Size = 100
    end
    object QrVSPedCabNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrVSPedCabNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrVSPedCabNO_CONDICAOPG: TWideStringField
      FieldName = 'NO_CONDICAOPG'
      Size = 50
    end
    object QrVSPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPedCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPedCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSPedCabVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrVSPedCabDataF: TDateField
      FieldName = 'DataF'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrVSPedCabValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSPedCabObz: TWideStringField
      FieldName = 'Obz'
      Size = 255
    end
    object QrVSPedCabTransp: TIntegerField
      FieldName = 'Transp'
    end
    object QrVSPedCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrVSPedCabPedidCli: TWideStringField
      FieldName = 'PedidCli'
      Size = 60
    end
    object QrVSPedCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSPedCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPedCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPedCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPedCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPedCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPedCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPedCabPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object QrVSPedCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object QrVSPedCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00 '
    end
    object QrVSPedCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00 '
    end
    object QrVSPedCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPedCabNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
  end
  object DsVSPedCab: TDataSource
    DataSet = QrVSPedCab
    Left = 88
    Top = 265
  end
  object QrCorda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Codigo'
      'FROM vspedits'
      'WHERE Status < 9')
    Left = 88
    Top = 168
    object QrCordaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end
