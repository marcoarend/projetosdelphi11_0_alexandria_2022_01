unit VSExBIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.ComCtrls,
  dmkEditDateTimePicker, Vcl.Grids, Vcl.DBGrids, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSExBIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    QrVSPallet: TmySQLQuery;
    DsVSPallet: TDataSource;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    BtExclui: TBitBtn;
    QrGraGruXGraGruY: TIntegerField;
    Panel5: TPanel;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    Label4: TLabel;
    SBPallet: TSpeedButton;
    Label9: TLabel;
    Label5: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    EdPecas: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    EdPesoKg: TdmkEdit;
    EdPsqPallet: TdmkEditCB;
    CBPsqPallet: TdmkDBLookupComboBox;
    EdObserv: TdmkEdit;
    EdValorT: TdmkEdit;
    Label14: TLabel;
    EdSrcMovID: TdmkEdit;
    Label10: TLabel;
    EdSrcNivel1: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    Label17: TLabel;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsNO_Pallet: TWideStringField;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TIntegerField;
    DsVSMovIts: TDataSource;
    Label11: TLabel;
    EdPsqFicha: TdmkEdit;
    QrVSMovItsSerieFch: TIntegerField;
    EdPsqSerieFch: TdmkEditCB;
    Label2: TLabel;
    CBPsqSerieFch: TdmkDBLookupComboBox;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label12: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdDtEntrada: TdmkDBEdit;
    EdSrcGGX: TdmkEdit;
    Label13: TLabel;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    EdIMEI: TdmkEdit;
    Label15: TLabel;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSMovItsClientMO: TIntegerField;
    QrVSMovItsStqCenLoc: TIntegerField;
    Panel3: TPanel;
    Panel7: TPanel;
    DBGVSMovIts: TDBGrid;
    Label16: TLabel;
    EdMaxPecas: TdmkEdit;
    RGPosiNega: TRadioGroup;
    RGDataHora: TRadioGroup;
    Panel8: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    TPDataSenha: TdmkEditDateTimePicker;
    EdHoraSenha: TdmkEdit;
    EdSenha: TEdit;
    QrLastMov: TMySQLQuery;
    QrLastMovDataHora: TDateTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBPalletClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure EdSrcMovIDChange(Sender: TObject);
    procedure EdSrcNivel1Change(Sender: TObject);
    procedure EdSrcNivel2Change(Sender: TObject);
    procedure EdFornecedorChange(Sender: TObject);
    procedure EdPsqPalletRedefinido(Sender: TObject);
    procedure DBGVSMovItsDblClick(Sender: TObject);
    procedure EdPsqFichaRedefinido(Sender: TObject);
    procedure EdPsqSerieFchRedefinido(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAreaM2Change(Sender: TObject);
    procedure RGPosiNegaClick(Sender: TObject);
    procedure EdIMEIRedefinido(Sender: TObject);
    procedure EdMaxPecasRedefinido(Sender: TObject);
    procedure EdPecasRedefinido(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    FBalDtHr: String;
    //
    procedure CopiaTotaisIMEI();
    procedure ReopenVSExbIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure HabilitaInclusao();
    procedure ReopenVSMovIts();
    procedure ReopenVSPallet();
    function  CalculaValorT(): Double;

  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FDataHora: TDateTime;
    FEmpresa, FAntSrcNivel2: Integer;
    //FDsCab: TDataSource;
  end;

  var
  FmVSExBIts: TFmVSExBIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  Principal, ModuleGeral, UnVS_PF, VSExbCab, AppListas;

{$R *.DFM}

procedure TFmVSExBIts.BtExcluiClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := EdControle.ValueVariant;
  if Geral.MB_Pergunta('Confirme a exclus�o do IME-I' +
  Geral.FF0(Controle) + '?') = ID_YES then
  begin
    VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle,
    Integer(TEstqMotivDel.emtdWetCurti079), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
    VS_PF.AtualizaSaldoIMEI(Controle, False);
    //
    Close;
  end;
end;

procedure TFmVSExBIts.BtOKClick(Sender: TObject);
const
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  CustoMOTot = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  //
  TpCalcAuto = -1;
  //
  EdFicha  = nil;
  EdPallet = nil;
  ExigeFornecedor = False;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  ItemNFe    = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  BalDtHr: String;
  //
  function DefineLastDtHr(DataHora: TDateTime): String;
  begin
    if VS_PF.ImpedePeloBalanco(DataHora, False, False) then
      Result := FBalDtHr
    else
      Result := Geral.FDT(DataHora, 109);
  end;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, StqCenCad,
  GraGruX, Terceiro, SerieFch, Ficha, SrcNivel1, SrcNivel2, (*Misturou,*)
  GraGruY, SrcGGX, VSMulFrnCab, ClientMO, FornecMO, StqCenLoc: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  ExigeAreaouPeca: Boolean;
begin
  SrcMovID       := EdSrcMovID.ValueVariant;
  SrcNivel1      := EdSrcNivel1.ValueVariant;
  SrcNivel2      := EdSrcNivel2.ValueVariant;
  SrcGGX         := EdSrcGGX.ValueVariant;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := FEmpresa;
  //ClienteMO      := FClientMO;
  //ClientMO       := FClientMO;
  ClientMO       := QrVSMovItsClientMO.Value;
  FornecMO       := QrVSMovItsFornecMO.Value;
  StqCenLoc      := QrVSMovItsStqCenLoc.Value;
  Terceiro       := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  //DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidExtraBxa;
  MovimNiv       := eminSemNiv;
  Pallet         := QrVSMovItsPallet.Value;
  GraGruX        := QrVSMovItsGraGruX.Value;
  FUltGGX        := GraGruX;
  Pecas          := -EdPecas.ValueVariant;
  PesoKg         := -EdPesoKg.ValueVariant;
  AreaM2         := -EdAreaM2.ValueVariant;
  AreaP2         := -EdAreaP2.ValueVariant;
  ValorT         := -EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  //
  SerieFch       := QrVSMovItsSerieFch.Value;
  Ficha          := QrVSMovItsFicha.Value;
  Marca          := QrVSMovItsMarca.Value;
  //Misturou       := QrVSMovItsMisturou.Value;
  //
  GraGruY        := QrGraGruXGraGruY.Value;
  //
  ExigeAreaouPeca :=
    (QrVSMovItsSdoVrtPeso.Value > 0) or (QrVSMovItsSdoVrtArM2.Value > 0);
  //
  if VS_PF.VSFic(GraGruX, Empresa, ClientMO, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca,
  nil) then
    Exit;
  //
  case RGDataHora.ItemIndex of
    1: DataHora := Geral.FDT(FDataHora, 109);
    2: DataHora := Geral.FDT(TPDataSenha.Date, 1) + ' ' + EdHoraSenha.Text;
    3:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLastMov, Dmod.MyDB, [
      'SELECT MAX(DataHora) DataHora ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE SrcNivel2=' + Geral.FF0(QrVSMovItsControle.Value),
      '']);
      if QrLastMovDataHora.Value > 2 then
        DataHora := DefineLastDtHr(QrLastMovDataHora.Value)
      else
        DataHora := Geral.FDT(FDataHora, 109);
    end;
    else
    begin
      Geral.MB_Erro('"Fonte de data / hora" n�o implementada!');
      DataHora := Geral.FDT(DmodG.ObtemAgora(), 109);
    end;
  end;
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
(*
  if Dmod.InsUpdVSMovIts_(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle) then
*)
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca,
  QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei059(*Baixa extra de couro na ribeira 1/2*)) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vsexbcab', MovimCod);
    VS_PF.AtualizaSaldoIMEI(FAntSrcNivel2, False);
    VS_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    FmVSExbCab.LocCod(Codigo, Codigo);
    ReopenVSExbIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;

      DBGVSMovIts.Enabled := True;
      //
      EdPecas.Enabled := False;
      EdAreaM2.Enabled := False;
      EdAreaP2.Enabled := False;
      EdPesoKg.Enabled := False;
      EdValorT.Enabled := False;
      //
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      EdValorT.ValueVariant      := 0;
      EdPsqPallet.ValueVariant   := 0;
      CBPsqPallet.KeyValue       := 0;
      EdPsqFicha.ValueVariant    := 0;
      EdObserv.Text              := '';
      EdSrcMovID.ValueVariant    := 0;
      EdSrcNivel1.ValueVariant   := 0;
      EdSrcNivel2.ValueVariant   := 0;
      EdSrcGGX.ValueVariant      := 0;
      //
      ReopenVSPallet();
      ReopenVSMovIts();
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmVSExBIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmVSExBIts.CalculaValorT(): Double;
begin
  if (QrVSMovItsSdoVrtArM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
    Result := QrVSMovItsSdoVrtArM2.Value *
    (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
  else
  if QrVSMovItsPecas.Value > 0 then
    Result := QrVSMovItsSdoVrtPeca.Value *
    (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value)
  else
    Result := 0.00;
  //
end;

procedure TFmVSExBIts.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSExBIts.CopiaTotaisIMEI;
begin
  EdPesoKg.ValueVariant := 0;
  EdAreaP2.ValueVariant := 0;
  // Deve ser depois dos zerados!
  EdPecas.ValueVariant  := QrVSMovItsSdoVrtPeca.Value;
  EdAreaM2.ValueVariant := QrVSMovItsSdoVrtArM2.Value;
  EdPesoKg.ValueVariant := QrVSMovItsSdoVrtPeso.Value;
  //
  EdValorT.ValueVariant := CalculaValorT();
end;

procedure TFmVSExBIts.DBGVSMovItsDblClick(Sender: TObject);
var
  Valor: Double;
begin
  DBGVSMovIts.Enabled := False;
  //
  EdSrcMovID.ValueVariant  := QrVSMovItsMovimID.Value;
  EdSrcNivel1.ValueVariant := QrVSMovItsCodigo.Value;
  EdSrcNivel2.ValueVariant := QrVSMovItsControle.Value;
  EdSrcGGX.ValueVariant    := QrVSMovItsGraGruX.Value;
  //
  EdPecas.Enabled := True;
  EdAreaM2.Enabled := True;
  EdAreaP2.Enabled := True;
  EdPesoKg.Enabled := True;
  EdValorT.Enabled := True;
  //
  try
    EdPecas.SetFocus;
  except
    //
  end;
end;

procedure TFmVSExBIts.EdPsqSerieFchRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

{
function TFmVSExbIts.DefineProximoPallet(): String;
var
  Txt: String;
begin
  Result := '';
  Txt := EdPallet.Text;
  if Geral.SoNumero_TT(Txt) = Txt then
  begin
    try
      Result := Geral.FI64(Geral.I64(EdPallet.Text) + 1);
    except
      Result := '';
    end;
  end else
    Result := '';
end;
}

procedure TFmVSExBIts.EdAreaM2Change(Sender: TObject);
begin
  EdValorT.ValueVariant := 0;
end;

procedure TFmVSExBIts.EdEmpresaRedefinido(Sender: TObject);
begin
  ReopenVSPallet();
end;

procedure TFmVSExBIts.EdFornecedorChange(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSExBIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSExBIts.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenVSPallet();
  ReopenVSMovIts();
  //
  DBGVSMovIts.Enabled := True;
  //
  EdPecas.Enabled := False;
  EdAreaM2.Enabled := False;
  EdAreaP2.Enabled := False;
  EdPesoKg.Enabled := False;
  EdValorT.Enabled := False;
  //
end;

procedure TFmVSExBIts.EdIMEIRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSExBIts.EdMaxPecasRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSExBIts.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaTotaisIMEI();
end;

procedure TFmVSExBIts.EdPecasRedefinido(Sender: TObject);
var
  Pecas: Double;
begin
  Pecas := EdPecas.ValueVariant;
  if Pecas > QrVSMovItsSdoVrtPeca.Value then
    EdPecas.ValueVariant := QrVSMovItsSdoVrtPeca.Value;
  if Pecas >= QrVSMovItsSdoVrtPeca.Value then
  begin
    EdAreaM2.ValueVariant := QrVSMovItsSdoVrtArM2.Value;
    EdPesoKg.ValueVariant := QrVSMovItsSdoVrtPeso.Value;
    EdValorT.ValueVariant := CalculaValorT();
    EdAreaM2.Enabled := False;
    EdAreaP2.Enabled := False;
    EdPesoKg.Enabled := False;
    EdValorT.Enabled := False;
  end else
  begin
    EdAreaM2.Enabled := True;
    EdAreaP2.Enabled := True;
    EdPesoKg.Enabled := True;
    EdValorT.Enabled := True;
  end;
end;

procedure TFmVSExBIts.EdPsqFichaRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSExBIts.EdPsqPalletRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSExBIts.EdSrcMovIDChange(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSExBIts.EdSrcNivel1Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSExBIts.EdSrcNivel2Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSExBIts.EdValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Pecas, AreaM2, Valor: Double;
begin
  if Key = VK_F5 then
  begin
    Pecas  := EdPecas.ValueVariant;
    AreaM2 := EdAreaM2.ValueVariant;
    Valor := 0;
    (*
    case TEstqNivCtrl(Dmod.QrControleVSNivGer.Value) of
      encPecas: ;//Valor := 0;
      encArea: ;//Valor := 0;
      encValor:
      begin
    *)
        if (QrVSMovItsAreaM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
          Valor := AreaM2 * (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
        else
        if QrVSMovItsPecas.Value > 0 then
          Valor := Pecas * (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value);
    (*
      end;
      else
      begin
        //Valor := 0;
        Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [1]');
      end;
    end;
    *)
    EdValorT.ValueVariant := Valor;
  end;
end;

procedure TFmVSExBIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSExBIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  //
  VS_PF.AbreVSSerFch(QrVSSerFch);
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' + CO_GraGruY_ALL_VS + ') ');
end;

procedure TFmVSExBIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSExBIts.HabilitaInclusao();
var
  Habilita: Boolean;
begin
  Habilita :=
    //(EdSrcMovID.ValueVariant <> 0) and  MovimID = 13 >> Inventario
    (EdSrcNivel1.ValueVariant <> 0) and
    (EdSrcNivel2.ValueVariant <> 0);
  //
  BtOK.Enabled := Habilita;
end;

procedure TFmVSExBIts.ImgTipoChange(Sender: TObject);
begin
  BtExclui.Visible := ImgTipo.SQLType = stUpd;
end;

procedure TFmVSExBIts.ReopenVSMovIts();
var
  Controle, GraGruX, Empresa, Pallet, SerieFch, Ficha, MaxPecas: Integer;
  SQL_Controle, SQL_Pallet, SQL_SerieFch, SQL_Ficha, SQL_GraGruX, SQL_DataHora,
  Sinal: String;
begin
  MaxPecas := EdMaxPecas.ValueVariant;
  Empresa    := FEmpresa;
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o definida!') then
  begin
    Close;
    Exit;
  end;
  SQL_DataHora := 'AND vmi.DataHora <= "' + Geral.FDT(FDataHora, 109) + '"';
  SQL_Pallet := '';
  SQL_Ficha  := '';
  Pallet     := EdPsqPallet.ValueVariant;
  Ficha      := EdPsqFicha.ValueVariant;
  GraGruX    := EdGraGruX.ValueVariant;
  Controle   := EdIMEI.ValueVariant;
  if Pallet <> 0 then
    SQL_Pallet := 'AND vmi.Pallet=' + Geral.FF0(Pallet);
  if SerieFch <> 0 then
    SQL_SerieFch  := 'AND vmi.SerieFch=' + Geral.FF0(SerieFch);
  if Ficha <> 0 then
    SQL_Ficha  := 'AND vmi.Ficha=' + Geral.FF0(Ficha);
  if GraGruX <> 0 then
    SQL_GraGruX  := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := '';
  if Controle <> 0 then
    SQL_Controle  := 'AND vmi.Controle=' + Geral.FF0(Controle)
  else
    SQL_Controle := '';
  if RGPosiNega.ItemIndex = 0 then
    Sinal := '>'
  else
    Sinal := '<';
  //
  //
  //
  //DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Empresa := FEmpresa;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT pal.Nome NO_Pallet, vmi.*,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vspalleta pal ON pal.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=vmi.Terceiro',
  'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
  SQL_GraGruX,
(*
  'AND ( ',
  // Ver no futuro se libera Artigos nao classificados tambem!!
  '  vmi.MovimID=' + Geral.FF0(Integer(emidExtraBxa)),
  '  OR  ',
  '  vmi.SrcMovID <> 0 ',
  ') ',
*)
  'AND (vmi.SdoVrtPeca ' + Sinal + ' 0 ',
  // Precisa? Nao!!!! tem saldos negativos!!!!
  //'OR vmi.SdoVrtArM2 > 0 ',
  ') ',
  'AND vmi.SdoVrtPeca <= ' + Geral.FF0(MaxPecas),
  SQL_Controle,
  SQL_Pallet,
  SQL_SerieFch,
  SQL_Ficha,
  SQL_DataHora,
  'ORDER BY DataHora, Pallet, Ficha ',
  '']);
  //
end;

procedure TFmVSExBIts.ReopenVSExbIts(Controle: Integer);
begin
  if (FQrIts <> nil)
  and (FQrCab <> nil) then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSExBIts.ReopenVSPallet();
var
  Empresa, GraGruX: Integer;
  SQL_GraGruX, Sinal: String;
begin
  Empresa    := FEmpresa;
  GraGruX := EdGraGruX.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o definida!') then
  begin
    Close;
    Exit;
  end;
  if GraGruX <> 0 then
    SQL_GraGruX  := '  AND vmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := '';
  if RGPosiNega.ItemIndex = 0 then
    Sinal := '>'
  else
    Sinal := '<';
  //
  //DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Empresa := FEmpresa;
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT Codigo, Nome  ',
  'FROM vspalleta ',
  'WHERE Codigo IN ( ',
  '  SELECT DISTINCT vmi.Pallet  ',
  '  FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  '  WHERE vmi.Pecas >=0 ',
  '  AND vmi.SdoVrtPeca ' + Sinal + ' 0 ',
  '  AND vmi.Empresa=' + Geral.FF0(Empresa),
  //'  AND vmi.GraGruX=' + Geral.FF0(GraGruX),
  SQL_GraGruX,
  ') ',
  'ORDER BY Nome ',
  ' ']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT pal.Codigo, pal.Nome ',
  'FROM vspalleta pal, ' + CO_SEL_TAB_VMI + ' vmi ',
  'WHERE pal.Codigo=vmi.Pallet ',
  'AND (',
  '  (vmi.Pecas>=0  AND vmi.SdoVrtPeca>0 ) ',
  '  OR',
  '  (vmi.PesoKg>0 AND vmi.SdoVrtPeso>0 )',
  ')',
  'AND vmi.Empresa=' + Geral.FF0(Empresa),
  SQL_GraGruX,
  'ORDER BY Nome ',
  '']);
  //Geral.MB_SQL(Self, QrVSPallet);
end;

procedure TFmVSExBIts.RGPosiNegaClick(Sender: TObject);
begin
  ReopenVSPallet();
  ReopenVSMovIts();
end;

procedure TFmVSExBIts.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_PF.MostraFormVSPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPsqPallet, CBPsqPallet, QrVSPallet, VAR_CADASTRO);
end;

procedure TFmVSExBIts.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
