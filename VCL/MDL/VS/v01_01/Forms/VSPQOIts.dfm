object FmVSPQOIts: TFmVSPQOIts
  Left = 339
  Top = 185
  Caption = 'QUI-RECEI-018 :: Item de Outras Emiss'#245'es de Insumos em VS'
  ClientHeight = 387
  ClientWidth = 796
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 796
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 748
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 700
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 544
        Height = 32
        Caption = 'Item de Outras Emiss'#245'es de Insumos em VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 544
        Height = 32
        Caption = 'Item de Outras Emiss'#245'es de Insumos em VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 544
        Height = 32
        Caption = 'Item de Outras Emiss'#245'es de Insumos em VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 317
    Width = 796
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 650
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 648
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 273
    Width = 796
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 792
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 796
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 4
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object EdOriCodi: TdmkEdit
      Left = 12
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnInsumos: TPanel
    Left = 0
    Top = 112
    Width = 796
    Height = 129
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label10: TLabel
      Left = 324
      Top = 4
      Width = 130
      Height = 13
      Caption = 'Material de uso e consumo:'
    end
    object Label12: TLabel
      Left = 4
      Top = 4
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object Label13: TLabel
      Left = 684
      Top = 4
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label17: TLabel
      Left = 324
      Top = 44
      Width = 98
      Height = 13
      Caption = 'Estoque quantidade:'
      FocusControl = DBEdit7
    end
    object Label18: TLabel
      Left = 432
      Top = 44
      Width = 51
      Height = 13
      Caption = 'Estoque $:'
      FocusControl = DBEdit8
    end
    object Label19: TLabel
      Left = 540
      Top = 44
      Width = 76
      Height = 13
      Caption = 'Custo unit'#225'rio $:'
      FocusControl = DBEdit9
    end
    object Label20: TLabel
      Left = 684
      Top = 44
      Width = 93
      Height = 13
      Caption = 'Saldo futuro quant.:'
    end
    object Label21: TLabel
      Left = 4
      Top = 84
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object EdPQ: TdmkEditCB
      Left = 324
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdPQChange
      DBLookupComboBox = CBPQ
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPQ: TdmkDBLookupComboBox
      Left = 380
      Top = 20
      Width = 301
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPQ
      TabOrder = 3
      dmkEditCB = EdPQ
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCI: TdmkEditCB
      Left = 4
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdCIChange
      DBLookupComboBox = CBCI
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCI: TdmkDBLookupComboBox
      Left = 60
      Top = 20
      Width = 261
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMECI'
      ListSource = DsCI
      TabOrder = 1
      dmkEditCB = EdCI
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPesoAdd: TdmkEdit
      Left = 684
      Top = 20
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdPesoAddExit
      OnRedefinido = EdPesoAddRedefinido
    end
    object DBEdit7: TDBEdit
      Left = 324
      Top = 60
      Width = 100
      Height = 21
      TabStop = False
      DataField = 'Peso'
      DataSource = DsSaldo
      TabOrder = 5
    end
    object DBEdit8: TDBEdit
      Left = 432
      Top = 60
      Width = 100
      Height = 21
      TabStop = False
      DataField = 'Valor'
      DataSource = DsSaldo
      TabOrder = 6
    end
    object DBEdit9: TDBEdit
      Left = 540
      Top = 60
      Width = 100
      Height = 21
      TabStop = False
      DataField = 'CUSTO'
      DataSource = DsSaldo
      TabOrder = 7
    end
    object EdSaldoFut: TdmkEdit
      Left = 684
      Top = 60
      Width = 100
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object CkDtCorrApo: TCheckBox
      Left = 4
      Top = 64
      Width = 185
      Height = 17
      Caption = #201' corre'#231#227'o de apontamento. Data:'
      TabOrder = 9
      OnClick = CkDtCorrApoClick
    end
    object TPDtCorrApo: TdmkEditDateTimePicker
      Left = 192
      Top = 60
      Width = 129
      Height = 21
      Time = 0.833253726850671200
      Enabled = False
      TabOrder = 10
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpSPED_EFD_MIN_MAX
    end
    object EdEmpresa: TdmkEditCB
      Left = 4
      Top = 100
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Empresa'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 60
      Top = 100
      Width = 437
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 12
      TabStop = False
      dmkEditCB = EdEmpresa
      QryCampo = 'Empresa'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 241
    Width = 796
    Height = 32
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 5
    object CkContinuar: TCheckBox
      Left = 4
      Top = 8
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM enticargos'
      'ORDER BY Nome')
    Left = 216
    Top = 44
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 216
    Top = 92
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 672
    Top = 8
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq '
      'WHERE Ativo=1'
      'AND GGXNiv2 IN (-1,-2,-4, -10, 1, 2, 4, 10)'
      'ORDER BY Nome')
    Left = 612
    Top = 12
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 612
    Top = 56
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 672
    Top = 60
  end
  object QrSaldo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CI, PQ, Peso, Valor, (Valor/Peso) CUSTO '
      'FROM pqcli'
      'WHERE CI=:P0'
      'AND PQ=:P1')
    Left = 728
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSaldoPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSaldoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSaldoCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
    object QrSaldoCI: TIntegerField
      FieldName = 'CI'
    end
    object QrSaldoPQ: TIntegerField
      FieldName = 'PQ'
    end
  end
  object DsSaldo: TDataSource
    DataSet = QrSaldo
    Left = 728
    Top = 56
  end
end
