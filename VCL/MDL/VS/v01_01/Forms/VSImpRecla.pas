unit VSImpRecla;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  // Chart
  frxChart, VCLTee.Chart, VCLTee.Series,
  // Fim Chart
  frxClass, frxDBSet, dmkDBLookupComboBox, dmkEditCB, UnGrl_Geral;

type
  TFmVSImpRecla = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrVSCacIts: TmySQLQuery;
    QrVSCacItsGraGruX: TIntegerField;
    QrVSCacItsPecas: TFloatField;
    QrVSCacItsAreaM2: TFloatField;
    QrVSCacItsAreaP2: TFloatField;
    QrVSCacItsGraGru1: TIntegerField;
    QrVSCacItsNO_PRD_TAM_COR: TWideStringField;
    frxWET_CURTI_048_001: TfrxReport;
    frxDsVSCacIts: TfrxDBDataset;
    QrVSCacSum: TmySQLQuery;
    QrVSCacItsPercM2: TFloatField;
    QrVSCacItsPercPc: TFloatField;
    QrVSCacSumPecas: TFloatField;
    QrVSCacSumAreaM2: TFloatField;
    QrVSCacSumAreaP2: TFloatField;
    QrVSGerRcl: TmySQLQuery;
    QrVSGerRclNO_TIPO: TWideStringField;
    QrVSGerRclNO_DtHrFimCla: TWideStringField;
    QrVSGerRclNO_DtHrLibCla: TWideStringField;
    QrVSGerRclNO_DtHrCfgCla: TWideStringField;
    QrVSGerRclCodigo: TIntegerField;
    QrVSGerRclMovimCod: TIntegerField;
    QrVSGerRclCacCod: TIntegerField;
    QrVSGerRclGraGruX: TIntegerField;
    QrVSGerRclVSPallet: TIntegerField;
    QrVSGerRclNome: TWideStringField;
    QrVSGerRclEmpresa: TIntegerField;
    QrVSGerRclDtHrLibCla: TDateTimeField;
    QrVSGerRclDtHrCfgCla: TDateTimeField;
    QrVSGerRclDtHrFimCla: TDateTimeField;
    QrVSGerRclTipoArea: TSmallintField;
    QrVSGerRclLk: TIntegerField;
    QrVSGerRclDataCad: TDateField;
    QrVSGerRclDataAlt: TDateField;
    QrVSGerRclUserCad: TIntegerField;
    QrVSGerRclUserAlt: TIntegerField;
    QrVSGerRclAlterWeb: TSmallintField;
    QrVSGerRclAtivo: TSmallintField;
    QrVSGerRclNO_EMPRESA: TWideStringField;
    QrVSGerRclNO_PRD_TAM_COR: TWideStringField;
    QrVSGerRclNO_PALLET: TWideStringField;
    QrVSCacItsMediaM2PC: TFloatField;
    QrVSCacItsAgrupaTudo: TFloatField;
    QrSubClas: TmySQLQuery;
    QrSubClasAgrupaTudo: TFloatField;
    QrSubClasGraGruX: TIntegerField;
    QrSubClasPecas: TFloatField;
    QrSubClasAreaM2: TFloatField;
    QrSubClasAreaP2: TFloatField;
    QrSubClasMediaM2PC: TFloatField;
    QrSubClasGraGru1: TIntegerField;
    QrSubClasSubClass: TWideStringField;
    QrSubClasNO_PRD_TAM_COR: TWideStringField;
    QrSubClasPercM2: TFloatField;
    QrSubClasPercPc: TFloatField;
    frxDsSubClas: TfrxDBDataset;
    frxWET_CURTI_048_002: TfrxReport;
    QrSubAll: TmySQLQuery;
    frxDsSubAll: TfrxDBDataset;
    QrSubAllAgrupaTudo: TFloatField;
    QrSubAllGraGruX: TIntegerField;
    QrSubAllPecas: TFloatField;
    QrSubAllAreaM2: TFloatField;
    QrSubAllAreaP2: TFloatField;
    QrSubAllMediaM2PC: TFloatField;
    QrSubAllGraGru1: TIntegerField;
    QrSubAllSubClass: TWideStringField;
    QrSubAllNO_PRD_TAM_COR: TWideStringField;
    QrSubAllPercPc: TFloatField;
    QrSubAllPercM2: TFloatField;
    Panel5: TPanel;
    Label1: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    DsVSCacIts: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_048_001GetValue(const VarName: string;
      var Value: Variant);
    procedure QrVSCacItsCalcFields(DataSet: TDataSet);
    procedure QrVSCacItsBeforeClose(DataSet: TDataSet);
    procedure QrVSCacItsAfterScroll(DataSet: TDataSet);
    procedure QrSubClasCalcFields(DataSet: TDataSet);
    procedure QrSubAllCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FTitulo: String;
    FReopenSub: Boolean;
    //
    procedure MostraReport001();
    procedure MostraReport002(MostraTotais: Boolean);
    procedure ReopenSubClas();
  public
    { Public declarations }
    FOC: Integer;
    //
    procedure ImprimeReclassOC(OC, CacCod: Integer);
    procedure ImprimeReclassDesnate(Codigo: Integer);
    procedure ImprimeReclassDesnSubAll(Codigo: Integer);
  end;

  var
  FmVSImpRecla: TFmVSImpRecla;

implementation

uses UnMyObjects, Module, DmkDAC_PF,
  {$IfDef sAllVS}UnVS_PF,{$EndIf}
  ModuleGeral;

{$R *.DFM}

procedure TFmVSImpRecla.BtOKClick(Sender: TObject);
const
  MostraTotais = False;
var
  GraGruX: Integer;
begin
  {$IfDef sAllVS}
 //ImprimeReclassOC(EdOC.ValueVariant);
  GraGruX := EdGraGruX.ValueVariant;
  FTitulo := 'Resultado de SubClasses do Desnate ' + Geral.FF0(FOC);
  //
  FReopenSub := True;
  VS_PF.ReopenDesnate(FOC, GraGruX, QrVSCacIts, QrVSCacSum);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSubAll, Dmod.MyDB, [
  'SELECT 0.000 AgrupaTudo, pla.GraGruX, SUM(cia.Pecas) Pecas, ',
  'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, ',
  'IF(SUM(cia.Pecas) > 0, SUM(cia.AreaM2) / SUM(cia.Pecas), 0) MediaM2PC, ',
  'ggx.GraGru1, IF(cia.SubClass="", "?", cia.SubClass) SubClass, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vspalleta  pla ON pla.Codigo=cia.VSPallet ',
  'LEFT JOIN vsdsnsub   vds ON vds.SubClass=cia.SubClass',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vds.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vsdsnits vdi ON vdi.VSCacCod=cia.CacCod',
  'LEFT JOIN vsdsncab vdc ON vdc.Codigo=vdi.Codigo',
  'WHERE vdc.Codigo=' + Geral.FF0(FOC),
  'GROUP BY cia.SubClass ',
  '']);
  MostraReport002(MostraTotais);
  //
  VS_PF.ReopenDesnate(FOC, 0, QrVSCacIts, QrVSCacSum);
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSImpRecla.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpRecla.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpRecla.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FReopenSub := False;
end;

procedure TFmVSImpRecla.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpRecla.frxWET_CURTI_048_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_TITULO' then
    Value := FTitulo
  else
  if VarName ='VARF_PALLET' then
    Value := QrVSGerRclVSPallet.Value
  else
  if VarName ='VARF_IMEC' then
    Value := QrVSGerRclCodigo.Value
  else
end;

procedure TFmVSImpRecla.ImprimeReclassDesnate(Codigo: Integer);
const
  GraGruX = 0;
begin
  {$IfDef sAllVS}
  FTitulo := 'Resultado de Reclassificação do Desnate ' + Geral.FF0(Codigo);
  //
  VS_PF.ReopenDesnate(Codigo, GraGruX, QrVSCacIts, QrVSCacSum);
  //
  FReopenSub := False;
  MostraReport001();
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSImpRecla.ImprimeReclassDesnSubAll(Codigo: Integer);
const
  GraGruX = 0;
  MostraTotais = True;
begin
  {$IfDef sAllVS}
  FTitulo := 'Resultado de SubClasses do Desnate ' + Geral.FF0(Codigo);
  //
  FReopenSub := True;
  VS_PF.ReopenDesnate(Codigo, GraGruX, QrVSCacIts, QrVSCacSum);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSubAll, Dmod.MyDB, [
  'SELECT 0.000 AgrupaTudo, pla.GraGruX, SUM(cia.Pecas) Pecas, ',
  'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, ',
  'IF(SUM(cia.Pecas) > 0, SUM(cia.AreaM2) / SUM(cia.Pecas), 0) MediaM2PC, ',
  'ggx.GraGru1, IF(cia.SubClass="", "?", cia.SubClass) SubClass, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vspalleta  pla ON pla.Codigo=cia.VSPallet ',
  'LEFT JOIN vsdsnsub   vds ON vds.SubClass=cia.SubClass',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vds.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vsdsnits vdi ON vdi.VSCacCod=cia.CacCod',
  'LEFT JOIN vsdsncab vdc ON vdc.Codigo=vdi.Codigo',
  'WHERE vdc.Codigo=' + Geral.FF0(FOC),
  'GROUP BY cia.SubClass ',
  '']);
  MostraReport002(MostraTotais);
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSImpRecla.ImprimeReclassOC(OC, CacCod: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerRcl, Dmod.MyDB, [
  'SELECT vgr.*, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, pal.Nome NO_PALLET ',
  'FROM vsgerrcla vgr ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vgr.Empresa ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vgr.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN vspalleta  pal ON pal.Codigo=vgr.VSPallet  ',
  'WHERE vgr.Codigo=' + Geral.FF0(OC),
  '']);
  FTitulo := 'Resultado de Reclassificação do Pallet ' +
    Geral.FF0(QrVSGerRclVSPallet.Value) + ' - IME-C ' +
    Geral.FF0(QrVSGerRclCodigo.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacSum, Dmod.MyDB, [
  'SELECT SUM(cia.Pecas) Pecas, ',
  'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2 ',
  'FROM vscacitsa cia ',
  'WHERE cia.CacCod=' + Geral.FF0(CacCod),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacIts, Dmod.MyDB, [
  'SELECT 0.000 AgrupaTudo, pla.GraGruX, SUM(cia.Pecas) Pecas, ',
  'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, ',
  'IF(SUM(cia.Pecas) > 0, SUM(cia.AreaM2) / SUM(cia.Pecas), 0) MediaM2PC, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vspalleta pla ON pla.Codigo=cia.VSPallet ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE cia.CacCod=' + Geral.FF0(CacCod),
  'GROUP BY pla.GraGruX ',
  '']);
  //Geral.MB_SQL(Self, QrVSCacIts);
  //
  MostraReport001();
end;

procedure TFmVSImpRecla.MostraReport001();
var
  Chart1: TfrxChartView;
  PieSeries: TPieSeries;
begin
  MyObjects.frxDefineDataSets(frxWET_CURTI_048_001, [
  DModG.frxDsDono,
  frxDsVSCacIts
  ]);
  Chart1 := frxWET_CURTI_048_001.FindObject('ChartPie0') as TfrxChartView;
  Chart1.Chart.Legend.Visible := False;
  //Chart1.Chart.Legend.Alignment := laBottom;
  //Chart1.Chart.Legend.Shadow.Visible := False;
  Chart1.Chart.View3D := False;

  // do VCLTee.Chart.CustomChart:
  Chart1.Chart.Series[0].Marks.Font.Size := 6;
  Chart1.Chart.Series[0].Marks.Transparent := True;
  Chart1.Chart.Series[0].Marks.Shadow.Visible := False;
  PieSeries := TPieSeries(Chart1.Chart.Series[0]);
  PieSeries.Circled := True;



    // do frxChat
  //Chart1.SeriesData[0].TopN := 20;
  //Chart1.SeriesData[0].TopNCaption := FOutrosChart;
  //Chart1.SeriesData.Items[0].Circled := True;
  //
  MyObjects.frxMostra(frxWET_CURTI_048_001, 'Reclasses Geradas');
end;

procedure TFmVSImpRecla.MostraReport002(MostraTotais: Boolean);
var
  Chart1, Chart2: TfrxChartView;
  Page: TfrxReportPage;
  PieSeries: TPieSeries;
begin
  MyObjects.frxDefineDataSets(frxWET_CURTI_048_002, [
  DModG.frxDsDono,
  frxDsVSCacIts,
  frxDsSubAll,
  frxDsSubClas
  ]);

  Page := frxWET_CURTI_048_002.FindObject('Page2') as TfrxReportPage;
  Page.Visible := MostraTotais;

////////////////////////////////////////////////////////////////////////////////
///
  Chart1 := frxWET_CURTI_048_002.FindObject('ChartPie0') as TfrxChartView;
  Chart1.Chart.Legend.Visible := False;
  //Chart1.Chart.Legend.Alignment := laBottom;
  //Chart1.Chart.Legend.Shadow.Visible := False;
  Chart1.Chart.View3D := False;

  // do VCLTee.Chart.CustomChart:
  Chart1.Chart.Series[0].Marks.Font.Size := 6;
  Chart1.Chart.Series[0].Marks.Transparent := True;
  Chart1.Chart.Series[0].Marks.Shadow.Visible := False;
  PieSeries := TPieSeries(Chart1.Chart.Series[0]);
  PieSeries.Circled := True;



    // do frxChat
  //Chart1.SeriesData[0].TopN := 20;
  //Chart1.SeriesData[0].TopNCaption := FOutrosChart;
  //Chart1.SeriesData.Items[0].Circled := True;
  //

////////////////////////////////////////////////////////////////////////////////
///
  Chart2 := frxWET_CURTI_048_002.FindObject('ChartPie1') as TfrxChartView;
  Chart2.Chart.Legend.Visible := False;
  //Chart2.Chart.Legend.Alignment := laBottom;
  //Chart2.Chart.Legend.Shadow.Visible := False;
  Chart2.Chart.View3D := False;

  // do VCLTee.Chart.CustomChart:
  Chart2.Chart.Series[0].Marks.Font.Size := 6;
  Chart2.Chart.Series[0].Marks.Transparent := True;
  Chart2.Chart.Series[0].Marks.Shadow.Visible := False;
  PieSeries := TPieSeries(Chart2.Chart.Series[0]);
  PieSeries.Circled := True;



    // do frxChat
  //Chart2.SeriesData[0].TopN := 20;
  //Chart2.SeriesData[0].TopNCaption := FOutrosChart;
  //Chart2.SeriesData.Items[0].Circled := True;
  //

////////////////////////////////////////////////////////////////////////////////

  MyObjects.frxMostra(frxWET_CURTI_048_002, 'Sub Classes de Reclasses');
end;

procedure TFmVSImpRecla.QrSubAllCalcFields(DataSet: TDataSet);
begin
  if QrVSCacSumAreaM2.Value > 0 then
    QrSubAllPercM2.Value := QrSubAllAreaM2.Value / QrVSCacSumAreaM2.Value * 100
  else
    QrSubAllPercM2.Value := 0;
  //
  if QrVSCacSumPecas.Value > 0 then
    QrSubAllPercPc.Value := QrSubAllPecas.Value / QrVSCacSumPecas.Value * 100
  else
    QrSubAllPercPc.Value := 0;
end;

procedure TFmVSImpRecla.QrSubClasCalcFields(DataSet: TDataSet);
begin
  if QrVSCacItsAreaM2.Value > 0 then
    QrSubClasPercM2.Value := QrSubClasAreaM2.Value / QrVSCacItsAreaM2.Value * 100
  else
    QrSubClasPercM2.Value := 0;
  //
  if QrVSCacItsPecas.Value > 0 then
    QrSubClasPercPc.Value := QrSubClasPecas.Value / QrVSCacItsPecas.Value * 100
  else
    QrSubClasPercPc.Value := 0;
end;

procedure TFmVSImpRecla.QrVSCacItsAfterScroll(DataSet: TDataSet);
begin
  if FReopenSub then
    ReopenSubClas();
end;

procedure TFmVSImpRecla.QrVSCacItsBeforeClose(DataSet: TDataSet);
begin
  QrSubClas.Close;
end;

procedure TFmVSImpRecla.QrVSCacItsCalcFields(DataSet: TDataSet);
begin
  if QrVSCacSumAreaM2.Value > 0 then
    QrVSCacItsPercM2.Value := QrVSCacItsAreaM2.Value / QrVSCacSumAreaM2.Value * 100
  else
    QrVSCacItsPercM2.Value := 0;
  //
  if QrVSCacSumPecas.Value > 0 then
    QrVSCacItsPercPc.Value := QrVSCacItsPecas.Value / QrVSCacSumPecas.Value * 100
  else
    QrVSCacItsPercPc.Value := 0;
end;

procedure TFmVSImpRecla.ReopenSubClas();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSubClas, Dmod.MyDB, [
  'SELECT 0.000 AgrupaTudo, pla.GraGruX, SUM(cia.Pecas) Pecas, ',
  'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, ',
  'IF(SUM(cia.Pecas) > 0, SUM(cia.AreaM2) / SUM(cia.Pecas), 0) MediaM2PC, ',
  'ggx.GraGru1, IF(cia.SubClass="", "?", cia.SubClass) SubClass, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vspalleta  pla ON pla.Codigo=cia.VSPallet ',
  'LEFT JOIN vsdsnsub   vds ON vds.SubClass=cia.SubClass',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vds.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vsdsnits vdi ON vdi.VSCacCod=cia.CacCod',
  'LEFT JOIN vsdsncab vdc ON vdc.Codigo=vdi.Codigo',
  'WHERE vdc.Codigo=' + Geral.FF0(FOC),
  'AND pla.GraGruX=' + Geral.FF0(QrVSCacItsGraGruX.Value),
  'GROUP BY pla.GraGruX, cia.SubClass ',
  '']);
end;

end.
