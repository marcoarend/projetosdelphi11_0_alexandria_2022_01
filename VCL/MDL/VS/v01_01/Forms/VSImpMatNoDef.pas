unit VSImpMatNoDef;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, Data.DB, mySQLDbTables,
  frxDBSet;

type
  TFmVSImpMatNoDef = class(TForm)
    frxWET_CURTI_099_A: TfrxReport;
    QrGraGruXCou: TmySQLQuery;
    QrGraGruXCouGraGruY: TIntegerField;
    QrGraGruXCouNO_GGY: TWideStringField;
    QrGraGruXCouNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXCouGraGruX: TIntegerField;
    QrGraGruXCouCouNiv1: TIntegerField;
    QrGraGruXCouCouNiv2: TIntegerField;
    QrGraGruXCouArtigoImp: TWideStringField;
    QrGraGruXCouClasseImp: TWideStringField;
    QrGraGruXCouLk: TIntegerField;
    QrGraGruXCouDataCad: TDateField;
    QrGraGruXCouDataAlt: TDateField;
    QrGraGruXCouUserCad: TIntegerField;
    QrGraGruXCouUserAlt: TIntegerField;
    QrGraGruXCouAlterWeb: TSmallintField;
    QrGraGruXCouAtivo: TSmallintField;
    QrGraGruXCouPrevPcPal: TIntegerField;
    QrGraGruXCouMediaMinM2: TFloatField;
    QrGraGruXCouMediaMaxM2: TFloatField;
    frxDsGraGruXCou: TfrxDBDataset;
    procedure frxWET_CURTI_099_AGetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ImprimeRelatorio();
  end;

var
  FmVSImpMatNoDef: TFmVSImpMatNoDef;

implementation

{$R *.dfm}

uses Module, DmkDAC_PF, UnMyObjects, ModuleGeral;

{ TFmVSImpMatNoDef }

procedure TFmVSImpMatNoDef.frxWET_CURTI_099_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmVSImpMatNoDef.ImprimeRelatorio();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruXCou, Dmod.MyDB, [
  'SELECT ggx.GraGruY, ggy.Nome NO_GGY,  ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))   ',
  ') NO_PRD_TAM_COR, cou.*  ',
  'FROM gragruxcou cou  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=cou.GraGruX  ',
  'LEFT JOIN gragruy ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE CouNiv1=0  ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_099_A, [
    DModG.frxDsDono,
    frxDsGraGruXCou
  ]);
  MyObjects.frxMostra(frxWET_CURTI_099_A, 'Couros sem defini��o de material');
end;

end.
