unit VSImpKardex3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, AppListas, CreateVS, frxClass, frxDBSet;

type
  TFmVSImpKardex3 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSMovIts: TMySQLQuery;
    QrVSMovItsCodigo: TLargeintField;
    QrVSMovItsControle: TLargeintField;
    QrVSMovItsMovimCod: TLargeintField;
    QrVSMovItsMovimNiv: TLargeintField;
    QrVSMovItsMovimTwn: TLargeintField;
    QrVSMovItsEmpresa: TLargeintField;
    QrVSMovItsTerceiro: TLargeintField;
    QrVSMovItsCliVenda: TLargeintField;
    QrVSMovItsMovimID: TLargeintField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TLargeintField;
    QrVSMovItsGraGruX: TLargeintField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TLargeintField;
    QrVSMovItsSrcNivel1: TLargeintField;
    QrVSMovItsSrcNivel2: TLargeintField;
    QrVSMovItsSrcGGX: TLargeintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TLargeintField;
    QrVSMovItsFicha: TLargeintField;
    QrVSMovItsMisturou: TLargeintField;
    QrVSMovItsFornecMO: TLargeintField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOM2: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TLargeintField;
    QrVSMovItsDstNivel1: TLargeintField;
    QrVSMovItsDstNivel2: TLargeintField;
    QrVSMovItsDstGGX: TLargeintField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsNO_PALLET: TWideStringField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsNO_TTW: TWideStringField;
    QrVSMovItsID_TTW: TLargeintField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSMovItsReqMovEstq: TLargeintField;
    QrVSMovItsNO_MovimID: TWideStringField;
    QrVSMovItsMediaArM2: TFloatField;
    QrVSMovItsMediaPeso: TFloatField;
    DsVSMovIts: TDataSource;
    Panel5: TPanel;
    Label7: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXNO_GraGruY: TWideStringField;
    QrVSAnt: TMySQLQuery;
    QrVSAntPecas: TFloatField;
    QrVSAntPesoKg: TFloatField;
    QrVSAntAreaM2: TFloatField;
    QrVSAntAreaP2: TFloatField;
    QrVSAntVAlorT: TFloatField;
    QrVSAntSdoVrtPeca: TFloatField;
    QrVSAntSdoVrtPeso: TFloatField;
    QrVSAntSdoVrtArM2: TFloatField;
    QrVSAntSdoVrtArP2: TFloatField;
    DsVSAnt: TDataSource;
    dmkDBGridZTO2: TdmkDBGridZTO;
    QrVSAntCusM2: TFloatField;
    DsVSAtu: TDataSource;
    QrVSAtu: TMySQLQuery;
    PB1: TProgressBar;
    frxWET_CURTI_245_A_1: TfrxReport;
    frxDsVSAtu: TfrxDBDataset;
    frxDsVSAnt: TfrxDBDataset;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label2: TLabel;
    GroupBox3: TGroupBox;
    TP02DataIni: TdmkEditDateTimePicker;
    Ck02DataIni: TCheckBox;
    Ck02DataFim: TCheckBox;
    TP02DataFim: TdmkEditDateTimePicker;
    CkSomComPc: TCheckBox;
    QrVSAtuControle: TLargeintField;
    QrVSAtuDataHora: TDateTimeField;
    QrVSAtuPecasInn: TFloatField;
    QrVSAtuAreaM2Inn: TFloatField;
    QrVSAtuValorTInn: TFloatField;
    QrVSAtuCustoM2Inn: TFloatField;
    QrVSAtuPecasOut: TFloatField;
    QrVSAtuAreaM2Out: TFloatField;
    QrVSAtuValorTOut: TFloatField;
    QrVSAtuCustoM2Out: TFloatField;
    QrVSAtuAcumPeca: TFloatField;
    QrVSAtuAcumPeso: TFloatField;
    QrVSAtuAcumArM2: TFloatField;
    QrVSAtuAcumArP2: TFloatField;
    QrVSAtuAcumValorT: TFloatField;
    QrVSAtuAcumCusPc: TFloatField;
    QrVSAtuAcumCusKg: TFloatField;
    QrVSAtuAcumCusM2: TFloatField;
    QrVSAtuAcumCusP2: TFloatField;
    frxWET_CURTI_245_B_1: TfrxReport;
    QrVSAtuPesoKgInn: TFloatField;
    QrVSAtuPesoKgOut: TFloatField;
    QrVSAtuCustoKgInn: TFloatField;
    QrVSAtuCustoKgOut: TFloatField;
    QrVSAntCusKg: TFloatField;
    QrVSAtuNO_MovimID: TWideStringField;
    frxWET_CURTI_245_B_2: TfrxReport;
    CkProcesso: TCheckBox;
    frxWET_CURTI_245_A_2: TfrxReport;
    QrGraGruXGrandeza: TSmallintField;
    QrVSAtuCustoMP: TFloatField;
    QrVSAntCustoMP: TFloatField;
    QrVSMovItsCustoMP: TFloatField;
    QrCorda: TMySQLQuery;
    QrVSAtuOrdem: TLargeintField;
    QrVSAtuPalOri: TLargeintField;
    QrVSAtuIMEIOri: TLargeintField;
    QrVSAtuAcImeiPeca: TFloatField;
    QrVSAtuAcImeiPeso: TFloatField;
    QrVSAtuAcImeiArM2: TFloatField;
    QrVSAtuAcImeiArP2: TFloatField;
    QrVSAtuAcImeiValorT: TFloatField;
    QrVSAtuAcImeiCusPc: TFloatField;
    QrVSAtuAcImeiCusKg: TFloatField;
    QrVSAtuAcImeiCusM2: TFloatField;
    QrVSAtuAcImeiCusP2: TFloatField;
    QrVSMovItsIMEIOri: TLargeintField;
    QrVSMovItsPalOri: TLargeintField;
    CkDivergencias: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_245_A_1GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    //FVSKardexAnt,
    FVSKardexMov: String;
    FEmpresa, FGraGruX, FGraGruY, FGrandeza, FAtuIMEIOri: Integer;
    FDataI_Dta, FDtaAnt_Dta: TDateTime;
    FDataI_Txt: String;
    //
    function  DefineSQLMovimID(const Obrigatorio: Boolean; const EdMovimID:
              TdmkEdit; var SQL: String): Boolean;
    procedure PesquisaLancamentos();
    procedure AtualizaEvolucao();
  public
    { Public declarations }
  end;

  var
  FmVSImpKardex3: TFmVSImpKardex3;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_PF, ModuleGeral, UnGrl_Consts,
  UnDmkProcFunc, UMySQLModule, ModVS, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSImpKardex3.AtualizaEvolucao();
var
  AcumPeca, AcumPeso, AcumArM2, AcumArP2, AcumValorT, AcumCusPc, AcumCusKg,
  AcumCusM2, AcumCusP2, AcumSdoVrtPeca, AcumSDoVrtPeso, AcumSdoVrtArM2,
  AcumSdoVrtArP2, AcumSdoVrtValr,
  AcImeiPeca, AcImeiPeso, AcImeiArM2, AcImeiArP2, AcImeiValorT, AcImeiCusPc, AcImeiCusKg,
  AcImeiCusM2, AcImeiCusP2: Double;
  DataHora, SQL: String;
  Controle, ControleAnt: Integer;
begin
  DataHora := Geral.FDT(FDataI_Dta - 1, 1) + ' 23:59:59';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSAnt, DModG.MyPID_DB, [
  'SELECT SUM(Pecas) Pecas, ',
  'SUM(PesoKg) PesoKg, ',
  'SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(ValorT) ValorT, ',
  'SUM(CustoMP) CustoMP, ',
  'SUM(ValorT) / SUM(AreaM2) CusM2, ',
  'SUM(ValorT) / SUM(PesoKg) CusKg, ',
  'SUM(SdoVrtPeca) SdoVrtPeca, ',
  'SUM(SdoVrtPeso) SdoVrtPeso, ',
  'SUM(SdoVrtArM2) SdoVrtArM2, ',
  'SUM(SdoVrtArM2) SdoVrtArP2  ',
  'FROM _vs_cardex_mov_3 ',
  'WHERE DataHora < "' + FDataI_Txt + '"  ',
  ' ']);
  //Geral.MB_Teste(QrVSAnt.SQL.Text);
  //

  AcumPeca     := QrVSAntPecas.Value;
  AcumPeso     := QrVSAntPesoKg.Value;
  AcumArM2     := QrVSAntAreaM2.Value;
  AcumArP2     := QrVSAntAreaP2.Value;
  //AcumValorT   := QrVSAntValorT.Value;
  AcumValorT   := QrVSAntCustoMP.Value;

  //
  AcumSdoVrtPeca := QrVSAntSdoVrtPeca.Value;
  AcumSDoVrtPeso := QrVSAntSdoVrtPeso.Value;
  AcumSdoVrtArM2 := QrVSAntSdoVrtArM2.Value;
  AcumSdoVrtArP2 := QrVSAntSdoVrtArP2.Value;

  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, DModG.MyPID_DB, [
  'SELECT * FROM ' + FVSKardexMov,
  'WHERE DataHora > "' + DataHora + '"',
  //'ORDER BY DataHora, Controle ',
  'ORDER BY PalOri, IMEIOri, Ordem, Controle ',
  '']);
  PB1.Position := 0;
  PB1.Max := QrVSMovIts.RecordCount;
  QrVSMovIts.DisableControls;
  try
    QrVSMovIts.First;
    while not QrVSMovIts.Eof do
    begin
      Controle := QrVSMovItsControle.Value;
      PB1.Position := PB1.Position + 1;
      Application.ProcessMessages;
      // ///////////////////////////////////////////////////////////////////////
      AcumPeca     := AcumPeca   + QrVSMovItsPecas.Value;
      AcumPeso     := AcumPeso   + QrVSMovItsPesoKg.Value;
      AcumArM2     := AcumArM2   + QrVSMovItsAreaM2.Value;
      AcumArP2     := AcumArP2   + QrVSMovItsAreaP2.Value;
      //AcumValorT   := AcumValorT + QrVSMovItsValorT.Value;
      AcumValorT   := AcumValorT + QrVSMovItsCustoMP.Value;
      //
      if AcumPeca <> 0 then
        AcumCusPc    := AcumValorT / AcumPeca
      else
        AcumCusPc    := 0.00;

      if AcumPeso <> 0 then
        AcumCusKg    := AcumValorT / AcumPeso
      else
        AcumCusKg    := 0.00;

      if (ABS(AcumValorT) >= 0.0001) AND (ABS(AcumArM2) >= 0.01) then
        AcumCusM2    := AcumValorT / AcumArM2
      else
        AcumCusM2    := 0.00;

      if AcumArP2 <> 0 then
        AcumCusP2    := AcumValorT / AcumArP2
      else
        AcumCusP2    := 0.00;
      //
      // ///////////////////////////////////////////////////////////////////////
      if FAtuIMEIOri <> QrVSMovItsIMEIOri.Value then
      begin
        UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
        'UPDATE _vs_cardex_mov_3 SET ' +
        '  AcImeiPeca=' + Geral.FFT_Dot(AcImeiPeca, 2, siNegativo) +
        ', AcImeiPeso=' + Geral.FFT_Dot(AcImeiPeso, 2, siNegativo) +
        ', AcImeiArM2=' + Geral.FFT_Dot(AcImeiArM2, 2, siNegativo) +
        ', AcImeiArP2=' + Geral.FFT_Dot(AcImeiArP2, 2, siNegativo) +
        ', AcImeiValorT=' + Geral.FFT_Dot(AcImeiValorT, 2, siNegativo) +
        ', AcImeiCusPc=' + Geral.FFT_Dot(AcImeiCusPc, 2, siNegativo) +
        ', AcImeiCusKg=' + Geral.FFT_Dot(AcImeiCusKg, 2, siNegativo) +
        ', AcImeiCusM2=' + Geral.FFT_Dot(AcImeiCusM2, 4, siNegativo) +
        ', AcImeiCusP2=' + Geral.FFT_Dot(AcImeiCusP2, 4, siNegativo) +
        '  WHERE Controle=' + IntToStr(ControleAnt) +
        '');
        //
        AcImeiPeca   := 0.000;
        AcImeiPeso   := 0.000;
        AcImeiArM2   := 0.00;
        AcImeiArP2   := 0.00;
        AcImeiValorT := 0.0000;
        FAtuIMEIOri := QrVSMovItsControle.Value;
      end;
      AcImeiPeca     := AcImeiPeca   + QrVSMovItsPecas.Value;
      AcImeiPeso     := AcImeiPeso   + QrVSMovItsPesoKg.Value;
      AcImeiArM2     := AcImeiArM2   + QrVSMovItsAreaM2.Value;
      AcImeiArP2     := AcImeiArP2   + QrVSMovItsAreaP2.Value;
      //AcImeiValorT   := AcImeiValorT + QrVSMovItsValorT.Value;
      AcImeiValorT   := AcImeiValorT + QrVSMovItsCustoMP.Value;
      //
      if AcImeiPeca <> 0 then
        AcImeiCusPc    := AcImeiValorT / AcImeiPeca
      else
        AcImeiCusPc    := 0.00;

      if AcImeiPeso <> 0 then
        AcImeiCusKg    := AcImeiValorT / AcImeiPeso
      else
        AcImeiCusKg    := 0.00;

      if (ABS(AcImeiValorT) >= 0.0001) AND (ABS(AcImeiArM2) >= 0.01) then
        AcImeiCusM2    := AcImeiValorT / AcImeiArM2
      else
        AcImeiCusM2    := 0.00;

      if AcImeiArP2 <> 0 then
        AcImeiCusP2    := AcImeiValorT / AcImeiArP2
      else
        AcImeiCusP2    := 0.00;
      //
      UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
      'UPDATE _vs_cardex_mov_3 SET ' +
      '  AcumPeca=' + Geral.FFT_Dot(AcumPeca, 2, siNegativo) +
      ', AcumPeso=' + Geral.FFT_Dot(AcumPeso, 2, siNegativo) +
      ', AcumArM2=' + Geral.FFT_Dot(AcumArM2, 2, siNegativo) +
      ', AcumArP2=' + Geral.FFT_Dot(AcumArP2, 2, siNegativo) +
      ', AcumValorT=' + Geral.FFT_Dot(AcumValorT, 2, siNegativo) +
      ', AcumCusPc=' + Geral.FFT_Dot(AcumCusPc, 2, siNegativo) +
      ', AcumCusKg=' + Geral.FFT_Dot(AcumCusKg, 2, siNegativo) +
      ', AcumCusM2=' + Geral.FFT_Dot(AcumCusM2, 4, siNegativo) +
      ', AcumCusP2=' + Geral.FFT_Dot(AcumCusP2, 4, siNegativo) +
(*
      ', AcImeiPeca=' + Geral.FFT_Dot(AcImeiPeca, 2, siNegativo) +
      ', AcImeiPeso=' + Geral.FFT_Dot(AcImeiPeso, 2, siNegativo) +
      ', AcImeiArM2=' + Geral.FFT_Dot(AcImeiArM2, 2, siNegativo) +
      ', AcImeiArP2=' + Geral.FFT_Dot(AcImeiArP2, 2, siNegativo) +
      ', AcImeiValorT=' + Geral.FFT_Dot(AcImeiValorT, 2, siNegativo) +
      ', AcImeiCusPc=' + Geral.FFT_Dot(AcImeiCusPc, 2, siNegativo) +
      ', AcImeiCusKg=' + Geral.FFT_Dot(AcImeiCusKg, 2, siNegativo) +
      ', AcImeiCusM2=' + Geral.FFT_Dot(AcImeiCusM2, 4, siNegativo) +
      ', AcImeiCusP2=' + Geral.FFT_Dot(AcImeiCusP2, 4, siNegativo) +
*)
      '  WHERE Controle=' + IntToStr(Controle) +
      '');
      //
      ControleAnt := Controle;
      QrVSMovIts.Next;
    end;
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
    'UPDATE _vs_cardex_mov_3 SET ' +
    '  AcImeiPeca=' + Geral.FFT_Dot(AcImeiPeca, 2, siNegativo) +
    ', AcImeiPeso=' + Geral.FFT_Dot(AcImeiPeso, 2, siNegativo) +
    ', AcImeiArM2=' + Geral.FFT_Dot(AcImeiArM2, 2, siNegativo) +
    ', AcImeiArP2=' + Geral.FFT_Dot(AcImeiArP2, 2, siNegativo) +
    ', AcImeiValorT=' + Geral.FFT_Dot(AcImeiValorT, 2, siNegativo) +
    ', AcImeiCusPc=' + Geral.FFT_Dot(AcImeiCusPc, 2, siNegativo) +
    ', AcImeiCusKg=' + Geral.FFT_Dot(AcImeiCusKg, 2, siNegativo) +
    ', AcImeiCusM2=' + Geral.FFT_Dot(AcImeiCusM2, 4, siNegativo) +
    ', AcImeiCusP2=' + Geral.FFT_Dot(AcImeiCusP2, 4, siNegativo) +
    '  WHERE Controle=' + IntToStr(ControleAnt) +
    '');
    QrVSMovIts.First;
  finally
    QrVSMovIts.EnableControls;
  end;
  //
end;

procedure TFmVSImpKardex3.BtOKClick(Sender: TObject);
const
  TemIMEiMrt = 0;
  MargemErro = 0.01;
  Avisa = True;
  ForcaMostrarForm = False;
  SelfCall = False;
var
  SQL_SomComPc, ATT_MovimID: String;
  Report: TfrxReport;
  GGY_OK: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
  FAtuIMEIOri := 0;
  FEmpresa := EdEmpresa.ValueVariant;
  if Ck02DataIni.Checked then
    FDataI_Dta  := Int(TP02DataIni.Date)
  else
    FDataI_Dta  := 0;
  //
  FDtaAnt_Dta := FDataI_Dta - 1;
  FDataI_Txt  := Geral.FDT(FDataI_Dta, 1);
  FGraGruX     := EdGraGruX.ValueVariant;
  FGraGruY     := QrGraGruXGraGruY.Value;
  FGrandeza    := QrGraGruXGrandeza.Value;
  //
  if MyObjects.FIC(FGraGruX = 0, EdGraGruX, 'Informe o artigo!') then
    Exit;
  GGY_OK := (FGraGruY = 1024) or (FGraGruY = 2048) or (FGraGruY = 3072);
  if MyObjects.FIC(GGY_OK = False, EdGraGruX,
  'Somente artigos "Artigo de Ribeira" est�o habilitados para este relat�rio. Para outros artigos solicite � DERMATEK!') then
   ;// Exit;
  //
  //
  if CkDivergencias.Checked then
  begin
    DmModVS.ZeraValoMP_de_MovimID_2();
    if DmModVS.IMEIsComCamposPositivosENegativosNoMesmoRegistro(TemIMEIMrt, Avisa,
      ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
    if DmModVS.RedConfltGerA(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2) then Exit;
    if DmModVS.RedConfltBaixa(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2) then Exit;
    if DmModVS.DstGGXDiferentesDeGraGruX(LaAviso1, LaAviso2) then Exit;
    if DmModVS.SrcGGXDiferentesDePreClasseParaClasseEReclasse(False, False, False, LaAviso1, LaAviso2) then Exit;
    if DmModVS.CustosDiferentesOrigemParaDestino2(FEmpresa, FGraGruX, MargemErro,
      False, False, False, TemIMEiMrt, LaAviso1, LaAviso2) then Exit;
    if VS_PF.RegistrosComProblema(FEmpresa, FGraGruX, LaAviso1, LaAviso2) then Exit;
    if DmModVS.CustosDifPreRecl(FEmpresa, FGraGruX, MargemErro,
      Avisa, ForcaMostrarForm, SelfCall, TemIMEiMrt, nil, nil) then Exit;
    if DmModVS.CustosDifReclasse(FEmpresa, FGraGruX, MargemErro,
      Avisa, ForcaMostrarForm, SelfCall, TemIMEiMrt, nil, nil) then Exit;
    if DmModVS.VSPcPosNegXValTNegPos(FEmpresa, FGraGruX, TemIMEIMrt, MargemErro,
      Avisa, ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
    if DmModVS.VmiPaiDifProcessos(ForcaMostrarForm, SelfCall, TemIMEIMrt, nil, nil) then Exit;
    if DmModVS.CustosDiferentesSaldosVirtuais3(FEmpresa, FGraGruX, TemIMEIMrt,
       0.0005, Avisa, ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
 end;
  //
 //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando pesquisa');
  //FVSKardexAnt := UnCreateVS.RecriaTempTableNovo(ntrttVSKardex, DModG.QrUpdPID1, False, 1, '_vs_Kardex_ant');
  FVSKardexMov := UnCreateVS.RecriaTempTableNovo(ntrttVSKardex3, DModG.QrUpdPID1, False, 1, '_vs_cardex_mov_3');

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados');
  PesquisaLancamentos();

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando acumulados');
  AtualizaEvolucao();

  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
  //
  if CkSomComPc.Checked then
    SQL_SomComPc := 'AND Pecas <> 0'
  else
    SQL_SomComPc := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSAnt, DModG.MyPID_DB, [
  'SELECT SUM(Pecas) Pecas, ',
  'SUM(PesoKg) PesoKg, ',
  'SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'SUM(ValorT) ValorT, ',
  'SUM(CustoMP) CustoMP, ',
  'SUM(ValorT) / SUM(AreaM2) CusM2, ',
  'SUM(ValorT) / SUM(PesoKg) CusKg, ',
  'SUM(SdoVrtPeca) SdoVrtPeca, ',
  'SUM(SdoVrtPeso) SdoVrtPeso, ',
  'SUM(SdoVrtArM2) SdoVrtArM2, ',
  'SUM(SdoVrtArM2) SdoVrtArP2  ',
  'FROM _vs_cardex_mov_3 ',
  'WHERE DataHora < "' + FDataI_Txt + '"  ',
  ' ']);
  //
  //
  ATT_MovimID := dmkPF.ArrayToTexto('MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID_FRENDLY);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSAtu, DModG.MyPID_DB, [
  'SELECT Controle, DataHora,',
  ATT_MovimID,
  'IF(Pecas > 0, Pecas, 0) PecasInn,',
  'IF(Pecas > 0, PesoKg, 0) PesoKgInn,',
  'IF(Pecas > 0, AreaM2, 0) AreaM2Inn,',
  'IF(Pecas > 0, ValorT, 0) ValorTInn,',
  'IF(Pecas > 0, ',
  '  IF(PesoKg > 0, ValorT / PesoKg, 0), ',
  '0) CustoKgInn,',
  'IF(Pecas > 0, ',
  '  IF(AreaM2 > 0, ValorT / AreaM2, 0), ',
  '0) CustoM2Inn,',
  'IF(Pecas < 0, -Pecas, 0) PecasOut,',
  'IF(Pecas < 0, -PesoKg, 0) PesoKgOut,',
  'IF(Pecas < 0, -AreaM2, 0) AreaM2Out,',
  'IF(Pecas < 0, -ValorT, 0) ValorTOut,',
  'IF(Pecas < 0, ',
  '  IF(PesoKg < 0, ValorT / PesoKg, 0), ',
  '0) CustoKgOut,',
  'IF(Pecas < 0, ',
  '  IF(AreaM2 < 0, ValorT / AreaM2, 0), ',
  '0) CustoM2Out,',
  'CustoMP, ',
  'AcumPeca, AcumPeso,  AcumArM2, ',
  'AcumArP2, AcumValorT, AcumCusPc, ',
  'AcumCusKg, AcumCusM2, AcumCusP2,',
  'AcImeiPeca, AcImeiPeso,  AcImeiArM2, ',
  'AcImeiArP2, AcImeiValorT, AcImeiCusPc, ',
  'AcImeiCusKg, AcImeiCusM2, AcImeiCusP2,',
  'PalOri, IMEIOri, Ordem ',
  'FROM ' + FVSKardexMov,
  'WHERE DataHora >= "' + FDataI_Txt + '"  ',
  SQL_SomComPc,
  //'ORDER BY DataHora, Controle ',
  'ORDER BY PalOri, IMEIOri, Ordem, Controle ',
  '']);
  //
  if FGrandeza = 1  then // m2
  begin
    if CkProcesso.Checked then
      Report := frxWET_CURTI_245_B_2
    else
      Report := frxWET_CURTI_245_B_1;
  end else
  begin
    if CkProcesso.Checked then
      Report := frxWET_CURTI_245_A_2
    else
      Report := frxWET_CURTI_245_A_1;
  end;
  //
  MyObjects.frxDefineDataSets(Report, [
    DModG.frxDsDono,
    frxDsVSAnt,
    frxDsVSAtu
    ]);
  MyObjects.frxMostra(Report, 'Ficha Kardex');
end;

procedure TFmVSImpKardex3.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmVSImpKardex3.DefineSQLMovimID(const Obrigatorio: Boolean;
  const EdMovimID: TdmkEdit; var SQL: String): Boolean;
var
  MovimID: Integer;
begin
  Result := False;
  SQL := '';
  MovimID := EdMovimID.ValueVariant;
  if MyObjects.FIC((MovimID < 0) and Obrigatorio, EdMovimID,
  'Informe o ID do movimento!') then
    Exit;
  if MovimID > -1 then
    SQL := 'AND vmi.MovimID=' + Geral.FF0(MovimID);
  Result := True;
end;

procedure TFmVSImpKardex3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpKardex3.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TP02DataIni.Date := Trunc(DModG.ObtemAgora()) - 30;
  TP02DataFim.Date := DModG.ObtemAgora();

  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa, DModG.QrEmpresas, 'Codigo');
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  //
end;

procedure TFmVSImpKardex3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpKardex3.frxWET_CURTI_245_A_1GetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_ARTIGO' then
    Value := dmkPF.ParValueCodTxt(
      'Artigo: ', CBGraGruX.Text, EdGraGruX.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_ANT' then
    Value := Geral.FDT(FDtaAnt_Dta + (1439/1440), 107)
end;

procedure TFmVSImpKardex3.PesquisaLancamentos();
  function SQL_LJ_GGX: String;
  begin
    Result := Geral.ATS([
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
  end;
var
  SQL_Periodo, SQL_GraGruX, ATT_MovimID, SQL_MovimID: String;
  GraGruX, Pallet: Integer;
  //
  DataHora, TxtItem: String;
  AcumPecas, AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT: Double;
  Controle: Integer;
  //
  procedure GeraVSKardex();
  const
    TemIMEIMrt = 1;
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_Artigo, SQL, Corda: String;
  begin
    SQL_Artigo := '';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de entrada');
    SQL_Flds := Geral.ATS([
   '1 Ordem, Pallet PalOri, Controle IMEIOri, Controle ',
    '']);
    SQL_Left := '';
    SQL_Wher := Geral.ATS([
    'WHERE (Pecas + PesoKg) > 0 ',
    'AND (NOT (MovimID IN (9,17,41))) ',
    'AND Empresa=' + Geral.FF0(FEmpresa),
    SQL_GraGruX,
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _vs_cardex_imeis_1; ',
    'CREATE TABLE _vs_cardex_imeis_1 ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ';',
    'SELECT DISTINCT Controle FROM _vs_cardex_imeis_1',
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, DModG.MyPID_DB, SQL);
    Corda := MyObjects.CordaDeQuery(QrCorda, 'Controle', '-999999999');
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados de baixa');
    SQL_Flds := Geral.ATS([
    '2 Ordem, ime.PalOri, vmi.SrcNivel2 IMEIOri, vmi.Controle ',
    '']);
    SQL_Left := 'LEFT JOIN _vs_cardex_imeis_1 ime ON ime.Controle=vmi.SrcNivel2';
    SQL_Wher := Geral.ATS([
    'WHERE  ',
    '  ( ',
    '    (SrcNivel2 IN ( ' + Corda + '    ))  ',
    (*'    OR  ',
    '    (MovimID IN (9,17,41)) ',*)
    '  ) ',
    'AND Empresa=' + Geral.FF0(FEmpresa),
    SQL_GraGruX,
    '']);
    SQL_Group := '';
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _vs_cardex_imeis_2; ',
    'CREATE TABLE _vs_cardex_imeis_2 ',
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_CRC_PF.GeraSQLVSMovItx_Base(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Unindo dados de entrada e baixa');
    SQL := Geral.ATS([
    'DROP TABLE IF EXISTS _vs_cardex_imeis_3; ',
    'CREATE TABLE _vs_cardex_imeis_3 ',
    'SELECT Ordem, PalOri, IMEIOri, Controle FROM _vs_cardex_imeis_1 ',
    'UNION ',
    'SELECT Ordem, PalOri, IMEIOri, Controle FROM _vs_cardex_imeis_2 ',
    '; ',
    //'SELECT Ordem, IMEIOri, Controle  ',
    'SELECT DISTINCT Controle  ',
    'FROM _vs_cardex_imeis_3 ',
    //'ORDER BY IMEIOri, Ordem, Controle; ',
    ' ']);
    //Geral.MB_Teste(SQL);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, DModG.MyPID_DB, SQL);
    Corda := MyObjects.CordaDeQuery(QrCorda, 'Controle', '-999999999');
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados');
    SQL_Flds := Geral.ATS([
    VS_PF.SQL_NO_GGX(),
    ATT_MovimID,
    'IF(ValorT<>0.00, ValorT, IF(MovimID IN (2), 0.00, IF(ValorMP <> 0, ValorMP, ValorT - CustoPQ))) CustoMP,',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
    'vsf.Nome NO_SerieFch, ',
    'vsp.Nome NO_Pallet, ',
    'IF(Pecas>0, AreaM2/Pecas, 0) MediaArM2, ',
    'IF(Pecas>0, PesoKg/Pecas, 0) MediaPeso, ',
    '0.00 AcumPeca, 0.00 AcumPeso, 0.00 AcumArM2, ',
    '0.00 AcumArP2, 0.00 AcumValorT, 0.00 AcumCusPc, ',
    '0.00 AcumCusKg, 0.00 AcumCusM2, 0.00 AcumCusP2, ',
    '0.00 AcImeiPeca, 0.00 AcImeiPeso, 0.00 AcImeiArM2, ',
    '0.00 AcImeiArP2, 0.00 AcImeiValorT, 0.00 AcImeiCusPc, ',
    '0.00 AcImeiCusKg, 0.00 AcImeiCusM2, 0.00 AcImeiCusP2, ',
    'ci3.Ordem, ci3.PalOri, ci3.IMEIOri ',
    '']);
    SQL_Left := Geral.ATS([
    SQL_LJ_GGX(),
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
    'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    'LEFT JOIN ' + TMeuDB + '.entidades  frn ON frn.Codigo=vmi.Terceiro',
    'LEFT JOIN _vs_cardex_imeis_3 ci3 ON ci3.Controle=vmi.Controle',
    '']);
    SQL_Wher := Geral.ATS([
    'WHERE vmi.Controle IN (' + Corda + ')',
    '']);
    SQL_Group := '';
    //UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
    SQL := Geral.ATS([
    'INSERT INTO ' + FVSKardexMov,
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    'ORDER BY NO_Pallet, Controle ',
    '']);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [SQL]);
    //
    //Geral.MB_Teste(SQL);
  end;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX = 0 then
    SQL_GraGruX := ''
  else
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
  //
  SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora', 0,
      TP02DataFim.Date, True, Ck02DataFim.Checked);

  //
  (*Empresa*) DModG.ObtemEmpresaSelecionada(EdEmpresa, FEmpresa);
  if MyObjects.FIC(FEmpresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  //
  GeraVSKardex();
  //
end;

end.
