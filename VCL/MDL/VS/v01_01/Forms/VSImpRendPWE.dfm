object FmVSImpRendPWE: TFmVSImpRendPWE
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-153 :: Impress'#227'o de Rendimento de Semi e Acabado'
  ClientHeight = 638
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 563
        Height = 32
        Caption = 'Impress'#227'o de Rendimento de Semi e Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 563
        Height = 32
        Caption = 'Impress'#227'o de Rendimento de Semi e Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 563
        Height = 32
        Caption = 'Impress'#227'o de Rendimento de Semi e Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 476
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 476
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 476
        Align = alClient
        TabOrder = 0
        object DBGIndef: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 459
          Align = alClient
          DataSource = DsIndef
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_CouNiv2'
              Title.Caption = 'Tipo de material'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CouNiv1'
              Title.Caption = 'Parte do material'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruY'
              Title.Caption = 'Grupo'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_GGY'
              Title.Caption = 'Nome do grupo'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Nome / tamenho / cor do artigo'
              Width = 360
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 524
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 568
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 11
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
  object QrRend: TmySQLQuery
   
    SQL.Strings = (
      'DROP TABLE IF EXISTS _vmi_emin_22_; '
      'CREATE TABLE _vmi_emin_22_ '
      'SELECT * '
      'FROM bluederm_meza.vsmovits '
      'WHERE MovimNiv=22; '
      ' '
      'DROP TABLE IF EXISTS _vmi_emin_23_; '
      'CREATE TABLE _vmi_emin_23_ '
      'SELECT * '
      'FROM bluederm_meza.vsmovits '
      'WHERE MovimNiv=23; '
      ' '
      'DROP TABLE IF EXISTS _vmi_emin_20_; '
      'CREATE TABLE _vmi_emin_20_ '
      'SELECT DISTINCT MovimCod, GraGruX '
      'FROM bluederm_meza.vsmovits '
      'WHERE MovimNiv=20 '
      'GROUP BY MovimCod; '
      ' '
      ' '
      
        'SELECT cou3.Bastidao, IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) ' +
        'NO_FORNECE, '
      '_20.GraGruX, CONCAT(gg11.Nome, '
      'IF(gti1.PrintTam=0, "", CONCAT(" ", gti1.Nome)), '
      'IF(gcc1.PrintCor=0,"", CONCAT(" ", gcc1.Nome))) '
      'NO_PRD_TAM_COR_22, '
      ' '
      'CONCAT(gg12.Nome, '
      'IF(gti2.PrintTam=0, "", CONCAT(" ", gti2.Nome)), '
      'IF(gcc2.PrintCor=0,"", CONCAT(" ", gcc2.Nome))) '
      'NO_PRD_TAM_COR, '
      ' '
      'IF(_23.AreaM2 <> 0, '
      '(_22.AreaM2+_23.AreaM2)/-_23.AreaM2*100, 0) Rendimento, '
      'cab.NFeRem, cab.Codigo, cab.MovimCod, _22.Controle Ctrl22, '
      
        '_22.MovimTwn, _22.Empresa, _22.Terceiro, _22.MovimID, _22.DataHo' +
        'ra, '
      '_22.Pallet, _22.GraGruX GGX22, _22.Pecas Pecas_22, '
      '_22.AreaM2 AreaM2_22, _23.Controle Ctrl23, _23.GraGruX GGX23, '
      '-_23.Pecas Pecas_23, -_23.AreaM2 AreaM2_23 '
      ' '
      'FROM _vmi_emin_22_ _22 '
      'LEFT JOIN _vmi_emin_20_ _20 ON _20.MovimCod=_22.MovimCod '
      'LEFT JOIN _vmi_emin_23_ _23 ON _23.MovimTwn=_22.MovimTwn '
      
        'LEFT JOIN bluederm_meza.vspwecab  cab ON cab.MovimCod=_22.MovimC' +
        'od '
      
        'LEFT JOIN bluederm_meza.entidades frn ON frn.Codigo=_22.Terceiro' +
        ' '
      
        'LEFT JOIN bluederm_meza.gragruxcou cou3 ON cou3.GraGruX=_23.GraG' +
        'ruX'
      
        'LEFT JOIN bluederm_meza.gragrux    ggx1 ON ggx1.Controle=_22.Gra' +
        'GruX '
      
        'LEFT JOIN bluederm_meza.gragruc    ggc1 ON ggc1.Controle=ggx1.Gr' +
        'aGruC '
      
        'LEFT JOIN bluederm_meza.gracorcad  gcc1 ON gcc1.Codigo=ggc1.GraC' +
        'orCad '
      
        'LEFT JOIN bluederm_meza.gratamits  gti1 ON gti1.Controle=ggx1.Gr' +
        'aTamI '
      
        'LEFT JOIN bluederm_meza.gragru1    gg11 ON gg11.Nivel1=ggx1.GraG' +
        'ru1 '
      ' '
      
        'LEFT JOIN bluederm_meza.gragrux    ggx2 ON ggx2.Controle=_20.Gra' +
        'GruX '
      
        'LEFT JOIN bluederm_meza.gragruc    ggc2 ON ggc2.Controle=ggx2.Gr' +
        'aGruC '
      
        'LEFT JOIN bluederm_meza.gracorcad  gcc2 ON gcc2.Codigo=ggc2.GraC' +
        'orCad '
      
        'LEFT JOIN bluederm_meza.gratamits  gti2 ON gti2.Controle=ggx2.Gr' +
        'aTamI '
      
        'LEFT JOIN bluederm_meza.gragru1    gg12 ON gg12.Nivel1=ggx2.GraG' +
        'ru1 '
      ' '
      'AND cou3.Bastidao IN (-999999999,1,2,3,4,5,6,7,8)'
      'WHERE cab.DtHrAberto  >= "2016-03-07"'
      'ORDER BY NFeRem, DataHora, NO_PRD_TAM_COR_22'
      '')
    Left = 64
    Top = 288
    object QrRendBastidao: TSmallintField
      FieldName = 'Bastidao'
    end
    object QrRendNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrRendGGX20: TIntegerField
      FieldName = 'GGX20'
    end
    object QrRendNO_PRD_TAM_COR_22: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_22'
      Size = 157
    end
    object QrRendNO_PRD_TAM_COR_20: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_20'
      Size = 157
    end
    object QrRendRendimento: TFloatField
      FieldName = 'Rendimento'
    end
    object QrRendNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrRendCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRendMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrRendCtrl22: TIntegerField
      FieldName = 'Ctrl22'
    end
    object QrRendMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrRendEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrRendTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrRendMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrRendDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrRendPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrRendGGX22: TIntegerField
      FieldName = 'GGX22'
    end
    object QrRendPecas_22: TFloatField
      FieldName = 'Pecas_22'
    end
    object QrRendAreaM2_22: TFloatField
      FieldName = 'AreaM2_22'
    end
    object QrRendCtrl23: TIntegerField
      FieldName = 'Ctrl23'
    end
    object QrRendGGX23: TIntegerField
      FieldName = 'GGX23'
    end
    object QrRendPecas_23: TFloatField
      FieldName = 'Pecas_23'
    end
    object QrRendAreaM2_23: TFloatField
      FieldName = 'AreaM2_23'
    end
    object QrRendDATA: TDateField
      FieldName = 'DATA'
    end
    object QrRendNO_Bastidao: TWideStringField
      FieldName = 'NO_Bastidao'
      Size = 60
    end
    object QrRendCtrl20: TIntegerField
      FieldName = 'Ctrl20'
    end
  end
  object QrIndef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cou.*, nv1.Nome NO_CouNiv1, nv2.Nome NO_CouNiv2, '
      'ggx.GraGruY, ggy.Nome NO_GGY, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM gragruxcou cou'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=cou.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=cou.CouNiv1'
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=cou.CouNiv2'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'WHERE Bastidao=-1')
    Left = 64
    Top = 100
    object QrIndefGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrIndefCouNiv1: TIntegerField
      FieldName = 'CouNiv1'
    end
    object QrIndefCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrIndefArtigoImp: TWideStringField
      FieldName = 'ArtigoImp'
      Required = True
      Size = 50
    end
    object QrIndefClasseImp: TWideStringField
      FieldName = 'ClasseImp'
      Required = True
      Size = 30
    end
    object QrIndefPrevPcPal: TIntegerField
      FieldName = 'PrevPcPal'
    end
    object QrIndefMediaMinM2: TFloatField
      FieldName = 'MediaMinM2'
    end
    object QrIndefMediaMaxM2: TFloatField
      FieldName = 'MediaMaxM2'
    end
    object QrIndefLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIndefDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIndefDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIndefUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIndefUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIndefAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrIndefAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrIndefMediaMinKg: TFloatField
      FieldName = 'MediaMinKg'
    end
    object QrIndefMediaMaxKg: TFloatField
      FieldName = 'MediaMaxKg'
    end
    object QrIndefPrevAMPal: TFloatField
      FieldName = 'PrevAMPal'
    end
    object QrIndefPrevKgPal: TFloatField
      FieldName = 'PrevKgPal'
    end
    object QrIndefGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrIndefBastidao: TSmallintField
      FieldName = 'Bastidao'
    end
    object QrIndefNO_CouNiv1: TWideStringField
      FieldName = 'NO_CouNiv1'
      Size = 60
    end
    object QrIndefNO_CouNiv2: TWideStringField
      FieldName = 'NO_CouNiv2'
      Size = 60
    end
    object QrIndefGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrIndefNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrIndefNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsIndef: TDataSource
    DataSet = QrIndef
    Left = 64
    Top = 148
  end
  object frxWET_CURTI_153_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'procedure Memo13OnPreviewDblClick(Sender: TfrxView; Button: TMou' +
        'seButton; Shift: Integer; var Modified: Boolean);'
      'begin'
      '  MyFunc(<frxDsRend."Ctrl22">);        '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_153_AGetValue
    OnUserFunction = frxWET_CURTI_153_AUserFunction
    Left = 64
    Top = 240
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsRend
        DataSetName = 'frxDsRend'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RENDIMENTO DE COURO SEMI ACABADO ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 143.622140000000000000
          Top = 64.252010000000000000
          Width = 170.078752360000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 86.929190000000000000
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '????')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 529.134200000000000000
          Top = 64.252010000000000000
          Width = 52.913388270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 480.000310000000000000
          Top = 64.252010000000000000
          Width = 49.133850940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo19: TfrxMemoView
          Left = 313.700990000000000000
          Top = 64.252010000000000000
          Width = 56.692930470000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NFeRem')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 430.866420000000000000
          Top = 64.252010000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo30: TfrxMemoView
          Left = 279.685220000000000000
          Top = 41.574830000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '???')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Top = 64.252010000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo70: TfrxMemoView
          Left = 650.079160000000000000
          Top = 64.252010000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo21: TfrxMemoView
          Left = 370.393940000000000000
          Top = 64.252010000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 582.047620000000000000
          Top = 64.252010000000000000
          Width = 52.913388270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Rendim.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 30.236240000000000000
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        DataSet = frxDsRend
        DataSetName = 'frxDsRend'
        RowCount = 0
        object Memo64: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 30.236240000000000000
          Width = 619.842920000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
          WordWrap = False
        end
        object MeValNome: TfrxMemoView
          Left = 143.622140000000000000
          Width = 170.078752360000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR_20'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR_20"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object MeValCodi: TfrxMemoView
          Left = 86.929190000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'GGX20'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRend."GGX20"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 430.866420000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."Pecas_23"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 370.393940000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."AreaM2_23"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 313.700990000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'NFeRem'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."NFeRem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 529.134200000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."AreaM2_22"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 480.000310000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'Pecas_22'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."Pecas_22"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 582.047620000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'Rendimento'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."Rendimento"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 30.236240000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          OnPreviewDblClick = 'Memo13OnPreviewDblClick'
          DataField = 'Ctrl22'
          DataSet = frxDsRend
          DataSetName = 'frxDsRend'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRend."Ctrl22"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 619.842920000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 566.929500000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000000000
          Width = 309.921264720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 430.866420000000000000
          Top = 15.118120000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 370.393940000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 529.134200000000000000
          Top = 15.118120000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 480.000310000000000000
          Top = 15.118120000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 582.047620000000000000
          Top = 15.118120000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsRend."AreaM2_23">,MD002,1)<=0,0,'
            
              '  ((SUM(<frxDsRend."AreaM2_22">,MD002,1)-SUM(<frxDsRend."AreaM2_' +
              '23">,MD002,1))/SUM(<frxDsRend."AreaM2_23">,MD002,1)*100))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsRend."GraGruY"'
        object Memo33: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Me_GH0: TfrxMemoView
          Width = 680.314985040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRend."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 249.448980000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsRend."GraGruX"'
        object Memo42: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Me_GH2: TfrxMemoView
          Left = 18.897650000000000000
          Width = 642.519685040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 204.094620000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsRend."GraGruX"'
        object Memo35: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Me_GH1: TfrxMemoView
          Left = 11.338590000000000000
          Width = 657.637805040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 453.543600000000000000
        Width = 680.315400000000000000
        object Memo49: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 430.866420000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT1: TfrxMemoView
          Left = 45.354360000000000000
          Width = 264.566929130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR"] ')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu1AreaM2: TfrxMemoView
          Left = 370.393940000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 529.134200000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 480.000310000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_02: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 415.748300000000000000
        Width = 680.315400000000000000
        object Memo45: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Me_FT2: TfrxMemoView
          Left = 45.354360000000000000
          Width = 264.566929130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR"] ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 430.866420000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSu2AreaM2: TfrxMemoView
          Left = 370.393940000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 529.134200000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 480.000310000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_03: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 294.803340000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsRend."GraGruX"'
        object Memo43: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
        end
        object Me_GH3: TfrxMemoView
          Left = 26.456710000000000000
          Width = 623.622035040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsRend."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_03: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object Memo44: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 430.866420000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT3: TfrxMemoView
          Left = 45.354360000000000000
          Width = 264.566929130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 370.393940000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 529.134200000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 480.000310000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 491.338900000000000000
        Width = 680.315400000000000000
        object Memo51: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 430.866420000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT0: TfrxMemoView
          Left = 45.354360000000000000
          Width = 264.566929130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRend."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu0AreaM2: TfrxMemoView
          Left = 370.393940000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_23">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 529.134200000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."AreaM2_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 480.000310000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRend."Pecas_22">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 582.047620000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsRend."AreaM2_23">,MD002,1)<=0,0,'
            
              '  ((SUM(<frxDsRend."AreaM2_22">,MD002,1)-SUM(<frxDsRend."AreaM2_' +
              '23">,MD002,1))/SUM(<frxDsRend."AreaM2_23">,MD002,1)*100))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsRend: TfrxDBDataset
    UserName = 'frxDsRend'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Bastidao=Bastidao'
      'NO_FORNECE=NO_FORNECE'
      'GGX20=GGX20'
      'NO_PRD_TAM_COR_22=NO_PRD_TAM_COR_22'
      'NO_PRD_TAM_COR_20=NO_PRD_TAM_COR_20'
      'Rendimento=Rendimento'
      'NFeRem=NFeRem'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Ctrl22=Ctrl22'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GGX22=GGX22'
      'Pecas_22=Pecas_22'
      'AreaM2_22=AreaM2_22'
      'Ctrl23=Ctrl23'
      'GGX23=GGX23'
      'Pecas_23=Pecas_23'
      'AreaM2_23=AreaM2_23'
      'DATA=DATA'
      'NO_Bastidao=NO_Bastidao'
      'Ctrl20=Ctrl20')
    DataSet = QrRend
    BCDToCurrency = False
    Left = 64
    Top = 336
  end
  object QrZ: TmySQLQuery
   
    SQL.Strings = (
      'SELECT * '
      'FROM _vs_imp_rend_imei_ '
      'WHERE IMEIIni=3363 '
      'AND Level1=0 ')
    Left = 272
    Top = 248
    object QrZControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrZCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrZMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrZMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrZMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrZEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrZTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrZCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrZMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrZDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrZPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrZGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrZPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrZPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrZAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrZAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrZValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrZSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrZSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrZSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrZSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrZSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrZSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrZSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrZObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrZSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrZFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrZMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrZFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrZDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrZDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrZDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrZDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrZQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrZQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrZQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrZQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrZQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrZQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrZQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrZQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrZNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrZMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrZReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrZStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrZItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrZVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrZClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrZNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrZNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrZVSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
  end
  object QrIncl: TmySQLQuery
    Database = Dmod.MyDB
    Left = 440
    Top = 224
  end
  object Qr_: TmySQLQuery
   
    SQL.Strings = (
      'SELECT * '
      'FROM _vs_imp_rend_imei_ '
      'WHERE IMEIIni=3363 '
      'AND Level1=0 ')
    Left = 276
    Top = 192
    object Qr_IMEIIni: TIntegerField
      FieldName = 'IMEIIni'
    end
    object Qr_Level1: TIntegerField
      FieldName = 'Level1'
    end
    object Qr_Level2: TIntegerField
      FieldName = 'Level2'
    end
    object Qr_Level3: TIntegerField
      FieldName = 'Level3'
    end
    object Qr_Controle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr_Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr_MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object Qr_MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object Qr_MovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object Qr_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr_Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object Qr_CliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object Qr_MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object Qr_DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object Qr_Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object Qr_GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr_Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr_PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object Qr_AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr_AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object Qr_ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object Qr_SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr_SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object Qr_SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object Qr_SrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object Qr_SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object Qr_SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object Qr_SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object Qr_Observ: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object Qr_SerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object Qr_Ficha: TIntegerField
      FieldName = 'Ficha'
    end
    object Qr_Misturou: TSmallintField
      FieldName = 'Misturou'
    end
    object Qr_FornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object Qr_DstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object Qr_DstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object Qr_DstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object Qr_DstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object Qr_QtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object Qr_QtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object Qr_QtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object Qr_QtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object Qr_QtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object Qr_QtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object Qr_QtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object Qr_QtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object Qr_NotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object Qr_Marca: TWideStringField
      FieldName = 'Marca'
    end
    object Qr_ReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object Qr_StqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object Qr_ItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object Qr_VSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object Qr_ClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object Qr_NFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object Qr_NFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object Qr_VSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
    object Qr_PrcPeca: TIntegerField
      FieldName = 'PrcPeca'
    end
    object Qr_PrcPeso: TIntegerField
      FieldName = 'PrcPeso'
    end
    object Qr_PrcArM2: TIntegerField
      FieldName = 'PrcArM2'
    end
    object Qr_PrcKnd: TIntegerField
      FieldName = 'PrcKnd'
    end
    object Qr_PrcRend: TIntegerField
      FieldName = 'PrcRend'
    end
  end
  object QrLevel2: TmySQLQuery
   
    BeforeClose = QrLevel2BeforeClose
    AfterScroll = QrLevel2AfterScroll
    SQL.Strings = (
      
        'SELECT Level1, Level2, COUNT(Controle) ITENS, SUM(Pecas) Pecas, ' +
        ' '
      'SUM(AreaM2) AreaM2, SUM(PesoKg) PesoKg '
      'FROM _vs_imp_rend_imei_ '
      'GROUP BY Level1 '
      'ORDER BY Level1 ')
    Left = 596
    Top = 280
    object QrLevel2Level1: TIntegerField
      FieldName = 'Level1'
    end
    object QrLevel2Level2: TIntegerField
      FieldName = 'Level2'
    end
    object QrLevel2ITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrLevel2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrLevel2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrLevel2PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrLevel2NO_Level2: TWideStringField
      FieldName = 'NO_Level2'
      Size = 60
    end
  end
  object frxDsLevel2: TfrxDBDataset
    UserName = 'frxDsLevel2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Level1=Level1'
      'Level2=Level2'
      'ITENS=ITENS'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'PesoKg=PesoKg'
      'NO_Level2=NO_Level2')
    DataSet = QrLevel2
    BCDToCurrency = False
    Left = 596
    Top = 328
  end
  object QrPallets: TmySQLQuery
   
    SQL.Strings = (
      'SELECT iri.MovimNiv, iri.Pallet, iri.GraGruX, NFeSer, NFeNum, '
      
        'PrcMovID, PrcMovNiv, PrcNivel1, PrcNivel2, PrcKnd, PrcRend, Lvl1' +
        'Ant,'
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, SUM(PesoKg) PesoKg, '
      
        'SUM(PrcPeca) PrcPeca, SUM(PrcPeso) PrcPeso, SUM(PrcArM2) PrcArM2' +
        ', '
      
        ' ELT(MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destino cl' +
        'assifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de arti' +
        'go","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Origem o' +
        'pera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino oper' +
        'a'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa em ' +
        'pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo Ger' +
        'ado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Artigo' +
        ' In Natura","Artigo In Natura","Artigo Gerado","Artigo Classific' +
        'ado","Artigo em Opera'#231#227'o","Origem semi acabado em processo","Sem' +
        'i acabado em processo","Destino semi acabado em processo","Baixa' +
        ' de semi acabado em processo","Artigo Semi Acabado","Artigo Acab' +
        'ado","Sub produto","Origem de transf. de local","Destino de tran' +
        'sf. de local","Origem caleiro em processo","Caleiro em processo"' +
        ',"Destino caleiro em processo","Baixa de caleiro em processo","A' +
        'rtigo de caleiro","Origem curtimento em processo","Curtimento em' +
        ' processo","Destino curtimento em processo","Baixa de curtimento' +
        ' em processo","Artigo de curtimento") NO_MovimNiv,'
      'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR '
      'FROM _vs_imp_rend_imei_ iri '
      
        'LEFT JOIN bluederm_meza.gragrux    ggx ON ggx.Controle=iri.GraGr' +
        'uX '
      
        'LEFT JOIN bluederm_meza.gragruc    ggc ON ggc.Controle=ggx.GraGr' +
        'uC  '
      
        'LEFT JOIN bluederm_meza.gracorcad  gcc ON gcc.Codigo=ggc.GraCorC' +
        'ad  '
      
        'LEFT JOIN bluederm_meza.gratamits  gti ON gti.Controle=ggx.GraTa' +
        'mI  '
      
        'LEFT JOIN bluederm_meza.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1' +
        '  '
      
        'LEFT JOIN bluederm_meza.unidmed    unm ON unm.Codigo=gg1.UnidMed' +
        '  '
      
        'LEFT JOIN bluederm_meza.stqcenloc  scl ON scl.Controle=iri.StqCe' +
        'nLoc '
      'LEFT JOIN bluederm_meza.stqcencad  scc ON scc.Codigo=scl.Codigo '
      'WHERE iri.Level1=2000'
      'GROUP BY iri.MovimNiv, iri.Pallet, iri.GraGruX'
      'ORDER BY MovimNiv, Pallet, GraGruX')
    Left = 596
    Top = 376
    object QrPalletsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrPalletsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrPalletsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPalletsNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrPalletsNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrPalletsPrcMovID: TIntegerField
      FieldName = 'PrcMovID'
    end
    object QrPalletsPrcMovNiv: TIntegerField
      FieldName = 'PrcMovNiv'
    end
    object QrPalletsPrcNivel1: TIntegerField
      FieldName = 'PrcNivel1'
    end
    object QrPalletsPrcNivel2: TIntegerField
      FieldName = 'PrcNivel2'
    end
    object QrPalletsPrcKnd: TIntegerField
      FieldName = 'PrcKnd'
    end
    object QrPalletsPrcRend: TFloatField
      FieldName = 'PrcRend'
    end
    object QrPalletsLvl1Ant: TIntegerField
      FieldName = 'Lvl1Ant'
    end
    object QrPalletsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrPalletsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrPalletsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrPalletsPrcPeca: TFloatField
      FieldName = 'PrcPeca'
    end
    object QrPalletsPrcPeso: TFloatField
      FieldName = 'PrcPeso'
    end
    object QrPalletsPrcArM2: TFloatField
      FieldName = 'PrcArM2'
    end
    object QrPalletsNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object QrPalletsNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 103
    end
    object QrPalletsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPalletsCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrPalletsNO_CouNiv2: TWideStringField
      FieldName = 'NO_CouNiv2'
      Size = 60
    end
  end
  object frxDsPallets: TfrxDBDataset
    UserName = 'frxDsPallets'
    CloseDataSource = False
    FieldAliases.Strings = (
      'MovimNiv=MovimNiv'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'NFeSer=NFeSer'
      'NFeNum=NFeNum'
      'PrcMovID=PrcMovID'
      'PrcMovNiv=PrcMovNiv'
      'PrcNivel1=PrcNivel1'
      'PrcNivel2=PrcNivel2'
      'PrcKnd=PrcKnd'
      'PrcRend=PrcRend'
      'Lvl1Ant=Lvl1Ant'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'PesoKg=PesoKg'
      'PrcPeca=PrcPeca'
      'PrcPeso=PrcPeso'
      'PrcArM2=PrcArM2'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_LOC_CEN=NO_LOC_CEN'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'CouNiv2=CouNiv2'
      'NO_CouNiv2=NO_CouNiv2')
    DataSet = QrPallets
    BCDToCurrency = False
    Left = 596
    Top = 424
  end
  object frxWET_CURTI_153_B: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'procedure Memo13OnPreviewDblClick(Sender: TfrxView; Button: TMou' +
        'seButton; Shift: Integer; var Modified: Boolean);'
      'begin'
      '  MyFunc(<frxDsRend."Ctrl22">);        '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_153_AGetValue
    OnUserFunction = frxWET_CURTI_153_AUserFunction
    Left = 596
    Top = 232
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsLevel2
        DataSetName = 'frxDsLevel2'
      end
      item
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
      end
      item
        DataSet = frxDsRnd2
        DataSetName = 'frxDsRnd2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 729.449290000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Top = 706.772110000000000000
        Width = 680.315400000000000000
      end
      object MD01: TfrxMasterData
        FillType = ftBrush
        Height = 37.795275590000000000
        Top = 117.165430000000000000
        Width = 680.315400000000000000
        DataSet = frxDsLevel2
        DataSetName = 'frxDsLevel2'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 56.692950000000000000
          Height = 18.897637800000000000
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nivel ')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 56.692950000000000000
          Width = 464.882190000000000000
          Height = 18.897637800000000000
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o do n'#237'vel')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 521.575140000000000000
          Width = 60.472480000000000000
          Height = 18.897637800000000000
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 582.047620000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 631.181510000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 18.897650000000000000
          Width = 56.692950000000000000
          Height = 18.897637800000000000
          DataField = 'Level2'
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLevel2."Level2"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 56.692950000000000000
          Top = 18.897650000000000000
          Width = 464.882190000000000000
          Height = 18.897637800000000000
          DataField = 'NO_Level2'
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLevel2."NO_Level2"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 521.575140000000000000
          Top = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 18.897637800000000000
          DataField = 'Pecas'
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLevel2."Pecas"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 582.047620000000000000
          Top = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          DataField = 'AreaM2'
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLevel2."AreaM2"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 631.181510000000000000
          Top = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          DataField = 'PesoKg'
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLevel2."PesoKg"]')
          ParentFont = False
        end
      end
      object DD01: TfrxDetailData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
        RowCount = 0
        object Memo13: TfrxMemoView
          Left = 143.622140000000000000
          Width = 170.078752360000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo17: TfrxMemoView
          Left = 86.929190000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo18: TfrxMemoView
          Left = 313.700990000000000000
          Width = 56.692930470000000000
          Height = 13.228346460000000000
          DataField = 'NFeNum'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPallets."NFeNum"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo23: TfrxMemoView
          Left = 510.236550000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DataField = 'Pecas'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPallets."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo24: TfrxMemoView
          Left = 559.370440000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPallets."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo26: TfrxMemoView
          Left = 30.236240000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'Pallet'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPallets."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo45: TfrxMemoView
          Left = 370.393940000000000000
          Width = 139.842512360000000000
          Height = 13.228346460000000000
          DataField = 'NO_LOC_CEN'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."NO_LOC_CEN"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GH01: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 177.637910000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPallets."MovimNiv"'
        object Memo42: TfrxMemoView
          Width = 680.315302360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'N'#237'vel do movimento: [frxDsPallets."MovimNiv"] - [frxDsPallets."N' +
              'O_MovimNiv"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GF01: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          Left = 510.236550000000000000
          Width = 49.133855830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPallets."Pecas">,DD01)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo40: TfrxMemoView
          Left = 559.370440000000000000
          Width = 60.472448270000000000
          Height = 15.118110240000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPallets."AreaM2">,DD01)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472448270000000000
          Height = 15.118110240000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPallets."PesoKg">,DD01)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo43: TfrxMemoView
          Width = 510.236452360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              'N'#237'vel do movimento: [frxDsPallets."MovimNiv"] - [frxDsPallets."N' +
              'O_MovimNiv"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 28.346466460000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPallets."CouNiv2"'
        object Memo14: TfrxMemoView
          Left = 7.559060000000000000
          Width = 672.755910390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'Tipo de material: [frxDsPallets."CouNiv2"] - [frxDsPallets."NO_C' +
              'ouNiv2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
          Formats = <
            item
            end
            item
            end>
        end
        object Memo28: TfrxMemoView
          Left = 143.622140000000000000
          Top = 15.118120000000000000
          Width = 170.078752360000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Left = 86.929190000000000000
          Top = 15.118120000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo32: TfrxMemoView
          Left = 313.700990000000000000
          Top = 15.118120000000000000
          Width = 56.692930470000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NFe')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo33: TfrxMemoView
          Left = 510.236550000000000000
          Top = 15.118120000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo36: TfrxMemoView
          Left = 559.370440000000000000
          Top = 15.118120000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          Left = 30.236240000000000000
          Top = 15.118120000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo38: TfrxMemoView
          Left = 619.842920000000000000
          Top = 15.118120000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo44: TfrxMemoView
          Left = 370.393940000000000000
          Top = 15.118120000000000000
          Width = 139.842512360000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Local (Centro)')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        object Memo15: TfrxMemoView
          Left = 510.236550000000000000
          Width = 49.133855830000000000
          Height = 15.118110240000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPallets."Pecas">,DD01)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo16: TfrxMemoView
          Left = 559.370440000000000000
          Width = 60.472448270000000000
          Height = 15.118110240000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPallets."AreaM2">,DD01)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo22: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472448270000000000
          Height = 15.118110240000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPallets."PesoKg">,DD01)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo25: TfrxMemoView
          Left = 7.559060000000000000
          Width = 502.677060390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              'Tipo de material: [frxDsPallets."CouNiv2"] - [frxDsPallets."NO_C' +
              'ouNiv2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
          Formats = <
            item
            end
            item
            end>
        end
      end
      object MD02: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 514.016080000000000000
        Width = 680.315400000000000000
        DataSet = frxDsRnd2
        DataSetName = 'frxDsRnd2'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 120.944960000000000000
          Width = 162.519692360000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRnd2."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object MeValCodi: TfrxMemoView
          Left = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRnd2."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 502.677490000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'Pecas'
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRnd2."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 340.157700000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'PrcPeso'
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRnd2."PrcPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 283.464750000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'NFeNum'
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRnd2."NFeNum"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 400.630180000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'PrcPeca'
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRnd2."PrcPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 551.811380000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRnd2."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 600.945270000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRnd2."PrcRend"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          OnPreviewDblClick = 'Memo13OnPreviewDblClick'
          DataField = 'Controle'
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRnd2."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 453.543600000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'PrcArM2'
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRnd2."PrcArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 653.858690000000000000
          Width = 26.456675830000000000
          Height = 13.228346460000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' [frxDsRnd2."NO_PrcKnd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH03: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsRnd2."PrcKnd"'
        object Memo56: TfrxMemoView
          Width = 680.315302360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma de c'#225'lculo de rendimento: [frxDsRnd2."NO_PrcKnd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GF03: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 593.386210000000000000
        Width = 680.315400000000000000
        object Memo65: TfrxMemoView
          Left = 340.157700000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRnd2."PrcPeso">,MD02)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 502.677490000000000000
          Width = 49.133850940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRnd2."Pecas">,MD02)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 551.811380000000000000
          Width = 49.133850940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRnd2."AreaM2">,MD02)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 400.630180000000000000
          Width = 52.913380940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRnd2."PrcPeca">,MD02)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 453.543600000000000000
          Width = 49.133850940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRnd2."PrcArM2">,MD02)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 600.945270000000000000
          Width = 52.913380940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF('
            'SUM(<frxDsRnd2."PrcArM2">,MD02) > 0'
            ','
            
              '(SUM(<frxDsRnd2."AreaM2">,MD02) - SUM(<frxDsRnd2."PrcArM2">,MD02' +
              ')) / SUM(<frxDsRnd2."PrcArM2">,MD02) * 100'
            ','
            
              'SUM(<frxDsRnd2."PrcPeso">,MD02) / SUM(<frxDsRnd2."AreaM2">,MD02)' +
              ' '
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 653.858690000000000000
          Width = 26.456670940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRnd2."NO_PrcKnd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Width = 340.157480310000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Forma de c'#225'lculo de rendimento: [frxDsRnd2."NO_PrcKnd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 381.732530000000000000
        Width = 680.315400000000000000
        StartNewPage = True
        object Memo53: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Rendimento de Couros Recurtidos')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 7.559060000000000000
        Top = 638.740570000000000000
        Width = 680.315400000000000000
      end
      object GH04: TfrxGroupHeader
        FillType = ftBrush
        Height = 28.346466460000000000
        Top = 461.102660000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsRnd2."GG1_Tam"'
        object MeTitNome: TfrxMemoView
          Left = 120.944960000000000000
          Top = 15.118120000000000000
          Width = 162.519692360000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 64.252010000000000000
          Top = 15.118120000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPecas: TfrxMemoView
          Left = 400.630180000000000000
          Top = 15.118120000000000000
          Width = 52.913388270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 551.811380000000000000
          Top = 15.118120000000000000
          Width = 49.133850940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo19: TfrxMemoView
          Left = 283.464750000000000000
          Top = 15.118120000000000000
          Width = 56.692930470000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NFeRem')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 502.677490000000000000
          Top = 15.118120000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo21: TfrxMemoView
          Left = 340.157700000000000000
          Top = 15.118120000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 600.945270000000000000
          Top = 15.118120000000000000
          Width = 79.370098270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Rendimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 7.559060000000000000
          Top = 15.118120000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo52: TfrxMemoView
          Left = 453.543600000000000000
          Top = 15.118120000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo55: TfrxMemoView
          Left = 7.559060000000000000
          Width = 672.756242360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo e tamanho: [frxDsRnd2."NO_GG1_GTI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GF04: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 551.811380000000000000
        Width = 680.315400000000000000
        object Memo57: TfrxMemoView
          Left = 340.157700000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRnd2."PrcPeso">,MD02)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 502.677490000000000000
          Width = 49.133850940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRnd2."Pecas">,MD02)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 551.811380000000000000
          Width = 49.133850940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRnd2."AreaM2">,MD02)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 400.630180000000000000
          Width = 52.913380940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRnd2."PrcPeca">,MD02)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 453.543600000000000000
          Width = 49.133850940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRnd2."PrcArM2">,MD02)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 600.945270000000000000
          Width = 52.913380940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF('
            'SUM(<frxDsRnd2."PrcArM2">,MD02) > 0'
            ','
            
              '(SUM(<frxDsRnd2."AreaM2">,MD02) - SUM(<frxDsRnd2."PrcArM2">,MD02' +
              ')) / SUM(<frxDsRnd2."PrcArM2">,MD02) * 100'
            ','
            
              'SUM(<frxDsRnd2."PrcPeso">,MD02) / SUM(<frxDsRnd2."AreaM2">,MD02)' +
              ' '
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 653.858690000000000000
          Width = 26.456670940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRnd2
          DataSetName = 'frxDsRnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRnd2."NO_PrcKnd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 7.559060000000000000
          Width = 332.598420310000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Artigo e tamanho: [frxDsRnd2."NO_GG1_GTI"] ')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
    end
  end
  object QrImeis: TmySQLQuery
   
    SQL.Strings = (
      'SELECT iri.*, '
      
        ' ELT(MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destino cl' +
        'assifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de arti' +
        'go","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Origem o' +
        'pera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino oper' +
        'a'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa em ' +
        'pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo Ger' +
        'ado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Artigo' +
        ' In Natura","Artigo In Natura","Artigo Gerado","Artigo Classific' +
        'ado","Artigo em Opera'#231#227'o","Origem semi acabado em processo","Sem' +
        'i acabado em processo","Destino semi acabado em processo","Baixa' +
        ' de semi acabado em processo","Artigo Semi Acabado","Artigo Acab' +
        'ado","Sub produto","Origem de transf. de local","Destino de tran' +
        'sf. de local","Origem caleiro em processo","Caleiro em processo"' +
        ',"Destino caleiro em processo","Baixa de caleiro em processo","A' +
        'rtigo de caleiro","Origem curtimento em processo","Curtimento em' +
        ' processo","Destino curtimento em processo","Baixa de curtimento' +
        ' em processo","Artigo de curtimento") NO_MovimNiv,'
      'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR '
      'FROM _vs_imp_rend_imei_ iri '
      
        'LEFT JOIN bluederm_meza.gragrux   ggx ON ggx.Controle=iri.GraGru' +
        'X '
      
        'LEFT JOIN bluederm_meza.gragruc    ggc ON ggc.Controle=ggx.GraGr' +
        'uC  '
      
        'LEFT JOIN bluederm_meza.gracorcad  gcc ON gcc.Codigo=ggc.GraCorC' +
        'ad  '
      
        'LEFT JOIN bluederm_meza.gratamits  gti ON gti.Controle=ggx.GraTa' +
        'mI  '
      
        'LEFT JOIN bluederm_meza.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1' +
        '  '
      
        'LEFT JOIN bluederm_meza.unidmed   unm ON unm.Codigo=gg1.UnidMed ' +
        ' '
      
        'LEFT JOIN bluederm_meza.stqcenloc  scl ON scl.Controle=iri.StqCe' +
        'nLoc '
      'LEFT JOIN bluederm_meza.stqcencad  scc ON scc.Codigo=scl.Codigo '
      'WHERE iri.Level1=2000'
      'ORDER BY iri.MovimNiv, iri.DataHora, iri.Controle ')
    Left = 296
    Top = 420
    object QrImeisIMEIIni: TIntegerField
      FieldName = 'IMEIIni'
    end
    object QrImeisSeq: TIntegerField
      FieldName = 'Seq'
    end
    object QrImeisLevel1: TIntegerField
      FieldName = 'Level1'
    end
    object QrImeisLevel2: TIntegerField
      FieldName = 'Level2'
    end
    object QrImeisLevel3: TIntegerField
      FieldName = 'Level3'
    end
    object QrImeisControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrImeisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImeisMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrImeisMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrImeisMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrImeisEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrImeisTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrImeisCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrImeisMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrImeisDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrImeisPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrImeisGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrImeisPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrImeisPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrImeisAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrImeisAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrImeisValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrImeisSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrImeisSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrImeisSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrImeisSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrImeisSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrImeisSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrImeisSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrImeisObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrImeisSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrImeisFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrImeisMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrImeisFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrImeisDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrImeisDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrImeisDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrImeisDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrImeisQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrImeisQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrImeisQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrImeisQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrImeisQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrImeisQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrImeisQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrImeisQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrImeisNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrImeisMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrImeisReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrImeisStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrImeisItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrImeisVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrImeisClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrImeisNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrImeisNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrImeisVSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
    object QrImeisPrcMovID: TIntegerField
      FieldName = 'PrcMovID'
    end
    object QrImeisPrcMovNiv: TIntegerField
      FieldName = 'PrcMovNiv'
    end
    object QrImeisPrcNivel1: TIntegerField
      FieldName = 'PrcNivel1'
    end
    object QrImeisPrcNivel2: TIntegerField
      FieldName = 'PrcNivel2'
    end
    object QrImeisPrcPeca: TFloatField
      FieldName = 'PrcPeca'
    end
    object QrImeisPrcPeso: TFloatField
      FieldName = 'PrcPeso'
    end
    object QrImeisPrcArM2: TFloatField
      FieldName = 'PrcArM2'
    end
    object QrImeisPrcKnd: TIntegerField
      FieldName = 'PrcKnd'
    end
    object QrImeisPrcRend: TFloatField
      FieldName = 'PrcRend'
    end
    object QrImeisLvl1Ant: TIntegerField
      FieldName = 'Lvl1Ant'
    end
    object QrImeisAtivo: TIntegerField
      FieldName = 'Ativo'
    end
    object QrImeisNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object QrImeisNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 103
    end
    object QrImeisNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object frxDsImeis: TfrxDBDataset
    UserName = 'frxDsImeis'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IMEIIni=IMEIIni'
      'Seq=Seq'
      'Level1=Level1'
      'Level2=Level2'
      'Level3=Level3'
      'Controle=Controle'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'NFeSer=NFeSer'
      'NFeNum=NFeNum'
      'VSMulNFeCab=VSMulNFeCab'
      'PrcMovID=PrcMovID'
      'PrcMovNiv=PrcMovNiv'
      'PrcNivel1=PrcNivel1'
      'PrcNivel2=PrcNivel2'
      'PrcPeca=PrcPeca'
      'PrcPeso=PrcPeso'
      'PrcArM2=PrcArM2'
      'PrcKnd=PrcKnd'
      'PrcRend=PrcRend'
      'Lvl1Ant=Lvl1Ant'
      'Ativo=Ativo'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_LOC_CEN=NO_LOC_CEN'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR')
    DataSet = QrImeis
    BCDToCurrency = False
    Left = 296
    Top = 464
  end
  object frxWET_CURTI_153_C: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'procedure Memo13OnPreviewDblClick(Sender: TfrxView; Button: TMou' +
        'seButton; Shift: Integer; var Modified: Boolean);'
      'begin'
      '  MyFunc(<frxDsRend."Ctrl22">);        '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_153_AGetValue
    OnUserFunction = frxWET_CURTI_153_AUserFunction
    Left = 300
    Top = 376
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsImeis
        DataSetName = 'frxDsImeis'
      end
      item
        DataSet = frxDsLevel2
        DataSetName = 'frxDsLevel2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RENDIMENTO DE COURO SEMI ACABADO ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 143.622140000000000000
          Top = 64.252010000000000000
          Width = 170.078752360000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 86.929190000000000000
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '????')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 529.134200000000000000
          Top = 64.252010000000000000
          Width = 52.913388270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 480.000310000000000000
          Top = 64.252010000000000000
          Width = 49.133850940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo19: TfrxMemoView
          Left = 313.700990000000000000
          Top = 64.252010000000000000
          Width = 56.692930470000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NFeRem')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 430.866420000000000000
          Top = 64.252010000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo30: TfrxMemoView
          Left = 279.685220000000000000
          Top = 41.574830000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '???')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Top = 64.252010000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo70: TfrxMemoView
          Left = 650.079160000000000000
          Top = 64.252010000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo21: TfrxMemoView
          Left = 370.393940000000000000
          Top = 64.252010000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 582.047620000000000000
          Top = 64.252010000000000000
          Width = 52.913388270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Rendim.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 30.236240000000000000
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 442.205010000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 389.291590000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000000000
          Width = 309.921264720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD01: TfrxMasterData
        FillType = ftBrush
        Height = 37.795275590000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        DataSet = frxDsLevel2
        DataSetName = 'frxDsLevel2'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 30.236240000000000000
          Width = 56.692950000000000000
          Height = 18.897637800000000000
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nivel ')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 86.929190000000000000
          Width = 226.771800000000000000
          Height = 18.897637800000000000
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o do n'#237'vel')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 370.393940000000000000
          Width = 60.472480000000000000
          Height = 18.897637800000000000
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 430.866420000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 480.000310000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 30.236240000000000000
          Top = 18.897650000000000000
          Width = 56.692950000000000000
          Height = 18.897637800000000000
          DataField = 'Level2'
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLevel2."Level2"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 86.929190000000000000
          Top = 18.897650000000000000
          Width = 226.771800000000000000
          Height = 18.897637800000000000
          DataField = 'NO_Level2'
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLevel2."NO_Level2"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 370.393940000000000000
          Top = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 18.897637800000000000
          DataField = 'Pecas'
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLevel2."Pecas"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 430.866420000000000000
          Top = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          DataField = 'AreaM2'
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLevel2."AreaM2"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 480.000310000000000000
          Top = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 18.897637800000000000
          DataField = 'PesoKg'
          DataSet = frxDsLevel2
          DataSetName = 'frxDsLevel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLevel2."PesoKg"]')
          ParentFont = False
        end
      end
      object DD01: TfrxDetailData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 272.126160000000000000
        Width = 680.315400000000000000
        DataSet = frxDsImeis
        DataSetName = 'frxDsImeis'
        RowCount = 0
        object Memo13: TfrxMemoView
          Left = 143.622140000000000000
          Width = 170.078752360000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsImeis
          DataSetName = 'frxDsImeis'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImeis."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo17: TfrxMemoView
          Left = 86.929190000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsImeis
          DataSetName = 'frxDsImeis'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImeis."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo18: TfrxMemoView
          Left = 313.700990000000000000
          Width = 56.692930470000000000
          Height = 13.228346460000000000
          DataField = 'NFeNum'
          DataSet = frxDsImeis
          DataSetName = 'frxDsImeis'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsImeis."NFeNum"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo23: TfrxMemoView
          Left = 370.393940000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DataField = 'Pecas'
          DataSet = frxDsImeis
          DataSetName = 'frxDsImeis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsImeis."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo24: TfrxMemoView
          Left = 419.527830000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsImeis
          DataSetName = 'frxDsImeis'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsImeis."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo26: TfrxMemoView
          Left = 30.236240000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsImeis
          DataSetName = 'frxDsImeis'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImeis."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          Left = 480.000310000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsImeis
          DataSetName = 'frxDsImeis'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsImeis."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GH01: TfrxGroupHeader
        FillType = ftBrush
        Height = 28.346466460000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsImeis."MovimNiv"'
        object Memo28: TfrxMemoView
          Left = 143.622140000000000000
          Top = 15.118120000000000000
          Width = 170.078752360000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Left = 86.929190000000000000
          Top = 15.118120000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo32: TfrxMemoView
          Left = 313.700990000000000000
          Top = 15.118120000000000000
          Width = 56.692930470000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'NFe')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo33: TfrxMemoView
          Left = 370.393940000000000000
          Top = 15.118120000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo34: TfrxMemoView
          Top = 15.118120000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo35: TfrxMemoView
          Left = 650.079160000000000000
          Top = 15.118120000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo36: TfrxMemoView
          Left = 419.527830000000000000
          Top = 15.118120000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          Left = 30.236240000000000000
          Top = 15.118120000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo38: TfrxMemoView
          Left = 480.000310000000000000
          Top = 15.118120000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo42: TfrxMemoView
          Width = 680.315302360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'N'#237'vel do movimento: [frxDsImeis."MovimNiv"] - [frxDsImeis."NO_Mo' +
              'vimNiv"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GF01: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          Left = 370.393940000000000000
          Width = 49.133855830000000000
          Height = 13.228346460000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsImeis."Pecas">,DD01)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo40: TfrxMemoView
          Left = 419.527830000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsImeis."AreaM2">,DD01)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          Left = 480.000310000000000000
          Width = 60.472448270000000000
          Height = 13.228346460000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsImeis."PesoKg">,DD01)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
    end
  end
  object QrRnd2: TmySQLQuery
   
    SQL.Strings = (
      'SELECT iri.*,'
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR '
      'FROM _vs_imp_rend_imei_ iri '
      
        'LEFT JOIN bluederm_meza.gragrux   ggx ON ggx.Controle=iri.GraGru' +
        'X '
      
        'LEFT JOIN bluederm_meza.gragruc    ggc ON ggc.Controle=ggx.GraGr' +
        'uC  '
      
        'LEFT JOIN bluederm_meza.gracorcad  gcc ON gcc.Codigo=ggc.GraCorC' +
        'ad  '
      
        'LEFT JOIN bluederm_meza.gratamits  gti ON gti.Controle=ggx.GraTa' +
        'mI  '
      
        'LEFT JOIN bluederm_meza.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1' +
        '  '
      
        'LEFT JOIN bluederm_meza.unidmed   unm ON unm.Codigo=gg1.UnidMed ' +
        ' '
      
        'LEFT JOIN bluederm_meza.gragruxcou xco ON xco.GraGruX=ggx.Contro' +
        'le '
      'LEFT JOIN bluederm_meza.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2'
      'LEFT JOIN bluederm_meza.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1'
      'WHERE iri.PrcKnd > 0'
      'ORDER BY iri.MovimNiv, xco.CouNiv2, iri.DataHora, iri.Controle ')
    Left = 592
    Top = 472
    object QrRnd2IMEIIni: TIntegerField
      FieldName = 'IMEIIni'
    end
    object QrRnd2Seq: TIntegerField
      FieldName = 'Seq'
    end
    object QrRnd2Level1: TIntegerField
      FieldName = 'Level1'
    end
    object QrRnd2Level2: TIntegerField
      FieldName = 'Level2'
    end
    object QrRnd2Level3: TIntegerField
      FieldName = 'Level3'
    end
    object QrRnd2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrRnd2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRnd2MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrRnd2MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrRnd2MovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrRnd2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrRnd2Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrRnd2CliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrRnd2MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrRnd2DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrRnd2Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrRnd2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrRnd2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrRnd2PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrRnd2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrRnd2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrRnd2ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrRnd2SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrRnd2SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrRnd2SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrRnd2SrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrRnd2SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrRnd2SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrRnd2SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrRnd2Observ: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrRnd2SerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrRnd2Ficha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrRnd2Misturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrRnd2FornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrRnd2DstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrRnd2DstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrRnd2DstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrRnd2DstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrRnd2QtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrRnd2QtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrRnd2QtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrRnd2QtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrRnd2QtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrRnd2QtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrRnd2QtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrRnd2QtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrRnd2NotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrRnd2Marca: TWideStringField
      FieldName = 'Marca'
    end
    object QrRnd2ReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrRnd2StqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrRnd2ItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrRnd2VSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrRnd2ClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrRnd2NFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrRnd2NFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrRnd2VSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
    object QrRnd2PrcMovID: TIntegerField
      FieldName = 'PrcMovID'
    end
    object QrRnd2PrcMovNiv: TIntegerField
      FieldName = 'PrcMovNiv'
    end
    object QrRnd2PrcNivel1: TIntegerField
      FieldName = 'PrcNivel1'
    end
    object QrRnd2PrcNivel2: TIntegerField
      FieldName = 'PrcNivel2'
    end
    object QrRnd2PrcPeca: TFloatField
      FieldName = 'PrcPeca'
    end
    object QrRnd2PrcPeso: TFloatField
      FieldName = 'PrcPeso'
    end
    object QrRnd2PrcArM2: TFloatField
      FieldName = 'PrcArM2'
    end
    object QrRnd2PrcKnd: TIntegerField
      FieldName = 'PrcKnd'
    end
    object QrRnd2PrcRend: TFloatField
      FieldName = 'PrcRend'
    end
    object QrRnd2Lvl1Ant: TIntegerField
      FieldName = 'Lvl1Ant'
    end
    object QrRnd2Ativo: TIntegerField
      FieldName = 'Ativo'
    end
    object QrRnd2NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrRnd2NO_PrcKnd: TWideStringField
      FieldName = 'NO_PrcKnd'
      Size = 10
    end
    object QrRnd2NO_GG1_GTI: TWideStringField
      FieldName = 'NO_GG1_GTI'
      Size = 120
    end
    object QrRnd2GG1_Tam: TWideStringField
      FieldName = 'GG1_Tam'
      Size = 25
    end
  end
  object frxDsRnd2: TfrxDBDataset
    UserName = 'frxDsRnd2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IMEIIni=IMEIIni'
      'Seq=Seq'
      'Level1=Level1'
      'Level2=Level2'
      'Level3=Level3'
      'Controle=Controle'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'NFeSer=NFeSer'
      'NFeNum=NFeNum'
      'VSMulNFeCab=VSMulNFeCab'
      'PrcMovID=PrcMovID'
      'PrcMovNiv=PrcMovNiv'
      'PrcNivel1=PrcNivel1'
      'PrcNivel2=PrcNivel2'
      'PrcPeca=PrcPeca'
      'PrcPeso=PrcPeso'
      'PrcArM2=PrcArM2'
      'PrcKnd=PrcKnd'
      'PrcRend=PrcRend'
      'Lvl1Ant=Lvl1Ant'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_PrcKnd=NO_PrcKnd'
      'NO_GG1_GTI=NO_GG1_GTI'
      'GG1_Tam=GG1_Tam')
    DataSet = QrRnd2
    BCDToCurrency = False
    Left = 592
    Top = 520
  end
end
