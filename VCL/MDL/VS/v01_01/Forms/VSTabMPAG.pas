unit VSTabMPAG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, frxClass, frxCross,
  frxDBSet;

type
  TFmVSTabMPAG = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrTab: TMySQLQuery;
    Panel2: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    EdPesoKgIni: TdmkEdit;
    EdFatorMP: TdmkEdit;
    LaFatorMP: TLabel;
    LaFatorAR: TLabel;
    EdFatorAR: TdmkEdit;
    EdPerCompe: TdmkEdit;
    Label8: TLabel;
    EdPesoKgFim: TdmkEdit;
    Label1: TLabel;
    EdPesoKgStp: TdmkEdit;
    Label3: TLabel;
    RGMP: TRadioGroup;
    RGAR: TRadioGroup;
    Label4: TLabel;
    EdPerCura: TdmkEdit;
    SGTabela: TStringGrid;
    SbFatorMP: TSpeedButton;
    SbFatorAR: TSpeedButton;
    BtImprime: TBitBtn;
    PB1: TProgressBar;
    frxWET_CURTI_263: TfrxReport;
    QrTabLinha: TIntegerField;
    QrTabPesoKg_A: TWideStringField;
    QrTabAreaM2_A: TWideStringField;
    QrTabAreaP2_A: TWideStringField;
    QrTabKgM2_A: TWideStringField;
    QrTabPesoKg_B: TWideStringField;
    QrTabAreaM2_B: TWideStringField;
    QrTabAreaP2_B: TWideStringField;
    QrTabKgM2_B: TWideStringField;
    QrTabPesoKg_C: TWideStringField;
    QrTabAreaM2_C: TWideStringField;
    QrTabAreaP2_C: TWideStringField;
    QrTabKgM2_C: TWideStringField;
    QrTabPesoKg_D: TWideStringField;
    QrTabAreaM2_D: TWideStringField;
    QrTabAreaP2_D: TWideStringField;
    QrTabKgM2_D: TWideStringField;
    QrTabPesoKg_E: TWideStringField;
    QrTabAreaM2_E: TWideStringField;
    QrTabAreaP2_E: TWideStringField;
    QrTabKgM2_E: TWideStringField;
    QrTabAtivo: TSmallintField;
    frxDsTab: TfrxDBDataset;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGMPClick(Sender: TObject);
    procedure RGARClick(Sender: TObject);
    procedure EdPesoKgIniRedefinido(Sender: TObject);
    procedure EdPesoKgFimRedefinido(Sender: TObject);
    procedure EdPesoKgStpRedefinido(Sender: TObject);
    procedure EdPerCompeRedefinido(Sender: TObject);
    procedure EdPerCuraRedefinido(Sender: TObject);
    procedure SbFatorMPClick(Sender: TObject);
    procedure SbFatorARClick(Sender: TObject);
    procedure EdFatorMPRedefinido(Sender: TObject);
    procedure EdFatorARRedefinido(Sender: TObject);
    procedure frxReport1BeforePrint(Sender: TfrxReportComponent);
    procedure frxReport3BeforePrint(Sender: TfrxReportComponent);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxWET_CURTI_263GetValue(const VarName: string;
      var Value: Variant);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    sVSTabMPAG: String;
    //
    procedure LimpaGrade();
  public
    { Public declarations }
  end;

  var
  FmVSTabMPAG: TFmVSTabMPAG;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_PF, MyDBCheck, CreateVS, ModuleGeral;

{$R *.DFM}

procedure TFmVSTabMPAG.BitBtn1Click(Sender: TObject);
const
  Linhas = 4;
var
  I, J, LMul, Linha: Integer;
  SQL: String;
begin
  Screen.Cursor := crHourGlass;
  try
  MyObjects.Informa2(Label1, Label2, True, 'Gerando dados');
  sVSTabMPAG :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSTabMPAG, DModG.QrUpdPID1, False);
  //
  LMul := 0;
  Linha := 0;
  //PB1.Position := 0;
  //PB1.Max := (SGtabela.RowCount + 1) div Linhas;
  //
  SQL := 'INSERT INTO ' + sVSTabMPAG +
  ' (Linha, ' +
  'PesoKg_A, AreaM2_A, AreaP2_A, KgM2_A, ' +
  'PesoKg_B, AreaM2_B, AreaP2_B, KgM2_B, ' +
  'PesoKg_C, AreaM2_C, AreaP2_C, KgM2_C, ' +
  'PesoKg_D, AreaM2_D, AreaP2_D, KgM2_D ' +
  ') VALUES ';
  while LMul < SGtabela.RowCount -1 do
  begin
    Linha := Linha + 1;
    //MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    if Linha = 1 then
      SQL := SQL + '('
    else
      SQL := SQL + ', (';

    SQL := SQL + Geral.FF0(Linha) + ', "' +
    SGTabela.Cells[0, LMul+1] + '", "' + SGTabela.Cells[1, LMul+1] + '", "' + SGTabela.Cells[2, LMul+1] + '", "' + SGTabela.Cells[3, LMul+1] + '", "' +
    SGTabela.Cells[0, LMul+2] + '", "' + SGTabela.Cells[1, LMul+2] + '", "' + SGTabela.Cells[2, LMul+2] + '", "' + SGTabela.Cells[3, LMul+2] + '", "' +
    SGTabela.Cells[0, LMul+3] + '", "' + SGTabela.Cells[1, LMul+3] + '", "' + SGTabela.Cells[2, LMul+3] + '", "' + SGTabela.Cells[3, LMul+3] + '", "' +
    SGTabela.Cells[0, LMul+4] + '", "' + SGTabela.Cells[1, LMul+4] + '", "' + SGTabela.Cells[2, LMul+4] + '", "' + SGTabela.Cells[3, LMul+4] + '")';
    //
    LMul := LMul + Linhas;
  end;
  //
  MyObjects.Informa2(Label1, Label2, True, 'Inserindo dados');
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  //
  MyObjects.Informa2(Label1, Label2, True, 'Gerando relat�rio');
  UnDmkDAC_PF.AbreMySQLQuery0(QrTab, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM _vs_tab_mpag_ ',
  'ORDER BY Linha ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_263, [
  DModG.frxDsDono,
  frxDsTab
  ]);
  MyObjects.frxMostra(frxWET_CURTI_263, 'Tabela de Rendimento');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSTabMPAG.BtImprimeClick(Sender: TObject);
const
  Linhas = 4;
var
  I, J, LMul, Linha: Integer;
  SQL: String;
begin
  Screen.Cursor := crHourGlass;
  try
  sVSTabMPAG :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSTabMPAG, DModG.QrUpdPID1, False);
  //
  LMul := 0;
  Linha := 0;
  PB1.Position := 0;
  PB1.Max := (SGtabela.RowCount + 1) div Linhas;
  while LMul < SGtabela.RowCount -1 do
  begin
    Linha := Linha + 1;
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    SQL := 'INSERT INTO ' + sVSTabMPAG +
    ' (Linha, ' +
    'PesoKg_A, AreaM2_A, AreaP2_A, KgM2_A, ' +
    'PesoKg_B, AreaM2_B, AreaP2_B, KgM2_B, ' +
    'PesoKg_C, AreaM2_C, AreaP2_C, KgM2_C, ' +
    'PesoKg_D, AreaM2_D, AreaP2_D, KgM2_D ' +
    ') VALUES (' +
    //
    Geral.FF0(Linha) + ', "' +
    SGTabela.Cells[0, LMul+1] + '", "' + SGTabela.Cells[1, LMul+1] + '", "' + SGTabela.Cells[2, LMul+1] + '", "' + SGTabela.Cells[3, LMul+1] + '", "' +
    SGTabela.Cells[0, LMul+2] + '", "' + SGTabela.Cells[1, LMul+2] + '", "' + SGTabela.Cells[2, LMul+2] + '", "' + SGTabela.Cells[3, LMul+2] + '", "' +
    SGTabela.Cells[0, LMul+3] + '", "' + SGTabela.Cells[1, LMul+3] + '", "' + SGTabela.Cells[2, LMul+3] + '", "' + SGTabela.Cells[3, LMul+3] + '", "' +
    SGTabela.Cells[0, LMul+4] + '", "' + SGTabela.Cells[1, LMul+4] + '", "' + SGTabela.Cells[2, LMul+4] + '", "' + SGTabela.Cells[3, LMul+4] + '")';
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
    //
    LMul := LMul + Linhas;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrTab, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM _vs_tab_mpag_ ',
  'ORDER BY Linha ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_263, [
  DModG.frxDsDono,
  frxDsTab
  ]);
  MyObjects.frxMostra(frxWET_CURTI_263, 'Tabela de Rendimento');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSTabMPAG.BtOKClick(Sender: TObject);
  function MediaKgM2(Peso, AreaM2: Double): Double;
  begin
    if AreaM2 = 0 then
      Result := 0
    else
      Result := Peso / AreaM2;
  end;
const
  Pecas = 1;
var
  PesoKgIni, PesoKgFim, Peso, PesoKgStp, FatorMP, FatorAR, AreaM2, AreaP2,
  KgM2, Nota, FatorCura, PerCura, PerCompe: Double;
  Linha: Integer;
  //I, Pi, Pf: Integer;
begin
  if MyObjects.FIC(RGMP.ItemIndex < 1, RGMP, 'Informe a mat�ria-prima') then Exit;
  if MyObjects.FIC(RGAR.ItemIndex < 1, RGAR, 'Informe o artigo') then Exit;
  if MyObjects.FIC(RGAR.ItemIndex < 1, RGAR, 'Informe o artigo') then Exit;
  PesoKgIni := EdPesoKgIni.ValueVariant;
  PesoKgFim := EdPesoKgFim.ValueVariant;
  if MyObjects.FIC(PesoKgIni >PesoKgFim, EdPesoKgIni,
    'O peso inicial n�o pode ser maior que o peso final!') then Exit;
  //
  PesoKgStp := EdPesoKgStp.ValueVariant;
  FatorMP := EdFatorMP.ValueVariant;
  FatorAR := EdFatorAR.ValueVariant;
  PerCura := EdPerCura.ValueVariant;
  FatorCura := 0.00;
  if RGMP.ItemIndex = 2 then
    FatorCura := 0.25 * (100 - PerCura) / 100;
  PerCompe := EdPerCompe.ValueVariant;
  Nota := 100.00 + PerCompe - 3.00;
  //
  Peso := PesoKgIni;
  Linha := 0;
  SGTabela.RowCount := Trunc(((PesoKgFim - PesoKgIni) / PesoKgStp) + 2.9999);
  while Peso <= PesoKgFim do
  begin
    AreaM2 := VS_PF.NotaCouroRibeiraApuca_Area(Pecas, Peso, (FatorMP - FatorCura), FatorAR, Nota);
    AreaP2 := Geral.ConverteArea(AreaM2, ctM2ToP2, cfQuarto);
    KgM2 := MediaKgM2(Peso, AreaM2);
    //
    Linha := Linha + 1;
    SGTabela.Cells[0, Linha] := Geral.FFT(Peso, 3, siPositivo);
    SGTabela.Cells[1, Linha] := Geral.FFT(AreaM2, 2, siPositivo);
    SGTabela.Cells[2, Linha] := Geral.FFT(AreaP2, 2, siPositivo);
    SGTabela.Cells[3, Linha] := Geral.FFT(KgM2, 3, siPositivo);
    // ...
    Peso := Peso + PesoKgStp;
  end;
  if SGTabela.RowCount > Linha + 1 then
    SGTabela.RowCount := Linha + 1;
end;

procedure TFmVSTabMPAG.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSTabMPAG.EdFatorARRedefinido(Sender: TObject);
begin
  LimpaGrade();
end;

procedure TFmVSTabMPAG.EdFatorMPRedefinido(Sender: TObject);
begin
  LimpaGrade();
end;

procedure TFmVSTabMPAG.EdPerCompeRedefinido(Sender: TObject);
begin
  LimpaGrade();
end;

procedure TFmVSTabMPAG.EdPerCuraRedefinido(Sender: TObject);
begin
  LimpaGrade();
end;

procedure TFmVSTabMPAG.EdPesoKgFimRedefinido(Sender: TObject);
begin
  LimpaGrade();
end;

procedure TFmVSTabMPAG.EdPesoKgIniRedefinido(Sender: TObject);
begin
  LimpaGrade();
end;

procedure TFmVSTabMPAG.EdPesoKgStpRedefinido(Sender: TObject);
begin
  LimpaGrade();
end;

procedure TFmVSTabMPAG.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSTabMPAG.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  SGTabela.Cells[0, 0] := 'Peso kg';
  SGTabela.Cells[1, 0] := '�rea m�';
  SGTabela.Cells[2, 0] := '�rea ft�';
  SGTabela.Cells[3, 0] := 'kg/m�';
end;

procedure TFmVSTabMPAG.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSTabMPAG.frxReport1BeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    for i := 1 to SGTabela.ColCount do
      for j := 1 to SGTabela.RowCount do
        //Cross.AddValue([i], [j], [SGTabela.Cells[i - 1, j - 1]]);
        Cross.AddValue([j], [i], [SGTabela.Cells[i - 1, j - 1]]);
  end;
end;

procedure TFmVSTabMPAG.frxReport3BeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j, n, ic, jc: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    n := 0;
      for j := 2 to SGTabela.Rowcount do
    for i := 1 to SGTabela.Colcount do
      //for j := 2 to SGTabela.Rowcount do
      begin
        n := n + 1;
        //
        jc := Trunc((n - 0.1) / 12) + 1;
        //
        ic := n mod 12;
        if ic = 0 then
          ic := 12;

        Cross.AddValue([jc], [ic], [SGTabela.Cells[i - 1, j - 1]]);
      end;
  end;
end;

procedure TFmVSTabMPAG.frxWET_CURTI_263GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_MP' then
    Value := RGMP.Items[RGMP.ItemIndex]
  else
  if VarName = 'VARF_AR' then
    Value := RGAR.Items[RGAR.ItemIndex]
  else
  if VarName = 'VARF_COMPENSACAO' then
    Value := EdPercompe.Text
  else
  if VarName = 'VARF_CURA' then
  begin
    if RGMP.ItemIndex = 2 then
      Value := 'Cura: ' + EdPerCura.Text + '%'
    else
      Value := '';
  end
  else
end;

procedure TFmVSTabMPAG.LimpaGrade();
begin
  MyObjects.LimpaGrade(SGTabela, 0, 1, True);
end;

procedure TFmVSTabMPAG.RGARClick(Sender: TObject);
begin
  case RGAR.ItemIndex of
    1: EdFatorAR.ValueVariant := 1.05;
    2: EdFatorAR.ValueVariant := 1.00;
    3: EdFatorAR.ValueVariant := 0.99;
    4: EdFatorAR.ValueVariant := 1.10;
    else EdFatorAR.ValueVariant := 0.00;
  end;
  LimpaGrade();
end;

procedure TFmVSTabMPAG.RGMPClick(Sender: TObject);
begin
  case RGMP.ItemIndex of
    1: EdFatorMP.ValueVariant := 1.00;
    2: EdFatorMP.ValueVariant := 1.25;
    else EdFatorMP.ValueVariant := 0.00;
  end;
  LimpaGrade();
end;

procedure TFmVSTabMPAG.SbFatorARClick(Sender: TObject);
begin
  if DBCheck.LiberaPelaSenhaBoss() then
  begin
    LaFatorAR.Enabled := True;
    EdFatorAR.Enabled := True;
  end;
end;

procedure TFmVSTabMPAG.SbFatorMPClick(Sender: TObject);
begin
  if DBCheck.LiberaPelaSenhaBoss() then
  begin
    LaFatorMP.Enabled := True;
    EdFatorMP.Enabled := True;
  end;
end;

end.
