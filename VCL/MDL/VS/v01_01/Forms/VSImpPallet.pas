unit VSImpPallet;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass,
  dmkGeral, UnInternalConsts, Data.DB, mySQLDbTables, frxDBSet,
  UnProjGroup_Consts, UnGrl_Consts, UnDmkEnums, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, dmkImage, Vcl.Grids, Vcl.DBGrids,
  UnProjGroup_Vars;

type
  TFmVSImpPallet = class(TForm)
    frxWET_CURTI_026_01_A: TfrxReport;
    QrEstqR5: TmySQLQuery;
    QrEstqR5Empresa: TIntegerField;
    QrEstqR5GraGruX: TIntegerField;
    QrEstqR5Pecas: TFloatField;
    QrEstqR5PesoKg: TFloatField;
    QrEstqR5AreaM2: TFloatField;
    QrEstqR5AreaP2: TFloatField;
    QrEstqR5GraGru1: TIntegerField;
    QrEstqR5NO_PRD_TAM_COR: TWideStringField;
    QrEstqR5Terceiro: TIntegerField;
    QrEstqR5NO_FORNECE: TWideStringField;
    QrEstqR5Pallet: TIntegerField;
    QrEstqR5NO_PALLET: TWideStringField;
    QrEstqR5ValorT: TFloatField;
    QrEstqR5Ativo: TSmallintField;
    QrEstqR5OrdGGX: TIntegerField;
    QrEstqR5NO_STATUS: TWideStringField;
    frxDsEstqR5: TfrxDBDataset;
    QrEstqR5NO_EMPRESA: TWideStringField;
    QrEstqR5DataHora: TDateTimeField;
    QrEstqR5NO_CliStat: TWideStringField;
    QrEstqR5CliStat: TIntegerField;
    QrEstqR5SdoVrtPeca: TFloatField;
    QrEstqR5SdoVrtPeso: TFloatField;
    QrEstqR5SdoVrtArM2: TFloatField;
    QrEstqR5PalVrtPeca: TFloatField;
    QrEstqR5PalVrtPeso: TFloatField;
    QrEstqR5PalVrtArM2: TFloatField;
    QrEstqR5LmbVrtPeca: TFloatField;
    QrEstqR5LmbVrtPeso: TFloatField;
    QrEstqR5LmbVrtArM2: TFloatField;
    QrEstqR5Status: TIntegerField;
    QrEstqR5OrdGGY: TIntegerField;
    QrEstqR5GraGruY: TIntegerField;
    QrEstqR5NO_GGY: TWideStringField;
    QrEstqR5NO_PalStat: TWideStringField;
    QrEstqR5Media: TFloatField;
    QrEstqR5PalStat: TIntegerField;
    QrVSCacIts: TmySQLQuery;
    QrVSCacItsNO_REVISOR: TWideStringField;
    QrVSCacItsPecas: TFloatField;
    frxWET_CURTI_026_02: TfrxReport;
    QrGGXPalTer: TmySQLQuery;
    frxDsGGXPalTer: TfrxDBDataset;
    QrGGXPalTerSdoVrtPeca: TFloatField;
    QrGGXPalTerSdoVrtPeso: TFloatField;
    QrGGXPalTerSdoVrtArM2: TFloatField;
    QrGGXPalTerTerceiro: TIntegerField;
    QrGGXPalTerGraGruX: TIntegerField;
    QrGGXPalTerPallet: TIntegerField;
    QrGGXPalTerSerieFch: TIntegerField;
    QrGGXPalTerFicha: TIntegerField;
    QrGGXPalTerNO_SerieFch: TWideStringField;
    QrGGXPalTerNO_PRD_TAM_COR: TWideStringField;
    QrGGXPalTerNO_Pallet: TWideStringField;
    QrGGXPalTerMedia: TFloatField;
    QrGGXPalTerInteiros: TFloatField;
    QrGGXPalTerNO_FORNECE: TWideStringField;
    QrMulFrn: TmySQLQuery;
    QrMulFrnFornece: TIntegerField;
    QrMulFrnPecas: TFloatField;
    QrMulFrnSiglaVS: TWideStringField;
    QrTotFrn: TmySQLQuery;
    QrTotFrnPecas: TFloatField;
    frxWET_CURTI_026_03_C_100x150: TfrxReport;
    frxReport1: TfrxReport;
    QrEstqR5TEXTO_QRCODE: TWideStringField;
    frxWET_CURTI_026_04: TfrxReport;
    QrEstqR5PAL_TXT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox1: TGroupBox;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    TbVSMovImp4: TmySQLTable;
    DsVSMovImp4: TDataSource;
    DBGrid1: TDBGrid;
    TbVSMovImp4Empresa: TIntegerField;
    TbVSMovImp4GraGruX: TIntegerField;
    TbVSMovImp4Pecas: TFloatField;
    TbVSMovImp4PesoKg: TFloatField;
    TbVSMovImp4AreaM2: TFloatField;
    TbVSMovImp4AreaP2: TFloatField;
    TbVSMovImp4ValorT: TFloatField;
    TbVSMovImp4SdoVrtPeca: TFloatField;
    TbVSMovImp4SdoVrtPeso: TFloatField;
    TbVSMovImp4SdoVrtArM2: TFloatField;
    TbVSMovImp4LmbVrtPeca: TFloatField;
    TbVSMovImp4LmbVrtPeso: TFloatField;
    TbVSMovImp4LmbVrtArM2: TFloatField;
    TbVSMovImp4GraGru1: TIntegerField;
    TbVSMovImp4NO_PRD_TAM_COR: TWideStringField;
    TbVSMovImp4Pallet: TIntegerField;
    TbVSMovImp4NO_PALLET: TWideStringField;
    TbVSMovImp4Terceiro: TIntegerField;
    TbVSMovImp4CliStat: TIntegerField;
    TbVSMovImp4Status: TIntegerField;
    TbVSMovImp4NO_FORNECE: TWideStringField;
    TbVSMovImp4NO_CLISTAT: TWideStringField;
    TbVSMovImp4NO_EMPRESA: TWideStringField;
    TbVSMovImp4NO_STATUS: TWideStringField;
    TbVSMovImp4DataHora: TDateTimeField;
    TbVSMovImp4OrdGGX: TIntegerField;
    TbVSMovImp4OrdGGY: TIntegerField;
    TbVSMovImp4GraGruY: TIntegerField;
    TbVSMovImp4NO_GGY: TWideStringField;
    TbVSMovImp4PalStat: TIntegerField;
    TbVSMovImp4CouNiv2: TIntegerField;
    TbVSMovImp4CouNiv1: TIntegerField;
    TbVSMovImp4NO_CouNiv2: TWideStringField;
    TbVSMovImp4NO_CouNiv1: TWideStringField;
    TbVSMovImp4FatorInt: TFloatField;
    TbVSMovImp4SdoInteiros: TFloatField;
    TbVSMovImp4LmbInteiros: TFloatField;
    TbVSMovImp4Codigo: TIntegerField;
    TbVSMovImp4IMEC: TIntegerField;
    TbVSMovImp4IMEI: TIntegerField;
    TbVSMovImp4MovimID: TIntegerField;
    TbVSMovImp4MovimNiv: TIntegerField;
    TbVSMovImp4NO_MovimID: TWideStringField;
    TbVSMovImp4NO_MovimNiv: TWideStringField;
    TbVSMovImp4SerieFch: TIntegerField;
    TbVSMovImp4NO_SerieFch: TWideStringField;
    TbVSMovImp4Ficha: TIntegerField;
    TbVSMovImp4StatPall: TIntegerField;
    TbVSMovImp4NO_StatPall: TWideStringField;
    TbVSMovImp4NFeSer: TSmallintField;
    TbVSMovImp4NFeNum: TIntegerField;
    TbVSMovImp4VSMulNFeCab: TIntegerField;
    TbVSMovImp4StqCenCad: TIntegerField;
    TbVSMovImp4NO_StqCenCad: TWideStringField;
    TbVSMovImp4StqCenLoc: TIntegerField;
    TbVSMovImp4NO_StqCenLoc: TWideStringField;
    TbVSMovImp4Ativo: TSmallintField;
    TbVSMovImp4NO_ARTIGO: TWideStringField;
    QrEstqR5SerieFch: TIntegerField;
    QrEstqR5Ficha: TIntegerField;
    QrEstqR5SdoVrtArP2: TFloatField;
    QrEstqR5IMEI: TIntegerField;
    frxWET_CURTI_026_01_B: TfrxReport;
    frxWET_CURTI_026_03_D_100x150: TfrxReport;
    QrEstqR5PAL_BARCODE: TWideStringField;
    frxWET_CURTI_026_03_A_100x150: TfrxReport;
    frxWET_CURTI_026_03_B_100x150: TfrxReport;
    frxWET_CURTI_026_03_A_095x220: TfrxReport;
    frxWET_CURTI_026_03_B_095x220: TfrxReport;
    frxWET_CURTI_026_03_C_095x220: TfrxReport;
    frxWET_CURTI_026_03_D_095x220: TfrxReport;
    procedure frxWET_CURTI_026_01_AGetValue(const VarName: string;
      var Value: Variant);
    procedure frxWET_CURTI_026_02GetValue(const VarName: string;
      var Value: Variant);
    procedure QrEstqR5CalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure TbVSMovImp4BeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    FVSImpRandStr: String;
    FJaDescript_VSImpRandStr: Boolean;
    //FVSMovImp5: String;
    //
    function  SQL_ListaPallets(): String;
    //
    function  EncodeDadosFicha(): String;
    function  Obtem_VSImpRandStr(): String;
  public
    { Public declarations }
    FVSMovImp4: String;
    FEmpresa_Cod: Integer;
    FEmpresa_Txt: String;
    FPallets: array of Integer;
    FInfoNO_PALLET, (*FTermica,*) FImpEmpresa: Boolean;
    FDestImprFichaPallet: TDestImprFichaPallet;
    //
    procedure PreparaPallets(VSMovImp4, VSLstPalBox: String;
              EmptyLabels: Integer = 0);
    procedure ImprimePallets(VSMovImp4: String; Ordem: TORD_SQL);
    procedure ImprimeIMEIsPallets(Ordenacao: Integer);
    procedure ImprimeTermica(DestImprFichaPallet: TDestImprFichaPallet);
    procedure ReopenGraGruX();
  end;

var
  FmVSImpPallet: TFmVSImpPallet;

implementation


uses Module, ModuleGeral, DmkDAC_PF, UnMyObjects, CreateBlueDerm, ModVS_CRC,
  UnDmkProcFunc, UMySQLModule;

{$R *.dfm}

(*
function A_Set_Sensor_Mode(type_Renamed: char; continuous: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Set_Speed(speed: char): integer; stdcall; external 'WINPPLA.DLL';
function A_Set_Gap(gap: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Set_Backfeed(back: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Set_Darkness(darkness: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Set_Syssetting(transfer, cut_peel, length, zero, pause: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_CreatePrn(selection: integer; FileName: pchar): integer; stdcall; external 'WINPPLA.DLL';
function A_CreateUSBPort(nPort: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Print_Out(width, height, copies, amount: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Prn_Text(x, y, ori, font, typee, hor_factor, ver_factor: integer; mode: char; numeric: integer; data: pchar): integer; stdcall; external 'WINPPLA.DLL';
function A_Prn_Barcode(x, y, ori: integer; typee: char; narrow, width, height: integer; mode: char; numeric: integer; data: pchar): integer; stdcall; external 'WINPPLA.DLL';
function A_Prn_Text_TrueType(x, y, FSize: integer; FType: pchar; Fspin, FWeight, FItalic, FUnline, FStrikeOut: integer; id_name, data: pchar; mem_mode: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Prn_Text_TrueType_W(x, y, FHeight, FWidth: integer; FType: pchar; Fspin, FWeight, FItalic, FUnline, FStrikeOut: integer; id_name, data: pchar; mem_mode: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Get_Graphic(x, y, mem_mode: integer; format: char; filename: pchar): integer; stdcall; external 'WINPPLA.DLL';
function A_Draw_Box(mode, x, y, width, height, top, side: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Draw_Line(mode, x, y, width, height: integer): integer; stdcall; external 'WINPPLA.DLL';
procedure A_ClosePrn(); stdcall; external 'WINPPLA.DLL';
function A_GetUSBBufferLen(): integer; stdcall; external 'WINPPLA.DLL';
procedure A_Feed_Label(); stdcall; external 'WINPPLA.DLL';
*)
procedure TFmVSImpPallet.frxWET_CURTI_026_02GetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt: String;
  Invalido: Boolean;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FEmpresa_Txt, FEmpresa_Cod, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmVSImpPallet.BtOKClick(Sender: TObject);
begin
  ImprimePallets(FVSMovImp4, sqlordASC);
end;

procedure TFmVSImpPallet.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmVSImpPallet.EncodeDadosFicha(): String;
var
  I: Integer;
  Texto, ContraSenha: String;
begin
  Texto :=
    Geral.FF0(QrEstqR5Pallet.Value) + sLineBreak +
    Geral.FF0(QrEstqR5GraGruX.Value) + '-' + QrEstqR5NO_PRD_TAM_COR.Value + sLineBreak +
    QrEstqR5NO_PALLET.Value + sLineBreak +
    Geral.FFT(QrEstqR5Pecas.Value, 1, siNegativo) + ' p�' + sLineBreak +
    Geral.FFT(QrEstqR5AreaM2.Value, 2, siNegativo) + ' m�' + sLineBreak +
    Geral.FFT(QrEstqR5PesoKg.Value, 3, siNegativo) + ' kg' + sLineBreak;
  ContraSenha := Obtem_VSImpRandStr();
  if ContraSenha <> '' then
    Result :=  DmkPF.HexCripto(Texto, ContraSenha)
  else
    Result := Texto;
end;

procedure TFmVSImpPallet.FormCreate(Sender: TObject);
begin
  FVSImpRandStr := '';
  FImpEmpresa   := True;
  FJaDescript_VSImpRandStr := False;
end;

procedure TFmVSImpPallet.frxWET_CURTI_026_01_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_DV_PALLET_BARCODE' then
  begin
    Value := Geral.FFN(DmkPF.DigitoVerificardorDmk3CasasInteger(QrEstqR5IMEI.Value), 4)
  end else
  if VarName = 'VARF_NO_PALLET' then
  begin
    if FInfoNO_PALLET then
      Value := QrEstqR5NO_PALLET.Value
    else
    begin
      if Trim(QrEstqR5NO_PALLET.Value) <> '' then
        Value := '...'
      else
        Value := '';
    end;
  end;
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_VSNIVGER' then
    Value := '00000' //Dmod.QrControleVSNivGer.Value    <=== Nao usa! Tirar!
  else
  if VarName ='VARF_REVISORES' then
  begin
    Value := '';
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacIts, Dmod.MyDB, [
    'SELECT cia.Revisor, SUM(cia.Pecas) Pecas, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_REVISOR ',
    'FROM vscacitsa cia ',
    'LEFT JOIN entidades ent ON ent.Codigo=cia.Revisor ',
    'WHERE cia.VSPallet=' + Geral.FF0(QrEstqR5Pallet.Value),
    'GROUP BY cia.Revisor',
    '']);
    QrVSCacIts.First;
    while not QrVSCacIts.Eof do
    begin
      Value := Value + QrVSCacItsNO_REVISOR.Value + ' (' + FloatToStr(
        QrVSCacItsPecas.Value) + ')' + sLineBreak;
      //
      QrVSCacIts.Next;
    end;
  end else
  if VarName = 'VARF_FORNECEDORES' then
  begin
    if VAR_InfoMulFrnImpVS (*Dmod.QrControleInfoMulFrnImpVS.Value*) = 0 then
      Value := ''
    else
      Value := DmModVS_CRC.ObtemFornecedores_Pallet(QrEstqR5Pallet.Value);
  end else
  if VarName = 'VARF_QRCODE_1' then
  begin
    Value := EncodeDadosFicha();
  end else
  if VarName = 'VARF_EMPDESC' then
  begin
    if FImpEmpresa then
      Value := 'Fabricado por: '
    else
      Value := '';
  end else
  if VarName = 'VARF_EMPNOME' then
  begin
    if FImpEmpresa then
      Value := QrEstqR5NO_EMPRESA.Value
    else
      Value := '';
  end else
  if VarName = 'VARF_DV_PALLET' then
  begin
    Value := DmkPF.DigitoVerificardorDmk3CasasInteger(QrEstqR5IMEI.Value)
  end else
  if VarName = 'VARF_MEDIA_TIT' then
  begin
    if QrEstqR5PalVrtPeca.Value > 0 then
    begin
      if QrEstqR5PalVrtArM2.Value > 0 then
        Value := 'M�dia m�/pe�a'
      else
      if QrEstqR5PalVrtPeso.Value > 0 then
        Value := 'M�dia kg/pe�a';
    end else
      Value := 'M�dia m�/pe�a';
  end else
  if VarName = 'VARF_MEDIA_VAL' then
  begin
    if QrEstqR5PalVrtPeca.Value > 0 then
    begin
      if QrEstqR5PalVrtArM2.Value > 0 then
        Value := Geral.FFT(
          QrEstqR5PalVrtArM2.Value / QrEstqR5PalVrtPeca.Value, 2, siNegativo)
      else
      if QrEstqR5PalVrtPeso.Value > 0 then
        Value := Geral.FFT(
          QrEstqR5PalVrtPeso.Value / QrEstqR5PalVrtPeca.Value, 3, siNegativo)
    end else
      Value := '0.00';
  end else
  if VarName = 'TitPeso_kg' then
  begin
    if QrEstqR5PalVrtPeso.Value > 0.001 then
      Value := 'Peso kg'
    else
      Value := 'm�/pe�a';
  end else
  if VarName = 'ValPeso_kg' then
  begin
    if QrEstqR5PalVrtPeso.Value > 0.001 then
      Value := Geral.FFT(QrEstqR5PalVrtPeso.Value, 3, siNegativo)
    else
    begin
      if QrEstqR5PalVrtPeca.Value = 0 then
        Value := '0,00'
      else
        Value := Geral.FFT(QrEstqR5PalVrtArM2.Value / QrEstqR5PalVrtPeca.Value, 2, siNegativo);
    end;
  end else
end;

procedure TFmVSImpPallet.ImprimePallets(VSMovImp4: String; Ordem: TORD_SQL);
const
  sProcName = 'FmVSImpPallet.ImprimePallets()';
var
  SQL_Pallets: String;
  SQL_ORD: String;
  FrxToImp: TfrxReport;
begin
  SQL_Pallets := SQL_ListaPallets();
  if Trim(SQL_Pallets) = '' then
    SQL_Pallets := Geral.FF0(- High(Integer));
  SQL_Pallets :=  'AND mi4.Pallet IN (' + SQL_Pallets + ') ';
  //
  if Ordem = TORD_SQL.sqlordDESC then
    SQL_ORD :=   'ORDER BY Pallet DESC, PalStat '
  else
    SQL_ORD := 'ORDER BY OrdGGX ';
  //
  //fazer ficha e testar
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR5, DModG.MyPID_DB, [
  'SELECT mi4.Empresa, mi4.GraGruX, SUM(mi4.Pecas) Pecas, SUM(mi4.PesoKg) PesoKg, ',
  'Sum(mi4.AreaM2) AreaM2, SUM(mi4.AreaP2) AreaP2, SUM(mi4.ValorT) ValorT, ',
  'SUM(mi4.SdoVrtPeca) SdoVrtPeca, SUM(mi4.SdoVrtPeso) SdoVrtPeso, ',
  'Sum(mi4.SdoVrtArM2) SdoVrtArM2, ',
  'SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca) PalVrtPeca, ',
  'SUM(mi4.SdoVrtPeso+mi4.LmbVrtPeso) PalVrtPeso, ',
  'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) PalVrtArM2, ',
  'SUM(mi4.LmbVrtPeca) LmbVrtPeca, ',
  'SUM(mi4.LmbVrtPeso) LmbVrtPeso, SUM(mi4.LmbVrtArM2) LmbVrtArM2, ',
  'mi4.GraGru1, mi4.NO_PRD_TAM_COR, mi4.Pallet, mi4.NO_PALLET, mi4.Terceiro, ',
  'mi4.CliStat, mi4.Status, mi4.NO_FORNECE,  mi4.NO_CLISTAT, mi4.NO_EMPRESA, ',
  'mi4.NO_STATUS, mi4.DataHora, mi4.OrdGGX, mi4.OrdGGY, mi4.GraGruY, ',
  'mi4.NO_GGY, StatPall PalStat, NO_StatPall NO_PalStat, mi4.Ativo, ',
  'IF(SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca)  <> 0, ',
  'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) / SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca), 0) ',
  'Media, IF(mi4.Pallet < 1, "           ",',
  ' CONCAT("PALLET ", FORMAT(mi4.Pallet, 0))) PAL_TXT, ',
  'mi4.SerieFch, mi4.Ficha, mi4.IMEI, LPAD(mi4.Pallet, 10, "0") PAL_BARCODE ',
  'FROM  ' + VSMovImp4 + ' mi4',
  'WHERE ((mi4.SdoVrtPeca > 0 OR mi4.LmbVrtPeca <> 0)  ',
  //'AND mi4.Pallet IN (' + SQL_ListaPallets() + ') ',
  SQL_Pallets,
  ') OR (Ativo  <> 1) ',
  'GROUP BY mi4.Pallet ',
  SQL_ORD,
  //
  '']);
  //Geral.MB_SQL(Self, QrEstqR5);
    //
  case FDestImprFichaPallet of
    difpPapelA4:
    begin
      if Dmod.QrControleVSImpMediaPall.Value = 1 then
        frxToImp := frxWET_CURTI_026_01_B
      else
        frxToImp := frxWET_CURTI_026_01_A;
      //
      MyObjects.frxDefineDataSets(frxToImp, [
      DModG.frxDsDono,
      frxDsEstqR5
      ]);
      MyObjects.frxMostra(frxToImp, 'Pallet de Artigo de Ribeira');
   end;
   difpEtiq2x4:
   begin
      MyObjects.frxDefineDataSets(frxWET_CURTI_026_04, [
      DModG.frxDsDono,
      frxDsEstqR5
      ]);
      MyObjects.frxMostra(frxWET_CURTI_026_04, 'Pallet de Artigo de Ribeira');
   end;

   difpTermica10x15,
   difpTermica10x22:
   begin
   //if FTermica then
     ImprimeTermica(FDestImprFichaPallet);
   end;
   else Geral.MB_Erro('"FDestImprFichaPallet" indefinido em ' + sProcName)
 end;
end;

procedure TFmVSImpPallet.ImprimeIMEISPallets(Ordenacao: Integer);
var
  SQL, Campo1A, Campo1B, Campo2A, Campo2B, TitGru1, TitGru2: String;
  Grupo1, Grupo2: TfrxGroupHeader;
  Futer1, Futer2: TfrxGroupFooter;
  Me_GH1, Me_FT1, Me_GH2, Me_FT2: TfrxMemoView;
begin
  SQL := SQL_ListaPallets();
  //
  if Ordenacao = 1 then
  begin
    Campo1A := 'Pallet';
    Campo1B := 'Pallet';
    TitGru1 := 'Pallet';
    //
    Campo2A := 'Terceiro';
    Campo2B := 'NO_FORNECE';
    TitGru2 := 'Fornecedor';
  end else
  begin
    Campo1A := 'Terceiro';
    Campo1B := 'NO_FORNECE';
    TitGru1 := 'Fornecedor';
    //
    Campo2A := 'Pallet';
    Campo2B := 'Pallet';
    TitGru2 := 'Pallet';
  end;
  if SQL <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGGXPalTer, Dmod.MyDB, [
    'SELECT SUM(vmi.SdoVrtPeca) SdoVrtPeca,  ',
    'SUM(vmi.SdoVrtPeso) SdoVrtPeso, SUM(vmi.SdoVrtArM2) SdoVrtArM2,  ',
    'vmi.Terceiro, vmi.GraGruX, vmi.Pallet,  ',
    'vmi.SerieFch, vmi.Ficha, ',
    'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
    'NO_PRD_TAM_COR, wbp.Nome NO_Pallet, ',
    'SUM(vmi.SdoVrtArM2) / SUM(vmi.SdoVrtPeca) Media,  ',
    'SUM(vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) Inteiros, ',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN vspalleta  wbp ON wbp.Codigo=vmi.Pallet ',
    'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
    'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
    'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
    'WHERE vmi.SdoVrtPeca > 0',
    'AND vmi.Pallet IN (' + SQL + ')',
    'GROUP BY ' + Campo1A + ', ' + Campo2A,
    'ORDER BY ' + Campo1A + ', ' + Campo2A + ', NO_PRD_TAM_COR',
    '']);
    //Geral.MB_SQL(Self, QrGGXPalTer);
     //
    MyObjects.frxDefineDataSets(frxWET_CURTI_026_02, [
      DModG.frxDsDono,
      frxDsGGXPalTer
    ]);
    //
    Grupo1 := frxWET_CURTI_026_02.FindObject('GH001') as TfrxGroupHeader;
    Grupo1.Visible := True;
    Futer1 := frxWET_CURTI_026_02.FindObject('GF001') as TfrxGroupFooter;
    Futer1.Visible := Grupo1.Visible;
    Me_GH1 := frxWET_CURTI_026_02.FindObject('MeGrupo1Head') as TfrxMemoView;
    Me_FT1 := frxWET_CURTI_026_02.FindObject('MeGrupo1Foot') as TfrxMemoView;
    Grupo1.Condition := 'frxDsGGXPalTer."'+Campo1A+'"';
    Me_GH1.Memo.Text := TitGru1+': [frxDsGGXPalTer."'+Campo1B+'"]';
    Me_FT1.Memo.Text := TitGru1+': [frxDsGGXPalTer."'+Campo1B+'"]';
    //
    Grupo2 := frxWET_CURTI_026_02.FindObject('GH002') as TfrxGroupHeader;
    Grupo2.Visible := False;
    Futer2 := frxWET_CURTI_026_02.FindObject('GF002') as TfrxGroupFooter;
    Futer2.Visible := Grupo2.Visible;
(*
    Grupo2.Visible := True;
    Futer2.Visible := Grupo2.Visible;
    Me_GH2 := frxWET_CURTI_026_02.FindObject('MeGrupo2Head') as TfrxMemoView;
    Me_FT2 := frxWET_CURTI_026_02.FindObject('MeGrupo2Foot') as TfrxMemoView;
    Grupo2.Condition := 'frxDsGGXPalTer."'+Campo2A+'"';
    Me_GH2.Memo.Text := TitGru2+': [frxDsGGXPalTer."'+Campo2B+'"]';
    Me_FT2.Memo.Text := TitGru2+': [frxDsGGXPalTer."'+Campo2B+'"]';
*)
    //
    MyObjects.frxMostra(frxWET_CURTI_026_02, 'Lista Pallets x Fornecedor');
  end;
end;

procedure TFmVSImpPallet.PreparaPallets(VSMovImp4, VSLstPalBox: String;
  EmptyLabels: Integer);
const
  sProcName = 'FmVSImpPallet.ImprimePallets()';
var
  SQL_Empresa: String;
  I, Inteiro: Integer;
begin
  if FEmpresa_Cod <> 0 then
    SQL_Empresa := 'AND wmi.Empresa=' + Geral.FF0(FEmpresa_Cod)
  else
    SQL_Empresa := '';
  //
  for I := 0 to EmptyLabels - 1 do
  begin
    Inteiro := High(Integer) - I;
    //Inteiro := - (I + 1);
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, VSMovImp4, False,
    ['Ativo', 'OrdGGX'], ['Pallet'], [2, I + 1], [Inteiro], False);
  end;
  //
end;

procedure TFmVSImpPallet.ImprimeTermica(DestImprFichaPallet: TDestImprFichaPallet);
const
  sProcName = 'TFmVSImpPallet.ImprimeTermica()';
var
  Page: TfrxReportPage;
  //Page: TfrxPage;
  // Topo em branco
(*
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  BANLEF, BANTOP, BANWID, BANHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  //Align,
  L, T, W, H, i: Integer;
  Fator: Double;
*)
//begin
(*

  // Configura p�gina
  Page := frxWET_CURTI_026_03.FindObject('Page1') as TfrxReportPage;
  //Page := frxWET_CURTI_026_03.Pages[0];
  Page.LeftMargin   := 0;//QrPagePagLef.Value / VAR_DOTIMP;
  Page.RightMargin  := 0;
  Page.TopMargin    := 0;//QrPagePagTop.Value / VAR_DOTIMP;
  Page.BottomMargin := 0;
  //
  Page.PaperSize    := DMPAPER_USER;
  Page.PaperHeight  := 50;
  Page.PaperWidth   := 50;
  //
//begin
*)


{
var pa_porta: string;
  pa_nLen, pa_cortador, pa_tamanho_etiqueta: integer;
  pa_tem_cortador, pa_ribon, pa_papelcontinuo: boolean;
begin
  //setar variaveis da impressora
  pa_porta := 'USB';

  if pa_porta = 'USB' then // se a impressora � conectada na USB
  begin
    pa_nLen := A_GetUSBBufferLen();
    A_CreateUSBPort(1);
  end
  else
  begin
    A_CreatePrn(1, 'c:\lpt1.prn'); //se a impressora � LPT1
  end;

  A_Prn_Text_TrueType(20, 160, 60, 'Arial', 1, 0, 0, 0, 0, 'A1', pwidechar('XXXXX'), 1);
  A_Prn_Text_TrueType(250, 160, 60, 'Arial', 1, 0, 0, 0, 0, 'A11', pchar('10 UN'), 1);
  A_Prn_Text_TrueType(20, 135, 50, 'Impact', 1, 0, 0, 0, 0, 'A2', pchar('DESCRICAO DO MATERIAL 1'), 1);
  A_Prn_Text_TrueType(20, 115, 50, 'Impact', 1, 0, 0, 0, 0, 'A21', pchar('DESCRICAO DO MATERIAL 2'), 1);
  A_Prn_Text_TrueType(20, 90, 50, 'Arial', 1, 0, 0, 0, 0, 'A3', pchar('Endere�o: R01.01.01.01'), 1);
  A_Prn_Barcode(150, 15, 1, 'E', 2, 2, 30, 'N', 0, pchar('XXXXX'));
  //pa_tamanho_etiqueta := 170;
  pa_tamanho_etiqueta := 300;

  A_Set_Darkness(round(8));
  A_Set_Speed('G');
  //A_Set_Backfeed(round(170));

  pa_tem_cortador := false;
  pa_ribon := true;
  pa_papelcontinuo := False; //etiqueta ou papel continuo


  if pa_tem_cortador then
    pa_cortador := 1 //com_cortador de papel
  else
    pa_cortador := 9; //sem_cortador cortador de papel

  if pa_ribon then
  begin
    A_Set_Syssetting(2, pa_cortador, 0, 0, 0); //tem ribbon
  end
  else
  begin
    A_Set_Syssetting(1, pa_cortador, 0, 0, 0); //n�o tem ribbon
  end;

  if pa_papelcontinuo then
  begin
    A_Set_Sensor_Mode('c', round(pa_tamanho_etiqueta)); //papel continuo
  end
  else
  begin
    A_Set_Sensor_Mode('r', 0); //etiqueta
  end;

  if A_Print_Out(1, 1, 1, 1) <> 0 then
    MessageDlg('N�o conseguiu imprimir!', mtError, [mbOk], 0);

  A_ClosePrn();
end;
}
var
  Modelo: Integer;
  Report: TfrxReport;
begin
  Modelo := MyObjects.SelRadioGroup('Formato de Impress�o T�rmica',
  'Selecione o modelo', ['A', 'B', 'C', 'D'], 2, -1, True);
  case DestImprFichaPallet of //: TDestImprFichaPallet
    TDestImprFichaPallet.difpTermica10x15:
    begin
      case Modelo of
        0: Report := frxWET_CURTI_026_03_A_100x150; // S� o GraGruX (Cliente) e no final com LWG e com NO_PALLET
        1: Report := frxWET_CURTI_026_03_B_100x150; // S� o NO_PRD_TAM_COR (interno) e no final com LWG e com NO_PALLET
        2: Report := frxWET_CURTI_026_03_C_100x150; // A + B + Fornecedores, sem LWG, sem NO_PALLET
        3: Report := frxWET_CURTI_026_03_D_100x150; // A + B + NO_PALLET
        else
          Geral.MB_Erro('Tipo (letra) de etiqueta n�o implementado. ' + sProcName);
      end;
    end;
    TDestImprFichaPallet.difpTermica10x22:
    begin
      case Modelo of
        0: Report := frxWET_CURTI_026_03_A_095x220;
        1: Report := frxWET_CURTI_026_03_B_095x220;
        2: Report := frxWET_CURTI_026_03_C_095x220;
        3: Report := frxWET_CURTI_026_03_D_095x220;
        else
          Geral.MB_Erro('Tipo (letra) de etiqueta n�o implementado. ' + sProcName);
      end;
    end;
    else
      Geral.MB_Erro('Tamanho de etiqueta n�o implementado. ' + sProcName)
  end;
  //
  MyObjects.frxDefineDataSets(Report, [
  DModG.frxDsDono,
  frxDsEstqR5
  ]);
  MyObjects.frxMostra(Report, 'Pallet de Artigo de Ribeira');
end;

function TFmVSImpPallet.Obtem_VSImpRandStr(): String;
begin
  if FJaDescript_VSImpRandStr = False then
  begin
    FVSImpRandStr := DmkPF.HexDecripto(VAR_VSImpRandStr(*Dmod.QrControleVSImpRandStr.Value*), CO_RandStrWeb01);
    FJaDescript_VSImpRandStr := True;
  end;
  Result := FVSImpRandStr;
end;

procedure TFmVSImpPallet.QrEstqR5CalcFields(DataSet: TDataSet);
begin
  QrEstqR5TEXTO_QRCODE.Value := QrEstqR5NO_PRD_TAM_COR.Value +
    sLineBreak + QrEstqR5NO_PALLET.Value;
  QrEstqR5SdoVrtArP2.Value := Geral.ConverteArea(QrEstqR5SdoVrtArM2.Value, ctM2toP2, cfQuarto);
end;

procedure TFmVSImpPallet.ReopenGraGruX;
begin
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
end;

function TFmVSImpPallet.SQL_ListaPallets(): String;
var
  I, N: Integer;
begin
  Result := '';
  N := High(FPallets);
  for I := Low(FPallets) to N do
  begin
    Result := Result + sLineBreak + Geral.FF0(FPallets[I]);
    if I < N then
      Result := Result + ',';
  end;
end;

procedure TFmVSImpPallet.TbVSMovImp4BeforePost(DataSet: TDataSet);
begin
  if TbVSMovImp4Pecas.Value = 0 then
  begin
    TbVSMovImp4Ativo.Value := 2;
  end else
  begin
    TbVSMovImp4Ativo.Value := 3;
    if TbVSMovImp4AreaM2.Value <> 0 then
      TbVSMovImp4AreaP2.Value := Geral.ConverteArea(TbVSMovImp4AreaM2.Value, ctM2toP2, cfQuarto)
    else
    if TbVSMovImp4AreaP2.Value <> 0 then
      TbVSMovImp4AreaM2.Value := Geral.ConverteArea(TbVSMovImp4AreaP2.Value, ctP2toM2, cfCento);
    //
    TbVSMovImp4NO_PRD_TAM_COR.Value := TbVSMovImp4NO_ARTIGO.Value;
    TbVSMovImp4Empresa.Value := FEmpresa_Cod;
    TbVSMovImp4NO_Empresa.Value := FEmpresa_Txt;
    TbVSMovImp4SdoVrtPeca.Value := TbVSMovImp4Pecas.Value;
    TbVSMovImp4SdoVrtPeso.Value := TbVSMovImp4PesoKg.Value;
    TbVSMovImp4SdoVrtArM2.Value := TbVSMovImp4AreaM2.Value;
    TbVSMovImp4DataHora.Value   := Now();
  end;
end;

end.
