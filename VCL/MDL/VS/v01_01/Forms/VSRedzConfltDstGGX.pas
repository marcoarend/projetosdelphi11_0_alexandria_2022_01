unit VSRedzConfltDstGGX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  THackDBGrid = class(TDBGrid);
  TFmVSRedzConfltDstGGX = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DGDados: TdmkDBGridZTO;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmVSRedzConfltDstGGX: TFmVSRedzConfltDstGGX;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModVS, UnVS_PF;

{$R *.DFM}

procedure TFmVSRedzConfltDstGGX.BtOKClick(Sender: TObject);
var
  sControle, sGraGruX: String;
begin
  Screen.Cursor := crHourGlass;
  try
  DmModVS.QrRedzConfltDstGGX.First;
  while not DmModVS.QrRedzConfltDstGGX.Eof do
  begin
    sGraGruX  := Geral.FF0(DmModVS.QrRedzConfltDstGGXGraGruX.Value);
    sControle := Geral.FF0(DmModVS.QrRedzConfltDstGGXSrcCtrl.Value);
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      ' UPDATE vsmovits SET DstGGX=' + sGraGruX +
      ' WHERE Controle=' + sControle);
    //
    DmModVS.QrRedzConfltDstGGX.Next;
  end;
  DmModVS.QrRedzConfltDstGGX.Close;
  DmModVS.QrRedzConfltDstGGX.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSRedzConfltDstGGX.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRedzConfltDstGGX.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrRedzConfltDstGGXControle.Value)
  else
  if Campo = 'SrcCtrl' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrRedzConfltDstGGXSrcCtrl.Value);
end;

procedure TFmVSRedzConfltDstGGX.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSRedzConfltDstGGX.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DGDados.Datasource := DmModVS.DsRedzConfltDstGGX;
end;

procedure TFmVSRedzConfltDstGGX.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
