unit VSGerRclCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, dmkDBGridZTO,
  UnProjGroup_Consts, UnGrl_Vars, UnAppEnums;

type
  TFmVSGerRclCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrVSGerRcl: TmySQLQuery;
    DsVSGerRcl: TDataSource;
    QrVSGerRclNO_TIPO: TWideStringField;
    QrVSGerRclNO_DtHrFimCla: TWideStringField;
    QrVSGerRclNO_DtHrLibCla: TWideStringField;
    QrVSGerRclNO_DtHrCfgCla: TWideStringField;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    Outrasimpresses1: TMenuItem;
    QrVSGerRclCodigo: TIntegerField;
    QrVSGerRclMovimCod: TIntegerField;
    QrVSGerRclCacCod: TIntegerField;
    QrVSGerRclGraGruX: TIntegerField;
    QrVSGerRclVSPallet: TIntegerField;
    QrVSGerRclNome: TWideStringField;
    QrVSGerRclEmpresa: TIntegerField;
    QrVSGerRclDtHrLibCla: TDateTimeField;
    QrVSGerRclDtHrCfgCla: TDateTimeField;
    QrVSGerRclDtHrFimCla: TDateTimeField;
    QrVSGerRclTipoArea: TSmallintField;
    QrVSGerRclLk: TIntegerField;
    QrVSGerRclDataCad: TDateField;
    QrVSGerRclDataAlt: TDateField;
    QrVSGerRclUserCad: TIntegerField;
    QrVSGerRclUserAlt: TIntegerField;
    QrVSGerRclAlterWeb: TSmallintField;
    QrVSGerRclAtivo: TSmallintField;
    QrVSGerRclNO_EMPRESA: TWideStringField;
    QrVSGerRclNO_PRD_TAM_COR: TWideStringField;
    QrVSGerRclNO_PALLET: TWideStringField;
    QrVSPaRclCab: TmySQLQuery;
    DsVSPaRclCab: TDataSource;
    QrVSPaRclCabCodigo: TIntegerField;
    QrVSPaRclCabVSGerRcl: TIntegerField;
    QrVSPaRclCabVSMovIts: TIntegerField;
    QrVSPaRclCabVSPallet: TIntegerField;
    QrVSPaRclCabCacCod: TIntegerField;
    QrVSPaRclCabLstPal01: TIntegerField;
    QrVSPaRclCabLstPal02: TIntegerField;
    QrVSPaRclCabLstPal03: TIntegerField;
    QrVSPaRclCabLstPal04: TIntegerField;
    QrVSPaRclCabLstPal05: TIntegerField;
    QrVSPaRclCabLstPal06: TIntegerField;
    QrVSPaRclCabDtHrFimCla: TDateTimeField;
    QrVSPaRclCabLk: TIntegerField;
    QrVSPaRclCabDataCad: TDateField;
    QrVSPaRclCabDataAlt: TDateField;
    QrVSPaRclCabUserCad: TIntegerField;
    QrVSPaRclCabUserAlt: TIntegerField;
    QrVSPaRclCabAlterWeb: TSmallintField;
    QrVSPaRclCabAtivo: TSmallintField;
    QrVSPaRclIts: TmySQLQuery;
    DsVSPaRclIts: TDataSource;
    QrVSPaRclItsCodigo: TIntegerField;
    QrVSPaRclItsControle: TIntegerField;
    QrVSPaRclItsVSPallet: TIntegerField;
    QrVSPaRclItsVMI_Sorc: TIntegerField;
    QrVSPaRclItsVMI_Baix: TIntegerField;
    QrVSPaRclItsVMI_Dest: TIntegerField;
    QrVSPaRclItsTecla: TIntegerField;
    QrVSPaRclItsDtHrIni: TDateTimeField;
    QrVSPaRclItsDtHrFim: TDateTimeField;
    QrVSPaRclItsLk: TIntegerField;
    QrVSPaRclItsDataCad: TDateField;
    QrVSPaRclItsDataAlt: TDateField;
    QrVSPaRclItsUserCad: TIntegerField;
    QrVSPaRclItsUserAlt: TIntegerField;
    QrVSPaRclItsAlterWeb: TSmallintField;
    QrVSPaRclItsAtivo: TSmallintField;
    QrVSPaRclItsDtHrFim_TXT: TWideStringField;
    QrVSPaRclCabDtHrFimCla_TXT: TWideStringField;
    PMVSPaRclIts: TPopupMenu;
    EncerraPallet1: TMenuItem;
    QrVSPaRclCabPecas: TFloatField;
    QrVSPaRclCabAreaM2: TFloatField;
    QrVSPaRclCabAreaP2: TFloatField;
    QrSum: TmySQLQuery;
    QrSumPecas: TFloatField;
    QrSumAreaM2: TFloatField;
    QrSumAreaP2: TFloatField;
    QrVSPallet: TmySQLQuery;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    QrVSPalletLk: TIntegerField;
    QrVSPalletDataCad: TDateField;
    QrVSPalletDataAlt: TDateField;
    QrVSPalletUserCad: TIntegerField;
    QrVSPalletUserAlt: TIntegerField;
    QrVSPalletAlterWeb: TSmallintField;
    QrVSPalletAtivo: TSmallintField;
    QrVSPalletEmpresa: TIntegerField;
    QrVSPalletNO_EMPRESA: TWideStringField;
    QrVSPalletStatus: TIntegerField;
    QrVSPalletCliStat: TIntegerField;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletNO_CLISTAT: TWideStringField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletNO_STATUS: TWideStringField;
    QrVSPalletDtHrEndAdd: TDateTimeField;
    QrVSPalletDtHrEndAdd_TXT: TWideStringField;
    QrVSPalletQtdPrevPc: TIntegerField;
    DsVSPallet: TDataSource;
    GBDados: TGroupBox;
    Panel6: TPanel;
    Label32: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label23: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    Label33: TLabel;
    DBEdit25: TDBEdit;
    Label34: TLabel;
    DBEdit26: TDBEdit;
    Label1: TLabel;
    DBEdit0: TdmkDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label37: TLabel;
    DBEdit15: TDBEdit;
    DBEdit28: TDBEdit;
    QrSumPall: TmySQLQuery;
    DsSumPall: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label9: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit27: TDBEdit;
    PelaOC1: TMenuItem;
    QrVMI_Sorc: TmySQLQuery;
    DsVMI_Sorc: TDataSource;
    QrVMI_Baix: TmySQLQuery;
    DsVMI_Baix: TDataSource;
    QrVMI_SorcVMI_Sorc: TIntegerField;
    QrVMI_SorcPecas: TFloatField;
    QrVMI_SorcAreaM2: TFloatField;
    QrVMI_BaixVMI_Baix: TIntegerField;
    QrVMI_BaixPecas: TFloatField;
    QrVMI_BaixAreaM2: TFloatField;
    PMQuery: TPopupMenu;
    Peladescriao1: TMenuItem;
    Poroutrasinformaes1: TMenuItem;
    QrPal: TmySQLQuery;
    QrPalDtHrEndAdd: TDateTimeField;
    BtClassesGeradas: TBitBtn;
    BtCorrigeBaix: TBitBtn;
    QrVMI_BaixVMI_Sorc: TIntegerField;
    QrVMI_BaixVMI_Dest: TIntegerField;
    QrVMI_Src: TmySQLQuery;
    QrVMI_SrcCodigo: TIntegerField;
    QrVMI_SrcControle: TIntegerField;
    QrVMI_SrcMovimCod: TIntegerField;
    QrVMI_SrcMovimNiv: TIntegerField;
    QrVMI_SrcMovimTwn: TIntegerField;
    QrVMI_SrcEmpresa: TIntegerField;
    QrVMI_SrcTerceiro: TIntegerField;
    QrVMI_SrcCliVenda: TIntegerField;
    QrVMI_SrcMovimID: TIntegerField;
    QrVMI_SrcLnkNivXtr1: TIntegerField;
    QrVMI_SrcLnkNivXtr2: TIntegerField;
    QrVMI_SrcDataHora: TDateTimeField;
    QrVMI_SrcPallet: TIntegerField;
    QrVMI_SrcGraGruX: TIntegerField;
    QrVMI_SrcPecas: TFloatField;
    QrVMI_SrcPesoKg: TFloatField;
    QrVMI_SrcAreaM2: TFloatField;
    QrVMI_SrcAreaP2: TFloatField;
    QrVMI_SrcValorT: TFloatField;
    QrVMI_SrcSrcMovID: TIntegerField;
    QrVMI_SrcSrcNivel1: TIntegerField;
    QrVMI_SrcSrcNivel2: TIntegerField;
    QrVMI_SrcSrcGGX: TIntegerField;
    QrVMI_SrcSdoVrtPeca: TFloatField;
    QrVMI_SrcSdoVrtPeso: TFloatField;
    QrVMI_SrcSdoVrtArM2: TFloatField;
    QrVMI_SrcObserv: TWideStringField;
    QrVMI_SrcSerieFch: TIntegerField;
    QrVMI_SrcFicha: TIntegerField;
    QrVMI_SrcMisturou: TSmallintField;
    QrVMI_SrcFornecMO: TIntegerField;
    QrVMI_SrcCustoMOKg: TFloatField;
    QrVMI_SrcCustoMOTot: TFloatField;
    QrVMI_SrcValorMP: TFloatField;
    QrVMI_SrcDstMovID: TIntegerField;
    QrVMI_SrcDstNivel1: TIntegerField;
    QrVMI_SrcDstNivel2: TIntegerField;
    QrVMI_SrcDstGGX: TIntegerField;
    QrVMI_SrcQtdGerPeca: TFloatField;
    QrVMI_SrcQtdGerPeso: TFloatField;
    QrVMI_SrcQtdGerArM2: TFloatField;
    QrVMI_SrcQtdGerArP2: TFloatField;
    QrVMI_SrcQtdAntPeca: TFloatField;
    QrVMI_SrcQtdAntPeso: TFloatField;
    QrVMI_SrcQtdAntArM2: TFloatField;
    QrVMI_SrcQtdAntArP2: TFloatField;
    QrVMI_SrcAptoUso: TSmallintField;
    QrVMI_SrcNotaMPAG: TFloatField;
    QrVMI_SrcMarca: TWideStringField;
    QrVMI_SrcLk: TIntegerField;
    QrVMI_SrcDataCad: TDateField;
    QrVMI_SrcDataAlt: TDateField;
    QrVMI_SrcUserCad: TIntegerField;
    QrVMI_SrcUserAlt: TIntegerField;
    QrVMI_SrcAlterWeb: TSmallintField;
    QrVMI_SrcAtivo: TSmallintField;
    QrVMI_SrcTpCalcAuto: TIntegerField;
    QrVMI_Dst: TmySQLQuery;
    QrVMI_DstCodigo: TIntegerField;
    QrVMI_DstControle: TIntegerField;
    QrVMI_DstMovimCod: TIntegerField;
    QrVMI_DstMovimNiv: TIntegerField;
    QrVMI_DstMovimTwn: TIntegerField;
    QrVMI_DstEmpresa: TIntegerField;
    QrVMI_DstTerceiro: TIntegerField;
    QrVMI_DstCliVenda: TIntegerField;
    QrVMI_DstMovimID: TIntegerField;
    QrVMI_DstLnkNivXtr1: TIntegerField;
    QrVMI_DstLnkNivXtr2: TIntegerField;
    QrVMI_DstDataHora: TDateTimeField;
    QrVMI_DstPallet: TIntegerField;
    QrVMI_DstGraGruX: TIntegerField;
    QrVMI_DstPecas: TFloatField;
    QrVMI_DstPesoKg: TFloatField;
    QrVMI_DstAreaM2: TFloatField;
    QrVMI_DstAreaP2: TFloatField;
    QrVMI_DstValorT: TFloatField;
    QrVMI_DstSrcMovID: TIntegerField;
    QrVMI_DstSrcNivel1: TIntegerField;
    QrVMI_DstSrcNivel2: TIntegerField;
    QrVMI_DstSrcGGX: TIntegerField;
    QrVMI_DstSdoVrtPeca: TFloatField;
    QrVMI_DstSdoVrtPeso: TFloatField;
    QrVMI_DstSdoVrtArM2: TFloatField;
    QrVMI_DstObserv: TWideStringField;
    QrVMI_DstSerieFch: TIntegerField;
    QrVMI_DstFicha: TIntegerField;
    QrVMI_DstMisturou: TSmallintField;
    QrVMI_DstFornecMO: TIntegerField;
    QrVMI_DstCustoMOKg: TFloatField;
    QrVMI_DstCustoMOTot: TFloatField;
    QrVMI_DstValorMP: TFloatField;
    QrVMI_DstDstMovID: TIntegerField;
    QrVMI_DstDstNivel1: TIntegerField;
    QrVMI_DstDstNivel2: TIntegerField;
    QrVMI_DstDstGGX: TIntegerField;
    QrVMI_DstQtdGerPeca: TFloatField;
    QrVMI_DstQtdGerPeso: TFloatField;
    QrVMI_DstQtdGerArM2: TFloatField;
    QrVMI_DstQtdGerArP2: TFloatField;
    QrVMI_DstQtdAntPeca: TFloatField;
    QrVMI_DstQtdAntPeso: TFloatField;
    QrVMI_DstQtdAntArM2: TFloatField;
    QrVMI_DstQtdAntArP2: TFloatField;
    QrVMI_DstAptoUso: TSmallintField;
    QrVMI_DstNotaMPAG: TFloatField;
    QrVMI_DstMarca: TWideStringField;
    QrVMI_DstLk: TIntegerField;
    QrVMI_DstDataCad: TDateField;
    QrVMI_DstDataAlt: TDateField;
    QrVMI_DstUserCad: TIntegerField;
    QrVMI_DstUserAlt: TIntegerField;
    QrVMI_DstAlterWeb: TSmallintField;
    QrVMI_DstAtivo: TSmallintField;
    QrVMI_DstTpCalcAuto: TIntegerField;
    QrVCI: TmySQLQuery;
    QrVCIPecas: TFloatField;
    QrVCIAreaM2: TFloatField;
    QrVCIAreaP2: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox3: TGroupBox;
    DGVSPaRclCab: TdmkDBGridZTO;
    Panel7: TPanel;
    GroupBox2: TGroupBox;
    DGVMI_Sorc: TdmkDBGridZTO;
    GroupBox4: TGroupBox;
    dmkDBGridZTO1: TdmkDBGridZTO;
    DGVSPaRclIts: TdmkDBGridZTO;
    TabSheet2: TTabSheet;
    DBGCacItsA: TdmkDBGridZTO;
    QrVSCacItsA: TmySQLQuery;
    QrVSCacItsACacCod: TIntegerField;
    QrVSCacItsACacID: TIntegerField;
    QrVSCacItsACodigo: TIntegerField;
    QrVSCacItsAControle: TLargeintField;
    QrVSCacItsAClaAPalOri: TIntegerField;
    QrVSCacItsARclAPalOri: TIntegerField;
    QrVSCacItsAVSPaClaIts: TIntegerField;
    QrVSCacItsAVSPaRclIts: TIntegerField;
    QrVSCacItsAVSPallet: TIntegerField;
    QrVSCacItsAVMI_Sorc: TIntegerField;
    QrVSCacItsAVMI_Dest: TIntegerField;
    QrVSCacItsABox: TIntegerField;
    QrVSCacItsAPecas: TFloatField;
    QrVSCacItsAAreaM2: TFloatField;
    QrVSCacItsAAreaP2: TFloatField;
    QrVSCacItsARevisor: TIntegerField;
    QrVSCacItsADigitador: TIntegerField;
    QrVSCacItsADataHora: TDateTimeField;
    QrVSCacItsAAlterWeb: TSmallintField;
    QrVSCacItsAAtivo: TSmallintField;
    QrVSCacItsASumido: TSmallintField;
    QrVSCacItsARclAPalDst: TIntegerField;
    QrVSCacItsAVMI_Baix: TIntegerField;
    QrVSCacItsAMartelo: TIntegerField;
    QrVSCacItsAFrmaIns: TSmallintField;
    QrVSCacItsASubClass: TWideStringField;
    DsVSCacItsA: TDataSource;
    Panel8: TPanel;
    BtExclCacIts: TBitBtn;
    QrVSPaRclCabFatorInt: TFloatField;
    QrVSPaRclItsFatorInt: TFloatField;
    QrVCIPcBxa: TFloatField;
    QrVSGerRclTemIMEIMrt: TIntegerField;
    QrSumPallPallet: TLargeintField;
    QrSumPallGraGruX: TLargeintField;
    QrSumPallPecas: TFloatField;
    QrSumPallAreaM2: TFloatField;
    QrSumPallAreaP2: TFloatField;
    QrSumPallPesoKg: TFloatField;
    QrSumPallNO_PRD_TAM_COR: TWideStringField;
    QrSumPallNO_Pallet: TWideStringField;
    QrSumPallSerieFch: TLargeintField;
    QrSumPallFicha: TLargeintField;
    QrSumPallNO_TTW: TWideStringField;
    QrSumPallID_TTW: TLargeintField;
    QrSumPallSeries_E_Fichas: TWideStringField;
    QrSumPallTerceiro: TLargeintField;
    QrSumPallMarca: TWideStringField;
    QrSumPallNO_FORNECE: TWideStringField;
    QrSumPallNO_SerieFch: TWideStringField;
    QrSumPallValorT: TFloatField;
    QrSumPallSdoVrtPeca: TFloatField;
    QrSumPallSdoVrtPeso: TFloatField;
    QrSumPallSdoVrtArM2: TFloatField;
    Label12: TLabel;
    DBEdit29: TDBEdit;
    QrVSPaRclCabLstPal07: TIntegerField;
    QrVSPaRclCabLstPal08: TIntegerField;
    QrVSPaRclCabLstPal09: TIntegerField;
    QrVSPaRclCabLstPal10: TIntegerField;
    QrVSPaRclCabLstPal11: TIntegerField;
    QrVSPaRclCabLstPal12: TIntegerField;
    QrVSPaRclCabLstPal13: TIntegerField;
    QrVSPaRclCabLstPal14: TIntegerField;
    QrVSPaRclCabLstPal15: TIntegerField;
    QrVSGerRclClientMO: TIntegerField;
    BtAlterarDatas: TBitBtn;
    QrVMI_DstStqCenLoc: TIntegerField;
    PMCorrigeBaix: TPopupMenu;
    Antigo1: TMenuItem;
    Novo1: TMenuItem;
    QrVMIsOri: TMySQLQuery;
    QrVMIsOriSrcNivel2: TIntegerField;
    QrVMI: TMySQLQuery;
    QrVMIPecas: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIAreaP2: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSGerRclAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSGerRclBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSGerRclAfterScroll(DataSet: TDataSet);
    procedure QrVSGerRclBeforeClose(DataSet: TDataSet);
    procedure QrVSGerRclCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure QrVSPaRclCabBeforeClose(DataSet: TDataSet);
    procedure QrVSPaRclCabAfterScroll(DataSet: TDataSet);
    procedure EncerraPallet1Click(Sender: TObject);
    procedure QrVSPaRclCabCalcFields(DataSet: TDataSet);
    procedure QrVSPalletAfterScroll(DataSet: TDataSet);
    procedure QrVSPalletBeforeClose(DataSet: TDataSet);
    procedure QrVSPalletCalcFields(DataSet: TDataSet);
    procedure PelaOC1Click(Sender: TObject);
    procedure QrVSPaRclItsBeforeClose(DataSet: TDataSet);
    procedure QrVSPaRclItsAfterScroll(DataSet: TDataSet);
    procedure Poroutrasinformaes1Click(Sender: TObject);
    procedure Peladescriao1Click(Sender: TObject);
    procedure BtClassesGeradasClick(Sender: TObject);
    procedure BtCorrigeBaixClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BtExclCacItsClick(Sender: TObject);
    procedure BtAlterarDatasClick(Sender: TObject);
    procedure Antigo1Click(Sender: TObject);
    procedure Novo1Click(Sender: TObject);
  private
    FAtualizando: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);

    procedure ReopenSum(_AND: String);
    procedure ReopenSumPall();
    procedure ReopenVMI_Baix();
    procedure ReopenVMI_Sorc();
    procedure ReopenVSPallet();
    procedure ReopenVSPaRclCab();
    procedure ReopenVSPaRclIts();
    //
    procedure AtualizaStatPallDeTodasreclassificacoes();
    procedure ReopenVSCacItsA(Controle: Int64);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmVSGerRclCab: TFmVSGerRclCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral,
  VSGerArtEnc, UnVS_CRC_PF, UnVS_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSGerRclCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSGerRclCab.Novo1Click(Sender: TObject);
const
  Gera = False;
var
  Controle, VMI_Sorc(*, I, N*): Integer;
  Pecas, AreaM2, AreaP2: Double;
  //ArrSrcs: array of Integer;
  //Achou: Boolean;
begin
{
  Screen.Cursor := crHourGlass;
  try
DsVSPaRclCab
    UnDmkDAC_PF.AbreMySQLQuery0(QrVMIsOri, Dmod.MyDB, [
    'SELECT DISTINCT(SrcNivel2) SrcNivel2',
    'FROM vsmovits',
    'WHERE MovimCod=' + Geral.FF0(),
    '']);
    PB1.Position := 0;
    PB1.Max := QrVSMovIts.RecordCount + QrVMIsOri.RecordCount;
    //SetLength(ArrSrcs, 0);
    //N := 0;
    QrVSMovIts.First;
    while not QrVSMovIts.Eof do
    begin
      Controle := QrVSMovItsControle.Value;
      case TEstqMovimNiv(QrVSMovItsMovimNiv.Value) of
        eminSorcClass: // Baixa
        begin
          MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
          'Atualizando quantidades do VMI_Baix = ' + Geral.FF0(Controle));
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
          'SELECT SUM(Pecas*FatorIntDst) Pecas, SUM(AreaM2) AreaM2, ',
          'SUM(AreaP2) AreaP2 ',
          'FROM vscacitsa ',
          'WHERE VMI_Baix=' + Geral.FF0(Controle),
          '']);
          Pecas  := QrVMIPecas.Value;
          AreaM2 := QrVMIAreaM2.Value;
          AreaP2 := QrVMIAreaP2.Value;
         //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmovits', False, [
          'Pecas', 'AreaM2', 'AreaP2'], [
          'Controle'], [
          Pecas, AreaM2, AreaP2], [
          Controle], True) then
          begin
            (*
            VMI_Sorc := QrVSMovItsSrcNivel2.Value;
            Achou := False;
            for I := Low(ArrSrcs) to High(ArrSrcs) do
            begin
              if ArrSrcs[I] = VMI_Sorc then
                Achou := True;
            end;
            if Achou = False then
            begin
              N := N + 1;
              SetLength(ArrSrcs, N);
              ArrSrcs[N - 1] := VMI_Sorc;
              PB1.Max := PB1.Max + 1;
            end;
            *)
          end;
        end;
        eminDestClass: // Gera��o
        begin
          MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
          'Atualizando quantidades do VMI_Dest = ' + Geral.FF0(Controle));
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
          'SELECT SUM(Pecas*FatorIntDst) Pecas, SUM(AreaM2) AreaM2, ',
          'SUM(AreaP2) AreaP2 ',
          'FROM vscacitsa ',
          'WHERE VMI_Dest=' + Geral.FF0(Controle),
          '']);
          Pecas  := QrVMIPecas.Value;
          AreaM2 := QrVMIAreaM2.Value;
          AreaP2 := QrVMIAreaP2.Value;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmovits', False, [
          'Pecas', 'AreaM2', 'AreaP2'], [
          'Controle'], [
          Pecas, AreaM2, AreaP2], [
          Controle], True) then
            VS_PF.AtualizaSaldoVirtualVSMovIts(Controle, Gera);
        end;
      end;
      //
      QrVSMovIts.Next;
    end;
    //
    //for I := 0 to N - 1 do
    QrVMIsOri.First;
    while not QrVMIsOri.Eof do
    begin
      VS_PF.AtualizaSaldoVirtualVSMovIts(QrVMIsOriSrcNivel2.Value, Gera);
      QrVMIsOri.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmVSGerRclCab.Outrasimpresses1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSGerRclCab.PageControl1Change(Sender: TObject);
begin
  if (PageControl1.ActivePageIndex = 1) and (QrVSCacItsA.State = dsInactive) then
    ReopenVSCacItsA(0);
end;

procedure TFmVSGerRclCab.Peladescriao1Click(Sender: TObject);
begin
  LocCod(QrVSGerRclCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsgerrcla', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSGerRclCab.PelaOC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_CRC_PF.LocalizaPelaOC(emidReclasXXUni);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSGerRclCab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSGerRclCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSGerRclCab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_CRC_PF.LocalizaPeloIMEC(emidReclasXXUni);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSGerRclCab.PeloIMEI1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_CRC_PF.LocalizaPeloIMEI(emidReclasXXUni);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSGerRclCab.Poroutrasinformaes1Click(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_CRC_PF.MostraFormVSRMPPsq(emidReclasXXUni, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    //Reopen????(Controle);
  end;
end;

procedure TFmVSGerRclCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSGerRclCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSGerRclCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsgerrcla';
  VAR_GOTOMYSQLTABLE := QrVSGerRcl;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vgr.*, pal.ClientMO, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('CONCAT(gg1.Nome, ');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ');
  VAR_SQLx.Add('NO_PRD_TAM_COR, pal.Nome NO_PALLET');
  VAR_SQLx.Add('FROM vsgerrcla vgr');
  VAR_SQLx.Add('LEFT JOIN entidades  ent ON ent.Codigo=vgr.Empresa');
  VAR_SQLx.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=vgr.GraGruX ');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ');
  VAR_SQLx.Add('LEFT JOIN vspalleta  pal ON pal.Codigo=vgr.VSPallet ');
  VAR_SQLx.Add('WHERE vgr.Codigo > 0');
  //
  VAR_SQL1.Add('AND vgr.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vgr.Nome Like :P0');
  //
end;

procedure TFmVSGerRclCab.EncerraPallet1Click(Sender: TObject);
const
  EncerrandoTodos = False;
  Pergunta = True;
var
  VSPaClaIts, Box, VMI_Sorc, VMI_Baix, VMI_Dest, VSPallet: Integer;
  ReabreVSPaRclCab: Boolean;
begin
  Box        := QrVSPaRclItsTecla.Value;
  VSPaClaIts := QrVSPaRclItsControle.Value;
  VSPallet   := QrVSPaRclItsVSPallet.Value;
  VMI_Sorc   := QrVSPaRclItsVMI_Sorc.Value;
  VMI_Baix   := QrVSPaRclItsVMI_Baix.Value;
  VMI_Dest   := QrVSPaRclItsVMI_Dest.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPal, Dmod.MyDB, [
  'SELECT DtHrEndAdd ',
  'FROM vspalleta ',
  'WHERE Codigo = ' + Geral.FF0(VSPallet),
  '']);
  if (QrPalDtHrEndAdd.Value < 2) and (QrVSPaRclItsDtHrFim.Value < 2) then
  begin
    {
    //Result :=
    VS_CRC_PF.EncerraPalletReclassificacaoOld(QrVSPaRclCabCacCod.Value,
    QrVSPaRclCabCodigo.Value, QrVSPaRclCabVSPallet.Value, Box, VSPaClaIts,
    VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest, EncerrandoTodos, Pergunta,
    ReabreVSPaRclCab);
    }
    if VS_CRC_PF.EncerraPalletNew(VSPallet, Pergunta) then
    //if ReabreVSPaRclCab then
        ReopenVSPaRclIts();
  end else
    Geral.MB_Aviso('Pallet j� encerrado!');
end;

procedure TFmVSGerRclCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSGerRclCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSGerRclCab.ReopenSum(_AND: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(Aream2) AreaM2, SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(QrVSGerRclCacCod.Value),
  _AND,
  ' ']);
end;

procedure TFmVSGerRclCab.ReopenSumPall();
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPall, Dmod.MyDB, [
  'SELECT vmi.GraGruX, SUM(vmi.Pecas) Pecas, ',
  'SUM(vmi.PesoKg) PesoKg, SUM(vmi.AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, SUM(ValorT) ValorT, ',
  'SUM(vmi.SdoVrtPeca) SdoVrtPeca, SUM(vmi.SdoVrtPeso) SdoVrtPeso, ',
  'SUM(vmi.SdoVrtArM2) SdoVrtArM2, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, MAX(vmi.DataHora) DataHora ',
  'FROM v s m o v i t s vmi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'GROUP BY vmi.GraGruX ',
  '']);
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSGerRclTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.Pallet=' + Geral.FF0(QrVSPalletCodigo.Value),
  'AND vmi.Pecas > 0 ',
  //'WHERE vmi.MovimCod=' + Geral.FF0(QrVSGerRclCabMovimCod.Value),
  //'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),
  '']);
  SQL_Group :=   'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPall, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  //Geral.MB_SQL(self, QrSumPall);
  //QrSumPall.Locate('Pallet', Pallet, []);
end;

procedure TFmVSGerRclCab.ReopenVMI_Baix();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI_Baix, Dmod.MyDB, [
  'SELECT VMI_Baix, VMI_Sorc, VMI_Dest, ',
  'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
  'FROM vscacitsa ',
  'WHERE VSPaRclIts=' + Geral.FF0(QrVSPaRclItsControle.Value),
  'GROUP BY VMI_Baix ',
  '']);
end;

procedure TFmVSGerRclCab.ReopenVMI_Sorc();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI_Sorc, Dmod.MyDB, [
  'SELECT VMI_Sorc, SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
  'FROM vscacitsa ',
  'WHERE VSPaRclIts=' + Geral.FF0(QrVSPaRclItsControle.Value),
  'GROUP BY VMI_Sorc ',
  '']);
end;

procedure TFmVSGerRclCab.ReopenVSCacItsA(Controle: Int64);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacItsA, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa',
  'WHERE CacCod=' + Geral.FF0(QrVSPaRclCabCacCod.Value),
  '']);
  if Controle > 0 then
    QrVSCacItsA.Locate('Controle', Controle, []);
end;

procedure TFmVSGerRclCab.ReopenVSPallet;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT let.*,   ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
  ' CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, vps.Nome NO_STATUS    ',
  'FROM vspalleta let   ',
  'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa   ',
  'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat   ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
  'WHERE let.Codigo=' + Geral.FF0(QrVSGerRclVSPallet.Value),
  ' ']);
end;

procedure TFmVSGerRclCab.ReopenVSPaRclCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaRclCab, Dmod.MyDB, [
  'SELECT pra.*, IF(pra.DtHrFimCla < "1900-01-01", "",  ',
  '  DATE_FORMAT(pra.DtHrFimCla, "%d/%m/%y %h:%i:%s")) DtHrFimCla_TXT,',
  'cn1.FatorInt  ',
  'FROM vsparclcaba pra',
  'LEFT JOIN vspalleta  let ON let.Codigo=pra.VSPallet',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE pra.VSGerRcl=' + Geral.FF0(QrVSGerRclCodigo.Value),
  'ORDER BY pra.Codigo ',
  '']);
end;

procedure TFmVSGerRclCab.ReopenVSPaRclIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaRclIts, Dmod.MyDB, [
  'SELECT pri.*, IF(DtHrFim < "1900-01-01", "",  ',
  '  DATE_FORMAT(DtHrFim, "%d/%m/%y %h:%i:%s")) DtHrFim_TXT, ',
  'cn1.FatorInt  ',
  'FROM vsparclitsa pri ',
  'LEFT JOIN vspalleta  let ON let.Codigo=pri.VSPallet',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE pri.Codigo=' + Geral.FF0(QrVSPaRclCabCodigo.Value),
  'ORDER BY pri.DtHrIni ',
  '']);
end;

procedure TFmVSGerRclCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSGerRclCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSGerRclCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSGerRclCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSGerRclCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSGerRclCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerRclCab.Antigo1Click(Sender: TObject);
// Parei aqui! 2015-04-10
  function CriaVSMovIts(Codigo, MovimCod, Empresa, ClientMO, FornecMO,
  StqCenLoc, Fornecedor, GraGruX: Integer; MovimID, SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2, SrcGGX: Integer; DstMovID: TEstqMovimID; DstNivel1,
  DstNivel2, DstGGX, MovimTwn: Integer; Pecas, AreaM2, AreaP2: Double): Integer;
  const
    QtdGerPeca = 0;
    QtdGerPeso = 0;
    LnkNivXtr1 = 0;
    LnkNivXtr2 = 0;
    CliVenda   = 0;
    CustoMOKg  = 0;
    CustoMOM2  = 0;
    CustoMOTot = 0;
    ValorT     = 0;
    //Misturou   = 0;
    AptoUso    = 0;
    PesoKg     = 0;
    Observ     = ''; // Como fazer!!!
    SerieFch   = 0;  // Fazer depois!!!
    Ficha      = 0;  // Fazer depois!!!
    Marca      = ''; // Fazer depois!!!
    ValorMP    = 0;  // Fazer depois!!!
    QtdGerArM2 = 0;  // Fazer depois!!!
    QtdGerArP2 = 0;  // Fazer depois!!!
    NotaMPAG   = 0;
    //
    ExigeFornecedor = True;
    //
    TpCalcAuto = -1;
    //
    EdPallet = nil;
    EdValorT = nil;
    EdAreaM2 = nil;
    //
    PedItsLib  = 0;
    PedItsFin  = 0;
    PedItsVda  = 0;
    //
    GSPSrcMovID = TEstqMovimID(0);
    GSPSrcNiv2 = 0;
    //
    ReqMovEstq = 0;
    //
    ItemNFe     = 0;
    VSMulFrnCab = 0;
    //
    QtdAntPeca = 0;
    QtdAntPeso = 0;
    QtdAntArM2 = 0;
    QtdAntArP2 = 0;
  var
    Controle, Pallet: Integer;
    MovimNiv: TEstqMovimNiv;
    DataHora: String;
    //AreaP2: Double;
  begin
    Result := 0;
    DataHora := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    // Baixa (Classe a classificar)
    //MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, stIns, MovimTwn);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, 0);
    //
    Pallet       := 0;
    //
    //
    MovimNiv := eminSorcClass;
    //
    if VS_CRC_PF.InsUpdVSMovIts3(stIns, Codigo, MovimCod, MovimTwn, Empresa,
    Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
    AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
    LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg,
    CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso,
    QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
    NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    CO_0_VmiPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iuvpei061(*Corre��o de baixa em reclassifica��o*)) then
    begin
      Result := Controle;
{
      // Nova classe
      SrcMovID  := MovimID;
      SrcNivel1 := Codigo;
      SrcNivel2 := Controle;
      SrcGGX    := GraGruX;
      //
      Pallet    := VSPallet;
      //
      Controle  := DstNivel2;
      //
      DstMovID     := TEstqMovimID(0);
      DstNivel1    := 0;
      DstNivel2    := 0;
      DstGGX       := 0;
      //
      MovimNiv     := eminDestClass;
      //
      if InsUpdVSMovIts_(stIns, Codigo, MovimCod, MovimTwn, Empresa,
      Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
      AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
      LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg,
      CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso,
      QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
      NotaMPAG, SrcGGX, SrcGGX, Marca, TpCalcAuto) then
      begin
        CtrlSorc := SrcNivel2;
        CtrlDest := Controle;
      end;
}
    end;
  end;
  //
var
  VMI_Baix, Controle, VSPaClaIts: Integer;
  FatorIntSrc, FatorIntDst, Pecas: Double;
begin
  if Geral.MB_Pergunta(
  'Deseja verificar tamb�m o fator de convers�o de inteiros para meios?') =
  ID_YES then
  begin
    QrVSPaRclIts.First;
    while not QrVSPaRclIts.Eof do
    begin
      VSPaClaIts := QrVSPaRclItsControle.Value;
      if VSPaClaIts > 0 then
      begin
        FatorIntSrc := QrVSPaRclCabFatorInt.Value;
        FatorIntDst := QrVSPaRclItsFatorInt.Value;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
        'FatorIntSrc', 'FatorIntDst'], [
        'VSPaRclIts'], [
        FatorIntSrc, FatorIntDst], [
        VSPaClaIts], VAR_InsUpd_AWServerID);
        //
        QrVMI_Baix.First;
        while not QrVMI_Baix.Eof do
        begin
          if QrVMI_BaixVMI_Baix.Value > 0 then
          begin
            Controle := QrVMI_BaixVMI_Baix.Value;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrVCI, Dmod.MyDB, [
            'SELECT VMI_Baix, SUM(Pecas) Pecas, ',
            'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2,',
            'SUM(Pecas / FatorIntSrc * FatorIntDst) PcBxa  ',
            'FROM vscacitsa ',
            'WHERE VMI_Baix=' + Geral.FF0(Controle),
            '']);
            Pecas := -QrVCIPcBxa.Value;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
            'Pecas'], ['Controle'], [
            Pecas], [Controle], True);
            //
            VS_CRC_PF.AtualizaSaldoIMEI(QrVMI_BaixVMI_Dest.Value, False);
            VS_CRC_PF.AtualizaSaldoIMEI(QrVMI_BaixVMI_Sorc.Value, False);
            //
          end;
          QrVMI_Baix.Next;
        end;
      end;
      //
      QrVSPaRclIts.Next;
    end;
  end;
  QrVSPaRclIts.First;
  while not QrVSPaRclIts.Eof do
  begin
    QrVMI_Baix.First;
    while not QrVMI_Baix.Eof do
    begin
      if QrVMI_BaixVMI_Baix.Value = 0 then
      begin
        Controle := QrVSPaRclItsControle.Value;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrVCI, Dmod.MyDB, [
        'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
        'SUM(AreaP2) AreaP2  ',
        'FROM vscacitsa ',
        'WHERE VSPaRclIts=' + Geral.FF0(Controle),
        'AND VMI_Baix=0 ',
        '']);
        UnDmkDAC_PF.AbreMySQLQuery0(QrVMI_Src, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE Controle=' + Geral.FF0(QrVMI_BaixVMI_Sorc.Value),
        '']);
        UnDmkDAC_PF.AbreMySQLQuery0(QrVMI_Dst, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE Controle=' + Geral.FF0(QrVMI_BaixVMI_Dest.Value),
        '']);
        VMI_Baix := CriaVSMovIts(QrVSGerRclCodigo.Value,
        QrVSGerRclMovimCod.Value, QrVSGerRclEmpresa.Value, QrVSGerRclClientMO.Value,
        QrVMI_DstFornecMO.Value, QrVMI_DstStqCenLoc.Value,
        QrVMI_SrcTerceiro.Value, QrVMI_SrcGraGruX.Value, emidReclasXXUni,
        TEstqMovimID(QrVMI_SrcMovimID.Value), QrVMI_SrcCodigo.Value,
        QrVMI_SrcControle.Value, QrVMI_SrcGraGruX.Value,
        TEstqMovimID(QrVMI_DstMovimID.Value), QrVMI_DstCodigo.Value,
        QrVMI_DstControle.Value, QrVMI_DstGraGruX.Value,
        QrVMI_DstMovimTwn.Value, -QrVCIPecas.Value, -QrVCIAreaM2.Value,
        -QrVCIAreaP2.Value);
        //

        //if
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclitsa', False, [
        'VMI_Baix'], [
        'Controle'], [
        VMI_Baix], [
        Controle], True);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
        'VMI_Baix'], [
        'VMI_Baix', 'VSPaRclIts'], [
        VMI_Baix], [
        0, Controle], VAR_InsUpd_AWServerID);
        //
      end;
      //
      VS_CRC_PF.AtualizaSaldoIMEI(QrVMI_BaixVMI_Dest.Value, False);
      VS_CRC_PF.AtualizaSaldoIMEI(QrVMI_BaixVMI_Sorc.Value, False);
      //
      QrVMI_Baix.Next;
    end;
    //
    QrVSPaRclIts.Next;
  end;
end;

procedure TFmVSGerRclCab.AtualizaStatPallDeTodasreclassificacoes();
var
  Qry: TmySQLQuery;
  Reclasse: Integer;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vsgerrcla ',
    'ORDER BY Codigo DESC',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Reclasse := Qry.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando a reclassifica��o ' +
        Geral.FF0(Reclasse));
        //
        LocCod(Reclasse, Reclasse);
        BtCorrigeBaixClick(Self);
        //
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSGerRclCab.BtAlterarDatasClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSGerRclCodigo.Value;
  //
  VS_Jan.MostraFormVSGerRclDatas(Codigo, QrVSGerRclMovimCod.Value,
  (*QrVSGerRclDtHrAberto.Value*)0, QrVSGerRclDtHrLibCla.Value,
  QrVSGerRclDtHrCfgCla.Value, QrVSGerRclDtHrFimCla.Value, emidIndsXX);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmVSGerRclCab.BtClassesGeradasClick(Sender: TObject);
begin
  VS_CRC_PF.ImprimeReclassOC(QrVSGerRclCodigo.Value, QrVSGerRclcacCod.Value);
end;

procedure TFmVSGerRclCab.BtCorrigeBaixClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMCorrigeBaix, BtCorrigeBaix);
end;

procedure TFmVSGerRclCab.BtExclCacItsClick(Sender: TObject);
const
  //Pergunta = 'Deseja realmente excluir este couro?';
  Pergunta = '';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Controle: Int64;
  Pecas, AreaM2, AreaP2: Double;
  Txt: String;
  Continua: Boolean;
  VMI_Dest, VMI_Sorc, VMI_Baix, Codigo: Integer;
begin
  if not Geral.MB_Pergunta('Confirma a exclus�o de TODOS itens?') = ID_YES then
    Exit;
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  QrVSCacItsA.First;
  while not QrVSCacItsA.Eof do
  begin
    Codigo         := QrVSPalletCodigo.Value;
    Controle       := QrVSCacItsAControle.Value;
    VMI_Dest       := QrVSCacItsAVMI_Dest.Value;
    VMI_Sorc       := QrVSCacItsAVMI_Sorc.Value;
    VMI_Baix       := QrVSCacItsAVMI_Baix.Value;
    VS_CRC_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB);
    VS_CRC_PF.AtualizaSaldoOrigemVMI_Dest(VMI_Dest, VMI_Sorc, VMI_Baix);
    //
    QrVSCacItsA.Next;
  end;
end;

procedure TFmVSGerRclCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSGerRclCodigo.Value;
  Close;
end;

procedure TFmVSGerRclCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  //CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  DBGCacItsA.Align := alClient;
  DGVSPaRclIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmVSGerRclCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSGerRclCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSGerRclCab.SbNovoClick(Sender: TObject);
begin
//  LaRegistro.Caption := GOTOy.CodUsu(QrVSGerRclCodigo.Value, LaRegistro.Caption);
  FAtualizando := not FAtualizando;
  if FAtualizando then
    AtualizaStatPallDeTodasreclassificacoes();
end;

procedure TFmVSGerRclCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmVSGerRclCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSGerRclCab.QrVSGerRclAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSGerRclCab.QrVSGerRclAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPallet();
  ReopenVSPaRclCab();
end;

procedure TFmVSGerRclCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    //CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSGerRclCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSGerRclCab.SbQueryClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMQuery, SbQuery);
end;

procedure TFmVSGerRclCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerRclCab.QrVSGerRclBeforeClose(
  DataSet: TDataSet);
begin
  QrVSPaRclCab.Close;
  QrVSPallet.Close;
end;

procedure TFmVSGerRclCab.QrVSGerRclBeforeOpen(DataSet: TDataSet);
begin
  QrVSGerRclCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSGerRclCab.QrVSGerRclCalcFields(DataSet: TDataSet);
begin
  case QrVSGerRclTipoArea.Value of
    0: QrVSGerRclNO_TIPO.Value := 'm�';
    1: QrVSGerRclNO_TIPO.Value := 'ft�';
    else QrVSGerRclNO_TIPO.Value := '???';
  end;
  QrVSGerRclNO_DtHrLibCla.Value := Geral.FDT(QrVSGerRclDtHrLibCla.Value, 106, True);
  QrVSGerRclNO_DtHrFimCla.Value := Geral.FDT(QrVSGerRclDtHrFimCla.Value, 106, True);
  QrVSGerRclNO_DtHrCfgCla.Value := Geral.FDT(QrVSGerRclDtHrCfgCla.Value, 106, True);
end;

procedure TFmVSGerRclCab.QrVSPalletAfterScroll(DataSet: TDataSet);
begin
  ReopenSumPall();
end;

procedure TFmVSGerRclCab.QrVSPalletBeforeClose(DataSet: TDataSet);
begin
  QrSumPall.Close;
end;

procedure TFmVSGerRclCab.QrVSPalletCalcFields(DataSet: TDataSet);
begin
  QrVSPalletDtHrEndAdd_TXT.Value := Geral.FDT(QrVSPalletDtHrEndAdd.Value, 106, True);
end;

procedure TFmVSGerRclCab.QrVSPaRclCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPaRclIts();
  QrVSCacItsA.Close;
  if PageControl1.ActivePageIndex = 1 then
    ReopenVSCacItsA(0);
end;

procedure TFmVSGerRclCab.QrVSPaRclCabBeforeClose(DataSet: TDataSet);
begin
  QrVSPaRclIts.Close;
  QrVSCacItsA.Close;
end;

procedure TFmVSGerRclCab.QrVSPaRclCabCalcFields(DataSet: TDataSet);
begin
  ReopenSum('AND Codigo=' + Geral.FF0(QrVSPaRclCabCodigo.Value));
  QrVSPaRclCabPecas.Value  := QrSumPecas.Value;
  QrVSPaRclCabAreaM2.Value := QrSumAreaM2.Value;
  QrVSPaRclCabAreaP2.Value := QrSumAreaP2.Value;
end;

procedure TFmVSGerRclCab.QrVSPaRclItsAfterScroll(DataSet: TDataSet);
begin
  ReopenVMI_Baix();
  ReopenVMI_Sorc();
end;

procedure TFmVSGerRclCab.QrVSPaRclItsBeforeClose(DataSet: TDataSet);
begin
  QrVMI_Baix.Close;
  QrVMI_Sorc.Close;
end;

/////////////// OC
(*
SELECT VSPaRclIts, VMI_Dest, VMI_Sorc, VMI_Baix,
SUM(Pecas) Pecas, SUM(AreaM2) AreaM2
FROM vscacitsa
WHERE CacCod=65
GROUP BY VSPaRclIts, VMI_Dest, VMI_Sorc, VMI_Baix
ORDER BY VSPaRclIts, VMI_Dest, VMI_Sorc, VMI_Baix
*)

end.

