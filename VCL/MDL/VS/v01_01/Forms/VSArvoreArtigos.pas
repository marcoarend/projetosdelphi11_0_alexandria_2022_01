unit VSArvoreArtigos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, Vcl.ImgList,
  CommCtrl, dmkEdit, UnVS_PF, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSArvoreArtigos = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Qr013: TmySQLQuery;
    Qr013Ficha: TIntegerField;
    Qr013SerieFch: TIntegerField;
    Qr013Marca: TWideStringField;
    Qr013Terceiro: TIntegerField;
    QrTwns: TmySQLQuery;
    QrTwnsMovimTwn: TIntegerField;
    Qr007: TmySQLQuery;
    Qr007Controle: TIntegerField;
    Qr007MovimNiv: TIntegerField;
    Qr007MovimID: TIntegerField;
    ImageList1: TImageList;
    Qr013MovimID: TIntegerField;
    Qr013MovimNiv: TIntegerField;
    Qr013Pecas: TFloatField;
    Qr013AreaM2: TFloatField;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsControle: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TreeView1: TTreeView;
    TabSheet2: TTabSheet;
    Memo1: TMemo;
    BitBtn1: TBitBtn;
    Informa��es: TTabSheet;
    Memo2: TMemo;
    Qr013Pallet: TIntegerField;
    EdIMEI: TdmkEdit;
    CkIMEI: TCheckBox;
    Qr013MovimCod: TIntegerField;
    EdIMEI_Ini: TdmkEdit;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TreeView1GetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure TreeView1CustomDraw(Sender: TCustomTreeView; const ARect: TRect;
      var DefaultDraw: Boolean);
    procedure TreeView1CustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode;
      State: TCustomDrawState; var DefaultDraw: Boolean);
  private
    { Private declarations }
    FIMEI_Raiz, FIMEI_Atual, FAtualizados, FMovID_Raiz,
    FMovNiv_Raiz, FMovCod_Raiz,
    FIMEI_Cod_X, FIMEI_Cod_Y, FIMEI_Cod_Z,
    FIMEI_Niv_X, FIMEI_Niv_Y, FIMEI_Niv_Z: Integer;
    FJaIMEPs, FJaIMEIs, FIMEIsN: array of Integer;
    FListaIMEx: TIMExArr;
    FJaIMECs: TIMECArr;
    FMsgMemo, FParar: Boolean;
    FNodeIMEI: TTreeNode;
    //FTotalPecas, FTotalAreaM2: Double;
    //
    function  DefineImageIndexETextoMovimA(const MovimID, MovimNiv: Integer;
              var Indice: Integer; var Texto: String): Boolean;
    function  DefineImageIndexETextoMovimB(const MovimID, MovimNiv, SrcMovID:
              Integer; var Indice: Integer; var Texto: String): Boolean;
(*
    procedure InjetaDadosFilhosDeArtigoGerado(IMEI: Integer; Noh_Pai: TTreeNode;
              QtdItensPai, QtdTotalPai: Double);
*)
    procedure InjetaDadosFilhosDeArtigoClassificado(IMEI, Pallet: Integer;
              Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai, PercentPai: Double);
    procedure InjetaDadosFilhosDeArtigoClassificado_PreClasse(IMEI, Pallet:
              //Integer; Noh_Pai: TTreeNode; QryIMEC: TmySQLQuery; QtdItensPai,
              Integer; Noh_Pai: TTreeNode; MovimCod, MovID_Pai: Integer;
              QtdItensPai, QtdTotalPai, PercentPai: Double);
    procedure InjetaDadosFilhosDeArtigoClassificado_Operacao(IMEI, Pallet:
              //Integer; Noh_Pai: TTreeNode; QryIMEC: TmySQLQuery; QtdItensPai,
              Integer; Noh_Pai: TTreeNode; MovimCod, MovID_Pai: Integer;
              QtdItensPai, QtdTotalPai, PercentPai: Double);
    procedure InjetaDadosFilhosDeArtigoClassificado_EmProcWE(IMEI, Pallet,
              IMEC_Pai, MovID_Pai: Integer; Noh_Pai: TTreeNode;
              QtdItensPai, QtdTotalPai, PercentPai: Double);
    procedure InjetaDadosFilhosDeArtigoPreReclassificado(IMEI, Pallet: Integer;
              Noh_Pai: TTreeNode; PercentPai: Double = 0);
    procedure DefineQtdeItensDeIMEI(const Qry: TmySQLQuery; var QtdItens:
              Double; var TipoCalc: TTipoCalcCouro);
    //
    ////////////////////////////////////////////////////////////////////////////
    //
    function  InjetaNodeAtual_A(Titulo, Texto: String; PK, Index: Integer;
              NodePai: TTreeNode; MovimType: TEstqMovimType;
              ItensParte: Double = 0; ItensTotal: Double = 0; PercentPai: Double = 100): TTreeNode;
              //ItensParte: Double; ItensTotal: Double; PercentPai: Double): TTreeNode;
    function  InjetaNodeAtual_B(Titulo, Texto: String; PK, Index: Integer;
              NodePai: TTreeNode; MovimType: TEstqMovimType;
              //ItensParte: Double = 0; ItensTotal: Double = 0; PercentPai: Double = 100): TTreeNode;
              ItensParte: Double; ItensTotal: Double; PercentPai: Double;
              ContaIMEI: Boolean): TTreeNode;
    function  InjetaNodeAtual_IMEC(Texto: String; IMEC, MovimID, IMEI_Pai,
              Index: Integer; NodePai: TTreeNode): TTreeNode;
    procedure MostraMBInfo(Msg: String);
    //
    ////////////////////////////////////////////////////////////////////////////
    //
    procedure InjetaDadosDeIMEC_Generico(IMEI, Pallet, MovimCod, MovimID:
              Integer; Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai, PercentPai:
              Double; Campo: String; MudaField: Boolean; SQL_Extra: String = '');
    procedure InjetaDadosDeIMEC_ID02(IMEI, IMEC_Pai, MovID_Pai: Integer; Noh_Pai:
              TTreeNode; QtdItensPai, QtdTotalPai, PercentPai: Double; Field:
              String; MudaField: Boolean);
    procedure InjetaDadosDeIMEC_ID06(IMEI, IMEC_Pai, MovID_Pai: Integer; Noh_Pai:
              TTreeNode; QtdItensPai, QtdTotalPai, PercentPai: Double);
    procedure InjetaDadosDeIMEC_ID07(IMEI, IMEC_Pai, MovID_Pai: Integer; Noh_Pai:
              TTreeNode; QtdItensPai, QtdTotalPai, PercentPai: Double);
    procedure InjetaDadosFilhosDeIMEI(*InNatura*)(IMEI, Pallet, MovimID_Pai,
              MovimNiv_Pai: Integer; Noh_Pai: TTreeNode; QtdItensPai,
              QtdTotalPai, PercentPai: Double; MudaField, FromPai: Boolean;
              SQL_Extra: String = '');
    //
  public
    { Public declarations }
    //
    procedure MontaArvore(IMEI_Raiz: Integer);
  end;

  var
  FmVSArvoreArtigos: TFmVSArvoreArtigos;

implementation

uses UnMyObjects, Module, DmkDAC_PF, AppListas;

{$R *.DFM}

const
  CO_IDX_IMG_INDEFINIDO     = 00;
  CO_IDX_IMG_GEMEOS         = 01;
  CO_IDX_IMG_ITENSARECLAS   = 02;
  CO_IDX_IMG_CLASSIFICAR    = 03;
  CO_IDX_IMG_RECLASSIFICAR  = 03;
  CO_IDX_IMG_PREPARA_RECLAS = 04;
  CO_IDX_IMG_PREPARA_OPERAR = 04;
  CO_IDX_IMG_OPERCAO        = 05;
  CO_IDX_IMG_PROCESSO       = 06;
  CO_IDX_IMG_INNATURA       = 07;
  CO_IDX_IMG_CURTIDO        = 08;
  CO_IDX_IMG_SEMCLASSE      = 09;
  CO_IDX_IMG_CLASSETR       = 10;
  CO_IDX_IMG_INNATURA_ADD   = 11;
  CO_IDX_IMG_INNATURA_OUT   = 12;
  CO_IDX_IMG_CURTIDO_ADD    = 13;
  CO_IDX_IMG_CURTIDO_OUT    = 14;
  CO_IDX_IMG_CLASSEVI       = 15;
  CO_IDX_IMG_RECURTIDO      = 16;
  CO_IDX_IMG_ACABADO        = 17;
  CO_IDX_IMG_BAIXA_GENERI_I = 18;
  CO_IDX_IMG_BAIXA_GENERI_C = 19;
  CO_IDX_IMG_INVENTARIO     = 20;
  CO_IDX_IMG_ENTRA_GENERI_I = 21;
  CO_IDX_IMG_SUB_PRODUTO    = 22;

procedure TFmVSArvoreArtigos.BitBtn1Click(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmVSArvoreArtigos.BtOKClick(Sender: TObject);
var
  Ini, Fim: Integer;
begin
  Geral.MB_Aviso('Deprecado. Caso usar implementar transf. local!');
  FParar := False;
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  //
  if CkIMEI.Checked then
  begin
    MontaArvore(EdIMEI.ValueVariant);
    PageControl1.SetFocus;
    if PageControl1.ActivePageIndex = 0 then
      TreeView1.SetFocus;
  end else
  begin
    if EdIMEI.ValueVariant < EdIMEI_Ini.ValueVariant then
    begin
      Ini := EdIMEI.ValueVariant;
      Fim := EdIMEI_Ini.ValueVariant;
    end else
    begin
      Ini := EdIMEI_Ini.ValueVariant;
      Fim := EdIMEI.ValueVariant;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle BETWEEN ' + Geral.FF0(Ini) +
    ' AND ' + Geral.FF0(Fim),
    'ORDER BY Controle DESC ',
    '']);
    QrVSMovIts.First;
    while not QrVSMovIts.Eof do
    begin
      if FParar then
        Exit;
      FMsgMemo := True;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando IMEI ' +
        Geral.FF0(QrVSMovItsControle.Value));
      MontaArvore(QrVSMovItsControle.Value);
      //
      QrVSMovIts.Next;
    end;
  end;
  if Memo1.Text <> '' then
    PageControl1.ActivePageIndex := 1;
end;

procedure TFmVSArvoreArtigos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmVSArvoreArtigos.DefineImageIndexETextoMovimA(const MovimID, MovimNiv:
  Integer; var Indice: Integer; var Texto: String): Boolean;
const
  SrcMovID = 0;
begin
  Result :=
    DefineImageIndexETextoMovimB(MovimID, MovimNiv, SrcMovID, Indice, Texto);
end;

function TFmVSArvoreArtigos.DefineImageIndexETextoMovimB(const MovimID,
  MovimNiv, SrcMovID: Integer; var Indice: Integer; var Texto: String): Boolean;
begin
  Result := False;
  Application.ProcessMessages;
  if FParar then
    Exit;
  Result := True;
  Indice := CO_IDX_IMG_INDEFINIDO;
  Texto := sEstqMovimID_FRENDLY[MovimID];

  case TEstqMovimID(MovimID) of
    emidCompra: Indice := CO_IDX_IMG_INNATURA;                                  //  1.0
    emidVenda,                                                                  //  2.0
    //emidReclasWE ???                                                          //  3.0
    //emidBaixa,  ???                                                           //  4.0
    //emidIndsWE  ???                                                           //  5.0
    emidForcado,                                                                //  9.0
    emidSemOrigem,                                                              // 10.0
    emidResiduoReclas,                                                          // 12.0
    emidExtraBxa,                                                               // 17.0
    emidSaldoAnterior: Indice := CO_IDX_IMG_BAIXA_GENERI_I;                     // 18.0
    emidIndsXX:                                                                 //  6.0
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminDestCurtiXX: Indice := CO_IDX_IMG_CURTIDO;
        eminSorcCurtiXX: Indice := CO_IDX_IMG_CURTIDO_ADD;
        eminBaixCurtiXX: Indice := CO_IDX_IMG_INNATURA_OUT;
        else MostraMBInfo(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [1]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
      Texto := Texto + ' (' + sEstqMovimNiv[MovimNiv] + ')';
    end;
    emidClassArtXXUni,                                                          //  7.0
    emidClassArtXXMul:                                                          // 14.0
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminSorcClass: Indice := CO_IDX_IMG_SEMCLASSE;
        eminDestClass: Indice := CO_IDX_IMG_CLASSETR;
        eminSorcReclass: Indice := CO_IDX_IMG_SEMCLASSE;
        eminDestReclass: Indice := CO_IDX_IMG_CLASSETR;
        else MostraMBInfo(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [2A]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
      case TEstqMovimNiv(MovimNiv) of
        eminSorcClass: Texto := Texto + ' (Origem)';
        eminDestClass: Texto := Texto + ' (Destino)';
        eminSorcReclass: Texto := Texto + ' (Origem)';
        eminDestReclass: Texto := Texto + ' (Destino)';
        else MostraMBInfo(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [2B]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
    end;
    emidReclasXXUni,                                                            //  8.0
    emidReclasXXMul:                                                            // 24.0
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminSorcClass: Indice := CO_IDX_IMG_CLASSETR;
        eminDestClass: Indice := CO_IDX_IMG_CLASSETR;
        eminSorcReclass: Indice := CO_IDX_IMG_CLASSETR;
        eminDestReclass: Indice := CO_IDX_IMG_CLASSETR;
        else MostraMBInfo(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [3A]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
      case TEstqMovimNiv(MovimNiv) of
        eminSorcClass: Texto := Texto + ' (Origem)';
        eminDestClass: Texto := Texto + ' (Destino)';
        eminSorcReclass: Texto := Texto + ' (Origem)';
        eminDestReclass: Texto := Texto + ' (Destino)';
        else MostraMBInfo(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [3B]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
    end;
    emidInventario: Indice := CO_IDX_IMG_INVENTARIO;                            // 13.0
    emidPreReclasse:                                                            // 15.0
    begin
      Indice := CO_IDX_IMG_INDEFINIDO;
      case TEstqMovimNiv(MovimNiv) of
        eminSorcClass: Texto := Texto + ' (Origem)';
        eminDestClass: Texto := Texto + ' (Destino)';
        eminSorcPreReclas: Texto := Texto + ' (Item de baixa)';
        eminDestPreReclas: Texto := Texto + ' (Somat�rio dos itens)';
        else MostraMBInfo(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [4A]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
      case TEstqMovimNiv(MovimNiv) of
        eminSorcClass,
        eminSorcPreReclas:
        begin
          case TEstqMovimID(SrcMovID) of
            emidAjuste:        Indice := CO_IDX_IMG_BAIXA_GENERI_I; //
            emidClassArtXXUni,
            emidClassArtXXMul: Indice := CO_IDX_IMG_CLASSETR;
            emidReclasXXUni,
            emidReclasXXMul:   Indice := CO_IDX_IMG_CLASSEVI;
            else MostraMBInfo(
            '"SrcMovID" n�o implementado "DefineImageIndexETextoMovimB" [4C]' +
            sLineBreak + 'SrcMovID = ' + Geral.FF0(SrcMovID));;
          end;
        end;
        eminDestClass: Indice := CO_IDX_IMG_CLASSETR;
        eminDestPreReclas: Indice := CO_IDX_IMG_CLASSEVI;
        else MostraMBInfo(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [4B]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
    end;
    emidEmOperacao:                                                             // 11.0
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminSorcOper:  Indice := CO_IDX_IMG_CURTIDO_OUT;
        eminEmOperInn: Indice := CO_IDX_IMG_CURTIDO_ADD;
        eminDestOper:  Indice := CO_IDX_IMG_CLASSEVI;
        eminEmOperBxa: Indice := CO_IDX_IMG_CURTIDO_OUT;
        //
        eminSdoArtEmOper:
        else MostraMBInfo(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [5]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
    end;
    emidEmProcWE:                                                               // 19.0
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminSorcWEnd:  Indice := CO_IDX_IMG_CURTIDO_OUT;
        eminEmWEndInn: Indice := CO_IDX_IMG_RECURTIDO;
        eminDestWEnd:  Indice := CO_IDX_IMG_ACABADO;
        eminEmWEndBxa: Indice := CO_IDX_IMG_RECURTIDO;
        else MostraMBInfo(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [5]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
    end;
    emidFinished:                                                               // 20.0
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminSorcWEnd:  Indice := CO_IDX_IMG_CURTIDO_OUT;
        eminEmWEndInn: Indice := CO_IDX_IMG_RECURTIDO;
        eminDestWEnd:  Indice := CO_IDX_IMG_ACABADO;
        eminEmWEndBxa: Indice := CO_IDX_IMG_RECURTIDO;
        else MostraMBInfo(
        '"MovimNiv" n�o implementado "DefineImageIndexETextoMovimB" [5]' +
        sLineBreak + 'MovimNiv = ' + Geral.FF0(MovimNiv));
      end;
    end;
    emidEntradaPlC,                                                             // 16.0
    emidDevolucao,                                                              // 21.0
    emidRetrabalho: Indice := CO_IDX_IMG_ENTRA_GENERI_I;                        // 22.0
    emidGeraSubProd: Indice := CO_IDX_IMG_SUB_PRODUTO;                          // 23.0
    else
    begin
      Result := False;
      Indice := 0;
      Texto := '"Image index" n�o definido!' +
      '  |  MovimID = ' + Geral.FF0(MovimID) +
      '  |  MovimNiv = ' + Geral.FF0(MovimNiv);
    end;
  end;
end;

procedure TFmVSArvoreArtigos.DefineQtdeItensDeIMEI(const Qry: TmySQLQuery;
  var QtdItens: Double; var TipoCalc: TTipoCalcCouro);
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  if (Qry.FieldByName('MovimID').AsInteger = 1) then
  begin
    QtdItens := Qry.FieldByName('Pecas').AsFloat;
    TipoCalc := ptcPecas;
  end else
  begin
    if Qry.FieldByName('AreaM2').AsFloat > 0 then
    begin
      QtdItens := Qry.FieldByName('AreaM2').AsFloat;
      TipoCalc := ptcAreaM2;
    end else
    begin
      QtdItens := Qry.FieldByName('Pecas').AsFloat;
      TipoCalc := ptcPecas;
    end;
  end;
end;

procedure TFmVSArvoreArtigos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if PageControl1.ActivePageIndex = 0 then
    TreeView1.SetFocus;
end;

procedure TFmVSArvoreArtigos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FParar := False;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmVSArvoreArtigos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSArvoreArtigos.InjetaDadosDeIMEC_Generico(IMEI, Pallet, MovimCod,
  MovimID: Integer; Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai, PercentPai:
  Double; Campo: String; MudaField: Boolean; SQL_Extra: String);
var
  QryVMov: TmySQLQuery;
  //MovimID: Integer;
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  QryVMov := TmySQLQuery.Create(Dmod);
  try
  // Ver qual eh o  tipo de movimento(MovimID).
////////////////////////////////////////////////////////////////////////////////
///  NAO USAR !!! Pode achar ID=6 em vez de ID=7 !!!
////////////////////////////////////////////////////////////////////////////////
  (*UnDmkDAC_PF.AbreMySQLQuery0(QryVMov, Dmod.MyDB, [
  'SELECT *  ',
  'FROM vsmovcab ',
  'WHERE Codigo=' + Geral.FF0(MovimCod),
  '']);
  MovimID := QryVMov.FieldByName('MovimID').AsInteger;
  if MovimID = 0 then
  begin
    MostraMBInfo('IMEI ' + Geral.FF0(IMEI) +
    ': "MovimID" n�o inserido na tabela "vsmovcab"!' + sLineBreak +
    'MovimID = ' + Geral.FF0(MovimID) + sLineBreak +
    'Avise a DERMATEK!');
    //
    *)



(*
    UnDmkDAC_PF.AbreMySQLQuery0(QryVMov, Dmod.MyDB, [
    'SELECT DISTINCT MovimID  ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    SQL_Extra,
    '']);
    MovimID := QryVMov.FieldByName('MovimID').AsInteger;
*)



  //end;
  case TEstqmovimID(MovimID) of
    //midAjuste=0,
    //emidCompra=1,
    emidVenda,          // 2
    //emidReclasWE=3,
    //emidBaixa=4,
    //emidIndsWE=5,
    emidForcado,        // 9
    emidSemOrigem,      // 10
    emidResiduoReclas,  // 12
    emidInventario,     // 13
    emidEntradaPlC,     // 16
    emidExtraBxa,       // 17
    emidSaldoAnterior,  // 18
    emidFinished,       // 20
    emidDevolucao,      // 21
    emidRetrabalho,     // 22
    emidGeraSubProd:    // 23
    InjetaDadosDeIMEC_ID02(IMEI, MovimCod, MovimID, Noh_Pai, QtdItensPai,
      QtdTotalPai, PercentPai, Campo, MudaField); // 02

    emidIndsXX: InjetaDadosDeIMEC_ID06(IMEI, MovimCod, MovimID, Noh_Pai, QtdItensPai, QtdTotalPai, PercentPai); // 06
    emidClassArtXXUni: InjetaDadosDeIMEC_ID07(IMEI, MovimCod, MovimID, Noh_Pai, QtdItensPai, QtdTotalPai, PercentPai); // 07
    emidReclasXXUni: InjetaDadosFilhosDeArtigoPreReclassificado(IMEI, Pallet, Noh_Pai, PercentPai); // 08
    emidEmOperacao:     // 11
      InjetaDadosFilhosDeArtigoClassificado_Operacao(IMEI, Pallet, Noh_Pai,
        //QryIMEC, QtdItensPai, QtdTotalPai, PercentPai);
        MovimCod, MovimID, QtdItensPai, QtdTotalPai, PercentPai);
    //emidClassArtXXMul=14,
    emidPreReclasse:    // 15
      InjetaDadosFilhosDeArtigoClassificado_PreClasse(IMEI, Pallet, Noh_Pai,
      //QryIMEC, QtdItensPai, QtdTotalPai, PercentPai);
      MovimCod, MovimID, QtdItensPai, QtdTotalPai, PercentPai);
    emidEmProcWE:       // 19
      InjetaDadosFilhosDeArtigoClassificado_EmProcWE(IMEI, Pallet, MovimCod,
        MovimID, Noh_Pai, QtdItensPai, QtdTotalPai, PercentPai);
    else MostraMBInfo(
    'IMEI ' + Geral.FF0(IMEI) +
    ': "MovimID" n�o implementado em "InjetaDadosDeIMEC_Generico"!' + sLineBreak +
    'MovimID = ' + Geral.FF0(MovimID));
  end;
  finally
    QryVMov.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaDadosDeIMEC_ID02(IMEI, IMEC_Pai,
  MovID_Pai: Integer; Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai, PercentPai:
  Double; Field: String; MudaField: Boolean);
const
  MudaCampo = False;
  FromPai = False;
var
  QryIMEC, QryIMEP, QryIMEI: TmySQLQuery;
  Indice, MovimID, MovimNiv, Controle, Pallet: Integer;
  NohMovimCod, NohIMEI: TTreeNode;
  Campo, Texto: String;
  QtdItens, QtdTotal, QtdSoma: Double;
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  QryIMEC := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  QtdSoma := 0;
  // Injetar noh do IMEC
  Indice := CO_IDX_IMG_BAIXA_GENERI_C;
  Texto  := sEstqMovimID_FRENDLY[MovID_Pai];
  NohMovimCod := InjetaNodeAtual_IMEC(Texto, IMEC_Pai, MovID_Pai, IMEI, Indice,
    Noh_Pai);
  if MudaField then
    Campo := Field
  else
    Campo := 'SrcNivel2';

  // IME-Is do IMEC selecionado que sao filhos do IMEI em questao
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(IMEC_Pai),
  'AND ' + Campo + '=' + Geral.FF0(IMEI),
  //'AND SrcNivel2=' + Geral.FF0(IMEI),
  'ORDER BY Controle ',
  '']);
  while not QryIMEI.Eof do
  begin
    MovimID  := QryIMEI.FieldByName('MovimID').AsInteger;
    MovimNiv := QryIMEI.FieldByName('MovimNiv').AsInteger;
    Controle := QryIMEI.FieldByName('Controle').AsInteger;
    Pallet   := QryIMEI.FieldByName('Pallet').AsInteger;

    DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
    QtdItens := QtdSoma;
    QtdTotal := QryIMEI.FieldByName('AreaM2').AsFloat;
    NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
      NohMovimCod, emitIME_I, QtdItens, QtdTotal, PercentPai);
    // N�o deveria ter mais nada, mas por desencargo de consciencia...
    InjetaDadosFilhosDeIMEI(*InNatura*)(Controle, Pallet, MovimID, MovimNiv,
    NohIMEI, QtdItens, QtdTotal, PercentPai, MudaCampo, FromPai);
    //
    QryIMEI.Next;
  end;
  //
  finally
    QryIMEI.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEC.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaDadosDeIMEC_ID06(IMEI, IMEC_Pai, MovID_Pai: Integer;
Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai, PercentPai: Double);
const
  MudaCampo = False;
  FromPai   = False;
var
  QryIMEC, QryIMEP, QryIMEI: TmySQLQuery;
  Indice, MovimTwn, MovimID, MovimNiv, Controle, Pallet: Integer;
  NohMovimCod, NohMovimTwn, NohIMEI: TTreeNode;
  Texto, Twns, SQL_Extra: String;
  QtdItens, QtdTotal, QtdSoma: Double;
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  QryIMEC := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  QtdSoma := 0;
  Twns := '';
  // Injetar noh do IMEC
  Indice := CO_IDX_IMG_PROCESSO;
  Texto  := sEstqMovimID_FRENDLY[MovID_Pai];
  NohMovimCod := InjetaNodeAtual_IMEC(Texto, IMEC_Pai, MovID_Pai, IMEI,
    Indice, Noh_Pai);

  // Listar todas duplas totalizadoras do Artigo gerado atual
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEP, Dmod.MyDB, [
  'SELECT DISTINCT MovimTwn ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimTwn <> 0 ',
  'AND MovimCod=' + Geral.FF0(IMEC_Pai),
  'AND MovimID=' + Geral.FF0(Integer(emidIndsXX)), // nao pegar ID=7 !!!
  'AND MovimNiv<>' + Geral.FF0(Integer(eminDestCurtiXX)),
  '']);

  //
  QryIMEP.First;
  while not QryIMEP.Eof do
  begin
    // Injetar noh do IMEP
    MovimTwn := QryIMEP.FieldByName('MovimTwn').AsInteger;
    Indice := CO_IDX_IMG_GEMEOS;
    Texto  := 'Par de Baixa-Gera��o';
    NohMovimTwn := InjetaNodeAtual_A('IME-P ', Texto, MovimTwn,
      Indice, NohMovimCod, emitIME_P, 0, 0);

    // Listar os itens (IME-Is) da dupla totalizadora atual do Artigo gerado atual
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimTwn <> 0 ',
    'AND MovimTwn=' + Geral.FF0(MovimTwn),
    'AND MovimNiv<>' + Geral.FF0(Integer(eminDestCurtiXX)),
    'ORDER BY MovimNiv ',
    '']);
    //
    Twns := Twns + ', ' + Geral.FF0(MovimTwn);
    //
    QryIMEI.First;
    while not QryIMEI.Eof do
    begin
      QtdItens := 0;
      //QtdTotal := 0;
      MovimID  := QryIMEI.FieldByName('MovimID').AsInteger;
      MovimNiv := QryIMEI.FieldByName('MovimNiv').AsInteger;
      Controle := QryIMEI.FieldByName('Controle').AsInteger;
      //DefineQtdeItensDeIMEI(QryIMEI, QtdItens, TipoCalc);
      if TEstqMovimID(MovimID) = emidIndsXX then
      begin
        if TEstqMovimNiv(MovimNiv) = eminBaixCurtiXX then // 15
        begin
          if QryIMEI.FieldByName('SrcNivel2').AsInteger = IMEI then
            QtdItens := QtdItens + (-QryIMEI.FieldByName('QtdGerArM2').AsFloat);
        end;
      end else
      begin
        if QryIMEI.FieldByName('SrcNivel2').AsInteger = IMEI then
          QtdItens := QtdItens + (-QryIMEI.FieldByName('QtdGerArM2').AsFloat);
      end;
      QtdTotal := QtdItens;
      QtdSoma  := QtdSoma + QtdItens;
      DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
      //NohIMEI :=
      InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
        NohMovimTwn, emitIME_I, QtdItens, QtdTotal, PercentPai);
      //
      QryIMEI.Next;
    end;
    //
    QryIMEP.Next;
  end;
  //
  // IME-I do totalizando os itens Artigo gerado atual
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(IMEC_Pai),
  'AND MovimID=' + Geral.FF0(Integer(emidIndsXX)), // nao pegar ID=7 !!!
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
  'ORDER BY MovimNiv ',
  '']);
  // Soh tem um, mas mostra erro sem houver mais!
  while not QryIMEI.Eof do
  begin
    MovimID  := QryIMEI.FieldByName('MovimID').AsInteger;
    MovimNiv := QryIMEI.FieldByName('MovimNiv').AsInteger;
    Controle := QryIMEI.FieldByName('Controle').AsInteger;
    Pallet   := QryIMEI.FieldByName('Pallet').AsInteger;
    DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
    QtdItens := QtdSoma;
    QtdTotal := QryIMEI.FieldByName('AreaM2').AsFloat;
    NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
      NohMovimCod, emitIME_I, QtdItens, QtdTotal, PercentPai);
    // Inserir itens classificados do IMEI Atual!
    //InjetaDadosFilhosDeArtigoGerado(Controle, NohIMEI, QtdItens, QtdTotal);
    if Trim(Twns) <> '' then
    begin
      Twns := Copy(Twns, 2);
      SQL_Extra := 'AND NOT (MovimTwn IN (' + Twns + ')) ';
    end else
      SQL_Extra := '';
    InjetaDadosFilhosDeIMEI(*InNatura*)(Controle, Pallet, MovimID, MovimNiv,
      NohIMEI, QtdItens, QtdTotal, PercentPai, MudaCampo, FromPai, SQL_Extra);
    //
    QryIMEI.Next;
  end;
  //
  finally
    QryIMEI.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEC.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaDadosDeIMEC_ID07(IMEI, IMEC_Pai, MovID_Pai: Integer;
  Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai, PercentPai: Double);
//InjetaDadosFilhosDeArtigoGerado(IMEI: Integer; Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai: Double);
var
  QryIMEP, QryIMEI, QryPall: TmySQLQuery;
  MovimCod, Indice, MovimTwn, MovimID, MovimNiv, Controle, Pallet: Integer;
  NohMovimCod, NohMovimTwn, NohIMEI: TTreeNode;
  Texto: String;
  //PercentPai,
  QtdItens, QtdTotal: Double;
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  QryPall := TmySQLQuery.Create(Dmod);
  try
    MovimCod := IMEC_Pai;
    //
    Indice := CO_IDX_IMG_CLASSIFICAR;
    Texto  := 'Classifica��o de Artigo Gerado';
    NohMovimCod := InjetaNodeAtual_IMEC(Texto, MovimCod, MovID_Pai, IMEI,
      Indice, Noh_Pai);

    // Listar todas duplas totalizadoras do Artigo gerado atual
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEP, Dmod.MyDB, [
    'SELECT DISTINCT MovimTwn ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimTwn <> 0 ',
    'AND MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidClassArtXXUni)),
    'AND SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    //
    QryIMEP.First;
    while not QryIMEP.Eof do
    begin
      // Injetar noh do IMEP
      MovimTwn := QryIMEP.FieldByName('MovimTwn').AsInteger;
      Indice := CO_IDX_IMG_GEMEOS;
      Texto  := 'Par de defini��o de classe';
      NohMovimTwn := InjetaNodeAtual_A('IME-P ', Texto, MovimTwn,
        Indice, NohMovimCod, emitIME_P, 0, 0);

      // Listar os itens (IME-Is) da dupla totalizadora atual do Artigo gerado atual
      UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimTwn <> 0 ',
      'AND MovimTwn=' + Geral.FF0(MovimTwn),
      'ORDER BY MovimNiv ',
      '']);
      //
      QryIMEI.First;
      while not QryIMEI.Eof do
      begin
        MovimID  := QryIMEI.FieldByName('MovimID').AsInteger;
        MovimNiv := QryIMEI.FieldByName('MovimNiv').AsInteger;
        Controle := QryIMEI.FieldByName('Controle').AsInteger;
        Pallet   := QryIMEI.FieldByName('Pallet').AsInteger;
        DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
        //
        if Pallet > 0 then
        begin
          VS_PF.ReopenVSPalletEntradasValidas(QryPall, Pallet);
          QtdItens := QryIMEI.FieldByName('AreaM2').AsFloat;
          QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
        end else
        begin
          QtdItens := 0;
          QtdTotal := 0;
        end;
        NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
          NohMovimTwn, emitIME_I, QtdItens, QtdTotal, PercentPai);
        //
        case TEstqMovimID(MovimID) of
          emidClassArtXXUni,
          emidClassArtXXMul,
          emidPreReclasse:
          begin
            if TEstqMovimNiv(MovimNiv) = eminDestClass then
            begin
              //Fazer Percent Filho := QtdItens / QtdTotal * PercentPai ????
              InjetaDadosFilhosDeArtigoClassificado(Controle, Pallet, NohIMEI,
              QtdItens, QtdTotal, PercentPai);
            end;
          end;
          else MostraMBInfo('"MovimID n�o implementado (FilhosDeArtigoGerado)"'
          + sLineBreak + 'MovimID: ' + Geral.FF0(MovimID));
        end;
        //
        QryIMEI.Next;
      end;
      //
      QryIMEP.Next;
    end;
  finally
    QryPall.Free;
  end;
  finally
    QryIMEI.Free;
  end;
  finally
    QryIMEP.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaDadosFilhosDeArtigoClassificado(
  IMEI, Pallet: Integer; Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai,
  PercentPai: Double);
const
  MudaCampo = False;
  Campo = 'SrcNivel2';
var
  //QryIMEP, QryIMEI, QryPrep,
  QryIMEC: TmySQLQuery;
  MovimCod, MovID_Pai: Integer;
(*
  Indice, MovimTwn, MovimNiv, Controle, SrcMovID,
  PalletFilho: Integer;
  NohMovimCod, NohPallet, NohMovimTwn, NohIMEI: TTreeNode;
  Texto: String;
*)
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  QryIMEC := TmySQLQuery.Create(Dmod);
  try
(*
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryPrep := TmySQLQuery.Create(Dmod);
  try
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
*)
  // listar todas cabecalhos de todas Pre Reclassificacoes
  // Pode ter varias para o mesmo Pallet!!
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEC, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod, MovimID  ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE ' + Campo + '=' + Geral.FF0(IMEI),
  'ORDER BY MovimCod ',
  '']);
  QryIMEC.First;
  while not QryIMEC.Eof do
  begin
    MovID_Pai := QryIMEC.FieldByName('MovimID').AsInteger;
    MovimCod  := QryIMEC.FieldByName('MovimCod').AsInteger;
    case TEstqMovimID(MovID_Pai) of
      emidPreReclasse: InjetaDadosFilhosDeArtigoClassificado_PreClasse(
                       IMEI, Pallet, Noh_Pai, (*QryIMEC,*) MovimCod, MovID_Pai,
                       QtdItensPai, QtdTotalPai, PercentPai);
      emidEmOperacao: InjetaDadosFilhosDeArtigoClassificado_Operacao(
                       IMEI, Pallet, Noh_Pai, (*QryIMEC,*) MovimCod, MovID_Pai,
                       QtdItensPai, QtdTotalPai, PercentPai);
      emidEmProcWE:   InjetaDadosFilhosDeArtigoClassificado_EmProcWE(
                       IMEI, Pallet, MovimCod, MovID_Pai, Noh_Pai,
                       QtdItensPai, QtdTotalPai, PercentPai);
      emidVenda,
      //emidBaixa, ???
      emidExtraBxa,
      emidForcado,
      emidInventario,
      emidResiduoReclas: InjetaDadosDeIMEC_ID02(IMEI, MovimCod, MovID_Pai,
        Noh_Pai, QtdItensPai, QtdTotalPai, PercentPai, Campo, MudaCampo); // 02
      else MostraMBInfo('"MovimID" n�o implementado! [5]' + sLineBreak +
      'MovimID = ' + Geral.FF0(MovID_Pai) + sLineBreak +
      'IME-I = ' + Geral.FF0(IMEI));
    end;
    QryIMEC.Next;
  end;
(*
  finally
    QryPrep.Free;
  end;
  finally
    QryIMEI.Free;
  end;
  finally
    QryIMEP.Free;
  end;
*)
  finally
    QryIMEC.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaDadosFilhosDeArtigoClassificado_EmProcWE(
  IMEI, Pallet, IMEC_Pai, MovID_Pai: Integer; Noh_Pai: TTreeNode; QtdItensPai,
  QtdTotalPai, PercentPai: Double);
var
  Indice, MovimID, MovimNiv, Controle, SrcMovID, PalletFilho,
  MovimTwn, SrcNivel2: Integer;
  Texto: String;
  NohMovimCod, NohPallet, NohIMEI, Noh_20, Noh_21, Noh_22, Noh_23: TTreeNode;
  QryIMEI_A, QryIMEI_B, QryIMEP, QryPall: TmySQLQuery;
  QtdItens, QtdTotal, QtdSoma, PercentNew: Double;
  ContaIMEI: Boolean;
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  QryIMEI_A := TmySQLQuery.Create(Dmod);
  try
  QryIMEI_B := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryPall := TmySQLQuery.Create(Dmod);
  try
  //QtdItens := 0;
  //QtdTotal := 0;
  // Injetar noh do IMEC
  Indice := CO_IDX_IMG_PROCESSO;
  Texto  := 'Processo de Semi Acabado';
  NohMovimCod := InjetaNodeAtual_IMEC(Texto, IMEC_Pai, MovID_Pai, IMEI,
    Indice, Noh_Pai);
(*
eminSorcWEnd=20,
eminEmWEndInn=21,
eminDestWEnd=22,
eminEmWEndBxa=23,
*)
  // Injetar noh para separar Baixas de origem (Niv=20), Totalizador (Niv=21)
  // Geracao de destino (22) + Baixas de destino (23)
  Indice := CO_IDX_IMG_PREPARA_OPERAR;
  Noh_20 := InjetaNodeAtual_A('Baixas ', Texto, IMEC_Pai,
    Indice, NohMovimCod, emitAgrupItensBxa, 0, 0, 0);
  Indice := CO_IDX_IMG_PREPARA_OPERAR;
  Noh_21 := InjetaNodeAtual_A('Em processo ', Texto, IMEC_Pai,
    Indice, NohMovimCod, emitAgrupTotalizador, 0, 0, 0);
  Indice := CO_IDX_IMG_PREPARA_OPERAR;
  Noh_23 := InjetaNodeAtual_A('Processado ', Texto, IMEC_Pai,
    Indice, NohMovimCod, emitAgrupItensNew, 0, 0, 0);
  //
  QtdSoma  := 0;
  QtdTotal := 0;
  // Abrir o 08 antes para saber o total de area!
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI_B, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(IMEC_Pai),
  'AND MovimNiv=' + Geral.FF0(Integer(eminEmWEndInn)),
  'ORDER BY MovimNiv DESC ',
  '']);
  QryIMEI_B.First;
  while not QryIMEI_B.Eof do
  begin
    QtdTotal := QtdTotal + QryIMEI_B.FieldByName('AreaM2').AsFloat;
    //
    QryIMEI_B.Next;
  end;
  // Abrir o 07 depois mas inserir antes do 08
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI_A, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(IMEC_Pai),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcWEnd)),
  'ORDER BY MovimNiv DESC ',
  '']);
  QryIMEI_A.First;
  while not QryIMEI_A.Eof do
  begin
    MovimID  := QryIMEI_A.FieldByName('MovimID').AsInteger;
    MovimNiv := QryIMEI_A.FieldByName('MovimNiv').AsInteger;
    //SrcMovID := QryIMEI_A.FieldByName('SrcMovID').AsInteger;
    Controle := QryIMEI_A.FieldByName('Controle').AsInteger;
    SrcNivel2 := QryIMEI_A.FieldByName('SrcNivel2').AsInteger;
    // somente do IMEI em pequisa!
    if SrcNivel2 = IMEI then
    begin
      QtdItens  := - QryIMEI_A.FieldByName('AreaM2').AsFloat;
      QtdSoma   := QtdSoma + QtdItens;
      ContaIMEI := True;
    end else
    begin
      QtdItens  := 0;
      ContaIMEI := False;
    end;
    //
    DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
    //NohIMEI :=
    InjetaNodeAtual_B('IME-I ', Texto, Controle, Indice,
      Noh_20, emitIME_I, QtdItens, QtdTotal, PercentPai, ContaIMEI);
    //
    QryIMEI_A.Next;
  end;
  // Deve ter soh um, mas mostrar todos para evidenciar algum erro!
  QryIMEI_B.First;
  while not QryIMEI_B.Eof do
  begin
    MovimID  := QryIMEI_B.FieldByName('MovimID').AsInteger;
    MovimNiv := QryIMEI_B.FieldByName('MovimNiv').AsInteger;
    //SrcMovID := QryIMEI_B.FieldByName('SrcMovID').AsInteger;
    Controle := QryIMEI_B.FieldByName('Controle').AsInteger;
    // Resultado da soma acima do 07
    QtdItens := QtdSoma;
    //
    DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
    //NohIMEI :=
    InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
      Noh_21, emitIME_I, QtdItens, QtdTotal, PercentPai);
    //
    //
    QryIMEI_B.Next;
  end;

  //
  if QtdTotal = 0 then
    PercentNew := 0
  else
    PercentNew := PercentPai * (QtdSoma / QtdTotal);
  //  Listar gemeos de pallets retornados!
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEP, Dmod.MyDB, [
  'SELECT DISTINCT MovimTwn ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimTwn <> 0 ',
  'AND MovimCod=' + Geral.FF0(IMEC_Pai),
  'AND MovimNiv IN (' + Geral.FF0(Integer(eminDestWEnd)) +
  ', ' + Geral.FF0(Integer(eminEmWEndBxa)) + ') ',
  'ORDER BY MovimCod ',
  '']);
  QryIMEP.First;
  while not QryIMEP.Eof do
  begin
    // Injetar noh do IMEP
    MovimTwn := QryIMEP.FieldByName('MovimTwn').AsInteger;
    Indice := CO_IDX_IMG_GEMEOS;
    Texto  := 'Par de defini��o de pallet de artigo processado';
    Noh_22 := InjetaNodeAtual_A('IME-P ', Texto, MovimTwn,
      Indice, Noh_23, emitIME_P, 0, 0);

    // Listar os itens (IME-Is) da dupla totalizadora atual do Artigo gerado atual
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI_A, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimTwn <> 0 ',
    'AND MovimTwn=' + Geral.FF0(MovimTwn),
    'ORDER BY MovimNiv DESC', // 10 depois 09
    '']);
    //
    QryIMEI_A.First;
    while not QryIMEI_A.Eof do
    begin
      MovimID  := QryIMEI_A.FieldByName('MovimID').AsInteger;
      MovimNiv := QryIMEI_A.FieldByName('MovimNiv').AsInteger;
      Controle := QryIMEI_A.FieldByName('Controle').AsInteger;
      Pallet   := QryIMEI_A.FieldByName('Pallet').AsInteger;
      DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
      //
      QtdItens := 0;
      QtdTotal := 0;
      if TEstqMovimNiv(MovimNiv) = eminDestWEnd then
      begin
        if Pallet > 0 then
        begin
          VS_PF.ReopenVSPalletEntradasValidas(QryPall, Pallet);
          QtdItens := QryIMEI_A.FieldByName('AreaM2').AsFloat;
          QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
        end else
        begin
          // se nao for pallet, ver do IMEI Gerado!
          UnDmkDAC_PF.AbreMySQLQuery0(QryPall, Dmod.MyDB, [
          'SELECT * ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE Controle=' + Geral.FF0(Controle),
          '']);
          QtdItens := QryIMEI_A.FieldByName('AreaM2').AsFloat;
          QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
        end;
      end;
      NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
        Noh_22, emitIME_I, QtdItens, QtdTotal, PercentNew);
      //
      if TEstqMovimNiv(MovimNiv) = eminDestWEnd then
      begin
        InjetaDadosFilhosDeArtigoClassificado(Controle, Pallet, NohIMEI,
        QtdItens, QtdTotal, PercentNew);
      end;
      //
      QryIMEI_A.Next;
    end;
    //
    QryIMEP.Next;
  end;
  finally
    QryPall.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEI_B.Free;
  end;
  finally
    QryIMEI_A.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaDadosFilhosDeArtigoClassificado_Operacao(
  IMEI, Pallet: Integer; Noh_Pai: TTreeNode;
  //QryIMEC: TmySQLQuery;
  MovimCod, MovID_Pai: Integer;
  QtdItensPai, QtdTotalPai, PercentPai: Double);
var
  //MovimCod,
  Indice, MovimID, MovimNiv, Controle, SrcMovID, PalletFilho,
  MovimTwn, SrcNivel2: Integer;
  Texto: String;
  NohMovimCod, NohPallet, NohIMEI, Noh_07, Noh_08, Noh_09, Noh_10: TTreeNode;
  QryIMEI_A, QryIMEI_B, QryIMEP, QryPall: TmySQLQuery;
  QtdItens, QtdTotal, QtdSoma, PercentNew: Double;
  ContaIMEI: Boolean;
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  QryIMEI_A := TmySQLQuery.Create(Dmod);
  try
  QryIMEI_B := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryPall := TmySQLQuery.Create(Dmod);
  try
  //QtdItens := 0;
  //QtdTotal := 0;
  // Injetar noh do IMEC
  //MovimCod := QryIMEC.FieldByName('MovimCod').AsInteger;
  Indice := CO_IDX_IMG_OPERCAO;
  Texto  := 'Opera��o em Artigo';
  NohMovimCod := InjetaNodeAtual_IMEC(Texto, MovimCod, MovID_Pai, IMEI,
    Indice, Noh_Pai);
  // Injetar noh para separar Baixas de origem (Niv=7), Totalizador (Niv=8)
  // Geracao de destino (9) + Baixas de destino (10)
  Indice := CO_IDX_IMG_PREPARA_OPERAR;
  Noh_07 := InjetaNodeAtual_A('Baixas ', Texto, MovimCod,
    Indice, NohMovimCod, emitAgrupItensBxa, 0, 0, 0);
  Indice := CO_IDX_IMG_PREPARA_OPERAR;
  Noh_08 := InjetaNodeAtual_A('Em opera��o ', Texto, MovimCod,
    Indice, NohMovimCod, emitAgrupTotalizador, 0, 0, 0);
  Indice := CO_IDX_IMG_PREPARA_OPERAR;
  Noh_10 := InjetaNodeAtual_A('Operados ', Texto, MovimCod,
    Indice, NohMovimCod, emitAgrupItensNew, 0, 0, 0);
  //
  QtdSoma  := 0;
  QtdTotal := 0;
  // Abrir o 08 antes para saber o total de area!
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI_B, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminEmOperInn)),
  'ORDER BY MovimNiv DESC ',
  '']);
  QryIMEI_B.First;
  while not QryIMEI_B.Eof do
  begin
    QtdTotal := QtdTotal + QryIMEI_B.FieldByName('AreaM2').AsFloat;
    //
    QryIMEI_B.Next;
  end;
  // Abrir o 07 depois mas inserir antes do 08
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI_A, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),
  'ORDER BY MovimNiv DESC ',
  '']);
  QryIMEI_A.First;
  while not QryIMEI_A.Eof do
  begin
    MovimID  := QryIMEI_A.FieldByName('MovimID').AsInteger;
    MovimNiv := QryIMEI_A.FieldByName('MovimNiv').AsInteger;
    //SrcMovID := QryIMEI_A.FieldByName('SrcMovID').AsInteger;
    Controle := QryIMEI_A.FieldByName('Controle').AsInteger;
    SrcNivel2 := QryIMEI_A.FieldByName('SrcNivel2').AsInteger;
    // somente do IMEI em pequisa!
    if SrcNivel2 = IMEI then
    begin
      QtdItens  := - QryIMEI_A.FieldByName('AreaM2').AsFloat;
      QtdSoma   := QtdSoma + QtdItens;
      ContaIMEI := True;
    end else
    begin
      QtdItens  := 0;
      ContaIMEI := False;
    end;
    //
    DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
    //NohIMEI :=
    InjetaNodeAtual_B('IME-I ', Texto, Controle, Indice,
      Noh_07, emitIME_I, QtdItens, QtdTotal, PercentPai, ContaIMEI);
    //
    QryIMEI_A.Next;
  end;
  // Deve ter soh um, mas mostrar todos para evidenciar algum erro!
  QryIMEI_B.First;
  while not QryIMEI_B.Eof do
  begin
    MovimID  := QryIMEI_B.FieldByName('MovimID').AsInteger;
    MovimNiv := QryIMEI_B.FieldByName('MovimNiv').AsInteger;
    //SrcMovID := QryIMEI_B.FieldByName('SrcMovID').AsInteger;
    Controle := QryIMEI_B.FieldByName('Controle').AsInteger;
    // Resultado da soma acima do 07
    QtdItens := QtdSoma;
    //
    DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
    //NohIMEI :=
    InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
      Noh_08, emitIME_I, QtdItens, QtdTotal, PercentPai);
    //
    //
    QryIMEI_B.Next;
  end;

  //
  if QtdTotal = 0 then
    PercentNew := 0
  else
    PercentNew := PercentPai * (QtdSoma / QtdTotal);
  //  Listar gemeos de pallets retornados!
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEP, Dmod.MyDB, [
  'SELECT DISTINCT MovimTwn ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimTwn <> 0 ',
  'AND MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv IN (' + Geral.FF0(Integer(eminDestOper)) + // 9
  ', ' + Geral.FF0(Integer(eminEmOperBxa)) + ') ',         // 10
  'ORDER BY MovimCod ',
  '']);
  QryIMEP.First;
  while not QryIMEP.Eof do
  begin
    // Injetar noh do IMEP
    MovimTwn := QryIMEP.FieldByName('MovimTwn').AsInteger;
    Indice := CO_IDX_IMG_GEMEOS;
    Texto  := 'Par de defini��o de pallet de artigo operado';
    Noh_09 := InjetaNodeAtual_A('IME-P ', Texto, MovimTwn,
      Indice, Noh_10, emitIME_P, 0, 0);

    // Listar os itens (IME-Is) da dupla totalizadora atual do Artigo gerado atual
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI_A, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimTwn <> 0 ',
    'AND MovimTwn=' + Geral.FF0(MovimTwn),
    'ORDER BY MovimNiv DESC', // 10 depois 09
    '']);
    //
    QryIMEI_A.First;
    while not QryIMEI_A.Eof do
    begin
      MovimID  := QryIMEI_A.FieldByName('MovimID').AsInteger;
      MovimNiv := QryIMEI_A.FieldByName('MovimNiv').AsInteger;
      Controle := QryIMEI_A.FieldByName('Controle').AsInteger;
      Pallet   := QryIMEI_A.FieldByName('Pallet').AsInteger;
      DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
      //
      QtdItens := 0;
      QtdTotal := 0;
      if TEstqMovimNiv(MovimNiv) = eminDestOper then
      begin
        if Pallet > 0 then
        begin
          VS_PF.ReopenVSPalletEntradasValidas(QryPall, Pallet);
          QtdItens := QryIMEI_A.FieldByName('AreaM2').AsFloat;
          QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
        end else
        begin
          // se nao for pallet, ver do IMEI Gerado!
          UnDmkDAC_PF.AbreMySQLQuery0(QryPall, Dmod.MyDB, [
          'SELECT * ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE Controle=' + Geral.FF0(Controle),
          '']);
          QtdItens := QryIMEI_A.FieldByName('AreaM2').AsFloat;
          QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
        end;
      end;
      NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
        Noh_09, emitIME_I, QtdItens, QtdTotal, PercentNew);
      //
      if TEstqMovimNiv(MovimNiv) = eminDestOper then
      begin
        InjetaDadosFilhosDeArtigoClassificado(Controle, Pallet, NohIMEI,
        QtdItens, QtdTotal, PercentNew);
      end;
      //
      QryIMEI_A.Next;
    end;
    //
    QryIMEP.Next;
  end;
  finally
    QryPall.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEI_B.Free;
  end;
  finally
    QryIMEI_A.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaDadosFilhosDeArtigoClassificado_PreClasse(
  IMEI, Pallet: Integer; Noh_Pai: TTreeNode;
  //QryIMEC: TmySQLQuery;
  MovimCod, MovID_Pai: Integer;
  QtdItensPai, QtdTotalPai, PercentPai: Double);
var
  //MovimCod,
  Indice, MovimID, MovimNiv, Controle, SrcMovID, PalletFilho: Integer;
  Texto: String;
  NohMovimCod, NohPallet, NohIMEI: TTreeNode;
  QryIMEI, QryPrep, QryPall: TmySQLQuery;
  QtdItens, QtdTotal, PercentNew: Double;
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  QryPrep := TmySQLQuery.Create(Dmod);
  try
  QryPall := TmySQLQuery.Create(Dmod);
  try
  // Injetar noh do IMEC
  //MovimCod := QryIMEC.FieldByName('MovimCod').AsInteger;
  Indice := CO_IDX_IMG_PREPARA_RECLAS;
  Texto  := 'Prepara��o de Reclassifica��o de Artigo Classificado';
  NohMovimCod := InjetaNodeAtual_IMEC(Texto, MovimCod, MovID_Pai, IMEI,
    Indice, Noh_Pai);
  // Injetar noh para separar Baixas (Niv=11) do Totalizador (Niv=12)!
  Indice := CO_IDX_IMG_ITENSARECLAS;
  Texto  := 'Prepara��o de Reclassifica��o de Artigo Classificado';
  NohPallet := InjetaNodeAtual_A('Pallet ', Texto, Pallet,
    Indice, NohMovimCod, emitPallet, QtdItensPai, QtdTotalPai, PercentPai);
  // Listar os itens (IME-Is) do pallet a ser reclassificado
  // Pode ter mais de um
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminSorcPreReclas)),
  'ORDER BY Controle ',
  '']);
  //
  QryIMEI.First;
  while not QryIMEI.Eof do
  begin
    MovimID  := QryIMEI.FieldByName('MovimID').AsInteger;
    MovimNiv := QryIMEI.FieldByName('MovimNiv').AsInteger;
    Controle := QryIMEI.FieldByName('Controle').AsInteger;
    PalletFilho := QryIMEI.FieldByName('Pallet').AsInteger;
    //
    if PalletFilho > 0 then
    begin
      VS_PF.ReopenVSPalletEntradasValidas(QryPall, Pallet);
      QtdItens := -QryIMEI.FieldByName('AreaM2').AsFloat;
      QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
    end else
    begin
      QtdItens := 0;
      QtdTotal := 0;
    end;
    DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
    //NohIMEI :=
    InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
      NohPallet, emitIME_I, QtdItens, QtdTotal, PercentPai);
    QryIMEI.Next;
  end;

  // Novo IME-I �nico do pallet preparado para reclassificacao
  // Se houver mais de um � erro. Mostrar itens mesmo assim.
  UnDmkDAC_PF.AbreMySQLQuery0(QryPrep, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestPreReclas)),
  'ORDER BY Controle ',
  '']);
  //
  QryPrep.First;
  while not QryPrep.Eof do
  begin
    MovimID     := QryPrep.FieldByName('MovimID').AsInteger;
    MovimNiv    := QryPrep.FieldByName('MovimNiv').AsInteger;
    Controle    := QryPrep.FieldByName('Controle').AsInteger;
    SrcMovID    := QryPrep.FieldByName('SrcMovID').AsInteger;
    PalletFilho := QryPrep.FieldByName('Pallet').AsInteger;
    //
    if PalletFilho > 0 then
    begin
      VS_PF.ReopenVSPalletEntradasValidas(QryPall, Pallet);
      QtdItens := QryIMEI.FieldByName('AreaM2').AsFloat;
      QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
    end else
    begin
      QtdItens := 0;
      QtdTotal := 0;
    end;
    //
    DefineImageIndexETextoMovimB(MovimID, MovimNiv, SrcMovID, Indice, Texto);
    NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
      NohMovimCod, emitIME_I, QtdItens, QtdTotal, PercentPai);
    //
    if QtdTotal = 0 then
      PercentNew := 0
    else
    begin
      PercentNew := (QtdItens / QtdTotal);
      if PercentNew < 0 then
        PercentNew := -PercentNew;
      PercentNew := PercentPai * PercentNew;
    end;
    InjetaDadosFilhosDeArtigoPreReclassificado(Controle, PalletFilho, NohIMEI,
    (*QtdItens, QtdTotal,*) PercentNew);
    //
    QryPrep.Next;
  end;
  finally
    QryPall.Free;
  end;
  finally
    QryPrep.Free;
  end;
  finally
    QryIMEI.Free;
  end;
end;

{
procedure TFmVSArvoreArtigos.InjetaDadosFilhosDeArtigoGerado(IMEI: Integer;
  Noh_Pai: TTreeNode; QtdItensPai, QtdTotalPai: Double);
var
  QryIMEC, QryIMEP, QryIMEI, QryPall: TmySQLQuery;
  MovimCod, Indice, MovimTwn, MovimID, MovimNiv, Controle, Pallet: Integer;
  NohMovimCod, NohMovimTwn, NohIMEI: TTreeNode;
  Texto: String;
  PercentPai, QtdItens, QtdTotal: Double;
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  QryIMEC := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  QryPall := TmySQLQuery.Create(Dmod);
  try
  if QtdTotalPai = 0 then
    PercentPai := 0
  else
    PercentPai := QtdItensPai / QtdTotalPai * 100;
  // listar todas cabecalhos de Artigos em Classificacao deste Artigo Gerado
  // Deveria ter so um. Mostrar erro caso tenha mais!
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEC, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod  ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE SrcNivel2=' + Geral.FF0(IMEI),
  'ORDER BY MovimCod ',
  '']);
  QryIMEC.First;
  while not QryIMEC.Eof do
  begin
    // Injetar noh do IMEC
    MovimCod := QryIMEC.FieldByName('MovimCod').AsInteger;
    MovimID  := ???;
    Indice := CO_IDX_IMG_CLASSIFICAR;
    Texto  := 'Classifica��o de Artigo Gerado';
    NohMovimCod := InjetaNodeAtual_IMEC(Texto, MovimCod, MovimID,
      Indice, Noh_Pai);

    // Listar todas duplas totalizadoras do Artigo gerado atual
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEP, Dmod.MyDB, [
    'SELECT DISTINCT MovimTwn ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimTwn <> 0 ',
    'AND MovimCod=' + Geral.FF0(MovimCod),
    'AND SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    //
    QryIMEP.First;
    while not QryIMEP.Eof do
    begin
      // Injetar noh do IMEP
      MovimTwn := QryIMEP.FieldByName('MovimTwn').AsInteger;
      Indice := CO_IDX_IMG_GEMEOS;
      Texto  := 'Par de defini��o de classe';
      NohMovimTwn := InjetaNodeAtual_A('IME-P ', Texto, MovimTwn,
        Indice, NohMovimCod, emitIME_P, 0, 0);

      // Listar os itens (IME-Is) da dupla totalizadora atual do Artigo gerado atual
      UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimTwn <> 0 ',
      'AND MovimTwn=' + Geral.FF0(MovimTwn),
      'ORDER BY MovimNiv ',
      '']);
      //
      QryIMEI.First;
      while not QryIMEI.Eof do
      begin
        MovimID  := QryIMEI.FieldByName('MovimID').AsInteger;
        MovimNiv := QryIMEI.FieldByName('MovimNiv').AsInteger;
        Controle := QryIMEI.FieldByName('Controle').AsInteger;
        Pallet   := QryIMEI.FieldByName('Pallet').AsInteger;
        DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);
        //
        if Pallet > 0 then
        begin
          VS_PF.ReopenVSPalletEntradasValidas(QryPall, Pallet);
          QtdItens := QryIMEI.FieldByName('AreaM2').AsFloat;
          QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
        end else
        begin
          QtdItens := 0;
          QtdTotal := 0;
        end;
        NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
          NohMovimTwn, emitIME_I, QtdItens, QtdTotal, PercentPai);
        //
        case TEstqMovimID(MovimID) of
          emidClassArtXXUni,
          emidClassArtXXMul,
          emidPreReclasse:
          begin
            if TEstqMovimNiv(MovimNiv) = eminDestClass then
            begin
              //Fazer Percent Filho := QtdItens / QtdTotal * PercentPai ????
              InjetaDadosFilhosDeArtigoClassificado(Controle, Pallet, NohIMEI,
              QtdItens, QtdTotal, PercentPai);
            end;
          end;
          else MostraMBInfo('"MovimID n�o implementado (FilhosDeArtigoGerado)"'
          + sLineBreak + 'MovimID: ' + Geral.FF0(MovimID));
        end;
        //
        QryIMEI.Next;
      end;
      //
      QryIMEP.Next;
    end;
    //
    QryIMEC.Next;
  end;
  finally
    QryPall.Free;
  end;
  finally
    QryIMEI.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEC.Free;
  end;
end;
}

procedure TFmVSArvoreArtigos.InjetaDadosFilhosDeArtigoPreReclassificado(IMEI,
  Pallet: Integer; Noh_Pai: TTreeNode; PercentPai: Double);
var
  QryIMEC, QryIMEP, QryIMEI, QryPall: TmySQLQuery;
  MovimCod, Indice, MovimTwn, MovimID, MovimNiv, Controle, PalletFilho: Integer;
  NohMovimCod, NohMovimTwn, NohIMEI: TTreeNode;
  Texto: String;
  QtdItens, QtdTotal: Double;
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  QryIMEC := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  QryPall := TmySQLQuery.Create(Dmod);
  try
  // listar todas cabecalhos de Artigos em Reclassificacao deste Artigo Cassificado
  // Pode ter varios???
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEC, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod  ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE SrcNivel2=' + Geral.FF0(IMEI),
  'ORDER BY MovimCod ',
  '']);
  QryIMEC.First;
  while not QryIMEC.Eof do
  begin
    // Injetar noh do IMEC
    MovimCod := QryIMEC.FieldByName('MovimCod').AsInteger;
    MovimID  := Integer(emidPreReclasse);
    Indice := CO_IDX_IMG_CLASSIFICAR;
    Texto  := 'Reclassifica��o de Artigo Classificado';
    NohMovimCod := InjetaNodeAtual_IMEC(Texto, MovimCod, MovimID, IMEI, Indice,
      Noh_Pai);

    // Listar todas duplas totalizadoras do Artigo gerado atual
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEP, Dmod.MyDB, [
    'SELECT DISTINCT MovimTwn ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimTwn <> 0 ',
    'AND MovimCod=' + Geral.FF0(MovimCod),
    'AND SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    //
    QryIMEP.First;
    while not QryIMEP.Eof do
    begin
      // Injetar noh do IMEP
      MovimTwn := QryIMEP.FieldByName('MovimTwn').AsInteger;
      Indice := CO_IDX_IMG_GEMEOS;
      Texto  := 'Par de defini��o de classe';
      NohMovimTwn := InjetaNodeAtual_A('IME-P ', Texto, MovimTwn,
        Indice, NohMovimCod, emitIME_P);

      // Listar os itens (IME-Is) da dupla totalizadora atual do Artigo Classificado atual
      UnDmkDAC_PF.AbreMySQLQuery0(QryIMEI, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimTwn <> 0 ',
      'AND MovimTwn=' + Geral.FF0(MovimTwn),
      'ORDER BY MovimNiv ',
      '']);
      //
      QryIMEI.First;
      while not QryIMEI.Eof do
      begin
        MovimID     := QryIMEI.FieldByName('MovimID').AsInteger;
        MovimNiv    := QryIMEI.FieldByName('MovimNiv').AsInteger;
        Controle    := QryIMEI.FieldByName('Controle').AsInteger;
        PalletFilho := QryIMEI.FieldByName('Pallet').AsInteger;
        DefineImageIndexETextoMovimA(MovimID, MovimNiv, Indice, Texto);

        //
        if (PalletFilho > 0) and (TEstqMovimNiv(MovimNiv) = eminDestClass) then
        begin
          VS_PF.ReopenVSPalletEntradasValidas(QryPall, PalletFilho);
          QtdItens := QryIMEI.FieldByName('AreaM2').AsFloat;
          QtdTotal := QryPall.FieldByName('AreaM2').AsFloat;
        end else
        begin
          QtdItens := 0;
          QtdTotal := 0;
        end;
        NohIMEI := InjetaNodeAtual_A('IME-I ', Texto, Controle, Indice,
          NohMovimTwn, emitIME_I, QtdItens, QtdTotal, PercentPai);
        //
        if TEstqMovimNiv(MovimNiv) = eminDestClass then
          InjetaDadosFilhosDeArtigoClassificado(Controle, PalletFilho, NohIMEI,
          QtdItens, QtdTotal, PercentPai);
        //
        QryIMEI.Next;
        // Parei Aqui
      end;
      //
      QryIMEP.Next;
    end;
    //
    QryIMEC.Next;
  end;
  finally
    QryPall.Free;
  end;
  finally
    QryIMEI.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEC.Free;
  end;
end;

procedure TFmVSArvoreArtigos.InjetaDadosFilhosDeIMEI(*InNatura*)(IMEI, Pallet,
  MovimID_Pai, MovimNiv_Pai: Integer; Noh_Pai: TTreeNode; QtdItensPai,
  QtdTotalPai, PercentPai: Double; MudaField, FromPai: Boolean;
  SQL_Extra: String);
var
  QryIMEC, QryIMEP, QryIMEI: TmySQLQuery;
  MovimCod, MovimID: Integer;
  Texto, Campo, SQL_MovimCod: String;
  //QtdSoma: Double;
  //
  procedure MsgNiv();
  begin
    MostraMBInfo('"MovimID" n�o implementado "InjetaDadosFilhosDeIMEI"' +
    sLineBreak +
    'IMEI: ' + Geral.FF0(IMEI) + sLineBreak +
    'MovimID: ' + Geral.FF0(MovimID_Pai)  + sLineBreak +
    'MovimNiv: ' + Geral.FF0(MovimNiv_Pai));
  end;
begin
  Application.ProcessMessages;
  if FParar then
    Exit;
  //if Noh_Pai = nil then
    //Exit;
  //Noh_Mae := Noh_Pai;
  QryIMEC := TmySQLQuery.Create(Dmod);
  try
  QryIMEP := TmySQLQuery.Create(Dmod);
  try
  QryIMEI := TmySQLQuery.Create(Dmod);
  try
  //QtdSoma := 0;
  Campo := 'SrcNivel2';
  // listar todas cabecalhos de Artigos Gerados deste In Natura
  // Primeiro ver se � o IMEI objeto da pesquisa...
  if (IMEI = FIMEI_Raiz) and MudaField then
  begin
    //  Fazendo
    case TEStqMovimID(MovimID_Pai) of
      //emidAjuste=0,
      emidCompra:          Campo := 'SrcNivel2';   // =1.0,
      emidVenda:           Campo := 'Controle';    // =2.0,
      //emidReclasWE                               // =3.0,
      //emidBaixa                                  // =4.0,
      //emidIndsWE                                 // =5.0,
      emidIndsXX:        //Campo := 'SrcNivel2';   // =6.0,
      begin
        case TEstqMovimNiv(MovimNiv_Pai) of
          eminDestCurtiXX: Campo := 'SrcNivel2';   //6.13
          eminSorcCurtiXX: Campo := 'Controle';    //6.14
          eminBaixCurtiXX: Campo := 'Controle';    //6.15
          else MsgNiv();
        end;
        Texto := Texto + ' (' + sEstqMovimNiv[MovimNiv_Pai] + ')';
      end;
      emidClassArtXXUni,                           // =7.0,
      emidClassArtXXMul,                           // =14.0,
      emidReclasXXUni,                             // =8.0,
      emidReclasXXMul:                             // =24.0,
      begin
        case TEstqMovimNiv(MovimNiv_Pai) of
          eminSorcClass,                           // X.01
          eminSorcReclass: Campo := 'Controle';    // X.05
          eminDestClass,                           // X.02
          eminDestReclass: Campo := 'SrcNivel2';   // X.06
          else MsgNiv();
        end;
      end;
      emidForcado:         Campo := 'Controle';    // =9.0,
      emidSemOrigem:       Campo := 'Controle';    // =10.0,
      emidEmOperacao:                              // =11.0,
      begin
        case TEStqMovimNiv(MovimNiv_Pai) of
          eminSorcOper,                            // =11.07,
          eminEmOperInn,                           // =11.08,
          eminEmOperBxa:     Campo := 'Controle';  // =11.10,
          eminDestOper:      Campo := 'SrcNivel2'; // =11.09,
          else MsgNiv();
        end;
      end;
      emidResiduoReclas:     Campo := 'Controle';  // =12.0,
      emidInventario:        Campo := 'Controle';  // =13.0,
      emidPreReclasse:       Campo := 'Controle';  // =15.0,
      emidEntradaPlC:        Campo := 'Controle';  // =16.0,
      emidExtraBxa:          Campo := 'Controle';  // =17.0,
      emidSaldoAnterior:     Campo := 'SrcNivel2'; // =18.0,
      emidEmProcWE:                                // =19.0,
      begin
        case TEstqMovimNiv(MovimNiv_Pai) of
          eminSorcWEnd,                            // =19.20,
          eminEmWEndInn,                           // =19.21,
          eminEmWEndBxa:     Campo := 'Controle';  // =19.23,
          eminDestWEnd:      Campo := 'SrcNivel2'; // =19.22,
          else MsgNiv();
        end;
      end;
      emidFinished:          Campo := 'SrcNivel2'; // =20.0
      emidDevolucao:         Campo := 'Controle';  // =21.0,
      emidRetrabalho:        Campo := 'Controle';  // =22.0,
      emidGeraSubProd:       Campo := 'Controle';  // =23.0,
      else MostraMBInfo(
        '"MovimID" n�o implementado "InjetaDadosFilhosDeIMEI" [1]' + sLineBreak
        + 'IMEI: ' + Geral.FF0(IMEI) + sLineBreak + 'MovimID: ' +
        Geral.FF0(MovimID_Pai));
    end;
  end
  // se nao for o IMEI objeto da pesquisa, entao:
  else
  begin
    Campo := 'SrcNivel2';
  end;
  if FromPai then
    SQL_MovimCod := ''
  else
    SQL_MovimCod := 'AND MovimCod<>' + Geral.FF0(FMovCod_Raiz);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QryIMEC, Dmod.MyDB, [
  'SELECT DISTINCT MovimCod, MovimID  ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE ' + Campo + '=' + Geral.FF0(IMEI),
  SQL_MovimCod,
  SQL_Extra,
  'ORDER BY MovimCod ',
  '']);
  //Geral.MB_SQL(Self, QryIMEC);
  QryIMEC.First;
  while not QryIMEC.Eof do
  begin
    MovimCod := QryIMEC.FieldByName('MovimCod').AsInteger;
    MovimID  := QryIMEC.FieldByName('MovimID').AsInteger;
    //if FromPai then
      //FMovimCodFromPai := MovimCod;  >>   FMovCod_Raiz !!!
    InjetaDadosdeIMEC_Generico(IMEI, Pallet, MovimCod, MovimID, Noh_Pai,//Noh_Mae,
      QtdItensPai, QtdTotalPai, PercentPai, (*QryIMEC,*) Campo, MudaField,
      SQL_Extra);
    QryIMEC.Next;
  end;
  finally
    QryIMEI.Free;
  end;
  finally
    QryIMEP.Free;
  end;
  finally
    QryIMEC.Free;
  end;
end;

function TFmVSArvoreArtigos.InjetaNodeAtual_A(Titulo, Texto: String; PK, Index:
  Integer; NodePai: TTreeNode; MovimType: TEstqMovimType;
  ItensParte: Double; ItensTotal: Double; PercentPai: Double): TTreeNode;
const
  ContaIMEI = True;
begin
  Result := nil;
  Application.ProcessMessages;
  if FParar then
    Exit;
  Result := InjetaNodeAtual_B(Titulo, Texto, PK, Index, NodePai, MovimType,
  ItensParte, ItensTotal, PercentPai, ContaIMEI);
end;

function TFmVSArvoreArtigos.InjetaNodeAtual_B(Titulo, Texto: String; PK,
  Index: Integer; NodePai: TTreeNode; MovimType: TEstqMovimType; ItensParte,
  ItensTotal, PercentPai: Double; ContaIMEI: Boolean): TTreeNode;
const
  TemIMEIMrt = 0;  // Ver o que fazer!
var
  Sentenca, Txt: String;
  Pallet: Integer;
  Qry: TmySQLQuery;
  Pecas, PesoKg, AreaM2, Percentual: Double;
  ItensLoc: Integer;
  treeItem: TTVItem;
  Noh: TTreeNode;
  Nivel, Itens: Integer;
  IMEx: TIMExReg;
begin
  Result := nil;
  //if PK = 12092 then
    //Geral.MB_Info('12092');
  //Exit;
  //
  Application.ProcessMessages;
  if FParar then
    Exit;
  if NodePai <> nil then
    Txt := NodePai.Text
  else
    Txt := '';

{
  case MovimType of
    emitIME_I:
    begin
      if VS_PF.IMEI_JaEstaNoArray(PK, FJaIMEIs, ItensLoc) then
      begin
        Memo1.Lines.Add('IME-I j� inserido: ' + Geral.FF0(PK) + '  N� pai: ' +
        Txt + '  IME-I raiz: ' + Geral.FF0(FIMEI_Raiz));
        //criar variavel para IMEI em nivelacao!
        Exit;
      end;
    end;
    emitIME_C:
    begin
      // Feito no InjetaNodeAtual_IMEC
(*      if VS_PF.IMEI_JaEstaNoArray(PK, FJaIMECs, ItensLoc) then
      begin
        Memo1.Lines.Add('IME-C j� inserido: ' + Geral.FF0(PK) + '  N� pai: ' +
        Txt + '  IME-I raiz: ' + Geral.FF0(FIMEI_Raiz));
        Exit;
      end;
*)
    end;
    emitIME_P:
    begin
      if VS_PF.IMEI_JaEstaNoArray(PK, FJaIMEPs, ItensLoc) then
      begin
        Memo1.Lines.Add('IME-P j� inserido: ' + Geral.FF0(PK) + '  N� pai: ' +
        Txt + '  IME-I raiz: ' + Geral.FF0(FIMEI_Raiz));
        Exit;
      end;
    end;
  end;
}
  Sentenca := Trim(Titulo) + ' ' + Geral.FF0(PK);
  if Trim(Texto) <> '' then
    Sentenca := Sentenca + ' :: ' + Texto;
  //
  case MovimType of
    //emitIndef=0,
    emitIME_I: //1
    begin
      FIMEI_Atual := PK;
      Qry := TmySQLQuery.Create(Dmod.MyDB);
      try
        VS_PF.ReopenVSIts_Controle_If(Qry, PK, TemIMEIMrt);
        //
        Pallet  := Qry.FieldByName('Pallet').AsInteger;
        Pecas   := Qry.FieldByName('Pecas').AsFloat;
        PesoKg  := Qry.FieldByName('PesoKg').AsFloat;
        AreaM2  := Qry.FieldByName('AreaM2').AsFloat;
        //
        if Pallet <> 0 then
          Sentenca := Sentenca + ' Pallet ' + Geral.FF0(Pallet);
        Sentenca := Sentenca + ' ' + Qry.FieldByName('NO_PRD_TAM_COR').AsString + ' (';
        if Pecas <> 0 then
          Sentenca := Sentenca + ' ' + Geral.FFI(Pecas) + ' pe�as';
        if PesoKg <> 0 then
          Sentenca := Sentenca + ' ' + Geral.FFI(PesoKg) + ' kg';
        if AreaM2 <> 0 then
          Sentenca := Sentenca + ' ' + Geral.FFT(AreaM2, 2, siNegativo) + ' m�';
        Sentenca := Sentenca + ')';
      finally
        Qry.Free;
      end;
      if ContaIMEI then
      begin
        SetLength(FJaIMEIs, Length(FJaIMEIs) + 1);
        FJaIMEIs[Length(FJaIMEIs)-1] := PK;
      if ContaIMEI and (MovimType = emitIME_I) then
        FAtualizados := FAtualizados + 1;
      end;
    end;
    emitIME_C:  //2
    begin
      // Feito no InjetaNodeAtual_IMEC
(*
      SetLength(FJaIMECs, Length(FJaIMECs) + 1);
      FJaIMECs[Length(FJaIMECs)-1][0] := PK;
      FJaIMECs[Length(FJaIMECs)-1][1] := ?;
*)
    end;
    emitIME_P: ; //3
    emitPallet: ; //4
    emitAvulsos: ; //5
    emitAgrupTotalizador: ; //6
    emitAgrupItensNew: ; //7
    emitAgrupItensBxa: ; // 8
    else MostraMBInfo(
    '"MovimType" n�o implementado "InjetaNodeAtual" [1]');
  end;
  if ItensParte <> 0 then
  begin
    if ItensTotal > 0 then
      Percentual := ItensParte / ItensTotal
    else
      Percentual := 0;
    //
    Percentual := Int(Percentual * PercentPai);
    Sentenca := Sentenca + ' (' + Geral.FFI(Percentual) + '%)';
  end;
  if NodePai <> nil then
    Result := TreeView1.Items.AddChild(NodePai, Sentenca)
  else
    Result := TreeView1.Items.Add(nil, Sentenca);
  Result.ImageIndex := Index;
  Result.StateIndex := Integer(MovimType);
  Result.SelectedIndex := PK;
  if (PK = FIMEI_Raiz) and (MovimType = emitIME_I) then
  begin
    FNodeIMEI := Result;
   //SendMessage(TreeView1.Handle, TVM_SETLINECOLOR, 0, ColorToRGB(clYellow));
   //



(*
   if not Assigned(Result) then Exit;
   with treeItem do
   begin
    hItem := Result.ItemId;
    stateMask := TVIS_BOLD;
    mask := TVIF_HANDLE or TVIF_STATE;
    //if Value then
      state := TVIS_BOLD
    //else
      //state := 0;
   end;
*)


 (*
    with Result.Canvas do
    begin
      Brush.Color := clBlack;
      FillRect(Rect);
      Font.Color := clWhite;
      OldAlign := SetTextAlign(Result.Handle, TA_LEFT);
      FillRect(MyRect);
      case Alinha of
        taRightJustify: TextOut(RI, MyRect.Top, Texto);
        taLeftJustify:  TextOut(LE, MyRect.Top, Texto);
        taCenter:       TextOut(CE, MyRect.Top, Texto);
        else            TextOut(LE, MyRect.Top, Texto);
      end;
      SetTextAlign(Handle, OldAlign);
      Polygon([Point(L, T), Point(L, B), Point(R, B), Point(R, T)]);
      Font.Style := [fsBold];
      Font.Color := clBlue;
      FillRect(Rect);
      TextOut(Rect.Left + 2, Rect.Top  + 2, Result.Text);
    end;
*)
(*
    procedure TForm1.TreeView1CustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
    begin
      with Sender as TCustomTreeView do
      begin
        Canvas.Brush.Color := clBlack;
        Canvas.Font.Color := clBlack
      end;
    end;
*)
  end;


(*

  if MovimType = emitIME_I then
  begin
    if Result.Level > FIMEI_Niv_Z then

    FIMEI_Cod_X, FIMEI_Cod_Y, FIMEI_Cod_Z,
    FIMEI_Niv_X, FIMEI_Niv_Y, FIMEI_Niv_Z: Integer;

    SetLength(FIMEIsN, 0);
    /
  end;
*)

  //if MovimType = emitIME_I then
  begin
    Nivel := 0;
    Noh := Result;
    while (Noh <> nil) and (Nivel < 32) do
    begin
      IMEx[Nivel][0] := Noh.StateIndex;
      IMEx[Nivel][1] := Noh.SelectedIndex;
      //
      Noh := Noh.Parent;
      //if Noh <> nil then
      //  Geral.MB_Aviso(Noh.Text);
      Nivel := Nivel + 1;
    end;
    if VS_PF.IMEx_JaEstaNoArray(IMEx, FListaIMEx) then
    begin
      Memo1.Lines.Add(
      sEstqMovimType[Integer(MovimType)] + ' j� inserido: ' + Geral.FF0(PK) +
      '  N� pai: ' + Txt +
      '  IME-I raiz: ' + Geral.FF0(FIMEI_Raiz));
    end;
  end;

  //VS_PF.IMEx_InsereNoArray(IMEx, FListaIMEx);
  Itens := Length(FListaIMEx) + 1;
  SetLength(FListaIMEx, Itens);
  FListaIMEx[Itens - 1] := IMEx;

  FIMEI_Atual := 0;
  //Geral.MB_Info(Geral.FF0(Result.Level));
end;

function TFmVSArvoreArtigos.InjetaNodeAtual_IMEC(Texto: String; IMEC, MovimID,
  IMEI_Pai, Index: Integer; NodePai: TTreeNode): TTreeNode;
var
  ItensLoc: Integer;
  Txt: String;
begin
  Result := nil;
  if NodePai <> nil then
    Txt := NodePai.Text
  else
    Txt := '';
  if VS_PF.IMEC_JaEstaNoArray(IMEC, MovimID, IMEI_Pai, FJaIMECs, ItensLoc) then
  begin
    Memo1.Lines.Add('IME-C j� inserido: ' + Geral.FF0(IMEC) +
    '  MovimID: ' + Geral.FF0(MovimID) +
    '  N� pai: ' + Txt +
    '  IME-I raiz: ' + Geral.FF0(FIMEI_Raiz));
    //Exit;
  end;
  //
  Result := InjetaNodeAtual_B('IMEC ', Texto, IMEC, Index, NodePai, emitIME_C,
    0, 0, 0, False);
  //
  SetLength(FJaIMECs, Length(FJaIMECs) + 1);
  FJaIMECs[Length(FJaIMECs)-1][0] := IMEC;
  FJaIMECs[Length(FJaIMECs)-1][1] := MovimID;
end;

procedure TFmVSArvoreArtigos.MontaArvore(IMEI_Raiz: Integer);
const
  PercentPai = 100;
  MudaCampo  = True;
  FromPai    = True;
var
  IMEIS, Texto: String;
  Indice, Pallet: Integer;
  TvIMEI_06_13: TTreeNode;
  QtdItens, QtdTotal: Double;
  TipoCalc: TTipoCalcCouro;
  InsereRaiz: Boolean;
  //
  procedure MsgNiv();
  begin
    MostraMBInfo('"MovimID" n�o implementado "InjetaDadosFilhosDeIMEI"' +
    sLineBreak +
    'IMEI: ' + Geral.FF0(IMEI_Raiz) + sLineBreak +
    'MovimID: ' + Geral.FF0(FMovID_Raiz)  + sLineBreak +
    'MovimNiv: ' + Geral.FF0(FMovNiv_Raiz));
  end;
begin
  InsereRaiz := True;
  TreeView1.Items.Clear;
  FNodeIMEI := nil;
  SetLength(FListaIMEx, 0);
(*
  FIMEI_X := 0;
  FIMEI_Y := 0;
  FIMEI_Z := 0;
  SetLength(FIMEIsN, 0);
*)
  //
  Application.ProcessMessages;
  if FParar then
    Exit;
    //
  FIMEI_Raiz := IMEI_Raiz;
  FParar := False;
  FAtualizados := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr013, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE Controle=' + Geral.FF0(IMEI_Raiz),
  '']);
  //
  if Qr013.RecordCount = 0 then
  begin
    MostraMBInfo('O IME-I ' + Geral.FF0(IMEI_Raiz) + ' n�o foi localizado!');
    Exit;
  end;
  try
  FMovID_Raiz  := Qr013MovimID.Value;
  FMovNiv_Raiz := Qr013MovimNiv.Value;
  FMovCod_Raiz := Qr013MovimCod.Value;

  SetLength(FJaIMEIs, 0);
  SetLength(FJaIMECs, 0);
  SetLength(FJaIMEPs, 0);
  //
  Pallet  := Qr013Pallet.Value;
  DefineQtdeItensDeIMEI(Qr013, QtdItens, TipoCalc);
  QtdTotal := QtdItens;
  case TEStqMovimID(FMovID_Raiz) of
    // Fazendo
    //emidAjuste=0,
    emidCompra:       InsereRaiz := True;      // =1.0,
    emidVenda:        InsereRaiz := False;     // =2.0,
    //emidReclasWE                             // =3.0,
    //emidBaixa                                // =4.0,
    //emidIndsWE                               // =5.0,
    emidIndsXX:        //InsereRaiz := True;   // =6.0,
    begin
      case TEstqMovimNiv(FMovNiv_Raiz) of
        eminDestCurtiXX: InsereRaiz := True;   // 6.13
        eminSorcCurtiXX: InsereRaiz := False;  // 6.14
        eminBaixCurtiXX: InsereRaiz := False;  // 6.15
        else MsgNiv();
      end;
      Texto := Texto + ' (' + sEstqMovimNiv[FMovNiv_Raiz] + ')';
    end;
    emidClassArtXXUni,                         // =7.0,
    emidClassArtXXMul,                         // =14.0,
    emidReclasXXUni,                           // =8.0,
    emidReclasXXMul:                           // =24.0,
    begin
      case TEstqMovimNiv(FMovNiv_Raiz) of
        eminSorcClass,                         //X.01
        eminSorcReclass: InsereRaiz := False;  //X.05
        eminDestClass,                         //X.02
        eminDestReclass: InsereRaiz := True;   //X.06
        else MsgNiv();
      end;
    end;
    emidForcado:         InsereRaiz := False;  // =9.0,
    emidSemOrigem:       InsereRaiz := False;  // =10.0,
    emidEmOperacao:                            // =11.0,
    begin
      case TEStqMovimNiv(FMovNiv_Raiz) of
        eminSorcOper,                          // =11.07,
        eminEmOperInn,                         // =11.08,
        eminEmOperBxa:  InsereRaiz := False;   // =11.10,
        eminDestOper:   InsereRaiz := True;    // =11.09,
        else MsgNiv();
      end;
    end;
    emidResiduoReclas:  InsereRaiz := False;   // =12.0,
    emidInventario:     InsereRaiz := False;   // =13.0,
    emidPreReclasse:    InsereRaiz := False;   // =15.0,
    emidEntradaPlC:     InsereRaiz := False;   // =16.0,
    emidExtraBxa:       InsereRaiz := False;   // =17.0,
    emidSaldoAnterior:  InsereRaiz := False;   // =18.0,
    emidEmProcWE:                              // =19.0,
    begin
      case TEstqMovimNiv(FMovNiv_Raiz) of
        eminSorcWEnd,                          // =19.20,
        eminEmWEndInn,                         // =19.21,
        eminEmWEndBxa: InsereRaiz := False;    // =19.23,
        eminDestWEnd:  InsereRaiz := True;     // =19.22,
        else MsgNiv();
      end;
    end;
    emidFinished:      InsereRaiz := True;     // =20.0
    emidDevolucao:     InsereRaiz := False;    // =21.0,
    emidRetrabalho:    InsereRaiz := False;    // =22.0,
    emidGeraSubProd:   InsereRaiz := True;     // =23.0,
    else MostraMBInfo(
      '"MovimID" n�o implementado "InjetaDadosFilhosDeIMEI" [1]' + sLineBreak
      + 'IMEI: ' + Geral.FF0(IMEI_Raiz) + sLineBreak + 'MovimID: ' +
      Geral.FF0(FMovID_Raiz));
  end;
  if InsereRaiz then
  begin
    DefineImageIndexETextoMovimA(FMovID_Raiz, FMovNiv_Raiz, Indice, Texto);
    TvIMEI_06_13 := InjetaNodeAtual_A('IME-I ', Texto, IMEI_Raiz,
      Indice, nil, emitIME_I, QtdItens, QtdTotal, PercentPai);
  end else
  begin
    TvIMEI_06_13 := nil;
  end;
  //
(*  if FMovID_Raiz = 6 then
  begin
    InjetaDadosDeIMEC_Generico(IMEI_Raiz, Pallet, FMovCod_Raiz, TvIMEI_06_13,
      QtdItens, QtdTotal, PercentPai, 'Controle', MudaCampo);
  end else
  begin
*)
    InjetaDadosFilhosDeIMEI(*InNatura*)(IMEI_Raiz, Pallet, FMovID_Raiz,
      FMovNiv_Raiz, TvIMEI_06_13, QtdItens, QtdTotal, PercentPai, MudaCampo,
      FromPai);
(*
  end;
*)
  //
  IMEIS := VS_PF.CordaIMEIS(FJaIMEIs);
  Memo2.Lines.Add('IME-I ' + Geral.FF0(IMEI_Raiz) + ' > Itens vinculados: ' + Geral.FF0(FAtualizados));
  Memo2.Lines.Add('IMEIS vinculados: ' + (*sLineBreak +*) IMEIs);
  //
  finally
    TreeView1.FullExpand;
  end;
  FMsgMemo := False;
  if FNodeIMEI <> nil then
  begin
    //FNodeIMEI.Selected := True;
    TreeView1.Selected := FNodeIMEI;
  end;
end;

procedure TFmVSArvoreArtigos.MostraMBInfo(Msg: String);
begin
  if FMsgMemo then
    Memo1.Lines.Add( Geral.Substitui(Geral.Substitui(Msg, #13, ';'), #10, ';'))
  else
    Geral.MB_Info(Msg);
end;

procedure TFmVSArvoreArtigos.TreeView1CustomDraw(Sender: TCustomTreeView;
  const ARect: TRect; var DefaultDraw: Boolean);
begin
(*
  if (FIMEI_Atual <> 0) and (FIMEI_Atual = FIMEI_Raiz) then
  begin
    with Sender as TCustomTreeView do
    begin
      Canvas.Brush.Color := clBlack;
      Canvas.Font.Color := clWhite;
    end;
  end;
*)
end;

procedure TFmVSArvoreArtigos.TreeView1CustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
(*
  if (FIMEI_Atual <> 0) and (FIMEI_Atual = FIMEI_Raiz) then
  begin
    with Node as TTreeNode do
    begin
      Canvas.Brush.Color := clRed;
      Canvas.Font.Color := clYellow;
    end;
  end;
*)
end;

procedure TFmVSArvoreArtigos.TreeView1GetImageIndex(Sender: TObject;
  Node: TTreeNode);
begin
  if Node.Focused then
    Exit;
end;

end.
