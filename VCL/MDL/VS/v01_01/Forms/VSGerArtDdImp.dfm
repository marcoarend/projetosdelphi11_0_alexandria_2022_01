object FmVSGerArtDdImp: TFmVSGerArtDdImp
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-242 :: Gera'#231#227'o de Artigos por Dia'
  ClientHeight = 563
  ClientWidth = 739
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 739
    Height = 389
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 739
      Height = 97
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 4
        Top = 4
        Width = 1113
        Height = 105
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox13: TGroupBox
          Left = 484
          Top = 1
          Width = 245
          Height = 69
          Caption = 'Data / hora compra:'
          Enabled = False
          TabOrder = 0
          object TPCompraIni: TdmkEditDateTimePicker
            Left = 8
            Top = 40
            Width = 112
            Height = 21
            CalColors.TextColor = clMenuText
            Date = 37636.000000000000000000
            Time = 0.777157974502188200
            TabOrder = 1
            OnClick = TPCompraIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPCompraIniRedefInPlace
          end
          object CkCompraIni: TCheckBox
            Left = 8
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data inicial'
            TabOrder = 0
            OnClick = CkCompraIniClick
          end
          object CkCompraFim: TCheckBox
            Left = 124
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data final'
            TabOrder = 2
            OnClick = CkCompraFimClick
          end
          object TPCompraFim: TdmkEditDateTimePicker
            Left = 124
            Top = 40
            Width = 112
            Height = 21
            Date = 37636.000000000000000000
            Time = 0.777203761601413100
            TabOrder = 3
            OnClick = TPCompraFimClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPCompraFimRedefInPlace
          end
        end
        object GroupBox1: TGroupBox
          Left = 732
          Top = 1
          Width = 245
          Height = 69
          Caption = 'Data / sa'#237'da:'
          Enabled = False
          TabOrder = 1
          object TPViagemIni: TdmkEditDateTimePicker
            Left = 8
            Top = 40
            Width = 112
            Height = 21
            CalColors.TextColor = clMenuText
            Date = 37636.000000000000000000
            Time = 0.777157974502188200
            TabOrder = 1
            OnClick = TPViagemIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPViagemIniRedefInPlace
          end
          object CkViagemIni: TCheckBox
            Left = 8
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data inicial'
            TabOrder = 0
            OnClick = CkViagemIniClick
          end
          object CkViagemFim: TCheckBox
            Left = 124
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data final'
            TabOrder = 2
            OnClick = CkViagemFimClick
          end
          object TPViagemFim: TdmkEditDateTimePicker
            Left = 124
            Top = 40
            Width = 112
            Height = 21
            Date = 37636.000000000000000000
            Time = 0.777203761601413100
            TabOrder = 3
            OnClick = TPViagemFimClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPViagemFimRedefInPlace
          end
        end
        object GroupBox2: TGroupBox
          Left = 0
          Top = 1
          Width = 245
          Height = 69
          Caption = 'Data / hora chegada:'
          TabOrder = 2
          object TPEntradaIni: TdmkEditDateTimePicker
            Left = 8
            Top = 40
            Width = 112
            Height = 21
            CalColors.TextColor = clMenuText
            Date = 37636.000000000000000000
            Time = 0.777157974502188200
            TabOrder = 1
            OnClick = TPEntradaIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPEntradaIniRedefInPlace
          end
          object CkEntradaIni: TCheckBox
            Left = 8
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data inicial'
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = CkEntradaIniClick
          end
          object CkEntradaFim: TCheckBox
            Left = 124
            Top = 20
            Width = 112
            Height = 17
            Caption = 'Data final'
            Checked = True
            State = cbChecked
            TabOrder = 2
            OnClick = CkEntradaFimClick
          end
          object TPEntradaFim: TdmkEditDateTimePicker
            Left = 124
            Top = 40
            Width = 112
            Height = 21
            Date = 37636.000000000000000000
            Time = 0.777203761601413100
            TabOrder = 3
            OnClick = TPEntradaFimClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
            OnRedefInPlace = TPEntradaFimRedefInPlace
          end
        end
        object CkTemIMEIMrt: TCheckBox
          Left = 0
          Top = 72
          Width = 149
          Height = 17
          Caption = 'Incluir arquivo morto.'
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = CkTemIMEIMrtClick
        end
        object GroupBox3: TGroupBox
          Left = 252
          Top = 1
          Width = 225
          Height = 69
          Caption = ' Impress'#227'o: '
          TabOrder = 4
          object RGFonte: TRadioGroup
            Left = 8
            Top = 20
            Width = 209
            Height = 41
            Caption = ' Fonte: '
            Columns = 5
            ItemIndex = 1
            Items.Strings = (
              '6'
              '7'
              '8'
              '9'
              '10')
            TabOrder = 0
          end
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 97
      Width = 739
      Height = 292
      Align = alClient
      DataSource = DsVSGer
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 739
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 691
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 643
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 330
        Height = 32
        Caption = 'Gera'#231#227'o de Artigos por Dia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 330
        Height = 32
        Caption = 'Gera'#231#227'o de Artigos por Dia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 330
        Height = 32
        Caption = 'Gera'#231#227'o de Artigos por Dia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 437
    Width = 739
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 735
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 735
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 493
    Width = 739
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 593
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 591
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 20
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 65523
  end
  object QrExec: TMySQLQuery
    Left = 472
    Top = 4
  end
  object QrVSGer: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT CAST(dst.GraGruX AS SIGNED) GraGruX, '
      'CAST(dst.MovimID AS SIGNED) DstMovID, '
      'dst.AnoMesDiaIn, dst.ArtGeComodty,'
      'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg,'
      
        'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2' +
        ','
      'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2,'
      'SUM(-dst.QtdGerArM2) / SUM(-dst.Pecas) MediaM2,'
      ''
      'SUM(inn.Pecas) InnPecas,  '
      'SUM(inn.PesoKg) InnPesoKg,  '
      'SUM(inn.PesoKg) / SUM(inn.Pecas) InnKgPeca,  '
      'SUM(inn.InfPecas) NFePecas,  '
      'SUM(inn.Pecas) - SUM(inn.SdoVrtPeca) CalPecas '
      ''
      'FROM _vs_in_cab_e_its inn '
      'LEFT JOIN _vs_ga_cab_e_its dst ON  '
      '  dst.SrcMovID=inn.MovimID '
      '  AND dst.SrcNivel1=inn.Codigo '
      '  AND dst.SrcNivel2=inn.Controle '
      'GROUP BY AnoMesDiaIn, dst.MovimID, GraGruX '
      'ORDER BY AnoMesDiaIn, dst.MovimID, GraGruX '
      ''
      ''
      '/*SELECT AnoMesDiaIn, ArtGeComodty,'
      'SUM(-Pecas) Pecas, SUM(-QtdGerPeso) QtdGerPeso,'
      'SUM(-QtdGerArP2) QtdGerArM2, SUM(-QtdGerArP2) QtdGerArP2,'
      'SUM(-PesoKg) / SUM(-QtdGerArM2) RendKgM2,'
      'SUM(-QtdGerArM2) / SUM(-Pecas) MediaM2'
      'FROM _vs_ga_cab_e_its'
      'GROUP BY AnoMesDiaIn, ArtGeComodty'
      'ORDER BY AnoMesDiaIn, ArtGeComodty'
      '*/')
    Left = 494
    Top = 175
    object QrVSGerGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSGerAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
      Required = True
    end
    object QrVSGerArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
      Required = True
    end
    object QrVSGerPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSGerPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSGerQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSGerQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSGerRendKgM2: TFloatField
      FieldName = 'RendKgM2'
    end
    object QrVSGerMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
    object QrVSGerDstMovID: TLargeintField
      FieldName = 'DstMovID'
    end
    object QrVSGerInnPecas: TFloatField
      FieldName = 'InnPecas'
    end
    object QrVSGerInnPesoKg: TFloatField
      FieldName = 'InnPesoKg'
    end
    object QrVSGerInnKgPeca: TFloatField
      FieldName = 'InnKgPeca'
    end
    object QrVSGerNFePecas: TFloatField
      FieldName = 'NFePecas'
    end
    object QrVSGerCalPecas: TFloatField
      FieldName = 'CalPecas'
    end
    object QrVSGerGGXInn: TLargeintField
      FieldName = 'GGXInn'
    end
    object QrVSGerResVrtPeca: TFloatField
      FieldName = 'ResVrtPeca'
    end
    object QrVSGerResVrtPeso: TFloatField
      FieldName = 'ResVrtPeso'
    end
    object QrVSGerDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
    end
  end
  object QrDia: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 536
    Top = 4
    object QrDiaInnPecas: TFloatField
      FieldName = 'InnPecas'
    end
  end
  object QrArtGeComodty: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT gci.ArtGeComodty, agc.Sigla, agc.VisuRel,'
      'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,'
      'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,'
      'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2'
      'FROM _vs_ga_cab_e_its gci'
      
        'LEFT JOIN bluederm_ciaanterior.artgecomodty agc ON agc.Codigo=gc' +
        'i.ArtGeComodty'
      'GROUP BY ArtGeComodty'
      'ORDER BY ArtGeComOrd, ArtGeComodty ')
    Left = 580
    Top = 120
    object QrArtGeComodtyArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
      Required = True
    end
    object QrArtGeComodtySigla: TWideStringField
      FieldName = 'Sigla'
      Size = 10
    end
    object QrArtGeComodtyVisuRel: TIntegerField
      FieldName = 'VisuRel'
    end
    object QrArtGeComodtyShowPecas: TLargeintField
      FieldName = 'ShowPecas'
    end
    object QrArtGeComodtyShowPesoKg: TLargeintField
      FieldName = 'ShowPesoKg'
    end
    object QrArtGeComodtyShowAreaM2: TLargeintField
      FieldName = 'ShowAreaM2'
    end
    object QrArtGeComodtyShowPercPc: TLargeintField
      FieldName = 'ShowPercPc'
    end
    object QrArtGeComodtyShowKgM2: TLargeintField
      FieldName = 'ShowKgM2'
    end
    object QrArtGeComodtyShowMediaM2: TLargeintField
      FieldName = 'ShowMediaM2'
    end
  end
  object frxWET_CURTI_242_1_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44981.814375798610000000
    ReportOptions.LastChange = 44981.814375798610000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 176
    Top = 220
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
    end
  end
  object QrGafid: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT gg1.Nome  NO_GGXInn, afi.* '
      'FROM _vs_ger_art_from_in_dd afi'
      
        'LEFT JOIN bluederm_ciaanterior.gragrux ggx ON ggx.Controle=afi.G' +
        'GXInn'
      
        'LEFT JOIN bluederm_ciaanterior.gragru1 gg1 ON gg1.Nivel1=ggx.Gra' +
        'Gru1')
    Left = 50
    Top = 151
  end
  object frxDsGafid: TfrxDBDataset
    UserName = 'frxDBGafid'
    CloseDataSource = False
    DataSet = QrGafid
    BCDToCurrency = False
    DataSetOptions = []
    Left = 48
    Top = 200
  end
  object QrEmids: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT DISTINCT dst.MovimID DstMovID '
      'FROM _vs_in_cab_e_its inn'
      'LEFT JOIN _vs_ga_cab_e_its dst ON '
      '  dst.SrcMovID=inn.MovimID'
      '  AND dst.SrcNivel1=inn.Codigo'
      '  AND dst.SrcNivel2=inn.Controle'
      ''
      'WHERE '
      '  dst.MovimID IS NULL'
      '  OR dst.MovimID <> 6'
      ''
      'ORDER BY DstMovID ')
    Left = 388
    Top = 248
    object QrEmidsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 60
    end
    object QrEmidsDstMovID: TLargeintField
      FieldName = 'DstMovID'
    end
  end
  object DsVSGer: TDataSource
    DataSet = QrVSGer
    Left = 492
    Top = 224
  end
  object QrVSInn: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT CAST(GraGruX AS SIGNED) GGXInn, AnoMesDiaIn, '
      'DATE_FORMAT(DtEntrada, "%Y-%m-%d") Data,'
      'SUM(Pecas) InnPecas, '
      'SUM(PesoKg) InnPesoKg, '
      'SUM(PesoKg) / SUM(Pecas) InnKgPeca, '
      'SUM(InfPecas) NFePecas, SUM(Pecas) - SUM(SdoVrtPeca) CalPecas  '
      'FROM _vs_in_cab_e_its'
      'GROUP BY GGXInn, AnoMesDiaIn'
      'ORDER BY GGXInn, AnoMesDiaIn')
    Left = 564
    Top = 312
    object QrVSInnGGXInn: TLargeintField
      FieldName = 'GGXInn'
      Required = True
    end
    object QrVSInnAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
      Required = True
    end
    object QrVSInnData: TWideStringField
      FieldName = 'Data'
      Size = 10
    end
    object QrVSInnInnPecas: TFloatField
      FieldName = 'InnPecas'
    end
    object QrVSInnInnPesoKg: TFloatField
      FieldName = 'InnPesoKg'
    end
    object QrVSInnInnKgPeca: TFloatField
      FieldName = 'InnKgPeca'
    end
    object QrVSInnNFePecas: TFloatField
      FieldName = 'NFePecas'
    end
    object QrVSInnCalPecas: TFloatField
      FieldName = 'CalPecas'
    end
    object QrVSInnInnSdoVrtPeso: TFloatField
      FieldName = 'InnSdoVrtPeso'
    end
    object QrVSInnInnSdoVrtPeca: TFloatField
      FieldName = 'InnSdoVrtPeca'
    end
  end
  object QrNaoProc: TMySQLQuery
    Left = 116
    Top = 380
    object QrNaoProcInnSdoVrtPeca: TFloatField
      FieldName = 'InnSdoVrtPeca'
    end
  end
  object QrOrfaos: TMySQLQuery
    Left = 300
    Top = 380
    object QrOrfaosGraGruY: TIntegerField
      DisplayLabel = 'Grupo de Estoque'
      FieldName = 'GraGruY'
    end
    object QrOrfaosGraGruX: TIntegerField
      DisplayLabel = 'Reduzido'
      FieldName = 'GraGruX'
    end
    object QrOrfaosNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsOrfaos: TDataSource
    DataSet = QrOrfaos
    Left = 300
    Top = 428
  end
end
