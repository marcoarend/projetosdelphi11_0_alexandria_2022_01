unit VSGerArtItsBar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, AppListas, dmkCheckGroup, UnProjGroup_Consts,
  UnAppEnums, BlueDermConsts;

type
  TFmVSGerArtItsBar = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrAptos: TmySQLQuery;
    DsAptos: TDataSource;
    QrAptosCodigo: TIntegerField;
    QrAptosControle: TIntegerField;
    QrAptosMovimCod: TIntegerField;
    QrAptosMovimNiv: TIntegerField;
    QrAptosMovimTwn: TIntegerField;
    QrAptosEmpresa: TIntegerField;
    QrAptosTerceiro: TIntegerField;
    QrAptosCliVenda: TIntegerField;
    QrAptosMovimID: TIntegerField;
    QrAptosLnkNivXtr1: TIntegerField;
    QrAptosLnkNivXtr2: TIntegerField;
    QrAptosDataHora: TDateTimeField;
    QrAptosPallet: TIntegerField;
    QrAptosGraGruX: TIntegerField;
    QrAptosPecas: TFloatField;
    QrAptosPesoKg: TFloatField;
    QrAptosAreaM2: TFloatField;
    QrAptosAreaP2: TFloatField;
    QrAptosSrcMovID: TIntegerField;
    QrAptosSrcNivel1: TIntegerField;
    QrAptosSrcNivel2: TIntegerField;
    QrAptosSdoVrtPeca: TFloatField;
    QrAptosSdoVrtArM2: TFloatField;
    QrAptosObserv: TWideStringField;
    QrAptosLk: TIntegerField;
    QrAptosDataCad: TDateField;
    QrAptosDataAlt: TDateField;
    QrAptosUserCad: TIntegerField;
    QrAptosUserAlt: TIntegerField;
    QrAptosAlterWeb: TSmallintField;
    QrAptosAtivo: TSmallintField;
    QrAptosFicha: TIntegerField;
    QrAptosMisturou: TSmallintField;
    QrAptosCustoMOKg: TFloatField;
    QrAptosCustoMOTot: TFloatField;
    QrAptosNO_PRD_TAM_COR: TWideStringField;
    QrAptosSdoVrtPeso: TFloatField;
    GBAptos: TGroupBox;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    LaTerceiro: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    LaFicha: TLabel;
    EdFicha: TdmkEdit;
    DBGAptos: TDBGrid;
    BtReabre: TBitBtn;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    EdSrcNivel2: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    QrAptosSerieFch: TIntegerField;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    QrAptosNO_SerieFch: TWideStringField;
    Label2: TLabel;
    EdSrcGGX: TdmkEdit;
    QrAptosMarca: TWideStringField;
    QrNiv1: TmySQLQuery;
    QrNiv1FatorInt: TFloatField;
    GroupBox2: TGroupBox;
    Panel3: TPanel;
    Label6: TLabel;
    EdControleBxa: TdmkEdit;
    LaPecasBxa: TLabel;
    EdPecasBxa: TdmkEdit;
    LaPesoKgBxa: TLabel;
    EdPesoKgBxa: TdmkEdit;
    SbPesoKgBxa: TSpeedButton;
    LaQtdGerArM2Bxa: TLabel;
    EdAreaM2Bxa: TdmkEditCalc;
    LaQtdGerArP2Bxa: TLabel;
    EdAreaP2Bxa: TdmkEditCalc;
    Label9: TLabel;
    EdObservBxa: TdmkEdit;
    CGTpCalcAutoBxa: TdmkCheckGroup;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    LaPesoKgSrc: TLabel;
    LaQtdGerArM2Src: TLabel;
    LaQtdGerArP2Src: TLabel;
    Label12: TLabel;
    EdControleSrc: TdmkEdit;
    EdPecasSrc: TdmkEdit;
    EdPesoKgSrc: TdmkEdit;
    EdQtdGerArM2Src: TdmkEditCalc;
    EdQtdGerArP2Src: TdmkEditCalc;
    EdObservSrc: TdmkEdit;
    Label13: TLabel;
    EdMovimTwn: TdmkEdit;
    QrNiv1MediaMinM2: TFloatField;
    QrNiv1MediaMaxM2: TFloatField;
    Label8: TLabel;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    CkSemArea: TCheckBox;
    QrAptosValorMP: TFloatField;
    QrAptosCustoPQ: TFloatField;
    QrAptosNFeSer: TSmallintField;
    QrAptosNFeNum: TIntegerField;
    QrAptosVSMulNFeCab: TIntegerField;
    QrVSNatArt: TmySQLQuery;
    Panel7: TPanel;
    Label7: TLabel;
    Label10: TLabel;
    AGBVSRibCad: TBitBtn;
    QrAptosValorT: TFloatField;
    EdMarca: TdmkEdit;
    Label15: TLabel;
    Label16: TLabel;
    EdCustoM2: TdmkEdit;
    EdCustoPc: TdmkEdit;
    Label18: TLabel;
    QrAptosFornecMO: TIntegerField;
    QrAptosStqCenLoc: TIntegerField;
    QrAptosVSMulFrnCab: TIntegerField;
    QrAptosClientMO: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure DBGAptosDblClick(Sender: TObject);
    procedure EdPecasBxaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPecasBxaChange(Sender: TObject);
    procedure EdPesoKgBxaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSerieFchChange(Sender: TObject);
    procedure SbPesoKgBxaClick(Sender: TObject);
    procedure EdPecasSrcKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AGBVSRibCadClick(Sender: TObject);
    procedure EdPesoKgSrcChange(Sender: TObject);
    procedure EdPecasSrcChange(Sender: TObject);
    procedure EdQtdGerArM2SrcChange(Sender: TObject);
  private
    { Private declarations }
    FFatorIntSrc, FFatorIntDst: Integer;
    FMediaMinM2, FMediaMaxM2: Double;
    //
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure FechaPesquisa();
    procedure ReopenVSGerArtSrc(Controle: Integer);
    function  ValorTParcial(): Double;
    function  ValorMPParcial(): Double;
    function  ValorMOTot(): Double;
    procedure LiberaEdicao(Libera: Boolean);
    procedure DefineCustoM2EPeca();
    procedure InsereVSExcIts(Codigo, MovimCod, Empresa: Integer; DataHora:
              String; Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FOrigMovimNiv: TEstqMovimNiv;
    FEmpresa, FClientMO, FFornecMO, FOrigMovimCod, FOrigCodigo, FNewGraGruX, FTipoArea: Integer;
    FCustoMOKg, FCredPereImposto: Double;
    FDataHora: TDateTime;
    //
    procedure ReopenItensAptos();
    procedure DefineTipoArea();
  end;

  var
  FmVSGerArtItsBar: TFmVSGerArtItsBar;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSGerArtCab, UnVS_CRC_PF, GetValor, UnVS_PF, ModuleGeral;

{$R *.DFM}

procedure TFmVSGerArtItsBar.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmVSGerArtItsBar.AGBVSRibCadClick(Sender: TObject);
begin
  VS_PF.MostraFormVSRibCad(0, False);
end;

procedure TFmVSGerArtItsBar.BtOKClick(Sender: TObject);
const
  QtdGerPeca = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  //MovimTwn   = 0; // Nao usar aqui! usar na classificacao???
  Pallet     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  //Misturou   = 0;
  AptoUso    = 1;
  //FornecMO   = 0;
  //
  ExigeFornecedor = True;
  ExigeAreaouPeca = True;
  //
  EdPallet    = nil;
  EdValorT    = nil;
  EdAreaM2    = nil;
  //
  PedItsLib   = 0;
  PedItsFin   = 0;
  PedItsVda   = 0;
  //
  GSPSrcMovID = emidAjuste;
  GSPSrcNiv2  = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  DataHora, Observ, Marca: String;
  Codigo, Controle, MovimCod, Empresa, ClienteMO, FornecMO, GraGruX, Fornecedor,
  SrcNivel1, SrcNivel2, DstNivel1, DstNivel2, SerieFch, Ficha, SrcGGX, DstGGX,
  MovimTwn, StqCenLoc, ReqMovEstq: Integer;
  AreaM2, AreaP2,
  Pecas, PesoKg, ValorMP, ValorT, QtdGerPeso, QtdGerArM2, QtdGerArP2,
  FatorMP, FatorAR, NotaMPAG, FatNotaVNC, FatNotaVRC, CustoMOTot: Double;
  DstMovID, SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  TpCalcAuto, ClientMO: Integer;
  Qry: TmySQLQuery;
  EdPecas: TdmkEdit;
  //
  sData: String;
  VSExCCab, VSExCIts, VSExCMovCod: Integer;
  Agora: TDateTime;
  DtEntrada: String;
  ExcPecas, ExcPesoKg, ExcValorT, ExcValorMP, ExcAreaM2, ExcAreaP2, ExcFator: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  Controle       := EdControleBxa.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := FEmpresa;
  ClienteMO      := FClientMO;
  FornecMO       := FFornecMO;
  Fornecedor     := QrAptosTerceiro.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidIndsXX;
  MovimNiv       := eminBaixCurtiXX;
  GraGruX        := QrAptosGraGruX.Value;
  Pecas          := -EdPecasBxa.ValueVariant;
  PesoKg         := -EdPesoKgBxa.ValueVariant;
  AreaM2         := -EdAreaM2Bxa.ValueVariant;
  AreaP2         := -EdAreaP2Bxa.ValueVariant;
  QtdGerPeso     := -EdPesoKgSrc.ValueVariant;
  QtdGerArM2     := -EdQtdGerArM2Src.ValueVariant;
  QtdGerArP2     := -EdQtdGerArP2Src.ValueVariant;
  // ini 2023-04-15
  //ValorMP        := -ValorTParcial();
  //ValorT         := ValorMP;
  ValorMP        := -ValorMPParcial();
  ValorT         := -ValorTParcial();
  // fim 2023-04-15
  CustoMOTot     := 0;
  Observ         := EdObservBxa.Text;
  //
  SerieFch       := QrAptosSerieFch.Value;
  Ficha          := QrAptosFicha.Value;
  Marca          := QrAptosMarca.Value;
  //
  (*DstMovID       := TEstqMovimID(EdSrcMovID.ValueVariant);
  DstNivel1      := EdSrcNivel1.ValueVariant;
  DstNivel2      := EdSrcNivel2.ValueVariant;
  DstGGX         := EdSrcGGX.ValueVariant;*)
  //
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  SrcMovID       := TEstqMovimID(QrAptosMovimID.Value);
  SrcNivel1      := QrAptosCodigo.Value;
  SrcNivel2      := QrAptosControle.Value;
  SrcGGX         := QrAptosGraGruX.Value;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := 0;
  ClientMO       := FClientMO;
  FornecMO       := FFornecMO;
  //
  if (EdQtdGerArM2Src.Enabled) and (not CkSemArea.Checked) then
  begin
    if MyObjects.FIC(-QtdGerArM2 < 0.01, EdQtdGerArM2Src, 'Informe a  �rea!') then
      Exit;
    if not VS_CRC_PF.AreaEstaNaMedia(EdPecasSrc.ValueVariant, -QtdGerArM2, FMediaMinM2, FMediaMaxM2, True) then
      Exit;
  end;
  if (EdPesoKgSrc.Enabled) and (not CkSemArea.Checked) then
  begin
    if MyObjects.FIC(-QtdGerPeso < 0.01, EdPesoKgSrc, 'Informe o  peso!') then
      Exit;
  end;
  //
  if QrAptosPecas.Value > 0 then
    EdPecas := EdPecasBxa
  else
    EdPecas := nil;
  //
  if VS_CRC_PF.VSFic(GraGruX, Empresa, ClienteMO, Fornecedor, Pallet, Ficha, Pecas,
    AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, (*EdFicha*) nil, EdPecas,
    EdAreaM2, EdPesoKgBxa, EdValorT, ExigeFornecedor, CO_GraGruY_2048_VSRibCad,
    ExigeAreaouPeca, EdStqCenLoc)
  then
    Exit;
  //
  if -Pecas > QrAptosSdoVrtPeca.Value then
  begin
    if Geral.MB_Pergunta(
    'A quantidade de pe�as a ser baixada � superior ao estoque dispon�vel!' +
    sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
      Exit;
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
    // Gerar cabe�alho de Entrada de excedente caso n�o exista!
    Agora := DModG.ObtemAgora();
    sData := FormatDateTime('YYYY-MM-DD', Agora);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo, MovimCod ',
    'FROM vsexccab ',
    'WHERE DATE_FORMAT(DtEntrada, "%Y-%m-%d")= "' + sData + '" ', // 2023-05-23
    '']);

    DtEntrada := Geral.FDT(Agora, 109);
    VSExCCab := Dmod.QrAux.Fields[0].AsInteger;
    VSExCMovCod := Dmod.QrAux.Fields[1].AsInteger;
    if VSExCCab = 0 then
    begin
      VSExCCab := UMyMod.BPGS1I32('vsexccab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, VSExCCab);
      VSExCMovCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsexccab', False, [
      'Nome', 'MovimCod', 'Empresa',
      'DtEntrada', 'Pecas', 'PesoKg',
      'AreaM2', 'AreaP2', 'ValorT'], [
      'Codigo'], [
      ''(*Nome*), VSExCMovCod(*MovimCod*), Empresa,
      DtEntrada, 0.000, 0.000,
      0.00, 0.00, 0.0000], [
      VSExCCab], True) then
        VS_PF.InsereVSMovCab(VSExCMovCod, emidEntraExced, VSExCCab);
    end;
    //
    ExcPecas   := ABS(Pecas) - ABS(QrAptosSdoVrtPeca.Value);
    if ABS(QrAptosPecas.Value) > 0 then
    begin
      ExcFator := ExcPecas / ABS(QrAptosPecas.Value);
      ExcPesoKg := ExcFator * ABS(QrAptosPesoKg.Value);
      ExcValorMP := ExcFator * ABS(QrAptosValorMP.Value);
      ExcValorT := ExcFator * ABS(QrAptosValorT.Value);
    end else
    begin
      ExcPesoKg  := 0.000;
      ExcValorT  := 0.0000;
      ExcValorMP := 0.0000;
    end;
    ExcAreaM2  := 0.00;
    ExcAreaP2  := 0.00;
    //
    InsereVSExcIts(VSExCCab, VSExCMovCod, Empresa, DtEntrada, ExcPecas,
      ExcPesoKg, ExcAreaM2, ExcAreaP2, ExcValorMP, ExcValorT);
    //
    ExcFator   := ABS(Pecas) / ABS(QrAptosSdoVrtPeca.Value);
  end else
  begin
    ExcFator   := 1.000000000000000;
    ExcPecas   := 0.000;
    ExcPesoKg  := 0.000;
    ExcValorT  := 0.0000;
    ExcValorMP := 0.0000;
  end;
  //
  PesoKg         := -EdPesoKgBxa.ValueVariant * ExcFator;
  AreaM2         := -EdAreaM2Bxa.ValueVariant * ExcFator;
  AreaP2         := -EdAreaP2Bxa.ValueVariant * ExcFator;
  QtdGerPeso     := -EdPesoKgSrc.ValueVariant;
  QtdGerArM2     := -EdQtdGerArM2Src.ValueVariant;
  QtdGerArP2     := -EdQtdGerArP2Src.ValueVariant;
  ValorMP        := -ValorMPParcial() * ExcFator;
  ValorT         := -ValorTParcial() * ExcFator;
  FatorMP  := VS_CRC_PF.FatorNotaMP(GraGruX);
  FatorAR  := VS_CRC_PF.FatorNotaAR(FNewGraGruX);
  NotaMPAG := VS_CRC_PF.NotaCouroRibeiraApuca(-Pecas, -PesoKg, -QtdGerArM2, FatorMP, FatorAR);
  FatNotaVRC := FatorAR;
  FatNotaVNC := FatorMP;

  //
  TpCalcAuto := CGTpCalcAutoBxa.Value;
  // Baixa couro In Natura
  MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  // 2015-05-09 Calcular Nota MPAG
    DstMovID       := MovimID;
    DstNivel1      := EdSrcNivel1.ValueVariant;
    DstNivel2      := EdControleSrc.ValueVariant;
    DstNivel2      := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, DstNivel2);
    DstGGX         := EdSrcGGX.ValueVariant;
    // FIM 2015-05-09 Calcular Nota MPAG
  //
  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID,
  DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
  AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei023(*Gera��o de artigo de ribeira direto da barraca*)) then
  begin
    VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
    VS_CRC_PF.AtualizaSerieNFeVMI(Controle, QrAptosNFeSer.Value,
      QrAptosNFeNum.Value, QrAptosVSMulNFeCab.Value);
    // 2015-05-09 Calcular Nota MPAG
    // Geracao Couro Curtido
    //Controle       := EdControleSrc.ValueVariant;
    Controle       := DstNivel2;
    // FIM 2015-05-09 Calcular Nota MPAG
    //
    MovimNiv       := eminSorcCurtiXX;
    GraGruX        := EdSrcGGX.ValueVariant;
    Pecas          := EdPecasSrc.ValueVariant;
    PesoKg         := EdPesoKgBxa.ValueVariant * ExcFator;
    AreaM2         := 0;
    AreaP2         := 0;
    QtdGerPeso     := EdPesoKgSrc.ValueVariant;
    QtdGerArM2     := EdQtdGerArM2Src.ValueVariant;
    QtdGerArP2     := EdQtdGerArP2Src.ValueVariant;
    CustoMOTot     := ValorMOTot() * ExcFator;
    // ini 2023-04-15
    //ValorMP        := ValorMPParcial();
    //ValorT         := ValorMP + CustoMOTot;
    // ini 2023-05-23
    ValorMP        := ABS(ValorMPParcial()) * ExcFator;
    ValorT         := (ABS(ValorTParcial()) * ExcFator) + ABS(CustoMOTot);
    // fim 2023-05-23
    // fim 2023-04-15
    Observ         := EdObservSrc.Text;
    //
    DstMovID       := TEstqMovimID(EdSrcMovID.ValueVariant);
    DstNivel1      := EdSrcNivel1.ValueVariant;
    DstNivel2      := EdSrcNivel2.ValueVariant;
    DstGGX         := EdSrcGGX.ValueVariant;
    //
    SrcMovID       := TEstqMovimID(0);
    SrcNivel1      := 0;
    SrcNivel2      := 0;
    SrcGGX         := 0;
    //
    TpCalcAuto     := 0;
    //
    StqCenLoc      := EdStqCenLoc.ValueVariant;
    ReqMovEstq     := EdReqMovEstq.ValueVariant;
    //
    //
    // 2015-05-09 Calcular Nota MPAG
    //Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
    // FIM 2015-05-09 Calcular Nota MPAG
    if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
    MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
    DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
    CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID,
    DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
    AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    CO_0_VmiPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iuvpei024(*Baixa de mat�ria prima na gera��o de artigo de ribeira direto da barraca*)) then
    begin
      VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
      VS_CRC_PF.AtualizaSerieNFeVMI(Controle, QrAptosNFeSer.Value,
        QrAptosNFeNum.Value, QrAptosVSMulNFeCab.Value);
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Atualizar dados de baixa do In Natura selecionado
      VS_CRC_PF.AtualizaSaldoIMEI(QrAptosControle.Value, True);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
      VS_CRC_PF.DistribuiCustoIndsVS(FOrigMovimNiv, FOrigMovimCod, FOrigCodigo,
        EdSrcNivel2.ValueVariant);
      //
      FmVSGerArtCab.AtualizaNFeItens();
      //
      FmVSGerArtCab.LocCod(Codigo, Codigo);
      ReopenVSGerArtSrc(Controle);
      //
      VS_CRC_PF.AtualizaCustosDescendentesGerArtFicha(Empresa, SerieFch, Ficha);
      if CkContinuar.Checked then
      begin
        EdMovimTwn.ValueVariant       := 0;
        CGTpCalcAutoBxa.Value         := 2;  // So PesoKg a principio!
        //
        ImgTipo.SQLType               := stIns;
        LiberaEdicao(False);
        ReopenItensAptos();
        //
        EdGraGruX.ValueVariant        := 0;
        CBGraGruX.KeyValue            := Null;
        //
        EdControleBxa.ValueVariant    := 0;
        EdPecasBxa.ValueVariant       := 0;
        EdPesoKgBxa.ValueVariant      := 0;
        EdAreaM2Bxa.ValueVariant      := 0;
        EdAreaP2Bxa.ValueVariant      := 0;
        EdObservBxa.Text              := '';
        //
        EdControleSrc.ValueVariant    := 0;
        EdPecasSrc.ValueVariant       := 0;
        EdPesoKgSrc.ValueVariant      := 0;
        EdQtdGerArM2Src.ValueVariant  := 0;
        EdQtdGerArP2Src.ValueVariant  := 0;
        EdObservSrc.Text              := '';
        //
        //erro ! EdGraGruX.SetFocus;
      end else
        Close;
    end;
  end;
end;

procedure TFmVSGerArtItsBar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerArtItsBar.DBGAptosDblClick(Sender: TObject);
begin
  LiberaEdicao(True);
end;

procedure TFmVSGerArtItsBar.DefineCustoM2EPeca();
var
  ValorMP, ValorMO, ValorTot, GerM2, ValorM2, Pecas, ValorPC: Double;
begin
  // ini 2023-04-15
  //ValorMP        := -ValorTParcial();
  ValorMP        := ValorMPParcial();
  // fim 2023-04-15
  ValorMO  := ValorMOTot();
  ValorTot := ValorMP + ValorMO;
  // $/m2
  GerM2    := EdQtdGerArM2Src.ValueVariant;
  if GerM2 > 0 then
    ValorM2 := ValorTot / GerM2
  else
    ValorM2 := 0.00;
  EdCustoM2.ValueVariant := ValorM2;
  // $/couro
  Pecas := EdPecasSrc.ValueVariant;
  if Pecas > 0 then
    ValorPc := ValorTot / Pecas
  else
    ValorPc := 0.00;
  EdCustoPc.ValueVariant := ValorPc;
end;

procedure TFmVSGerArtItsBar.DefineTipoArea();
begin
  LaQtdGerArM2Src.Enabled := FTipoArea = 0;
  EdQtdGerArM2Src.Enabled := FTipoArea = 0;
  //
  LaQtdGerArP2Src.Enabled := FTipoArea = 1;
  EdQtdGerArP2Src.Enabled := FTipoArea = 1;
end;

procedure TFmVSGerArtItsBar.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsBar.EdGraGruXChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsBar.EdPecasBxaChange(Sender: TObject);
  procedure CalculaPesoParcial();
  var
    Pecas, PesoKg, AreaM2: Double;
  begin
    Pecas  := EdPecasBxa.ValueVariant;
    PesoKg := 0;
    if QrAptosSdoVrtPeca.Value > 0 then
      PesoKg := Pecas / QrAptosSdoVrtPeca.Value * QrAptosSdoVrtPeso.Value;
    EdPesoKgBxa.ValueVariant := PesoKg;
    //
    AreaM2 := 0;
    if QrAptosSdoVrtArM2.Value > 0 then
      AreaM2 := Pecas / QrAptosSdoVrtPeca.Value * QrAptosSdoVrtArM2.Value;
    EdAreaM2Bxa.ValueVariant  := AreaM2;
  end;
var
  Pecas: Double;
begin
  // ver pelas pecas se eh o resto e pegar todo resto do peso
  Pecas := EdPecasBxa.ValueVariant;
  if (QrAptos.State <> dsInactive) and (QrAptos.RecordCount > 0)
  and (Pecas >= QrAptosSdoVrtPeca.Value) then
  begin
    EdPesoKgBxa.ValueVariant  := QrAptosSdoVrtPeso.Value;
    LaPesoKgBxa.Enabled  := False;
    EdPesoKgBxa.Enabled  := False;
    EdAreaM2Bxa.ValueVariant  := QrAptosSdoVrtArM2.Value;
  end else
  begin
    //LaPesoKg.Enabled  := GBGerar.Visible;
    //EdPesoKg.Enabled  := GBGerar.Visible;
    CalculaPesoParcial();
  end;
  if (FFatorIntSrc <> 0) and (FFatorIntDst <> 0) then
    EdPecasSrc.ValueVariant := Pecas * FFatorIntSrc / FFatorIntDst
  else
    EdPecasSrc.ValueVariant := Pecas;
end;

procedure TFmVSGerArtItsBar.EdPecasBxaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Txt: String;
  Pecas: Double;
begin
  if Geral.IntInConjunto(1, CGTpCalcAutoBxa.Value) then
    CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value - 1;
  //
  if Key = VK_F4 then
  begin
    EdPecasBxa.ValueVariant := QrAptosSdoVrtPeca.Value;
    EdPesoKgBxa.ValueVariant := QrAptosSdoVrtPeso.Value;
    if not Geral.IntInConjunto(1, CGTpCalcAutoBxa.Value) then
      CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value + 1;
  end else
  if Key = VK_F5 then
  begin
    Txt := '0,00';
    if InputQuery('Pe�as', 'Informe as pe�as a gerar:', Txt) then
    begin
      Pecas := Geral.DMV(Txt);
      EdPecasBxa.ValueVariant := Pecas / 2;
      EdPecasSrc.ValueVariant := Pecas;
    end;
  end;
end;

procedure TFmVSGerArtItsBar.EdPecasSrcChange(Sender: TObject);
begin
  DefineCustoM2EPeca();
end;

procedure TFmVSGerArtItsBar.EdPecasSrcKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Fator: Integer;
begin
  if Key = VK_F5 then
  begin
    Fator := MyObjects.SelRadioGroup('Fator de divis�o',
    'Escolha a forma de divis�o', [
    'Sem reparti��o ao meio',
    'Foi repartido de inteiros para meios'],
    -1);
    case Fator of
      0: Fator := 1;
      1: Fator := 2;
      else Fator := 0;
    end;
    if Fator <> 0 then
      EdPecasSrc.ValueVariant := EdPecasBxa.ValueVariant * Fator;
  end;
end;

procedure TFmVSGerArtItsBar.EdPesoKgBxaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  Pecas, PesoKg: Double;
*)
begin
(*
  if Key = VK_F4 then
  begin
    Pecas  := EdPecas.ValueVariant;
    PesoKg := 0;
    if QrAptosSdoVrtPeca.Value > 0 then
      PesoKg := Pecas / QrAptosSdoVrtPeca.Value * QrAptosSdoVrtPeso.Value;
    //
    EdPesoKg.ValueVariant := PesoKg;
  end;
*)
end;

procedure TFmVSGerArtItsBar.EdPesoKgSrcChange(Sender: TObject);
begin
  DefineCustoM2EPeca();
end;

procedure TFmVSGerArtItsBar.EdQtdGerArM2SrcChange(Sender: TObject);
begin
  DefineCustoM2EPeca();
end;

procedure TFmVSGerArtItsBar.EdSerieFchChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsBar.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsBar.FechaPesquisa();
begin
  QrAptos.Close;
end;

procedure TFmVSGerArtItsBar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSGerArtItsBar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VS_CRC_PF.AbreVSSerFch(QrVSSerFch);
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1024_VSNatCad));

  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  CGTpCalcAutoBxa.Value := 2; // so peso a principio
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSGerArtItsBar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerArtItsBar.InsereVSExcIts(Codigo, MovimCod, Empresa: Integer;
  DataHora: String; Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double);
const
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  CustoMOTot = 0;
  //ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  //
  TpCalcAuto = -1;
  //
  EdFicha  = nil;
  EdPallet = nil;
  ExigeFornecedor = False;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  ItemNFe     = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  Observ, Marca: String;
  Pallet, Controle, GraGruX, Terceiro, SerieFch,
  Ficha, SrcNivel1, SrcNivel2, (*Misturou,*) SrcGGX,
  VSMulFrnCab, ClientMO, FornecMO, StqCenLoc: Integer;
  SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  ExigeAreaouPeca: Boolean;
begin
  SrcMovID       := TEstqmovimID(QrAptosMovimID.Value);
  SrcNivel1      := QrAptosCodigo.Value;
  SrcNivel2      := QrAptosControle.Value;
  SrcGGX         := QrAptosGraGruX.Value;
  //
  //Codigo         := ;
  Controle       := 0;
  //MovimCod       := ;
  //Empresa        := ;
  ClientMO       := QrAptosClientMO.Value;
  FornecMO       := QrAptosFornecMO.Value;
  StqCenLoc      := QrAptosStqCenLoc.Value;
  Terceiro       := QrAptosTerceiro.Value;
  VSMulFrnCab    := QrAptosVSMulFrnCab.Value;
  //DataHora       := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimID        := emidEntraExced;
  MovimNiv       := eminSemNiv;
  Pallet         := QrAptosPallet.Value;
  GraGruX        := QrAptosGraGruX.Value;
  (*
  Pecas          := -QrAptosSdoVrtPeca.Value;
  PesoKg         := -QrAptosSdoVrtPeso.Value;
  AreaM2         := -QrAptosSdoVrtArM2.Value;
  AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  if (AreaM2 <> 0) and (QrAptosAreaM2.Value <> 0) then
    ValorT := AreaM2 / QrAptosAreaM2.Value * QrAptosValorT.Value
  else
  if (PesoKg <> 0) and (QrAptosPesoKg.Value <> 0) then
    ValorT := PesoKg / QrAptosPesoKg.Value * QrAptosValorT.Value
  else
    ValorT         := 0.00;
  *)
  Observ         := '';
  //
  SerieFch       := QrAptosSerieFch.Value;
  Ficha          := QrAptosFicha.Value;
  Marca          := QrAptosMarca.Value;
  //Misturou       := QrAptosMisturou.Value;
  //
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab,
  ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei115(*Entrada excedente de couro*)) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vsexbcab', MovimCod);
    VS_PF.AtualizaSaldoIMEI(SrcNivel2, False);
  end;
end;

procedure TFmVSGerArtItsBar.LiberaEdicao(Libera: Boolean);
var
  Status, EhArea: Boolean;
begin
  if Libera then
    Status := QrAptos.RecordCount > 0
  else
    Status := False;
  //
  if Status then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(EdSrcGGX.ValueVariant),
    '']);
    FFatorIntDst := Trunc(QrNiv1FatorInt.Value * 1000);
    FMediaMinM2  := QrNiv1MediaMinM2.Value;
    FMediaMaxM2  := QrNiv1MediaMaxM2.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(QrAptosGraGruX.Value),
    '']);
    FFatorIntSrc := Trunc(QrNiv1FatorInt.Value * 1000);
    //
    if VS_CRC_PF.FatoresIncompativeis(FFatorIntSrc, FFatorIntDst, nil) then
      Exit;
  end;
  //
  GBAptos.Enabled := not Status;

  GBGerar.Visible := Status;
  //
  LaFicha.Enabled := not Status;
  EdFicha.Enabled := not Status;
  LaGraGruX.Enabled := not Status;
  EdGraGruX.Enabled := not Status;
  CBGraGruX.Enabled := not Status;
  LaTerceiro.Enabled := not Status;
  EdTerceiro.Enabled := not Status;
  CBTerceiro.Enabled := not Status;
  BtReabre.Enabled := not Status;
  //
  EhArea := (FFatorIntSrc + FFatorIntDst > 0) and (FmVSGerArtCab.QrVSGerArtNewGrandeza.Value <> 2(*Kg*));
  //
  EdQtdGerArM2Src.Enabled := EhArea;
  LaQtdGerArM2Src.Enabled := EhArea;
  EdPesoKgSrc.Enabled := not EhArea;
  LaPesoKgSrc.Enabled := not EhArea;
  //
  if Libera and (EdPecasBxa.Enabled) and (EdPecasBxa.Visible) then
    EdPecasBxa.SetFocus;
  //
(*
  if GBGerar.Visible then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(EdSrcGGX.ValueVariant),
    '']);
    FFatorIntDst := Trunc(QrNiv1FatorInt.Value * 1000);
    FMediaMinM2  := QrNiv1MediaMinM2.Value;
    FMediaMaxM2  := QrNiv1MediaMaxM2.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(QrAptosGraGruX.Value),
    '']);
    FFatorIntSrc := Trunc(QrNiv1FatorInt.Value * 1000);
    //
    if FFatorIntSrc <> FFatorIntDst then
      Geral.MB_Aviso('CUIDADO!!!' + sLineBreak +
      'Ao informar a quantidade leve em considera��o a possibilidade de transform��o de inteiros para meios!!!');
  end;
*)
end;

procedure TFmVSGerArtItsBar.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmVSGerArtItsBar.ReopenItensAptos();
var
  SQL_GraGruX, SQL_Terceiro, SQL_SerieFch, SQL_Ficha, SQL_Marca: String;
begin
  SQL_GraGruX  := '';
  SQL_Terceiro := '';
  SQL_Ficha    := '';
  SQL_Marca    := '';
  //
  if EdGraGruX.ValueVariant <> 0 then
    SQL_GraGruX  := 'AND vmi.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant);
  if EdTerceiro.ValueVariant <> 0 then
    SQL_Terceiro  := 'AND vmi.Terceiro=' + Geral.FF0(EdTerceiro.ValueVariant);
  if EdSerieFch.ValueVariant <> 0 then
    SQL_SerieFch  := 'AND vmi.SerieFch=' + Geral.FF0(EdSerieFch.ValueVariant);
  if EdFicha.ValueVariant <> 0 then
    SQL_Ficha  := 'AND vmi.Ficha=' + Geral.FF0(EdFicha.ValueVariant);
  if EdMarca.Text <> '' then
    SQL_Marca  := 'AND vmi.Marca="' + EdMarca.Text + '"';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAptos, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vsmovcab vmc ON vmc.Codigo=vmi.Codigo ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vsnatart   vna ON vna.VSNatCad=vmi.GraGruX',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  // 2015-07-10
  (*
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidCompra)),
  'AND vna.VSRibCad=' + Geral.FF0(FNewGraGruX),
  *)
  'WHERE ( ',
  '  (vmi.MovimID=' + Geral.FF0(Integer(emidCompra)),
  '  AND ',
  '  vna.VSRibCad=' + Geral.FF0(FNewGraGruX),
  '  ) OR ',
  '  (vmi.MovimID=' + Geral.FF0(Integer(emidInventario)),
  '  AND ',
  '  ggx.GraGruY IN (' + CO_GraGruY_GER_VS + ') ',
  '  ) ',
  ') ',
  //'AND SdoVrtPeca>0 ',
  'AND ( ',
  '  vmi.SdoVrtPeca>0  ',
  '  OR ( ',
  '  vmi.Pecas=0 AND vmi.SdoVrtPeso>0 AND ggx.GraGruY=1024 ',
  '  ) ',
  ') ',
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND ClientMO=' + Geral.FF0(FClientMO),
  // FIM 2015-07-10
  SQL_GraGruX,
  SQL_Terceiro,
  SQL_SerieFch,
  SQL_Ficha,
  SQL_Marca,
  'ORDER BY vmi.Controle ',
  '']);
  //Geral.MB_Teste(QrAptos.SQL.Text);
end;

procedure TFmVSGerArtItsBar.ReopenVSGerArtSrc(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSGerArtItsBar.SbPesoKgBxaClick(Sender: TObject);
var
  ValVar: Variant;
  PesoKg: Double;
begin
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, 0, 3, 0,
  '0,000', EdPesoKgBxa.Text, True,
  'Peso kg', 'Informe o peso (kg): ', 0, ValVar) then
  begin
    EdPesoKgBxa.ValueVariant := Geral.DMV(ValVar);
    //FTpCalcAuto := UnAppListas.DefineTpCalcAutoCouro(False, True, False, False);
    if Geral.IntInConjunto(2, CGTpCalcAutoBxa.Value) then
      CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value - 2;
  end else;
end;

function TFmVSGerArtItsBar.ValorMOTot(): Double;
var
  Retorno, CustoMOKg: Double;
begin
  Retorno := (FCustoMOKg * FCredPereImposto) / 100;
  CustoMOKg := FCustoMOKg - Retorno;
  Result := EdPesoKgBxa.ValueVariant * CustoMOKg;
end;

function TFmVSGerArtItsBar.ValorMPParcial(): Double;
var
  ValorMP: Double;
begin
  // Da barraca n�o pode buscar custoPQ, pois busca do couro de entrada!
  ValorMP := QrAptosValorT.Value; // + QrAptosCustoPQ.Value;
  //
  if QrAptosPesoKg.Value > 0 then
    Result := EdPesoKgBxa.ValueVariant / QrAptosPesoKg.Value * ValorMP
  else
  if QrAptosAreaM2.Value > 0 then
    Result := EdAreaM2Bxa.ValueVariant / QrAptosAreaM2.Value * ValorMP
  else
  if QrAptosPecas.Value > 0 then
    Result := EdPecasBxa.ValueVariant / QrAptosPecas.Value * ValorMP
  else
  Result := 0;
end;

function TFmVSGerArtItsBar.ValorTParcial(): Double;
var
  ValorCurtido: Double;
begin
  // Da barraca n�o pode buscar custoPQ, pois busca do couro de entrada!
  //ValorCurtido := QrAptosValorMP.Value + QrAptosCustoPQ.Value;
  ValorCurtido := QrAptosValorT.Value;
  //
  if QrAptosPesoKg.Value > 0 then
    Result := EdPesoKgBxa.ValueVariant / QrAptosPesoKg.Value * ValorCurtido
  else
  if QrAptosAreaM2.Value > 0 then
    Result := EdAreaM2Bxa.ValueVariant / QrAptosAreaM2.Value * ValorCurtido
  else
  if QrAptosPecas.Value > 0 then
    Result := EdPecasBxa.ValueVariant / QrAptosPecas.Value * ValorCurtido
  else
  Result := 0;
end;

end.
