unit VSInfPal;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, Vcl.ComCtrls,
  dmkEditDateTimePicker, Vcl.Menus, UnProjGroup_Consts;

type
  TFmVSInfPal = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVSInfPal: TmySQLQuery;
    QrVSInfPalCodigo: TIntegerField;
    DsVSInfPal: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdResponsa: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    TPDataHora: TdmkEditDateTimePicker;
    Label52: TLabel;
    EdDataHora: TdmkEdit;
    Label53: TLabel;
    EdNumIni: TdmkEdit;
    EdNumFim: TdmkEdit;
    Label55: TLabel;
    QrVSInfPalEmpresa: TIntegerField;
    QrVSInfPalDataHora: TDateTimeField;
    QrVSInfPalNumIni: TIntegerField;
    QrVSInfPalNumFim: TIntegerField;
    QrVSInfPalLk: TIntegerField;
    QrVSInfPalDataCad: TDateField;
    QrVSInfPalDataAlt: TDateField;
    QrVSInfPalUserCad: TIntegerField;
    QrVSInfPalUserAlt: TIntegerField;
    QrVSInfPalAlterWeb: TSmallintField;
    QrVSInfPalAtivo: TSmallintField;
    QrVSInfPalResponsa: TWideStringField;
    QrVSInfPalNO_EMP: TWideStringField;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrVSInfPalQuantidade: TFloatField;
    Label10: TLabel;
    DBEdit7: TDBEdit;
    PMImprime: TPopupMenu;
    N1vianormal1: TMenuItem;
    N1viareduzido1: TMenuItem;
    N2viasnormal1: TMenuItem;
    N2viasreduzido1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSInfPalAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSInfPalBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure N1vianormal1Click(Sender: TObject);
    procedure N1viareduzido1Click(Sender: TObject);
    procedure N2viasnormal1Click(Sender: TObject);
    procedure N2viasreduzido1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ImprimeRMEs(Vias: Integer; Reduzido: Boolean);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmVSInfPal: TFmVSInfPal;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, VSImpRequis;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSInfPal.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSInfPal.N1vianormal1Click(Sender: TObject);
begin
  ImprimeRMEs(1, False);
end;

procedure TFmVSInfPal.N1viareduzido1Click(Sender: TObject);
begin
  ImprimeRMEs(1, True);
end;

procedure TFmVSInfPal.N2viasnormal1Click(Sender: TObject);
begin
  ImprimeRMEs(2, False);
end;

procedure TFmVSInfPal.N2viasreduzido1Click(Sender: TObject);
begin
  ImprimeRMEs(2, True);
end;

procedure TFmVSInfPal.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSInfPalCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSInfPal.DefParams;
begin
  VAR_GOTOTABELA := 'vsinfpal';
  VAR_GOTOMYSQLTABLE := QrVSInfPal;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := 'Responsa';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vrm.*, vrm.NumFim - vrm.NumIni + 1.000 Quantidade, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP');
  VAR_SQLx.Add('FROM vsinfpal vrm');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=vrm.Empresa ');
  VAR_SQLx.Add('WHERE vrm.Codigo > 0');
  //
  VAR_SQL1.Add('AND vrm.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND vrm.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vrm.Responsa LIKE :P0');
  //
end;

procedure TFmVSInfPal.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSInfPal.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSInfPal.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmVSInfPal.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSInfPal.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSInfPal.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSInfPal.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSInfPal.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSInfPal.BtAlteraClick(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSInfPal, [PnDados],
  [PnEdita], EdResponsa, ImgTipo, 'vsinfpal');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSInfPalEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSInfPal.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSInfPalCodigo.Value;
  Close;
end;

procedure TFmVSInfPal.BtConfirmaClick(Sender: TObject);
var
  DataHora, Responsa: String;
  Codigo, Empresa, NumIni, NumFim: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DataHora       := Geral.FDT_TP_Ed(TPDataHora.Date, EdDataHora.Text);
  NumIni         := EdNumIni.ValueVariant;
  NumFim         := EdNumFim.ValueVariant;
  Responsa       := EdResponsa.Text;
  //
  if MyObjects.FIC(Length(Responsa) = 0, EdResponsa, 'Informe o responsável!') then
    Exit;
  Codigo := UMyMod.BPGS1I32('vsinfpal', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsinfpal', False, [
  'Empresa', CO_DATA_HORA_GRL, 'NumIni',
  'NumFim', 'Responsa'], [
  'Codigo'], [
  Empresa, DataHora, NumIni,
  NumFim, Responsa], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSInfPal.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsinfpal', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVSInfPal.BtIncluiClick(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSInfPal, [PnDados],
  [PnEdita], EdResponsa, ImgTipo, 'vsinfpal');
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDataHora.Date := Agora;
  EdDataHora.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDataHora.SetFocus;
end;

procedure TFmVSInfPal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmVSInfPal.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSInfPalCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSInfPal.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSInfPal.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSInfPal.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSInfPalCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSInfPal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSInfPal.QrVSInfPalAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSInfPal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSInfPal.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSInfPalCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsinfpal', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSInfPal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSInfPal.ImprimeRMEs(Vias: Integer; Reduzido: Boolean);
begin
  Application.CreateForm(TFmVSImpRequis, FmVSImpRequis);
  FmVSImpRequis.FEmpresa_Cod   := QrVSInfPalEmpresa.Value;
  FmVSImpRequis.FEmpresa_Txt   := QrVSInfPalNO_EMP.Value;
  FmVSImpRequis.FResponsa      := QrVSInfPalResponsa.Value;
  //
  FmVSImpRequis.FQuantidade    :=  Trunc(QrVSInfPalQuantidade.Value);
  FmVSImpRequis.FNumInicial    :=  QrVSInfPalNumIni.Value;
  FmVSImpRequis.FNumVias       :=  Vias;
  FmVSImpRequis.FReduzido      :=  Reduzido;
  //
  FmVSImpRequis.ImprimeIPM();
  //
  FmVSImpRequis.Destroy;
end;

procedure TFmVSInfPal.QrVSInfPalBeforeOpen(DataSet: TDataSet);
begin
  QrVSInfPalCodigo.DisplayFormat := FFormatFloat;
end;

end.

