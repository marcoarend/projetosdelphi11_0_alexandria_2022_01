object FmVSNatPDA: TFmVSNatPDA
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-171 :: Configura'#231#227'o de Mat'#233'rias-primas PDA'
  ClientHeight = 589
  ClientWidth = 919
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 919
    Height = 493
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 919
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'GraGruX'
        DataSource = DsVSNatPDA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 589
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVSNatPDA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 429
      Width = 919
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 396
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtArtigo: TBitBtn
          Tag = 421
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Artigo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtArtigoClick
        end
        object BtMP: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sub Produto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtMPClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object DBGrid1: TdmkDBGridZTO
      Left = 0
      Top = 121
      Width = 919
      Height = 252
      Align = alTop
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID Link'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Nome do produto'
          Width = 737
          Visible = True
        end>
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 61
      Width = 919
      Height = 60
      Align = alTop
      TabOrder = 3
      object Label8: TLabel
        Left = 16
        Top = 16
        Width = 78
        Height = 13
        Caption = 'Tipo de material:'
      end
      object Label10: TLabel
        Left = 340
        Top = 16
        Width = 82
        Height = 13
        Caption = 'Parte do material:'
      end
      object Label3: TLabel
        Left = 672
        Top = 16
        Width = 28
        Height = 13
        Caption = 'PPP*:'
      end
      object Label19: TLabel
        Left = 720
        Top = 16
        Width = 30
        Height = 13
        Caption = 'PMP*:'
      end
      object Label20: TLabel
        Left = 776
        Top = 16
        Width = 28
        Height = 13
        Caption = 'PKP*:'
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        DataField = 'CouNiv2'
        DataSource = DsGraGruXCou
        TabOrder = 0
      end
      object DBEdit5: TDBEdit
        Left = 72
        Top = 32
        Width = 264
        Height = 21
        DataField = 'NO_CouNiv2'
        DataSource = DsGraGruXCou
        TabOrder = 1
      end
      object DBEdit6: TDBEdit
        Left = 340
        Top = 32
        Width = 56
        Height = 21
        DataField = 'CouNiv1'
        DataSource = DsGraGruXCou
        TabOrder = 2
      end
      object DBEdit7: TDBEdit
        Left = 396
        Top = 32
        Width = 264
        Height = 21
        DataField = 'NO_CouNiv1'
        DataSource = DsGraGruXCou
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 672
        Top = 32
        Width = 44
        Height = 21
        DataField = 'PrevPcPal'
        DataSource = DsGraGruXCou
        TabOrder = 4
      end
      object DBEdit9: TDBEdit
        Left = 720
        Top = 32
        Width = 52
        Height = 21
        DataField = 'PrevAMPal'
        DataSource = DsGraGruXCou
        TabOrder = 5
      end
      object DBEdit10: TDBEdit
        Left = 776
        Top = 32
        Width = 52
        Height = 21
        DataField = 'PrevKgPal'
        DataSource = DsGraGruXCou
        TabOrder = 6
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 919
    Height = 493
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 430
      Width = 919
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 779
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 919
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 48
        Height = 13
        Caption = 'Reduzido:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label15: TLabel
        Left = 352
        Top = 16
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 84
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 476
        Top = 16
        Width = 19
        Height = 13
        Caption = 'Cor:'
        Color = clBtnFace
        ParentColor = False
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 32
        Width = 68
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsVSNatPDA
        Enabled = False
        TabOrder = 0
      end
      object EdNome: TdmkEdit
        Left = 84
        Top = 32
        Width = 265
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdTam: TdmkEdit
        Left = 352
        Top = 32
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCor: TdmkEdit
        Left = 476
        Top = 32
        Width = 241
        Height = 21
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBCouNiv: TGroupBox
      Left = 0
      Top = 65
      Width = 919
      Height = 60
      Align = alTop
      TabOrder = 2
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 78
        Height = 13
        Caption = 'Tipo de material:'
      end
      object Label4: TLabel
        Left = 340
        Top = 16
        Width = 82
        Height = 13
        Caption = 'Parte do material:'
      end
      object Label13: TLabel
        Left = 672
        Top = 16
        Width = 28
        Height = 13
        Caption = 'PPP*:'
      end
      object Label21: TLabel
        Left = 720
        Top = 16
        Width = 30
        Height = 13
        Caption = 'PMP*:'
      end
      object Label22: TLabel
        Left = 776
        Top = 16
        Width = 28
        Height = 13
        Caption = 'PKP*:'
      end
      object EdCouNiv2: TdmkEditCB
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrGrGruXCou'
        QryCampo = 'CouNiv2'
        UpdCampo = 'CouNiv2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCouNiv2
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCouNiv2: TdmkDBLookupComboBox
        Left = 72
        Top = 32
        Width = 264
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCouNiv2
        TabOrder = 1
        dmkEditCB = EdCouNiv2
        QryName = 'QrGrGruXCou'
        QryCampo = 'CouNiv2'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCouNiv1: TdmkEditCB
        Left = 340
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrGrGruXCou'
        QryCampo = 'CouNiv1'
        UpdCampo = 'CouNiv1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCouNiv1
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCouNiv1: TdmkDBLookupComboBox
        Left = 396
        Top = 32
        Width = 264
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCouNiv1
        TabOrder = 3
        dmkEditCB = EdCouNiv1
        QryName = 'QrGrGruXCou'
        QryCampo = 'CouNiv1'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPrevPcPal: TdmkEdit
        Left = 672
        Top = 32
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '110'
        QryCampo = 'PrevPcPal'
        UpdCampo = 'PrevPcPal'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 110
        ValWarn = False
      end
      object EdPrevAMPal: TdmkEdit
        Left = 720
        Top = 32
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'PrevAMPal'
        UpdCampo = 'PrevAMPal'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPrevKgPal: TdmkEdit
        Left = 776
        Top = 32
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PrevKgPal'
        UpdCampo = 'PrevKgPal'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 125
      Width = 919
      Height = 64
      Align = alTop
      Caption = ' Impress'#227'o para clientes: '
      TabOrder = 3
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 915
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label11: TLabel
          Left = 12
          Top = 4
          Width = 30
          Height = 13
          Caption = 'Artigo:'
        end
        object Label12: TLabel
          Left = 360
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Classe:'
        end
        object EdArtigoImp: TdmkEdit
          Left = 12
          Top = 20
          Width = 344
          Height = 21
          MaxLength = 50
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ArtigoImp'
          UpdCampo = 'ArtigoImp'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdClasseImp: TdmkEdit
          Left = 360
          Top = 20
          Width = 300
          Height = 21
          MaxLength = 30
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ClasseImp'
          UpdCampo = 'ClasseImp'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 919
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 871
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 655
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 465
        Height = 32
        Caption = 'Configura'#231#227'o de Mat'#233'rias-primas PDA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 465
        Height = 32
        Caption = 'Configura'#231#227'o de Mat'#233'rias-primas PDA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 465
        Height = 32
        Caption = 'Configura'#231#227'o de Mat'#233'rias-primas PDA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 919
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 915
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 460
        Height = 16
        Caption = 
          'PPP*, PMP* e PKP* : Previs'#227'o de Pe'#231'as, Metros quadrados e Kilos ' +
          'por Pallet '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 460
        Height = 16
        Caption = 
          'PPP*, PMP* e PKP* : Previs'#227'o de Pe'#231'as, Metros quadrados e Kilos ' +
          'por Pallet '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrVSNatPDA: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSNatPDABeforeOpen
    AfterOpen = QrVSNatPDAAfterOpen
    BeforeClose = QrVSNatPDABeforeClose
    AfterScroll = QrVSNatPDAAfterScroll
    SQL.Strings = (
      'SELECT flu.Nome NO_Fluxo, fo1.Nome NO_ReceiRecu,'
      'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,'
      'wac.*, '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM wbartcab wac'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wac.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN fluxos  flu ON flu.Codigo=wac.Fluxo'
      'LEFT JOIN formulas fo1 ON fo1.Numero=wac.ReceiRecu'
      'LEFT JOIN formulas fo2 ON fo2.Numero=wac.ReceiRefu'
      'LEFT JOIN formulas ti1 ON ti1.Numero=wac.ReceiAcab')
    Left = 96
    Top = 20
    object QrVSNatPDAGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrVSNatPDAMediaMinM2: TFloatField
      FieldName = 'MediaMinM2'
    end
    object QrVSNatPDAMediaMaxM2: TFloatField
      FieldName = 'MediaMaxM2'
    end
    object QrVSNatPDAArtigoImp: TWideStringField
      FieldName = 'ArtigoImp'
      Size = 50
    end
    object QrVSNatPDAClasseImp: TWideStringField
      FieldName = 'ClasseImp'
      Size = 30
    end
    object QrVSNatPDAGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSNatPDALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSNatPDADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSNatPDADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSNatPDAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSNatPDAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSNatPDAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSNatPDAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSNatPDAGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSNatPDANO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsVSNatPDA: TDataSource
    DataSet = QrVSNatPDA
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtMP
    CanUpd01 = BtArtigo
    Left = 168
    Top = 20
  end
  object QrCad: TmySQLQuery
    Database = Dmod.MyDB
    Left = 180
    Top = 88
    object QrCadGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCadLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCadUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCadGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrCadNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrCadOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCadBRLMedM2: TFloatField
      FieldName = 'BRLMedM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object PMMP: TPopupMenu
    OnPopup = PMMPPopup
    Left = 436
    Top = 432
    object Incluinovoartigoclassificado1: TMenuItem
      Caption = '&Inclui nova mat'#233'ria-prima PDA'
      OnClick = Incluinovoartigoclassificado1Click
    end
    object AlteraArtigoClassificadoAtual1: TMenuItem
      Caption = '&Altera mat'#233'ria-prima PDA atual'
      Enabled = False
      OnClick = AlteraArtigoClassificadoAtual1Click
    end
    object ExcluiArtigoClassificadoAtual1: TMenuItem
      Caption = '&Exclui mat'#233'ria-prima PDA atual'
      Enabled = False
      OnClick = ExcluiArtigoClassificadoAtual1Click
    end
  end
  object PMArtigo: TPopupMenu
    Left = 556
    Top = 436
    object Incluilinkdeprodutos1: TMenuItem
      Caption = '&Inclui link de artigo'
      OnClick = Incluilinkdeprodutos1Click
    end
    object Alteralinkdeprodutos1: TMenuItem
      Caption = 'Altera link de artigo'
      Visible = False
      OnClick = Alteralinkdeprodutos1Click
    end
    object Excluilinkdeprodutos1: TMenuItem
      Caption = '&Exclui link de artigo'
    end
  end
  object QrCouNiv1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv1'
      'ORDER BY Nome')
    Left = 544
    Top = 128
    object QrCouNiv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv1: TDataSource
    DataSet = QrCouNiv1
    Left = 544
    Top = 176
  end
  object QrCouNiv2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv2'
      'ORDER BY Nome')
    Left = 612
    Top = 128
    object QrCouNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv2: TDataSource
    DataSet = QrCouNiv2
    Left = 612
    Top = 176
  end
  object QrGraGruXCou: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cn1.Nome NO_CouNiv1, '
      'cn2.Nome NO_CouNiv2, gxc.* '
      'FROM gragruxcou gxc'
      'LEFT JOIN couniv1 cn1 ON cn1.Codigo=gxc.CouNiv1'
      'LEFT JOIN couniv2 cn2 ON cn2.Codigo=gxc.CouNiv2'
      'WHERE gxc.GraGruX=1')
    Left = 684
    Top = 128
    object QrGraGruXCouPrevAMPal: TFloatField
      FieldName = 'PrevAMPal'
    end
    object QrGraGruXCouPrevKgPal: TFloatField
      FieldName = 'PrevKgPal'
    end
    object QrGraGruXCouArtigoImp: TWideStringField
      FieldName = 'ArtigoImp'
      Size = 50
    end
    object QrGraGruXCouClasseImp: TWideStringField
      FieldName = 'ClasseImp'
      Size = 30
    end
    object QrGraGruXCouNO_CouNiv1: TWideStringField
      FieldName = 'NO_CouNiv1'
      Size = 60
    end
    object QrGraGruXCouNO_CouNiv2: TWideStringField
      FieldName = 'NO_CouNiv2'
      Size = 60
    end
    object QrGraGruXCouGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXCouCouNiv1: TIntegerField
      FieldName = 'CouNiv1'
    end
    object QrGraGruXCouCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrGraGruXCouLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGruXCouDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGruXCouDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGruXCouUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGruXCouUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGruXCouAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGruXCouAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraGruXCouPrevPcPal: TIntegerField
      FieldName = 'PrevPcPal'
    end
    object QrGraGruXCouGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object DsGraGruXCou: TDataSource
    DataSet = QrGraGruXCou
    Left = 684
    Top = 176
  end
end
