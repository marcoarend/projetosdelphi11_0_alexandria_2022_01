object FmVSCPMRSBCb: TFmVSCPMRSBCb
  Left = 368
  Top = 194
  Caption = 
    'WET-CURTI-191 :: Configura'#231#227'o de Plano de Metas de Rendimento de' +
    ' Sub-produtos'
  ClientHeight = 451
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 105
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 16
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 76
        Top = 16
        Width = 41
        Height = 13
        Caption = 'Empresa'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 848
        Top = 16
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
        FocusControl = DBEdit3
      end
      object Label5: TLabel
        Left = 924
        Top = 16
        Width = 48
        Height = 13
        Caption = 'Data final:'
        FocusControl = DBEdit4
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSCPMRSBCb
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 16
        Top = 72
        Width = 981
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsVSCPMRSBCb
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSCPMRSBCb
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 136
        Top = 32
        Width = 709
        Height = 21
        DataField = 'NO_EMP'
        DataSource = DsVSCPMRSBCb
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 848
        Top = 32
        Width = 72
        Height = 21
        DataField = 'DataIni_TXT'
        DataSource = DsVSCPMRSBCb
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 924
        Top = 32
        Width = 72
        Height = 21
        DataField = 'DataFim_TXT'
        DataSource = DsVSCPMRSBCb
        TabOrder = 5
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 291
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 578
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 605
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 105
      Width = 1008
      Height = 148
      Align = alTop
      DataSource = DsVSCPMRSBIt
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Artigo'
          Width = 473
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DivKgSPpcCou'
          Title.Caption = 'Kg/Pe'#231'a'#185':'
          Width = 79
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PercKgSPKgCou'
          Title.Caption = '% Kg/kg'#178':'
          Width = 76
          Visible = True
        end>
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label6: TLabel
        Left = 768
        Top = 16
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label10: TLabel
        Left = 884
        Top = 16
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 981
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 633
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object TPDataIni: TdmkEditDateTimePicker
        Left = 768
        Top = 32
        Width = 112
        Height = 21
        Date = 0.624225613399175900
        Time = 0.624225613399175900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataIni'
        UpdCampo = 'DataIni'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPDataFim: TdmkEditDateTimePicker
        Left = 884
        Top = 32
        Width = 112
        Height = 21
        Date = 0.624225613399175900
        Time = 0.624225613399175900
        TabOrder = 4
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataFim'
        UpdCampo = 'DataFim'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 292
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 797
        Height = 32
        Caption = 'Configura'#231#227'o de Plano de Metas de Rendimento de Sub-produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 797
        Height = 32
        Caption = 'Configura'#231#227'o de Plano de Metas de Rendimento de Sub-produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 797
        Height = 32
        Caption = 'Configura'#231#227'o de Plano de Metas de Rendimento de Sub-produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 590
        Height = 16
        Caption = 
          #185': Kg de sub-produto por pe'#231'a de couro.                '#178':Percent' +
          'ual de Kg de sub-produto por kg de couro.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 590
        Height = 16
        Caption = 
          #185': Kg de sub-produto por pe'#231'a de couro.                '#178':Percent' +
          'ual de Kg de sub-produto por kg de couro.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSCPMRSBCb: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSCPMRSBCbBeforeOpen
    AfterOpen = QrVSCPMRSBCbAfterOpen
    BeforeClose = QrVSCPMRSBCbBeforeClose
    AfterScroll = QrVSCPMRSBCbAfterScroll
    SQL.Strings = (
      'SELECT cab.*, '
      'IF(emp.Tipo=0, RazaoSocial, emp.Nome) NO_EMP  '
      'FROM vscpmrsbcb cab '
      'LEFT JOIN entidades emp ON emp.Codigo=cab.Empresa ')
    Left = 92
    Top = 233
    object QrVSCPMRSBCbCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCPMRSBCbNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSCPMRSBCbEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSCPMRSBCbDataIni: TDateField
      FieldName = 'DataIni'
    end
    object QrVSCPMRSBCbDataFim: TDateField
      FieldName = 'DataFim'
    end
    object QrVSCPMRSBCbLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSCPMRSBCbDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSCPMRSBCbDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSCPMRSBCbUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSCPMRSBCbUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSCPMRSBCbAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSCPMRSBCbAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSCPMRSBCbNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrVSCPMRSBCbDataIni_TXT: TWideStringField
      FieldName = 'DataIni_TXT'
      Size = 10
    end
    object QrVSCPMRSBCbDataFim_TXT: TWideStringField
      FieldName = 'DataFim_TXT'
      Size = 10
    end
  end
  object DsVSCPMRSBCb: TDataSource
    DataSet = QrVSCPMRSBCb
    Left = 92
    Top = 277
  end
  object QrVSCPMRSBIt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT its.*, CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR '
      'FROM vscpmrsbit its '
      'LEFT JOIN gragrux ggx ON ggx.Controle=its.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ')
    Left = 188
    Top = 237
    object QrVSCPMRSBItCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCPMRSBItControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSCPMRSBItGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSCPMRSBItDivKgSPpcCou: TFloatField
      FieldName = 'DivKgSPpcCou'
      DisplayFormat = '0.00'
    end
    object QrVSCPMRSBItPercKgSPKgCou: TFloatField
      FieldName = 'PercKgSPKgCou'
      DisplayFormat = '0.0000'
    end
    object QrVSCPMRSBItLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSCPMRSBItDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSCPMRSBItDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSCPMRSBItUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSCPMRSBItUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSCPMRSBItAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSCPMRSBItAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSCPMRSBItNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsVSCPMRSBIt: TDataSource
    DataSet = QrVSCPMRSBIt
    Left = 188
    Top = 281
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
end
