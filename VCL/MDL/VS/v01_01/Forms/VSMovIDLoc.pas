unit VSMovIDLoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, UnGrl_Vars;

type
  TFmVSMovIDLoc = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdMovimID: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenVSMovIDLoc(Codigo: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSMovIDLoc: TFmVSMovIDLoc;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnVS_CRC_PF(*, UnGrade_PF, UnGrade_Jan*);

{$R *.DFM}

procedure TFmVSMovIDLoc.BtOKClick(Sender: TObject);
var
  Codigo, MovimID, StqCenLoc: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimID        := Geral.IMV(DBEdMovimID.Text);
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  //
  if MyObjects.FIC(MovimID = 0,  nil, 'MovimID indefinido!') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local (3)!') then
    Exit;
  Codigo := UMyMod.BPGS1I32('vsmovidloc', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmovidloc', False, [
  'MovimID', 'StqCenLoc'], [
  'Codigo'], [
  MovimID, StqCenLoc], [
  Codigo], True) then
  begin
    ReopenVSMovIDLoc(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdCodigo.ValueVariant    := 0;
      EdStqCenLoc.ValueVariant := 0;
      CBStqCenLoc.KeyValue     := Null;
    end else Close;
  end;
end;

procedure TFmVSMovIDLoc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMovIDLoc.FormActivate(Sender: TObject);
begin
  DBEdMovimID.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmVSMovIDLoc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSMovIDLoc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMovIDLoc.ReopenVSMovIDLoc(Codigo: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Codigo <> 0 then
      FQrIts.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmVSMovIDLoc.SpeedButton1Click(Sender: TObject);
begin
  VAR_CAD_STQCENLOC := 0;
  VS_CRC_PF.MostraFormStqCenCad(0, EdStqCenLoc.ValueVariant);
  UMyMod.SetaCodigoPesquisado(
    EdStqCenLoc, CBStqCenLoc, QrStqCenLoc, VAR_CAD_STQCENLOC);
end;

end.
