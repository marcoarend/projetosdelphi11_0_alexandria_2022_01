unit VSCfgEstqInNatEFD;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  dmkDBGridZTO, Vcl.Mask, UnDmkProcFunc, dmkDBLookupComboBox, dmkEditCB,
  AppListas, UnProjGroup_Vars, UnProjGroup_Consts, mySQLDirectQuery,
  dmkCheckGroup, UMySQLDB, Vcl.Menus, UnAppEnums;

type
  TFmVSCfgEstqInNatEFD = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PB1: TProgressBar;
    DGPsq: TdmkDBGridZTO;
    DGBxa: TdmkDBGridZTO;
    Splitter1: TSplitter;
    Panel5: TPanel;
    BtCorrige: TBitBtn;
    QrPsq: TmySQLQuery;
    DsPsq: TDataSource;
    QrBxa: TmySQLQuery;
    QrPsqGGXInn: TIntegerField;
    QrPsqIMEIInn: TIntegerField;
    QrPsqDataHora: TDateTimeField;
    QrPsqMovimID: TIntegerField;
    QrPsqMovimNiv: TIntegerField;
    QrPsqSrcMovID: TIntegerField;
    QrPsqPecasInn: TFloatField;
    QrPsqAreaM2Inn: TFloatField;
    QrPsqPesoKgInn: TFloatField;
    QrPsqGGXBxa: TIntegerField;
    QrPsqSrcNivel2: TIntegerField;
    QrPsqPecasBxa: TFloatField;
    QrPsqAreaM2Bxa: TFloatField;
    QrPsqPesoKgBxa: TFloatField;
    QrPsqSdoPc: TFloatField;
    QrPsqSdoM2: TFloatField;
    QrPsqSdoKg: TFloatField;
    QrPsqReduzido: TIntegerField;
    QrPsqNO_PRD_TAM_COR: TWideStringField;
    QrBxaGGXBxa: TIntegerField;
    QrBxaIMEIBxa: TIntegerField;
    QrBxaDataHora: TDateTimeField;
    QrBxaMovimID: TIntegerField;
    QrBxaMovimNiv: TIntegerField;
    QrBxaSrcMovID: TIntegerField;
    QrBxaPecas: TFloatField;
    QrBxaAreaM2: TFloatField;
    QrBxaPesoKg: TFloatField;
    QrBxaSrcNivel2: TIntegerField;
    DsBxa: TDataSource;
    PMCorrigeID01: TPopupMenu;
    CorrigePesosBaixaInNatura1: TMenuItem;
    CorrigePesosBaixaInNatura_Atual1: TMenuItem;
    CorrigePesosBaixaInNatura_Selecionadospossveis1: TMenuItem;
    CorrigePesosBaixaInNatura_Todospossveis1: TMenuItem;
    ID01N2CorrigepeasepesobaizaInNatura1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtCorrigeClick(Sender: TObject);
    procedure QrPsqBeforeClose(DataSet: TDataSet);
    procedure QrPsqAfterScroll(DataSet: TDataSet);
    procedure QrBxaAfterOpen(DataSet: TDataSet);
    procedure QrBxaBeforeClose(DataSet: TDataSet);
    procedure PMCorrigeID01Popup(Sender: TObject);
    procedure CorrigePesosBaixaInNatura_Atual1Click(Sender: TObject);
    procedure CorrigePesosBaixaInNatura_Todospossveis1Click(Sender: TObject);
    procedure CorrigePesosBaixaInNatura_Selecionadospossveis1Click(
      Sender: TObject);
    procedure ID01N2CorrigepeasepesobaizaInNatura1Click(Sender: TObject);
  private
    { Private declarations }
    FDiaIni, FDiaFim: TDateTime;
    //
    procedure CorrigeBaixasPesoKgAtual();
    function  HabilitaCorrecaoPesoBaixaPsqAtual(): Boolean;
    function  HabilitaCorrecaoPecaEPesoBaixaPsqAtual(): Boolean;
    procedure RemoveIMEI(IMEIInn: Integer);
    procedure ReopenSdoInn();
    procedure PesquisaErros();
  public
    { Public declarations }
    FImporExpor, FAnoMes, FEmpresa, FPeriApu: Integer;
    //
    procedure PreencheAnoMesPeriodo(AnoMes: Integer);
  end;

  var
  FmVSCfgEstqInNatEFD: TFmVSCfgEstqInNatEFD;

implementation

uses UnMyObjects, DmkDAC_PF, Module, ModuleGeral, UMySQLModule, UnVS_PF;

{$R *.DFM}

procedure TFmVSCfgEstqInNatEFD.BtCorrigeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCorrigeID01, BtCorrige);
end;

procedure TFmVSCfgEstqInNatEFD.BtOKClick(Sender: TObject);
begin
  PesquisaErros();
end;

procedure TFmVSCfgEstqInNatEFD.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCfgEstqInNatEFD.CorrigeBaixasPesoKgAtual();
var
  MediaKg, PesoKg, SaldoKg, SaldoPc: Double;
  Controle: Integer;
begin
  MediaKg := 0;
  if QrPsqPecasInn.Value > 0 then
    MediaKg := QrPsqPesoKgInn.Value / QrPsqPecasInn.Value;
  //
  if (MediaKg > 0) and HabilitaCorrecaoPesoBaixaPsqAtual() then
  begin
    SaldoKg := QrPsqPesoKgInn.Value;
    SaldoPc := QrPsqPecasInn.Value;
    //
    QrBxa.First;
    while not QrBxa.Eof do
    begin
      Controle := QrBxaIMEIBxa.Value;
      //
      PesoKg := (Round(QrBxaPecas.Value * MediaKg * 1000)) /1000;
      SaldoKg := SaldoKg + PesoKg;
      SaldoPc := SaldoPc + QrBxaPecas.Value;
      //
      if (QrBxa.RecNo = QrBxa.RecordCount) and (SaldoPc = 0) and
      (SaldoKg <> 0) then
        PesoKg := PesoKg - SaldoKg;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'PesoKg'], ['Controle'], [
      PesoKg], [Controle], True);
      //
      QrBxa.Next;
    end;
    Controle := QrPsqIMEIInn.Value;
    VS_PF.AtualizaSaldoIMEI(Controle, True);
    RemoveIMEI(Controle);
  end;
end;

procedure TFmVSCfgEstqInNatEFD.CorrigePesosBaixaInNatura_Atual1Click(
  Sender: TObject);
begin
  CorrigeBaixasPesoKgAtual();
  ReopenSdoInn();
end;

procedure TFmVSCfgEstqInNatEFD.CorrigePesosBaixaInNatura_Selecionadospossveis1Click(
  Sender: TObject);
var
  I: Integer;
begin
  with DGPsq.DataSource.DataSet do
  for I := 0 to DGPsq.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DGPsq.SelectedRows.Items[I]));
    GotoBookmark(DGPsq.SelectedRows.Items[I]);
    //
    CorrigeBaixasPesoKgAtual();
  end;
  ReopenSdoInn();
end;

procedure TFmVSCfgEstqInNatEFD.CorrigePesosBaixaInNatura_Todospossveis1Click(
  Sender: TObject);
begin
  QrPsq.First;
  while not QrPsq.Eof do
  begin
    CorrigeBaixasPesoKgAtual();
    //
    QrPsq.Next;
  end;
  ReopenSdoInn();
end;

procedure TFmVSCfgEstqInNatEFD.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCfgEstqInNatEFD.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSCfgEstqInNatEFD.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmVSCfgEstqInNatEFD.HabilitaCorrecaoPecaEPesoBaixaPsqAtual: Boolean;
begin
  Result := (QrPsqSdoPc.Value < 0) and (QrPsqSdoKg.Value < 0) and
  (QrBxa.State <> dsInactive) and (QrBxa.RecordCount > 0)
  and (QrPsqMovimID.Value = Integer(emidCompra));
end;

function TFmVSCfgEstqInNatEFD.HabilitaCorrecaoPesoBaixaPsqAtual(): Boolean;
begin
  Result := (QrBxa.State <> dsInactive) and (QrBxa.RecordCount > 0)
  and (QrPsqMovimID.Value = Integer(emidCompra)) and (QrPsqSdoPc.Value = 0);
end;

procedure TFmVSCfgEstqInNatEFD.ID01N2CorrigepeasepesobaizaInNatura1Click(
  Sender: TObject);
var
  Pecas, PesoKg: Double;
  Controle: Integer;
begin
  Controle := QrBxaIMEIBxa.Value;
  //
  Pecas  := QrBxaPecas.Value - QrPsqSdoPc.Value;
  PesoKg := QrBxaPesoKg.Value - QrPsqSdoKg.Value;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'Pecas', 'PesoKg'], ['Controle'], [
  Pecas, PesoKg], [Controle], True);
  //
  Controle := QrPsqIMEIInn.Value;
  VS_PF.AtualizaSaldoIMEI(Controle, True);
  RemoveIMEI(Controle);
  ReopenSdoInn();
end;

procedure TFmVSCfgEstqInNatEFD.PesquisaErros();
begin
  Screen.Cursor := crHourGlass;
  try
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '1/5 - Criando tabela temporária de entrada de in natura');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _LCT_INN_; ',
  'CREATE TABLE _LCT_INN_ ',
  'SELECT GraGruX GGXInn, Controle IMEIInn,  ',
  'DataHora, MovimID, MovimNiv,  ',
  'SrcMovID, Pecas PecasInn,  ',
  'AreaM2 AreaM2Inn, PesoKg PesoKgInn  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimID=1 ',
  'ORDER BY DataHora ',
  '; ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '2/5 - Criando tabela temporária de baixa de in natura');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _LCT_BXA_; ',
  'CREATE TABLE _LCT_BXA_ ',
  'SELECT GraGruX GGXBxa, Controle IMEIBxa,  ',
  'DataHora, MovimID, MovimNiv,  ',
  'SrcMovID, Pecas, AreaM2, PesoKg,  ',
  'SrcNivel2  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE SrcMovID=1 ',
  'ORDER BY DataHora ',
  '; ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '3/5 - Criando tabela temporária de soma de baixa de in natura');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _SUM_BXA_; ',
  'CREATE TABLE _SUM_BXA_ ',
  'SELECT GraGruX GGXBxa, SrcNivel2,  ',
  'SUM(Pecas) PecasBxa,  ',
  'SUM(AreaM2) AreaM2Bxa, ',
  'SUM(PesoKg) PesoKgBxa ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE SrcMovID=1 ',
  'GROUP BY SrcNivel2 ',
  '; ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '4/5 - Criando tabela temporária de saldos de in natura');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _SDO_INN_; ',
  'CREATE TABLE _SDO_INN_ ',
  'SELECT inn.*, bxa.*, ',
  'inn.PecasInn+bxa.PecasBxa SdoPc, ',
  'inn.AreaM2Inn+bxa.AreaM2Bxa SdoM2, ',
  'inn.PesoKgInn+bxa.PesoKgBxa SdoKg  ',
  'FROM _LCT_INN_ inn  ',
  'LEFT JOIN _SUM_BXA_ bxa ON inn.IMEIInn=bxa.SrcNivel2 ',
  '; ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '5/5 - Abrindo tabela temporária de saldos de in natura');
  ReopenSdoInn();
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCfgEstqInNatEFD.PMCorrigeID01Popup(Sender: TObject);
var
  Habilita1, Habilita2, Habilita3, Habilita4: Boolean;
begin
  Habilita1 := (QrPsq.State <> dsInactive);
  CorrigePesosBaixaInNatura1.Enabled := Habilita1;
  //
  Habilita2 := Habilita1 and (QrPsq.RecordCount > 0);
  CorrigePesosBaixaInNatura_Selecionadospossveis1.Enabled := Habilita2;
  CorrigePesosBaixaInNatura_Todospossveis1.Enabled := Habilita2;
  //
  Habilita3 := Habilita2 and HabilitaCorrecaoPesoBaixaPsqAtual();
  CorrigePesosBaixaInNatura_Atual1.Enabled := Habilita3;
  //////////////////////////////////////////////////////////////////////////////
  Habilita4 := Habilita1 and HabilitaCorrecaoPecaEPesoBaixaPsqAtual();
  ID01N2CorrigepeasepesobaizaInNatura1.Enabled := Habilita4;
  //////////////////////////////////////////////////////////////////////////////
end;

procedure TFmVSCfgEstqInNatEFD.PreencheAnoMesPeriodo(AnoMes: Integer);
begin
end;

procedure TFmVSCfgEstqInNatEFD.QrBxaAfterOpen(DataSet: TDataSet);
begin
  BtCorrige.Enabled := QrBxa.RecordCount > 0;
end;

procedure TFmVSCfgEstqInNatEFD.QrBxaBeforeClose(DataSet: TDataSet);
begin
  BtCorrige.Enabled := False;
end;

procedure TFmVSCfgEstqInNatEFD.QrPsqAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBxa, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM _lct_bxa_',
  'WHERE SrcNivel2=' + Geral.FF0(QrPsqIMEIInn.Value),
  'ORDER BY IMEIBxa DESC',
  '']);
end;

procedure TFmVSCfgEstqInNatEFD.QrPsqBeforeClose(DataSet: TDataSet);
begin
  QrBxa.Close;
end;

procedure TFmVSCfgEstqInNatEFD.RemoveIMEI(IMEIInn: Integer);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM _lct_bxa_',
  'WHERE SrcNivel2=' + Geral.FF0(IMEIInn),
  '']);
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM _SDO_INN_',
  'WHERE IMEIInn=' + Geral.FF0(IMEIInn),
  '']);
end;

procedure TFmVSCfgEstqInNatEFD.ReopenSdoInn();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, DModG.MyPID_DB, [
  'SELECT sdo.*, ggx.Controle Reduzido, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR  ',
  'FROM _SDO_INN_ sdo ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=sdo.GGXInn ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE SdoPc <= 0 ',
  'AND ( ',
  '  SdoM2 <> 0 ',
  '  OR ',
  '  SdoKg <> 0 ',
  ') ',
  'ORDER BY DataHora ',
  '']);
end;

end.
