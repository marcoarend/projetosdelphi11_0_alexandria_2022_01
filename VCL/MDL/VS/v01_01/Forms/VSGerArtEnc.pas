unit VSGerArtEnc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkEditCalc,
  UnProjGroup_Consts, UnAppEnums;

type
  TFmVSGerArtEnc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    LaAreaM2: TLabel;
    EdAreaManM2: TdmkEditCalc;
    EdAreaManP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    LaPecas: TLabel;
    EdPecasMan: TdmkEdit;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdAreaM2: TdmkEdit;
    EdAreaP2: TdmkEdit;
    EdPecas: TdmkEdit;
    EdCodigo: TdmkEdit;
    Label5: TLabel;
    SbCopiar: TSpeedButton;
    Label4: TLabel;
    EdMovimCod: TdmkEdit;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdValorManMP: TdmkEdit;
    Label8: TLabel;
    Label12: TLabel;
    EdCustoManMOKg: TdmkEdit;
    Label13: TLabel;
    EdCustoManMOTot: TdmkEdit;
    Label7: TLabel;
    EdValorManT: TdmkEdit;
    EdValorMP: TdmkEdit;
    Label9: TLabel;
    Label10: TLabel;
    EdCustoMOKg: TdmkEdit;
    Label11: TLabel;
    EdCustoMOTot: TdmkEdit;
    Label14: TLabel;
    EdValorT: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbCopiarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmVSGerArtEnc: TFmVSGerArtEnc;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSGerArtEnc.BtOKClick(Sender: TObject);
var
  DtHrLibCla: String;
  MovimNiv: TEstqMovimNiv;
  MovimCod, Codigo: Integer;
  PecasMan, AreaManM2, AreaManP2: Double;
  CustoManMOKg, CustoManMOTot, ValorManMP, ValorManT: Double;
  procedure AtualizaCampoVSMovIts(Campo: String; Valor: Double);
  var
    MovimID: Integer;
    Controle: Integer;
  begin
    //if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
    //Campo], ['Codigo'], [Valor], [Codigo], True) then
    //begin
      //MovimNiv := Integer(eminDestInds);
      //MovimID  := Integer(emidIndsXX);
      //MovimCod := EdMovimCod.ValueVariant;
      Controle := EdControle.ValueVariant;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      Campo], ['Controle'], [Valor], [Controle], True) then
      begin
        VS_CRC_PF.AtualizaSaldoIMEI(Controle, True);
      end;
    //end;
  end;
begin
  Codigo         := EdCodigo.ValueVariant;
  DtHrLibCla     := Geral.FDT(DModG.ObtemAgora(), 109);
  PecasMan       := EdPecasMan.ValueVariant;
  AreaManM2      := EdAreaManM2.ValueVariant;
  AreaManP2      := EdAreaManP2.ValueVariant;
  //
  CustoManMOKg   := EdCustoManMOKg.ValueVariant;
  CustoManMOTot  := EdCustoManMOTot.ValueVariant;
  ValorManMP     := EdValorManMP.ValueVariant;
  ValorManT      := EdValorManT.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
  'DtHrLibCla', 'PecasMan',
  'AreaManM2', 'AreaManP2',
  'CustoManMOKg', 'CustoManMOTot',
  'ValorManMP', 'ValorManT'
  ], [
  'Codigo'], [
  DtHrLibCla, PecasMan,
  AreaManM2, AreaManP2,
  CustoManMOKg, CustoManMOTot,
  ValorManMP, ValorManT
  ], [
  Codigo], True) then
  begin
    // Aumentar Saldo e custo do Artigo de Ribeira gerado...
    MovimNiv := eminDestCurtiXX;
    MovimCod := EdMovimCod.ValueVariant;
    VS_CRC_PF.DistribuiCustoIndsVS(MovimNiv, MovimCod, Codigo,
      EdControle.ValueVariant);
    //
    // ... caso tenha info manual, este prevalece.
    if PecasMan > 0 then
      AtualizaCampoVSMovIts('Pecas', PecasMan);
    if AreaManM2 > 0 then
      AtualizaCampoVSMovIts('AreaM2', AreaManM2);
    if AreaManP2 > 0 then
      AtualizaCampoVSMovIts('AreaP2', AreaManP2);
    //
    if CustoManMOKg > 0 then
      AtualizaCampoVSMovIts('CustoMOKg', CustoManMOKg);
    if CustoManMOTot > 0 then
      AtualizaCampoVSMovIts('CustoMOTot', CustoManMOTot);
    if ValorManMP > 0 then
      AtualizaCampoVSMovIts('ValorMP', ValorManMP);
    if ValorManT > 0 then
      AtualizaCampoVSMovIts('ValorT', ValorManT);
    //
    Close;
  end;
end;

procedure TFmVSGerArtEnc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerArtEnc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSGerArtEnc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSGerArtEnc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerArtEnc.SbCopiarClick(Sender: TObject);
begin
  EdPecasMan.ValueVariant      := EdPecas.ValueVariant;
  EdAreaManM2.ValueVariant     := EdAreaM2.ValueVariant;
  EdAreaManP2.ValueVariant     := EdAreaP2.ValueVariant;
  //
  EdValorManMP.ValueVariant    := EdValorMP.ValueVariant;
  EdCustoManMOKg.ValueVariant  := EdCustoMOKg.ValueVariant;
  EdCustoManMOTot.ValueVariant := EdCustoMOTot.ValueVariant;
  EdValorManT.ValueVariant     := EdValorT.ValueVariant;
end;

end.
