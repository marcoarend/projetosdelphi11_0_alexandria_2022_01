object DmModVS: TDmModVS
  Height = 931
  Width = 1024
  PixelsPerInch = 96
  object Qr14GraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle>0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 56
    Top = 4
    object IntegerField5: TIntegerField
      FieldName = 'GraGru1'
    end
    object IntegerField11: TIntegerField
      FieldName = 'Controle'
    end
    object StringField5: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object StringField11: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object IntegerField12: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object StringField12: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds14GraGruX: TDataSource
    DataSet = Qr14GraGruX
    Left = 56
    Top = 52
  end
  object Qr14Fornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 56
    Top = 100
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds14Fornecedor: TDataSource
    DataSet = Qr14Fornecedor
    Left = 56
    Top = 144
  end
  object Qr14VSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 56
    Top = 188
    object Qr14VSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr14VSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds14VSSerFch: TDataSource
    DataSet = Qr14VSSerFch
    Left = 57
    Top = 233
  end
  object Qr13Sintetico: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vfi.*, vsf.Nome NO_SERFCH, ggy.Nome NO_GGY '
      'FROM _vsfluxincon_fmvsmovimp vfi'
      'LEFT JOIN bluederm.gragruy ggy ON ggy.Codigo=vfi.GraGruY'
      'LEFT JOIN bluederm.vsserfch vsf ON vsf.Codigo=vfi.SerieFch'
      'ORDER BY vfi.GraGruY, vfi.SerieFch, '
      'vfi.Ficha, vfi.IMEI_Src, vfi.Pallet')
    Left = 140
    Top = 4
    object Qr13SinteticoGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object Qr13SinteticoInteiros: TFloatField
      FieldName = 'Inteiros'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object Qr13SinteticoSerieFch: TIntegerField
      FieldName = 'SerieFch'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object Qr13SinteticoFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object Qr13SinteticoIMEI_Src: TIntegerField
      FieldName = 'IMEI_Src'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object Qr13SinteticoPallet: TIntegerField
      FieldName = 'Pallet'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object Qr13SinteticoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr13SinteticoNO_SERFCH: TWideStringField
      FieldName = 'NO_SERFCH'
      Size = 60
    end
    object Qr13SinteticoNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object Qr13SinteticoOperacao: TIntegerField
      FieldName = 'Operacao'
    end
  end
  object Qr13DtHr: TMySQLQuery
    Database = Dmod.MyDB
    Left = 140
    Top = 49
    object Qr13DtHrDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
  object Qr16Fatores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pra.DtHrFimCla, pra.Codigo CodigoClaRcl, '
      'pra.CacCod OC, pra.VSGerRcl IDGeracao,'
      'pra.VSPallet Pallet_Src, pri.VSPallet Pallet_Dst, '
      'c1s.FatorInt FatorIntSrc, c1d.FatorInt FatorIntDst '
      'FROM vsparclitsa pri'
      'LEFT JOIN vsparclcaba pra ON pra.Codigo=pri.Codigo'
      'LEFT JOIN vspalleta  pas ON pas.Codigo=pra.VSPallet'
      'LEFT JOIN vspalleta  pad ON pad.Codigo=pri.VSPallet'
      'LEFT JOIN gragruxcou xcs ON xcs.GraGruX=pas.GraGruX'
      'LEFT JOIN gragruxcou xcd ON xcd.GraGruX=pad.GraGruX'
      'LEFT JOIN couniv1    c1s ON c1s.Codigo=xcs.CouNiv1 '
      'LEFT JOIN couniv1    c1d ON c1d.Codigo=xcd.CouNiv1 '
      'WHERE c1s.FatorInt <> c1d.FatorInt')
    Left = 140
    Top = 97
    object Qr16FatoresClaRcl: TFloatField
      DisplayLabel = 'Tipo C/R'
      FieldName = 'ClaRcl'
    end
    object Qr16FatoresDtHrFimCla: TDateTimeField
      DisplayLabel = 'Final C/R'
      FieldName = 'DtHrFimCla'
    end
    object Qr16FatoresCodigoClaRcl: TIntegerField
      DisplayLabel = 'ID C/R'
      FieldName = 'CodigoClaRcl'
      Required = True
    end
    object Qr16FatoresOC: TIntegerField
      FieldName = 'OC'
    end
    object Qr16FatoresIDGeracao: TIntegerField
      DisplayLabel = 'ID Gera'#231#227'o'
      FieldName = 'IDGeracao'
    end
    object Qr16FatoresPallet_Src: TFloatField
      DisplayLabel = 'Pallet origem'
      FieldName = 'Pallet_Src'
    end
    object Qr16FatoresPallet_Dst: TIntegerField
      DisplayLabel = 'Pallet destino'
      FieldName = 'Pallet_Dst'
    end
    object Qr16FatoresFatorIntSrc: TFloatField
      DisplayLabel = 'Fator origem'
      FieldName = 'FatorIntSrc'
    end
    object Qr16FatoresFatorIntDst: TFloatField
      DisplayLabel = 'Fator destino'
      FieldName = 'FatorIntDst'
    end
  end
  object Ds16Fatores: TDataSource
    DataSet = Qr16Fatores
    Left = 140
    Top = 148
  end
  object Qr13VSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 140
    Top = 192
    object Qr13VSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr13VSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds13VSSerFch: TDataSource
    DataSet = Qr13VSSerFch
    Left = 140
    Top = 240
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, ggx.GraGruY'
      'FROM vsmovits wmi  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet  '
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa  '
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro '
      'WHERE wmi.Pecas > 0 '
      'AND wmi.Empresa=-11'
      'AND wmi.Pallet > 0 '
      'AND wmi.Pallet=505'
      'AND wmi.SdoVrtPeca > 0 '
      'AND MovimID IN (0,7,8,11,13,14) '
      'ORDER BY Controle ')
    Left = 220
    Top = 5
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVSMovItsReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovItsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object QrUniFrn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 564
    Top = 4
    object QrUniFrnTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrUniFrnVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrUniFrnPecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrMulFrn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 564
    Top = 52
    object QrMulFrnFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrMulFrnPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMulFrnSiglaVS: TWideStringField
      FieldName = 'SiglaVS'
      Size = 10
    end
  end
  object QrLocMFI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 304
    Top = 52
    object QrLocMFIControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrLocMFC: TMySQLQuery
    Database = Dmod.MyDB
    Left = 304
    Top = 4
    object QrLocMFCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrTotFrn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 568
    Top = 100
    object QrTotFrnPecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrMFDst: TMySQLQuery
    Database = Dmod.MyDB
    Left = 568
    Top = 145
  end
  object QrMFSrc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 568
    Top = 196
    object QrMFSrcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMFSrcMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrMFSrcMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrMFSrcMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object QrMFOrfaos: TMySQLQuery
    Database = Dmod.MyDB
    Left = 568
    Top = 244
    object QrMFOrfaosControle: TSmallintField
      FieldName = 'Controle'
    end
    object QrMFOrfaosTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrMFOrfaosVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
  end
  object QrIMEI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vsmovits'
      'WHERE Controle=:P0')
    Left = 388
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrIMEICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIMEIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIMEIMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrIMEIMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrIMEIMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrIMEIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrIMEITerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrIMEICliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrIMEIMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrIMEILnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrIMEILnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrIMEILnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrIMEIPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrIMEIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrIMEIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrIMEIPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrIMEIAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrIMEIAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrIMEIValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrIMEISrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrIMEISrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrIMEISrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrIMEISrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrIMEISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrIMEISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrIMEISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrIMEISerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrIMEIFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrIMEIMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrIMEIFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrIMEICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrIMEIDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrIMEIDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrIMEIDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrIMEIDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrIMEIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrIMEIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrIMEIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrIMEIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrIMEIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrIMEIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrIMEIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrIMEIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrIMEIAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrIMEINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrIMEIMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEITpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrIMEIZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrIMEIEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrIMEINotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrIMEIFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrIMEIFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrIMEIPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrIMEIPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrIMEIPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrIMEILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIMEIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIMEIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIMEIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIMEIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIMEIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrIMEIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrIMEICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrIMEIReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrIMEIStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrIMEIItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrIMEIVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
    end
    object QrIMEIVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
  end
  object TbCouNiv1: TMySQLTable
    Database = Dmod.MyDB
    AfterEdit = TbCouNiv1AfterEdit
    TableName = 'couniv1'
    Left = 384
    Top = 52
    object TbCouNiv1Codigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
      Required = True
      DisplayFormat = '000'
    end
    object TbCouNiv1Nome: TWideStringField
      FieldName = 'Nome'
      ReadOnly = True
      Required = True
      Size = 60
    end
    object TbCouNiv1FatorInt: TFloatField
      FieldName = 'FatorInt'
      ReadOnly = True
      Required = True
      DisplayFormat = '0.000'
    end
  end
  object DsCouNiv1: TDataSource
    DataSet = TbCouNiv1
    Left = 384
    Top = 100
  end
  object QrCorda: TMySQLQuery
    Database = Dmod.MyDB
    Left = 120
    Top = 320
  end
  object QrVSProQui: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM vsproqui '
      'WHERE Controle=1 ')
    Left = 476
    Top = 4
    object QrVSProQuiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSProQuiControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSProQuiSetorID: TSmallintField
      FieldName = 'SetorID'
    end
    object QrVSProQuiMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSProQuiMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSProQuiMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSProQuiEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSProQuiTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSProQuiMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSProQuiPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSProQuiPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSProQuiAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSProQuiCusPQ: TFloatField
      FieldName = 'CusPQ'
    end
    object QrVSProQuiFuloes: TIntegerField
      FieldName = 'Fuloes'
    end
    object QrVSProQuiMinDta: TDateField
      FieldName = 'MinDta'
    end
    object QrVSProQuiMaxDta: TDateField
      FieldName = 'MaxDta'
    end
    object QrVSProQuiEstqPeca: TFloatField
      FieldName = 'EstqPeca'
    end
    object QrVSProQuiEstqPeso: TFloatField
      FieldName = 'EstqPeso'
    end
    object QrVSProQuiEstqArM2: TFloatField
      FieldName = 'EstqArM2'
    end
    object QrVSProQuiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSProQuiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSProQuiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSProQuiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSProQuiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSProQuiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSProQuiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrSCus2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emc.MPIn, SUM(emc.Custo) Custo,'
      'SUM(emc.Pecas) Pecas, SUM(emc.Peso) Peso,'
      'MIN(emc.DataEmis) MinDta, MAX(emc.DataEmis) MaxDta,'
      'COUNT(emc.MPIn) Fuloes, rec.Setor'
      'FROM emitcus emc'
      'LEFT JOIN formulas rec ON rec.Numero=emc.Formula'
      'WHERE emc.MPIn=:P0'
      'GROUP BY emc.MPIn, rec.Setor')
    Left = 476
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCus2MPIn: TIntegerField
      FieldName = 'MPIn'
      Required = True
    end
    object QrSCus2Custo: TFloatField
      FieldName = 'Custo'
    end
    object QrSCus2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSCus2Peso: TFloatField
      FieldName = 'Peso'
    end
    object QrSCus2MinDta: TDateTimeField
      FieldName = 'MinDta'
      Required = True
    end
    object QrSCus2MaxDta: TDateTimeField
      FieldName = 'MaxDta'
      Required = True
    end
    object QrSCus2Fuloes: TLargeintField
      FieldName = 'Fuloes'
      Required = True
    end
    object QrSCus2Setor: TIntegerField
      FieldName = 'Setor'
    end
  end
  object QrVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vsmovits')
    Left = 480
    Top = 100
    object QrVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVMIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVMIMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVMIMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVMIMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVMIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVMITerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVMICliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVMIMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVMILnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVMILnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVMIDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVMIPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVMIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVMIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVMIValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVMISrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVMISrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVMISrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVMISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVMIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVMIFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVMIMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVMICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVMICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVMISrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVMISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVMISerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVMIFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVMIValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVMIDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVMIDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVMIDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVMIDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVMIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVMIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVMIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVMIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVMIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVMIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVMIAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVMINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVMIMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVMITpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVMIZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrVMIEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrVMILnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrVMICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrVMINotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrVMIFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrVMIFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrVMIPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrVMIPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrVMIPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrVMIReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrVMIStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVMIItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrVMIVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
    end
    object QrVMIVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
  end
  object QrSumEmit: TMySQLQuery
    Database = Dmod.MyDB
    Left = 480
    Top = 156
    object QrSumEmitCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrSumEmitPeso: TFloatField
      FieldName = 'Peso'
    end
  end
  object QrVMIs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 484
    Top = 204
    object QrVMIsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVMIsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVMIsQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrSumVMI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 484
    Top = 252
    object QrSumVMIQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrUniNFe: TMySQLQuery
    Database = Dmod.MyDB
    Left = 652
    Top = 4
    object QrUniNFeNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrUniNFeNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrUniNFeVSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
    object QrUniNFePecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrMulNFe: TMySQLQuery
    Database = Dmod.MyDB
    Left = 652
    Top = 52
    object QrMulNFeNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrMulNFeNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrMulNFePecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrTotNFe: TMySQLQuery
    Database = Dmod.MyDB
    Left = 656
    Top = 100
    object QrTotNFePecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrVSCabNFe: TMySQLQuery
    Database = Dmod.MyDB
    Left = 656
    Top = 148
    object QrVSCabNFeSerie: TSmallintField
      FieldName = 'Serie'
    end
    object QrVSCabNFenNF: TIntegerField
      FieldName = 'nNF'
    end
  end
  object QrVSMovCab: TMySQLQuery
    Database = Dmod.MyDB
    Left = 360
    Top = 208
  end
  object QrVSBalEmp: TMySQLQuery
    Database = Dmod.MyDB
    Left = 220
    Top = 56
    object QrVSBalEmpAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
  end
  object QrX999_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM efd_h010'
      'WHERE ImporExpor=3'
      'AND AnoMes=201401'
      'AND Empresa=-11'
      'AND H005=1'
      'AND BalID=4'
      'AND BalNum=0'
      'AND BalItm=0')
    Left = 744
    Top = 4
  end
  object QrRedzConfltDstGGX: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _t03_; '
      'CREATE TABLE _t03_ '
      'SELECT DstNivel2, DstGGX  '
      'FROM vsmovits '
      'WHERE DstNivel2 <> 0; '
      ' '
      ' '
      'SELECT vmi.Controle, vmi.GraGruX, '
      't03.DstNivel2, t03.DstGGX  '
      'FROM vsmovits vmi '
      'LEFT JOIN _t03_ t03 ON t03.DstNivel2=vmi.Controle '
      'WHERE vmi.GraGruX<>t03.DstGGX; ')
    Left = 48
    Top = 356
    object QrRedzConfltDstGGXControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRedzConfltDstGGXGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrRedzConfltDstGGXSrcCtrl: TIntegerField
      FieldName = 'SrcCtrl'
    end
    object QrRedzConfltDstGGXDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
  end
  object DsRedzConfltDstGGX: TDataSource
    DataSet = QrRedzConfltDstGGX
    Left = 48
    Top = 404
  end
  object DsRedzConfltPreClsRcl: TDataSource
    DataSet = QrRedzConfltPreClsRcl
    Left = 52
    Top = 504
  end
  object QrRedzConfltPreClsRcl: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _t03_; '
      'CREATE TABLE _t03_ '
      'SELECT Controle SrcCtrl, MovimID SrcID,'
      'MovimNiv SrcNiv, SrcNivel2, SrcGGX,'
      'Pecas SrcPc  '
      'FROM vsmovits '
      'WHERE SrcNivel2 <> 0'
      'AND MovimID IN (7,8); '
      ' '
      'SELECT vmi.Codigo, vmi.Controle, vmi.MovimID,'
      'vmi.MovimNiv, vmi.GraGruX, vmi.Pecas, t03.*  '
      'FROM vsmovits vmi '
      'LEFT JOIN _t03_ t03 ON t03.SrcNivel2=vmi.Controle '
      'WHERE vmi.GraGruX<>t03.SrcGGX'
      'AND vmi.MovimID=15')
    Left = 52
    Top = 456
    object QrRedzConfltPreClsRclCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRedzConfltPreClsRclControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRedzConfltPreClsRclMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrRedzConfltPreClsRclMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrRedzConfltPreClsRclGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrRedzConfltPreClsRclPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrRedzConfltPreClsRclSrcCtrl: TIntegerField
      FieldName = 'SrcCtrl'
    end
    object QrRedzConfltPreClsRclSrcID: TIntegerField
      FieldName = 'SrcID'
    end
    object QrRedzConfltPreClsRclSrcNiv: TIntegerField
      FieldName = 'SrcNiv'
    end
    object QrRedzConfltPreClsRclSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrRedzConfltPreClsRclSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrRedzConfltPreClsRclSrcPc: TFloatField
      FieldName = 'SrcPc'
    end
  end
  object QrCustosDifSrcDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _rastro_posit_; '
      'CREATE TABLE _rastro_posit_ '
      'SELECT Codigo, Controle, MovimCod, MovimID,  '
      'MovimNiv, Pallet, Pecas, PesoKg, AreaM2, ValorT, '
      'Pecas SdoVrtPeca, AreaM2 SdoVrtArM2,  '
      'SrcMovID, SrcNivel1, SrcNivel2, '
      'DstMovID, DstNivel1, DstNivel2 '
      'FROM bluederm.vsmovits '
      'WHERE Empresa=-11 '
      'AND GraGruX=1 '
      'AND (Pecas>0 OR PesoKg>0 OR AreaM2>0) '
      'AND (NOT (MovimID IN (9,12,17,41))); '
      ' '
      'DROP TABLE IF EXISTS _rastro_negat_;  '
      'CREATE TABLE _rastro_negat_  '
      'SELECT Codigo, Controle, MovimCod, MovimID,   '
      'MovimNiv, Pecas, PesoKg, AreaM2, ValorT,  '
      '0.000 SdoVrtPeca, 0.00 SdoVrtArM2,   '
      'SrcMovID, SrcNivel1, SrcNivel2,  '
      'DstMovID, DstNivel1, DstNivel2  '
      'FROM bluederm.vsmovits '
      'WHERE (  '
      '  (Pecas < 0)  '
      '  OR  '
      '  (AreaM2 < 0)   '
      '  OR  '
      '  (pesoKg < 0)  '
      '  OR   '
      '  (MovimID IN (9,12,17,41))  '
      ')  '
      'AND GraGruX=1;  '
      ' '
      'DROP TABLE IF EXISTS _rastro_difer_;  '
      'CREATE TABLE _rastro_difer_  '
      'SELECT rp.Controle RpControle, '
      'IF(rp.AreaM2 <> 0, "RpCusArM2", "RpCusPeso") RpTpCalc, '
      'IF(rp.AreaM2 <> 0, rp.ValorT/rp.AreaM2,  '
      '  IF(rp.PesoKg <> 0, rp.ValorT/rp.PesoKg, 0.00)) RpCusValr, '
      'IF(rp.AreaM2 <> 0, rn.ValorT/rn.AreaM2,  '
      '  IF(rn.PesoKg <> 0, rn.ValorT/rn.PesoKg, 0.00)) RnCusValr, '
      'rn.Controle RnControle, rn.MovimID RnMovimID, '
      'IF(rp.AreaM2 <> 0, rn.AreaM2,  '
      '  IF(rn.PesoKg <> 0, rn.PesoKg, 0.00)) RnCusQtde '
      '  '
      'FROM _rastro_negat_ rn '
      'LEFT JOIN _rastro_posit_  rp ON rp.Controle=rn.SrcNivel2; '
      ' '
      'SELECT rd.*,  '
      'IF(rd.RnCusValr > rd.RpCusValr, rd.RnCusValr / rd.RpCusValr, '
      '  rd.RpCusValr / rd.RnCusValr) * 100 Perc '
      'FROM _rastro_difer_ rd '
      'WHERE ROUND(rd.RnCusValr, 2) <> ROUND(rd.RpCusValr, 2)  '
      'AND RnCusQtde <> 0 '
      
        'AND ABS(rd.RnCusValr - rd.RpCusValr) * ABS(rd.RnCusQtde) >= 0.01' +
        ' ')
    Left = 52
    Top = 556
    object QrCustosDifSrcDstRpNO_TTW: TWideStringField
      FieldName = 'RpNO_TTW'
      Size = 6
    end
    object QrCustosDifSrcDstRpID_TTW: TLargeintField
      FieldName = 'RpID_TTW'
    end
    object QrCustosDifSrcDstRpControle: TIntegerField
      FieldName = 'RpControle'
    end
    object QrCustosDifSrcDstRpTpCalc: TWideStringField
      FieldName = 'RpTpCalc'
      Required = True
      Size = 9
    end
    object QrCustosDifSrcDstRpCusValr: TFloatField
      FieldName = 'RpCusValr'
      DisplayFormat = '#,###,##0.000'
    end
    object QrCustosDifSrcDstRnCusValr: TFloatField
      FieldName = 'RnCusValr'
      DisplayFormat = '#,###,##0.000'
    end
    object QrCustosDifSrcDstRnControle: TIntegerField
      FieldName = 'RnControle'
      Required = True
    end
    object QrCustosDifSrcDstRnMovimID: TIntegerField
      FieldName = 'RnMovimID'
      Required = True
    end
    object QrCustosDifSrcDstRnCusQtde: TFloatField
      FieldName = 'RnCusQtde'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrCustosDifSrcDstPerc: TFloatField
      FieldName = 'Perc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCustosDifSrcDstRnNO_TTW: TWideStringField
      FieldName = 'RnNO_TTW'
      Size = 6
    end
    object QrCustosDifSrcDstRnID_TTW: TLargeintField
      FieldName = 'RnID_TTW'
    end
    object QrCustosDifSrcDstValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrCustosDifSrcDstValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrCustosDifSrcDstCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrCustosDifSrcDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
  end
  object DsCustosDifSrcDst: TDataSource
    DataSet = QrCustosDifSrcDst
    Left = 52
    Top = 604
  end
  object QrCustosDifSdoVr2: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _t05a_;'
      'CREATE TABLE _t05a_'
      'SELECT Controle, Pallet, Pecas Pecas_A, '
      'PesoKg PesoKg_A, AreaM2 AreaM2_A, '
      'ValorT ValorT_A, SdoVrtPeca, '
      'SdoVrtPeso, SdoVrtArM2,'
      ''
      'IF(AreaM2 > 0, ValorT / AreaM2,  '
      '  IF(PesoKg > 0, ValorT / PesoKg,  '
      '0)) Preco_A, '
      ''
      'IF(AreaM2 > 0, ValorT / AreaM2 * SdoVrtArM2,  '
      '  IF(PesoKg > 0, ValorT / PesoKg * SdoVrtPeso,  '
      '0)) SdoVrtValT_A '
      ''
      'FROM bluederm.vsmovits vmi '
      'WHERE vmi.GraGruX=1'
      'AND (Pecas>0 OR PesoKg>0 OR AreaM2>0)'
      'AND (NOT (MovimID IN (9,12,17,41)))'
      ';'
      ''
      'DROP TABLE IF EXISTS _t05b_;'
      'CREATE TABLE _t05b_'
      'SELECT SrcNivel2, SUM(Pecas) Pecas_B, '
      'SUM(PesoKg) PesoKg_B, SUM(AreaM2) AreaM2_B, '
      'SUM(ValorT) ValorT_B '
      'FROM bluederm.vsmovits vmi '
      'WHERE SrcNivel2 IN ('
      '  SELECT Controle FROM _t05a_'
      ') '
      'GROUP BY SrcNivel2;'
      ''
      'SELECT t05a.*, t05b.*, t05a.ValorT_A + t05b.ValorT_B DifVal'
      'FROM _t05a_ t05a'
      'LEFT JOIN _t05b_ t05b ON t05b.SrcNivel2=t05a.Controle'
      'WHERE ABS((t05a.ValorT_A - t05a.SdoVrtValT_A) '
      '  + t05b.ValorT_B) > 0.01')
    Left = 52
    Top = 652
    object QrCustosDifSdoVr2Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCustosDifSdoVr2Pallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrCustosDifSdoVr2Pecas_A: TFloatField
      FieldName = 'Pecas_A'
      Required = True
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrCustosDifSdoVr2PesoKg_A: TFloatField
      FieldName = 'PesoKg_A'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrCustosDifSdoVr2AreaM2_A: TFloatField
      CustomConstraint = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'AreaM2_A'
      Required = True
    end
    object QrCustosDifSdoVr2ValorT_A: TFloatField
      CustomConstraint = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'ValorT_A'
      Required = True
    end
    object QrCustosDifSdoVr2SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Required = True
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrCustosDifSdoVr2SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrCustosDifSdoVr2SdoVrtArM2: TFloatField
      CustomConstraint = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'SdoVrtArM2'
      Required = True
    end
    object QrCustosDifSdoVr2CustoUnit_A: TFloatField
      FieldName = 'CustoUnit_A'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrCustosDifSdoVr2SdoVrtValT_A: TFloatField
      FieldName = 'SdoVrtValT_A'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrCustosDifSdoVr2SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrCustosDifSdoVr2Pecas_B: TFloatField
      FieldName = 'Pecas_B'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrCustosDifSdoVr2PesoKg_B: TFloatField
      FieldName = 'PesoKg_B'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrCustosDifSdoVr2AreaM2_B: TFloatField
      CustomConstraint = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'AreaM2_B'
    end
    object QrCustosDifSdoVr2ValorT_B: TFloatField
      CustomConstraint = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'ValorT_B'
    end
    object QrCustosDifSdoVr2DifVal: TFloatField
      CustomConstraint = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'DifVal'
    end
    object QrCustosDifSdoVr2NO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Size = 6
    end
    object QrCustosDifSdoVr2ID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrCustosDifSdoVr2MovimID_A: TIntegerField
      FieldName = 'MovimID_A'
    end
    object QrCustosDifSdoVr2MovimNiv_A: TIntegerField
      FieldName = 'MovimNiv_A'
    end
    object QrCustosDifSdoVr2DifKg: TFloatField
      FieldName = 'DifKg'
    end
    object QrCustosDifSdoVr2DifPc: TFloatField
      FieldName = 'DifPc'
    end
    object QrCustosDifSdoVr2DifM2: TFloatField
      FieldName = 'DifM2'
    end
    object QrCustosDifSdoVr2FatorLimitante: TFloatField
      FieldName = 'FatorLimitante'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object DsCustosDifSdoVr2: TDataSource
    DataSet = QrCustosDifSdoVr2
    Left = 52
    Top = 704
  end
  object QrGroupConcat: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 192
    Top = 352
  end
  object QrFldPosNegMesmoReg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle, MovimCod, MovimID,  '
      'MovimNiv, Pallet, Pecas, PesoKg, AreaM2, ValorT, '
      'SdoVrtPeca, SdoVrtPeso, SdoVrtArM2,  '
      'SrcMovID, SrcNivel1, SrcNivel2, '
      'DstMovID, DstNivel1, DstNivel2 '
      'FROM vsmovits'
      'WHERE GraGruX = 1 '
      'AND ( '
      '(pecas > 0 AND (PesoKg < 0 OR AreaM2 <0)) '
      'OR '
      '(PesoKg > 0 AND (Pecas < 0 OR AreaM2 <0)) '
      'OR '
      '(AreaM2 > 0 AND (PesoKg < 0 OR AreaM2 <0)) '
      ') ')
    Left = 192
    Top = 404
    object QrFldPosNegMesmoRegCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFldPosNegMesmoRegControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFldPosNegMesmoRegMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrFldPosNegMesmoRegMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrFldPosNegMesmoRegMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrFldPosNegMesmoRegPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrFldPosNegMesmoRegPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrFldPosNegMesmoRegPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrFldPosNegMesmoRegAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrFldPosNegMesmoRegValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrFldPosNegMesmoRegSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Required = True
    end
    object QrFldPosNegMesmoRegSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      Required = True
    end
    object QrFldPosNegMesmoRegSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      Required = True
    end
    object QrFldPosNegMesmoRegSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrFldPosNegMesmoRegSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrFldPosNegMesmoRegSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrFldPosNegMesmoRegDstMovID: TIntegerField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrFldPosNegMesmoRegDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrFldPosNegMesmoRegDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
      Required = True
    end
  end
  object DsFldPosNegMesmoReg: TDataSource
    DataSet = QrFldPosNegMesmoReg
    Left = 192
    Top = 456
  end
  object QrRedConfltGerA: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _ger_confl_13_; '
      'CREATE TABLE _ger_confl_13_ '
      'SELECT Codigo, MovimCod, GraGruX '
      'FROM bluederm_cia2.vsmovits '
      'WHERE MovimNiv=13 '
      '; '
      'DROP TABLE IF EXISTS _ger_confl_15_; '
      'CREATE TABLE _ger_confl_15_ '
      'SELECT Controle, DstNivel2, Codigo, MovimCod, DstGGX '
      'FROM bluederm_cia2.vsmovits '
      'WHERE MovimNiv=15 '
      '; '
      ' '
      'DROP TABLE IF EXISTS _ger_confl_er_; '
      'CREATE TABLE _ger_confl_er_ '
      'SELECT _13.Codigo, _13.MovimCod, _13.GraGruX, '
      '_15.DstGGX, _15.Controle, _15.DstNivel2  '
      'FROM _ger_confl_13_ _13 '
      'LEFT JOIN _ger_confl_15_ _15 ON _13.Codigo=_15.Codigo '
      'AND _13.GraGruX <> _15.DstGGX '
      '; '
      'SELECT *  '
      'FROM _ger_confl_er_ '
      'WHERE GraGruX <> DstGGX '
      'AND DstGGX <> 0 ')
    Left = 192
    Top = 508
    object QrRedConfltGerACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRedConfltGerAMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrRedConfltGerAGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrRedConfltGerADstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrRedConfltGerAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrRedConfltGerADstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
  end
  object DsRedConfltGerA: TDataSource
    DataSet = QrRedConfltGerA
    Left = 192
    Top = 556
  end
  object QrRedConfltBaixa: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _vs_err_ggx_bxa_;'
      'CREATE TABLE _vs_err_ggx_bxa_'
      'SELECT Codigo, MovimCod, '
      'Controle, MovimID, MovimNiv,'
      'GraGruX, SrcGGX, SrcNivel2'
      'FROM bluederm_cia2.vsmovits'
      'WHERE Pecas < 0'
      'AND GraGruX=118'
      ';'
      'DROP TABLE IF EXISTS _vs_err_ggx_ori_;'
      'CREATE TABLE _vs_err_ggx_ori_'
      'SELECT Codigo, MovimCod, Controle,'
      'MovimID, MovimNiv, GraGruX'
      'FROM bluederm_cia2.vsmovits'
      'WHERE Controle IN ('
      '  SELECT SrcNivel2'
      '  FROM _vs_err_ggx_bxa_'
      ')'
      ';'
      'SELECT bxa.MovimCod MovimCod_Bxa, '
      'bxa.Controle Controle_Bxa, '
      'bxa.MovimID MovimID_Bxa,'
      'bxa.GraGruX GraGruX_Bxa, '
      'bxa.SrcGGX SrcGGX_Bxa,'
      'ori.MovimCod MovimCod_Ori, '
      'ori.Controle Controle_Ori,'
      'ori.MovimID MovimID_Ori,'
      'ori.GraGruX GraGruX_Ori'
      'FROM _vs_err_ggx_bxa_ bxa'
      'LEFT JOIN _vs_err_ggx_ori_ ori ON ori.Controle=bxa.SrcNivel2'
      'WHERE ori.GraGruX<>bxa.GraGruX'
      ''
      ''
      ''
      ' ')
    Left = 192
    Top = 608
    object QrRedConfltBaixaMovimCod_Bxa: TIntegerField
      FieldName = 'MovimCod_Bxa'
      Required = True
    end
    object QrRedConfltBaixaControle_Bxa: TIntegerField
      FieldName = 'Controle_Bxa'
      Required = True
    end
    object QrRedConfltBaixaMovimID_Bxa: TIntegerField
      FieldName = 'MovimID_Bxa'
      Required = True
    end
    object QrRedConfltBaixaGraGruX_Bxa: TIntegerField
      FieldName = 'GraGruX_Bxa'
      Required = True
    end
    object QrRedConfltBaixaSrcGGX_Bxa: TIntegerField
      FieldName = 'SrcGGX_Bxa'
      Required = True
    end
    object QrRedConfltBaixaMovimCod_Ori: TIntegerField
      FieldName = 'MovimCod_Ori'
    end
    object QrRedConfltBaixaControle_Ori: TIntegerField
      FieldName = 'Controle_Ori'
    end
    object QrRedConfltBaixaMovimID_Ori: TIntegerField
      FieldName = 'MovimID_Ori'
    end
    object QrRedConfltBaixaGraGruX_Ori: TIntegerField
      FieldName = 'GraGruX_Ori'
    end
  end
  object DsRedConfltBaixa: TDataSource
    DataSet = QrRedConfltBaixa
    Left = 192
    Top = 656
  end
  object QrCustosDifPreRecl: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _fix_recl_15_12_;'
      'CREATE TABLE _fix_recl_15_12_'
      'SELECT Codigo, MovimCod, Controle, ValorT'
      'FROM bluederm.vsmovits'
      'WHERE MovimID = 15'
      'AND MovimNiv = 12'
      'AND Empresa=-11'
      'AND GraGruX=208;'
      ''
      ''
      'DROP TABLE IF EXISTS _fix_recl_15_11_;'
      'CREATE TABLE _fix_recl_15_11_'
      'SELECT DstNivel2, ABS(SUM(ValorT)) ValorT'
      'FROM bluederm.vsmovits'
      'WHERE MovimID = 15'
      'AND MovimNiv = 11'
      'GROUP BY DstNivel2;'
      ''
      'SELECT src.DstNivel2, src.ValorT '
      'FROM _fix_recl_15_12_ dst'
      'LEFT JOIN _fix_recl_15_11_ src ON src.DstNivel2=dst.Controle'
      'WHERE dst.ValorT <> src.ValorT'
      'ORDER BY DstNivel2;')
    Left = 324
    Top = 352
    object QrCustosDifPreReclDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrCustosDifPreReclValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrCustosDifPreReclID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object DsCustosDifPreRecl: TDataSource
    DataSet = QrCustosDifPreRecl
    Left = 324
    Top = 400
  end
  object QrCustosDifReclasse: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _dif_custo_twn_1_;'
      'CREATE TABLE _dif_custo_twn_1_'
      'SELECT DataHora, MovimTwn MovimTwn_1, '
      'ValorT ValorT_1, AreaM2 AreaM2_1'
      'FROM bluederm.vsmovits'
      'WHERE MovimTwn>0'
      'AND MovimNiv=1'
      'AND Empresa=-11'
      'AND DstGGX=208'
      ';'
      ''
      'DROP TABLE IF EXISTS _dif_custo_twn_2_;'
      'CREATE TABLE _dif_custo_twn_2_'
      'SELECT MovimTwn MovimTwn_2, Controle Controle_2, '
      'ValorT ValorT_2, AreaM2 AreaM2_2'
      'FROM bluederm.vsmovits'
      'WHERE MovimTwn>0'
      'AND Empresa=-11'
      'AND GraGruX=208'
      'AND MovimNiv=2;'
      ''
      ''
      'SELECT twn1.*, twn2.*'
      'FROM _dif_custo_twn_1_ twn1'
      
        'LEFT JOIN _dif_custo_twn_2_ twn2 ON twn2.MovimTwn_2=twn1.MovimTw' +
        'n_1'
      'WHERE ABS(twn1.ValorT_1) <> ABS(twn2.ValorT_2)')
    Left = 324
    Top = 456
    object QrCustosDifReclasseDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrCustosDifReclasseMovimTwn_1: TIntegerField
      FieldName = 'MovimTwn_1'
      Required = True
    end
    object QrCustosDifReclasseValorT_1: TFloatField
      FieldName = 'ValorT_1'
      Required = True
    end
    object QrCustosDifReclasseAreaM2_1: TFloatField
      FieldName = 'AreaM2_1'
      Required = True
    end
    object QrCustosDifReclasseMovimTwn_2: TIntegerField
      FieldName = 'MovimTwn_2'
    end
    object QrCustosDifReclasseControle_2: TIntegerField
      FieldName = 'Controle_2'
    end
    object QrCustosDifReclasseValorT_2: TFloatField
      FieldName = 'ValorT_2'
    end
    object QrCustosDifReclasseAreaM2_2: TFloatField
      FieldName = 'AreaM2_2'
    end
    object QrCustosDifReclasseID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object DsCustosDifReclasse: TDataSource
    DataSet = QrCustosDifReclasse
    Left = 324
    Top = 508
  end
  object QrCorreCustosIMECs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.DataHora, vmi.Codigo, vmi.Controle, '
      'vmi.MovimCod, vmi.MovimID, vmi.MovimNiv, '
      'vmi.SrcMovID, vmi.SrcGGX, vmi.SrcNivel2, '
      'vmi.AreaM2, vmi.ValorT,'
      'ABS(vmi.ValorT / vmi.AreaM2) CustoM2'
      'FROM vsmovits vmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'WHERE vmi.AreaM2 <> 0'
      'AND ggx.GraGruY=3072'
      'AND NOT (MovimID IN (41))'
      'AND ABS(vmi.ValorT / vmi.AreaM2) < 13.34'
      'ORDER BY MovimCod, Controle')
    Left = 324
    Top = 560
    object QrCorreCustosIMECsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrCorreCustosIMECsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrCorreCustosIMECsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCorreCustosIMECsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCorreCustosIMECsMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrCorreCustosIMECsMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrCorreCustosIMECsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrCorreCustosIMECsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrCorreCustosIMECsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrCorreCustosIMECsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrCorreCustosIMECsAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrCorreCustosIMECsValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrCorreCustosIMECsCustoM2: TFloatField
      FieldName = 'CustoM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
  end
  object DsCorreCustosIMECs: TDataSource
    DataSet = QrCorreCustosIMECs
    Left = 324
    Top = 612
  end
  object QrVmiPaiDifProcessos: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _corrigir_vmi_pai_a_;'
      'CREATE TABLE _corrigir_vmi_pai_a_'
      'SELECT DISTINCT MovimCod'
      'FROM bluederm_cia2.vsmovits'
      'WHERE MovimID IN (11,19,26,27,29,32,33,38,39)'
      ';'
      'SELECT Codigo, Controle, MovimCod, MovimID, MovimNiv, '
      'SrcMovID, SrcNivel1, SrcNivel2 '
      'FROM bluederm_cia2.vsmovits'
      'WHERE MovimCod IN ('
      '  SELECT MovimCod'
      '  FROM _corrigir_vmi_pai_a_'
      ')  '
      'AND NOT (MovimNiv IN (7,20,27,29,34,39,44,49,54,65))'
      'AND ('
      '  (Pecas < 0)'
      '   OR'
      '  (MovimID =41)'
      ')'
      'AND VmiPai=0')
    Left = 460
    Top = 352
    object QrVmiPaiDifProcessosID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVmiPaiDifProcessosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVmiPaiDifProcessosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrVmiPaiDifProcessosMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVmiPaiDifProcessosMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVmiPaiDifProcessosMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVmiPaiDifProcessosSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVmiPaiDifProcessosSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVmiPaiDifProcessosSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVmiPaiDifProcessosMovCodPai: TIntegerField
      FieldName = 'MovCodPai'
    end
  end
  object DsVmiPaiDifProcessos: TDataSource
    DataSet = QrVmiPaiDifProcessos
    Left = 460
    Top = 400
  end
  object QrCustosDifSdoVr3: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _t05a_;'
      'CREATE TABLE _t05a_'
      'SELECT Controle, Pallet, Pecas Pecas_A, '
      'PesoKg PesoKg_A, AreaM2 AreaM2_A, '
      'ValorT ValorT_A, SdoVrtPeca, '
      'SdoVrtPeso, SdoVrtArM2,'
      ''
      'IF(AreaM2 > 0, ValorT / AreaM2,  '
      '  IF(PesoKg > 0, ValorT / PesoKg,  '
      '0)) Preco_A, '
      ''
      'IF(AreaM2 > 0, ValorT / AreaM2 * SdoVrtArM2,  '
      '  IF(PesoKg > 0, ValorT / PesoKg * SdoVrtPeso,  '
      '0)) SdoVrtValT_A '
      ''
      'FROM bluederm.vsmovits vmi '
      'WHERE vmi.GraGruX=1'
      'AND (Pecas>0 OR PesoKg>0 OR AreaM2>0)'
      'AND (NOT (MovimID IN (9,12,17,41)))'
      ';'
      ''
      'DROP TABLE IF EXISTS _t05b_;'
      'CREATE TABLE _t05b_'
      'SELECT SrcNivel2, SUM(Pecas) Pecas_B, '
      'SUM(PesoKg) PesoKg_B, SUM(AreaM2) AreaM2_B, '
      'SUM(ValorT) ValorT_B '
      'FROM bluederm.vsmovits vmi '
      'WHERE SrcNivel2 IN ('
      '  SELECT Controle FROM _t05a_'
      ') '
      'GROUP BY SrcNivel2;'
      ''
      'SELECT t05a.*, t05b.*, t05a.ValorT_A + t05b.ValorT_B DifVal'
      'FROM _t05a_ t05a'
      'LEFT JOIN _t05b_ t05b ON t05b.SrcNivel2=t05a.Controle'
      'WHERE ABS((t05a.ValorT_A - t05a.SdoVrtValT_A) '
      '  + t05b.ValorT_B) > 0.01')
    Left = 52
    Top = 756
    object QrCustosDifSdoVr3Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCustosDifSdoVr3Pallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrCustosDifSdoVr3Pecas_A: TFloatField
      FieldName = 'Pecas_A'
      Required = True
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrCustosDifSdoVr3PesoKg_A: TFloatField
      FieldName = 'PesoKg_A'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrCustosDifSdoVr3AreaM2_A: TFloatField
      CustomConstraint = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'AreaM2_A'
      Required = True
    end
    object QrCustosDifSdoVr3ValorT_A: TFloatField
      FieldName = 'ValorT_A'
      Required = True
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrCustosDifSdoVr3SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Required = True
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrCustosDifSdoVr3SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrCustosDifSdoVr3SdoVrtArM2: TFloatField
      CustomConstraint = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'SdoVrtArM2'
      Required = True
    end
    object QrCustosDifSdoVr3CustoUnit_A: TFloatField
      FieldName = 'CustoUnit_A'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrCustosDifSdoVr3SdoVrtValT_A: TFloatField
      FieldName = 'SdoVrtValT_A'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrCustosDifSdoVr3SrcVmi: TIntegerField
      FieldName = 'SrcVmi'
    end
    object QrCustosDifSdoVr3Pecas_B: TFloatField
      FieldName = 'Pecas_B'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrCustosDifSdoVr3PesoKg_B: TFloatField
      FieldName = 'PesoKg_B'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrCustosDifSdoVr3AreaM2_B: TFloatField
      CustomConstraint = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'AreaM2_B'
    end
    object QrCustosDifSdoVr3ValorT_B: TFloatField
      FieldName = 'ValorT_B'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrCustosDifSdoVr3DifVal: TFloatField
      FieldName = 'DifVal'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrCustosDifSdoVr3NO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Size = 6
    end
    object QrCustosDifSdoVr3ID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrCustosDifSdoVr3MovimID_A: TIntegerField
      FieldName = 'MovimID_A'
    end
    object QrCustosDifSdoVr3MovimNiv_A: TIntegerField
      FieldName = 'MovimNiv_A'
    end
    object QrCustosDifSdoVr3DifKg: TFloatField
      FieldName = 'DifKg'
    end
    object QrCustosDifSdoVr3DifPc: TFloatField
      FieldName = 'DifPc'
    end
    object QrCustosDifSdoVr3DifM2: TFloatField
      FieldName = 'DifM2'
    end
    object QrCustosDifSdoVr3FatorLimitante: TFloatField
      FieldName = 'FatorLimitante'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object DsCustosDifSdoVr3: TDataSource
    DataSet = QrCustosDifSdoVr3
    Left = 52
    Top = 808
  end
  object QrPcPosNegXValTNegPos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle,'
      'MovimCod, MovimID, MovimNiv,'
      'Pecas, PesoKg, AreaM2, ValorT'
      'FROM vsmovits'
      'WHERE ('
      '  (Pecas < 0 AND ((PesoKg<0) OR (AreaM2<0)) AND ValorT > 0 )'
      '   OR'
      '  (Pecas > 0 AND ((PesoKg>0) OR (AreaM2>0)) AND ValorT < 0 )'
      ')'
      'AND NOT (MovimID IN (9,12,17,41))'
      ''
      'ORDER BY Controle'
      '')
    Left = 460
    Top = 452
    object QrPcPosNegXValTNegPosID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrPcPosNegXValTNegPosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPcPosNegXValTNegPosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPcPosNegXValTNegPosMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrPcPosNegXValTNegPosMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrPcPosNegXValTNegPosMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrPcPosNegXValTNegPosPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrPcPosNegXValTNegPosPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrPcPosNegXValTNegPosAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrPcPosNegXValTNegPosValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
  end
  object DsPcPosNegXValTNegPos: TDataSource
    DataSet = QrPcPosNegXValTNegPos
    Left = 460
    Top = 500
  end
  object QrVlrPosNegSameReg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, MovimID, MovimNiv, '
      'ValorMP, ValorT, CustoMOTot, CusFrtAvuls,'
      'ValorT-ValorMP Diferenca'
      'FROM vsmovits'
      'WHERE ValorMP <> 0'
      'AND ValorT <> 0'
      'AND ('
      ' ('
      '  ValorMP < 0 '
      '  AND '
      '  ValorT > 0'
      ' )'
      ' OR'
      ' ('
      '  ValorMP > 0 '
      '  AND '
      '  ValorT < 0'
      ' )'
      ')')
    Left = 460
    Top = 552
    object QrVlrPosNegSameRegID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVlrPosNegSameRegControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrVlrPosNegSameRegMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVlrPosNegSameRegMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVlrPosNegSameRegValorMP: TFloatField
      FieldName = 'ValorMP'
      Required = True
    end
    object QrVlrPosNegSameRegValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrVlrPosNegSameRegCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      Required = True
    end
    object QrVlrPosNegSameRegCusFrtAvuls: TFloatField
      FieldName = 'CusFrtAvuls'
      Required = True
    end
    object QrVlrPosNegSameRegDiferenca: TFloatField
      FieldName = 'Diferenca'
      Required = True
    end
  end
  object DsVlrPosNegSameReg: TDataSource
    DataSet = QrVlrPosNegSameReg
    Left = 460
    Top = 604
  end
  object QrVsSifDipoa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vssifdipoa '
      'WHERE Empresa=-11')
    Left = 812
    Top = 4
    object QrVsSifDipoaEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVsSifDipoaRegistro: TWideStringField
      FieldName = 'Registro'
    end
    object QrVsSifDipoaDdValid: TIntegerField
      FieldName = 'DdValid'
      Required = True
    end
    object QrVsSifDipoaArqLogoFilial: TWideStringField
      FieldName = 'ArqLogoFilial'
      Size = 511
    end
    object QrVsSifDipoaArqLogoSIF: TWideStringField
      FieldName = 'ArqLogoSIF'
      Size = 511
    end
    object QrVsSifDipoaQualDtFab: TSmallintField
      FieldName = 'QualDtFab'
    end
  end
  object QrAux: TMySQLQuery
    Database = Dmod.MyDB
    Left = 852
    Top = 612
  end
end
