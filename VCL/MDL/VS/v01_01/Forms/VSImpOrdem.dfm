object FmVSImpOrdem: TFmVSImpOrdem
  Left = 0
  Top = 0
  Caption = 'WET-CURTI-183 :: Impress'#227'o de Ordem de Produ'#231#227'o'
  ClientHeight = 503
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  PixelsPerInch = 96
  TextHeight = 13
  object QrVSOrdem: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVSOrdemCalcFields
    SQL.Strings = (
      'SELECT vcc.Nome NO_VSCOPCab,  '
      'IF(voc.PecasMan<>0, voc.PecasMan, -voc.PecasSrc) PecasINI, '
      'IF(voc.AreaManM2<>0, voc.AreaManM2, -voc.AreaSrcM2) AreaINIM2, '
      'IF(voc.AreaManP2<>0, voc.AreaManP2, -voc.AreaSrcM2) AreaINIP2, '
      'IF(voc.PesoKgMan<>0, voc.PesoKgMan, -voc.PesoKgSrc) PesoKgINI, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,  '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente,  '
      'ope.Nome NO_Operacoes, egr.Nome NO_EmitGru,  '
      'voc.* '
      'FROM vspwecab voc '
      'LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa '
      'LEFT JOIN entidades cli ON cli.Codigo=voc.Cliente '
      'LEFT JOIN vscopcab  vcc ON vcc.Codigo=voc.VSCOPCab '
      'LEFT JOIN emitgru   egr ON egr.Codigo=voc.EmitGru '
      'LEFT JOIN operacoes ope ON ope.Codigo=voc.Operacoes '
      'WHERE voc.Codigo > 0 ')
    Left = 32
    Top = 9
    object QrVSOrdemNO_VSCOPCab: TWideStringField
      FieldName = 'NO_VSCOPCab'
      Size = 100
    end
    object QrVSOrdemPecasINI: TFloatField
      FieldName = 'PecasINI'
      Required = True
    end
    object QrVSOrdemAreaINIM2: TFloatField
      FieldName = 'AreaINIM2'
      Required = True
    end
    object QrVSOrdemAreaINIP2: TFloatField
      FieldName = 'AreaINIP2'
      Required = True
    end
    object QrVSOrdemPesoKgINI: TFloatField
      FieldName = 'PesoKgINI'
      Required = True
    end
    object QrVSOrdemNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSOrdemNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrVSOrdemNO_Operacoes: TWideStringField
      FieldName = 'NO_Operacoes'
      Size = 30
    end
    object QrVSOrdemNO_EmitGru: TWideStringField
      FieldName = 'NO_EmitGru'
      Size = 60
    end
    object QrVSOrdemCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSOrdemMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSOrdemCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSOrdemGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSOrdemNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSOrdemEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSOrdemDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
    end
    object QrVSOrdemDtHrLibOpe: TDateTimeField
      FieldName = 'DtHrLibOpe'
    end
    object QrVSOrdemDtHrCfgOpe: TDateTimeField
      FieldName = 'DtHrCfgOpe'
    end
    object QrVSOrdemDtHrFimOpe: TDateTimeField
      FieldName = 'DtHrFimOpe'
    end
    object QrVSOrdemPesoKgMan: TFloatField
      FieldName = 'PesoKgMan'
    end
    object QrVSOrdemPecasMan: TFloatField
      FieldName = 'PecasMan'
    end
    object QrVSOrdemAreaManM2: TFloatField
      FieldName = 'AreaManM2'
    end
    object QrVSOrdemAreaManP2: TFloatField
      FieldName = 'AreaManP2'
    end
    object QrVSOrdemValorTMan: TFloatField
      FieldName = 'ValorTMan'
    end
    object QrVSOrdemPesoKgSrc: TFloatField
      FieldName = 'PesoKgSrc'
    end
    object QrVSOrdemPecasSrc: TFloatField
      FieldName = 'PecasSrc'
    end
    object QrVSOrdemAreaSrcM2: TFloatField
      FieldName = 'AreaSrcM2'
    end
    object QrVSOrdemAreaSrcP2: TFloatField
      FieldName = 'AreaSrcP2'
    end
    object QrVSOrdemValorTSrc: TFloatField
      FieldName = 'ValorTSrc'
    end
    object QrVSOrdemPesoKgDst: TFloatField
      FieldName = 'PesoKgDst'
    end
    object QrVSOrdemPecasDst: TFloatField
      FieldName = 'PecasDst'
    end
    object QrVSOrdemAreaDstM2: TFloatField
      FieldName = 'AreaDstM2'
    end
    object QrVSOrdemAreaDstP2: TFloatField
      FieldName = 'AreaDstP2'
    end
    object QrVSOrdemValorTDst: TFloatField
      FieldName = 'ValorTDst'
    end
    object QrVSOrdemPesoKgBxa: TFloatField
      FieldName = 'PesoKgBxa'
    end
    object QrVSOrdemPecasBxa: TFloatField
      FieldName = 'PecasBxa'
    end
    object QrVSOrdemAreaBxaM2: TFloatField
      FieldName = 'AreaBxaM2'
    end
    object QrVSOrdemAreaBxaP2: TFloatField
      FieldName = 'AreaBxaP2'
    end
    object QrVSOrdemValorTBxa: TFloatField
      FieldName = 'ValorTBxa'
    end
    object QrVSOrdemPesoKgSdo: TFloatField
      FieldName = 'PesoKgSdo'
    end
    object QrVSOrdemPecasSdo: TFloatField
      FieldName = 'PecasSdo'
    end
    object QrVSOrdemAreaSdoM2: TFloatField
      FieldName = 'AreaSdoM2'
    end
    object QrVSOrdemAreaSdoP2: TFloatField
      FieldName = 'AreaSdoP2'
    end
    object QrVSOrdemValorTSdo: TFloatField
      FieldName = 'ValorTSdo'
    end
    object QrVSOrdemTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSOrdemCustoManMOKg: TFloatField
      FieldName = 'CustoManMOKg'
    end
    object QrVSOrdemCustoManMOTot: TFloatField
      FieldName = 'CustoManMOTot'
    end
    object QrVSOrdemValorManMP: TFloatField
      FieldName = 'ValorManMP'
    end
    object QrVSOrdemValorManT: TFloatField
      FieldName = 'ValorManT'
    end
    object QrVSOrdemCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSOrdemNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrVSOrdemLPFMO: TWideStringField
      FieldName = 'LPFMO'
      Size = 30
    end
    object QrVSOrdemTemIMEIMrt: TSmallintField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSOrdemKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
    end
    object QrVSOrdemGGXDst: TIntegerField
      FieldName = 'GGXDst'
    end
    object QrVSOrdemNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
    end
    object QrVSOrdemGGXSrc: TIntegerField
      FieldName = 'GGXSrc'
    end
    object QrVSOrdemSerieRem: TIntegerField
      FieldName = 'SerieRem'
    end
    object QrVSOrdemOperacoes: TIntegerField
      FieldName = 'Operacoes'
    end
    object QrVSOrdemVSCOPCab: TIntegerField
      FieldName = 'VSCOPCab'
    end
    object QrVSOrdemEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrVSOrdemNO_ACAO_COURO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ACAO_COURO'
      Size = 100
      Calculated = True
    end
  end
  object QrVSOri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 30
    Top = 105
    object QrVSOriCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSOriControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSOriMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSOriMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSOriMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSOriEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSOriTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSOriCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSOriMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSOriDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSOriPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSOriGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSOriPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSOriPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSOriAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSOriSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSOriSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSOriSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSOriSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSOriSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSOriSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSOriSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSOriFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSOriMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSOriFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSOriCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSOriDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSOriDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSOriDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSOriQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSOriQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSOriQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSOriQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSOriQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSOriNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSOriNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSOriNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSOriNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSOriID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSOriNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSOriNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSOriReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object QrVSDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 32
    Top = 201
    object QrVSDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSDstNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrVSDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSDstNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
  end
  object frxWET_CURTI_183_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41905.510812419000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxWET_CURTI_183_01GetValue
    Left = 256
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVSAtu
        DataSetName = 'frxDsVSAtu'
      end
      item
        DataSet = frxDsVSDst
        DataSetName = 'frxDsVSDst'
      end
      item
        DataSet = frxDsVSOrdem
        DataSetName = 'frxDsVSOrdem'
      end
      item
        DataSet = frxDsVSOri
        DataSetName = 'frxDsVSOri'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 344.039350560000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDsVSOrdem
        DataSetName = 'frxDsVSOrdem'
        RowCount = 0
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 26.559060000000000000
          Width = 415.748300000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Ordem de Produ'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 30.338590000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 30.338590000000000000
          Width = 177.637846540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Item [Line#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 20.015770000000000000
          Top = 56.984230000000000000
          Width = 222.991830630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#176' [frxDsVSOrdem."Codigo"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 1.118120000000000000
          Top = 98.267699450000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 204.094463780000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976377950000000000
          Top = 204.094463780000000000
          Width = 302.362204720000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NFe Remessa MO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 135.866110000000000000
          Width = 26.456631890000000000
          Height = 68.031496062992100000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 302.464554720000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 151.180870470000000000
          Top = 302.464554720000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590490160000000000
          Top = 302.464554720000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Top = 230.551181100000000000
          Width = 188.976377950000000000
          Height = 52.913395590000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOrdem."DtHrAberto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976377950000000000
          Top = 230.551181100000000000
          Width = 302.362204720000000000
          Height = 52.913395590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOrdem."NFeRem"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456631890000000000
          Top = 135.866110000000000000
          Width = 653.858328740000000000
          Height = 68.031496060000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -29
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSAtu."GraGruX"] - [frxDsVSAtu."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Top = 321.362182760000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOrdem."PecasINI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 151.180870470000000000
          Top = 321.362182760000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOrdem."PesoKgINI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590490160000000000
          Top = 321.362182760000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOrdem."AreaINIM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267701890000000000
          Top = 98.267699450000000000
          Width = 582.047258740000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSOrdem."NO_EMPRESA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Top = 98.267699450000000000
          Width = 98.267701890000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente Interno: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Top = 19.000000000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 53.015770000000000000
          Width = 411.968330630000000000
          Height = 41.779530000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'IME-C: [frxDsVSOrdem."MovimCod"] [frxDsVSOrdem."NO_ACAO_COURO"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 204.094463780000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '-------')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 230.551181100000000000
          Width = 188.976377950000000000
          Height = 52.913395590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 283.464750000000000000
          Width = 226.771751180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Total origem')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 302.464554720000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 377.952670470000000000
          Top = 302.464554720000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362290160000000000
          Top = 302.464554720000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 321.362182760000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOrdem."PecasSdo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 377.952670470000000000
          Top = 321.362182760000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOrdem."PesoKgSdo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362290160000000000
          Top = 321.362182760000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOrdem."AreaSdoM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 283.464750000000000000
          Width = 226.771751180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo n'#227'o baixado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 302.464554720000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724470470000000000
          Top = 302.464554720000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134090160000000000
          Top = 302.464554720000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 321.362182760000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOrdem."PecasDst"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724470470000000000
          Top = 321.362182760000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOrdem."PesoKgDst"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134090160000000000
          Top = 321.362182760000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOrdem."AreaDstM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 283.464750000000000000
          Width = 226.771751180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Total gerado')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 35.905526460000000000
        Top = 385.512060000000000000
        Width = 680.315400000000000000
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Top = 22.677180000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'RME')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 22.677180000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 22.677180000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 192.756030000000000000
          Top = 22.677180000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Top = 22.677180000000000000
          Width = 230.551281180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo / tamanho / cor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 22.677180000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724470470000000000
          Top = 22.677180000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134090160000000000
          Top = 22.677180000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315277950000000000
          Height = 22.677162910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Origem')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228368420000000000
        Top = 445.984540000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSOri
        DataSetName = 'frxDsVSOri'
        RowCount = 0
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Top = 0.000021959999999988
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataField = 'DataHora'
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOri."DataHora"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Top = 0.000021959999999988
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'ReqMovEstq'
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOri."ReqMovEstq"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 0.000021959999999988
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOri."Controle"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 0.000021959999999988
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOri."Pallet"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 192.756030000000000000
          Top = 0.000021959999999988
          Width = 30.236220472440940000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOri."GraGruX"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Top = 0.000021959999999988
          Width = 230.551281180000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSOri."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'Pecas'
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOri."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724470470000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOri."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134090160000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOri."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSOri."Pecas">,DetailData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724470470000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSOri."PesoKg">,DetailData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134090160000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSOri."AreaM2">,DetailData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 35.905526460000000000
        Top = 521.575140000000000000
        Width = 680.315400000000000000
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 22.677180000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'RME')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165430000000000000
          Top = 22.677180000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 22.677180000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 22.677180000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Top = 22.677180000000000000
          Width = 222.992221180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Artigo / tamanho / cor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 22.677180000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724470470000000000
          Top = 22.677180000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134090160000000000
          Top = 22.677180000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315277950000000000
          Height = 22.677162910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Destino')
          ParentFont = False
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 619.842920000000000000
        Width = 680.315400000000000000
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSDst."Pecas">,DetailData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724470470000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSDst."PesoKg">,DetailData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134090160000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataSet = frxDsVSOri
          DataSetName = 'frxDsVSOri'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSDst."AreaM2">,DetailData2)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 582.047620000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSDst
        DataSetName = 'frxDsVSDst'
        RowCount = 0
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Top = 0.000021959999999988
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'DataHora'
          DataSet = frxDsVSDst
          DataSetName = 'frxDsVSDst'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSDst."DataHora"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 0.000021959999999988
          Width = 41.574803149606300000
          Height = 13.228346460000000000
          DataField = 'ReqMovEstq'
          DataSet = frxDsVSDst
          DataSetName = 'frxDsVSDst'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSDst."ReqMovEstq"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165430000000000000
          Top = 0.000021959999999988
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsVSDst
          DataSetName = 'frxDsVSDst'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSDst."Controle"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 0.000021959999999988
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'Pallet'
          DataSet = frxDsVSDst
          DataSetName = 'frxDsVSDst'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSDst."Pallet"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 0.000021959999999988
          Width = 30.236191180000000000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsVSDst
          DataSetName = 'frxDsVSDst'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSDst."GraGruX"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Top = 0.000021959999999988
          Width = 222.992221180000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSDst
          DataSetName = 'frxDsVSDst'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsVSDst."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'Pecas'
          DataSet = frxDsVSDst
          DataSetName = 'frxDsVSDst'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSDst."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724470470000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsVSDst
          DataSetName = 'frxDsVSDst'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSDst."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134090160000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsVSDst
          DataSetName = 'frxDsVSDst'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSDst."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsVSOrdem: TfrxDBDataset
    UserName = 'frxDsVSOrdem'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_VSCOPCab=NO_VSCOPCab'
      'PecasINI=PecasINI'
      'AreaINIM2=AreaINIM2'
      'AreaINIP2=AreaINIP2'
      'PesoKgINI=PesoKgINI'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_Cliente=NO_Cliente'
      'NO_Operacoes=NO_Operacoes'
      'NO_EmitGru=NO_EmitGru'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'CacCod=CacCod'
      'GraGruX=GraGruX'
      'Nome=Nome'
      'Empresa=Empresa'
      'DtHrAberto=DtHrAberto'
      'DtHrLibOpe=DtHrLibOpe'
      'DtHrCfgOpe=DtHrCfgOpe'
      'DtHrFimOpe=DtHrFimOpe'
      'PesoKgMan=PesoKgMan'
      'PecasMan=PecasMan'
      'AreaManM2=AreaManM2'
      'AreaManP2=AreaManP2'
      'ValorTMan=ValorTMan'
      'PesoKgSrc=PesoKgSrc'
      'PecasSrc=PecasSrc'
      'AreaSrcM2=AreaSrcM2'
      'AreaSrcP2=AreaSrcP2'
      'ValorTSrc=ValorTSrc'
      'PesoKgDst=PesoKgDst'
      'PecasDst=PecasDst'
      'AreaDstM2=AreaDstM2'
      'AreaDstP2=AreaDstP2'
      'ValorTDst=ValorTDst'
      'PesoKgBxa=PesoKgBxa'
      'PecasBxa=PecasBxa'
      'AreaBxaM2=AreaBxaM2'
      'AreaBxaP2=AreaBxaP2'
      'ValorTBxa=ValorTBxa'
      'PesoKgSdo=PesoKgSdo'
      'PecasSdo=PecasSdo'
      'AreaSdoM2=AreaSdoM2'
      'AreaSdoP2=AreaSdoP2'
      'ValorTSdo=ValorTSdo'
      'TipoArea=TipoArea'
      'CustoManMOKg=CustoManMOKg'
      'CustoManMOTot=CustoManMOTot'
      'ValorManMP=ValorManMP'
      'ValorManT=ValorManT'
      'Cliente=Cliente'
      'NFeRem=NFeRem'
      'LPFMO=LPFMO'
      'TemIMEIMrt=TemIMEIMrt'
      'KgCouPQ=KgCouPQ'
      'GGXDst=GGXDst'
      'NFeStatus=NFeStatus'
      'GGXSrc=GGXSrc'
      'SerieRem=SerieRem'
      'Operacoes=Operacoes'
      'VSCOPCab=VSCOPCab'
      'EmitGru=EmitGru'
      'NO_ACAO_COURO=NO_ACAO_COURO')
    DataSet = QrVSOrdem
    BCDToCurrency = False
    DataSetOptions = []
    Left = 32
    Top = 60
  end
  object frxDsVSOri: TfrxDBDataset
    UserName = 'frxDsVSOri'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch'
      'ReqMovEstq=ReqMovEstq')
    DataSet = QrVSOri
    BCDToCurrency = False
    DataSetOptions = []
    Left = 32
    Top = 156
  end
  object frxDsVSDst: TfrxDBDataset
    UserName = 'frxDsVSDst'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch'
      'ReqMovEstq=ReqMovEstq'
      'PedItsFin=PedItsFin'
      'Marca=Marca'
      'StqCenLoc=StqCenLoc'
      'NO_LOC_CEN=NO_LOC_CEN')
    DataSet = QrVSDst
    BCDToCurrency = False
    DataSetOptions = []
    Left = 32
    Top = 252
  end
  object QrVSAtu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 32
    Top = 297
    object QrVSAtuCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSAtuControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSAtuMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSAtuMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSAtuMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSAtuEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSAtuTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSAtuCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSAtuMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSAtuDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSAtuPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSAtuGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSAtuPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSAtuPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSAtuAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSAtuAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSAtuValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSAtuSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSAtuSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSAtuSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSAtuSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSAtuSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSAtuSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSAtuSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSAtuObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSAtuSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSAtuFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSAtuMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSAtuFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSAtuCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSAtuCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSAtuCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSAtuValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSAtuDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSAtuDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSAtuDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSAtuDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSAtuQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSAtuQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSAtuQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSAtuQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSAtuQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSAtuQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSAtuQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSAtuQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSAtuNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSAtuNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSAtuNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSAtuNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSAtuID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSAtuNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSAtuReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSAtuCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSAtuCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSAtuNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSAtuMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSAtuPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVSAtuStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSAtuNO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
    object QrVSAtuNO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrVSAtuClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSAtuCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object frxDsVSAtu: TfrxDBDataset
    UserName = 'frxDsVSAtu'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'NO_FORNECE=NO_FORNECE'
      'ReqMovEstq=ReqMovEstq'
      'CUSTO_M2=CUSTO_M2'
      'CUSTO_P2=CUSTO_P2'
      'NO_LOC_CEN=NO_LOC_CEN'
      'Marca=Marca'
      'PedItsLib=PedItsLib'
      'StqCenLoc=StqCenLoc'
      'NO_FICHA=NO_FICHA'
      'NO_FORNEC_MO=NO_FORNEC_MO'
      'ClientMO=ClientMO'
      'CustoPQ=CustoPQ')
    DataSet = QrVSAtu
    BCDToCurrency = False
    DataSetOptions = []
    Left = 32
    Top = 348
  end
end
