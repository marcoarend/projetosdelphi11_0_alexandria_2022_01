object FmVSReclassifOneOld: TFmVSReclassifOneOld
  Left = 0
  Top = 0
  Caption = 
    'WET-CURTI-030 :: Reclassifica'#231#227'o de Artigo de Ribeira Couro a Co' +
    'uro'
  ClientHeight = 812
  ClientWidth = 1904
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 707
    Width = 1904
    Height = 105
    Align = alBottom
    TabOrder = 2
    object Panel43: TPanel
      Left = 1
      Top = 1
      Width = 748
      Height = 103
      Align = alLeft
      TabOrder = 0
      object Panel6: TPanel
        Left = 1
        Top = 1
        Width = 746
        Height = 101
        Align = alClient
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label3: TLabel
          Left = 4
          Top = 8
          Width = 18
          Height = 13
          Caption = 'OC:'
          FocusControl = DBEdCodigo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 128
          Top = 56
          Width = 45
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdit19
        end
        object Label22: TLabel
          Left = 100
          Top = 8
          Width = 27
          Height = 13
          Caption = #193'rea:'
          FocusControl = DBEdit14
        end
        object Label14: TLabel
          Left = 172
          Top = 8
          Width = 81
          Height = 13
          Caption = 'Artigo de ribeira:'
        end
        object Label17: TLabel
          Left = 4
          Top = 32
          Width = 30
          Height = 13
          Caption = 'Pallet:'
        end
        object Label16: TLabel
          Left = 232
          Top = 32
          Width = 32
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label20: TLabel
          Left = 328
          Top = 32
          Width = 43
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object Label21: TLabel
          Left = 456
          Top = 32
          Width = 43
          Height = 13
          Caption = #193'rea ft'#178':'
        end
        object Label26: TLabel
          Left = 592
          Top = 32
          Width = 53
          Height = 13
          Caption = 'Ficha RMP:'
          FocusControl = DBEdit18
        end
        object Label18: TLabel
          Left = 4
          Top = 80
          Width = 167
          Height = 13
          Caption = 'Observa'#231#227'o sobre o pallet gerado:'
        end
        object Label24: TLabel
          Left = 476
          Top = 56
          Width = 59
          Height = 13
          Caption = 'Fornecedor:'
          FocusControl = DBEdit16
        end
        object Label84: TLabel
          Left = 120
          Top = 32
          Width = 30
          Height = 13
          Caption = 'IME-I:'
        end
        object Label85: TLabel
          Left = 640
          Top = 8
          Width = 36
          Height = 13
          Caption = 'Tempo:'
        end
        object Label87: TLabel
          Left = 4
          Top = 56
          Width = 79
          Height = 13
          Caption = 'C'#243'digo reclasse:'
          FocusControl = DBEdit35
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 28
          Top = 4
          Width = 68
          Height = 21
          TabStop = False
          DataField = 'CacCod'
          DataSource = DsVSPaRclCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit19: TDBEdit
          Left = 176
          Top = 52
          Width = 32
          Height = 21
          DataField = 'Empresa'
          DataSource = DsVSPaRclCab
          TabOrder = 1
        end
        object DBEdit2: TDBEdit
          Left = 204
          Top = 52
          Width = 269
          Height = 21
          DataField = 'NO_EMPRESA'
          DataSource = DsVSPaRclCab
          TabOrder = 2
        end
        object DBEdit14: TDBEdit
          Left = 136
          Top = 4
          Width = 32
          Height = 21
          DataField = 'NO_TIPO'
          DataSource = DsVSPaRclCab
          TabOrder = 3
        end
        object DBEdit6: TDBEdit
          Left = 256
          Top = 4
          Width = 57
          Height = 21
          DataField = 'GraGruX'
          DataSource = DsVSPallet0
          TabOrder = 4
        end
        object DBEdit7: TDBEdit
          Left = 312
          Top = 4
          Width = 321
          Height = 21
          DataField = 'NO_PRD_TAM_COR'
          DataSource = DsVSPallet0
          TabOrder = 5
        end
        object DBEdit10: TDBEdit
          Left = 40
          Top = 28
          Width = 76
          Height = 21
          DataField = 'VSPallet'
          DataSource = DsVSPaRclCab
          TabOrder = 6
        end
        object DBEdit9: TDBEdit
          Left = 268
          Top = 28
          Width = 56
          Height = 21
          DataField = 'Pecas'
          DataSource = DsVSGerArtNew
          TabOrder = 7
        end
        object DBEdit12: TDBEdit
          Left = 376
          Top = 28
          Width = 76
          Height = 21
          DataField = 'AreaM2'
          DataSource = DsVSGerArtNew
          TabOrder = 8
        end
        object DBEdit13: TDBEdit
          Left = 504
          Top = 28
          Width = 84
          Height = 21
          DataField = 'AreaP2'
          DataSource = DsVSGerArtNew
          TabOrder = 9
        end
        object DBEdit18: TDBEdit
          Left = 648
          Top = 28
          Width = 93
          Height = 21
          DataField = 'NO_FICHA'
          DataSource = DsVSGerArtNew
          TabOrder = 10
        end
        object DBEdit11: TDBEdit
          Left = 184
          Top = 76
          Width = 557
          Height = 21
          DataField = 'Nome'
          DataSource = DsVSPallet0
          TabOrder = 11
        end
        object DBEdit16: TDBEdit
          Left = 540
          Top = 52
          Width = 56
          Height = 21
          DataField = 'Terceiro'
          DataSource = DsVSGerArtNew
          TabOrder = 12
        end
        object DBEdit17: TDBEdit
          Left = 596
          Top = 52
          Width = 145
          Height = 21
          DataField = 'NO_TERCEIRO'
          DataSource = DsVSGerArtNew
          TabOrder = 13
        end
        object DBEdit5: TDBEdit
          Left = 152
          Top = 28
          Width = 76
          Height = 21
          DataField = 'VSMovIts'
          DataSource = DsVSPaRclCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clFuchsia
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
        end
        object EdTempo: TEdit
          Left = 680
          Top = 4
          Width = 61
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 15
          Text = '0,000'
        end
        object DBEdit35: TDBEdit
          Left = 92
          Top = 52
          Width = 32
          Height = 21
          DataField = 'Codigo'
          DataSource = DsVSPaRclCab
          TabOrder = 16
        end
      end
    end
    object Panel44: TPanel
      Left = 749
      Top = 1
      Width = 725
      Height = 103
      Align = alClient
      TabOrder = 1
      object RGFrmaIns: TRadioGroup
        Left = 1
        Top = 1
        Width = 148
        Height = 101
        Align = alLeft
        Caption = ' Forma classifica'#231#227'o:'
        Columns = 2
        Enabled = False
        ItemIndex = 0
        Items.Strings = (
          'Medidos'
          'Sumidos'
          'Ambos'
          'Manual')
        TabOrder = 0
        OnClick = RGFrmaInsClick
      end
      object BtClassesGeradas: TBitBtn
        Left = 156
        Top = 12
        Width = 160
        Height = 40
        Cursor = crHandPoint
        Caption = '&Reclasses Geradas'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtClassesGeradasClick
      end
    end
    object Panel11: TPanel
      Left = 1474
      Top = 1
      Width = 429
      Height = 103
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object Label70: TLabel
        Left = 8
        Top = 17
        Width = 109
        Height = 23
        Caption = 'Classificador:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label71: TLabel
        Left = 8
        Top = 57
        Width = 83
        Height = 23
        Caption = 'Digitador:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object EdRevisor: TdmkEditCB
        Left = 124
        Top = 13
        Width = 56
        Height = 31
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdRevisorRedefinido
        DBLookupComboBox = CBRevisor
        IgnoraDBLookupComboBox = False
      end
      object CBRevisor: TdmkDBLookupComboBox
        Left = 182
        Top = 13
        Width = 220
        Height = 31
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsRevisores
        ParentFont = False
        TabOrder = 1
        OnClick = CBRevisorClick
        dmkEditCB = EdRevisor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDigitador: TdmkEditCB
        Left = 124
        Top = 53
        Width = 56
        Height = 31
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdDigitadorRedefinido
        DBLookupComboBox = CBDigitador
        IgnoraDBLookupComboBox = False
      end
      object CBDigitador: TdmkDBLookupComboBox
        Left = 182
        Top = 53
        Width = 220
        Height = 31
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDigitadores
        ParentFont = False
        TabOrder = 3
        OnClick = CBDigitadorClick
        dmkEditCB = EdDigitador
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PnBoxesAll: TPanel
    Left = 229
    Top = 97
    Width = 1451
    Height = 610
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object PnBoxesT01: TPanel
      Left = 0
      Top = 0
      Width = 1451
      Height = 300
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object PnBoxT1L1: TPanel
        Left = 0
        Top = 0
        Width = 257
        Height = 300
        Align = alLeft
        TabOrder = 0
        object PnBox01: TPanel
          Left = 1
          Top = 1
          Width = 255
          Height = 298
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 255
            Height = 42
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel8: TPanel
              Left = 41
              Top = 0
              Width = 135
              Height = 42
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel49: TPanel
                Left = 0
                Top = 21
                Width = 135
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit3: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 35
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet1
                  TabOrder = 0
                end
                object DBEdit8: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet1
                  TabOrder = 1
                end
              end
              object DBEdit1: TDBEdit
                Left = 0
                Top = 0
                Width = 135
                Height = 21
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet1
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel50: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 42
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label79: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel51: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 29
                Align = alClient
                BevelOuter = bvNone
                Caption = '1'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Font.Quality = fqAntialiased
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel64: TPanel
              Left = 176
              Top = 0
              Width = 79
              Height = 42
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass1: TCheckBox
                Left = 4
                Top = 0
                Width = 69
                Height = 17
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass1Click
              end
            end
          end
          object Panel5: TPanel
            Left = 0
            Top = 42
            Width = 170
            Height = 256
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 170
              Height = 137
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel3: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 120
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label4: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit4
                end
                object Label7: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit20
                end
                object Label8: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea1
                end
                object Label11: TLabel
                  Left = 4
                  Top = 100
                  Width = 55
                  Height = 13
                  Caption = '% da '#225'rea:'
                  FocusControl = DBEdArea1
                end
                object Label54: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit4
                end
                object DBEdit4: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalRclIts1
                  TabOrder = 0
                end
                object DBEdit20: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum1
                  TabOrder = 1
                end
                object DBEdArea1: TDBEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum1
                  TabOrder = 2
                end
                object EdPercent1: TdmkEdit
                  Left = 64
                  Top = 96
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdit50: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalRclIts1
                  TabOrder = 4
                end
              end
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 137
              Width = 170
              Height = 119
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel16: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 102
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label52: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit36
                end
                object Label55: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit57
                end
                object Label53: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea1
                end
                object Label13: TLabel
                  Left = 4
                  Top = 76
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                  FocusControl = DBEdArea1
                end
                object DBEdit36: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal1
                  TabOrder = 0
                end
                object DBEdit43: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal1
                  TabOrder = 1
                end
                object DBEdit57: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet1
                  TabOrder = 2
                end
                object EdMedia1: TdmkEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object DBGPallet1: TdmkDBGridZTO
            Left = 170
            Top = 42
            Width = 85
            Height = 256
            Align = alClient
            DataSource = DsItens1
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            OptionsEx = []
            PopupMenu = PMItens01
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea'
                Width = 56
                Visible = True
              end>
          end
        end
      end
      object PnBoxT1L2: TPanel
        Left = 257
        Top = 0
        Width = 872
        Height = 300
        Align = alClient
        TabOrder = 1
        object PnBox02: TPanel
          Left = 1
          Top = 1
          Width = 870
          Height = 298
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel17: TPanel
            Left = 0
            Top = 0
            Width = 870
            Height = 42
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel18: TPanel
              Left = 41
              Top = 0
              Width = 750
              Height = 42
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel52: TPanel
                Left = 0
                Top = 21
                Width = 750
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit25: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 650
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet2
                  TabOrder = 0
                end
                object DBEdit26: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet2
                  TabOrder = 1
                end
              end
              object DBEdit22: TDBEdit
                Left = 0
                Top = 0
                Width = 750
                Height = 21
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet2
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel53: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 42
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label80: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel54: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 29
                Align = alClient
                BevelOuter = bvNone
                Caption = '2'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Font.Quality = fqAntialiased
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel66: TPanel
              Left = 791
              Top = 0
              Width = 79
              Height = 42
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass2: TCheckBox
                Left = 4
                Top = 0
                Width = 69
                Height = 17
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass2Click
              end
            end
          end
          object Panel19: TPanel
            Left = 0
            Top = 42
            Width = 170
            Height = 256
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 170
              Height = 137
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel26: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 120
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label5: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit15
                end
                object Label27: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit27
                end
                object Label28: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea2
                end
                object Label29: TLabel
                  Left = 4
                  Top = 100
                  Width = 55
                  Height = 13
                  Caption = '% da '#225'rea:'
                  FocusControl = DBEdArea2
                end
                object Label30: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit15
                end
                object DBEdit15: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalRclIts2
                  TabOrder = 0
                end
                object DBEdit27: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum2
                  TabOrder = 1
                end
                object DBEdArea2: TDBEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum2
                  TabOrder = 2
                end
                object EdPercent2: TdmkEdit
                  Left = 64
                  Top = 96
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdit29: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalRclIts2
                  TabOrder = 4
                end
              end
            end
            object GroupBox4: TGroupBox
              Left = 0
              Top = 137
              Width = 170
              Height = 119
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel30: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 102
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label31: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit58
                end
                object Label56: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit60
                end
                object Label57: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea2
                end
                object Label72: TLabel
                  Left = 4
                  Top = 76
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdit58: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal2
                  TabOrder = 0
                end
                object DBEdit59: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal2
                  TabOrder = 1
                end
                object DBEdit60: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet2
                  TabOrder = 2
                end
                object EdMedia2: TdmkEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object DBGPallet2: TdmkDBGridZTO
            Left = 170
            Top = 42
            Width = 700
            Height = 256
            Align = alClient
            DataSource = DsItens2
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            OptionsEx = []
            PopupMenu = PMItens02
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea'
                Width = 56
                Visible = True
              end>
          end
        end
      end
      object PnBoxT1L3: TPanel
        Left = 1129
        Top = 0
        Width = 322
        Height = 300
        Align = alRight
        TabOrder = 2
        object PnBox03: TPanel
          Left = 1
          Top = 1
          Width = 320
          Height = 298
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel20: TPanel
            Left = 0
            Top = 0
            Width = 320
            Height = 42
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel21: TPanel
              Left = 41
              Top = 0
              Width = 200
              Height = 42
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel58: TPanel
                Left = 0
                Top = 21
                Width = 200
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit31: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet3
                  TabOrder = 0
                end
                object DBEdit32: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet3
                  TabOrder = 1
                end
              end
              object DBEdit30: TDBEdit
                Left = 0
                Top = 0
                Width = 200
                Height = 21
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet3
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel59: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 42
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label82: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel60: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 29
                Align = alClient
                BevelOuter = bvNone
                Caption = '3'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Font.Quality = fqAntialiased
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel68: TPanel
              Left = 241
              Top = 0
              Width = 79
              Height = 42
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass3: TCheckBox
                Left = 4
                Top = 0
                Width = 69
                Height = 17
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass3Click
              end
            end
          end
          object Panel22: TPanel
            Left = 0
            Top = 42
            Width = 170
            Height = 256
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object GroupBox5: TGroupBox
              Left = 0
              Top = 0
              Width = 170
              Height = 137
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel34: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 120
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label32: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit33
                end
                object Label33: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit34
                end
                object Label34: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea3
                end
                object Label35: TLabel
                  Left = 4
                  Top = 100
                  Width = 55
                  Height = 13
                  Caption = '% da '#225'rea:'
                  FocusControl = DBEdArea3
                end
                object Label36: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit33
                end
                object DBEdit33: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalRclIts3
                  TabOrder = 0
                end
                object DBEdit34: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum3
                  TabOrder = 1
                end
                object DBEdArea3: TDBEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum3
                  TabOrder = 2
                end
                object EdPercent3: TdmkEdit
                  Left = 64
                  Top = 96
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdit61: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalRclIts3
                  TabOrder = 4
                end
              end
            end
            object GroupBox6: TGroupBox
              Left = 0
              Top = 137
              Width = 170
              Height = 119
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel35: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 102
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label58: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit62
                end
                object Label59: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit64
                end
                object Label60: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea3
                end
                object Label73: TLabel
                  Left = 4
                  Top = 76
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdit62: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal3
                  TabOrder = 0
                end
                object DBEdit63: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal3
                  TabOrder = 1
                end
                object DBEdit64: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet3
                  TabOrder = 2
                end
                object EdMedia3: TdmkEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object DBGPallet3: TdmkDBGridZTO
            Left = 170
            Top = 42
            Width = 150
            Height = 256
            Align = alClient
            DataSource = DsItens3
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            OptionsEx = []
            PopupMenu = PMItens03
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea'
                Width = 56
                Visible = True
              end>
          end
        end
      end
    end
    object PnBoxesT02: TPanel
      Left = 0
      Top = 300
      Width = 1451
      Height = 310
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object PnBoxT2L3: TPanel
        Left = 1131
        Top = 0
        Width = 320
        Height = 310
        Align = alRight
        TabOrder = 2
        object PnBox06: TPanel
          Left = 1
          Top = 1
          Width = 318
          Height = 308
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel23: TPanel
            Left = 0
            Top = 0
            Width = 318
            Height = 43
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel62: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 43
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label83: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel63: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 30
                Align = alClient
                BevelOuter = bvNone
                Caption = '6'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Font.Quality = fqAntialiased
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel24: TPanel
              Left = 41
              Top = 0
              Width = 198
              Height = 43
              Align = alClient
              TabOrder = 1
              object Panel61: TPanel
                Left = 1
                Top = 22
                Width = 196
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit38: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 96
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet6
                  TabOrder = 0
                end
                object DBEdit39: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet6
                  TabOrder = 1
                end
              end
              object DBEdit37: TDBEdit
                Left = 1
                Top = 1
                Width = 196
                Height = 21
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet6
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel69: TPanel
              Left = 239
              Top = 0
              Width = 79
              Height = 43
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass6: TCheckBox
                Left = 4
                Top = 0
                Width = 69
                Height = 17
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object Panel25: TPanel
            Left = 0
            Top = 43
            Width = 170
            Height = 265
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object GroupBox11: TGroupBox
              Left = 0
              Top = 0
              Width = 170
              Height = 137
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel40: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 120
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label37: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit40
                end
                object Label38: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit41
                end
                object Label39: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea6
                end
                object Label40: TLabel
                  Left = 4
                  Top = 100
                  Width = 55
                  Height = 13
                  Caption = '% da '#225'rea:'
                  FocusControl = DBEdArea6
                end
                object Label41: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit40
                end
                object DBEdit40: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalRclIts6
                  TabOrder = 0
                end
                object DBEdit41: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum6
                  TabOrder = 1
                end
                object DBEdArea6: TDBEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum6
                  TabOrder = 2
                end
                object EdPercent6: TdmkEdit
                  Left = 64
                  Top = 96
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdit72: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalRclIts6
                  TabOrder = 4
                end
              end
            end
            object GroupBox12: TGroupBox
              Left = 0
              Top = 137
              Width = 170
              Height = 128
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel41: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 111
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label67: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit73
                end
                object Label68: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit75
                end
                object Label69: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea6
                end
                object Label76: TLabel
                  Left = 4
                  Top = 76
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdit73: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal6
                  TabOrder = 0
                end
                object DBEdit74: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal6
                  TabOrder = 1
                end
                object DBEdit75: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet6
                  TabOrder = 2
                end
                object EdMedia6: TdmkEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object DBGPallet6: TdmkDBGridZTO
            Left = 170
            Top = 43
            Width = 148
            Height = 265
            Align = alClient
            DataSource = DsItens6
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            OptionsEx = []
            PopupMenu = PMItens06
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea'
                Width = 56
                Visible = True
              end>
          end
        end
      end
      object PnBoxT2L2: TPanel
        Left = 257
        Top = 0
        Width = 874
        Height = 310
        Align = alClient
        TabOrder = 1
        object PnBox05: TPanel
          Left = 1
          Top = 1
          Width = 872
          Height = 308
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel27: TPanel
            Left = 0
            Top = 0
            Width = 872
            Height = 43
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel28: TPanel
              Left = 41
              Top = 0
              Width = 752
              Height = 43
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel55: TPanel
                Left = 0
                Top = 21
                Width = 752
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit45: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 652
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet5
                  TabOrder = 0
                end
                object DBEdit46: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet5
                  TabOrder = 1
                end
              end
              object DBEdit44: TDBEdit
                Left = 0
                Top = 0
                Width = 752
                Height = 21
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet5
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel56: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 43
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label81: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel57: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 30
                Align = alClient
                BevelOuter = bvNone
                Caption = '5'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Font.Quality = fqAntialiased
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel67: TPanel
              Left = 793
              Top = 0
              Width = 79
              Height = 43
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass5: TCheckBox
                Left = 4
                Top = 0
                Width = 69
                Height = 17
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass5Click
              end
            end
          end
          object Panel29: TPanel
            Left = 0
            Top = 43
            Width = 170
            Height = 265
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object GroupBox9: TGroupBox
              Left = 0
              Top = 0
              Width = 170
              Height = 137
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel38: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 120
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label42: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit47
                end
                object Label43: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit48
                end
                object Label44: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea5
                end
                object Label45: TLabel
                  Left = 4
                  Top = 100
                  Width = 55
                  Height = 13
                  Caption = '% da '#225'rea:'
                  FocusControl = DBEdArea5
                end
                object Label46: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit47
                end
                object DBEdit47: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalRclIts5
                  TabOrder = 0
                end
                object DBEdit48: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum5
                  TabOrder = 1
                end
                object DBEdArea5: TDBEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum5
                  TabOrder = 2
                end
                object EdPercent5: TdmkEdit
                  Left = 64
                  Top = 96
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdit68: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalRclIts5
                  TabOrder = 4
                end
              end
            end
            object GroupBox10: TGroupBox
              Left = 0
              Top = 137
              Width = 170
              Height = 128
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel39: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 111
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label64: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit69
                end
                object Label65: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit71
                end
                object Label66: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea5
                end
                object Label75: TLabel
                  Left = 4
                  Top = 76
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdit69: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal5
                  TabOrder = 0
                end
                object DBEdit70: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal5
                  TabOrder = 1
                end
                object DBEdit71: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet5
                  TabOrder = 2
                end
                object EdMedia5: TdmkEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object DBGPallet5: TdmkDBGridZTO
            Left = 170
            Top = 43
            Width = 702
            Height = 265
            Align = alClient
            DataSource = DsItens5
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            OptionsEx = []
            PopupMenu = PMItens05
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea'
                Width = 56
                Visible = True
              end>
          end
        end
      end
      object PnBoxT2L1: TPanel
        Left = 0
        Top = 0
        Width = 257
        Height = 310
        Align = alLeft
        TabOrder = 0
        object PnBox04: TPanel
          Left = 1
          Top = 1
          Width = 255
          Height = 308
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel31: TPanel
            Left = 0
            Top = 0
            Width = 255
            Height = 43
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel32: TPanel
              Left = 41
              Top = 0
              Width = 135
              Height = 43
              Align = alClient
              TabOrder = 0
              object Panel46: TPanel
                Left = 1
                Top = 22
                Width = 133
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit52: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 33
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet4
                  TabOrder = 0
                end
                object DBEdit53: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet4
                  TabOrder = 1
                end
              end
              object DBEdit51: TDBEdit
                Left = 1
                Top = 1
                Width = 133
                Height = 21
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet4
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel47: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 43
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label78: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel48: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 30
                Align = alClient
                BevelOuter = bvNone
                Caption = '4'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Font.Quality = fqAntialiased
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel65: TPanel
              Left = 176
              Top = 0
              Width = 79
              Height = 43
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass4: TCheckBox
                Left = 8
                Top = 0
                Width = 69
                Height = 17
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass4Click
              end
            end
          end
          object Panel33: TPanel
            Left = 0
            Top = 43
            Width = 170
            Height = 265
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object GroupBox7: TGroupBox
              Left = 0
              Top = 0
              Width = 170
              Height = 137
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel36: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 120
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label47: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit28
                end
                object Label48: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit54
                end
                object Label49: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea4
                end
                object Label50: TLabel
                  Left = 4
                  Top = 100
                  Width = 55
                  Height = 13
                  Caption = '% da '#225'rea:'
                  FocusControl = DBEdArea4
                end
                object Label51: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit28
                end
                object DBEdit28: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalRclIts4
                  TabOrder = 0
                end
                object DBEdit54: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum4
                  TabOrder = 1
                end
                object DBEdArea4: TDBEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum4
                  TabOrder = 2
                end
                object EdPercent4: TdmkEdit
                  Left = 64
                  Top = 96
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object DBEdit56: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalRclIts4
                  TabOrder = 4
                end
              end
            end
            object GroupBox8: TGroupBox
              Left = 0
              Top = 137
              Width = 170
              Height = 128
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel37: TPanel
                Left = 2
                Top = 15
                Width = 166
                Height = 111
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label61: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit65
                end
                object Label62: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit67
                end
                object Label63: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea4
                end
                object Label74: TLabel
                  Left = 4
                  Top = 76
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdit65: TDBEdit
                  Left = 64
                  Top = 24
                  Width = 100
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal4
                  TabOrder = 0
                end
                object DBEdit66: TDBEdit
                  Left = 64
                  Top = 48
                  Width = 100
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal4
                  TabOrder = 1
                end
                object DBEdit67: TDBEdit
                  Left = 64
                  Top = 0
                  Width = 100
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet4
                  TabOrder = 2
                end
                object EdMedia4: TdmkEdit
                  Left = 64
                  Top = 72
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object DBGPallet4: TdmkDBGridZTO
            Left = 170
            Top = 43
            Width = 85
            Height = 265
            Align = alClient
            DataSource = DsItens4
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            OptionsEx = []
            PopupMenu = PMItens04
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea'
                Width = 56
                Visible = True
              end>
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 1680
    Top = 97
    Width = 224
    Height = 610
    Align = alRight
    Caption = 'Panel2'
    TabOrder = 3
    object DBGAll: TdmkDBGridZTO
      Left = 1
      Top = 1
      Width = 222
      Height = 608
      Align = alClient
      DataSource = DsAll
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      OptionsEx = []
      PopupMenu = PMAll
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Box'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SubClass'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VMI_Dest'
          Title.Caption = 'IME-I'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea'
          Width = 56
          Visible = True
        end>
    end
  end
  object PnInfoBig: TPanel
    Left = 0
    Top = 0
    Width = 1904
    Height = 97
    Align = alTop
    TabOrder = 0
    object PnDigitacao: TPanel
      Left = 1540
      Top = 1
      Width = 363
      Height = 95
      Align = alRight
      Enabled = False
      TabOrder = 0
      object Label12: TLabel
        Left = 1
        Top = 1
        Width = 83
        Height = 23
        Align = alTop
        Alignment = taCenter
        Caption = ' Digita'#231#227'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object PnBox: TPanel
        Left = 224
        Top = 24
        Width = 55
        Height = 70
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object Label2: TLabel
          Left = 0
          Top = 0
          Width = 37
          Height = 23
          Align = alTop
          Caption = 'Box:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdBox: TdmkEdit
          Left = 0
          Top = 23
          Width = 55
          Height = 47
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdBoxChange
          OnKeyDown = EdBoxKeyDown
        end
      end
      object PnArea: TPanel
        Left = 1
        Top = 24
        Width = 223
        Height = 70
        Align = alClient
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label1: TLabel
          Left = 0
          Top = 0
          Width = 45
          Height = 23
          Align = alTop
          Caption = #193'rea:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LaTipoArea: TLabel
          Left = 200
          Top = 23
          Width = 23
          Height = 29
          Align = alRight
          Caption = '?'#178
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdArea: TdmkEdit
          Left = 0
          Top = 23
          Width = 200
          Height = 47
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object PnSubClass: TPanel
        Left = 279
        Top = 24
        Width = 83
        Height = 70
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object Label86: TLabel
          Left = 0
          Top = 0
          Width = 73
          Height = 23
          Align = alTop
          Caption = 'SubClas:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdSubClass: TdmkEdit
          Left = 0
          Top = 23
          Width = 83
          Height = 47
          Align = alClient
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Tahoma'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSubClassChange
          OnExit = EdSubClassExit
          OnKeyDown = EdSubClassKeyDown
        end
      end
    end
    object Panel12: TPanel
      Left = 321
      Top = 1
      Width = 320
      Height = 95
      Align = alLeft
      TabOrder = 1
      object Label15: TLabel
        Left = 1
        Top = 1
        Width = 185
        Height = 23
        Align = alTop
        Alignment = taCenter
        Caption = 'Couros j'#225' classificados'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Panel10: TPanel
        Left = 1
        Top = 24
        Width = 120
        Height = 70
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'Panel9'
        TabOrder = 0
        object Label10: TLabel
          Left = 0
          Top = 0
          Width = 60
          Height = 23
          Align = alTop
          Caption = ' Pe'#231'as:'
          FocusControl = DBEdit23
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBEdit23: TDBEdit
          Left = 0
          Top = 23
          Width = 120
          Height = 47
          TabStop = False
          Align = alClient
          DataField = 'Pecas'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 37
        end
      end
      object Panel9: TPanel
        Left = 121
        Top = 24
        Width = 198
        Height = 70
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel9'
        TabOrder = 1
        object Label9: TLabel
          Left = 0
          Top = 0
          Width = 51
          Height = 23
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEDAreaT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBEDAreaT: TDBEdit
          Left = 0
          Top = 23
          Width = 198
          Height = 47
          TabStop = False
          Align = alClient
          DataField = 'AreaM2'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 37
        end
      end
    end
    object Panel13: TPanel
      Left = 1
      Top = 1
      Width = 320
      Height = 95
      Align = alLeft
      TabOrder = 2
      object Label19: TLabel
        Left = 1
        Top = 1
        Width = 238
        Height = 23
        Align = alTop
        Alignment = taCenter
        Caption = 'Couros que faltam classificar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Panel14: TPanel
        Left = 1
        Top = 24
        Width = 120
        Height = 70
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'Panel9'
        TabOrder = 0
        object Label23: TLabel
          Left = 0
          Top = 0
          Width = 60
          Height = 23
          Align = alTop
          Caption = ' Pe'#231'as:'
          FocusControl = DBEdit21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBEdit21: TDBEdit
          Left = 0
          Top = 23
          Width = 120
          Height = 47
          TabStop = False
          Align = alClient
          DataField = 'FALTA_PECA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 37
        end
      end
      object Panel15: TPanel
        Left = 121
        Top = 24
        Width = 198
        Height = 70
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel9'
        TabOrder = 1
        object Label25: TLabel
          Left = 0
          Top = 0
          Width = 51
          Height = 23
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEdit24
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBEdit24: TDBEdit
          Left = 0
          Top = 23
          Width = 198
          Height = 47
          TabStop = False
          Align = alClient
          DataField = 'FALTA_AREA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 37
        end
      end
    end
    object BtEncerra: TBitBtn
      Tag = 10134
      Left = 734
      Top = 8
      Width = 216
      Height = 72
      Caption = '&Menu'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      TabStop = False
      OnClick = BtEncerraClick
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 956
      Top = 8
      Width = 216
      Height = 72
      Caption = '&Imprimir'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = BtImprimeClick
    end
    object BtDigitacao: TButton
      Left = 652
      Top = 28
      Width = 75
      Height = 37
      Caption = 'Teste'
      Enabled = False
      TabOrder = 5
      TabStop = False
      Visible = False
      OnClick = BtDigitacaoClick
    end
    object BtReabre: TBitBtn
      Tag = 18
      Left = 1177
      Top = 8
      Width = 72
      Height = 72
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      TabStop = False
      OnClick = BtReabreClick
    end
  end
  object Panel42: TPanel
    Left = 0
    Top = 97
    Width = 229
    Height = 610
    Align = alLeft
    Caption = 'Panel2'
    TabOrder = 4
    object Splitter1: TSplitter
      Left = 1
      Top = 157
      Width = 227
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 1
    end
    object Splitter2: TSplitter
      Left = 1
      Top = 383
      Width = 227
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitLeft = -3
      ExplicitTop = 401
    end
    object DBGItensACP: TdmkDBGridZTO
      Left = 1
      Top = 1
      Width = 227
      Height = 156
      Align = alClient
      DataSource = DsItensACP
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      OptionsEx = []
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'VMI_Dest'
          Title.Caption = 'IME-I Ori.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID couro'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea'
          Width = 56
          Visible = True
        end>
    end
    object Memo1: TMemo
      Left = 156
      Top = 12
      Width = 185
      Height = 89
      Lines.Strings = (
        'Memo1')
      TabOrder = 1
      Visible = False
    end
    object PnDesnate: TPanel
      Left = 1
      Top = 388
      Width = 227
      Height = 221
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object Panel45: TPanel
        Left = 0
        Top = 0
        Width = 227
        Height = 29
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object SbDesnate: TSpeedButton
          Left = 136
          Top = 4
          Width = 23
          Height = 22
          OnClick = SbDesnateClick
        end
        object Label77: TLabel
          Left = 4
          Top = 8
          Width = 44
          Height = 13
          Caption = 'Desnate:'
        end
        object EdDesnate: TdmkEdit
          Left = 52
          Top = 4
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CkDesnate: TCheckBox
          Left = 172
          Top = 8
          Width = 97
          Height = 17
          Caption = 'Auto.'
          TabOrder = 1
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 29
        Width = 227
        Height = 192
        Align = alClient
        DataSource = DsVSCacIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercM2'
            Title.Caption = '% m'#178
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end>
      end
    end
    object PnEqualize: TPanel
      Left = 1
      Top = 162
      Width = 227
      Height = 221
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object Panel70: TPanel
        Left = 0
        Top = 0
        Width = 227
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object SbEqualize: TSpeedButton
          Left = 136
          Top = 8
          Width = 23
          Height = 22
          OnClick = SbEqualizeClick
        end
        object Label88: TLabel
          Left = 8
          Top = 12
          Width = 43
          Height = 13
          Caption = 'Equ'#225'lize:'
        end
        object EdEqualize: TdmkEdit
          Left = 52
          Top = 8
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CkEqualize: TCheckBox
          Left = 164
          Top = 0
          Width = 50
          Height = 17
          Caption = 'Auto.'
          TabOrder = 1
        end
        object CkNota: TCheckBox
          Left = 164
          Top = 16
          Width = 50
          Height = 17
          Caption = 'Nota.'
          TabOrder = 2
          OnClick = CkNotaClick
        end
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 74
        Width = 227
        Height = 147
        Align = alClient
        DataSource = DsNotas
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SubClass'
            Title.Caption = 'Sub classe'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercM2'
            Title.Caption = '% m'#178
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end>
      end
      object PnNota: TPanel
        Left = 0
        Top = 33
        Width = 227
        Height = 41
        Align = alTop
        TabOrder = 2
        Visible = False
        object DBEdit42: TDBEdit
          Left = 1
          Top = 1
          Width = 225
          Height = 39
          Align = alClient
          DataField = 'NotaEqzM2'
          DataSource = DsNotaCrr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Font.Quality = fqAntialiased
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
    end
  end
  object QrVSPaRclCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaRclCabBeforeClose
    AfterScroll = QrVSPaRclCabAfterScroll
    OnCalcFields = QrVSPaRclCabCalcFields
    SQL.Strings = (
      'SELECT vga.GraGruX, vga.Nome, '
      'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,'
      'vga.VSPallet'
      'FROM vspaclacab pcc'
      'LEFT JOIN vsgerart vga ON vga.Codigo=pcc.vsgerart'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vga.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa'
      'WHERE pcc.Codigo=5'
      ''
      ''
      '')
    Left = 24
    Top = 308
    object QrVSPaRclCabNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSPaRclCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPaRclCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSPaRclCabTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSPaRclCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPaRclCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSPaRclCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaRclCabVSGerRcl: TIntegerField
      FieldName = 'VSGerRcl'
    end
    object QrVSPaRclCabLstPal01: TIntegerField
      FieldName = 'LstPal01'
    end
    object QrVSPaRclCabLstPal02: TIntegerField
      FieldName = 'LstPal02'
    end
    object QrVSPaRclCabLstPal03: TIntegerField
      FieldName = 'LstPal03'
    end
    object QrVSPaRclCabLstPal04: TIntegerField
      FieldName = 'LstPal04'
    end
    object QrVSPaRclCabLstPal05: TIntegerField
      FieldName = 'LstPal05'
    end
    object QrVSPaRclCabLstPal06: TIntegerField
      FieldName = 'LstPal06'
    end
    object QrVSPaRclCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaRclCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaRclCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaRclCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaRclCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaRclCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaRclCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaRclCabNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPaRclCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSPaRclCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPaRclCabVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSPaRclCabVSGerRclA: TIntegerField
      FieldName = 'VSGerRclA'
    end
    object QrVSPaRclCabVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
  end
  object DsVSPaRclCab: TDataSource
    DataSet = QrVSPaRclCab
    Left = 24
    Top = 356
  end
  object QrVSGerArtNew: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wbp.Empresa, wbp.GraGruX,'
      'SUM(wmi.Pecas) Pecas, SUM(wmi.PesoKg) PesoKg,  '
      'SUM(wmi.AreaM2) AreaM2, SUM(wmi.AreaP2) AreaP2,  '
      'SUM(wmi.ValorT) ValorT,  '
      'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,  '
      
        'IF(COUNT(DISTINCT wmi.SerieFch) <> 1, 0, wmi.SerieFch) SerieFch,' +
        ' '
      'IF(COUNT(DISTINCT wmi.Ficha) <> 1, 0, wmi.Ficha) Ficha, '
      
        'IF(COUNT(DISTINCT wmi.Terceiro) <> 1, 0, wmi.Terceiro) Terceiro,' +
        ' '
      'IF(COUNT(DISTINCT wmi.Terceiro) <> 1, "V'#225'rios", '
      '  IF(wmi.Terceiro=0, "V'#225'rios",  '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)  '
      ')) NO_TERCEIRO,  '
      'IF(COUNT(DISTINCT wmi.SerieFch) <> 1, "V'#225'rias",'
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL,  '
      '"?", vsf.Nome), " ", wmi.Ficha))) NO_FICHA,  '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2,  '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2  '
      'FROM vspalleta  wbp  '
      'LEFT JOIN vsmovits   wmi ON wbp.Codigo=wmi.Pallet  '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=wbp.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro  '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch  '
      'WHERE wmi.Pallet>=0 '
      'AND wmi.Pecas>0  '
      'GROUP BY wmi.Pallet  ')
    Left = 88
    Top = 348
    object QrVSGerArtNewPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSGerArtNewPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSGerArtNewAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSGerArtNewAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSGerArtNewValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSGerArtNewNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArtNewNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrVSGerArtNewNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrVSGerArtNewNO_FICHA: TWideStringField
      FieldName = 'NO_FICHA'
      Size = 72
    end
    object QrVSGerArtNewCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
    end
    object QrVSGerArtNewCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
    end
    object QrVSGerArtNewEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerArtNewGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerArtNewMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSGerArtNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerArtNewControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSGerArtNewMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSGerArtNewSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSGerArtNewFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSGerArtNewTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
  end
  object DsVSGerArtNew: TDataSource
    DataSet = QrVSGerArtNew
    Left = 88
    Top = 397
  end
  object QrVSPallet1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 604
    Top = 81
    object QrVSPallet1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet1AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet1Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet1Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet1CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet1GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet1NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet1NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet1NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet1: TDataSource
    DataSet = QrVSPallet1
    Left = 604
    Top = 125
  end
  object QrVSPallet2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 668
    Top = 81
    object QrVSPallet2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet2UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet2UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet2AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet2Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet2Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet2CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet2NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet2NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet2NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet2: TDataSource
    DataSet = QrVSPallet2
    Left = 668
    Top = 125
  end
  object QrVSPallet3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 732
    Top = 81
    object QrVSPallet3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet3Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet3DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet3DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet3UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet3UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet3AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet3Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet3Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet3Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet3CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet3GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet3NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet3NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet3NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet3: TDataSource
    DataSet = QrVSPallet3
    Left = 732
    Top = 125
  end
  object QrVSPallet4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 796
    Top = 81
    object QrVSPallet4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet4Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet4DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet4DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet4UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet4UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet4AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet4Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet4Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet4Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet4CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet4GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet4NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet4NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet4NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet4: TDataSource
    DataSet = QrVSPallet4
    Left = 796
    Top = 125
  end
  object QrVSPallet5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 860
    Top = 81
    object QrVSPallet5Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet5Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet5Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet5DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet5DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet5UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet5UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet5AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet5Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet5Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet5Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet5CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet5GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet5NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet5NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet5NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet5: TDataSource
    DataSet = QrVSPallet5
    Left = 860
    Top = 125
  end
  object QrVSPallet6: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 924
    Top = 81
    object QrVSPallet6Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet6Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet6Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet6DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet6DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet6UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet6UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet6AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet6Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet6Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet6Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet6CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet6GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet6NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet6NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet6NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet6: TDataSource
    DataSet = QrVSPallet6
    Left = 924
    Top = 125
  end
  object QrVSPalRclIts1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 604
    Top = 172
    object QrVSPalRclIts1VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalRclIts1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalRclIts1DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrVSPalRclIts1VMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPalRclIts1VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
  end
  object DsVSPalRclIts1: TDataSource
    DataSet = QrVSPalRclIts1
    Left = 604
    Top = 220
  end
  object QrVSPalRclIts2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 672
    Top = 172
    object QrVSPalRclIts2VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalRclIts2VMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPalRclIts2VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalRclIts2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalRclIts2DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalRclIts2: TDataSource
    DataSet = QrVSPalRclIts2
    Left = 672
    Top = 220
  end
  object QrVSPalRclIts3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 736
    Top = 172
    object QrVSPalRclIts3VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalRclIts3VMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPalRclIts3VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalRclIts3Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalRclIts3DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalRclIts3: TDataSource
    DataSet = QrVSPalRclIts3
    Left = 732
    Top = 220
  end
  object QrVSPalRclIts4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 796
    Top = 172
    object QrVSPalRclIts4VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalRclIts4VMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPalRclIts4VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalRclIts4Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalRclIts4DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalRclIts4: TDataSource
    DataSet = QrVSPalRclIts4
    Left = 796
    Top = 220
  end
  object QrVSPalRclIts5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 860
    Top = 172
    object QrVSPalRclIts5VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalRclIts5VMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPalRclIts5VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalRclIts5Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalRclIts5DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalRclIts5: TDataSource
    DataSet = QrVSPalRclIts5
    Left = 860
    Top = 220
  end
  object QrVSPalRclIts6: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 924
    Top = 172
    object QrVSPalRclIts6VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalRclIts6VMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPalRclIts6VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalRclIts6Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalRclIts6DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalRclIts6: TDataSource
    DataSet = QrVSPalRclIts6
    Left = 924
    Top = 220
  end
  object QrItens1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 604
    Top = 264
    object QrItens1Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens1Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens1AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens1AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens1: TDataSource
    DataSet = QrItens1
    Left = 604
    Top = 312
  end
  object QrSum1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 604
    Top = 356
    object QrSum1Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum1AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum1AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum1: TDataSource
    DataSet = QrSum1
    Left = 604
    Top = 404
  end
  object QrItens2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 672
    Top = 264
    object QrItens2Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens2AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens2AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens2: TDataSource
    DataSet = QrItens2
    Left = 672
    Top = 312
  end
  object QrSum2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 672
    Top = 356
    object QrSum2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum2: TDataSource
    DataSet = QrSum2
    Left = 672
    Top = 404
  end
  object QrItens3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 732
    Top = 264
    object QrItens3Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens3Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens3AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens3AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens3: TDataSource
    DataSet = QrItens3
    Left = 732
    Top = 312
  end
  object QrSum3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 732
    Top = 356
    object QrSum3Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum3AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum3AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum3: TDataSource
    DataSet = QrSum3
    Left = 732
    Top = 404
  end
  object QrItens4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 796
    Top = 264
    object QrItens4Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens4Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens4AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens4AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens4: TDataSource
    DataSet = QrItens4
    Left = 796
    Top = 312
  end
  object QrSum4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 796
    Top = 356
    object QrSum4Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum4AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum4AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum4: TDataSource
    DataSet = QrSum4
    Left = 796
    Top = 404
  end
  object QrItens5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 860
    Top = 264
    object QrItens5Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens5Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens5AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens5AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens5: TDataSource
    DataSet = QrItens5
    Left = 860
    Top = 312
  end
  object QrSum5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 860
    Top = 356
    object QrSum5Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum5AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum5AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSum5: TDataSource
    DataSet = QrSum5
    Left = 860
    Top = 404
  end
  object QrItens6: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 924
    Top = 264
    object QrItens6Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens6Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens6AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens6AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens6: TDataSource
    DataSet = QrItens6
    Left = 924
    Top = 312
  end
  object QrSum6: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 924
    Top = 356
    object QrSum6Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum6AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum6AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum6: TDataSource
    DataSet = QrSum6
    Left = 924
    Top = 404
  end
  object QrSumT: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSumTCalcFields
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 24
    Top = 404
    object QrSumTPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumTAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumTAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumTFALTA_PECA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FALTA_PECA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSumTFALTA_AREA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FALTA_AREA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsSumT: TDataSource
    DataSet = QrSumT
    Left = 24
    Top = 452
  end
  object QrAll: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'ORDER BY Controle DESC')
    Left = 24
    Top = 496
    object QrAllControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrAllPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrAllAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAllAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAllBox: TIntegerField
      FieldName = 'Box'
    end
    object QrAllVSPaRclIts: TIntegerField
      FieldName = 'VSPaRclIts'
    end
    object QrAllVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrAllVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrAllVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrAllSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
  end
  object DsAll: TDataSource
    DataSet = QrAll
    Left = 24
    Top = 544
  end
  object PMItens01: TPopupMenu
    OnPopup = PMitensPopup
    Left = 452
    Top = 140
    object AdicionarPallet01: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet01: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet01: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados1: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens02: TPopupMenu
    OnPopup = PMitensPopup
    Left = 452
    Top = 184
    object AdicionarPallet02: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet02: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet02: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados2: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object QrSumPal1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 608
    Top = 456
    object QrSumPal1Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal1AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal1AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal1: TDataSource
    DataSet = QrSumPal1
    Left = 608
    Top = 504
  end
  object QrSumPal2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 672
    Top = 448
    object QrSumPal2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal2: TDataSource
    DataSet = QrSumPal2
    Left = 672
    Top = 496
  end
  object QrSumPal3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 732
    Top = 452
    object QrSumPal3Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal3AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal3AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal3: TDataSource
    DataSet = QrSumPal3
    Left = 732
    Top = 500
  end
  object QrSumPal4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 796
    Top = 452
    object QrSumPal4Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal4AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal4AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal4: TDataSource
    DataSet = QrSumPal4
    Left = 796
    Top = 500
  end
  object QrSumPal5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 860
    Top = 448
    object QrSumPal5Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal5AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal5AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal5: TDataSource
    DataSet = QrSumPal5
    Left = 860
    Top = 496
  end
  object QrSumPal6: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 924
    Top = 452
    object QrSumPal6Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal6AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal6AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal6: TDataSource
    DataSet = QrSumPal6
    Left = 924
    Top = 500
  end
  object QrSumVMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 608
    Top = 552
    object QrSumVMIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object PMItens03: TPopupMenu
    OnPopup = PMitensPopup
    Left = 452
    Top = 228
    object AdicionarPallet03: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet03: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet03: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados3: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens04: TPopupMenu
    OnPopup = PMitensPopup
    Left = 452
    Top = 272
    object AdicionarPallet04: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet04: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet04: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados4: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens05: TPopupMenu
    OnPopup = PMitensPopup
    Left = 452
    Top = 320
    object AdicionarPallet05: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet05: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet05: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados5: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens06: TPopupMenu
    OnPopup = PMitensPopup
    Left = 452
    Top = 360
    object AdicionarPallet06: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet06: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet06: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados6: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 460
    Top = 488
    object EstaOCOrdemdeclassificao1: TMenuItem
      Caption = 'Encerra esta OC (Ordem de classifica'#231#227'o)'
      OnClick = EstaOCOrdemdeclassificao1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Palletdobox11: TMenuItem
      Caption = 'Encerra o pallet do box &1'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox21: TMenuItem
      Caption = 'Encerra o pallet do box &2'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox31: TMenuItem
      Caption = 'Encerra o pallet do box &3'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox41: TMenuItem
      Caption = 'Encerra o pallet do box &4'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox51: TMenuItem
      Caption = 'Encerra o pallet do box &5'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox61: TMenuItem
      Caption = 'Encerra o pallet do box &6'
      OnClick = EncerrarPalletClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Mostrartodosboxes1: TMenuItem
      Caption = 'Mostrar todos boxes'
      OnClick = Mostrartodosboxes1Click
    end
  end
  object QrItensACP: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrItensACPAfterOpen
    BeforeClose = QrItensACPBeforeClose
    AfterScroll = QrItensACPAfterScroll
    Left = 20
    Top = 148
    object QrItensACPCacID: TIntegerField
      FieldName = 'CacID'
    end
    object QrItensACPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrItensACPControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItensACPAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrItensACPAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrItensACPVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
  end
  object DsItensACP: TDataSource
    DataSet = QrItensACP
    Left = 20
    Top = 196
  end
  object QrVSPallet0: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 24
    Top = 57
    object QrVSPallet0Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet0Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet0Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet0DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet0DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet0UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet0UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet0AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet0Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet0Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet0Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet0CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet0GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet0NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet0NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet0NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet0: TDataSource
    DataSet = QrVSPallet0
    Left = 24
    Top = 101
  end
  object QrRevisores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 84
    Top = 452
    object QrRevisoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRevisoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsRevisores: TDataSource
    DataSet = QrRevisores
    Left = 84
    Top = 496
  end
  object QrDigitadores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 164
    Top = 456
    object QrDigitadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDigitadoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsDigitadores: TDataSource
    DataSet = QrDigitadores
    Left = 164
    Top = 500
  end
  object QrSorces: TmySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 156
    object QrSorcesPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSorcesAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSorcesAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSorcesVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
  end
  object QrVsiDest: TmySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 60
    object QrVsiDestMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVsiDestMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVsiDestCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVsiDestControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVsiDestGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVsiDestMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVsiDestEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVsiDestTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
  end
  object QrVsiSorc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 108
    object QrVsiSorcGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVsiSorcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVsiSorcValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVsiSorcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVsiSorcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVsiSorcFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVsiSorcSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVsiSorcMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object QrSumSorc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 204
    object QrSumSorcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumSorcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumSorcAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object PMAll: TPopupMenu
    Left = 1764
    Top = 404
    object Alteraitematual1: TMenuItem
      Caption = '&Altera item atual - '#225'rea'
      OnClick = Alteraitematual1Click
    end
    object AlteraitematualSubClasse1: TMenuItem
      Caption = 'Altera item atual - Sub &Classe'
      OnClick = AlteraitematualSubClasse1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Excluiitematual1: TMenuItem
      Caption = '&Exclui item atual'
      OnClick = Excluiitematual1Click
    end
  end
  object QrVSCacIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVSCacItsCalcFields
    SQL.Strings = (
      'SELECT pla.GraGruX, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vscacitsa cia '
      'LEFT JOIN vspalleta pla ON pla.Codigo=cia.VSPallet '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE cia.vmi_Sorc=5'
      'GROUP BY pla.GraGruX ')
    Left = 96
    Top = 624
    object QrVSCacItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSCacItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSCacItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSCacItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSCacItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCacItsPercM2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercM2'
      DisplayFormat = '0.00'
      Calculated = True
    end
    object QrVSCacItsPercPc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercPc'
      Calculated = True
    end
    object QrVSCacItsMediaM2PC: TFloatField
      FieldName = 'MediaM2PC'
    end
    object QrVSCacItsAgrupaTudo: TFloatField
      FieldName = 'AgrupaTudo'
    end
  end
  object QrVSCacSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2'
      'FROM vscacitsa cia '
      'WHERE cia.vmi_Sorc=5')
    Left = 168
    Top = 644
    object QrVSCacSumPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacSumAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSCacSumAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsVSCacIts: TDataSource
    DataSet = QrVSCacIts
    Left = 96
    Top = 668
  end
  object QrSumDest1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 728
    Top = 616
  end
  object QrSumSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 732
    Top = 664
  end
  object QrVMISorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 812
    Top = 616
  end
  object QrPalSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 812
    Top = 664
  end
  object QrNotaCrr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 932
    Top = 608
    object QrNotaCrrPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaCrrAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaCrr: TDataSource
    DataSet = QrNotaCrr
    Left = 932
    Top = 652
  end
  object QrNotas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cia.SubClass, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaM2,'
      'SUM(cia.AreaM2) / 27.0034 PercM2 '
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vdi ON vdi.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vdc ON vdc.Codigo=vdi.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vdc.Codigo=1'
      'GROUP BY cia.SubClass')
    Left = 992
    Top = 608
    object QrNotasSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrNotasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotasAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasNotaM2: TFloatField
      FieldName = 'NotaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasPercM2: TFloatField
      FieldName = 'PercM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsNotas: TDataSource
    DataSet = QrNotas
    Left = 992
    Top = 652
  end
  object QrNotaAll: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 872
    Top = 612
    object QrNotaAllPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaAllAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaAll: TDataSource
    DataSet = QrNotaAll
    Left = 872
    Top = 656
  end
end
