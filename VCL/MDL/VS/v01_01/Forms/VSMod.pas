unit VSMod;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, mySQLDbTables,
  DmkGeral, UnDmkEnums, UnProjGroup_Consts, Vcl.StdCtrls, Vcl.ComCtrls,
  dmkPermissoes, Vcl.Buttons, Vcl.ExtCtrls, dmkImage, Vcl.Menus, dmkEditCB,
  dmkDBLookupComboBox, UnAppEnums;

type
  TDfVSMod = class(TForm)
    QrVsiDest: TmySQLQuery;
    QrVsiDestMovimTwn: TIntegerField;
    QrVsiDestMovimID: TIntegerField;
    QrVsiDestCodigo: TIntegerField;
    QrVsiDestControle: TIntegerField;
    QrVsiDestGraGruX: TIntegerField;
    QrVsiDestMovimCod: TIntegerField;
    QrVsiDestEmpresa: TIntegerField;
    QrVsiDestTerceiro: TIntegerField;
    QrVsiSorc: TmySQLQuery;
    QrVsiSorcGraGruX: TIntegerField;
    QrVsiSorcAreaM2: TFloatField;
    QrVsiSorcValorT: TFloatField;
    QrVsiSorcCodigo: TIntegerField;
    QrVsiSorcControle: TIntegerField;
    QrVsiSorcFicha: TIntegerField;
    QrVsiSorcSerieFch: TIntegerField;
    QrVsiSorcMarca: TWideStringField;
    QrSorces: TmySQLQuery;
    QrSorcesPecas: TFloatField;
    QrSorcesAreaM2: TFloatField;
    QrSorcesAreaP2: TFloatField;
    QrSorcesVMI_Sorc: TIntegerField;
    QrSumSorc: TmySQLQuery;
    QrSumSorcPecas: TFloatField;
    QrSumSorcAreaM2: TFloatField;
    QrSumSorcAreaP2: TFloatField;
    QrVSPallet: TmySQLQuery;
    QrVSPalletGraGruX: TIntegerField;
    QrVMI_Sorc: TmySQLQuery;
    QrVMI_SorcVMI_Sorc: TIntegerField;
    QrVMI_SorcPecas: TFloatField;
    QrVMI_SorcAreaM2: TFloatField;
    QrVMI_Baix: TmySQLQuery;
    QrVMI_BaixVMI_Baix: TIntegerField;
    QrVMI_BaixPecas: TFloatField;
    QrVMI_BaixAreaM2: TFloatField;
    QrFI: TmySQLQuery;
    QrFIFrmaIns: TSmallintField;
    QrFIITENS: TLargeintField;
    QrOrfaos: TmySQLQuery;
    QrSrc0: TmySQLQuery;
    QrSrcVMI: TmySQLQuery;
    QrSrcVMIControle: TIntegerField;
    QrSrcVMISdoVrtArM2: TFloatField;
    QrSrcVMISdoVrtPeca: TFloatField;
    QrVsiSorcMovimID: TIntegerField;
    QrMulRecl: TmySQLQuery;
    QrMulReclCacCod: TIntegerField;
    QrMulReclVSPaRclIts: TIntegerField;
    QrMulReclCodigo: TIntegerField;
    QrMulReclVMI_Dest: TIntegerField;
    QrMulReclVSPallet: TIntegerField;
    QrMulti: TmySQLQuery;
    QrIDs: TmySQLQuery;
    QrIDsCacID: TIntegerField;
    QrMultiITENS: TFloatField;
    QrBoxesPal: TmySQLQuery;
    QrBoxesPalCodigo: TIntegerField;
    QrBoxesPalCacCod: TIntegerField;
    QrBoxesPalVSPaXXXIts: TIntegerField;
    QrBoxesPalVSPallet: TFloatField;
    QrBoxesPalVMI_Dest: TIntegerField;
    QrBoxesPalCacID: TFloatField;
    QrSumDest1: TmySQLQuery;
    QrSumSorc1: TmySQLQuery;
    QrVMISorc1: TmySQLQuery;
    QrPalSorc1: TmySQLQuery;
    QrBoxesPalVMI_Sorc: TIntegerField;
    QrBoxesPalVMI_Baix: TIntegerField;
    QrBoxesPalTecla: TIntegerField;
    QrVSXxxCab: TmySQLQuery;
    QrVSXxxCabCodigo: TIntegerField;
    QrVSXxxCabMovimCod: TIntegerField;
    QrAsce: TmySQLQuery;
    QrAsceAreaM2: TFloatField;
    QrAsceValorT: TFloatField;
    QrDesc: TmySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox1: TGroupBox;
    dmkPermissoes1: TdmkPermissoes;
    PB1: TProgressBar;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function  ReopenVSPalletA(Pallet: Integer): Integer;

  public
    { Public declarations }
    FReabreVSPaRclCab: Boolean;
    //
    procedure AtualizaCustosDescendentesGerArtFicha(Empresa, SerieFch, Ficha:
              Integer; QrDesc: TmySQLQuery);
    function  EncerraPallet(Pallet: Integer; Pergunta: Boolean): Boolean;
    function  EncerraPalletReclassificacaoNew(VSPaRclCabCacCod,
              VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box, Box_VSPaClaIts,
              Box_VSPallet, Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
              EncerrandoTodos, Pergunta: Boolean): Boolean;
    function  EncerraVSPaRclItsA(DtHrFim: String; Controle: Integer): Boolean;
    procedure RemovePallet(VSPaRclCabCodigo, Box: Integer; Reopen: Boolean);
    procedure ReopenMulti(Pallet: Integer);

  end;

var
  DfVSMod: TDfVSMod;

implementation

uses
  Module, ModuleGeral, UMySQLModule, DmkDAC_PF, UnVS_CRC_PF, UnMyObjects;

{$R *.dfm}

{ TDfVSMod }

function TDfVSMod.EncerraPalletReclassificacaoNew(VSPaRclCabCacCod,
  VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box, Box_VSPaClaIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  EncerrandoTodos, Pergunta: Boolean): Boolean;
begin
  // Parei Aqui 2015-03-12
  UnDmkDAC_PF.AbreMySQLQuery0(QrFI, Dmod.MyDB, [
  'SELECT FrmaIns, COUNT(FrmaIns) ITENS ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(VSPaRclCabCacCod),
  'GROUP BY FrmaIns ',
  '']);
  if (QrFI.RecordCount <> 1) or (QrFIFrmaIns.Value <> 0) then
  begin
  // Nao pode ser por medidos!
    Result := EncerraPallet(Box_VSPallet, Pergunta);
    FReabreVSPaRclCab := Result;
  end else
  begin
    // Pode ser por medidos!
    // Ver no futuro como fazer se alguem pedir!!!
    (*
    EncerraPalletReclass_Orig_Known(VSPaRclCabCacCod,
    VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box,
    Box_VSPaClaIts, Box_VSPallet, Box_VMI_Sorc, Box_VMI_Baix,
    Box_VMI_Dest, EncerrandoTodos, Pergunta);
    *)
    Result := EncerraPallet(Box_VSPallet, Pergunta);
    FReabreVSPaRclCab := Result;
  end;
end;

function TDfVSMod.EncerraVSPaRclItsA(DtHrFim: String; Controle: Integer): Boolean;
  procedure ReopenVMI_Baix();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVMI_Baix, Dmod.MyDB, [
    'SELECT VMI_Baix, SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
    'FROM vscacitsa ',
    'WHERE VSPaRclIts=' + Geral.FF0(Controle),
    'GROUP BY VMI_Baix ',
    '']);
  end;
  procedure ReopenVMI_Sorc();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVMI_Sorc, Dmod.MyDB, [
    'SELECT VMI_Sorc, SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
    'FROM vscacitsa ',
    'WHERE VSPaRclIts=' + Geral.FF0(Controle),
    'GROUP BY VMI_Sorc ',
    '']);
  end;
var
  DtHrIni,
  VMI_Sorc, VMI_Baix, QIt_Sorc, QIt_Baix: Integer;
begin
  Result := False;
  //
  ReopenVMI_Sorc();
  QIt_Sorc := QrVMI_Sorc.RecordCount;
  //
  ReopenVMI_Baix();
  QIt_Baix := QrVMI_Baix.RecordCount;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclitsa', False, [
  'DtHrFim', 'QIt_Sorc', 'QIt_Baix'], [
  'Controle'], [
  DtHrFim, QIt_Sorc, QIt_Baix], [
  Controle], True);
end;

procedure TDfVSMod.FormCreate(Sender: TObject);
begin
  FReabreVSPaRclCab := False;
end;

procedure TDfVSMod.RemovePallet(VSPaRclCabCodigo, Box: Integer; Reopen: Boolean);
const
  Valor = 0;
var
  Campo: String;
  Codigo: Integer;
begin
  Campo := VS_CRC_PF.CampoLstPal(Box);
  //Codigo := QrVSPaRclCabCodigo.Value;
  Codigo := VSPaRclCabCodigo;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclcaba', False, [
  Campo], ['Codigo'], [Valor], [Codigo], True) then
  begin
    if Reopen then
      //ReopenVSPaRclCab();
      FReabreVSPaRclCab := True;
  end;
end;

procedure TDfVSMod.ReopenMulti(Pallet: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMulti, Dmod.MyDB, [
  'SELECT Count(pci.VMI_Sorc) + 0.000 ITENS ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vspaclaitsa pci ON cia.Codigo=pci.Codigo ',
  'WHERE cia.VSPallet=' + Geral.FF0(Pallet),
  'AND pci.VSPallet=' + Geral.FF0(Pallet),
  'AND cia.CacID=7 ', // Classe
  'AND pci.DtHrFim < "1900-01-01" ',
  '']);
  //Geral.MB_SQL(nil, QrMulti);
end;

function TDfVSMod.ReopenVSPalletA(Pallet: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT GraGruX ',
  'FROM vspalleta ',
  'WHERE Codigo=' + Geral.FF0(Pallet),
  '']);
  Result := QrVSPalletGraGruX.Value;
end;

procedure TDfVSMod.AtualizaCustosDescendentesGerArtFicha(Empresa, SerieFch,
  Ficha: Integer; QrDesc: TmySQLQuery);
var
  CustoM2: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Verificando necessidade de atualizar descendentes! (1)');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDesc, Dmod.myDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + '  ',
    'WHERE SerieFch=' + Geral.FF0(SerieFch),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND Ficha=' + Geral.FF0(Ficha),
    'AND MovimID NOT IN (' +
      Geral.FF0(Integer(TEstqMovimID.emidCompra)) + ',' +
      Geral.FF0(Integer(TEstqMovimID.emidIndsXX)) + ')  ',
    '']);
    //Geral.MB_SQL(Self, QrDesc);
    if QrDesc.RecordCount > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Verificando necessidade de atualizar descendentes! (2)');
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrAsce, Dmod.myDB, [
      'SELECT SUM(QtdGerArM2) AreaM2,  ',
      'SUM(ValorT) ValorT ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE Empresa=' + Geral.FF0(Empresa),
      'AND SerieFch=' + Geral.FF0(SerieFch),
      'AND Ficha=' + Geral.FF0(Ficha),
      'AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
      'AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcCurtiXX)),
      '']);
      if (QrAsceAreaM2.Value > 0) and (QrAsceValorT.Value > 0) then
      begin
        PB1.Position := 0;
        PB1.Max      := QrAsce.RecordCount;
        //
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Atualizando descendentes' );
        CustoM2 := QrAsceValorT.Value / QrAsceAreaM2.Value;
        //Geral.MB_Info('Custo da Ficha mudou para m2 = ' + Geral.FFT(CustoM2, 6, siNegativo));
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.myDB, [
        'UPDATE ' + CO_UPD_TAB_VMI + ' ',
        'SET ValorT = AreaM2 * ' + Geral.FFT_Dot(CustoM2, 10, siNegativo),
        'WHERE Empresa=' + Geral.FF0(Empresa),
        'AND SerieFch=' + Geral.FF0(SerieFch),
        'AND Ficha=' + Geral.FF0(Ficha),
        'AND MovimID NOT IN (' +
          Geral.FF0(Integer(TEstqMovimID.emidCompra)) + ',' +
          Geral.FF0(Integer(TEstqMovimID.emidIndsXX)) + ')  ',
        'AND AreaM2<>0 ',
        '']);
        // Parei Aqui!
        //VS_PF.AtualizaTotaisVSOpeCab(MovimCod: Integer);
        // VS_PF.AtualizaTotaisVSPWECab(MovimCod: Integer);
        //
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDfVSMod.EncerraPallet(Pallet: Integer; Pergunta: Boolean): Boolean;
var
  VMI_Dest, VMI_Baix, VMI_Sorc: Integer;
  VSPaXXXIts, Codigo: Integer;
  DtHrFim, TabCab, TabIts, Campo: String;
begin
  Result := False;
  //
  if (not Pergunta)
  or (Geral.MB_Pergunta('Deseja realmente encerrar o pallet ' +
  Geral.FF0(Pallet) +  ' ?') = ID_YES) then
  begin
    DtHrFim := Geral.FDT(DModG.ObtemAgora(), 109);
    UnDmkDAC_PF.AbreMySQLQuery0(QrBoxesPal, Dmod.MyDB, [
    'SELECT DISTINCT prc.Codigo, prc.CacCod, pri.Controle VSPaXXXIts, ',
    '0.000 VSPallet, pri.VMI_Dest, pri.VMI_Sorc, pri.VMI_Baix, ',
    'pri.Tecla, 7.000 CacID ',
    'FROM vspaclaitsa pri  ',
    'LEFT JOIN vspaclacaba prc ON prc.Codigo=pri.Codigo ',
    'LEFT JOIN vscacitsa cia ON pri.Controle=cia.VSPaRclIts ',
    'WHERE pri.VSPallet=' + Geral.FF0(Pallet),
    ' ',
    'UNION ',
    ' ',
    'SELECT DISTINCT prc.Codigo, prc.CacCod, pri.Controle VSPaXXXIts, ',
    'prc.VSPallet + 0.000 VSPallet, ',
    'pri.VMI_Dest, pri.VMI_Sorc, pri.VMI_Baix, ',
    'pri.Tecla, 8.000 CacID ',
    'FROM vsparclitsa pri  ',
    'LEFT JOIN vsparclcaba prc ON prc.Codigo=pri.Codigo ',
    'LEFT JOIN vscacitsa cia ON pri.Controle=cia.VSPaRclIts ',
    'WHERE pri.VSPallet=' + Geral.FF0(Pallet),
    '']);
    //
    QrBoxesPal.First;
    while not QrBoxesPal.Eof do
    begin
      VMI_Dest := QrBoxesPalVMI_Dest.Value;
      VMI_Baix := QrBoxesPalVMI_Baix.Value;
      VMI_Sorc := QrBoxesPalVMI_Sorc.Value;
      VS_CRC_PF.AtualizaVMIsDeBox(Pallet, VMI_Dest, VMI_Baix, VMI_Sorc,
        QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
      //
      VSPaXXXIts := QrBoxesPalVSPaXXXIts.Value;
      case TEstqMovimID(Trunc(QrBoxesPalCacID.Value)) of
        (*7*)emidClassArtXXUni:
        begin
          TabCab := 'vspaclacaba';
          TabIts := 'vspaclaitsa';
        end;
        (*8*)emidReclasXXUni:
        begin
          TabCab := 'vsparclcaba';
          TabIts := 'vsparclitsa';
        end;
        //(*14*)emidClassArtXXMul: ;// ????? Precisa???

         else
         begin
           TabCab := '';
           TabIts := '';
           Geral.MB_Erro('"MovimID" n�o implementado em "EncerraItensDePallet"');
         end;
      end;
      if TabIts <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabIts, False, [
        'DtHrFim'], ['Controle'], [DtHrFim], [VSPaXXXIts], True);
        //
        Campo    := 'LstPal' + FormatFloat('00', QrBoxesPalTecla.Value);
        Codigo   := QrBoxesPalCodigo.Value;
        //
        //if
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabCab, False, [
        Campo], ['Codigo', Campo], [
        0], [Codigo, Pallet], True);
      end;
      //
      QrBoxesPal.Next;
    end;

    //////
    Codigo := Pallet;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'VSPalleta', False, [
    'DtHrEndAdd'], ['Codigo'], [DtHrFim], [Codigo], True) then
    begin
      VS_CRC_PF.AtualizaStatPall(Pallet);
      Result := True;
    end;
  end;
end;

end.
