object FmVSClassifOneRet: TFmVSClassifOneRet
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: ??? ??? ???'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object PnPartida: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 44
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LaVSRibCad: TLabel
            Left = 8
            Top = 0
            Width = 53
            Height = 13
            Caption = 'Ficha RMP'
          end
          object SpeedButton1: TSpeedButton
            Left = 92
            Top = 16
            Width = 21
            Height = 21
            Caption = '>'
            OnClick = SpeedButton1Click
          end
          object EdFicha: TdmkEdit
            Left = 8
            Top = 16
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = True
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnKeyDown = EdFichaKeyDown
          end
        end
        object DBGEmClasse: TdmkDBGridZTO
          Left = 2
          Top = 59
          Width = 808
          Height = 209
          Align = alClient
          DataSource = DsVSMovIts
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGEmClasseDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_FICHA'
              Title.Caption = 'Ficha'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IMEI_NO_PRD_TAM_COR_FICHA'
              Title.Caption = 'IMEI / Artigo / Ficha'
              Width = 521
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end>
        end
        object DBGAClassificar: TdmkDBGridZTO
          Left = 2
          Top = 268
          Width = 808
          Height = 197
          Align = alBottom
          DataSource = DsVSGerArtNew
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 2
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGAClassificarDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_FICHA'
              Width = 117
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IMEI_NO_PRD_TAM_COR_FICHA'
              Title.Caption = 'Descri'#231#227'o'
              Width = 520
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IMEI'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object QrVSMovIts: TmySQLQuery
    Database = Dmod.ZZDB
    Left = 164
    Top = 52
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSMovItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrVSMovItsNO_FICHA: TWideStringField
      FieldName = 'NO_FICHA'
    end
    object QrVSMovItsCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSMovItsCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSMovItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSMovItsIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField
      FieldName = 'IMEI_NO_PRD_TAM_COR_FICHA'
      Size = 255
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 164
    Top = 100
  end
  object QrVSPaClaCab: TmySQLQuery
    Database = Dmod.MyDB
    Left = 356
    Top = 140
    object QrVSPaClaCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaClaCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
  end
  object QrVSGerArtNew: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet ')
    Left = 724
    Top = 117
    object QrVSGerArtNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerArtNewControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSGerArtNewMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGerArtNewEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerArtNewMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSGerArtNewGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerArtNewPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGerArtNewDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGerArtNewDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGerArtNewUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGerArtNewUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGerArtNewAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGerArtNewAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArtNewSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSGerArtNewSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSGerArtNewSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSGerArtNewPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSGerArtNewNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerArtNewSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGerArtNewValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSGerArtNewCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrVSGerArtNewMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSGerArtNewTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSGerArtNewCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSGerArtNewLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSGerArtNewFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSGerArtNewLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSGerArtNewDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSGerArtNewMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSGerArtNewCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSGerArtNewValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSGerArtNewDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSGerArtNewDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSGerArtNewQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrVSGerArtNewNO_FICHA: TWideStringField
      FieldName = 'NO_FICHA'
    end
    object QrVSGerArtNewCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField
      FieldName = 'IMEI_NO_PRD_TAM_COR_FICHA'
      Size = 255
    end
  end
  object DsVSGerArtNew: TDataSource
    DataSet = QrVSGerArtNew
    Left = 724
    Top = 165
  end
end
