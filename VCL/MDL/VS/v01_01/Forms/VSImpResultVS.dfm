object FmVSImpResultVS: TFmVSImpResultVS
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-170 :: Lista Resultado VS - do Prestador da M.O.'
  ClientHeight = 576
  ClientWidth = 922
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 922
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 874
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 826
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 518
        Height = 32
        Caption = 'Lista Resultado VS - do Prestador da M.O.'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 518
        Height = 32
        Caption = 'Lista Resultado VS - do Prestador da M.O.'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 518
        Height = 32
        Caption = 'Lista Resultado VS - do Prestador da M.O.'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 922
    Height = 414
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 922
      Height = 414
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 922
        Height = 221
        Align = alTop
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 315
          Height = 204
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object RGRelatorio: TRadioGroup
            Left = 0
            Top = 0
            Width = 185
            Height = 204
            Align = alLeft
            Caption = ' Relat'#243'rio: '
            ItemIndex = 0
            Items.Strings = (
              'Indefinido'
              'Resultado do prestador da M.O.'
              'WB - Custo de produ'#231#227'o'
              'Entrada de couro')
            TabOrder = 0
            OnClick = RGRelatorioClick
          end
          object GroupBox3: TGroupBox
            Left = 185
            Top = 0
            Width = 130
            Height = 204
            Align = alClient
            Caption = ' Per'#237'odo: '
            TabOrder = 1
            object TPDataIni: TdmkEditDateTimePicker
              Left = 8
              Top = 40
              Width = 112
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.000000000000000000
              Time = 0.777157974502188200
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object CkDataIni: TCheckBox
              Left = 8
              Top = 20
              Width = 112
              Height = 17
              Caption = 'Data inicial'
              Checked = True
              State = cbChecked
              TabOrder = 0
            end
            object CkDataFim: TCheckBox
              Left = 8
              Top = 64
              Width = 112
              Height = 17
              Caption = 'Data final'
              Checked = True
              State = cbChecked
              TabOrder = 2
            end
            object TPDataFim: TdmkEditDateTimePicker
              Left = 8
              Top = 84
              Width = 112
              Height = 21
              Date = 37636.000000000000000000
              Time = 0.777203761601413100
              TabOrder = 3
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
        end
        object Panel6: TPanel
          Left = 317
          Top = 15
          Width = 603
          Height = 204
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object LaClienteMO: TLabel
            Left = 8
            Top = 0
            Width = 74
            Height = 13
            Caption = 'Dono do couro:'
            Enabled = False
          end
          object LaFornecedor: TLabel
            Left = 8
            Top = 40
            Width = 137
            Height = 13
            Caption = 'Fornecedor da mat'#233'ria-prima:'
            Enabled = False
          end
          object LaProcednc: TLabel
            Left = 8
            Top = 80
            Width = 143
            Height = 13
            Caption = 'Proced'#234'ncia da mat'#233'ria-prima:'
            Enabled = False
          end
          object LaGraGruX: TLabel
            Left = 8
            Top = 120
            Width = 66
            Height = 13
            Caption = 'Mat'#233'ria-prima:'
            Enabled = False
          end
          object LaGraCusPrc: TLabel
            Left = 8
            Top = 160
            Width = 69
            Height = 13
            Caption = 'Lista de custo:'
            Enabled = False
          end
          object EdClienteMO: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBClienteMO
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBClienteMO: TdmkDBLookupComboBox
            Left = 64
            Top = 16
            Width = 453
            Height = 21
            Enabled = False
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsClientMO
            TabOrder = 1
            dmkEditCB = EdClienteMO
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdFornecedor: TdmkEditCB
            Left = 8
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFornecedor
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFornecedor: TdmkDBLookupComboBox
            Left = 64
            Top = 56
            Width = 453
            Height = 21
            Enabled = False
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsFornecedores
            TabOrder = 3
            dmkEditCB = EdFornecedor
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdProcednc: TdmkEditCB
            Left = 8
            Top = 96
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBProcednc
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBProcednc: TdmkDBLookupComboBox
            Left = 64
            Top = 96
            Width = 453
            Height = 21
            Enabled = False
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsProcedncs
            TabOrder = 5
            dmkEditCB = EdProcednc
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdGraGruX: TdmkEditCB
            Left = 8
            Top = 135
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'GraGruX'
            UpdCampo = 'GraGruX'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGraGruX
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraGruX: TdmkDBLookupComboBox
            Left = 64
            Top = 135
            Width = 453
            Height = 21
            Enabled = False
            KeyField = 'Controle'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsGraGruX
            TabOrder = 7
            dmkEditCB = EdGraGruX
            QryCampo = 'GraGruX'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdGraCusPrc: TdmkEditCB
            Left = 8
            Top = 176
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGraCusPrc
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraCusPrc: TdmkDBLookupComboBox
            Left = 64
            Top = 176
            Width = 453
            Height = 21
            Enabled = False
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsGraCusPrc
            TabOrder = 9
            dmkEditCB = EdGraCusPrc
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
      object Panel26: TPanel
        Left = 0
        Top = 221
        Width = 922
        Height = 188
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object RG00_Ordem1: TRadioGroup
          Left = 0
          Top = 0
          Width = 117
          Height = 188
          Align = alLeft
          Caption = ' Ordem 1:'
          ItemIndex = 5
          Items.Strings = (
            'MP / Artigo'
            'Fornecedor'
            'Pallet'
            'IME-I'
            'Ficha RMP'
            'Centro'
            'Local'
            'Tipo Material'
            'Est'#225'gio Artigo'
            'NFe')
          TabOrder = 0
          Visible = False
        end
        object RG00_Ordem2: TRadioGroup
          Left = 117
          Top = 0
          Width = 117
          Height = 188
          Align = alLeft
          Caption = ' Ordem 2:'
          ItemIndex = 8
          Items.Strings = (
            'MP / Artigo'
            'Fornecedor'
            'Pallet'
            'IME-I'
            'Ficha RMP'
            'Centro'
            'Local'
            'Tipo Material'
            'Est'#225'gio Artigo'
            'NFe')
          TabOrder = 1
          Visible = False
        end
        object RG00_Ordem3: TRadioGroup
          Left = 234
          Top = 0
          Width = 117
          Height = 188
          Align = alLeft
          Caption = ' Ordem 3:'
          ItemIndex = 0
          Items.Strings = (
            'MP / Artigo'
            'Fornecedor'
            'Pallet'
            'IME-I'
            'Ficha RMP'
            'Centro'
            'Local'
            'Tipo Material'
            'Est'#225'gio Artigo'
            'NFe')
          TabOrder = 2
          Visible = False
        end
        object Panel49: TPanel
          Left = 803
          Top = 0
          Width = 119
          Height = 188
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 4
          object RG00_Agrupa: TRadioGroup
            Left = 0
            Top = 0
            Width = 119
            Height = 61
            Align = alTop
            Caption = ' Agrupamentos:'
            Columns = 3
            ItemIndex = 2
            Items.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4')
            TabOrder = 0
            Visible = False
          end
          object Panel50: TPanel
            Left = 0
            Top = 61
            Width = 119
            Height = 127
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Panel58: TPanel
              Left = 0
              Top = 0
              Width = 119
              Height = 45
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
            end
          end
        end
        object RG00_Ordem4: TRadioGroup
          Left = 351
          Top = 0
          Width = 117
          Height = 188
          Align = alLeft
          Caption = ' Ordem 4:'
          ItemIndex = 2
          Items.Strings = (
            'MP / Artigo'
            'Fornecedor'
            'Pallet'
            'IME-I'
            'Ficha RMP'
            'Centro'
            'Local'
            'Tipo Material'
            'Est'#225'gio Artigo'
            'NFe')
          TabOrder = 3
          Visible = False
        end
        object RG00_Ordem5: TRadioGroup
          Left = 468
          Top = 0
          Width = 335
          Height = 188
          Align = alClient
          Caption = ' Ordem 5:'
          ItemIndex = 1
          Items.Strings = (
            'MP / Artigo'
            'Fornecedor'
            'Pallet'
            'IME-I'
            'Ficha RMP'
            'Centro'
            'Local'
            'Tipo Material'
            'Est'#225'gio Artigo'
            'NFe')
          TabOrder = 5
          Visible = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 462
    Width = 922
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 918
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 506
    Width = 922
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 776
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 774
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 13
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrClientMO: TMySQLQuery
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 308
    Top = 65520
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 308
    Top = 28
  end
  object frxWET_CURTI_164_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41909.805786724500000000
    ReportOptions.LastChange = 41909.805786724500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_164_01GetValue
    Left = 596
    Top = 256
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVSInnIts
        DataSetName = 'frxDsVSInnIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 94.488250000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Resultado Ribeira - do Prestador da M.O.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Top = 79.370130000000000000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie e ficha RMP')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 79.370130000000000000
          Width = 86.929190000000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente de m'#227'o-de-obra')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 79.370071420000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Entrada')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 79.370130000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ M.C.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 79.370130000000000000
          Width = 41.574830000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 79.370130000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTMO]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472479999999990000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Top = 79.370130000000000000
          Width = 117.165430000000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima / artigo')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 79.370130000000000000
          Width = 79.370086060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472479999999990000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-prima / artigo: [VARF_MPRIMAART]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 468.661720000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 415.748300000000000000
        Width = 680.315400000000000000
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 487.559370000000000000
          Height = 18.897640240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DO PER'#205'ODO: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 18.897640240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM>,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 3.779530000000022000
          Width = 41.574830000000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."Pecas">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesoKg">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSInnIts
        DataSetName = 'frxDsVSInnIts'
        RowCount = 0
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."NO_SerieFch"] [frxDsVSInnIts."Ficha"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Width = 86.929190000000000000
          Height = 15.118110240000000000
          DataField = 'NO_CLIENTEMO'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."NO_CLIENTEMO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149601420000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."DataHora"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_VAL_ITEM]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 41.574830000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."Pecas"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 117.165430000000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 411.968770000000000000
          Width = 75.590556060000000000
          Height = 15.118110240000000000
          DataField = 'Marca'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Marca"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 173.858380000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSInnIts."NO_CLIENTEMO"'
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897640240000000000
          DataField = 'NO_CLIENTEMO'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."NO_CLIENTEMO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 336.378170000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Width = 487.559370000000000000
          Height = 18.897640240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."NO_CLIENTEMO"] ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897640240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM>,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 41.574830000000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."Pecas">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesoKg">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSInnIts."NO_PRD_TAM_COR"'
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897640240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              '[frxDsVSInnIts."NO_CLIENTEMO"] : [frxDsVSInnIts."NO_PRD_TAM_COR"' +
              ']')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Width = 487.559370000000000000
          Height = 18.897640240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[frxDsVSInnIts."NO_CLIENTEMO"] : [frxDsVSInnIts."NO_PRD_TAM_COR"' +
              ']')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897640240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<VARF_VAL_ITEM>,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 41.574830000000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."Pecas">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesoKg">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrVSInnIts: TMySQLQuery
    AfterScroll = QrVSInnItsAfterScroll
    SQL.Strings = (
      'SELECT  "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, '
      'CAST(vmi.Codigo AS SIGNED) Codigo, '
      'CAST(vmi.Controle AS SIGNED) Controle, '
      'CAST(vmi.MovimCod AS SIGNED) MovimCod, '
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, '
      'CAST(vmi.MovimTwn AS SIGNED) MovimTwn, '
      'CAST(vmi.Empresa AS SIGNED) Empresa, '
      'CAST(vmi.ClientMO AS SIGNED) ClientMO, '
      'CAST(vmi.Terceiro AS SIGNED) Terceiro, '
      'CAST(vmi.CliVenda AS SIGNED) CliVenda, '
      'CAST(vmi.MovimID AS SIGNED) MovimID, '
      'CAST(vmi.DataHora AS DATETIME) DataHora, '
      'CAST(vmi.Pallet AS SIGNED) Pallet, '
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, '
      'CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas, '
      'CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg, '
      'CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2, '
      'CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2, '
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, '
      'CAST(vmi.SrcMovID AS SIGNED) SrcMovID, '
      'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1, '
      'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2, '
      'CAST(vmi.SrcGGX AS SIGNED) SrcGGX, '
      'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca, '
      'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso, '
      'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2, '
      'CAST(vmi.Observ AS CHAR) Observ, '
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, '
      'CAST(vmi.Ficha AS SIGNED) Ficha, '
      'CAST(vmi.Misturou AS UNSIGNED) Misturou, '
      'CAST(vmi.FornecMO AS SIGNED) FornecMO, '
      'CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg, '
      'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, '
      'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP, '
      'CAST(vmi.CustoPQ AS DECIMAL (15,4)) CustoPQ, '
      'CAST(vmi.DstMovID AS SIGNED) DstMovID, '
      'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1, '
      'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2, '
      'CAST(vmi.DstGGX AS SIGNED) DstGGX, '
      'CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca, '
      'CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, '
      'CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, '
      'CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, '
      'CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca, '
      'CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso, '
      'CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2, '
      'CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2, '
      'CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG, '
      'CAST(vmi.Marca AS CHAR) Marca, '
      'CAST(vmi.PedItsLib AS SIGNED) PedItsLib, '
      'CAST(vmi.PedItsFin AS SIGNED) PedItsFin, '
      'CAST(vmi.PedItsVda AS SIGNED) PedItsVda, '
      'CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2, '
      'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq, '
      'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc, '
      'CAST(vmi.ItemNFe AS SIGNED) ItemNFe, '
      'CAST(vmi.VSMulFrnCab AS SIGNED) VSMulFrnCab, '
      'CAST(vmi.NFeSer AS SIGNED) NFeSer, '
      'CAST(vmi.NFeNum AS SIGNED) NFeNum, '
      'CAST(vmi.VSMulNFeCab AS SIGNED) VSMulNFeCab, '
      'CAST(vmi.JmpMovID AS SIGNED) JmpMovID, '
      'CAST(vmi.JmpNivel1 AS SIGNED) JmpNivel1, '
      'CAST(vmi.JmpNivel2 AS SIGNED) JmpNivel2, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      ' '
      'vsp.Nome NO_Pallet, '
      'IF(vmi.SdoVrtPeca > 0, 0, '
      
        '  IF(vmi.QtdGerArM2 > 0, vmi.QtdGerArM2 / vmi.Pecas, 0)) RendKgm' +
        '2, '
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT( '
      '  IF(vmi.QtdGerArM2 > 0, vmi.PesoKg/vmi.QtdGerArM2, 0), 3), '
      '  ",", ""), ".", ",")) RendKgm2_TXT, '
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE( '
      'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, '
      'IF(vmi.Misturou = 1, "SIM", "N'#195'O") Misturou_TXT, '
      'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, '
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT( '
      '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), '
      '  ",", ""), ".", ",")) m2_CouroTXT, '
      'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, '
      'vmi.ClientMO, fch.Nome NO_SerieFch, '
      'cab.*, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, '
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO, '
      'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC, '
      'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA '
      ' '
      ' '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      ' '
      'LEFT JOIN vspalleta vsp ON vsp.Codigo=vmi.Pallet '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed '
      'LEFT JOIN vsserfch  fch ON fch.Codigo=vmi.SerieFch '
      'LEFT JOIN vsinncab  cab ON cab.MovimCod=vmi.MovimCod '
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Empresa '
      'LEFT JOIN entidades frn ON frn.Codigo=cab.Fornecedor '
      'LEFT JOIN entidades trn ON trn.Codigo=cab.Transporta '
      'LEFT JOIN entidades cli ON cli.Codigo=cab.ClienteMO '
      'LEFT JOIN entidades prc ON prc.Codigo=cab.Procednc '
      'LEFT JOIN entidades mot ON mot.Codigo=cab.Motorista '
      ' '
      ' '
      'WHERE vmi.MovimCod=0 '
      '')
    Left = 668
    Top = 237
    object QrVSInnItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSInnItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSInnItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSInnItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSInnItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSInnItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSInnItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSInnItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSInnItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSInnItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSInnItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSInnItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSInnItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSInnItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSInnItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSInnItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSInnItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSInnItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSInnItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSInnItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSInnItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSInnItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSInnItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSInnItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSInnItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSInnItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSInnItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSInnItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSInnItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSInnItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSInnItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSInnItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSInnItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSInnItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSInnItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSInnItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSInnItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSInnItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSInnItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSInnItsRendKgm2: TFloatField
      FieldName = 'RendKgm2'
      DisplayFormat = '0.000'
    end
    object QrVSInnItsNotaMPAG_TXT: TWideStringField
      FieldName = 'NotaMPAG_TXT'
      Size = 30
    end
    object QrVSInnItsRendKgm2_TXT: TWideStringField
      FieldName = 'RendKgm2_TXT'
      Size = 30
    end
    object QrVSInnItsMisturou_TXT: TWideStringField
      FieldName = 'Misturou_TXT'
      Size = 3
    end
    object QrVSInnItsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrVSInnItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrVSInnItsm2_CouroTXT: TWideStringField
      FieldName = 'm2_CouroTXT'
      Size = 30
    end
    object QrVSInnItsKgMedioCouro: TFloatField
      FieldName = 'KgMedioCouro'
      DisplayFormat = '0.000'
    end
    object QrVSInnItsVSMulFrnCab: TLargeintField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSInnItsClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSInnItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSInnItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
    end
    object QrVSInnItsPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrVSInnItsPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrVSInnItsItemNFe: TLargeintField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrVSInnItsNFeSer: TLargeintField
      FieldName = 'NFeSer'
      Required = True
    end
    object QrVSInnItsNFeNum: TLargeintField
      FieldName = 'NFeNum'
      Required = True
    end
    object QrVSInnItsVSMulNFeCab: TLargeintField
      FieldName = 'VSMulNFeCab'
      Required = True
    end
    object QrVSInnItsJmpMovID: TLargeintField
      FieldName = 'JmpMovID'
      Required = True
    end
    object QrVSInnItsJmpNivel1: TLargeintField
      FieldName = 'JmpNivel1'
      Required = True
    end
    object QrVSInnItsJmpNivel2: TLargeintField
      FieldName = 'JmpNivel2'
      Required = True
    end
    object QrVSInnItsClientMO_1: TIntegerField
      FieldName = 'ClientMO_1'
    end
    object QrVSInnItsCodigo_1: TIntegerField
      FieldName = 'Codigo_1'
      Required = True
    end
    object QrVSInnItsMovimCod_1: TIntegerField
      FieldName = 'MovimCod_1'
      Required = True
    end
    object QrVSInnItsEmpresa_1: TIntegerField
      FieldName = 'Empresa_1'
    end
    object QrVSInnItsDtCompra: TDateTimeField
      FieldName = 'DtCompra'
    end
    object QrVSInnItsDtViagem: TDateTimeField
      FieldName = 'DtViagem'
    end
    object QrVSInnItsDtEntrada: TDateTimeField
      FieldName = 'DtEntrada'
    end
    object QrVSInnItsFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrVSInnItsTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrVSInnItsPecas_1: TFloatField
      FieldName = 'Pecas_1'
    end
    object QrVSInnItsPesoKg_1: TFloatField
      FieldName = 'PesoKg_1'
    end
    object QrVSInnItsAreaM2_1: TFloatField
      FieldName = 'AreaM2_1'
    end
    object QrVSInnItsAreaP2_1: TFloatField
      FieldName = 'AreaP2_1'
    end
    object QrVSInnItsValorT_1: TFloatField
      FieldName = 'ValorT_1'
    end
    object QrVSInnItsClienteMO: TIntegerField
      FieldName = 'ClienteMO'
    end
    object QrVSInnItsProcednc: TIntegerField
      FieldName = 'Procednc'
    end
    object QrVSInnItsMotorista: TIntegerField
      FieldName = 'Motorista'
    end
    object QrVSInnItsPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrVSInnItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSInnItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSInnItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSInnItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSInnItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSInnItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSInnItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSInnItsTemIMEIMrt: TSmallintField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSInnItside_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrVSInnItside_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrVSInnItsemi_serie: TIntegerField
      FieldName = 'emi_serie'
    end
    object QrVSInnItsemi_nNF: TIntegerField
      FieldName = 'emi_nNF'
    end
    object QrVSInnItsNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
    end
    object QrVSInnItsNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSInnItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSInnItsNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
    object QrVSInnItsNO_CLIENTEMO: TWideStringField
      FieldName = 'NO_CLIENTEMO'
      Size = 100
    end
    object QrVSInnItsNO_PROCEDNC: TWideStringField
      FieldName = 'NO_PROCEDNC'
      Size = 100
    end
    object QrVSInnItsNO_MOTORISTA: TWideStringField
      FieldName = 'NO_MOTORISTA'
      Size = 100
    end
  end
  object DsVSInnIts: TDataSource
    DataSet = QrVSInnIts
    Left = 672
    Top = 281
  end
  object frxDsVSInnIts: TfrxDBDataset
    UserName = 'frxDsVSInnIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'PedItsFin=PedItsFin'
      'Marca=Marca'
      'StqCenLoc=StqCenLoc'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'ReqMovEstq=ReqMovEstq'
      'RendKgm2=RendKgm2'
      'NotaMPAG_TXT=NotaMPAG_TXT'
      'RendKgm2_TXT=RendKgm2_TXT'
      'Misturou_TXT=Misturou_TXT'
      'NOMEUNIDMED=NOMEUNIDMED'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'm2_CouroTXT=m2_CouroTXT'
      'KgMedioCouro=KgMedioCouro'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'NO_SerieFch=NO_SerieFch'
      'CustoPQ=CustoPQ'
      'PedItsLib=PedItsLib'
      'PedItsVda=PedItsVda'
      'ItemNFe=ItemNFe'
      'NFeSer=NFeSer'
      'NFeNum=NFeNum'
      'VSMulNFeCab=VSMulNFeCab'
      'JmpMovID=JmpMovID'
      'JmpNivel1=JmpNivel1'
      'JmpNivel2=JmpNivel2'
      'ClientMO_1=ClientMO_1'
      'Codigo_1=Codigo_1'
      'MovimCod_1=MovimCod_1'
      'Empresa_1=Empresa_1'
      'DtCompra=DtCompra'
      'DtViagem=DtViagem'
      'DtEntrada=DtEntrada'
      'Fornecedor=Fornecedor'
      'Transporta=Transporta'
      'Pecas_1=Pecas_1'
      'PesoKg_1=PesoKg_1'
      'AreaM2_1=AreaM2_1'
      'AreaP2_1=AreaP2_1'
      'ValorT_1=ValorT_1'
      'ClienteMO=ClienteMO'
      'Procednc=Procednc'
      'Motorista=Motorista'
      'Placa=Placa'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'TemIMEIMrt=TemIMEIMrt'
      'ide_serie=ide_serie'
      'ide_nNF=ide_nNF'
      'emi_serie=emi_serie'
      'emi_nNF=emi_nNF'
      'NFeStatus=NFeStatus'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_FORNECE=NO_FORNECE'
      'NO_TRANSPORTA=NO_TRANSPORTA'
      'NO_CLIENTEMO=NO_CLIENTEMO'
      'NO_PROCEDNC=NO_PROCEDNC'
      'NO_MOTORISTA=NO_MOTORISTA')
    DataSet = QrVSInnIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 672
    Top = 328
  end
  object QrVSMovIts: TMySQLQuery
    SQL.Strings = (
      'SELECT  '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN 1 '
      '  WHEN 23 THEN 1 '
      '  WHEN 26 THEN 2 '
      '  WHEN 27 THEN 2 '
      'ELSE 9 END ID_GRUPO,   '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN "RECEITAS" '
      '  WHEN 23 THEN "RECEITAS" '
      '  WHEN 26 THEN "DESPESAS" '
      '  WHEN 27 THEN "DESPESAS" '
      'ELSE "OUTROS" END NO_GRUPO,   '
      'CASE vmi.MovimID  '
      '  WHEN 01 THEN 101 '
      '  WHEN 23 THEN 102 '
      '  WHEN 26 THEN 201 '
      '  WHEN 27 THEN 202 '
      'ELSE 999 END ORDEM2,   '
      'vmi.*,  '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Entrada","Sa'#237'da","Reclasse","Baixa"' +
        ',"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado"' +
        ',"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","' +
        'Pr'#233' reclasse","Compra de Classificado","Baixa extra","Saldo ante' +
        'rior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub P' +
        'roduto","Reclasse Mul.","Transfer. Local","Em proc. caleiro","Em' +
        ' proc. curtimento") NO_MovimID, '
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto","Origem de transf. de local","Destino de ' +
        'transf. de local","Origem caleiro em processo","Caleiro em proce' +
        'sso","Destino caleiro em processo","Baixa de caleiro em processo' +
        '","Artigo de caleiro","Origem curtimento em processo","Curtiment' +
        'o em processo","Destino curtimento em processo","Baixa de curtim' +
        'ento em processo","Artigo de curtimento") NO_MovimNiv, '
      'ggx.GraGru1, CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,  '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,  '
      'ggx.GraGruY, ggy.Nome NO_GGY '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY '
      'WHERE vmi.SerieFch=2 '
      'AND vmi.Ficha=16 '
      'AND (NOT MovimNiv IN (14,15,30,35))  '
      'ORDER BY ORDEM2  '
      '  ')
    Left = 740
    Top = 260
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrVSMovItsMargem: TFloatField
      FieldName = 'Margem'
    end
  end
  object frxDsVSMovIts: TfrxDBDataset
    UserName = 'frxDsVSMovIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SrcGGX=SrcGGX'
      'SdoVrtPeso=SdoVrtPeso'
      'SerieFch=SerieFch'
      'FornecMO=FornecMO'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'TpCalcAuto=TpCalcAuto'
      'Zerado=Zerado'
      'EmFluxo=EmFluxo'
      'LnkIDXtr=LnkIDXtr'
      'CustoMOM2=CustoMOM2'
      'NotFluxo=NotFluxo'
      'FatNotaVNC=FatNotaVNC'
      'FatNotaVRC=FatNotaVRC'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'GSPInnNiv2=GSPInnNiv2'
      'GSPArtNiv2=GSPArtNiv2'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'VSMorCab=VSMorCab'
      'VSMulFrnCab=VSMulFrnCab'
      'ClientMO=ClientMO'
      'CustoPQ=CustoPQ'
      'NO_MovimID=NO_MovimID'
      'NO_MovimNiv=NO_MovimNiv'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'CODUSUUNIDMED=CODUSUUNIDMED'
      'NOMEUNIDMED=NOMEUNIDMED'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'KgCouPQ=KgCouPQ'
      'ID_GRUPO=ID_GRUPO'
      'NO_GRUPO=NO_GRUPO'
      'ORDEM2=ORDEM2')
    OpenDataSource = False
    DataSet = QrVSMovIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 740
    Top = 312
  end
  object QrGraGruX: TMySQLQuery
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 796
    Top = 276
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 796
    Top = 324
  end
  object QrFornecedores: TMySQLQuery
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 384
    Top = 65520
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 384
    Top = 28
  end
  object QrProcedncs: TMySQLQuery
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 456
    Top = 65520
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsProcedncs: TDataSource
    DataSet = QrProcedncs
    Left = 456
    Top = 28
  end
  object QrGraCusPrc: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM gracusprc'
      'ORDER BY Nome')
    Left = 528
    Top = 65520
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 528
    Top = 28
  end
  object QrClasses: TMySQLQuery
    SQL.Strings = (
      'SELECT IF(gg1.Referencia <> '#39#39', gg1.Referencia, '
      
        'CONCAT("Red.",ggx.Controle)) Referencia, vmi.GraGruX, SUM(AreaM2' +
        ') AreaM2, '
      'val.CustoPreco '
      'FROM bluederm_cialeather.vsmovits vmi '
      
        'LEFT JOIN bluederm_cialeather.gragrux ggx ON ggx.Controle=vmi.Gr' +
        'aGruX '
      
        'LEFT JOIN bluederm_cialeather.gragru1 gg1 ON gg1.Nivel1=ggx.GraG' +
        'ru1 '
      'LEFT JOIN _Res_MovID_Fch_ fch '
      '  ON fch.SerieFch=vmi.SerieFch '
      '  AND fch.Ficha=vmi.Ficha '
      'LEFT JOIN bluederm_cialeather.gragruval val '
      '  ON val.GraGruX=vmi.GraGruX '
      '  AND val.GraCusPrc=6 '
      '  AND val.Entidade=-11 '
      'WHERE MovimID IN (7, 14) '
      'AND MovimNiv=2 '
      'AND DataHora >= "2018-12-01" '
      'AND AreaM2>0 '
      'GROUP BY vmi.GraGruX '
      'ORDER BY vmi.AreaM2 DESC ')
    Left = 220
    Top = 180
    object QrClassesReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrClassesGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrClassesAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrClassesCustoPreco: TFloatField
      FieldName = 'CustoPreco'
    end
  end
  object QrMargem: TMySQLQuery
    SQL.Strings = (
      'SELECT res.*,'
      
        'IF(AreaM2_01=0,0,((Valor_01 - Custo_01) / Custo_01) * 100) Marge' +
        'm_01,'
      
        'IF(AreaM2_02=0,0,((Valor_02 - Custo_02) / Custo_02) * 100) Marge' +
        'm_02,'
      
        'IF(AreaM2_03=0,0,((Valor_03 - Custo_03) / Custo_03) * 100) Marge' +
        'm_03,'
      
        'IF(AreaM2_04=0,0,((Valor_04 - Custo_04) / Custo_04) * 100) Marge' +
        'm_04,'
      
        'IF(AreaM2_05=0,0,((Valor_05 - Custo_05) / Custo_05) * 100) Marge' +
        'm_05,'
      
        'IF(AreaM2_06=0,0,((Valor_06 - Custo_06) / Custo_06) * 100) Marge' +
        'm_06,'
      
        'IF(AreaM2_07=0,0,((Valor_07 - Custo_07) / Custo_07) * 100) Marge' +
        'm_07,'
      
        'IF(AreaM2_08=0,0,((Valor_08 - Custo_08) / Custo_08) * 100) Marge' +
        'm_08,'
      
        'IF(AreaM2_09=0,0,((Valor_09 - Custo_09) / Custo_09) * 100) Marge' +
        'm_09,'
      
        'IF(AreaM2_10=0,0,((Valor_10 - Custo_10) / Custo_10) * 100) Marge' +
        'm_10,'
      
        'IF(AreaM2_11=0,0,((Valor_11 - Custo_11) / Custo_11) * 100) Marge' +
        'm_11,'
      
        'IF(AreaM2_12=0,0,((Valor_12 - Custo_12) / Custo_12) * 100) Marge' +
        'm_12,'
      
        'IF(AreaM2_13=0,0,((Valor_13 - Custo_13) / Custo_13) * 100) Marge' +
        'm_13,'
      
        'IF(AreaM2_14=0,0,((Valor_14 - Custo_14) / Custo_14) * 100) Marge' +
        'm_14,'
      
        'IF(AreaM2_15=0,0,((Valor_15 - Custo_15) / Custo_15) * 100) Marge' +
        'm_15,'
      'Valor_01 + Valor_02 + Valor_03 +'
      'Valor_04 + Valor_05 + Valor_06 +'
      'Valor_07 + Valor_08 + Valor_09 +'
      'Valor_10 + Valor_11 + Valor_12 +'
      'Valor_13 + Valor_14 + Valor_15 ValorTot,'
      'Custo_01 + Custo_02 + Custo_03 +'
      'Custo_04 + Custo_05 + Custo_06 +'
      'Custo_07 + Custo_08 + Custo_09 +'
      'Custo_10 + Custo_11 + Custo_12 +'
      'Custo_13 + Custo_14 + Custo_15 CustoTot,'
      '(Valor_01 + Valor_02 + Valor_03 +'
      'Valor_04 + Valor_05 + Valor_06 +'
      'Valor_07 + Valor_08 + Valor_09 +'
      'Valor_10 + Valor_11 + Valor_12 +'
      'Valor_13 + Valor_14 + Valor_15)'
      '-'
      '(Custo_01 + Custo_02 + Custo_03 +'
      'Custo_04 + Custo_05 + Custo_06 +'
      'Custo_07 + Custo_08 + Custo_09 +'
      'Custo_10 + Custo_11 + Custo_12 +'
      'Custo_13 + Custo_14 + Custo_15) MargemTot,'
      '(((Valor_01 + Valor_02 + Valor_03 +'
      'Valor_04 + Valor_05 + Valor_06 +'
      'Valor_07 + Valor_08 + Valor_09 +'
      'Valor_10 + Valor_11 + Valor_12 +'
      'Valor_13 + Valor_14 + Valor_15)'
      '-'
      '(Custo_01 + Custo_02 + Custo_03 +'
      'Custo_04 + Custo_05 + Custo_06 +'
      'Custo_07 + Custo_08 + Custo_09 +'
      'Custo_10 + Custo_11 + Custo_12 +'
      'Custo_13 + Custo_14 + Custo_15))'
      '/'
      '(Custo_01 + Custo_02 + Custo_03 +'
      'Custo_04 + Custo_05 + Custo_06 +'
      'Custo_07 + Custo_08 + Custo_09 +'
      'Custo_10 + Custo_11 + Custo_12 +'
      'Custo_13 + Custo_14 + Custo_15))'
      '* 100 MargemPer, '
      'CONCAT(vsf.Nome, " ", res.Ficha) NO_SERIE_FICHA '
      'FROM _res_movid0702_1402_ res'
      
        'LEFT JOIN bluederm_cialeather.vsserfch   vsf ON vsf.Codigo=res.S' +
        'erieFch ')
    Left = 672
    Top = 388
    object QrMargemDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrMargemSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrMargemFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrMargemPecasTt: TFloatField
      FieldName = 'PecasTt'
    end
    object QrMargemAreaM2Tt: TFloatField
      FieldName = 'AreaM2Tt'
    end
    object QrMargemPecas_01: TFloatField
      FieldName = 'Pecas_01'
    end
    object QrMargemAreaM2_01: TFloatField
      FieldName = 'AreaM2_01'
    end
    object QrMargemValor_01: TFloatField
      FieldName = 'Valor_01'
    end
    object QrMargemCusto_01: TFloatField
      FieldName = 'Custo_01'
    end
    object QrMargemCusM2_01: TFloatField
      FieldName = 'CusM2_01'
    end
    object QrMargemPecas_02: TFloatField
      FieldName = 'Pecas_02'
    end
    object QrMargemAreaM2_02: TFloatField
      FieldName = 'AreaM2_02'
    end
    object QrMargemValor_02: TFloatField
      FieldName = 'Valor_02'
    end
    object QrMargemCusto_02: TFloatField
      FieldName = 'Custo_02'
    end
    object QrMargemCusM2_02: TFloatField
      FieldName = 'CusM2_02'
    end
    object QrMargemPecas_03: TFloatField
      FieldName = 'Pecas_03'
    end
    object QrMargemAreaM2_03: TFloatField
      FieldName = 'AreaM2_03'
    end
    object QrMargemValor_03: TFloatField
      FieldName = 'Valor_03'
    end
    object QrMargemCusto_03: TFloatField
      FieldName = 'Custo_03'
    end
    object QrMargemCusM2_03: TFloatField
      FieldName = 'CusM2_03'
    end
    object QrMargemPecas_04: TFloatField
      FieldName = 'Pecas_04'
    end
    object QrMargemAreaM2_04: TFloatField
      FieldName = 'AreaM2_04'
    end
    object QrMargemValor_04: TFloatField
      FieldName = 'Valor_04'
    end
    object QrMargemCusto_04: TFloatField
      FieldName = 'Custo_04'
    end
    object QrMargemCusM2_04: TFloatField
      FieldName = 'CusM2_04'
    end
    object QrMargemPecas_05: TFloatField
      FieldName = 'Pecas_05'
    end
    object QrMargemAreaM2_05: TFloatField
      FieldName = 'AreaM2_05'
    end
    object QrMargemValor_05: TFloatField
      FieldName = 'Valor_05'
    end
    object QrMargemCusto_05: TFloatField
      FieldName = 'Custo_05'
    end
    object QrMargemCusM2_05: TFloatField
      FieldName = 'CusM2_05'
    end
    object QrMargemPecas_06: TFloatField
      FieldName = 'Pecas_06'
    end
    object QrMargemAreaM2_06: TFloatField
      FieldName = 'AreaM2_06'
    end
    object QrMargemValor_06: TFloatField
      FieldName = 'Valor_06'
    end
    object QrMargemCusto_06: TFloatField
      FieldName = 'Custo_06'
    end
    object QrMargemCusM2_06: TFloatField
      FieldName = 'CusM2_06'
    end
    object QrMargemPecas_07: TFloatField
      FieldName = 'Pecas_07'
    end
    object QrMargemAreaM2_07: TFloatField
      FieldName = 'AreaM2_07'
    end
    object QrMargemValor_07: TFloatField
      FieldName = 'Valor_07'
    end
    object QrMargemCusto_07: TFloatField
      FieldName = 'Custo_07'
    end
    object QrMargemPecas_08: TFloatField
      FieldName = 'Pecas_08'
    end
    object QrMargemAreaM2_08: TFloatField
      FieldName = 'AreaM2_08'
    end
    object QrMargemValor_08: TFloatField
      FieldName = 'Valor_08'
    end
    object QrMargemCusto_08: TFloatField
      FieldName = 'Custo_08'
    end
    object QrMargemPecas_09: TFloatField
      FieldName = 'Pecas_09'
    end
    object QrMargemAreaM2_09: TFloatField
      FieldName = 'AreaM2_09'
    end
    object QrMargemValor_09: TFloatField
      FieldName = 'Valor_09'
    end
    object QrMargemCusto_09: TFloatField
      FieldName = 'Custo_09'
    end
    object QrMargemPecas_10: TFloatField
      FieldName = 'Pecas_10'
    end
    object QrMargemAreaM2_10: TFloatField
      FieldName = 'AreaM2_10'
    end
    object QrMargemValor_10: TFloatField
      FieldName = 'Valor_10'
    end
    object QrMargemCusto_10: TFloatField
      FieldName = 'Custo_10'
    end
    object QrMargemPecas_11: TFloatField
      FieldName = 'Pecas_11'
    end
    object QrMargemAreaM2_11: TFloatField
      FieldName = 'AreaM2_11'
    end
    object QrMargemValor_11: TFloatField
      FieldName = 'Valor_11'
    end
    object QrMargemCusto_11: TFloatField
      FieldName = 'Custo_11'
    end
    object QrMargemPecas_12: TFloatField
      FieldName = 'Pecas_12'
    end
    object QrMargemAreaM2_12: TFloatField
      FieldName = 'AreaM2_12'
    end
    object QrMargemValor_12: TFloatField
      FieldName = 'Valor_12'
    end
    object QrMargemCusto_12: TFloatField
      FieldName = 'Custo_12'
    end
    object QrMargemPecas_13: TFloatField
      FieldName = 'Pecas_13'
    end
    object QrMargemAreaM2_13: TFloatField
      FieldName = 'AreaM2_13'
    end
    object QrMargemValor_13: TFloatField
      FieldName = 'Valor_13'
    end
    object QrMargemCusto_13: TFloatField
      FieldName = 'Custo_13'
    end
    object QrMargemPecas_14: TFloatField
      FieldName = 'Pecas_14'
    end
    object QrMargemAreaM2_14: TFloatField
      FieldName = 'AreaM2_14'
    end
    object QrMargemValor_14: TFloatField
      FieldName = 'Valor_14'
    end
    object QrMargemCusto_14: TFloatField
      FieldName = 'Custo_14'
    end
    object QrMargemPecas_15: TFloatField
      FieldName = 'Pecas_15'
    end
    object QrMargemAreaM2_15: TFloatField
      FieldName = 'AreaM2_15'
    end
    object QrMargemValor_15: TFloatField
      FieldName = 'Valor_15'
    end
    object QrMargemCusto_15: TFloatField
      FieldName = 'Custo_15'
    end
    object QrMargemMargem_01: TFloatField
      FieldName = 'Margem_01'
    end
    object QrMargemMargem_02: TFloatField
      FieldName = 'Margem_02'
    end
    object QrMargemMargem_03: TFloatField
      FieldName = 'Margem_03'
    end
    object QrMargemMargem_04: TFloatField
      FieldName = 'Margem_04'
    end
    object QrMargemMargem_05: TFloatField
      FieldName = 'Margem_05'
    end
    object QrMargemMargem_06: TFloatField
      FieldName = 'Margem_06'
    end
    object QrMargemMargem_07: TFloatField
      FieldName = 'Margem_07'
    end
    object QrMargemMargem_08: TFloatField
      FieldName = 'Margem_08'
    end
    object QrMargemMargem_09: TFloatField
      FieldName = 'Margem_09'
    end
    object QrMargemMargem_10: TFloatField
      FieldName = 'Margem_10'
    end
    object QrMargemMargem_11: TFloatField
      FieldName = 'Margem_11'
    end
    object QrMargemMargem_12: TFloatField
      FieldName = 'Margem_12'
    end
    object QrMargemMargem_13: TFloatField
      FieldName = 'Margem_13'
    end
    object QrMargemMargem_14: TFloatField
      FieldName = 'Margem_14'
    end
    object QrMargemMargem_15: TFloatField
      FieldName = 'Margem_15'
    end
    object QrMargemValorTot: TFloatField
      FieldName = 'ValorTot'
    end
    object QrMargemCustoTot: TFloatField
      FieldName = 'CustoTot'
    end
    object QrMargemMargemTot: TFloatField
      FieldName = 'MargemTot'
    end
    object QrMargemMargemPer: TFloatField
      FieldName = 'MargemPer'
    end
    object QrMargemNO_SERIE_FICHA: TWideStringField
      FieldName = 'NO_SERIE_FICHA'
      Size = 72
    end
  end
  object frxWET_CURTI_164_02: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41909.805786724500000000
    ReportOptions.LastChange = 41909.805786724500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_164_01GetValue
    Left = 600
    Top = 304
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMargem
        DataSetName = 'frxDsMargem'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 124.724490000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 1046.929810000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 744.567410000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Margem Artigos de Ribeira Classificados')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748610000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Top = 94.488250000000000000
          Width = 68.031459450000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie e ficha RMP')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 94.488250000000000000
          Width = 41.574803149606300000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 109.606370000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTMO]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472479999999990000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 94.488250000000000000
          Width = 37.795300000000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as tot.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 94.488250000000000000
          Width = 109.606326060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[COURO_01]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472479999999990000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-prima / artigo: [VARF_MPRIMAART]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 109.606311420000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Entrada In Natura')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 79.370130000000000000
          Width = 937.323381420000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Classifica'#231#227'o de Artigo Gerado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Top = 94.488250000000000000
          Width = 49.133890000000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea tot.m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Top = 109.606370000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 94.488250000000000000
          Width = 37.795280470000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'cus$m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Top = 109.606370000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% M.C.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 109.606370000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 94.488250000000000000
          Width = 109.606326060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[COURO_02]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 109.606370000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 109.606370000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% M.C.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 109.606370000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 94.488250000000000000
          Width = 109.606326060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[COURO_03]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Top = 109.606370000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Top = 109.606370000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% M.C.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Top = 109.606370000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Top = 94.488250000000000000
          Width = 109.606326060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[COURO_04]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 744.567410000000000000
          Top = 109.606370000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 789.921770000000000000
          Top = 109.606370000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% M.C.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 823.937540000000000000
          Top = 109.606370000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 823.937540000000000000
          Top = 94.488250000000000000
          Width = 109.606326060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[COURO_05]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 857.953310000000000000
          Top = 109.606370000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Top = 109.606370000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% M.C.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 937.323440000000000000
          Top = 109.606370000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 937.323440000000000000
          Top = 94.488250000000000000
          Width = 109.606326060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[COURO_06]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 971.339210000000000000
          Top = 109.606370000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 1016.693570000000000000
          Top = 109.606370000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% M.C.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 94.488250000000000000
          Width = 49.133858267716530000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 94.488250000000000000
          Width = 52.913385830000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 94.488250000000000000
          Width = 30.236240000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% M.C.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 325.039580000000000000
        Width = 1046.929810000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 480.000310000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Width = 642.520100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 204.094620000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsMargem
        DataSetName = 'frxDsMargem'
        RowCount = 0
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 68.031459450000000000
          Height = 15.118110240000000000
          DataField = 'NO_SERIE_FICHA'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMargem."NO_SERIE_FICHA"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 41.574803149606300000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMargem."DataHora"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataField = 'PecasTt'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."PecasTt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2Tt'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."AreaM2Tt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataField = 'Pecas_01'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Pecas_01"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2_01'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."AreaM2_01"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 37.795280470000000000
          Height = 15.118110240000000000
          DataField = 'CusM2_01'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."CusM2_01"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataField = 'Margem_01'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Margem_01"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataField = 'Pecas_02'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Pecas_02"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2_02'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."AreaM2_02"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataField = 'Margem_02'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Margem_02"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataField = 'Pecas_03'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Pecas_03"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2_03'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."AreaM2_03"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataField = 'Margem_03'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Margem_03"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataField = 'Pecas_04'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Pecas_04"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 744.567410000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2_04'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."AreaM2_04"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 789.921770000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataField = 'Margem_04'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Margem_04"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 823.937540000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataField = 'Pecas_05'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Pecas_05"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 857.953310000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2_05'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."AreaM2_05"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataField = 'Margem_05'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Margem_05"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 937.323440000000000000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataField = 'Pecas_06'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Pecas_06"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 971.339210000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2_06'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."AreaM2_06"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 1016.693570000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataField = 'Margem_06'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."Margem_06"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Width = 49.133858267716530000
          Height = 15.118110240000000000
          DataField = 'CustoTot'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."CustoTot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'ValorTot'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."ValorTot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataField = 'MargemPer'
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMargem."MargemPer"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 279.685220000000000000
        Width = 1046.929810000000000000
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 109.606311420000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Totais')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."PecasTt">,MasterData1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Top = 3.779530000000022000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."AreaM2Tt">,MasterData1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 3.779530000000022000
          Width = 37.795280470000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(IIF('
            'SUM(<frxDsMargem."AreaM2Tt">,MasterData1) <> 0'
            ','
            
              '(SUM(<frxDsMargem."CustoTot">,MasterData1)) / (SUM(<frxDsMargem.' +
              '"AreaM2Tt">,MasterData1))'
            ','
            '0'
            '))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 3.779530000000022000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."CustoTot">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 3.779530000000022000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."ValorTot">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 3.779530000000022000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(IIF('
            'SUM(<frxDsMargem."CustoTot">,MasterData1) <> 0'
            ','
            '('
            '  (SUM(<frxDsMargem."ValorTot">,MasterData1)) '
            '  -'
            '  (SUM(<frxDsMargem."CustoTot">,MasterData1)) '
            ') /'
            ' (SUM(<frxDsMargem."CustoTot">,MasterData1))'
            ''
            '  * 100'
            ','
            '0'
            '))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 3.779530000000022000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."Pecas_01">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Top = 3.779530000000022000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."AreaM2_01">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Top = 3.779530000000022000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(IIF('
            'SUM(<frxDsMargem."Custo_01">,MasterData1) <> 0'
            ','
            '('
            '  (SUM(<frxDsMargem."Valor_01">,MasterData1)) '
            '  -'
            '  (SUM(<frxDsMargem."Custo_01">,MasterData1)) '
            ') /'
            ' (SUM(<frxDsMargem."Custo_01">,MasterData1))'
            ''
            '  * 100'
            ','
            '0'
            '))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 3.779529999999965000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."Pecas_02">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 3.779529999999965000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."AreaM2_02">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000100000
          Top = 3.779529999999965000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(IIF('
            'SUM(<frxDsMargem."Custo_02">,MasterData1) <> 0'
            ','
            '('
            '  (SUM(<frxDsMargem."Valor_02">,MasterData1)) '
            '  -'
            '  (SUM(<frxDsMargem."Custo_02">,MasterData1)) '
            ') /'
            ' (SUM(<frxDsMargem."Custo_02">,MasterData1))'
            ''
            '  * 100'
            ','
            '0'
            '))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779529999999965000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."Pecas_03">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Top = 3.779529999999965000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."AreaM2_03">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Top = 3.779529999999965000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(IIF('
            'SUM(<frxDsMargem."Custo_03">,MasterData1) <> 0'
            ','
            '('
            '  (SUM(<frxDsMargem."Valor_03">,MasterData1)) '
            '  -'
            '  (SUM(<frxDsMargem."Custo_03">,MasterData1)) '
            ') /'
            ' (SUM(<frxDsMargem."Custo_03">,MasterData1))'
            ''
            '  * 100'
            ','
            '0'
            '))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Top = 3.779529999999965000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."Pecas_04">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 744.567410000000000000
          Top = 3.779529999999965000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."AreaM2_04">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 789.921770000000000000
          Top = 3.779529999999965000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(IIF('
            'SUM(<frxDsMargem."Custo_04">,MasterData1) <> 0'
            ','
            '('
            '  (SUM(<frxDsMargem."Valor_04">,MasterData1)) '
            '  -'
            '  (SUM(<frxDsMargem."Custo_04">,MasterData1)) '
            ') /'
            ' (SUM(<frxDsMargem."Custo_04">,MasterData1))'
            ''
            '  * 100'
            ','
            '0'
            '))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 823.937540000000000000
          Top = 3.779530000000022000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."Pecas_05">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 857.953310000000000000
          Top = 3.779530000000022000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."AreaM2_05">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Top = 3.779530000000022000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(IIF('
            'SUM(<frxDsMargem."Custo_05">,MasterData1) <> 0'
            ','
            '('
            '  (SUM(<frxDsMargem."Valor_05">,MasterData1)) '
            '  -'
            '  (SUM(<frxDsMargem."Custo_05">,MasterData1)) '
            ') /'
            ' (SUM(<frxDsMargem."Custo_05">,MasterData1))'
            ''
            '  * 100'
            ','
            '0'
            '))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 937.323440000000000000
          Top = 3.779529999999965000
          Width = 34.015770000000010000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."Pecas_06">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 971.339210000000000000
          Top = 3.779529999999965000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMargem."AreaM2_06">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 1016.693570000000000000
          Top = 3.779529999999965000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          DataSet = frxDsMargem
          DataSetName = 'frxDsMargem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(IIF('
            'SUM(<frxDsMargem."Custo_06">,MasterData1) <> 0'
            ','
            '('
            '  (SUM(<frxDsMargem."Valor_06">,MasterData1)) '
            '  -'
            '  (SUM(<frxDsMargem."Custo_06">,MasterData1)) '
            ') /'
            ' (SUM(<frxDsMargem."Custo_06">,MasterData1))'
            ''
            '  * 100'
            ','
            '0'
            '))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsMargem: TfrxDBDataset
    UserName = 'frxDsMargem'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataHora=DataHora'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'PecasTt=PecasTt'
      'AreaM2Tt=AreaM2Tt'
      'Pecas_01=Pecas_01'
      'AreaM2_01=AreaM2_01'
      'Valor_01=Valor_01'
      'Custo_01=Custo_01'
      'CusM2_01=CusM2_01'
      'Pecas_02=Pecas_02'
      'AreaM2_02=AreaM2_02'
      'Valor_02=Valor_02'
      'Custo_02=Custo_02'
      'CusM2_02=CusM2_02'
      'Pecas_03=Pecas_03'
      'AreaM2_03=AreaM2_03'
      'Valor_03=Valor_03'
      'Custo_03=Custo_03'
      'CusM2_03=CusM2_03'
      'Pecas_04=Pecas_04'
      'AreaM2_04=AreaM2_04'
      'Valor_04=Valor_04'
      'Custo_04=Custo_04'
      'CusM2_04=CusM2_04'
      'Pecas_05=Pecas_05'
      'AreaM2_05=AreaM2_05'
      'Valor_05=Valor_05'
      'Custo_05=Custo_05'
      'CusM2_05=CusM2_05'
      'Pecas_06=Pecas_06'
      'AreaM2_06=AreaM2_06'
      'Valor_06=Valor_06'
      'Custo_06=Custo_06'
      'CusM2_06=CusM2_06'
      'Pecas_07=Pecas_07'
      'AreaM2_07=AreaM2_07'
      'Valor_07=Valor_07'
      'Custo_07=Custo_07'
      'Pecas_08=Pecas_08'
      'AreaM2_08=AreaM2_08'
      'Valor_08=Valor_08'
      'Custo_08=Custo_08'
      'Pecas_09=Pecas_09'
      'AreaM2_09=AreaM2_09'
      'Valor_09=Valor_09'
      'Custo_09=Custo_09'
      'Pecas_10=Pecas_10'
      'AreaM2_10=AreaM2_10'
      'Valor_10=Valor_10'
      'Custo_10=Custo_10'
      'Pecas_11=Pecas_11'
      'AreaM2_11=AreaM2_11'
      'Valor_11=Valor_11'
      'Custo_11=Custo_11'
      'Pecas_12=Pecas_12'
      'AreaM2_12=AreaM2_12'
      'Valor_12=Valor_12'
      'Custo_12=Custo_12'
      'Pecas_13=Pecas_13'
      'AreaM2_13=AreaM2_13'
      'Valor_13=Valor_13'
      'Custo_13=Custo_13'
      'Pecas_14=Pecas_14'
      'AreaM2_14=AreaM2_14'
      'Valor_14=Valor_14'
      'Custo_14=Custo_14'
      'Pecas_15=Pecas_15'
      'AreaM2_15=AreaM2_15'
      'Valor_15=Valor_15'
      'Custo_15=Custo_15'
      'Margem_01=Margem_01'
      'Margem_02=Margem_02'
      'Margem_03=Margem_03'
      'Margem_04=Margem_04'
      'Margem_05=Margem_05'
      'Margem_06=Margem_06'
      'Margem_07=Margem_07'
      'Margem_08=Margem_08'
      'Margem_09=Margem_09'
      'Margem_10=Margem_10'
      'Margem_11=Margem_11'
      'Margem_12=Margem_12'
      'Margem_13=Margem_13'
      'Margem_14=Margem_14'
      'Margem_15=Margem_15'
      'ValorTot=ValorTot'
      'CustoTot=CustoTot'
      'MargemTot=MargemTot'
      'MargemPer=MargemPer'
      'NO_SERIE_FICHA=NO_SERIE_FICHA')
    DataSet = QrMargem
    BCDToCurrency = False
    DataSetOptions = []
    Left = 672
    Top = 440
  end
  object frxWET_CURTI_164_03: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41909.805786724500000000
    ReportOptions.LastChange = 41909.805786724500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_164_01GetValue
    Left = 600
    Top = 360
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVSInnIts
        DataSetName = 'frxDsVSInnIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 94.488250000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Resultado Ribeira - do Prestador da M.O.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Top = 79.370130000000000000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie e ficha RMP')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 79.370130000000000000
          Width = 162.519790000000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forneceor')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 79.370071420000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Entrada')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Top = 79.370130000000000000
          Width = 41.574830000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 79.370130000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTMO]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472479999999990000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Top = 79.370130000000000000
          Width = 117.165430000000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima / artigo')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 79.370130000000000000
          Width = 79.370086060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472479999999990000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-prima / artigo: [VARF_MPRIMAART]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 468.661720000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 415.748300000000000000
        Width = 680.315400000000000000
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 563.149970000000000000
          Height = 18.897640240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DO PER'#205'ODO: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Top = 3.779530000000000000
          Width = 41.574830000000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."Pecas">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779530000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesoKg">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSInnIts
        DataSetName = 'frxDsVSInnIts'
        RowCount = 0
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 120.944879450000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."NO_SerieFch"] [frxDsVSInnIts."Ficha"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Width = 162.519790000000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149601420000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."DataHora"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Width = 41.574830000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."Pecas"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Width = 117.165430000000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Width = 79.370086060000000000
          Height = 15.118110240000000000
          DataField = 'Marca'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Marca"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 173.858380000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSInnIts."NO_CLIENTEMO"'
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897640240000000000
          DataField = 'NO_CLIENTEMO'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."NO_CLIENTEMO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 336.378170000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Width = 563.149970000000000000
          Height = 18.897640240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."NO_CLIENTEMO"] ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Width = 41.574830000000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."Pecas">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesoKg">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsVSInnIts."NO_PRD_TAM_COR"'
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897640240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              '[frxDsVSInnIts."NO_CLIENTEMO"] : [frxDsVSInnIts."NO_PRD_TAM_COR"' +
              ']')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Width = 563.149970000000000000
          Height = 18.897640240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[frxDsVSInnIts."NO_CLIENTEMO"] : [frxDsVSInnIts."NO_PRD_TAM_COR"' +
              ']')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Width = 41.574830000000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."Pecas">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '#,###,###,###'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesoKg">,MasterData1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
