unit VSFchRslIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmVSFchRslIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Qr_Sel_: TmySQLQuery;
    Qr_Sel_Codigo: TIntegerField;
    Qr_Sel_CodUsu: TIntegerField;
    Qr_Sel_Nome: TWideStringField;
    Ds_Sel_: TDataSource;
    VU_Sel_: TdmkValUsu;
    EdSerieFch: TdmkEdit;
    EdFicha: TdmkEdit;
    Label2: TLabel;
    EdGraGruX: TdmkEdit;
    EdNO_PRD_TAM_COR: TdmkEdit;
    GroupBox5: TGroupBox;
    Panel10: TPanel;
    Label9: TLabel;
    Label22: TLabel;
    EdCustoMOKg: TdmkEdit;
    EdCusFretKg: TdmkEdit;
    GroupBox6: TGroupBox;
    Panel13: TPanel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    EdPreco: TdmkEdit;
    EdFreteM2: TdmkEdit;
    EdComissP: TdmkEdit;
    EdImpostP: TdmkEdit;
    EdCustoKg: TdmkEdit;
    Label14: TLabel;
    EdPesoKg: TdmkEdit;
    Label1: TLabel;
    EdAreaM2: TdmkEdit;
    Label4: TLabel;
    EdPecas: TdmkEdit;
    Label6: TLabel;
    EdImpostCred: TdmkEdit;
    Label7: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSFchRslIts: TFmVSFchRslIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnVS_PF;

{$R *.DFM}

procedure TFmVSFchRslIts.BtOKClick(Sender: TObject);
var
  SerieFch, Ficha, GraGruX: Integer;
  CustoKg, CustoMOKg, CusFretKg, Preco, ImpostP, ComissP, FreteM2,
  Pecas, AreaM2, PesoKg, SrcMPAG, ImpostCred: Double;
  //
begin
  SerieFch       := EdSerieFch.ValueVariant;
  Ficha          := EdFicha.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  //                        //
  Pecas          := EdPecas.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  CustoKg        := EdCustoKg.ValueVariant;
  //
  CustoMOKg      := EdCustoMOKg.ValueVariant;
  CusFretKg      := EdCusFretKg.ValueVariant;
  Preco          := EdPreco.ValueVariant;
  ImpostP        := EdImpostP.ValueVariant;
  ComissP        := EdComissP.ValueVariant;
  FreteM2        := EdFreteM2.ValueVariant;
  //SrcMPAG        := EdSrcMPAG.ValueVariant;
  ImpostCred     := EdImpostCred.ValueVariant;
  //
  VS_PF.InsereVSFchRslIts(ImgTipo.SQLType, SerieFch, Ficha, GraGruX, CustoKg,
  CustoMOKg, CusFretKg, Preco, ImpostP, ComissP, FreteM2, Pecas, AreaM2, PesoKg,
  ImpostCred);
  //
  Close;
end;

procedure TFmVSFchRslIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSFchRslIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSFchRslIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmVSFchRslIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
