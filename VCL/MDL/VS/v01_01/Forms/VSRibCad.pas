unit VSRibCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkMemo, frxClass,
  frxDBSet, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO;

type
  TFmVSRibCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtMPrima: TBitBtn;
    BtMP: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVSRibCad: TmySQLQuery;
    DsVSRibCad: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrVSRibCadGraGruX: TIntegerField;
    QrVSRibCadLk: TIntegerField;
    QrVSRibCadDataCad: TDateField;
    QrVSRibCadDataAlt: TDateField;
    QrVSRibCadUserCad: TIntegerField;
    QrVSRibCadUserAlt: TIntegerField;
    QrVSRibCadAlterWeb: TSmallintField;
    QrVSRibCadAtivo: TSmallintField;
    QrVSRibCadGraGru1: TIntegerField;
    QrVSRibCadNO_PRD_TAM_COR: TWideStringField;
    EdFatorNota: TdmkEdit;
    Label5: TLabel;
    frxWET_RECUR_013: TfrxReport;
    frxDsCad: TfrxDBDataset;
    QrCad: TmySQLQuery;
    QrCadGraGruX: TIntegerField;
    QrCadLk: TIntegerField;
    QrCadDataCad: TDateField;
    QrCadDataAlt: TDateField;
    QrCadUserCad: TIntegerField;
    QrCadUserAlt: TIntegerField;
    QrCadAlterWeb: TSmallintField;
    QrCadAtivo: TSmallintField;
    QrCadGraGru1: TIntegerField;
    QrCadNO_PRD_TAM_COR: TWideStringField;
    QrCadOrdem: TIntegerField;
    QrCadBRLMedM2: TFloatField;
    PMMP: TPopupMenu;
    IncluiMP1: TMenuItem;
    AlteraMP1: TMenuItem;
    ExcluiMP1: TMenuItem;
    PMMPrima: TPopupMenu;
    QrVSNatArt: TmySQLQuery;
    DsVSNatArt: TDataSource;
    Incluilinkdeprodutos1: TMenuItem;
    Alteralinkdeprodutos1: TMenuItem;
    Excluilinkdeprodutos1: TMenuItem;
    QrVSNatArtGraGruX: TIntegerField;
    QrVSNatArtNO_PRD_TAM_COR: TWideStringField;
    QrVSNatArtCodigo: TIntegerField;
    QrVSNatArtVSNatCad: TIntegerField;
    QrVSNatArtVSRibCad: TIntegerField;
    QrVSNatArtLk: TIntegerField;
    QrVSNatArtDataCad: TDateField;
    QrVSNatArtDataAlt: TDateField;
    QrVSNatArtUserCad: TIntegerField;
    QrVSNatArtUserAlt: TIntegerField;
    QrVSNatArtAlterWeb: TSmallintField;
    QrVSNatArtAtivo: TSmallintField;
    BtClassificado: TBitBtn;
    PMClassificado: TPopupMenu;
    Incluilinkdeartigoclassificado1: TMenuItem;
    Alteralinkdeartigoclassificado1: TMenuItem;
    Excluilinkdeartigoclassificado1: TMenuItem;
    GroupBox1: TGroupBox;
    GBVSRibArt: TGroupBox;
    Splitter1: TSplitter;
    DBGrid1: TdmkDBGridZTO;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrVSRibArt: TmySQLQuery;
    QrVSRibArtGraGruX: TIntegerField;
    QrVSRibArtNO_PRD_TAM_COR: TWideStringField;
    QrVSRibArtCodigo: TIntegerField;
    QrVSRibArtVSRibCad: TIntegerField;
    QrVSRibArtVSRibCla: TIntegerField;
    QrVSRibArtLk: TIntegerField;
    QrVSRibArtDataCad: TDateField;
    QrVSRibArtDataAlt: TDateField;
    QrVSRibArtUserCad: TIntegerField;
    QrVSRibArtUserAlt: TIntegerField;
    QrVSRibArtAlterWeb: TSmallintField;
    QrVSRibArtAtivo: TSmallintField;
    DsVSRibArt: TDataSource;
    QrVSRibCadFatorNota: TFloatField;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    QrGraGruXCou: TmySQLQuery;
    QrGraGruXCouNO_CouNiv1: TWideStringField;
    QrGraGruXCouNO_CouNiv2: TWideStringField;
    QrGraGruXCouGraGruX: TIntegerField;
    QrGraGruXCouCouNiv1: TIntegerField;
    QrGraGruXCouCouNiv2: TIntegerField;
    QrGraGruXCouLk: TIntegerField;
    QrGraGruXCouDataCad: TDateField;
    QrGraGruXCouDataAlt: TDateField;
    QrGraGruXCouUserCad: TIntegerField;
    QrGraGruXCouUserAlt: TIntegerField;
    QrGraGruXCouAlterWeb: TSmallintField;
    QrGraGruXCouAtivo: TSmallintField;
    DsGraGruXCou: TDataSource;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    GBCouNiv: TGroupBox;
    Label6: TLabel;
    Label4: TLabel;
    EdCouNiv2: TdmkEditCB;
    CBCouNiv2: TdmkDBLookupComboBox;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    EdArtigoImp: TdmkEdit;
    EdClasseImp: TdmkEdit;
    QrVSRibCadArtigoImp: TWideStringField;
    QrVSRibCadClasseImp: TWideStringField;
    QrGraGruXCouArtigoImp: TWideStringField;
    QrGraGruXCouClasseImp: TWideStringField;
    Label13: TLabel;
    EdPrevPcPal: TdmkEdit;
    QrGraGruXCouPrevPcPal: TIntegerField;
    DBEdit8: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    QrVSRibCadMediaMinM2: TFloatField;
    QrVSRibCadMediaMaxM2: TFloatField;
    Label17: TLabel;
    Label18: TLabel;
    EdMediaMinM2: TdmkEdit;
    EdMediaMaxM2: TdmkEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Memo1: TMemo;
    Label9: TLabel;
    Label19: TLabel;
    EdNome: TdmkEdit;
    EdTam: TdmkEdit;
    EdCor: TdmkEdit;
    Label20: TLabel;
    Label21: TLabel;
    EdPrevAMPal: TdmkEdit;
    EdPrevKgPal: TdmkEdit;
    Label22: TLabel;
    QrGraGruXCouPrevAMPal: TFloatField;
    QrGraGruXCouPrevKgPal: TFloatField;
    Label23: TLabel;
    DBEdit2: TDBEdit;
    DBEdit11: TDBEdit;
    Label24: TLabel;
    QrGraGruXCouGrandeza: TSmallintField;
    RGBastidao: TdmkRadioGroup;
    QrArtGeComodty: TMySQLQuery;
    QrArtGeComodtyCodigo: TIntegerField;
    QrArtGeComodtyNome: TWideStringField;
    DsArtGeComodty: TDataSource;
    LaArtGerComodty: TLabel;
    EdArtGeComodty: TdmkEditCB;
    CBArtGeComodty: TdmkDBLookupComboBox;
    SbArtGeComodty: TSpeedButton;
    QrGraGruXCouArtGeComodty: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSRibCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSRibCadBeforeOpen(DataSet: TDataSet);
    procedure BtMPClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMMPPopup(Sender: TObject);
    procedure IncluiMP1Click(Sender: TObject);
    procedure AlteraMP1Click(Sender: TObject);
    procedure ExcluiMP1Click(Sender: TObject);
    procedure BtMPrimaClick(Sender: TObject);
    procedure PMMPrimaPopup(Sender: TObject);
    procedure QrVSRibCadBeforeClose(DataSet: TDataSet);
    procedure QrVSRibCadAfterScroll(DataSet: TDataSet);
    procedure Incluilinkdeprodutos1Click(Sender: TObject);
    procedure Alteralinkdeprodutos1Click(Sender: TObject);
    procedure Excluilinkdeprodutos1Click(Sender: TObject);
    procedure Incluilinkdeartigoclassificado1Click(Sender: TObject);
    procedure Alteralinkdeartigoclassificado1Click(Sender: TObject);
    procedure Excluilinkdeartigoclassificado1Click(Sender: TObject);
    procedure PMClassificadoPopup(Sender: TObject);
    procedure BtClassificadoClick(Sender: TObject);
    procedure SbArtGeComodtyClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure MostraVSNatArt(SQLType: TSQLType);
    procedure ReopenVSNatArt(Codigo: Integer);
    procedure MostraVSRibArt(SQLType: TSQLType);
    procedure ReopenVSRibArt(Codigo: Integer);

  public
    { Public declarations }
    FSeq: Integer;
    procedure LocCod(Atual, GraGruX: Integer);
  end;

var
  FmVSRibCad: TFmVSRibCad;
const
  FFormatFloat = '00000';
  FCo_Codigo = 'GraGruX';
  FCo_Nome = 'CONCAT(gg1.Nome, ' + sLineBreak +
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ' + sLineBreak +
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))';

implementation

uses UnMyObjects, Module, MeuDBUses, DMkDAC_PF, ModuleGeral, MyDBCheck,
  VSNatArt, VSRibArt, UnVS_PF, AppListas, UnGrade_PF, UnGrade_Jan, UnVS_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSRibCad.LocCod(Atual, GraGruX: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, GraGruX);
end;

procedure TFmVSRibCad.MostraVSNatArt(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSNatArt, FmVSNatArt, afmoNegarComAviso) then
  begin
    FmVSNatArt.ImgTipo.SQLType := SQLType;
    FmVSNatArt.FQrCab := QrVSRibCad;
    FmVSNatArt.FDsCab := DsVSRibCad;
    FmVSNatArt.FQrIts := QrVSNatArt;
    //
    FmVSNatArt.LaVSNatCad.Enabled := True;
    FmVSNatArt.EdVSNatCad.Enabled := True;
    FmVSNatArt.CBVSNatCad.Enabled := True;
    //
    FmVSNatArt.LaVSRibCad.Enabled := False;
    FmVSNatArt.EdVSRibCad.Enabled := False;
    FmVSNatArt.CBVSRibCad.Enabled := False;
    //
    FmVSNatArt.EdVSRibCad.ValueVariant := QrVSRibCadGraGruX.Value;
    FmVSNatArt.CBVSRibCad.KeyValue     := QrVSRibCadGraGruX.Value;
    //
    if SQLType = stIns then
    begin
      FmVSNatArt.FCodigo := 0;
    end else
    begin
      FmVSNatArt.FCodigo := QrVSNatArtCodigo.Value;
      //
      FmVSNatArt.EdVSNatCad.ValueVariant := QrVSNatArtVSNatCad.Value;
      FmVSNatArt.CBVSNatCad.KeyValue     := QrVSNatArtVSNatCad.Value;
    end;
    FmVSNatArt.ShowModal;
    FmVSNatArt.Destroy;
  end;
end;

procedure TFmVSRibCad.MostraVSRibArt(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSRibArt, FmVSRibArt, afmoNegarComAviso) then
  begin
    FmVSRibArt.ImgTipo.SQLType := SQLType;
    FmVSRibArt.FQrCab := QrVSRibCad;
    FmVSRibArt.FDsCab := DsVSRibCad;
    FmVSRibArt.FQrIts := QrVSRibArt;
    //
    FmVSRibArt.LaVSRibCla.Enabled := True;
    FmVSRibArt.EdVSRibCla.Enabled := True;
    FmVSRibArt.CBVSRibCla.Enabled := True;
    //
    FmVSRibArt.LaVSRibCad.Enabled := False;
    FmVSRibArt.EdVSRibCad.Enabled := False;
    FmVSRibArt.CBVSRibCad.Enabled := False;
    //

    FmVSRibArt.EdVSRibCad.ValueVariant := QrVSRibCadGraGruX.Value;
    FmVSRibArt.CBVSRibCad.KeyValue     := QrVSRibCadGraGruX.Value;
    //
    if SQLType = stIns then
    begin
      FmVSRibArt.FCodigo := 0;
    end else
    begin
      FmVSRibArt.FCodigo := QrVSRibArtCodigo.Value;
      //
      FmVSRibArt.EdVSRibCla.ValueVariant := QrVSRibArtVSRibCla.Value;
      FmVSRibArt.CBVSRibCla.KeyValue     := QrVSRibArtVSRibCla.Value;
      //
    end;
    FmVSRibArt.ShowModal;
    FmVSRibArt.Destroy;
  end;
end;

procedure TFmVSRibCad.PMMPrimaPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluilinkdeprodutos1, QrVSRibCad);
  MyObjects.HabilitaMenuItemItsUpd(Alteralinkdeprodutos1, QrVSNatArt);
  MyObjects.HabilitaMenuItemItsDel(Alteralinkdeprodutos1, QrVSNatArt);
end;

procedure TFmVSRibCad.PMClassificadoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluilinkdeartigoclassificado1, QrVSRibCad);
  MyObjects.HabilitaMenuItemItsUpd(Alteralinkdeartigoclassificado1, QrVSRibArt);
  MyObjects.HabilitaMenuItemItsDel(Excluilinkdeartigoclassificado1, QrVSRibArt);
end;

procedure TFmVSRibCad.PMMPPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(AlteraMP1, QrVSRibCad);
  //MyObjects.HabilitaMenuItemCabDel(ExcluiMP1, QrVSRibCad);
end;

procedure TFmVSRibCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSRibCadGraGruX.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSRibCad.DefParams;
begin
  VAR_GOTOTABELA := 'vsribcad';
  VAR_GOTOMYSQLTABLE := QrVSRibCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := FCo_Codigo;
  VAR_GOTONOME := FCo_Nome;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wmp.*, cou.*, ');
  VAR_SQLx.Add('ggx.GraGru1, CONCAT(gg1.Nome,');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  VAR_SQLx.Add('NO_PRD_TAM_COR');
  VAR_SQLx.Add('FROM vsribcad wmp');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('WHERE wmp.GraGruX > 0');
  //
  VAR_SQL1.Add('AND wmp.GraGruX=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE ' + FCo_Nome + ' LIKE :P0 ');
  //
end;

procedure TFmVSRibCad.Excluilinkdeartigoclassificado1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI(
  'Confirma a retirada do link do artigo classificado selecionado?',
  'VSRibArt', 'Codigo', QrVSRibArtCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSRibArt,
      QrVSRibArtCodigo, QrVSRibArtCodigo.Value);
    ReopenVSRibArt(Codigo);
  end;
end;

procedure TFmVSRibCad.Excluilinkdeprodutos1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI(
  'Confirma a retirada do link de mat�ria-prima selecionado?',
  'VSNatArt', 'Codigo', QrVSNatArtCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSNatArt,
      QrVSNatArtCodigo, QrVSNatArtCodigo.Value);
    ReopenVSNatArt(Codigo);
  end;
end;

procedure TFmVSRibCad.ExcluiMP1Click(Sender: TObject);
begin
//
end;

procedure TFmVSRibCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSRibCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSRibCad.ReopenVSNatArt(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSNatArt, Dmod.MyDB, [
  'SELECT wmp.GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vna.* ',
  'FROM vsnatart vna ',
  'LEFT JOIN vsnatcad wmp ON wmp.GraGruX=vna.VSNatCad ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vna.VSRibCad=' + Geral.FF0(QrVSRibCadGraGruX.Value),
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmVSRibCad.ReopenVSRibArt(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSRibArt, Dmod.MyDB, [
  'SELECT wmp.GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vna.* ',
  'FROM vsribart vna ',
  'LEFT JOIN vsribcla wmp ON wmp.GraGruX=vna.VSRibCla ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vna.VSRibCad=' + Geral.FF0(QrVSRibCadGraGruX.Value),
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmVSRibCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSRibCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSRibCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSRibCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSRibCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSRibCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRibCad.Alteralinkdeartigoclassificado1Click(Sender: TObject);
begin
  MostraVSRibArt(stUpd);
end;

procedure TFmVSRibCad.Alteralinkdeprodutos1Click(Sender: TObject);
begin
 MostraVSNatArt(stUpd);
end;

procedure TFmVSRibCad.AlteraMP1Click(Sender: TObject);
begin
  EdFatorNota.ValueVariant  := QrVSRibCadFatorNota.Value;
  EdMediaMinM2.ValueVariant := QrVSRibCadMediaMinM2.Value;
  EdMediaMaxM2.ValueVariant := QrVSRibCadMediaMaxM2.Value;
  Grade_PF.DefineNomeTamCorGraGruX(EdNome, EdTam, EdCor, QrVSRibCadGraGruX.Value);
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGruXCou, [PnDados],
  [PnEdita], EdFatorNota, ImgTipo, 'gragruxcou');
end;

procedure TFmVSRibCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSRibCadGraGruX.Value;
  Close;
end;

procedure TFmVSRibCad.BtClassificadoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMClassificado, BtClassificado);
end;

procedure TFmVSRibCad.BtConfirmaClick(Sender: TObject);

var
  GraGruX, Ordem, CouNiv1, CouNiv2, PrevPcPal, GraGru1, Bastidao: Integer;
  FatorNota: Double;
  Nome, ArtigoImp, ClasseImp: String;
  MediaMinM2, MediaMaxM2, PrevAMPal, PrevKgPal: Double;
  //
  MediaMinKg, MediaMaxKg: Double;
  GGXPronto: Integer;
  FatrClase, BaseValCusto, BaseValVenda: Double;
  BaseCliente: String;
  BaseImpostos, BasePerComis, BasFrteVendM2, BaseValLiq: Double;
  ArtGeComodty: Integer;
begin
  MediaMinKg :=  0;
  MediaMaxKg :=  0;
  GGXPronto :=  0;
  FatrClase :=  999.000000;
  BaseValCusto :=  0;
  BaseValVenda :=  0;
  BaseCliente :=  '';
  BaseImpostos :=  0;
  BasePerComis :=  0;
  BasFrteVendM2 :=  0;
  BaseValLiq :=  0;
  //
  GraGruX        := QrVSRibCadGraGruX.Value;
  GraGru1        := QrVSRibCadGraGru1.Value;
  //Ordem          := EdOrdem.ValueVariant;
  FatorNota      := EdFatorNota.ValueVariant;
  CouNiv1        := EdCouNiv1.ValueVariant;
  CouNiv2        := EdCouNiv2.ValueVariant;
  ArtigoImp      := EdArtigoImp.Text;
  ClasseImp      := EdClasseImp.Text;
  PrevPcPal      := EdPrevPcPal.ValueVariant;
  PrevAMPal      := EdPrevAMPal.ValueVariant;
  PrevKgPal      := EdPrevKgPal.ValueVariant;
  MediaMinM2     := EdMediaMinM2.ValueVariant;
  MediaMaxM2     := EdMediaMaxM2.ValueVariant;
  Nome           := EdNome.Text;
  Bastidao       := RGBastidao.ItemIndex;
  ArtGeComodty   := EdArtGeComodty.ValueVariant;
  //
  if MyObjects.FIC(FatorNota <= 0, EdFatorNota, 'Informe o "Fator Nota MPAG"!') then
    Exit;
  if MyObjects.FIC(CouNiv2 = 0, EdCouNiv2, 'Informe o "Tipo do material"!') then
    Exit;
  if MyObjects.FIC(CouNiv1 = 0, EdCouNiv1, 'Informe a "Parte do material"!') then
    Exit;
  if MyObjects.FIC(MediaMinM2 = 0, EdMediaMinM2, 'Informe a "M�dia m�n. m�"!') then
    Exit;
  if MyObjects.FIC(MediaMaxM2 = 0, EdMediaMaxM2, 'Informe a "M�dia m�x. m�"!') then
    Exit;
  if MyObjects.FIC(MediaMaxM2 <= MediaMinM2, EdMediaMaxM2,
  'A m�dia m�xima deve se superior a m�dia m�nima!') then
    Exit;
  if MyObjects.FIC(ArtGeComodty = 0, EdArtGeComodty, 'Informe "' +
    LaArtGerComodty.Caption + '"!') then
    Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsribcad', False, [
  'FatorNota'(*, 'ArtigoImp', 'ClasseImp'*)], [
  'GraGruX'], [
  FatorNota(*, ArtigoImp, ClasseImp*)], [
  GraGruX], True) then
  begin
    VS_PF.ReIncluiCouNivs(
      GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1,
      //(*Bastidao*) Integer(TVSBastidao.vsbstdIntegral), PrevAMPal,
      Bastidao, PrevAMPal, PrevKgPal,
      ArtigoImp, ClasseImp, MediaMinM2, MediaMaxM2, MediaMinKg, MediaMaxKg,
      GGXPronto, FatrClase, BaseValCusto, BaseValVenda, BaseCliente,
      BaseImpostos, BasePerComis, BasFrteVendM2, BaseValLiq, ArtGeComodty);
    //
    Grade_PF.GraGru1_AlteraNome(GraGru1, Nome);
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    if FSeq = 0 then
    begin
      LocCod(GraGruX, GraGruX);
    end else
      Close;
  end;
end;

procedure TFmVSRibCad.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVSRibCad.BtMPrimaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMPrima, BtMPrima);
end;

procedure TFmVSRibCad.BtMPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMP, BtMP);
end;

procedure TFmVSRibCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSeq := 0;
  //GBEdita.Align := alClient;
  DBGrid1.Align := alClient;
  GBVSRibArt.Align := alClient;
  VS_PF.ConfiguraRGVSBastidao(RGBastidao, True(*Habilita*), (*Default*)-1);
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrArtGeComodty, Dmod.MyDB);
  CriaOForm;
end;

procedure TFmVSRibCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSRibCadGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmVSRibCad.SbArtGeComodtyClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_Jan.MostraFormArtGeComodty(EdArtGeComodty.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdArtGeComodty, CBArtGeComodty, QrArtGeComodty, VAR_CADASTRO);
end;

procedure TFmVSRibCad.SbImprimeClick(Sender: TObject);
var
  Ordem: Integer;
  Ord_Txt: String;
begin
  Ordem := MyObjects.SelRadioGroup('Ordem de Impress�o',
  'Selecione a ordem de Impress�o', [
  'Ordem',
  'Nome',
  'Pre�o'], 2);
  case Ordem of
    0: Ord_Txt := 'Ordem';
    1: Ord_Txt := 'NO_PRD_TAM_COR';
    2: Ord_Txt := 'BRLMedM2';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCad, Dmod.MyDB, [
  'SELECT wmp.*, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vsribcad wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY ' + Ord_Txt,
  '']);
  MyObjects.frxDefineDataSets(frxWET_RECUR_013, [
  DModG.frxDsDono,
  frxDsCad]);
  MyObjects.frxMostra(frxWET_RECUR_013,
    'Configura��o de Mat�ria-prima para Semi / Acabado');
end;

procedure TFmVSRibCad.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSRibCad.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSRibCadGraGruX.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSRibCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSRibCad.QrVSRibCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSRibCad.QrVSRibCadAfterScroll(DataSet: TDataSet);
begin
  ReopenVSNatArt(0);
  ReopenVSRibArt(0);
  VS_PF.ReopenGraGruXCou(QrGraGruXCou, QrVSRibCadGraGruX.Value);
end;

procedure TFmVSRibCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSRibCad.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSRibCadGraGruX.Value,
  CuringaLoc.CriaForm(FCo_Codigo, FCo_Nome, 'vsribcad', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSRibCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRibCad.Incluilinkdeartigoclassificado1Click(Sender: TObject);
begin
  MostraVSRibArt(stIns);
end;

procedure TFmVSRibCad.Incluilinkdeprodutos1Click(Sender: TObject);
begin
  MostraVSNatArt(stIns);
end;

procedure TFmVSRibCad.IncluiMP1Click(Sender: TObject);
var
  DefinedSQL: String;
  Qry: TmySQLQuery;
  GraGruX: Integer;
begin
(*
  DefinedSQL := Geral.ATS([
  'SELECT ggx.Controle _Codigo, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) _Nome ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) LIKE %s ',
  'ORDER BY _Nome ',
  '']);
  FmMeuDBUses.PesquisaNome('', '', '', DefinedSQL);
  GraGruX := VAR_CADASTRO;
*)
  GraGruX := Grade_Jan.ObtemGraGruXSemY(CO_GraGruY_2048_VSRibCad);
  if GraGruX <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      //ShowMessage(Geral.FF0(VAR_CADASTRO));
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM vsribcad ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if Qry.FieldByName('GraGruX').AsInteger = GraGruX then
      begin
        Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
        ' j� estava adicionado � configura��o de mat�ria-prima!');
      end else
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsribcad', False, [
        ], [
        'GraGruX'], [
        ], [
        GraGruX], True);
      end;
      //
      LocCod(GraGruX, GraGruX);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmVSRibCad.QrVSRibCadBeforeClose(DataSet: TDataSet);
begin
  QrVSNatArt.Close;
  QrGraGruXCou.Close;
end;

procedure TFmVSRibCad.QrVSRibCadBeforeOpen(DataSet: TDataSet);
begin
  QrVSRibCadGraGruX.DisplayFormat := FFormatFloat;
end;

end.

