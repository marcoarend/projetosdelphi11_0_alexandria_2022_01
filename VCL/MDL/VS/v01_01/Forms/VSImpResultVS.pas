unit VSImpResultVS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, frxClass, frxDBSet,
  UnProjGroup_Consts, AppListas, UnGrl_Consts, UnAppEnums;

type
  TFmVSImpResultVS = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel26: TPanel;
    RG00_Ordem1: TRadioGroup;
    RG00_Ordem2: TRadioGroup;
    RG00_Ordem3: TRadioGroup;
    Panel49: TPanel;
    RG00_Agrupa: TRadioGroup;
    Panel50: TPanel;
    Panel58: TPanel;
    RG00_Ordem4: TRadioGroup;
    RG00_Ordem5: TRadioGroup;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    frxWET_CURTI_164_01: TfrxReport;
    QrVSInnIts: TmySQLQuery;
    QrVSInnItsCodigo: TLargeintField;
    QrVSInnItsControle: TLargeintField;
    QrVSInnItsMovimCod: TLargeintField;
    QrVSInnItsMovimNiv: TLargeintField;
    QrVSInnItsMovimTwn: TLargeintField;
    QrVSInnItsEmpresa: TLargeintField;
    QrVSInnItsTerceiro: TLargeintField;
    QrVSInnItsCliVenda: TLargeintField;
    QrVSInnItsMovimID: TLargeintField;
    QrVSInnItsDataHora: TDateTimeField;
    QrVSInnItsPallet: TLargeintField;
    QrVSInnItsGraGruX: TLargeintField;
    QrVSInnItsPecas: TFloatField;
    QrVSInnItsPesoKg: TFloatField;
    QrVSInnItsAreaM2: TFloatField;
    QrVSInnItsAreaP2: TFloatField;
    QrVSInnItsValorT: TFloatField;
    QrVSInnItsSrcMovID: TLargeintField;
    QrVSInnItsSrcNivel1: TLargeintField;
    QrVSInnItsSrcNivel2: TLargeintField;
    QrVSInnItsSrcGGX: TLargeintField;
    QrVSInnItsSdoVrtPeca: TFloatField;
    QrVSInnItsSdoVrtPeso: TFloatField;
    QrVSInnItsSdoVrtArM2: TFloatField;
    QrVSInnItsObserv: TWideStringField;
    QrVSInnItsSerieFch: TLargeintField;
    QrVSInnItsFicha: TLargeintField;
    QrVSInnItsMisturou: TLargeintField;
    QrVSInnItsFornecMO: TLargeintField;
    QrVSInnItsCustoMOKg: TFloatField;
    QrVSInnItsCustoMOM2: TFloatField;
    QrVSInnItsCustoMOTot: TFloatField;
    QrVSInnItsValorMP: TFloatField;
    QrVSInnItsDstMovID: TLargeintField;
    QrVSInnItsDstNivel1: TLargeintField;
    QrVSInnItsDstNivel2: TLargeintField;
    QrVSInnItsDstGGX: TLargeintField;
    QrVSInnItsQtdGerPeca: TFloatField;
    QrVSInnItsQtdGerPeso: TFloatField;
    QrVSInnItsQtdGerArM2: TFloatField;
    QrVSInnItsQtdGerArP2: TFloatField;
    QrVSInnItsQtdAntPeca: TFloatField;
    QrVSInnItsQtdAntPeso: TFloatField;
    QrVSInnItsQtdAntArM2: TFloatField;
    QrVSInnItsQtdAntArP2: TFloatField;
    QrVSInnItsNotaMPAG: TFloatField;
    QrVSInnItsPedItsFin: TLargeintField;
    QrVSInnItsMarca: TWideStringField;
    QrVSInnItsStqCenLoc: TLargeintField;
    QrVSInnItsNO_PALLET: TWideStringField;
    QrVSInnItsNO_PRD_TAM_COR: TWideStringField;
    QrVSInnItsNO_TTW: TWideStringField;
    QrVSInnItsID_TTW: TLargeintField;
    QrVSInnItsReqMovEstq: TLargeintField;
    QrVSInnItsRendKgm2: TFloatField;
    QrVSInnItsNotaMPAG_TXT: TWideStringField;
    QrVSInnItsRendKgm2_TXT: TWideStringField;
    QrVSInnItsMisturou_TXT: TWideStringField;
    QrVSInnItsNOMEUNIDMED: TWideStringField;
    QrVSInnItsSIGLAUNIDMED: TWideStringField;
    QrVSInnItsm2_CouroTXT: TWideStringField;
    QrVSInnItsKgMedioCouro: TFloatField;
    QrVSInnItsVSMulFrnCab: TLargeintField;
    QrVSInnItsClientMO: TLargeintField;
    QrVSInnItsNO_SerieFch: TWideStringField;
    DsVSInnIts: TDataSource;
    frxDsVSInnIts: TfrxDBDataset;
    QrVSInnItsCustoPQ: TFloatField;
    QrVSInnItsPedItsLib: TLargeintField;
    QrVSInnItsPedItsVda: TLargeintField;
    QrVSInnItsItemNFe: TLargeintField;
    QrVSInnItsNFeSer: TLargeintField;
    QrVSInnItsNFeNum: TLargeintField;
    QrVSInnItsVSMulNFeCab: TLargeintField;
    QrVSInnItsJmpMovID: TLargeintField;
    QrVSInnItsJmpNivel1: TLargeintField;
    QrVSInnItsJmpNivel2: TLargeintField;
    QrVSInnItsClientMO_1: TIntegerField;
    QrVSInnItsCodigo_1: TIntegerField;
    QrVSInnItsMovimCod_1: TIntegerField;
    QrVSInnItsEmpresa_1: TIntegerField;
    QrVSInnItsDtCompra: TDateTimeField;
    QrVSInnItsDtViagem: TDateTimeField;
    QrVSInnItsDtEntrada: TDateTimeField;
    QrVSInnItsFornecedor: TIntegerField;
    QrVSInnItsTransporta: TIntegerField;
    QrVSInnItsPecas_1: TFloatField;
    QrVSInnItsPesoKg_1: TFloatField;
    QrVSInnItsAreaM2_1: TFloatField;
    QrVSInnItsAreaP2_1: TFloatField;
    QrVSInnItsValorT_1: TFloatField;
    QrVSInnItsClienteMO: TIntegerField;
    QrVSInnItsProcednc: TIntegerField;
    QrVSInnItsMotorista: TIntegerField;
    QrVSInnItsPlaca: TWideStringField;
    QrVSInnItsLk: TIntegerField;
    QrVSInnItsDataCad: TDateField;
    QrVSInnItsDataAlt: TDateField;
    QrVSInnItsUserCad: TIntegerField;
    QrVSInnItsUserAlt: TIntegerField;
    QrVSInnItsAlterWeb: TSmallintField;
    QrVSInnItsAtivo: TSmallintField;
    QrVSInnItsTemIMEIMrt: TSmallintField;
    QrVSInnItside_serie: TIntegerField;
    QrVSInnItside_nNF: TIntegerField;
    QrVSInnItsemi_serie: TIntegerField;
    QrVSInnItsemi_nNF: TIntegerField;
    QrVSInnItsNFeStatus: TIntegerField;
    QrVSInnItsNO_EMPRESA: TWideStringField;
    QrVSInnItsNO_FORNECE: TWideStringField;
    QrVSInnItsNO_TRANSPORTA: TWideStringField;
    QrVSInnItsNO_CLIENTEMO: TWideStringField;
    QrVSInnItsNO_PROCEDNC: TWideStringField;
    QrVSInnItsNO_MOTORISTA: TWideStringField;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCustoMOTot: TFloatField;
    frxDsVSMovIts: TfrxDBDataset;
    QrVSMovItsCustoPQ: TFloatField;
    QrVSMovItsMargem: TFloatField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    Panel5: TPanel;
    RGRelatorio: TRadioGroup;
    GroupBox3: TGroupBox;
    TPDataIni: TdmkEditDateTimePicker;
    CkDataIni: TCheckBox;
    CkDataFim: TCheckBox;
    TPDataFim: TdmkEditDateTimePicker;
    Panel6: TPanel;
    LaClienteMO: TLabel;
    EdClienteMO: TdmkEditCB;
    CBClienteMO: TdmkDBLookupComboBox;
    LaFornecedor: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    LaProcednc: TLabel;
    EdProcednc: TdmkEditCB;
    CBProcednc: TdmkDBLookupComboBox;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    QrFornecedores: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsFornecedores: TDataSource;
    QrProcedncs: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsProcedncs: TDataSource;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    DsGraCusPrc: TDataSource;
    LaGraCusPrc: TLabel;
    EdGraCusPrc: TdmkEditCB;
    CBGraCusPrc: TdmkDBLookupComboBox;
    QrClasses: TmySQLQuery;
    QrClassesReferencia: TWideStringField;
    QrClassesGraGruX: TIntegerField;
    QrClassesAreaM2: TFloatField;
    QrClassesCustoPreco: TFloatField;
    QrMargem: TmySQLQuery;
    frxWET_CURTI_164_02: TfrxReport;
    frxDsMargem: TfrxDBDataset;
    QrMargemDataHora: TDateTimeField;
    QrMargemSerieFch: TIntegerField;
    QrMargemFicha: TIntegerField;
    QrMargemPecasTt: TFloatField;
    QrMargemAreaM2Tt: TFloatField;
    QrMargemPecas_01: TFloatField;
    QrMargemAreaM2_01: TFloatField;
    QrMargemValor_01: TFloatField;
    QrMargemCusto_01: TFloatField;
    QrMargemCusM2_01: TFloatField;
    QrMargemPecas_02: TFloatField;
    QrMargemAreaM2_02: TFloatField;
    QrMargemValor_02: TFloatField;
    QrMargemCusto_02: TFloatField;
    QrMargemCusM2_02: TFloatField;
    QrMargemPecas_03: TFloatField;
    QrMargemAreaM2_03: TFloatField;
    QrMargemValor_03: TFloatField;
    QrMargemCusto_03: TFloatField;
    QrMargemCusM2_03: TFloatField;
    QrMargemPecas_04: TFloatField;
    QrMargemAreaM2_04: TFloatField;
    QrMargemValor_04: TFloatField;
    QrMargemCusto_04: TFloatField;
    QrMargemCusM2_04: TFloatField;
    QrMargemPecas_05: TFloatField;
    QrMargemAreaM2_05: TFloatField;
    QrMargemValor_05: TFloatField;
    QrMargemCusto_05: TFloatField;
    QrMargemCusM2_05: TFloatField;
    QrMargemPecas_06: TFloatField;
    QrMargemAreaM2_06: TFloatField;
    QrMargemValor_06: TFloatField;
    QrMargemCusto_06: TFloatField;
    QrMargemCusM2_06: TFloatField;
    QrMargemPecas_07: TFloatField;
    QrMargemAreaM2_07: TFloatField;
    QrMargemValor_07: TFloatField;
    QrMargemCusto_07: TFloatField;
    QrMargemPecas_08: TFloatField;
    QrMargemAreaM2_08: TFloatField;
    QrMargemValor_08: TFloatField;
    QrMargemCusto_08: TFloatField;
    QrMargemPecas_09: TFloatField;
    QrMargemAreaM2_09: TFloatField;
    QrMargemValor_09: TFloatField;
    QrMargemCusto_09: TFloatField;
    QrMargemPecas_10: TFloatField;
    QrMargemAreaM2_10: TFloatField;
    QrMargemValor_10: TFloatField;
    QrMargemCusto_10: TFloatField;
    QrMargemPecas_11: TFloatField;
    QrMargemAreaM2_11: TFloatField;
    QrMargemValor_11: TFloatField;
    QrMargemCusto_11: TFloatField;
    QrMargemPecas_12: TFloatField;
    QrMargemAreaM2_12: TFloatField;
    QrMargemValor_12: TFloatField;
    QrMargemCusto_12: TFloatField;
    QrMargemPecas_13: TFloatField;
    QrMargemAreaM2_13: TFloatField;
    QrMargemValor_13: TFloatField;
    QrMargemCusto_13: TFloatField;
    QrMargemPecas_14: TFloatField;
    QrMargemAreaM2_14: TFloatField;
    QrMargemValor_14: TFloatField;
    QrMargemCusto_14: TFloatField;
    QrMargemPecas_15: TFloatField;
    QrMargemAreaM2_15: TFloatField;
    QrMargemValor_15: TFloatField;
    QrMargemCusto_15: TFloatField;
    QrMargemMargem_01: TFloatField;
    QrMargemMargem_02: TFloatField;
    QrMargemMargem_03: TFloatField;
    QrMargemMargem_04: TFloatField;
    QrMargemMargem_05: TFloatField;
    QrMargemMargem_06: TFloatField;
    QrMargemMargem_07: TFloatField;
    QrMargemMargem_08: TFloatField;
    QrMargemMargem_09: TFloatField;
    QrMargemMargem_10: TFloatField;
    QrMargemMargem_11: TFloatField;
    QrMargemMargem_12: TFloatField;
    QrMargemMargem_13: TFloatField;
    QrMargemMargem_14: TFloatField;
    QrMargemMargem_15: TFloatField;
    QrMargemValorTot: TFloatField;
    QrMargemCustoTot: TFloatField;
    QrMargemMargemTot: TFloatField;
    QrMargemMargemPer: TFloatField;
    QrMargemNO_SERIE_FICHA: TWideStringField;
    frxWET_CURTI_164_03: TfrxReport;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrVSInnCabAfterScroll(DataSet: TDataSet);
    procedure QrVSInnCabBeforeClose(DataSet: TDataSet);
    procedure frxWET_CURTI_164_01GetValue(const VarName: string;
      var Value: Variant);
    procedure QrVSInnItsAfterScroll(DataSet: TDataSet);
    procedure RGRelatorioClick(Sender: TObject);
  private
    { Private declarations }
    //
    procedure CustoDeProducao();
    procedure EntradaDeCouro();
    procedure ReopenVSInnIts();
    procedure ResultadoRibeira();
  public
    { Public declarations }
  end;

  var
  FmVSImpResultVS: TFmVSImpResultVS;

implementation

uses UnMyObjects, DmkDAC_PF, Module, ModuleGeral, UnDmkProcFunc, UnVS_PF;

{$R *.DFM}

const
  FCols = 15;
var
  FColunas, FColSQL: array[0..FCols-1] of String;
  FPrecos: array[0..FCols-1] of Double;

procedure TFmVSImpResultVS.BtOKClick(Sender: TObject);
const
  sProcName = 'FmVSImpResultVS.BtOKClick()';
begin
  case RGRelatorio.ItemIndex of
    //0: ---
    1: ResultadoRibeira();
    2: CustoDeProducao();
    3: EntradaDeCouro();
    else Geral.MB_Erro('Relat�rio n�o definido ou n�o implementado! ' +
      sProcName);
  end;
end;

procedure TFmVSImpResultVS.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpResultVS.CustoDeProducao();
{

//////// S�ries e Fichas lan�adas no per�odo e cuto MP /////////////////////////

DROP TABLE IF EXISTS _Res_MovID_Fch_;
CREATE TABLE _Res_MovID_Fch_
SELECT vmi.SerieFch, vmi.Ficha,
cab.ClienteMO, cab.Fornecedor, cab.Procednc,
SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) PesoKg,
SUM(vmi.ValorT) ValorT
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi
LEFT JOIN ' + TMeuDB + '.vsinncab cab
  ON cab.MovimCod=vmi.MovimCod
WHERE vmi.MovimID=1
AND vmi.MovimNiv=0
AND vmi.DataHora BETWEEN "2018-12-01"
  AND "2018-12-31 23:59:59"
GROUP BY vmi.SerieFch, vmi.Ficha;

SELECT * FROM _Res_MovID_Fch_

//////////////////////// Custo do couro curtido ////////////////////////////////

(*
DROP TABLE IF EXISTS _Res_MovID0100_;
CREATE TABLE _Res_MovID0100_
SELECT vmi.*
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi
LEFT JOIN _Res_MovID_Fch_ fch
  ON fch.SerieFch=vmi.SerieFch
  AND fch.Ficha=vmi.Ficha
WHERE MovimID=1
AND MovimNiv=0
AND DataHora >= "2018-12-01"
;
*)

DROP TABLE IF EXISTS _Res_MovID0613_;
CREATE TABLE _Res_MovID0613_
SELECT vmi.DataHora, vmi.SerieFch, vmi.Ficha,
SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) PesoKg,
SUM(vmi.AreaM2) AreaM2, SUM(vmi.ValorT) ValorT,
SUM(vmi.CustoMOTot) CustoMOTot,
SUM(vmi.ValorMP) ValorMP,
SUM(vmi.QtdAntPeso) QtdAntPeso,
SUM(vmi.ValorMP)/ SUM(vmi.QtdAntPeso) CustoMPKg,
SUM(vmi.CustoMOTot)/ SUM(vmi.QtdAntPeso) CustoMOkg,
SUM(vmi.ValorT) / SUM(vmi.AreaM2) CustoM2
FROM _Res_MovID_Fch_ fch
LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi
  ON fch.SerieFch=vmi.SerieFch
  AND fch.Ficha=vmi.Ficha
WHERE vmi.MovimID=6
AND vmi.MovimNiv=13
AND vmi.DataHora >= "2018-12-01"
GROUP BY vmi.SerieFch, vmi.Ficha
ORDER BY vmi.DataHora, vmi.SerieFch, vmi.Ficha;

SELECT * FROM _Res_MovID0613_


SELECT * FROM _Res_MovID_Fch_

//////////////////////// Classes usadas e suas informa��es /////////////////////

SELECT IF(gg1.Referencia <> '', gg1.Referencia,
CONCAT("Red.",ggx.Controle)) Referencia, vmi.GraGruX, SUM(AreaM2) AreaM2,
val.CustoPreco
FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi
LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX
LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1
LEFT JOIN _Res_MovID_Fch_ fch
  ON fch.SerieFch=vmi.SerieFch
  AND fch.Ficha=vmi.Ficha
LEFT JOIN ' + TMeuDB + '.gragruval val
  ON val.GraGruX=vmi.GraGruX
  AND val.GraCusPrc=6
  AND val.Entidade=-11
WHERE MovimID IN (7, 14)
AND MovimNiv=2
AND DataHora >= "2018-12-01"
AND AreaM2>0
GROUP BY vmi.GraGruX
ORDER BY vmi.AreaM2 DESC

//////////////////////  Resultados de classes por Ficha ////////////////////////

SELECT fch.SerieFch, fch.Ficha,
SUM(vmi.Pecas) PecasTt, SUM(vmi.AreaM2) AreaM2Tt,

SUM(IF(vmi.GraGruX=000033, vmi.Pecas, 0)) Pecas01,
SUM(IF(vmi.GraGruX=000033, vmi.AreaM2, 0)) Area01,

SUM(IF(vmi.GraGruX=000008, vmi.Pecas, 0)) Pecas02,
SUM(IF(vmi.GraGruX=000008, vmi.AreaM2, 0)) Area02

FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi
LEFT JOIN _Res_MovID_Fch_ fch
  ON fch.SerieFch=vmi.SerieFch
  AND fch.Ficha=vmi.Ficha
WHERE MovimID IN (7, 14)
AND MovimNiv=2
AND DataHora >= "2018-12-01"
AND fch.Ficha<>0
GROUP BY fch.SerieFch, fch.Ficha
ORDER BY fch.SerieFch, fch.Ficha
}

var
  //SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_GGX: String;
  //TemIMEIMrt
  I, ClienteMO, Fornecedor, Procednc, GraCusPrc: Integer;
  SQL_Periodo, SQL_DataMin, SQL_ClienteMO, SQL_Fornecedor, SQL_Procednc,
  GGXsExtras, SQL_Cols, Liga, GGXsPrc: String;
begin
  Screen.Cursor := crHourGlass;
  try
    //
    GraCusPrc := EdGraCusPrc.ValueVariant;
    if MyObjects.FIC(GraCusPrc=0, EdGraCusPrc, 'Informe a tabela de pre�os!') then
      Exit;
    ClienteMO := EdClienteMO.ValueVariant;
    if MyObjects.FIC(ClienteMO=0, EdClienteMO, 'Informe o dono dos couros!') then
      Exit;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela tempor�ria de couro in natura');
    for I := 0 to FCols-1 do
    begin
      if I = FCols-1 then
        Liga := ''
      else
        Liga := ',' + sLineBreak;
      //
      FColunas[I] := '';
      FPrecos[I] := 0.000000;
      FColSQL[I]  :=
        '0.000 Pecas_' + Geral.FFN(I + 1, 2) +
        ', 0.000 AreaM2_' + Geral.FFN(I + 1, 2) +
        ', 0.000 Valor_' + Geral.FFN(I + 1, 2) +
        ', 0.000 Custo_' + Geral.FFN(I + 1, 2) +
        ', 0.000 CusM2_' + Geral.FFN(I + 1, 2) +
        Liga;
    end;
    SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora ',
      TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked);
    //
    SQL_DataMin := 'AND vmi.DataHora >= "' + Geral.FDT(TPDataIni.Date, 1) + '" ';
    //
    SQL_ClienteMO := '';
    ClienteMO := EdClienteMO.ValueVariant;
    if ClienteMO <> 0 then
      SQL_ClienteMO := 'AND cab.ClienteMO=' + Geral.FF0(ClienteMO);
    //
    SQL_Fornecedor := '';
    Fornecedor := EdFornecedor.ValueVariant;
    if Fornecedor <> 0 then
      SQL_Fornecedor := 'AND cab.Fornecedor=' + Geral.FF0(Fornecedor);
    //
    SQL_Procednc := '';
    Procednc := EdProcednc.ValueVariant;
    if Procednc <> 0 then
      SQL_Procednc := 'AND cab.Procednc=' + Geral.FF0(Procednc);
    //

  (*
    SQL_GGX := '';
    if EdGraGruX.ValueVariant <> 0 then
      SQL_GGX := 'AND vmi.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant);
  *)
    //
    //TemIMEIMrt := 1;//QrVsInnCabTemIMEIMrt.Value;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1,  DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _Res_MovID_Fch_; ',
    'CREATE TABLE _Res_MovID_Fch_ ',
    'SELECT vmi.DataHora, vmi.SerieFch, vmi.Ficha, ',
    'cab.ClienteMO, cab.Fornecedor, cab.Procednc, ',
    'SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) PesoKg, ',
    'SUM(vmi.ValorT) ValorT ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN ' + TMeuDB + '.vsinncab cab ',
    '  ON cab.MovimCod=vmi.MovimCod ',
    'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidCompra)), // 1
    'AND vmi.MovimNiv=0 ',
    SQL_Periodo,
    SQL_ClienteMO,
    SQL_Fornecedor,
    SQL_Procednc,
    'GROUP BY vmi.SerieFch, vmi.Ficha; ',
    ' ',
    //'SELECT * FROM _Res_MovID_Fch_ ',
    '']);
    //Geral.MB_SQL(Self, DModG.QrUpdPID1);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela tempor�ria de artigos gerados');
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1,  DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _Res_MovID0613_; ',
    'CREATE TABLE _Res_MovID0613_ ',
    'SELECT vmi.SerieFch, vmi.Ficha, ',
    'SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) PesoKg, ',
    'SUM(vmi.AreaM2) AreaM2, SUM(vmi.ValorT) ValorT, ',
    'SUM(vmi.CustoMOTot) CustoMOTot, ',
    'SUM(vmi.ValorMP) ValorMP, ',
    'SUM(vmi.QtdAntPeso) QtdAntPeso, ',
    'SUM(vmi.ValorMP)/ SUM(vmi.QtdAntPeso) CustoMPKg, ',
    'SUM(vmi.CustoMOTot)/ SUM(vmi.QtdAntPeso) CustoMOkg, ',
    'SUM(vmi.ValorT) / SUM(vmi.AreaM2) CustoM2 ',
    'FROM _Res_MovID_Fch_ fch ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    '  ON fch.SerieFch=vmi.SerieFch ',
    '  AND fch.Ficha=vmi.Ficha ',
    'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidIndsXX)), // 6
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
    SQL_DataMin,
    'GROUP BY vmi.SerieFch, vmi.Ficha ',
    'ORDER BY vmi.DataHora, vmi.SerieFch, vmi.Ficha; ',
    '']);
    //Geral.MB_SQL(Self, DModG.QrUpdPID1);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando classes usadas e suas informa��es');
    UnDmkDAC_PF.AbreMySQLQuery0(QrClasses,  DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _Res_MovIDClas_; ',
    'CREATE TABLE _Res_MovIDClas_ ',
    'SELECT IF(gg1.Referencia <> "", gg1.Referencia, ',
    'CONCAT("Red.",ggx.Controle)) Referencia, vmi.GraGruX, SUM(AreaM2) AreaM2, ',
    'val.CustoPreco ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN _Res_MovID_Fch_ fch ',
    '  ON fch.SerieFch=vmi.SerieFch ',
    '  AND fch.Ficha=vmi.Ficha ',
    'LEFT JOIN ' + TMeuDB + '.gragruval val ',
    '  ON val.GraGruX=vmi.GraGruX ',
    //'  AND val.GraCusPrc=6 ',
    '  AND val.GraCusPrc=' + Geral.FF0(GraCusPrc),
    //'  AND val.Entidade=-11 ',
    '  AND val.Entidade=' + Geral.FF0(ClienteMO),
    'WHERE MovimID IN (' +
    Geral.FF0(Integer(emidClassArtXXUni)) + ', ' + // 7
    Geral.FF0(Integer(emidClassArtXXMul)) + ') ',  // 14
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)), // 2
    SQL_DataMin,
    'AND AreaM2>0 ',
    'GROUP BY vmi.GraGruX ',
    //'ORDER BY vmi.AreaM2 DESC ',
    '; ',
    'SELECT * ',
    'FROM _Res_MovIDClas_ ',
    'ORDER BY AreaM2 DESC; ',
    '']);
    //Geral.MB_SQL(Self, QrClasses);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados das classifica��es');
    GGXsExtras := '';
    GGXsPrc    := '';
    QrClasses.First;
    while not QrClasses.Eof do
    begin
      if QrClassesCustoPreco.Value < 0.0000001 then
        GGXsPrc := GGXsPrc + ', ' + Geral.FF0(QrClassesGraGruX.Value);
      //
      I := QrClasses.RecNo - 1;
      //
      if (I >= FCols-1) and (QrClasses.RecordCount > FCols) then
      begin
        Liga := '';
        GGXsExtras := GGXsExtras + ', ' + Geral.FF0(QrClassesGraGruX.Value) + sLineBreak;
      end else
      begin
        Liga := ',' + sLineBreak;
        //
        FColunas[I] := QrClassesReferencia.Value;
        FPrecos[I]  := QrClassesCustoPreco.Value;
        FColSQL[I]  :=
        'SUM(IF(vmi.GraGruX=' + Geral.FF0(QrClassesGraGruX.Value) + ', vmi.Pecas, 0)) Pecas_' + Geral.FFN(I + 1, 2) +
        ', SUM(IF(vmi.GraGruX=' + Geral.FF0(QrClassesGraGruX.Value) + ', vmi.AreaM2, 0)) AreaM2_' + Geral.FFN(I + 1, 2) +
        ', SUM(IF(vmi.GraGruX=' + Geral.FF0(QrClassesGraGruX.Value) + ', vmi.AreaM2, 0)) * ' + Geral.FFT_Dot(QrClassesCustoPreco.Value, 10, siNegativo) + ' Valor_' + Geral.FFN(I + 1, 2) +
        ', SUM(IF(vmi.GraGruX=' + Geral.FF0(QrClassesGraGruX.Value) + ', vmi.AreaM2, 0)) * cus.CustoM2 Custo_' + Geral.FFN(I + 1, 2) +
        ', cus.CustoM2 CusM2_' + Geral.FFN(I + 1, 2) + Liga;
        //
      end;
      //
      QrClasses.Next;
    end;
    if GGXsPrc <> '' then
    begin;
      Geral.MB_Aviso('Reduzidos sem pre�o definido na tabela selecionada: ' + Copy(GGXsPrc, 2));
    end;
    if GGXsExtras <> '' then
    begin;
      Geral.MB_Info('Reduzidos n�o calculados: ' + GGXsExtras);
  (*
      Liga := '';
      FColunas[FCols-1] := 'V�rios';
      FColSQL[FCols-1]  :=
      'SUM(IF(vmi.GraGruX IN (' + GGXsExtras + '), vmi.Pecas, 0)) Pecas_' + Geral.FFN(FCols, 2) +
      ', SUM(IF(vmi.GraGruX IN (' + GGXsExtras + '), vmi.AreaM2, 0)) AreaM2_' + Geral.FFN(FCols, 2) +
      ', SUM(IF(vmi.GraGruX IN (' + GGXsExtras + '), vmi.AreaM2, 0)) * ' + Geral.FFT_Dot(QrClassesCustoPreco.Value, 10, siNegativo) + ' Valor_' + Geral.FFN(I + 1, 2) +
      ', SUM(IF(vmi.GraGruX IN (' + GGXsExtras + '), vmi.AreaM2, 0)) * cus.CustoM2 Custo_' + Geral.FFN(I + 1, 2) + Liga;
  *)
    end;
    //
    SQL_Cols := '';
    for I := 0 to FCols-1 do
      SQL_Cols := SQL_Cols + FColSQL[I];// + sLineBreak;
    //
    //UnDmkDAC_PF.AbreMySQLQuery0(Query,  DModG.MyPID_DB, [
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1,  DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _Res_MovID0702_1402_; ',
    'CREATE TABLE _Res_MovID0702_1402_ ',
    //
    'SELECT fch.DataHora, fch.SerieFch, fch.Ficha,',
    'SUM(vmi.Pecas) PecasTt, SUM(vmi.AreaM2) AreaM2Tt,',
    '',
    SQL_Cols,
    '',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi',
    'LEFT JOIN _Res_MovID_Fch_ fch',
    '  ON fch.SerieFch=vmi.SerieFch',
    '  AND fch.Ficha=vmi.Ficha',
    'LEFT JOIN _res_movid0613_ cus ',
    'ON cus.SerieFch=vmi.SerieFch ',
    'AND cus.Ficha=vmi.Ficha ',
    'WHERE MovimID IN (' +
    Geral.FF0(Integer(emidClassArtXXUni)) + ', ' + // 7
    Geral.FF0(Integer(emidClassArtXXMul)) + ') ',  // 14
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)), // 2
    SQL_DataMin,
    'AND fch.Ficha<>0',
    'GROUP BY fch.SerieFch, fch.Ficha',
    'ORDER BY fch.SerieFch, fch.Ficha',
    '']);
    //Geral.MB_Teste(DModG.QrUpdPID1.SQL.Text);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando impress�o');
    UnDmkDAC_PF.AbreMySQLQuery0(QrMargem,  DModG.MyPID_DB, [
    'SELECT res.*,',
    'IF(AreaM2_01=0,0,((Valor_01 - Custo_01) / Custo_01) * 100) Margem_01,',
    'IF(AreaM2_02=0,0,((Valor_02 - Custo_02) / Custo_02) * 100) Margem_02,',
    'IF(AreaM2_03=0,0,((Valor_03 - Custo_03) / Custo_03) * 100) Margem_03,',
    'IF(AreaM2_04=0,0,((Valor_04 - Custo_04) / Custo_04) * 100) Margem_04,',
    'IF(AreaM2_05=0,0,((Valor_05 - Custo_05) / Custo_05) * 100) Margem_05,',
    'IF(AreaM2_06=0,0,((Valor_06 - Custo_06) / Custo_06) * 100) Margem_06,',
    'IF(AreaM2_07=0,0,((Valor_07 - Custo_07) / Custo_07) * 100) Margem_07,',
    'IF(AreaM2_08=0,0,((Valor_08 - Custo_08) / Custo_08) * 100) Margem_08,',
    'IF(AreaM2_09=0,0,((Valor_09 - Custo_09) / Custo_09) * 100) Margem_09,',
    'IF(AreaM2_10=0,0,((Valor_10 - Custo_10) / Custo_10) * 100) Margem_10,',
    'IF(AreaM2_11=0,0,((Valor_11 - Custo_11) / Custo_11) * 100) Margem_11,',
    'IF(AreaM2_12=0,0,((Valor_12 - Custo_12) / Custo_12) * 100) Margem_12,',
    'IF(AreaM2_13=0,0,((Valor_13 - Custo_13) / Custo_13) * 100) Margem_13,',
    'IF(AreaM2_14=0,0,((Valor_14 - Custo_14) / Custo_14) * 100) Margem_14,',
    'IF(AreaM2_15=0,0,((Valor_15 - Custo_15) / Custo_15) * 100) Margem_15,',
    'Valor_01 + Valor_02 + Valor_03 +',
    'Valor_04 + Valor_05 + Valor_06 +',
    'Valor_07 + Valor_08 + Valor_09 +',
    'Valor_10 + Valor_11 + Valor_12 +',
    'Valor_13 + Valor_14 + Valor_15 ValorTot,',
    '',
    'Custo_01 + Custo_02 + Custo_03 +',
    'Custo_04 + Custo_05 + Custo_06 +',
    'Custo_07 + Custo_08 + Custo_09 +',
    'Custo_10 + Custo_11 + Custo_12 +',
    'Custo_13 + Custo_14 + Custo_15 CustoTot,',
    '',
    '(Valor_01 + Valor_02 + Valor_03 +',
    'Valor_04 + Valor_05 + Valor_06 +',
    'Valor_07 + Valor_08 + Valor_09 +',
    'Valor_10 + Valor_11 + Valor_12 +',
    'Valor_13 + Valor_14 + Valor_15)',
    '-',
    '(Custo_01 + Custo_02 + Custo_03 +',
    'Custo_04 + Custo_05 + Custo_06 +',
    'Custo_07 + Custo_08 + Custo_09 +',
    'Custo_10 + Custo_11 + Custo_12 +',
    'Custo_13 + Custo_14 + Custo_15) MargemTot,',
    '',
    '(((Valor_01 + Valor_02 + Valor_03 +',
    'Valor_04 + Valor_05 + Valor_06 +',
    'Valor_07 + Valor_08 + Valor_09 +',
    'Valor_10 + Valor_11 + Valor_12 +',
    'Valor_13 + Valor_14 + Valor_15)',
    '-',
    '(Custo_01 + Custo_02 + Custo_03 +',
    'Custo_04 + Custo_05 + Custo_06 +',
    'Custo_07 + Custo_08 + Custo_09 +',
    'Custo_10 + Custo_11 + Custo_12 +',
    'Custo_13 + Custo_14 + Custo_15))',
    '/',
    '(Custo_01 + Custo_02 + Custo_03 +',
    'Custo_04 + Custo_05 + Custo_06 +',
    'Custo_07 + Custo_08 + Custo_09 +',
    'Custo_10 + Custo_11 + Custo_12 +',
    'Custo_13 + Custo_14 + Custo_15))',
    '* 100 MargemPer, ',
    '',
    'CONCAT(vsf.Nome, " ", res.Ficha) NO_SERIE_FICHA ',
    'FROM _res_movid0702_1402_ res',
    'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=res.SerieFch ',
    '',
    '']);
    //Geral.MB_SQL(Self, QrMargem);
    //
    MyObjects.frxDefineDataSets(frxWET_CURTI_164_02, [
      DModG.frxDsDono,
      frxDsMargem
    ]);
    MyObjects.frxMostra(frxWET_CURTI_164_02, 'Margem Artigos de Ribeira Classificados');
    //
    //
    // ...
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSImpResultVS.EntradaDeCouro();
begin
  ReopenVSInnIts();
  MyObjects.frxDefineDataSets(frxWET_CURTI_164_03, [
    DModG.frxDsDono,
    frxDsVSInnIts
    //frxDsVSItsBxa,
    //frxDsVSItsGer,
    //
    //frxDsVSMovIts,
    //frxDsVSGerados
  ]);
  MyObjects.frxMostra(frxWET_CURTI_164_03, 'Resultado Ribeira');
end;

procedure TFmVSImpResultVS.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpResultVS.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcedncs, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCusPrc, Dmod.MyDB);
  VS_PF.AbreGraGruXY(QrGraGruX, 'AND ggx.Controle > 0');
  //
  TPDataIni.Date := IncMonth(Geral.PrimeiroDiaDoMes(Date()), -1);
  TPDataFim.Date := Geral.PrimeiroDiaDoMes(Date()) - 1;
end;

procedure TFmVSImpResultVS.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpResultVS.frxWET_CURTI_164_01GetValue(const VarName: string;
  var Value: Variant);
var
  I: Integer;
  x, y: String;
begin
  if VarName = 'VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_VAL_ITEM' then
    Value := QrVSMovItsMargem.Value
  else
  if VarName = 'VARF_CLIENTMO' then
    Value := dmkPF.ParValueCodTxt(
      'Cliente de M.O.: ', CBClienteMO.Text, EdClienteMO.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(TPDataIni.Date, TPDataFim.Date,
    CkDataIni.Checked, CkDataFim.Checked, '', 'at�', '')
  else
  if VarName = 'VARF_MPRIMAART' then
    Value := dmkPF.ParValueCodTxt(
      '', CBGraGruX.Text, EdGraGruX.ValueVariant, 'TODOS')
  else
  begin
    x := Copy(VarName, 1, 6);
    y := Copy(VarName, 7);
    if (Length(VarName) = 8) and (x = 'COURO_') then
    begin
      I := Geral.IMV(y);
      Value :=
        FColunas[I-1] + ' (' + Geral.FFT(FPrecos[I-1], 2, siNegativo) + ')';
    end;
  end
end;

procedure TFmVSImpResultVS.QrVSInnCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSInnIts();
end;

procedure TFmVSImpResultVS.QrVSInnCabBeforeClose(DataSet: TDataSet);
begin
  QrVSInnIts.Close;
end;

procedure TFmVSImpResultVS.QrVSInnItsAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVsMovIts, Dmod.MyDB, [
  'SELECT ',
  'SUM(IF(MovimNiv IN (29,34), 0, CustoMOTot)) CustoMOTot,',
  'SUM(IF(MovimNiv IN (29,34), -CustoPQ, 0)) CustoPQ,',
  'SUM(IF(MovimNiv IN (29,34), CustoPQ, CustoMOTot)) Margem',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi',
  'WHERE vmi.SerieFch=' + Geral.FF0(QrVSInnItsSerieFch.Value),
  'AND vmi.Ficha=' + Geral.FF0(QrVSInnItsFicha.Value),
  'AND (NOT MovimNiv IN (14,15,30,35)) ',
  'AND NOT vmi.MovimID IN (6,29)',
  '']);
end;

procedure TFmVSImpResultVS.ReopenVSInnIts();
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_GGX: String;
  TemIMEIMrt, ClienteMO, Fornecedor: Integer;
  SQL_Periodo, SQL_ClienteMO, SQL_Fornecedor: String;
begin
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE vmi.DataHora ',
    TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked);
  //
  SQL_ClienteMO := '';
  ClienteMO := EdClienteMO.ValueVariant;
  if ClienteMO <> 0 then
    SQL_ClienteMO := 'AND cab.ClienteMO=' + Geral.FF0(ClienteMO);
  //
  SQL_GGX := '';
  if EdGraGruX.ValueVariant <> 0 then
    SQL_GGX := 'AND vmi.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant);
  //
  SQL_Fornecedor := '';
  Fornecedor := EdFornecedor.ValueVariant;
  if Fornecedor <> 0 then
    SQL_Fornecedor := 'AND cab.Fornecedor=' + Geral.FF0(Fornecedor);
  //
  //
  TemIMEIMrt := 1;//QrVsInnCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  'IF(vmi.SdoVrtPeca > 0, 0, ',
  '  IF(vmi.QtdGerArM2 > 0, vmi.QtdGerArM2 / vmi.Pecas, 0)) RendKgm2, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT( ',
  '  IF(vmi.QtdGerArM2 > 0, vmi.PesoKg/vmi.QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE( ',
  'FORMAT(vmi.NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT( ',
  '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, ',
  'vmi.ClientMO, fch.Nome NO_SerieFch, ',
  //
  'cab.*, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO, ',
  'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC, ',
  'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN vsserfch  fch ON fch.Codigo=vmi.SerieFch ',
  'LEFT JOIN vsinncab  cab ON cab.MovimCod=vmi.MovimCod ',
  'LEFT JOIN entidades ent ON ent.Codigo=cab.Empresa ',
  'LEFT JOIN entidades frn ON frn.Codigo=cab.Fornecedor ',
  'LEFT JOIN entidades trn ON trn.Codigo=cab.Transporta ',
  'LEFT JOIN entidades cli ON cli.Codigo=cab.ClienteMO ',
  'LEFT JOIN entidades prc ON prc.Codigo=cab.Procednc ',
  'LEFT JOIN entidades mot ON mot.Codigo=cab.Motorista ',
  '']);
  SQL_Wher := Geral.ATS([
  SQL_Periodo,
  SQL_ClienteMO,
  SQL_GGX,
  SQL_Fornecedor,
  'AND vmi.MovimID=1 ',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVsInnIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_CLIENTEMO, NO_PRD_TAM_COR, NO_SerieFch, Controle ',
  '']);
  //Geral.MB_Teste(QrVsInnIts.SQL.Text);
end;

procedure TFmVSImpResultVS.ResultadoRibeira();
begin
  ReopenVSInnIts();
  MyObjects.frxDefineDataSets(frxWET_CURTI_164_01, [
    DModG.frxDsDono,
    frxDsVSInnIts
    //frxDsVSItsBxa,
    //frxDsVSItsGer,
    //
    //frxDsVSMovIts,
    //frxDsVSGerados
  ]);
  MyObjects.frxMostra(frxWET_CURTI_164_01, 'Resultado Ribeira');
end;

procedure TFmVSImpResultVS.RGRelatorioClick(Sender: TObject);
var
  ClienteMO, Fornecedor, Procednc, GraGruX, GraCusPrc: Boolean;
begin
  ClienteMO  := False;
  Fornecedor := False;
  Procednc   := False;
  GraGruX    := False;
  GraCusPrc  := False;
  //
  case RGRelatorio.Itemindex of
    0: ;
    1, 3: //
    begin
      ClienteMO  := True;
      Fornecedor := True;
      //Procednc   := False;
      GraGruX    := True;
      //GraCusPrc  := False;
    end;
    2: //
    begin
      ClienteMO  := True;
      Fornecedor := True;
      Procednc   := True;
      //GraGruX    := False;
      GraCusPrc  := True;
    end;
  end;
  //
  LaClienteMO.Enabled  := ClienteMO;
  EdClienteMO.Enabled  := ClienteMO;
  CBClienteMO.Enabled  := ClienteMO;
  //
  LaFornecedor.Enabled := Fornecedor;
  EdFornecedor.Enabled := Fornecedor;
  CBFornecedor.Enabled := Fornecedor;
  //
  LaProcednc.Enabled   := Procednc;
  EdProcednc.Enabled   := Procednc;
  CBProcednc.Enabled   := Procednc;
  //
  LaGraGruX.Enabled    := GraGruX;
  EdGraGruX.Enabled    := GraGruX;
  CBGraGruX.Enabled    := GraGruX;
  //
  LaGraCusPrc.Enabled  := GraCusPrc;
  EdGraCusPrc.Enabled  := GraCusPrc;
  CBGraCusPrc.Enabled  := GraCusPrc;
  //
  if ClienteMO and (EdClienteMO.ValueVariant = 0) then
  begin
    EdClienteMO.ValueVariant := -11;
    CBClienteMO.KeyValue := -11;
  end;
  //
  if GraCusPrc and (EdGraCusPrc.ValueVariant = 0) then
  begin
    EdGraCusPrc.ValueVariant := 6; // Cont�bil
    CBGraCusPrc.KeyValue := 6;  // Cont�bil
  end;
end;

end.
