unit VSCorrigeSN2Orfao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables,
  UnProjGroup_Consts, Vcl.Menus;

type
  TFmVSCorrigeSN2Orfao = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAcao: TBitBtn;
    LaTitulo1C: TLabel;
    GradeItens: TdmkDBGridZTO;
    QrVSMovIts: TmySQLQuery;
    DsVSMovIts: TDataSource;
    PMAcao: TPopupMenu;
    IrparajaneladoIMEI1: TMenuItem;
    IrparaajaneladoMovimento1: TMenuItem;
    ExcluirIMEIincondicionalmente1: TMenuItem;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure IrparaajaneladoMovimento1Click(Sender: TObject);
    procedure IrparajaneladoIMEI1Click(Sender: TObject);
    procedure ExcluirIMEIincondicionalmente1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FProblemas: Integer;
    //
    function ReopenVSMovIts(): Integer;
  end;

  var
  FmVSCorrigeSN2Orfao: TFmVSCorrigeSN2Orfao;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, MyDBCheck, UnVS_PF;

{$R *.DFM}

procedure TFmVSCorrigeSN2Orfao.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmVSCorrigeSN2Orfao.BtNenhumClick(Sender: TObject);
begin
  GradeItens.SelectedRows.Clear;
end;

procedure TFmVSCorrigeSN2Orfao.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCorrigeSN2Orfao.BtTudoClick(Sender: TObject);
begin
  MyObjects.DBGridSelectAll(TDBGrid(GradeItens));
end;

procedure TFmVSCorrigeSN2Orfao.ExcluirIMEIincondicionalmente1Click(
  Sender: TObject);
var
  I: Integer;
begin
  if GradeItens.SelectedRows.Count > 0 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    with GradeItens.DataSource.DataSet do
    for I:= 0 to GradeItens.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(GradeItens.SelectedRows.Items[I]));
      GotoBookmark(GradeItens.SelectedRows.Items[I]);
      //
      VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(
      QrVSMovIts.FieldByName('Controle').AsInteger,
      Integer(TEstqMotivDel.emtdWetCurti136),
      Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
    end;
    ReopenVSMovIts();
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmVSCorrigeSN2Orfao.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCorrigeSN2Orfao.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FProblemas := ReopenVSMovIts();
end;

procedure TFmVSCorrigeSN2Orfao.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCorrigeSN2Orfao.IrparaajaneladoMovimento1Click(Sender: TObject);
begin
  VS_PF.MostraFormVS_Do_IMEI(QrVSMovIts.FieldByName('Controle').AsInteger);
end;

procedure TFmVSCorrigeSN2Orfao.IrparajaneladoIMEI1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovIts(QrVSMovIts.FieldByName('Controle').AsInteger);
end;

function TFmVSCorrigeSN2Orfao.ReopenVSMovIts(): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _TESTE_2; ',
  'CREATE TABLE _TESTE_2 ',
  'SELECT Controle, SrcNivel2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE SrcNivel2 <> 0 ',
  'UNION ',
  'SELECT Controle, SrcNivel2 ',
  'FROM ' + TMeuDB + '.' + CO_TAB_VMB + ' ',
  'WHERE SrcNivel2 <> 0 ',
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _TESTE_1; ',
  'CREATE TABLE _TESTE_1 ',
  'SELECT Controle ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE Controle IN ( ',
  '  SELECT SrcNivel2 ',
  '  FROM _TESTE_2 t2 ',
  ') ',
  'UNION ',
  'SELECT Controle ',
  'FROM ' + TMeuDB + '.' + CO_TAB_VMB + ' ',
  'WHERE Controle IN ( ',
  '  SELECT SrcNivel2 ',
  '  FROM _TESTE_2 t2 ',
  ') ',
  '; ',
  ' ',
  //'SELECT * ',
  //'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  //'WHERE Controle IN ( ',
  '  SELECT Controle ',
  '  FROM _TESTE_2 t2 ',
  '  WHERE NOT SrcNivel2 IN ( ',
  '    SELECT Controle ',
  '    FROM _TESTE_1 ',
  //'  ) ',
  ') ',
  '']);
  Result := QrVSMovIts.RecordCount;
  if Result > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle IN ( ',
    '  SELECT Controle ',
    '  FROM _TESTE_2 t2 ',
    '  WHERE NOT SrcNivel2 IN ( ',
    '    SELECT Controle ',
    '    FROM _TESTE_1 ',
    '  ) ',
    ') ',
    '']);
  end;
end;

end.
