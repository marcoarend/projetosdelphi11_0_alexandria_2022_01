object FmVSGerClaCab: TFmVSGerClaCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-046 :: Gerenciamento de Classifica'#231#227'o'
  ClientHeight = 840
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 744
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 681
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 744
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 680
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtVoltaAClassificar: TBitBtn
          Tag = 600
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Volta a classificar'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtVoltaAClassificarClick
        end
        object BtCorrigeBaixas: TBitBtn
          Tag = 253
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Corrige Baixas'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtCorrigeBaixasClick
        end
        object BtAlterarDatas: TBitBtn
          Tag = 10098
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Alterar datas'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtAlterarDatasClick
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 101
      Align = alTop
      Caption = ' Dados da Configura'#231#227'o de OC: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 82
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label32: TLabel
          Left = 8
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = dmkDBEdit1
        end
        object Label23: TLabel
          Left = 72
          Top = 0
          Width = 24
          Height = 13
          Caption = 'O.C.:'
          FocusControl = dmkDBEdit2
        end
        object Label2: TLabel
          Left = 144
          Top = 0
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdit1
        end
        object Label22: TLabel
          Left = 548
          Top = 0
          Width = 25
          Height = 13
          Caption = #193'rea:'
          FocusControl = DBEdit14
        end
        object Label11: TLabel
          Left = 584
          Top = 0
          Width = 92
          Height = 13
          Caption = 'Lib. p/ reclassificar:'
          FocusControl = DBEdit3
        end
        object Label33: TLabel
          Left = 700
          Top = 0
          Width = 103
          Height = 13
          Caption = 'Configur. p/ reclassif.:'
          FocusControl = DBEdit25
        end
        object Label34: TLabel
          Left = 816
          Top = 0
          Width = 92
          Height = 13
          Caption = 'Fim reclassifica'#231#227'o:'
          FocusControl = DBEdit26
        end
        object Label1: TLabel
          Left = 932
          Top = 0
          Width = 32
          Height = 13
          Caption = 'IME-C:'
          FocusControl = DBEdit0
        end
        object Label5: TLabel
          Left = 8
          Top = 40
          Width = 163
          Height = 13
          Caption = 'Observa'#231#227'o sobre a classifica'#231#227'o:'
          FocusControl = DBEdit4
        end
        object Label37: TLabel
          Left = 584
          Top = 40
          Width = 30
          Height = 13
          Caption = 'Artigo:'
          FocusControl = DBEdit15
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 8
          Top = 16
          Width = 61
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsVSGerCla
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 72
          Top = 16
          Width = 69
          Height = 21
          TabStop = False
          DataField = 'CacCod'
          DataSource = DsVSGerCla
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit1: TDBEdit
          Left = 144
          Top = 16
          Width = 56
          Height = 21
          DataField = 'Empresa'
          DataSource = DsVSGerCla
          TabOrder = 2
        end
        object DBEdit2: TDBEdit
          Left = 200
          Top = 16
          Width = 345
          Height = 21
          DataField = 'NO_EMPRESA'
          DataSource = DsVSGerCla
          TabOrder = 3
        end
        object DBEdit14: TDBEdit
          Left = 548
          Top = 16
          Width = 32
          Height = 21
          DataField = 'NO_TIPO'
          DataSource = DsVSGerCla
          TabOrder = 4
        end
        object DBEdit3: TDBEdit
          Left = 584
          Top = 16
          Width = 112
          Height = 21
          DataField = 'NO_DtHrLibCla'
          DataSource = DsVSGerCla
          TabOrder = 5
        end
        object DBEdit25: TDBEdit
          Left = 700
          Top = 16
          Width = 112
          Height = 21
          DataField = 'NO_DtHrCfgCla'
          DataSource = DsVSGerCla
          TabOrder = 6
        end
        object DBEdit26: TDBEdit
          Left = 816
          Top = 16
          Width = 112
          Height = 21
          DataField = 'NO_DtHrFimCla'
          DataSource = DsVSGerCla
          TabOrder = 7
        end
        object DBEdit0: TdmkDBEdit
          Left = 932
          Top = 16
          Width = 61
          Height = 21
          TabStop = False
          DataField = 'MovimCod'
          DataSource = DsVSGerCla
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 8
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 56
          Width = 573
          Height = 21
          DataField = 'Nome'
          DataSource = DsVSGerCla
          TabOrder = 9
        end
        object DBEdit15: TDBEdit
          Left = 584
          Top = 56
          Width = 56
          Height = 21
          DataField = 'GraGruX'
          DataSource = DsVSGerCla
          TabOrder = 10
        end
        object DBEdit28: TDBEdit
          Left = 644
          Top = 56
          Width = 348
          Height = 21
          DataField = 'NO_PRD_TAM_COR'
          DataSource = DsVSGerCla
          TabOrder = 11
        end
      end
    end
    object PCFormaClas: TPageControl
      Left = 0
      Top = 101
      Width = 1008
      Height = 485
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Couro a Couro'
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 457
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Splitter2: TSplitter
            Left = 545
            Top = 188
            Height = 269
            Align = alRight
            ExplicitLeft = 553
            ExplicitTop = 289
            ExplicitHeight = 242
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 188
            Align = alTop
            Caption = ' Sequencia de reconfigura'#231#245'es (trocas de pallets de destino): '
            TabOrder = 0
            object DGVSPaClaCab: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 996
              Height = 171
              Align = alClient
              DataSource = DsVSPaClaCab
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID Reconfig'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VSMovIts'
                  Title.Caption = 'IME-I'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CacCod'
                  Title.Caption = 'OC'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LstPal01'
                  Title.Caption = 'Pallet Box 1'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LstPal02'
                  Title.Caption = 'Pallet Box 2'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LstPal03'
                  Title.Caption = 'Pallet Box 3'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LstPal04'
                  Title.Caption = 'Pallet Box4'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LstPal05'
                  Title.Caption = 'Pallet Box 5'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LstPal06'
                  Title.Caption = 'Pallet Box 6'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtHrFimCla_TXT'
                  Title.Caption = 'Fim classifica'#231#227'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178':'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaP2'
                  Title.Caption = #193'rea ft'#178':'
                  Visible = True
                end>
            end
          end
          object Panel7: TPanel
            Left = 548
            Top = 188
            Width = 452
            Height = 269
            Align = alRight
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Splitter1: TSplitter
              Left = 226
              Top = 0
              Height = 269
              ExplicitHeight = 242
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 0
              Width = 226
              Height = 269
              Align = alLeft
              Caption = ' IME-Is de origem: '
              TabOrder = 0
              object DGVMI_Sorc: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 222
                Height = 252
                Align = alClient
                DataSource = DsVMI_Sorc
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'VMI_Sorc'
                    Title.Caption = 'IME-I origem'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VMI_Baix'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VMI_Dest'
                    Visible = True
                  end>
              end
            end
            object GroupBox4: TGroupBox
              Left = 229
              Top = 0
              Width = 223
              Height = 269
              Align = alClient
              Caption = ' IME-Is de baixa: '
              TabOrder = 1
              object DGVMI_Baix: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 219
                Height = 252
                Align = alClient
                DataSource = DsVMI_Baix
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'VMI_Baix'
                    Title.Caption = 'IME-I baixa'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VMI_Sorc'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VMI_Dest'
                    Visible = True
                  end>
              end
            end
          end
          object DGVSPaClaIts: TdmkDBGridZTO
            Left = -12
            Top = 323
            Width = 549
            Height = 161
            DataSource = DsVSPaClaIts
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            PopupMenu = PMVSPaClaIts
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tecla'
                Title.Caption = 'Box'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VSPallet'
                Title.Caption = 'Pallet'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VMI_Sorc'
                Title.Caption = 'IMEI Origem'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VMI_Dest'
                Title.Caption = 'IME-I Destino'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtHrIni'
                Title.Caption = 'In'#237'cio'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtHrFim_TXT'
                Title.Caption = 'Final'
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'M'#250'ltiplos'
        ImageIndex = 1
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 457
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox5: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 61
            Align = alTop
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 0
            object Label8: TLabel
              Left = 8
              Top = 16
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
              FocusControl = dmkDBEdit3
            end
            object Label12: TLabel
              Left = 140
              Top = 16
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label14: TLabel
              Left = 428
              Top = 16
              Width = 58
              Height = 13
              Caption = 'Data / hora:'
              Color = clBtnFace
              ParentColor = False
            end
            object Label24: TLabel
              Left = 932
              Top = 16
              Width = 56
              Height = 13
              Caption = 'ID Estoque:'
              Color = clBtnFace
              ParentColor = False
            end
            object Label25: TLabel
              Left = 580
              Top = 16
              Width = 33
              Height = 13
              Caption = 'Pe'#231'as:'
            end
            object Label26: TLabel
              Left = 668
              Top = 16
              Width = 39
              Height = 13
              Caption = #193'rea m'#178':'
            end
            object Label27: TLabel
              Left = 756
              Top = 16
              Width = 37
              Height = 13
              Caption = #193'rea ft'#178':'
            end
            object Label28: TLabel
              Left = 844
              Top = 16
              Width = 27
              Height = 13
              Caption = 'Peso:'
            end
            object Label29: TLabel
              Left = 68
              Top = 16
              Width = 58
              Height = 13
              Caption = 'ID Classific.:'
              FocusControl = DBEdit32
            end
            object dmkDBEdit3: TdmkDBEdit
              Left = 8
              Top = 32
              Width = 56
              Height = 21
              TabStop = False
              DataField = 'Codigo'
              DataSource = DsVSPaMulCab
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
              UpdType = utYes
              Alignment = taRightJustify
            end
            object DBEdit6: TDBEdit
              Left = 140
              Top = 32
              Width = 56
              Height = 21
              DataField = 'Empresa'
              DataSource = DsVSPaMulCab
              TabOrder = 1
            end
            object DBEdit12: TDBEdit
              Left = 428
              Top = 32
              Width = 148
              Height = 21
              DataField = 'DataHora'
              DataSource = DsVSPaMulCab
              TabOrder = 2
            end
            object DBEdit16: TDBEdit
              Left = 932
              Top = 32
              Width = 64
              Height = 21
              DataField = 'MovimCod'
              DataSource = DsVSPaMulCab
              TabOrder = 3
            end
            object DBEdit17: TDBEdit
              Left = 580
              Top = 32
              Width = 84
              Height = 21
              DataField = 'Pecas'
              DataSource = DsVSPaMulCab
              TabOrder = 4
            end
            object DBEdit29: TDBEdit
              Left = 844
              Top = 32
              Width = 84
              Height = 21
              DataField = 'PesoKg'
              DataSource = DsVSPaMulCab
              TabOrder = 5
            end
            object DBEdit30: TDBEdit
              Left = 668
              Top = 32
              Width = 84
              Height = 21
              DataField = 'AreaM2'
              DataSource = DsVSPaMulCab
              TabOrder = 6
            end
            object DBEdit31: TDBEdit
              Left = 756
              Top = 32
              Width = 84
              Height = 21
              DataField = 'AreaP2'
              DataSource = DsVSPaMulCab
              TabOrder = 7
            end
            object DBEdit32: TDBEdit
              Left = 68
              Top = 32
              Width = 69
              Height = 21
              DataField = 'CacCod'
              DataSource = DsVSPaMulCab
              TabOrder = 8
            end
          end
          object GBIts: TGroupBox
            Left = 0
            Top = 61
            Width = 1000
            Height = 396
            Align = alClient
            Caption = ' Itens da classifica'#231#227'o: '
            TabOrder = 1
            object DGDados: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 996
              Height = 379
              Align = alClient
              DataSource = DsVSPaMulIts
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_TTW'
                  Title.Caption = 'Tabela'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'IME-I'
                  Width = 60
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'StqCenLoc'
                  Title.Caption = 'Local'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ReqMovEstq'
                  Title.Caption = 'N'#176' RME'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pallet'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PRD_TAM_COR'
                  Title.Caption = 'Artigo'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaP2'
                  Title.Caption = #193'rea ft'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso kg'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorT'
                  Title.Caption = 'Valor total'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataHora'
                  Title.Caption = 'Data / Hora'
                  Width = 88
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_FORNECE'
                  Title.Caption = 'Fornecedor / Cliente'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PALLET'
                  Title.Caption = 'Obs. Pallet'
                  Width = 76
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observ'
                  Title.Caption = 'Observa'#231#245'es'
                  Width = 300
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 586
      Width = 1008
      Height = 94
      Align = alBottom
      Caption = ' Dados do IME-I de Origem: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 3
      object Label3: TLabel
        Left = 64
        Top = 16
        Width = 28
        Height = 13
        Caption = 'IME-I:'
        FocusControl = DBEdCodigo
      end
      object Label4: TLabel
        Left = 16
        Top = 56
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label7: TLabel
        Left = 124
        Top = 16
        Width = 30
        Height = 13
        Caption = 'Artigo:'
      end
      object Label10: TLabel
        Left = 508
        Top = 16
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label13: TLabel
        Left = 864
        Top = 16
        Width = 100
        Height = 13
        Caption = 'Data / hora gera'#231#227'o:'
        FocusControl = DBEdit13
      end
      object Label15: TLabel
        Left = 420
        Top = 56
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        FocusControl = DBEdit18
      end
      object Label16: TLabel
        Left = 492
        Top = 56
        Width = 43
        Height = 13
        Caption = 'Peso Kg:'
        FocusControl = DBEdit19
      end
      object Label17: TLabel
        Left = 564
        Top = 56
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        FocusControl = DBEdit20
      end
      object Label18: TLabel
        Left = 636
        Top = 56
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        FocusControl = DBEdit21
      end
      object Label19: TLabel
        Left = 708
        Top = 56
        Width = 39
        Height = 13
        Caption = '$ Custo:'
        FocusControl = DBEdit22
      end
      object Label20: TLabel
        Left = 780
        Top = 56
        Width = 50
        Height = 13
        Caption = 'Sdo Pe'#231'a:'
        FocusControl = DBEdit23
      end
      object Label21: TLabel
        Left = 852
        Top = 56
        Width = 38
        Height = 13
        Caption = 'Sdo Kg:'
        FocusControl = DBEdit24
      end
      object Label9: TLabel
        Left = 924
        Top = 56
        Width = 36
        Height = 13
        Caption = 'Sdo m'#178':'
        FocusControl = DBEdit27
      end
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 39
        Height = 13
        Caption = 'Arquivo:'
        FocusControl = DBEdit5
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 64
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Controle'
        DataSource = DsVMI_Orig
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 16
        Top = 72
        Width = 401
        Height = 21
        Color = clWhite
        DataField = 'Observ'
        DataSource = DsVMI_Orig
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit7: TDBEdit
        Left = 124
        Top = 32
        Width = 56
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsVMI_Orig
        TabOrder = 2
      end
      object DBEdit8: TDBEdit
        Left = 180
        Top = 32
        Width = 325
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVMI_Orig
        TabOrder = 3
      end
      object DBEdit9: TDBEdit
        Left = 508
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Terceiro'
        DataSource = DsVMI_Orig
        TabOrder = 4
      end
      object DBEdit10: TDBEdit
        Left = 564
        Top = 32
        Width = 297
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsVMI_Orig
        TabOrder = 5
      end
      object DBEdit13: TDBEdit
        Left = 864
        Top = 32
        Width = 129
        Height = 21
        DataField = 'DataHora'
        DataSource = DsVMI_Orig
        TabOrder = 6
      end
      object DBEdit18: TDBEdit
        Left = 420
        Top = 72
        Width = 68
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVMI_Orig
        TabOrder = 7
      end
      object DBEdit19: TDBEdit
        Left = 492
        Top = 72
        Width = 68
        Height = 21
        DataField = 'PesoKg'
        DataSource = DsVMI_Orig
        TabOrder = 8
      end
      object DBEdit20: TDBEdit
        Left = 564
        Top = 72
        Width = 68
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVMI_Orig
        TabOrder = 9
      end
      object DBEdit21: TDBEdit
        Left = 636
        Top = 72
        Width = 68
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVMI_Orig
        TabOrder = 10
      end
      object DBEdit22: TDBEdit
        Left = 708
        Top = 72
        Width = 68
        Height = 21
        DataField = 'ValorT'
        DataSource = DsVMI_Orig
        TabOrder = 11
      end
      object DBEdit23: TDBEdit
        Left = 780
        Top = 72
        Width = 68
        Height = 21
        DataField = 'SdoVrtPeca'
        DataSource = DsVMI_Orig
        TabOrder = 12
      end
      object DBEdit24: TDBEdit
        Left = 852
        Top = 72
        Width = 68
        Height = 21
        DataField = 'SdoVrtPeso'
        DataSource = DsVMI_Orig
        TabOrder = 13
      end
      object DBEdit27: TDBEdit
        Left = 924
        Top = 72
        Width = 68
        Height = 21
        DataField = 'SdoVrtArM2'
        DataSource = DsVMI_Orig
        TabOrder = 14
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 32
        Width = 45
        Height = 21
        DataField = 'NO_TTW'
        DataSource = DsVMI_Orig
        TabOrder = 15
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 392
        Height = 32
        Caption = 'Gerenciamento de Classifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 392
        Height = 32
        Caption = 'Gerenciamento de Classifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 392
        Height = 32
        Caption = 'Gerenciamento de Classifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = FmVSGerArtCab.BtIts
    Left = 156
    Top = 65524
  end
  object QrVSGerCla: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSGerClaBeforeOpen
    AfterOpen = QrVSGerClaAfterOpen
    BeforeClose = QrVSGerClaBeforeClose
    AfterScroll = QrVSGerClaAfterScroll
    OnCalcFields = QrVSGerClaCalcFields
    SQL.Strings = (
      'SELECT vgr.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      '/*'
      ', pal.Nome NO_PALLET'
      '*/'
      'FROM vsgerarta vgr'
      'LEFT JOIN entidades  ent ON ent.Codigo=vgr.Empresa'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vgr.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      '/*'
      'LEFT JOIN vspalleta  pal ON pal.Codigo=vgr.VSPallet '
      '*/'
      'WHERE vgr.Codigo > 0'
      'AND vgr.Codigo=:P0'
      ''
      '/*'
      'object QrVSGerClaVSPallet: TIntegerField'
      '  FieldName = '#39'VSPallet'#39
      'end'
      'object QrVSGerClaNO_PALLET: TWideStringField'
      '  FieldName = '#39'NO_PALLET'#39
      '  Size = 60'
      'end'
      '*/')
    Left = 20
    Top = 461
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        ParamType = ptUnknown
      end>
    object QrVSGerClaNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSGerClaNO_DtHrFimCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimCla'
      Calculated = True
    end
    object QrVSGerClaNO_DtHrLibCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibCla'
      Calculated = True
    end
    object QrVSGerClaNO_DtHrCfgCla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgCla'
      Calculated = True
    end
    object QrVSGerClaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerClaMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGerClaCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSGerClaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerClaNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSGerClaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerClaDtHrLibCla: TDateTimeField
      FieldName = 'DtHrLibCla'
    end
    object QrVSGerClaDtHrCfgCla: TDateTimeField
      FieldName = 'DtHrCfgCla'
    end
    object QrVSGerClaDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
    end
    object QrVSGerClaTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSGerClaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGerClaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGerClaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGerClaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGerClaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGerClaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGerClaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGerClaNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSGerClaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerClaDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
    end
    object QrVSGerClaPecasMan: TFloatField
      FieldName = 'PecasMan'
    end
    object QrVSGerClaAreaManM2: TFloatField
      FieldName = 'AreaManM2'
    end
    object QrVSGerClaAreaManP2: TFloatField
      FieldName = 'AreaManP2'
    end
    object QrVSGerClaCustoManMOKg: TFloatField
      FieldName = 'CustoManMOKg'
    end
    object QrVSGerClaCustoManMOTot: TFloatField
      FieldName = 'CustoManMOTot'
    end
    object QrVSGerClaValorManMP: TFloatField
      FieldName = 'ValorManMP'
    end
    object QrVSGerClaValorManT: TFloatField
      FieldName = 'ValorManT'
    end
    object QrVSGerClaTemIMEIMrt: TSmallintField
      FieldName = 'TemIMEIMrt'
    end
  end
  object DsVSGerCla: TDataSource
    DataSet = QrVSGerCla
    Left = 16
    Top = 509
  end
  object PMNumero: TPopupMenu
    Left = 100
    Top = 65532
    object PeloCdigo1: TMenuItem
      Caption = 'Pela &Gera'#231#227'o (c'#243'digo)'
      OnClick = PeloCdigo1Click
    end
    object PeloIMEC1: TMenuItem
      Caption = 'Pelo I&ME-C'
      OnClick = PeloIMEC1Click
    end
    object PeloIMEI1: TMenuItem
      Caption = 'Pelo IME-&I'
      OnClick = PeloIMEI1Click
    end
    object PelaOC1: TMenuItem
      Caption = 'Pela OC'
      OnClick = PelaOC1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 36
    Top = 12
    object Outrasimpresses1: TMenuItem
      Caption = 'Outras impress'#245'es (outra janela)'
      OnClick = Outrasimpresses1Click
    end
  end
  object QrVSPaClaCab: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPaClaCabAfterOpen
    BeforeClose = QrVSPaClaCabBeforeClose
    AfterScroll = QrVSPaClaCabAfterScroll
    OnCalcFields = QrVSPaClaCabCalcFields
    SQL.Strings = (
      'SELECT prc.* '
      'FROM vspaclacaba prc '
      'WHERE VSGerArt=5 ')
    Left = 232
    Top = 460
    object QrVSPaClaCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaClaCabVSGerArt: TIntegerField
      FieldName = 'VSGerArt'
    end
    object QrVSPaClaCabVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSPaClaCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSPaClaCabLstPal01: TIntegerField
      FieldName = 'LstPal01'
    end
    object QrVSPaClaCabLstPal02: TIntegerField
      FieldName = 'LstPal02'
    end
    object QrVSPaClaCabLstPal03: TIntegerField
      FieldName = 'LstPal03'
    end
    object QrVSPaClaCabLstPal04: TIntegerField
      FieldName = 'LstPal04'
    end
    object QrVSPaClaCabLstPal05: TIntegerField
      FieldName = 'LstPal05'
    end
    object QrVSPaClaCabLstPal06: TIntegerField
      FieldName = 'LstPal06'
    end
    object QrVSPaClaCabLstPal07: TIntegerField
      FieldName = 'LstPal07'
    end
    object QrVSPaClaCabLstPal08: TIntegerField
      FieldName = 'LstPal08'
    end
    object QrVSPaClaCabLstPal09: TIntegerField
      FieldName = 'LstPal09'
    end
    object QrVSPaClaCabLstPal10: TIntegerField
      FieldName = 'LstPal10'
    end
    object QrVSPaClaCabLstPal11: TIntegerField
      FieldName = 'LstPal11'
    end
    object QrVSPaClaCabLstPal12: TIntegerField
      FieldName = 'LstPal12'
    end
    object QrVSPaClaCabLstPal13: TIntegerField
      FieldName = 'LstPal13'
    end
    object QrVSPaClaCabLstPal14: TIntegerField
      FieldName = 'LstPal14'
    end
    object QrVSPaClaCabLstPal15: TIntegerField
      FieldName = 'LstPal15'
    end
    object QrVSPaClaCabDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPaClaCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaClaCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaClaCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaClaCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaClaCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaClaCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaClaCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaClaCabDtHrFimCla_TXT: TWideStringField
      FieldName = 'DtHrFimCla_TXT'
    end
    object QrVSPaClaCabPecas: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Pecas'
      Calculated = True
    end
    object QrVSPaClaCabAreaM2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrVSPaClaCabAreaP2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrVSPaClaCabFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
  end
  object DsVSPaClaCab: TDataSource
    DataSet = QrVSPaClaCab
    Left = 232
    Top = 509
  end
  object QrVSPaClaIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaClaItsBeforeClose
    AfterScroll = QrVSPaClaItsAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM vsparclitsa'
      'WHERE Codigo=5'
      'ORDER BY DtHrIni')
    Left = 316
    Top = 460
    object QrVSPaClaItsFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPaClaItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaClaItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPaClaItsVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSPaClaItsVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPaClaItsVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPaClaItsVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPaClaItsTecla: TIntegerField
      FieldName = 'Tecla'
    end
    object QrVSPaClaItsDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPaClaItsDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrVSPaClaItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaClaItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaClaItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaClaItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaClaItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaClaItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaClaItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaClaItsDtHrFim_TXT: TWideStringField
      FieldName = 'DtHrFim_TXT'
    end
  end
  object DsVSPaClaIts: TDataSource
    DataSet = QrVSPaClaIts
    Left = 316
    Top = 509
  end
  object PMVSPaClaIts: TPopupMenu
    Left = 256
    Top = 260
    object EncerraPallet1: TMenuItem
      Caption = '&Encerra Pallet'
      OnClick = EncerraPallet1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object IrparajaneladedadosdoIMEI1: TMenuItem
      Caption = 'Ir para janela de dados do IME-I '
      OnClick = IrparajaneladedadosdoIMEI1Click
    end
    object AlteradadosdoartigodoIMEI1: TMenuItem
      Caption = '&Altera dados do artigo do IME-I'
      OnClick = AlteradadosdoartigodoIMEI1Click
    end
  end
  object QrSum: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Aream2) AreaM2, SUM(AreaP2) AreaP2'
      'FROM vscacitsa'
      'WHERE CacCod=65'
      'AND Codigo=5')
    Left = 384
    Top = 460
    object QrSumPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object QrVMI_Sorc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT VMI_Sorc, VMI_Baix, VMI_Dest, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2'
      'FROM vscacitsa'
      'WHERE VSPaClaIts=14'
      'GROUP BY VMI_Sorc')
    Left = 456
    Top = 460
    object QrVMI_SorcVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVMI_SorcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMI_SorcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVMI_SorcVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVMI_SorcVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
  end
  object DsVMI_Sorc: TDataSource
    DataSet = QrVMI_Sorc
    Left = 456
    Top = 513
  end
  object QrVMI_Baix: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT VMI_Sorc, VMI_Baix, VMI_Dest, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2'
      'FROM vscacitsa'
      'WHERE VSPaClaIts=14'
      'GROUP BY VMI_Baix')
    Left = 536
    Top = 460
    object QrVMI_BaixVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVMI_BaixPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMI_BaixVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVMI_BaixVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVMI_BaixAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object DsVMI_Baix: TDataSource
    DataSet = QrVMI_Baix
    Left = 536
    Top = 509
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MovimID, Codigo, Controle, GraGruX '
      'FROM vsmovits'
      'WHERE Controle=5232')
    Left = 632
    Top = 464
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrVSGerArtA: TMySQLQuery
    Database = Dmod.MyDB
    Left = 360
    Top = 176
    object QrVSGerArtACodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrVCI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 404
    Top = 168
    object QrVCIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVCIAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVCIAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVCIPcBxa: TFloatField
      FieldName = 'PcBxa'
    end
  end
  object QrVMI_Orig: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVMI_OrigAfterOpen
    BeforeClose = QrVMI_OrigBeforeClose
    SQL.Strings = (
      '/* MySQL Error Code: (1054)'
      'Unknown column '#39'scl.Nome'#39' in '#39'field list'#39
      ''
      'Owner: FmVSMovIts'
      ' */'
      ''
      'SELECT  "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, '
      'CAST(vmi.Codigo AS SIGNED) Codigo, '
      'CAST(vmi.Controle AS SIGNED) Controle, '
      'CAST(vmi.MovimCod AS SIGNED) MovimCod, '
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, '
      'CAST(vmi.MovimTwn AS SIGNED) MovimTwn, '
      'CAST(vmi.Empresa AS SIGNED) Empresa, '
      'CAST(vmi.Terceiro AS SIGNED) Terceiro, '
      'CAST(vmi.CliVenda AS SIGNED) CliVenda, '
      'CAST(vmi.MovimID AS SIGNED) MovimID, '
      'CAST(vmi.DataHora AS DATETIME) DataHora, '
      'CAST(vmi.Pallet AS SIGNED) Pallet, '
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, '
      'CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas, '
      'CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg, '
      'CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2, '
      'CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2, '
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, '
      'CAST(vmi.SrcMovID AS SIGNED) SrcMovID, '
      'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1, '
      'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2, '
      'CAST(vmi.SrcGGX AS SIGNED) SrcGGX, '
      'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca, '
      'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso, '
      'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2, '
      'CAST(vmi.Observ AS CHAR) Observ, '
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, '
      'CAST(vmi.Ficha AS SIGNED) Ficha, '
      'CAST(vmi.Misturou AS UNSIGNED) Misturou, '
      'CAST(vmi.FornecMO AS SIGNED) FornecMO, '
      'CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg, '
      'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, '
      'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP, '
      'CAST(vmi.DstMovID AS SIGNED) DstMovID, '
      'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1, '
      'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2, '
      'CAST(vmi.DstGGX AS SIGNED) DstGGX, '
      'CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca, '
      'CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, '
      'CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, '
      'CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, '
      'CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca, '
      'CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso, '
      'CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2, '
      'CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2, '
      'CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG, '
      'CAST(vmi.Marca AS CHAR) Marca, '
      'CAST(vmi.PedItsLib AS SIGNED) PedItsLib, '
      'CAST(vmi.PedItsFin AS SIGNED) PedItsFin, '
      'CAST(vmi.PedItsVda AS SIGNED) PedItsVda, '
      'CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2, '
      'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq, '
      'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc, '
      'CAST(vmi.ItemNFe AS SIGNED) ItemNFe, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, '
      'vsf.Nome NO_SerieFch, '
      'vsp.Nome NO_Pallet, '
      'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, '
      'CAST(ggx.GraGru1 AS SIGNED) GraGru1,'
      'vmi.Marca,'
      'CAST(vmi.LnkNivXtr1 AS SIGNED) LnkNivXtr1,'
      'CAST(vmi.LnkNivXtr2 AS SIGNED) LnkNivXtr2'
      ''
      ''
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      ''
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro'
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc '
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo '
      ''
      'WHERE vmi.Controle > 0'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'AND vmi.Controle=27493')
    Left = 120
    Top = 449
    object QrVMI_OrigCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVMI_OrigControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVMI_OrigMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVMI_OrigMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVMI_OrigMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVMI_OrigEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVMI_OrigTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVMI_OrigCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVMI_OrigMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVMI_OrigDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVMI_OrigPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVMI_OrigGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVMI_OrigPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMI_OrigPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMI_OrigAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVMI_OrigSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVMI_OrigSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVMI_OrigSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVMI_OrigSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMI_OrigSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMI_OrigSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVMI_OrigSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVMI_OrigFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVMI_OrigMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVMI_OrigFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVMI_OrigCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVMI_OrigDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVMI_OrigDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVMI_OrigDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVMI_OrigQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMI_OrigQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMI_OrigQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMI_OrigQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMI_OrigQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMI_OrigNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVMI_OrigNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Origin = 'vspalleta.Nome'
      Size = 60
    end
    object QrVMI_OrigNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVMI_OrigNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVMI_OrigID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVMI_OrigNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVMI_OrigNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Origin = 'vsserfch.Nome'
      Size = 60
    end
    object QrVMI_OrigReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVMI_OrigNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrVMI_OrigNO_DstMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DstMovID'
      Size = 255
      Calculated = True
    end
    object QrVMI_OrigNO_SrcMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_SrcMovID'
      Size = 255
      Calculated = True
    end
    object QrVMI_OrigNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 103
    end
    object QrVMI_OrigGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrVMI_OrigMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVMI_OrigPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrVMI_OrigPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrVMI_OrigPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrVMI_OrigStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrVMI_OrigItemNFe: TLargeintField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrVMI_OrigMarca_1: TWideStringField
      FieldName = 'Marca_1'
    end
    object QrVMI_OrigLnkNivXtr1: TLargeintField
      FieldName = 'LnkNivXtr1'
      Required = True
    end
    object QrVMI_OrigLnkNivXtr2: TLargeintField
      FieldName = 'LnkNivXtr2'
      Required = True
    end
  end
  object DsVMI_Orig: TDataSource
    DataSet = QrVMI_Orig
    Left = 120
    Top = 497
  end
  object QrVSPaMulCab: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPaMulCabAfterOpen
    BeforeClose = QrVSPaMulCabBeforeClose
    AfterScroll = QrVSPaMulCabAfterScroll
    SQL.Strings = (
      'SELECT pra.*,'
      'cn1.FatorInt  '
      'FROM vspamulcaba pra'
      'LEFT JOIN vsmovits vmi ON vmi.Controle=pra.VMI_Sorc'
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX'
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 '
      'WHERE pra.VSGerArt=2'
      'ORDER BY pra.Codigo ')
    Left = 232
    Top = 564
    object QrVSPaMulCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaMulCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSPaMulCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSPaMulCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPaMulCabVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPaMulCabDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSPaMulCabPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSPaMulCabPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSPaMulCabAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSPaMulCabAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSPaMulCabValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSPaMulCabVSGerArt: TIntegerField
      FieldName = 'VSGerArt'
    end
    object QrVSPaMulCabPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSPaMulCabTemIMEIMrt: TSmallintField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSPaMulCabSerieRem: TSmallintField
      FieldName = 'SerieRem'
    end
    object QrVSPaMulCabNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrVSPaMulCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaMulCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaMulCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaMulCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaMulCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaMulCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaMulCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaMulCabFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
  end
  object DsVSPaMulCab: TDataSource
    DataSet = QrVSPaMulCab
    Left = 232
    Top = 612
  end
  object QrVSPaMulIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 312
    Top = 557
    object QrVSPaMulItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPaMulItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPaMulItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPaMulItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPaMulItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPaMulItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPaMulItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPaMulItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPaMulItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPaMulItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrVSPaMulItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPaMulItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPaMulItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPaMulItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPaMulItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPaMulItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPaMulItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPaMulItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPaMulItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPaMulItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPaMulItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPaMulItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPaMulItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPaMulItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPaMulItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPaMulItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPaMulItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPaMulItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPaMulItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPaMulItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPaMulItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPaMulItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPaMulItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPaMulItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPaMulItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPaMulItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPaMulItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSPaMulItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object DsVSPaMulIts: TDataSource
    DataSet = QrVSPaMulIts
    Left = 312
    Top = 601
  end
end
