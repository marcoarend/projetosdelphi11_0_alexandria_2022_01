unit VSArtGGX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBGridZTO, mySQLDbTables;

type
  TFmVSArtGGX = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    Label1: TLabel;
    EdPesq: TdmkEdit;
    DBGReduzidos: TdmkDBGridZTO;
    QrReduzidos: TmySQLQuery;
    DsReduzidos: TDataSource;
    QrReduzidosControle: TIntegerField;
    QrReduzidosNO_PRD_TAM_COR: TWideStringField;
    QrVSArtGGX: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FControle: Integer;
    //
    procedure ReopenReduzidos();
  end;

  var
  FmVSArtGGX: TFmVSArtGGX;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmVSArtGGX.BtOKClick(Sender: TObject);
const
  SQLType = stIns;
var
  N, I, GraGruX: Integer;
begin
  if MyObjects.FIC(DBGReduzidos.SelectedRows.Count < 1, nil,
  'Nenhum reduzido foi selecionado!') then Exit;
  //
  QrReduzidos.DisableControls;
  try
    N := 0;
    with DBGReduzidos.DataSource.DataSet do
    for I := 0 to DBGReduzidos.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGReduzidos.SelectedRows.Items[I]));
      GotoBookmark(DBGReduzidos.SelectedRows.Items[I]);
      GraGruX := QrReduzidosControle.Value;
      //
      UnDMkDAC_PF.AbreMySQLQuery0(QrVSArtGGX, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM vsartggx ',
      'WHERE Codigo=' + Geral.FF0(FCodigo),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if QrVSArtGGX.FieldByName('GraGruX').AsInteger = GraGruX then
      begin
        //N := N + 1;
        Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
        ' j� estava adicionado � configura��o de artigos!');
      end else
      begin
        FControle := UMyMod.BPGS1I32('vsartggx', 'Controle', '', '', tsPos, SQLType, 0);
        //if
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsartggx', False, [
        'Codigo', 'GraGruX'], [
        'Controle'], [
        FCodigo, GraGruX], [
        FControle], True);
      end;
    end;
  finally
    QrReduzidos.EnableControls;
  end;
  //Result := N;
  Close;
end;

procedure TFmVSArtGGX.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSArtGGX.EdPesqChange(Sender: TObject);
begin
  ReopenReduzidos();
end;

procedure TFmVSArtGGX.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSArtGGX.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSArtGGX.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSArtGGX.ReopenReduzidos();
var
  SQL_Texto: String;
begin
  if Length(EdPesq.Text) > 1 then
    SQL_Texto := Geral.ATS([
      'AND CONCAT(gg1.Nome,  ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
      'LIKE "%' + EdPesq.Text + '%"  '])
  else
    SQL_Texto := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrReduzidos, Dmod.MyDB, [
  'SELECT ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
  'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
  'WHERE ggx.Controle>0',
  'AND pgt.Tipo_Item IN (3,4,6)',
  SQL_Texto,
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
end;

end.
