object FmVSDsnCab: TFmVSDsnCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-049 :: Gerenciamento de Desnate'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 681
      Top = 137
      Height = 364
      ExplicitLeft = 572
      ExplicitTop = 149
    end
    object Splitter2: TSplitter
      Left = 357
      Top = 137
      Height = 364
      ExplicitLeft = 444
      ExplicitTop = 125
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 137
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label5: TLabel
        Left = 16
        Top = 56
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label6: TLabel
        Left = 16
        Top = 96
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 584
        Top = 56
        Width = 39
        Height = 13
        Caption = 'Revisor:'
      end
      object Label11: TLabel
        Left = 692
        Top = 16
        Width = 87
        Height = 13
        Caption = 'Data / hora in'#237'cio:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 844
        Top = 16
        Width = 95
        Height = 13
        Caption = 'Data / hora t'#233'rmino:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 892
        Top = 96
        Width = 98
        Height = 13
        Caption = 'Base para nota zero:'
      end
      object Label14: TLabel
        Left = 388
        Top = 76
        Width = 33
        Height = 13
        Caption = 'DtHrIni'
        FocusControl = DBEdit3
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSDsnCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSDsnCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 557
        Height = 21
        DataField = 'NO_EMP'
        DataSource = DsVSDsnCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 692
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtHrIni'
        DataSource = DsVSDsnCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 844
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtHrFim'
        DataSource = DsVSDsnCab
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 72
        Top = 72
        Width = 505
        Height = 21
        DataField = 'NO_CLI'
        DataSource = DsVSDsnCab
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsVSDsnCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 584
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Desnatador'
        DataSource = DsVSDsnCab
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 640
        Top = 72
        Width = 353
        Height = 21
        DataField = 'NO_REV'
        DataSource = DsVSDsnCab
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 16
        Top = 112
        Width = 873
        Height = 21
        DataField = 'Nome'
        DataSource = DsVSDsnCab
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 892
        Top = 112
        Width = 100
        Height = 21
        DataField = 'BasNotZero'
        DataSource = DsVSDsnCab
        TabOrder = 10
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 352
        Top = 15
        Width = 654
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 521
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtArt: TBitBtn
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Artigo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtArtClick
        end
        object BtIts: TBitBtn
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pallet'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtItsClick
        end
        object BtSub: TBitBtn
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sub classe'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtSubClick
        end
      end
    end
    object GBArt: TGroupBox
      Left = 0
      Top = 137
      Width = 357
      Height = 364
      Align = alLeft
      Caption = ' Artigos: '
      TabOrder = 2
      object DGDados: TDBGrid
        Left = 2
        Top = 15
        Width = 353
        Height = 347
        Align = alClient
        DataSource = DsVSDsnArt
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BasNota'
            Title.Caption = 'Nota base'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Interesse'
            Title.Caption = 'Leva?'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Nome Artigo'
            Width = 180
            Visible = True
          end>
      end
    end
    object GBIts: TGroupBox
      Left = 740
      Top = 137
      Width = 268
      Height = 364
      Align = alRight
      Caption = 'Pallets (OCs) de origem: '
      TabOrder = 3
      object DBGrid1: TDBGrid
        Left = 2
        Top = 15
        Width = 264
        Height = 347
        Align = alClient
        DataSource = DsVSDsnIts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'VSPallet'
            Title.Caption = 'Pallet'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Nome artigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VSCacCod'
            Title.Caption = 'OC'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MovimCod'
            Title.Caption = 'IME-C'
            Visible = True
          end>
      end
    end
    object GroupBox1: TGroupBox
      Left = 360
      Top = 137
      Width = 321
      Height = 364
      Align = alLeft
      Caption = ' Sub classes: '
      TabOrder = 4
      object DBGrid2: TDBGrid
        Left = 2
        Top = 15
        Width = 317
        Height = 347
        Align = alClient
        DataSource = DsVSDsnSub
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SubClass'
            Title.Caption = 'Sigla'
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Width = 180
            Visible = True
          end>
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 293
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label52: TLabel
        Left = 692
        Top = 16
        Width = 87
        Height = 13
        Caption = 'Data / hora in'#237'cio:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 844
        Top = 16
        Width = 95
        Height = 13
        Caption = 'Data / hora t'#233'rmino:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 96
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label20: TLabel
        Left = 16
        Top = 56
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object SbCliente: TSpeedButton
        Left = 560
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbClienteClick
      end
      object Label3: TLabel
        Left = 584
        Top = 56
        Width = 39
        Height = 13
        Caption = 'Revisor:'
      end
      object SBDesnatador: TSpeedButton
        Left = 972
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBDesnatadorClick
      end
      object Label4: TLabel
        Left = 892
        Top = 96
        Width = 98
        Height = 13
        Caption = 'Base para nota zero:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 557
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPDtHrIni: TdmkEditDateTimePicker
        Left = 692
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrIni: TdmkEdit
        Left = 800
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtHrFim: TdmkEditDateTimePicker
        Left = 844
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrFim: TdmkEdit
        Left = 952
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 112
        Width = 873
        Height = 21
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 489
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientes
        TabOrder = 8
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDesnatador: TdmkEditCB
        Left = 584
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Desnatador'
        UpdCampo = 'ClienteMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBDesnatador
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBDesnatador: TdmkDBLookupComboBox
        Left = 640
        Top = 72
        Width = 333
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDesnatadores
        TabOrder = 10
        dmkEditCB = EdDesnatador
        QryCampo = 'Desnatador'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdBasNotZero: TdmkEdit
        Left = 892
        Top = 112
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'BasNotZero'
        UpdCampo = 'BasNotZero'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 333
        Height = 32
        Caption = 'Gerenciamento de Desnate'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 333
        Height = 32
        Caption = 'Gerenciamento de Desnate'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 333
        Height = 32
        Caption = 'Gerenciamento de Desnate'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSDsnCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSDsnCabBeforeOpen
    AfterOpen = QrVSDsnCabAfterOpen
    BeforeClose = QrVSDsnCabBeforeClose
    AfterScroll = QrVSDsnCabAfterScroll
    SQL.Strings = (
      'SELECT vdc.*,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, '
      'IF(rev.Tipo=0, rev.RazaoSocial, rev.Nome) NO_REV'
      'FROM vsdsncab vdc'
      'LEFT JOIN entidades emp ON emp.Codigo=vdc.Empresa'
      'LEFT JOIN entidades cli ON cli.Codigo=vdc.Cliente'
      'LEFT JOIN entidades rev ON rev.Codigo=vdc.Desnatador'
      '')
    Left = 48
    Top = 233
    object QrVSDsnCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSDsnCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSDsnCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSDsnCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSDsnCabDesnatador: TIntegerField
      FieldName = 'Desnatador'
    end
    object QrVSDsnCabDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrVSDsnCabDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrVSDsnCabBasNotZero: TFloatField
      FieldName = 'BasNotZero'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSDsnCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSDsnCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSDsnCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSDsnCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSDsnCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSDsnCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSDsnCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSDsnCabNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrVSDsnCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrVSDsnCabNO_REV: TWideStringField
      FieldName = 'NO_REV'
      Size = 100
    end
  end
  object DsVSDsnCab: TDataSource
    DataSet = QrVSDsnCab
    Left = 48
    Top = 281
  end
  object QrVSDsnArt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dsa.*, '
      'CONCAT(gg1.Nome,    '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR '
      'FROM vsdsnart dsa '
      'LEFT JOIN gragrux ggx ON ggx.Controle=dsa.GraGruX    '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ')
    Left = 140
    Top = 237
    object QrVSDsnArtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSDsnArtControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSDsnArtGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSDsnArtBasNota: TFloatField
      FieldName = 'BasNota'
    end
    object QrVSDsnArtInteresse: TSmallintField
      FieldName = 'Interesse'
    end
    object QrVSDsnArtLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSDsnArtDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSDsnArtDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSDsnArtUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSDsnArtUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSDsnArtAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSDsnArtAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSDsnArtNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSDsnArtNO_Interesse: TWideStringField
      FieldName = 'NO_Interesse'
      Size = 3
    end
  end
  object DsVSDsnArt: TDataSource
    DataSet = QrVSDsnArt
    Left = 140
    Top = 285
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      object Palletdeorigem1: TMenuItem
        Caption = 'Pallet de origem'
        OnClick = Palletdeorigem1Click
      end
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrVSDsnIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dsi.*, ger.GraGruX, ger.MovimCod, ger.VSPallet,'
      'CONCAT(gg1.Nome,    '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR  '
      'FROM vsdsnits dsi'
      'LEFT JOIN vsgerrcla ger ON ger.CacCod=dsi.VSCacCod'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ger.GraGruX    '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ')
    Left = 228
    Top = 237
    object QrVSDsnItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSDsnItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSDsnItsVSCacCod: TIntegerField
      FieldName = 'VSCacCod'
    end
    object QrVSDsnItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSDsnItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSDsnItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSDsnItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSDsnItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSDsnItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSDsnItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSDsnItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSDsnItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSDsnItsVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSDsnItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsVSDsnIts: TDataSource
    DataSet = QrVSDsnIts
    Left = 228
    Top = 285
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 312
    Top = 240
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 312
    Top = 288
  end
  object QrDesnatadores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'OR ent.Terceiro="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 388
    Top = 240
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsDesnatadores: TDataSource
    DataSet = QrDesnatadores
    Left = 388
    Top = 288
  end
  object PMArt: TPopupMenu
    OnPopup = PMArtPopup
    Left = 364
    Top = 376
    object AdicionaArtigo1: TMenuItem
      Caption = '&Adiciona Artigo'
      Enabled = False
      OnClick = AdicionaArtigo1Click
    end
    object EditaArtigo1: TMenuItem
      Caption = '&Edita Artigo'
      Enabled = False
      OnClick = EditaArtigo1Click
    end
    object RemoveArtigo1: TMenuItem
      Caption = '&Remove Artigo'
      Enabled = False
      OnClick = RemoveArtigo1Click
    end
  end
  object QrVSDsnSub: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vds.*,'
      'CONCAT(gg1.Nome,    '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR '
      'FROM vsdsnsub vds'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vds.GraGruX    '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      '')
    Left = 108
    Top = 468
    object QrVSDsnSubCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSDsnSubControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSDsnSubSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrVSDsnSubGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSDsnSubBasNota: TFloatField
      FieldName = 'BasNota'
    end
    object QrVSDsnSubInteresse: TSmallintField
      FieldName = 'Interesse'
    end
    object QrVSDsnSubLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSDsnSubDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSDsnSubDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSDsnSubUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSDsnSubUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSDsnSubAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSDsnSubAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSDsnSubNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsVSDsnSub: TDataSource
    DataSet = QrVSDsnSub
    Left = 108
    Top = 516
  end
  object PMSub: TPopupMenu
    OnPopup = PMSubPopup
    Left = 476
    Top = 376
    object AdicionaSigla1: TMenuItem
      Caption = '&Adiciona info de Sigla'
      OnClick = AdicionaSigla1Click
    end
    object EditaSigla1: TMenuItem
      Caption = '&Edita info de Sigla'
      OnClick = EditaSigla1Click
    end
    object RemoveSigla1: TMenuItem
      Caption = '&Remove info Sigla'
      OnClick = RemoveSigla1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 252
    Top = 120
    object Sinttico1: TMenuItem
      Caption = '&Sint'#233'tico'
      OnClick = Sinttico1Click
    end
    object Analtico1: TMenuItem
      Caption = '&Anal'#237'tico'
      object Classeespecfica1: TMenuItem
        Caption = 'Classe &Espec'#237'fica'
        OnClick = Classeespecfica1Click
      end
      object odasClasses1: TMenuItem
        Caption = '&Todas Classes'
        OnClick = odasClasses1Click
      end
    end
  end
end
