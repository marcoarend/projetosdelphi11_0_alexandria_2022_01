unit VSDsnCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB;

type
  TFmVSDsnCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    DBEdCodigo: TdmkDBEdit;
    QrVSDsnCab: TmySQLQuery;
    DsVSDsnCab: TDataSource;
    QrVSDsnArt: TmySQLQuery;
    DsVSDsnArt: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtArt: TBitBtn;
    QrVSDsnIts: TmySQLQuery;
    DsVSDsnIts: TDataSource;
    QrVSDsnCabCodigo: TIntegerField;
    QrVSDsnCabNome: TWideStringField;
    QrVSDsnCabEmpresa: TIntegerField;
    QrVSDsnCabCliente: TIntegerField;
    QrVSDsnCabDesnatador: TIntegerField;
    QrVSDsnCabDtHrIni: TDateTimeField;
    QrVSDsnCabDtHrFim: TDateTimeField;
    QrVSDsnCabBasNotZero: TFloatField;
    QrVSDsnCabLk: TIntegerField;
    QrVSDsnCabDataCad: TDateField;
    QrVSDsnCabDataAlt: TDateField;
    QrVSDsnCabUserCad: TIntegerField;
    QrVSDsnCabUserAlt: TIntegerField;
    QrVSDsnCabAlterWeb: TSmallintField;
    QrVSDsnCabAtivo: TSmallintField;
    QrVSDsnCabNO_EMP: TWideStringField;
    QrVSDsnCabNO_CLI: TWideStringField;
    QrVSDsnCabNO_REV: TWideStringField;
    QrVSDsnArtCodigo: TIntegerField;
    QrVSDsnArtControle: TIntegerField;
    QrVSDsnArtGraGruX: TIntegerField;
    QrVSDsnArtBasNota: TFloatField;
    QrVSDsnArtInteresse: TSmallintField;
    QrVSDsnArtLk: TIntegerField;
    QrVSDsnArtDataCad: TDateField;
    QrVSDsnArtDataAlt: TDateField;
    QrVSDsnArtUserCad: TIntegerField;
    QrVSDsnArtUserAlt: TIntegerField;
    QrVSDsnArtAlterWeb: TSmallintField;
    QrVSDsnArtAtivo: TSmallintField;
    QrVSDsnArtNO_PRD_TAM_COR: TWideStringField;
    QrVSDsnItsCodigo: TIntegerField;
    QrVSDsnItsControle: TIntegerField;
    QrVSDsnItsVSCacCod: TIntegerField;
    QrVSDsnItsLk: TIntegerField;
    QrVSDsnItsDataCad: TDateField;
    QrVSDsnItsDataAlt: TDateField;
    QrVSDsnItsUserCad: TIntegerField;
    QrVSDsnItsUserAlt: TIntegerField;
    QrVSDsnItsAlterWeb: TSmallintField;
    QrVSDsnItsAtivo: TSmallintField;
    QrVSDsnItsGraGruX: TIntegerField;
    QrVSDsnItsMovimCod: TIntegerField;
    QrVSDsnItsVSPallet: TIntegerField;
    QrVSDsnItsNO_PRD_TAM_COR: TWideStringField;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label52: TLabel;
    TPDtHrIni: TdmkEditDateTimePicker;
    EdDtHrIni: TdmkEdit;
    Label53: TLabel;
    TPDtHrFim: TdmkEditDateTimePicker;
    EdDtHrFim: TdmkEdit;
    EdNome: TdmkEdit;
    Label9: TLabel;
    QrClientes: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientes: TDataSource;
    Label20: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    SbCliente: TSpeedButton;
    EdDesnatador: TdmkEditCB;
    Label3: TLabel;
    CBDesnatador: TdmkDBLookupComboBox;
    SBDesnatador: TSpeedButton;
    QrDesnatadores: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsDesnatadores: TDataSource;
    Label4: TLabel;
    EdBasNotZero: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label14: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    GBArt: TGroupBox;
    DGDados: TDBGrid;
    GBIts: TGroupBox;
    DBGrid1: TDBGrid;
    PMArt: TPopupMenu;
    AdicionaArtigo1: TMenuItem;
    EditaArtigo1: TMenuItem;
    RemoveArtigo1: TMenuItem;
    BtIts: TBitBtn;
    QrVSDsnArtNO_Interesse: TWideStringField;
    Palletdeorigem1: TMenuItem;
    GroupBox1: TGroupBox;
    DBGrid2: TDBGrid;
    QrVSDsnSub: TmySQLQuery;
    DsVSDsnSub: TDataSource;
    QrVSDsnSubCodigo: TIntegerField;
    QrVSDsnSubControle: TIntegerField;
    QrVSDsnSubSubClass: TWideStringField;
    QrVSDsnSubGraGruX: TIntegerField;
    QrVSDsnSubBasNota: TFloatField;
    QrVSDsnSubLk: TIntegerField;
    QrVSDsnSubDataCad: TDateField;
    QrVSDsnSubDataAlt: TDateField;
    QrVSDsnSubUserCad: TIntegerField;
    QrVSDsnSubUserAlt: TIntegerField;
    QrVSDsnSubAlterWeb: TSmallintField;
    QrVSDsnSubAtivo: TSmallintField;
    QrVSDsnSubNO_PRD_TAM_COR: TWideStringField;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    BtSub: TBitBtn;
    PMSub: TPopupMenu;
    AdicionaSigla1: TMenuItem;
    EditaSigla1: TMenuItem;
    RemoveSigla1: TMenuItem;
    QrVSDsnSubInteresse: TSmallintField;
    PMImprime: TPopupMenu;
    Sinttico1: TMenuItem;
    Analtico1: TMenuItem;
    Classeespecfica1: TMenuItem;
    odasClasses1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSDsnCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSDsnCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSDsnCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSDsnCabBeforeClose(DataSet: TDataSet);
    procedure SbClienteClick(Sender: TObject);
    procedure SBDesnatadorClick(Sender: TObject);
    procedure AdicionaArtigo1Click(Sender: TObject);
    procedure EditaArtigo1Click(Sender: TObject);
    procedure RemoveArtigo1Click(Sender: TObject);
    procedure PMArtPopup(Sender: TObject);
    procedure BtArtClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Palletdeorigem1Click(Sender: TObject);
    procedure BtSubClick(Sender: TObject);
    procedure PMSubPopup(Sender: TObject);
    procedure AdicionaSigla1Click(Sender: TObject);
    procedure EditaSigla1Click(Sender: TObject);
    procedure RemoveSigla1Click(Sender: TObject);
    procedure Sinttico1Click(Sender: TObject);
    procedure odasClasses1Click(Sender: TObject);
    procedure Classeespecfica1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSDsnArt(SQLType: TSQLType);
    procedure MostraFormVSDsnIts(SQLType: TSQLType);
    procedure MostraFormVSDsnSub(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure ReopenVSDsnArt(Controle: Integer);
    procedure ReopenVSDsnIts(Controle: Integer);
    procedure ReopenVSDsnSub(Controle: Integer);

  end;

var
  FmVSDsnCab: TFmVSDsnCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSDsnArt, VSDsnIts, VSDsnSub,
  ModuleGeral, UnVS_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSDsnCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSDsnCab.MostraFormVSDsnArt(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSDsnArt, FmVSDsnArt, afmoNegarComAviso) then
  begin
    FmVSDsnArt.ImgTipo.SQLType := SQLType;
    FmVSDsnArt.FQrCab := QrVSDsnCab;
    FmVSDsnArt.FDsCab := DsVSDsnCab;
    FmVSDsnArt.FQrIts := QrVSDsnArt;
    if SQLType = stIns then
      //
    else
    begin
      FmVSDsnArt.EdControle.ValueVariant := QrVSDsnArtControle.Value;
      //
      FmVSDsnArt.EdBasNota.ValueVariant := QrVSDsnArtBasNota.Value;
      FmVSDsnArt.RGInteresse.ItemIndex  := QrVSDsnArtInteresse.Value;
      FmVSDsnArt.EdGraGruX.ValueVariant := QrVSDsnArtGraGruX.Value;
      FmVSDsnArt.CBGraGruX.KeyValue     := QrVSDsnArtGraGruX.Value;
      //
    end;
    FmVSDsnArt.ShowModal;
    FmVSDsnArt.Destroy;
  end;
end;

procedure TFmVSDsnCab.MostraFormVSDsnIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSDsnIts, FmVSDsnIts, afmoNegarComAviso) then
  begin
    FmVSDsnIts.ImgTipo.SQLType := SQLType;
    FmVSDsnIts.FQrCab := QrVSDsnCab;
    FmVSDsnIts.FDsCab := DsVSDsnCab;
    FmVSDsnIts.FQrIts := QrVSDsnIts;
    if SQLType = stIns then
      //
    else
    begin
      FmVSDsnIts.EdControle.ValueVariant := QrVSDsnItsControle.Value;
      //
      FmVSDsnIts.EdVSCacCod.ValueVariant := QrVSDsnItsVSCacCod.Value;
      FmVSDsnIts.CBVSCacCod.KeyValue     := QrVSDsnItsVSCacCod.Value;
    end;
    FmVSDsnIts.ShowModal;
    FmVSDsnIts.Destroy;
  end;
end;

procedure TFmVSDsnCab.MostraFormVSDsnSub(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSDsnSub, FmVSDsnSub, afmoNegarComAviso) then
  begin
    FmVSDsnSub.ImgTipo.SQLType := SQLType;
    FmVSDsnSub.FQrCab := QrVSDsnCab;
    FmVSDsnSub.FDsCab := DsVSDsnCab;
    FmVSDsnSub.FQrIts := QrVSDsnSub;
    if SQLType = stIns then
      //
    else
    begin
      FmVSDsnSub.EdControle.ValueVariant := QrVSDsnSubControle.Value;
      //
      FmVSDsnSub.EdBasNota.ValueVariant := QrVSDsnSubBasNota.Value;
      FmVSDsnSub.RGInteresse.ItemIndex  := QrVSDsnSubInteresse.Value;
      FmVSDsnSub.EdGraGruX.ValueVariant := QrVSDsnSubGraGruX.Value;
      FmVSDsnSub.CBGraGruX.KeyValue     := QrVSDsnSubGraGruX.Value;
      //
    end;
    FmVSDsnSub.ShowModal;
    FmVSDsnSub.Destroy;
  end;
end;

procedure TFmVSDsnCab.odasClasses1Click(Sender: TObject);
begin
  VS_PF.ImprimeReclassDesnate(QrVSDsnCabCodigo.Value, impdsnDenSubAll);
end;

procedure TFmVSDsnCab.Palletdeorigem1Click(Sender: TObject);
begin
  MostraFormVSDsnIts(stIns);
end;

procedure TFmVSDsnCab.PMArtPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(AdicionaArtigo1, QrVSDsnCab);
  MyObjects.HabilitaMenuItemItsUpd(EditaArtigo1,    QrVSDsnArt);
  MyObjects.HabilitaMenuItemItsDel(RemoveArtigo1,   QrVSDsnArt);
end;

procedure TFmVSDsnCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSDsnCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSDsnCab, QrVSDsnIts);
end;

procedure TFmVSDsnCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSDsnCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSDsnIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSDsnIts);
end;

procedure TFmVSDsnCab.PMSubPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(AdicionaSigla1, QrVSDsnCab);
  MyObjects.HabilitaMenuItemItsUpd(EditaSigla1,    QrVSDsnSub);
  MyObjects.HabilitaMenuItemItsDel(RemoveSigla1,   QrVSDsnSub);
end;

procedure TFmVSDsnCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSDsnCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSDsnCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsdsncab';
  VAR_GOTOMYSQLTABLE := QrVSDsnCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vdc.*,');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, ');
  VAR_SQLx.Add('IF(rev.Tipo=0, rev.RazaoSocial, rev.Nome) NO_REV');
  VAR_SQLx.Add('FROM vsdsncab vdc');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=vdc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=vdc.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades rev ON rev.Codigo=vdc.Desnatador');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE vdc.Codigo > 0');
  //
  VAR_SQL1.Add('AND vdc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND vdc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vdc.Nome Like :P0');
  //
end;

procedure TFmVSDsnCab.EditaArtigo1Click(Sender: TObject);
begin
  MostraFormVSDsnArt(stIns);
end;

procedure TFmVSDsnCab.EditaSigla1Click(Sender: TObject);
begin
  MostraFormVSDsnSub(stUpd);
end;

procedure TFmVSDsnCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSDsnIts(stUpd);
end;

procedure TFmVSDsnCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSDsnCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSDsnCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSDsnCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI('Confirma a do pallet (OC) selecionado?',
  'vsdsnits', 'Controle', QrVSDsnItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSDsnIts,
      QrVSDsnItsControle, QrVSDsnItsControle.Value);
    ReopenVSDsnIts(Controle);
  end;
end;

procedure TFmVSDsnCab.RemoveArtigo1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'vsdsnart', 'Controle', QrVSDsnArtControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSDsnArt,
      QrVSDsnArtControle, QrVSDsnArtControle.Value);
    ReopenVSDsnArt(Controle);
  end;
end;

procedure TFmVSDsnCab.RemoveSigla1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'vsdsnsub', 'Controle', QrVSDsnsubControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSDsnsub,
      QrVSDsnsubControle, QrVSDsnsubControle.Value);
    ReopenVSDsnSub(Controle);
  end;
end;

procedure TFmVSDsnCab.ReopenVSDsnArt(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSDsnArt, Dmod.MyDB, [
  'SELECT dsa.*, ELT(dsa.Interesse+1, "Sim", "N�o", "???") NO_Interesse, ',
  'CONCAT(gg1.Nome,    ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR ',
  'FROM vsdsnart dsa ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=dsa.GraGruX    ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'WHERE dsa.Codigo=' + Geral.FF0(QrVSDsnCabCodigo.Value),
  '']);
  //
  QrVSDsnArt.Locate('Controle', Controle, []);
end;


procedure TFmVSDsnCab.ReopenVSDsnIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSDsnIts, Dmod.MyDB, [
  'SELECT dsi.*, ger.GraGruX, ger.MovimCod, ger.VSPallet,  ',
  'CONCAT(gg1.Nome,      ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR    ',
  'FROM vsdsnits dsi  ',
  'LEFT JOIN vsgerrcla ger ON ger.CacCod=dsi.VSCacCod  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ger.GraGruX      ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC      ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad      ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI      ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'WHERE dsi.Codigo=' + Geral.FF0(QrVSDsnCabCodigo.Value),
  '']);
  //
  QrVSDsnIts.Locate('Controle', Controle, []);
end;

procedure TFmVSDsnCab.ReopenVSDsnSub(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSDsnSub, Dmod.MyDB, [
  'SELECT vds.*, ',
  'CONCAT(gg1.Nome,     ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR  ',
  'FROM vsdsnsub vds ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vds.GraGruX     ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC     ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad     ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI     ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'WHERE vds.Codigo=' + Geral.FF0(QrVSDsnCabCodigo.Value),
  ' ']);
end;

procedure TFmVSDsnCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSDsnCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSDsnCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSDsnCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSDsnCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSDsnCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSDsnCab.BtSubClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSub, BtSub);
end;

procedure TFmVSDsnCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSDsnCabCodigo.Value;
  Close;
end;

procedure TFmVSDsnCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSDsnCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsdsncab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSDsnCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSDsnCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtHrIni, DtHrFim: String;
  Codigo, Empresa, Cliente, Desnatador: Integer;
  BasNotZero: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Cliente        := EdCliente.ValueVariant;
  Desnatador     := EdDesnatador.ValueVariant;
  DtHrIni        := Geral.FDT_TP_Ed(TPDtHrIni.Date, EdDtHrIni.Text);
  DtHrFim        := Geral.FDT_TP_Ed(TPDtHrFim.Date, EdDtHrFim.Text);
  BasNotZero     := EdBasNotZero.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a empresa!') then
    Exit;
  //
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Defina um cliente!') then
    Exit;
  //if MyObjects.FIC(BasNotZero = 0, EdBasNotZero, 'Defina uma nota base zero!') then
    //Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsdsncab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsdsncab', False, [
  'Nome', 'Empresa', 'Cliente',
  'Desnatador', 'DtHrIni', 'DtHrFim',
  'BasNotZero'], [
  'Codigo'], [
  Nome, Empresa, Cliente,
  Desnatador, DtHrIni, DtHrFim,
  BasNotZero], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSDsnCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsdsncab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsdsncab', 'Codigo');
end;

procedure TFmVSDsnCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSDsnCab.AdicionaArtigo1Click(Sender: TObject);
begin
  MostraFormVSDsnArt(stIns);
end;

procedure TFmVSDsnCab.AdicionaSigla1Click(Sender: TObject);
begin
  MostraFormVSDsnSub(stIns);
end;

procedure TFmVSDsnCab.BtArtClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArt, BtArt);
end;

procedure TFmVSDsnCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSDsnCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  GBIts.Align   := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDesnatadores, Dmod.MyDB);
end;

procedure TFmVSDsnCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSDsnCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSDsnCab.SbClienteClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdCliente.Text := IntToStr(VAR_ENTIDADE);
    CBCliente.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSDsnCab.SBDesnatadorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrDesnatadores, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdDesnatador.Text := IntToStr(VAR_ENTIDADE);
    CBDesnatador.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSDsnCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(PMImprime, SbImprime);
end;

procedure TFmVSDsnCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSDsnCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSDsnCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSDsnCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSDsnCab.QrVSDsnCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSDsnCab.QrVSDsnCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSDsnArt(0);
  ReopenVSDsnIts(0);
  ReopenVSDsnSub(0);
end;

procedure TFmVSDsnCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSDsnCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSDsnCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSDsnCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsdsncab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSDsnCab.Sinttico1Click(Sender: TObject);
begin
  VS_PF.ImprimeReclassDesnate(QrVSDsnCabCodigo.Value, impdsnDenate);
end;

procedure TFmVSDsnCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSDsnCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSDsnCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsdsncab');
end;

procedure TFmVSDsnCab.Classeespecfica1Click(Sender: TObject);
begin
  VS_PF.ImprimeReclassDesnate(QrVSDsnCabCodigo.Value, impdsnDenSubUni);
end;

procedure TFmVSDsnCab.QrVSDsnCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSDsnArt.Close;
  QrVSDsnIts.Close;
  QrVSDsnSub.Close;
end;

procedure TFmVSDsnCab.QrVSDsnCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSDsnCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

