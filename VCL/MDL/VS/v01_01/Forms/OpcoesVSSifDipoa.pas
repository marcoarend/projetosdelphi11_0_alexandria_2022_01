unit OpcoesVSSifDipoa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkRadioGroup;

type
  TFmOpcoesVSSifDipoa = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrVSSifDipoa: TMySQLQuery;
    DsVSSifDipoa: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    QrParamsEmp: TMySQLQuery;
    DsParamsEmp: TDataSource;
    Panel5: TPanel;
    DBGParamsEmp: TDBGrid;
    Label1: TLabel;
    QrParamsEmpCodigo: TIntegerField;
    QrParamsEmpNO_EMP: TWideStringField;
    QrVSSifDipoaEmpresa: TIntegerField;
    QrVSSifDipoaRegistro: TWideStringField;
    QrVSSifDipoaDdValid: TIntegerField;
    QrVSSifDipoaArqLogoFilial: TWideStringField;
    QrVSSifDipoaArqLogoSIF: TWideStringField;
    Label2: TLabel;
    EdRegistro: TdmkEdit;
    Label3: TLabel;
    EdDdValid: TdmkEdit;
    Label4: TLabel;
    EdArqLogoFilial: TdmkEdit;
    SbArqLogoFilial: TSpeedButton;
    Label5: TLabel;
    EdArqLogoSIF: TdmkEdit;
    SbArqLogoSIF: TSpeedButton;
    RGQualDtFab: TdmkRadioGroup;
    QrVSSifDipoaQualDtFab: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrParamsEmpAfterScroll(DataSet: TDataSet);
    procedure QrParamsEmpBeforeClose(DataSet: TDataSet);
    procedure SbArqLogoFilialClick(Sender: TObject);
    procedure SbArqLogoSIFClick(Sender: TObject);
    procedure EdRegistroRedefinido(Sender: TObject);
    procedure EdDdValidRedefinido(Sender: TObject);
    procedure EdArqLogoFilialRedefinido(Sender: TObject);
    procedure EdArqLogoSIFRedefinido(Sender: TObject);
  private
    { Private declarations }
    FAlterou: Boolean;
    //
    procedure ReopenVSSifDipoa();
    procedure Salva(ForcaSalvamento: Boolean);
  public
    { Public declarations }
  end;

  var
  FmOpcoesVSSifDipoa: TFmOpcoesVSSifDipoa;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmOpcoesVSSifDipoa.BtOKClick(Sender: TObject);
begin
  Salva(True);
end;

procedure TFmOpcoesVSSifDipoa.BtSaidaClick(Sender: TObject);
begin
  Salva(False);
  Close;
end;

procedure TFmOpcoesVSSifDipoa.EdArqLogoFilialRedefinido(Sender: TObject);
begin
  FAlterou := True;
end;

procedure TFmOpcoesVSSifDipoa.EdArqLogoSIFRedefinido(Sender: TObject);
begin
  FAlterou := True;
end;

procedure TFmOpcoesVSSifDipoa.EdDdValidRedefinido(Sender: TObject);
begin
  FAlterou := True;
end;

procedure TFmOpcoesVSSifDipoa.EdRegistroRedefinido(Sender: TObject);
begin
  FAlterou := True;
end;

procedure TFmOpcoesVSSifDipoa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesVSSifDipoa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrParamsEmp, Dmod.MyDB);
end;

procedure TFmOpcoesVSSifDipoa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesVSSifDipoa.QrParamsEmpAfterScroll(DataSet: TDataSet);
begin
  Salva(False);
  ReopenVSSifDipoa();
end;

procedure TFmOpcoesVSSifDipoa.QrParamsEmpBeforeClose(DataSet: TDataSet);
begin
  QrVSSifDipoa.Close;
end;

procedure TFmOpcoesVSSifDipoa.ReopenVSSifDipoa();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSSifDipoa, Dmod.MyDB, [
  'SELECT * ',
  'FROM vssifdipoa ',
  'WHERE Empresa=' + Geral.FF0(QrParamsEmpCodigo.Value),
  '']);
  //
  EdRegistro.ValueVariant      := QrVSSifDipoaRegistro.Value;
  if QrVSSifDipoa.RecordCount > 0 then
    EdDdValid.ValueVariant       := QrVSSifDipoaDdValid.Value;
  EdArqLogoFilial.ValueVariant := QrVSSifDipoaArqLogoFilial.Value;
  EdArqLogoSIF.ValueVariant    := QrVSSifDipoaArqLogoSIF.Value;
  RGQualDtFab.ItemIndex        := QrVSSifDipoaQualDtFab.Value;
  //
  FAlterou := False;
end;

procedure TFmOpcoesVSSifDipoa.Salva(ForcaSalvamento: Boolean);
var
  Salvar: Boolean;
var
  Registro, ArqLogoFilial, ArqLogoSIF: String;
  Empresa, DdValid, QualDtFab: Integer;
  SQLType: TSQLType;
begin
  if (FAlterou = True) and (ForcaSalvamento = False) then
    Salvar := Geral.MB_Pergunta('Deseja salvar as altera��es?') = ID_Yes
  else
    Salvar := ForcaSalvamento;
  if Salvar then
  begin
    if QrVSSifDipoaEmpresa.Value = 0 then
      SQLType      := stIns
    else
      SQLType      := stUpd;
    Empresa        := QrParamsEmpCodigo.Value;
    Registro       := EdRegistro.ValueVariant;
    DdValid        := EdDdValid.ValueVariant;
    ArqLogoFilial  := EdArqLogoFilial.ValueVariant;
    ArqLogoSIF     := EdArqLogoSIF.ValueVariant;
    QualDtFab      := RGQualDtFab.ItemIndex;
    //
    if MyObjects.FIC(QualDtFab < 1, RGQualDtFab,
    'Defina a "Data da produ��o para efeitos de validade"!') then Exit;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vssifdipoa', False, [
    'Registro', 'DdValid', 'ArqLogoFilial',
    'ArqLogoSIF', 'QualDtFab'], [
    'Empresa'], [
    Registro, DdValid, ArqLogoFilial,
    ArqLogoSIF, QualDtFab], [
    Empresa], True) then
    begin
      Geral.MB_Info('Dados salvos com sucesso!');
    end;
  end;
  FAlterou := False;
end;

procedure TFmOpcoesVSSifDipoa.SbArqLogoFilialClick(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdArqLogoFilial);
end;

procedure TFmOpcoesVSSifDipoa.SbArqLogoSIFClick(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdArqLogoSIF);
end;

end.
