unit VSOCGerCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables,
  DmkDAC_PF, Vcl.Menus, UnAppEnums;

type
  TFmVSOCGerCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBGVSPaCRIts: TdmkDBGridZTO;
    QrVSPaXxxCab: TmySQLQuery;
    DsPaXxxCab: TDataSource;
    BtOC: TBitBtn;
    QrVSPaXxxCabCacCod: TIntegerField;
    QrVSPaXxxCabClaRcl: TFloatField;
    QrVSPaXxxCabNO_ClaRcl: TWideStringField;
    QrVSPaXxxCabCodigo: TIntegerField;
    QrVSPaXxxCabVSGerXxxCab: TIntegerField;
    QrVSPaXxxCabVSMovIts: TIntegerField;
    QrVSPaXxxCabVSPallet: TFloatField;
    QrVSPaXxxCabLstPal01: TIntegerField;
    QrVSPaXxxCabLstPal02: TIntegerField;
    QrVSPaXxxCabLstPal03: TIntegerField;
    QrVSPaXxxCabLstPal04: TIntegerField;
    QrVSPaXxxCabLstPal05: TIntegerField;
    QrVSPaXxxCabLstPal06: TIntegerField;
    QrVSPaXxxCabDtHrFimCla: TDateTimeField;
    QrVSPaXxxCabDtHrFimCla_TXT: TWideStringField;
    PMOC: TPopupMenu;
    ContinuarOCconfigurada1: TMenuItem;
    GerenciamentodaOC1: TMenuItem;
    QrVSPaXxxCabLstPal07: TIntegerField;
    QrVSPaXxxCabLstPal08: TIntegerField;
    QrVSPaXxxCabLstPal09: TIntegerField;
    QrVSPaXxxCabLstPal10: TIntegerField;
    QrVSPaXxxCabLstPal11: TIntegerField;
    QrVSPaXxxCabLstPal12: TIntegerField;
    QrVSPaXxxCabLstPal13: TIntegerField;
    QrVSPaXxxCabLstPal14: TIntegerField;
    QrVSPaXxxCabLstPal15: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGVSPaCRItsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGVSPaCRItsDblClick(Sender: TObject);
    procedure ContinuarOCconfigurada1Click(Sender: TObject);
    procedure GerenciamentodaOC1Click(Sender: TObject);
    procedure BtOCClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenVSPaXxxCab(CacCod: Integer);
  public
    { Public declarations }
  end;

  var
  FmVSOCGerCab: TFmVSOCGerCab;

implementation

uses UnMyObjects, Module, MyDBCheck, VSMod, ModuleGeral, UMySQLModule,
  UnVS_PF;

{$R *.DFM}

procedure TFmVSOCGerCab.BtOCClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMOC, BtOC);
end;

procedure TFmVSOCGerCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOCGerCab.ContinuarOCconfigurada1Click(Sender: TObject);
const
  StqCenLoc = 0; // Ver!!!!
var
  Codigo, CacCod: Integer;
begin
  if QrVSPaXxxCab.State = dsInactive then
    Exit;
  //
  Codigo  := QrVSPaXxxCabCodigo.Value;
  CacCod  := QrVSPaXxxCabCacCod.Value;
  case Trunc(QrVSPaXxxCabClaRcl.Value) of
    // Classificacao
    1: VS_PF.MostraFormVSClassifOne(Codigo, CacCod, emidClassArtXXUni);
    // Relassificacao
    2: VS_PF.MostraFormVSReclassifOneNew(Codigo, CacCod, StqCenLoc, emidReclasXXUni);
    //
    else Geral.MB_Aviso('Tipo de OC n�o implementado!');
  end;
  ReopenVSPaXxxCab(CacCod);
end;

procedure TFmVSOCGerCab.DBGVSPaCRItsDblClick(Sender: TObject);
begin
 //
end;

procedure TFmVSOCGerCab.DBGVSPaCRItsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
(*
var
  Cor: TColor;
*)
begin
(*
  if QrVSPaCRItsEstahNaOC.Value = 0 then
    Cor := clBlack
  else
    Cor := clRed;
  with DBGVSPaCRIts.Canvas do
  begin
    if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
    Font.Color := Cor;
    FillRect(Rect);
    TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
  end;
*)
end;

procedure TFmVSOCGerCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSOCGerCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenVSPaXxxCab(0);
end;

procedure TFmVSOCGerCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOCGerCab.GerenciamentodaOC1Click(Sender: TObject);
var
  Codigo, CacCod, Config: Integer;
begin
  if QrVSPaXxxCab.State = dsInactive then
    Exit;
  //
  Codigo  := QrVSPaXxxCabCodigo.Value;
  CacCod  := QrVSPaXxxCabCacCod.Value;
  Config  := QrVSPaXxxCabVSGerXxxCab.Value;
  case Trunc(QrVSPaXxxCabClaRcl.Value) of
    // Classificacao
    1: VS_PF.MostraFormVSGerClaCab(Config);
    // Reclassificacao
    2: VS_PF.MostraFormVSGerRclCab(Codigo, 0);
    //
    else Geral.MB_Aviso('Tipo de OC n�o implementado!');
  end;
  ReopenVSPaXxxCab(CacCod);
end;

procedure TFmVSOCGerCab.ReopenVSPaXxxCab(CacCod: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaXxxCab, Dmod.MyDB, [
  'SELECT cla.CacCod, 1.000 ClaRcl, "Classifica��o" NO_ClaRcl, ',
  'cla.Codigo, cla.VSGerArt VSGerXxxCab, cla.VSMovIts, ',
  '0.000 VSPallet, ',
  'cla.LstPal01, cla.LstPal02, cla.LstPal03, cla.LstPal04, cla.LstPal05, ',
  'cla.LstPal06, cla.LstPal07, cla.LstPal08, cla.LstPal09, cla.LstPal10, ',
  'cla.LstPal11, cla.LstPal12, cla.LstPal13, cla.LstPal14, cla.LstPal15, ',
  'cla.DtHrFimCla,  ',
  'IF(cla.DtHrFimCla < "1900-01-01", "", DATE_FORMAT(  ',
  'cla.DtHrFimCla, "%d/%m/%Y %H:%i:%s")) DtHrFimCla_TXT ',
  'FROM vspaclacaba cla ',
  'WHERE cla.DtHrFimCla < "1900-01-01" ',
  'OR cla.LstPal01 <> 0 OR cla.LstPal02 <> 0  ',
  'OR cla.LstPal03 <> 0 OR cla.LstPal04 <> 0 ',
  'OR cla.LstPal05 <> 0 OR cla.LstPal06 <> 0 ',
  ' ',
  'UNION ',
  ' ',
  'SELECT rcl.CacCod, 2.000 ClaRcl, "Reclassifica��o" NO_ClaRcl, ',
  'rcl.Codigo, rcl.VSGerRcl VSGerXxxCab, rcl.VSMovIts, rcl.VSPallet + ',
  '0.000 VSPallet, ',
  'rcl.LstPal01, rcl.LstPal02, rcl.LstPal03, rcl.LstPal04, rcl.LstPal05, ',
  'rcl.LstPal06, rcl.LstPal07, rcl.LstPal08, rcl.LstPal09, rcl.LstPal10, ',
  'rcl.LstPal11, rcl.LstPal12, rcl.LstPal13, rcl.LstPal14, rcl.LstPal15, ',
  'rcl.DtHrFimCla, ',
  'IF(rcl.DtHrFimCla < "1900-01-01", "", DATE_FORMAT(  ',
  'rcl.DtHrFimCla, "%d/%m/%Y %H:%i:%s")) DtHrFimCla_TXT ',
  'FROM vsparclcaba rcl ',
  'WHERE rcl.DtHrFimCla < "1900-01-01" ',
  'OR rcl.LstPal01 <> 0 OR rcl.LstPal02 <> 0 ',
  'OR rcl.LstPal03 <> 0 OR rcl.LstPal04 <> 0 ',
  'OR rcl.LstPal05 <> 0 OR rcl.LstPal06 <> 0 ',
  'OR rcl.LstPal07 <> 0 OR rcl.LstPal08 <> 0 ',
  'OR rcl.LstPal09 <> 0 OR rcl.LstPal10 <> 0 ',
  'OR rcl.LstPal11 <> 0 OR rcl.LstPal12 <> 0 ',
  'OR rcl.LstPal13 <> 0 OR rcl.LstPal14 <> 0 ',
  'OR rcl.LstPal15 <> 0 ',
  ' ',
  'ORDER BY CacCod ',
  ' ',
  ' ']);
  //
  if CacCod <> 0 then
    QrVSPaXxxCab.Locate('CacCod', CacCod, []);
end;

end.
