object FmVSClassifOneQnz: TFmVSClassifOneQnz
  Left = 0
  Top = 0
  Caption = 
    'WET-CURTI-015 :: Classifica'#231#227'o de Artigo de Ribeira Couro a Cour' +
    'o'
  ClientHeight = 807
  ClientWidth = 1904
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PanelOC: TPanel
    Left = 0
    Top = 706
    Width = 1904
    Height = 101
    Align = alBottom
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    ExplicitWidth = 1584
    object Panel42: TPanel
      Left = 0
      Top = 0
      Width = 1436
      Height = 101
      Align = alClient
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      ExplicitWidth = 1116
      object Label3: TLabel
        Left = 4
        Top = 8
        Width = 30
        Height = 21
        Caption = 'OC:'
        FocusControl = DBEdCodigo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 172
        Top = 8
        Width = 71
        Height = 21
        Caption = 'Empresa:'
        FocusControl = DBEdit19
      end
      object Label22: TLabel
        Left = 520
        Top = 8
        Width = 41
        Height = 21
        Caption = #193'rea:'
        FocusControl = DBEdit14
      end
      object Label13: TLabel
        Left = 612
        Top = 8
        Width = 47
        Height = 21
        Caption = 'IME-I:'
      end
      object Label14: TLabel
        Left = 792
        Top = 8
        Width = 126
        Height = 21
        Caption = 'Artigo de ribeira:'
      end
      object Label16: TLabel
        Left = 4
        Top = 40
        Width = 49
        Height = 21
        Caption = 'Pe'#231'as:'
      end
      object Label17: TLabel
        Left = 192
        Top = 40
        Width = 111
        Height = 21
        Caption = 'Peso (origem):'
      end
      object Label20: TLabel
        Left = 444
        Top = 40
        Width = 68
        Height = 21
        Caption = #193'rea m'#178':'
      end
      object Label21: TLabel
        Left = 680
        Top = 40
        Width = 65
        Height = 21
        Caption = #193'rea ft'#178':'
      end
      object Label26: TLabel
        Left = 920
        Top = 40
        Width = 83
        Height = 21
        Caption = 'Ficha RMP:'
        FocusControl = DBEdit18
      end
      object Label24: TLabel
        Left = 1216
        Top = 40
        Width = 89
        Height = 21
        Caption = 'Fornecedor:'
        FocusControl = DBEdit16
      end
      object Label18: TLabel
        Left = 4
        Top = 72
        Width = 257
        Height = 21
        Caption = 'Observa'#231#227'o sobre o artigo gerado:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 44
        Top = 4
        Width = 120
        Height = 28
        TabStop = False
        DataField = 'CacCod'
        DataSource = DsVSPaClaCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit19: TDBEdit
        Left = 252
        Top = 4
        Width = 60
        Height = 29
        DataField = 'Empresa'
        DataSource = DsVSPaClaCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 312
        Top = 4
        Width = 205
        Height = 29
        DataField = 'NO_EMPRESA'
        DataSource = DsVSPaClaCab
        TabOrder = 2
      end
      object DBEdit14: TDBEdit
        Left = 568
        Top = 4
        Width = 40
        Height = 29
        DataField = 'NO_TIPO'
        DataSource = DsVSPaClaCab
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 668
        Top = 4
        Width = 120
        Height = 29
        DataField = 'Controle'
        DataSource = DsVSGerArtNew
        PopupMenu = PMIMEI
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 940
        Top = 4
        Width = 60
        Height = 29
        DataField = 'GraGruX'
        DataSource = DsVSGerArtNew
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 1000
        Top = 4
        Width = 429
        Height = 29
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVSGerArtNew
        TabOrder = 6
      end
      object DBEdit9: TDBEdit
        Left = 64
        Top = 36
        Width = 120
        Height = 29
        DataField = 'Pecas'
        DataSource = DsVSGerArtNew
        TabOrder = 7
      end
      object DBEdit10: TDBEdit
        Left = 320
        Top = 36
        Width = 120
        Height = 29
        DataField = 'QtdAntPeso'
        DataSource = DsVSGerArtNew
        TabOrder = 8
      end
      object DBEdit12: TDBEdit
        Left = 524
        Top = 36
        Width = 150
        Height = 29
        DataField = 'AreaM2'
        DataSource = DsVSGerArtNew
        TabOrder = 9
      end
      object DBEdit13: TDBEdit
        Left = 760
        Top = 36
        Width = 150
        Height = 29
        DataField = 'AreaP2'
        DataSource = DsVSGerArtNew
        TabOrder = 10
      end
      object DBEdit18: TDBEdit
        Left = 1020
        Top = 36
        Width = 190
        Height = 29
        DataField = 'NO_FICHA'
        DataSource = DsVSGerArtNew
        TabOrder = 11
      end
      object DBEdit16: TDBEdit
        Left = 1324
        Top = 36
        Width = 105
        Height = 29
        DataField = 'Terceiro'
        DataSource = DsVSGerArtNew
        TabOrder = 12
      end
      object DBEdit11: TDBEdit
        Left = 300
        Top = 68
        Width = 1129
        Height = 29
        DataField = 'Observ'
        DataSource = DsVSGerArtNew
        TabOrder = 13
      end
    end
    object Panel43: TPanel
      Left = 1436
      Top = 0
      Width = 468
      Height = 101
      Align = alRight
      BevelOuter = bvLowered
      TabOrder = 1
      ExplicitLeft = 1116
      object Label71: TLabel
        Left = 4
        Top = 8
        Width = 74
        Height = 21
        Caption = 'Digitador:'
      end
      object Label70: TLabel
        Left = 4
        Top = 40
        Width = 99
        Height = 21
        Caption = 'Classificador:'
      end
      object Label85: TLabel
        Left = 292
        Top = 72
        Width = 57
        Height = 21
        Caption = 'Tempo:'
      end
      object CBRevisor: TdmkDBLookupComboBox
        Left = 180
        Top = 36
        Width = 285
        Height = 29
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsRevisores
        TabOrder = 0
        dmkEditCB = EdRevisor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBDigitador: TdmkDBLookupComboBox
        Left = 180
        Top = 4
        Width = 285
        Height = 29
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDigitadores
        TabOrder = 1
        dmkEditCB = EdDigitador
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDigitador: TdmkEditCB
        Left = 124
        Top = 4
        Width = 56
        Height = 31
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdDigitadorRedefinido
        DBLookupComboBox = CBDigitador
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdRevisor: TdmkEditCB
        Left = 124
        Top = 36
        Width = 56
        Height = 31
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdRevisorRedefinido
        DBLookupComboBox = CBRevisor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdTempo: TEdit
        Left = 368
        Top = 68
        Width = 96
        Height = 29
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 4
        Text = '0,000'
      end
    end
  end
  object PnBoxesAll: TPanel
    Left = 233
    Top = 97
    Width = 1351
    Height = 609
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 1031
    object PnBoxesT01: TPanel
      Left = 0
      Top = 0
      Width = 1351
      Height = 280
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1031
      object PnBoxT1L1: TPanel
        Left = 0
        Top = 0
        Width = 269
        Height = 280
        Align = alLeft
        TabOrder = 0
        object PnBox01: TPanel
          Left = 1
          Top = 1
          Width = 267
          Height = 278
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel5: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 226
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel6: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label4: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit4
                end
                object Label7: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit20
                end
                object Label8: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea1
                end
                object Label54: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit4
                end
                object DBEdit4: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts01
                  TabOrder = 0
                end
                object DBEdit20: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum01
                  TabOrder = 1
                end
                object DBEdArea1: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum01
                  TabOrder = 2
                end
                object DBEdit50: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts01
                  TabOrder = 3
                end
              end
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 113
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel16: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label52: TLabel
                  Left = 4
                  Top = 26
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal01
                end
                object Label55: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit57
                end
                object Label53: TLabel
                  Left = 4
                  Top = 48
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea1
                end
                object Label73: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'm'#178' / Pe'#231'a:'
                  FocusControl = DBEdArea1
                end
                object DBEdSumPcPal01: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal01
                  TabOrder = 0
                  OnChange = DBEdSumPcPal01Change
                end
                object DBEdit43: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal01
                  TabOrder = 1
                end
                object DBEdit57: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet01
                  TabOrder = 2
                end
                object EdMedia01: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 267
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel8: TPanel
              Left = 41
              Top = 0
              Width = 104
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel11: TPanel
                Left = 0
                Top = 29
                Width = 104
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit3: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 4
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet01
                  TabOrder = 0
                end
                object DBEdit8: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet01
                  TabOrder = 1
                end
              end
              object DBEdit1: TDBEdit
                Left = 0
                Top = 0
                Width = 104
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet01
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel50: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label79: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel51: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '01'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel64: TPanel
              Left = 145
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass1: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass1Click
              end
            end
          end
          object Panel3: TPanel
            Left = 165
            Top = 52
            Width = 102
            Height = 226
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei01: TPanel
              Left = 0
              Top = 198
              Width = 102
              Height = 28
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object DBGPallet1: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 102
              Height = 198
              Align = alClient
              DataSource = DsItens01
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              PopupMenu = PMItens01
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT1L2: TPanel
        Left = 269
        Top = 0
        Width = 269
        Height = 280
        Align = alLeft
        TabOrder = 1
        object PnBox02: TPanel
          Left = 1
          Top = 1
          Width = 267
          Height = 278
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel19: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 226
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel26: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label5: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit15
                end
                object Label27: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit27
                end
                object Label28: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea2
                end
                object Label30: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit15
                end
                object DBEdit15: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts02
                  TabOrder = 0
                end
                object DBEdit27: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum02
                  TabOrder = 1
                end
                object DBEdArea2: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum02
                  TabOrder = 2
                end
                object DBEdit29: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts02
                  TabOrder = 3
                end
              end
            end
            object GroupBox4: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 113
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel30: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label31: TLabel
                  Left = 4
                  Top = 26
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal02
                end
                object Label56: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit60
                end
                object Label57: TLabel
                  Left = 4
                  Top = 48
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea2
                end
                object Label74: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'm'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal02: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal02
                  TabOrder = 0
                  OnChange = DBEdSumPcPal02Change
                end
                object DBEdit59: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal02
                  TabOrder = 1
                end
                object DBEdit60: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet02
                  TabOrder = 2
                end
                object EdMedia02: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel17: TPanel
            Left = 0
            Top = 0
            Width = 267
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel18: TPanel
              Left = 41
              Top = 0
              Width = 104
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel44: TPanel
                Left = 0
                Top = 29
                Width = 104
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit25: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 4
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet02
                  TabOrder = 0
                end
                object DBEdit26: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet02
                  TabOrder = 1
                end
              end
              object DBEdit22: TDBEdit
                Left = 0
                Top = 0
                Width = 104
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet02
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel45: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label80: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel46: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '02'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel62: TPanel
              Left = 145
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass2: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass2Click
              end
            end
          end
          object Panel67: TPanel
            Left = 165
            Top = 52
            Width = 102
            Height = 226
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei02: TPanel
              Left = 0
              Top = 198
              Width = 102
              Height = 28
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object DBGPallet2: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 102
              Height = 198
              Align = alClient
              DataSource = DsItens02
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              PopupMenu = PMItens02
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT1L3: TPanel
        Left = 538
        Top = 0
        Width = 274
        Height = 280
        Align = alClient
        TabOrder = 2
        object PnBox03: TPanel
          Left = 1
          Top = 1
          Width = 272
          Height = 278
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel22: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 226
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox5: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel34: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label32: TLabel
                  Left = 4
                  Top = 0
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit33
                end
                object Label33: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit34
                end
                object Label34: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea3
                end
                object Label36: TLabel
                  Left = 6
                  Top = 24
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit33
                end
                object DBEdit33: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts03
                  TabOrder = 0
                end
                object DBEdit34: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum03
                  TabOrder = 1
                end
                object DBEdArea3: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum03
                  TabOrder = 2
                end
                object DBEdit61: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts03
                  TabOrder = 3
                end
              end
            end
            object GroupBox6: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 113
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel35: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label58: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal03
                end
                object Label59: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit64
                end
                object Label60: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea3
                end
                object Label75: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal03: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal03
                  TabOrder = 0
                  OnChange = DBEdSumPcPal03Change
                end
                object DBEdit63: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal03
                  TabOrder = 1
                end
                object DBEdit64: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet03
                  TabOrder = 2
                end
                object EdMedia03: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel20: TPanel
            Left = 0
            Top = 0
            Width = 272
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel21: TPanel
              Left = 41
              Top = 0
              Width = 109
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel55: TPanel
                Left = 0
                Top = 29
                Width = 109
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit31: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 9
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet03
                  TabOrder = 0
                end
                object DBEdit32: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet03
                  TabOrder = 1
                end
              end
              object DBEdit30: TDBEdit
                Left = 0
                Top = 0
                Width = 109
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet03
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel56: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label83: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel57: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '03'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel65: TPanel
              Left = 150
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass3: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass3Click
              end
            end
          end
          object Panel72: TPanel
            Left = 165
            Top = 52
            Width = 107
            Height = 226
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei03: TPanel
              Left = 0
              Top = 198
              Width = 107
              Height = 28
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object DBGPallet3: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 107
              Height = 198
              Align = alClient
              DataSource = DsItens03
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              PopupMenu = PMItens03
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT1L4: TPanel
        Left = 812
        Top = 0
        Width = 269
        Height = 280
        Align = alRight
        TabOrder = 3
        ExplicitLeft = 492
        object PnBox04: TPanel
          Left = 1
          Top = 1
          Width = 267
          Height = 278
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel33: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 226
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox7: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel36: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label47: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit28
                end
                object Label48: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcIMEI04
                end
                object Label49: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea4
                end
                object Label51: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit28
                end
                object DBEdit28: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts04
                  TabOrder = 0
                end
                object DBEdSumPcIMEI04: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum04
                  TabOrder = 1
                end
                object DBEdArea4: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum04
                  TabOrder = 2
                end
                object DBEdit56: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts04
                  TabOrder = 3
                end
              end
            end
            object GroupBox8: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 113
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel37: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label61: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal04
                end
                object Label62: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit67
                end
                object Label63: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea4
                end
                object Label76: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'm'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal04: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal04
                  TabOrder = 0
                  OnChange = DBEdSumPcPal04Change
                end
                object DBEdit66: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal04
                  TabOrder = 1
                end
                object DBEdit67: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet04
                  TabOrder = 2
                end
                object EdMedia04: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel31: TPanel
            Left = 0
            Top = 0
            Width = 267
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel32: TPanel
              Left = 41
              Top = 0
              Width = 104
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel47: TPanel
                Left = 0
                Top = 29
                Width = 104
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit52: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 4
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet04
                  TabOrder = 0
                end
                object DBEdit53: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet04
                  TabOrder = 1
                end
              end
              object DBEdit51: TDBEdit
                Left = 0
                Top = 0
                Width = 104
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet04
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel48: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label81: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel49: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '04'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel61: TPanel
              Left = 145
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass4: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass4Click
              end
            end
          end
          object Panel68: TPanel
            Left = 165
            Top = 52
            Width = 102
            Height = 226
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei04: TPanel
              Left = 0
              Top = 198
              Width = 102
              Height = 28
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object DBGPallet4: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 102
              Height = 198
              Align = alClient
              DataSource = DsItens04
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              PopupMenu = PMItens04
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT1L5: TPanel
        Left = 1081
        Top = 0
        Width = 270
        Height = 280
        Align = alRight
        TabOrder = 4
        ExplicitLeft = 761
        object PnBox05: TPanel
          Left = 1
          Top = 1
          Width = 268
          Height = 278
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel29: TPanel
            Left = 0
            Top = 52
            Width = 173
            Height = 226
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox9: TGroupBox
              Left = 0
              Top = 0
              Width = 173
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel38: TPanel
                Left = 2
                Top = 15
                Width = 169
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label42: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit47
                end
                object Label43: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit48
                end
                object Label44: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea5
                end
                object Label46: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit47
                end
                object DBEdit47: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts05
                  TabOrder = 0
                end
                object DBEdit48: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum05
                  TabOrder = 1
                end
                object DBEdArea5: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum05
                  TabOrder = 2
                end
                object DBEdit68: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts05
                  TabOrder = 3
                end
              end
            end
            object GroupBox10: TGroupBox
              Left = 0
              Top = 113
              Width = 173
              Height = 113
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel39: TPanel
                Left = 2
                Top = 15
                Width = 169
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label64: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal05
                end
                object Label65: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit71
                end
                object Label66: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea5
                end
                object Label77: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'm'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal05: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal05
                  TabOrder = 0
                  OnChange = DBEdSumPcPal05Change
                end
                object DBEdit70: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal05
                  TabOrder = 1
                end
                object DBEdit71: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet05
                  TabOrder = 2
                end
                object EdMedia05: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel27: TPanel
            Left = 0
            Top = 0
            Width = 268
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel28: TPanel
              Left = 41
              Top = 0
              Width = 105
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel52: TPanel
                Left = 0
                Top = 29
                Width = 105
                Height = 31
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit45: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 5
                  Height = 31
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet05
                  TabOrder = 0
                  ExplicitHeight = 21
                end
                object DBEdit46: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 31
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet05
                  TabOrder = 1
                  ExplicitHeight = 21
                end
              end
              object DBEdit44: TDBEdit
                Left = 0
                Top = 0
                Width = 105
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet05
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel53: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label82: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel54: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '05'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel63: TPanel
              Left = 146
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass5: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass5Click
              end
            end
          end
          object Panel69: TPanel
            Left = 173
            Top = 52
            Width = 95
            Height = 226
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei05: TPanel
              Left = 0
              Top = 198
              Width = 95
              Height = 28
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object DBGPallet5: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 95
              Height = 198
              Align = alClient
              DataSource = DsItens05
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              PopupMenu = PMItens05
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object PnBoxesT02: TPanel
      Left = 0
      Top = 280
      Width = 1351
      Height = 49
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 1031
      object PnBoxT2L1: TPanel
        Left = 0
        Top = 0
        Width = 269
        Height = 49
        Align = alLeft
        TabOrder = 0
        object PnBox06: TPanel
          Left = 1
          Top = 1
          Width = 267
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel25: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 229
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox11: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel40: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label37: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit40
                end
                object Label38: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcIMEI06
                end
                object Label39: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea6
                end
                object Label41: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit40
                end
                object DBEdit40: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts06
                  TabOrder = 0
                end
                object DBEdSumPcIMEI06: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum06
                  TabOrder = 1
                end
                object DBEdArea6: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum06
                  TabOrder = 2
                end
                object DBEdit72: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts06
                  TabOrder = 3
                end
              end
            end
            object GroupBox12: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 116
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel41: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 99
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label67: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal06
                end
                object Label68: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit75
                end
                object Label69: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdArea6
                end
                object Label78: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal06: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal06
                  TabOrder = 0
                  OnChange = DBEdSumPcPal06Change
                end
                object DBEdit74: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal06
                  TabOrder = 1
                end
                object DBEdit75: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet06
                  TabOrder = 2
                end
                object EdMedia06: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel23: TPanel
            Left = 0
            Top = 0
            Width = 267
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel24: TPanel
              Left = 41
              Top = 0
              Width = 104
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel58: TPanel
                Left = 0
                Top = 29
                Width = 104
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit38: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 4
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet06
                  TabOrder = 0
                end
                object DBEdit39: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet06
                  TabOrder = 1
                end
              end
              object DBEdit37: TDBEdit
                Left = 0
                Top = 0
                Width = 104
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet06
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel59: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label84: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel60: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '06'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel66: TPanel
              Left = 145
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CkSubClass6: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object Panel71: TPanel
            Left = 165
            Top = 52
            Width = 102
            Height = 229
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei06: TPanel
              Left = 0
              Top = 202
              Width = 102
              Height = 27
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object DBGPallet6: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 102
              Height = 202
              Align = alClient
              DataSource = DsItens06
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              PopupMenu = PMItens06
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT2L2: TPanel
        Left = 269
        Top = 0
        Width = 269
        Height = 49
        Align = alLeft
        TabOrder = 1
        object PnBox07: TPanel
          Left = 1
          Top = 1
          Width = 267
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel115: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 229
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox19: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel116: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label109: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit94
                end
                object Label110: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit95
                end
                object Label111: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit96
                end
                object Label112: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit94
                end
                object DBEdit94: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts07
                  TabOrder = 0
                end
                object DBEdit95: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum07
                  TabOrder = 1
                end
                object DBEdit96: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum07
                  TabOrder = 2
                end
                object DBEdit97: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts07
                  TabOrder = 3
                end
              end
            end
            object GroupBox20: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 116
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel117: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 99
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label113: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal07
                end
                object Label114: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit100
                end
                object Label115: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit96
                end
                object Label116: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal07: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal07
                  TabOrder = 0
                  OnChange = DBEdSumPcPal07Change
                end
                object DBEdit99: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal07
                  TabOrder = 1
                end
                object DBEdit100: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet07
                  TabOrder = 2
                end
                object EdMedia07: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel118: TPanel
            Left = 0
            Top = 0
            Width = 267
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel119: TPanel
              Left = 41
              Top = 0
              Width = 104
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel120: TPanel
                Left = 0
                Top = 29
                Width = 104
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit101: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 4
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet07
                  TabOrder = 0
                end
                object DBEdit102: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet07
                  TabOrder = 1
                end
              end
              object DBEdit103: TDBEdit
                Left = 0
                Top = 0
                Width = 104
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet07
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel121: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label117: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel122: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '07'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel123: TPanel
              Left = 145
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CheckBox4: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object Panel124: TPanel
            Left = 165
            Top = 52
            Width = 102
            Height = 229
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei07: TPanel
              Left = 0
              Top = 202
              Width = 102
              Height = 27
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object dmkDBGridZTO4: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 102
              Height = 202
              Align = alClient
              DataSource = DsItens07
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT2L3: TPanel
        Left = 538
        Top = 0
        Width = 274
        Height = 49
        Align = alClient
        TabOrder = 2
        object PnBox08: TPanel
          Left = 1
          Top = 1
          Width = 272
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel74: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 229
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox21: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel75: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label118: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit49
                end
                object Label119: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit78
                end
                object Label120: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit88
                end
                object Label121: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit49
                end
                object DBEdit49: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts08
                  TabOrder = 0
                end
                object DBEdit78: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum08
                  TabOrder = 1
                end
                object DBEdit88: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum08
                  TabOrder = 2
                end
                object DBEdit98: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts08
                  TabOrder = 3
                end
              end
            end
            object GroupBox22: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 116
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel126: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 99
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label122: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal08
                end
                object Label123: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit105
                end
                object Label124: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit88
                end
                object Label125: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal08: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal08
                  TabOrder = 0
                  OnChange = DBEdSumPcPal08Change
                end
                object DBEdit104: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal08
                  TabOrder = 1
                end
                object DBEdit105: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet08
                  TabOrder = 2
                end
                object EdMedia08: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel127: TPanel
            Left = 0
            Top = 0
            Width = 272
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel128: TPanel
              Left = 41
              Top = 0
              Width = 109
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel129: TPanel
                Left = 0
                Top = 29
                Width = 109
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit106: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 9
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet08
                  TabOrder = 0
                end
                object DBEdit107: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet08
                  TabOrder = 1
                end
              end
              object DBEdit108: TDBEdit
                Left = 0
                Top = 0
                Width = 109
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet08
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel130: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label126: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel131: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '08'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel132: TPanel
              Left = 150
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CheckBox5: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object Panel133: TPanel
            Left = 165
            Top = 52
            Width = 107
            Height = 229
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei08: TPanel
              Left = 0
              Top = 202
              Width = 107
              Height = 27
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object dmkDBGridZTO5: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 107
              Height = 202
              Align = alClient
              DataSource = DsItens08
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT2L4: TPanel
        Left = 812
        Top = 0
        Width = 269
        Height = 49
        Align = alRight
        TabOrder = 3
        ExplicitLeft = 492
        object PnBox09: TPanel
          Left = 1
          Top = 1
          Width = 267
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel137: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 229
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox23: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel138: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label127: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit114
                end
                object Label128: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit115
                end
                object Label129: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit116
                end
                object Label130: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit114
                end
                object DBEdit114: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts09
                  TabOrder = 0
                end
                object DBEdit115: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum09
                  TabOrder = 1
                end
                object DBEdit116: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum09
                  TabOrder = 2
                end
                object DBEdit117: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts09
                  TabOrder = 3
                end
              end
            end
            object GroupBox24: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 116
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel139: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 99
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label131: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal09
                end
                object Label132: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit120
                end
                object Label133: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit116
                end
                object Label134: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal09: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal09
                  TabOrder = 0
                  OnChange = DBEdSumPcPal09Change
                end
                object DBEdit119: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal09
                  TabOrder = 1
                end
                object DBEdit120: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet09
                  TabOrder = 2
                end
                object EdMedia09: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel140: TPanel
            Left = 0
            Top = 0
            Width = 267
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel141: TPanel
              Left = 41
              Top = 0
              Width = 104
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel142: TPanel
                Left = 0
                Top = 29
                Width = 104
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit121: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 4
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet09
                  TabOrder = 0
                end
                object DBEdit122: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet09
                  TabOrder = 1
                end
              end
              object DBEdit123: TDBEdit
                Left = 0
                Top = 0
                Width = 104
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet09
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel143: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label135: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel144: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '09'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel145: TPanel
              Left = 145
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CheckBox6: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object Panel146: TPanel
            Left = 165
            Top = 52
            Width = 102
            Height = 229
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei09: TPanel
              Left = 0
              Top = 202
              Width = 102
              Height = 27
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object dmkDBGridZTO6: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 102
              Height = 202
              Align = alClient
              DataSource = DsItens09
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT2L5: TPanel
        Left = 1081
        Top = 0
        Width = 270
        Height = 49
        Align = alRight
        TabOrder = 4
        ExplicitLeft = 761
        object PnBox10: TPanel
          Left = 1
          Top = 1
          Width = 268
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel148: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 229
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox25: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel149: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label136: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit124
                end
                object Label137: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdit125
                end
                object Label138: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit126
                end
                object Label139: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit124
                end
                object DBEdit124: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts10
                  TabOrder = 0
                end
                object DBEdit125: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum10
                  TabOrder = 1
                end
                object DBEdit126: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum10
                  TabOrder = 2
                end
                object DBEdit127: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts10
                  TabOrder = 3
                end
              end
            end
            object GroupBox26: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 116
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel150: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 99
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label140: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal10
                end
                object Label141: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit130
                end
                object Label142: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit126
                end
                object Label143: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal10: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal10
                  TabOrder = 0
                  OnChange = DBEdSumPcPal10Change
                end
                object DBEdit129: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal10
                  TabOrder = 1
                end
                object DBEdit130: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet10
                  TabOrder = 2
                end
                object EdMedia10: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel151: TPanel
            Left = 0
            Top = 0
            Width = 268
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel152: TPanel
              Left = 41
              Top = 0
              Width = 105
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel153: TPanel
                Left = 0
                Top = 29
                Width = 105
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit131: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 5
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet10
                  TabOrder = 0
                end
                object DBEdit132: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet10
                  TabOrder = 1
                end
              end
              object DBEdit133: TDBEdit
                Left = 0
                Top = 0
                Width = 105
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet10
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel154: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label144: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel155: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '10'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel156: TPanel
              Left = 146
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CheckBox7: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object Panel157: TPanel
            Left = 165
            Top = 52
            Width = 103
            Height = 229
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei10: TPanel
              Left = 0
              Top = 202
              Width = 103
              Height = 27
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object dmkDBGridZTO7: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 103
              Height = 202
              Align = alClient
              DataSource = DsItens10
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object Panel73: TPanel
      Left = 0
      Top = 329
      Width = 1351
      Height = 280
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      ExplicitWidth = 1031
      object PnBoxT3L3: TPanel
        Left = 546
        Top = 0
        Width = 269
        Height = 280
        Align = alLeft
        TabOrder = 2
        object PnBox13: TPanel
          Left = 1
          Top = 1
          Width = 267
          Height = 278
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel76: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 226
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox13: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel77: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label11: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit17
                end
                object Label29: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcIMEI13
                end
                object Label35: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit35
                end
                object Label40: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit17
                end
                object DBEdit17: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts13
                  TabOrder = 0
                end
                object DBEdSumPcIMEI13: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum13
                  TabOrder = 1
                end
                object DBEdit35: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum13
                  TabOrder = 2
                end
                object DBEdit36: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts13
                  TabOrder = 3
                end
              end
            end
            object GroupBox14: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 113
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel78: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label45: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal13
                end
                object Label50: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit54
                end
                object Label87: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit35
                end
                object Label89: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal13: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal13
                  TabOrder = 0
                  OnChange = DBEdSumPcPal13Change
                end
                object DBEdit41: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal13
                  TabOrder = 1
                end
                object DBEdit54: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet13
                  TabOrder = 2
                end
                object EdMedia13: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel79: TPanel
            Left = 0
            Top = 0
            Width = 267
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel80: TPanel
              Left = 41
              Top = 0
              Width = 104
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel81: TPanel
                Left = 0
                Top = 29
                Width = 104
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit55: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 4
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet13
                  TabOrder = 0
                end
                object DBEdit58: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet13
                  TabOrder = 1
                end
              end
              object DBEdit62: TDBEdit
                Left = 0
                Top = 0
                Width = 104
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet13
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel82: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label90: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel83: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '13'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel84: TPanel
              Left = 145
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CheckBox1: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object Panel85: TPanel
            Left = 165
            Top = 52
            Width = 102
            Height = 226
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei13: TPanel
              Left = 0
              Top = 198
              Width = 102
              Height = 28
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object dmkDBGridZTO1: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 102
              Height = 198
              Align = alClient
              DataSource = DsItens13
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT3L2: TPanel
        Left = 277
        Top = 0
        Width = 269
        Height = 280
        Align = alLeft
        TabOrder = 1
        object PnBox12: TPanel
          Left = 1
          Top = 1
          Width = 267
          Height = 278
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel89: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 226
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox15: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel90: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label91: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit69
                end
                object Label92: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcIMEI12
                end
                object Label93: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit73
                end
                object Label94: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit69
                end
                object DBEdit69: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts12
                  TabOrder = 0
                end
                object DBEdSumPcIMEI12: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum12
                  TabOrder = 1
                end
                object DBEdit73: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum12
                  TabOrder = 2
                end
                object DBEdit76: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts12
                  TabOrder = 3
                end
              end
            end
            object GroupBox16: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 113
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel91: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label95: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal12
                end
                object Label96: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit79
                end
                object Label97: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit73
                end
                object Label98: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal12: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal12
                  TabOrder = 0
                  OnChange = DBEdSumPcPal12Change
                end
                object DBEdit77: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal12
                  TabOrder = 1
                end
                object DBEdit79: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet12
                  TabOrder = 2
                end
                object EdMedia12: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel92: TPanel
            Left = 0
            Top = 0
            Width = 267
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel93: TPanel
              Left = 41
              Top = 0
              Width = 104
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel94: TPanel
                Left = 0
                Top = 29
                Width = 104
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit80: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 4
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet12
                  TabOrder = 0
                end
                object DBEdit81: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet12
                  TabOrder = 1
                end
              end
              object DBEdit82: TDBEdit
                Left = 0
                Top = 0
                Width = 104
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet12
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel95: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label99: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel96: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '12'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel97: TPanel
              Left = 145
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CheckBox2: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object Panel98: TPanel
            Left = 165
            Top = 52
            Width = 102
            Height = 226
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei12: TPanel
              Left = 0
              Top = 198
              Width = 102
              Height = 28
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object dmkDBGridZTO2: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 102
              Height = 198
              Align = alClient
              DataSource = DsItens12
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT3L1: TPanel
        Left = 0
        Top = 0
        Width = 277
        Height = 280
        Align = alLeft
        TabOrder = 0
        object PnBox11: TPanel
          Left = 1
          Top = 1
          Width = 275
          Height = 278
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel102: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 226
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox17: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel103: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label100: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit84
                end
                object Label101: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcIMEI11
                end
                object Label102: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit85
                end
                object Label103: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit84
                end
                object DBEdit84: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts11
                  TabOrder = 0
                end
                object DBEdSumPcIMEI11: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum11
                  TabOrder = 1
                end
                object DBEdit85: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum11
                  TabOrder = 2
                end
                object DBEdit86: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts11
                  TabOrder = 3
                end
              end
            end
            object GroupBox18: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 113
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel104: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label104: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal11
                end
                object Label105: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit89
                end
                object Label106: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit85
                end
                object Label107: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal11: TDBEdit
                  Left = 68
                  Top = 28
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal11
                  TabOrder = 0
                  OnChange = DBEdSumPcPal11Change
                end
                object DBEdit87: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal11
                  TabOrder = 1
                end
                object DBEdit89: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet11
                  TabOrder = 2
                end
                object EdMedia11: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel105: TPanel
            Left = 0
            Top = 0
            Width = 275
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel106: TPanel
              Left = 41
              Top = 0
              Width = 112
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel107: TPanel
                Left = 0
                Top = 29
                Width = 112
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit90: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 12
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet11
                  TabOrder = 0
                end
                object DBEdit91: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet11
                  TabOrder = 1
                end
              end
              object DBEdit92: TDBEdit
                Left = 0
                Top = 0
                Width = 112
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet11
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel108: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label108: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel109: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '11'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel110: TPanel
              Left = 153
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CheckBox3: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object Panel111: TPanel
            Left = 165
            Top = 52
            Width = 110
            Height = 226
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei11: TPanel
              Left = 0
              Top = 198
              Width = 110
              Height = 28
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object dmkDBGridZTO3: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 110
              Height = 198
              Align = alClient
              DataSource = DsItens11
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT3L4: TPanel
        Left = 815
        Top = 0
        Width = 269
        Height = 280
        Align = alLeft
        TabOrder = 3
        object PnBox14: TPanel
          Left = 1
          Top = 1
          Width = 267
          Height = 278
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel87: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 226
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox27: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel88: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label145: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit65
                end
                object Label146: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcIMEI14
                end
                object Label147: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit83
                end
                object Label148: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit65
                end
                object DBEdit65: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts14
                  TabOrder = 0
                end
                object DBEdSumPcIMEI14: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum14
                  TabOrder = 1
                end
                object DBEdit83: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum14
                  TabOrder = 2
                end
                object DBEdit93: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts14
                  TabOrder = 3
                end
              end
            end
            object GroupBox28: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 113
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel100: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label149: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal14
                end
                object Label150: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit110
                end
                object Label151: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit83
                end
                object Label152: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal14: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal14
                  TabOrder = 0
                  OnChange = DBEdSumPcPal14Change
                end
                object DBEdit109: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal14
                  TabOrder = 1
                end
                object DBEdit110: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet14
                  TabOrder = 2
                end
                object EdMedia14: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel101: TPanel
            Left = 0
            Top = 0
            Width = 267
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel113: TPanel
              Left = 41
              Top = 0
              Width = 104
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel114: TPanel
                Left = 0
                Top = 29
                Width = 104
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit111: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 4
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet14
                  TabOrder = 0
                end
                object DBEdit112: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet14
                  TabOrder = 1
                end
              end
              object DBEdit113: TDBEdit
                Left = 0
                Top = 0
                Width = 104
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet14
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel135: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label153: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel136: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '14'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel159: TPanel
              Left = 145
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CheckBox8: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object Panel160: TPanel
            Left = 165
            Top = 52
            Width = 102
            Height = 226
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei14: TPanel
              Left = 0
              Top = 198
              Width = 102
              Height = 28
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object dmkDBGridZTO8: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 102
              Height = 198
              Align = alClient
              DataSource = DsItens14
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object PnBoxT3L5: TPanel
        Left = 1081
        Top = 0
        Width = 270
        Height = 280
        Align = alRight
        TabOrder = 4
        ExplicitLeft = 761
        object PnBox15: TPanel
          Left = 1
          Top = 1
          Width = 268
          Height = 278
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel162: TPanel
            Left = 0
            Top = 52
            Width = 165
            Height = 226
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox29: TGroupBox
              Left = 0
              Top = 0
              Width = 165
              Height = 113
              Align = alTop
              Caption = ' Do artigo na classe: '
              TabOrder = 0
              object Panel163: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label154: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit118
                end
                object Label155: TLabel
                  Left = 4
                  Top = 52
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcIMEI15
                end
                object Label156: TLabel
                  Left = 4
                  Top = 76
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit128
                end
                object Label157: TLabel
                  Left = 6
                  Top = 28
                  Width = 30
                  Height = 13
                  Caption = 'IME-I:'
                  FocusControl = DBEdit118
                end
                object DBEdit118: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Controle'
                  DataSource = DsVSPalClaIts15
                  TabOrder = 0
                end
                object DBEdSumPcIMEI15: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSum15
                  TabOrder = 1
                end
                object DBEdit128: TDBEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSum15
                  TabOrder = 2
                end
                object DBEdit134: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'VMI_Dest'
                  DataSource = DsVSPalClaIts15
                  TabOrder = 3
                end
              end
            end
            object GroupBox30: TGroupBox
              Left = 0
              Top = 113
              Width = 165
              Height = 113
              Align = alClient
              Caption = ' Do Pallet: '
              TabOrder = 1
              object Panel164: TPanel
                Left = 2
                Top = 15
                Width = 161
                Height = 96
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label158: TLabel
                  Left = 4
                  Top = 28
                  Width = 32
                  Height = 13
                  Caption = 'Pe'#231'as:'
                  FocusControl = DBEdSumPcPal15
                end
                object Label159: TLabel
                  Left = 4
                  Top = 4
                  Width = 15
                  Height = 13
                  Caption = 'ID:'
                  FocusControl = DBEdit136
                end
                object Label160: TLabel
                  Left = 4
                  Top = 52
                  Width = 27
                  Height = 13
                  Caption = #193'rea:'
                  FocusControl = DBEdit128
                end
                object Label161: TLabel
                  Left = 4
                  Top = 75
                  Width = 50
                  Height = 13
                  Caption = 'M'#178' / Pe'#231'a:'
                end
                object DBEdSumPcPal15: TDBEdit
                  Left = 68
                  Top = 24
                  Width = 92
                  Height = 21
                  DataField = 'Pecas'
                  DataSource = DsSumPal15
                  TabOrder = 0
                  OnChange = DBEdSumPcPal15Change
                end
                object DBEdit135: TDBEdit
                  Left = 68
                  Top = 48
                  Width = 92
                  Height = 21
                  DataField = 'AreaM2'
                  DataSource = DsSumPal15
                  TabOrder = 1
                end
                object DBEdit136: TDBEdit
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 21
                  DataField = 'Codigo'
                  DataSource = DsVSPallet15
                  TabOrder = 2
                end
                object EdMedia15: TdmkEdit
                  Left = 68
                  Top = 72
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object Panel165: TPanel
            Left = 0
            Top = 0
            Width = 268
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Panel166: TPanel
              Left = 41
              Top = 0
              Width = 105
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel167: TPanel
                Left = 0
                Top = 29
                Width = 105
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object DBEdit137: TDBEdit
                  Left = 100
                  Top = 0
                  Width = 5
                  Height = 21
                  Align = alClient
                  DataField = 'NO_CLISTAT'
                  DataSource = DsVSPallet15
                  TabOrder = 0
                end
                object DBEdit138: TDBEdit
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 21
                  Align = alLeft
                  DataField = 'NO_STATUS'
                  DataSource = DsVSPallet15
                  TabOrder = 1
                end
              end
              object DBEdit139: TDBEdit
                Left = 0
                Top = 0
                Width = 105
                Height = 29
                Align = alTop
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsVSPallet15
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel168: TPanel
              Left = 0
              Top = 0
              Width = 41
              Height = 52
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label162: TLabel
                Left = 0
                Top = 0
                Width = 18
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Box'
              end
              object Panel169: TPanel
                Left = 0
                Top = 13
                Width = 41
                Height = 39
                Align = alClient
                BevelOuter = bvNone
                Caption = '15'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -32
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel170: TPanel
              Left = 146
              Top = 0
              Width = 122
              Height = 52
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object CheckBox9: TCheckBox
                Left = 4
                Top = 0
                Width = 113
                Height = 25
                Caption = 'Sub classe'
                TabOrder = 0
                OnClick = CkSubClass6Click
              end
            end
          end
          object Panel171: TPanel
            Left = 165
            Top = 52
            Width = 103
            Height = 226
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PnIntMei15: TPanel
              Left = 0
              Top = 198
              Width = 103
              Height = 28
              Align = alBottom
              Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
              UseDockManager = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Visible = False
            end
            object dmkDBGridZTO9: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 103
              Height = 198
              Align = alClient
              DataSource = DsItens15
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              OptionsEx = []
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 1584
    Top = 97
    Width = 320
    Height = 609
    Align = alRight
    Caption = 'Panel2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    ExplicitLeft = 1264
    object DBGAll: TdmkDBGridZTO
      Left = 1
      Top = 1
      Width = 318
      Height = 607
      Align = alClient
      DataSource = DsAll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      OptionsEx = []
      ParentFont = False
      PopupMenu = PMAll
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Box'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SubClass'
          Title.Caption = 'Sub classe'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_MARTELO'
          Title.Caption = 'Martelo'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VMI_Dest'
          Title.Caption = 'IME-I'
          Width = 100
          Visible = True
        end>
    end
  end
  object PnInfoBig: TPanel
    Left = 0
    Top = 0
    Width = 1904
    Height = 97
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 1584
    object PnDigitacao: TPanel
      Left = 1371
      Top = 1
      Width = 532
      Height = 95
      Align = alRight
      Enabled = False
      TabOrder = 0
      ExplicitLeft = 1051
      object Label12: TLabel
        Left = 1
        Top = 1
        Width = 530
        Height = 21
        Align = alTop
        Alignment = taCenter
        Caption = ' Digita'#231#227'o'
        ExplicitWidth = 75
      end
      object Panel7: TPanel
        Left = 141
        Top = 22
        Width = 52
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label2: TLabel
          Left = 0
          Top = 0
          Width = 33
          Height = 21
          Align = alTop
          Caption = 'Box:'
        end
        object EdBox: TdmkEdit
          Left = 0
          Top = 21
          Width = 52
          Height = 51
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdBoxChange
        end
      end
      object PnArea: TPanel
        Left = 1
        Top = 22
        Width = 140
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 0
          Top = 0
          Width = 41
          Height = 21
          Align = alTop
          Caption = #193'rea:'
        end
        object LaTipoArea: TLabel
          Left = 105
          Top = 21
          Width = 35
          Height = 39
          Align = alRight
          Caption = '?'#178
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdArea: TdmkEdit
          Left = 0
          Top = 21
          Width = 105
          Height = 51
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object PnMartelo: TPanel
        Left = 276
        Top = 22
        Width = 255
        Height = 72
        Align = alClient
        TabOrder = 3
        Visible = False
        object Label72: TLabel
          Left = 1
          Top = 1
          Width = 253
          Height = 21
          Align = alTop
          Caption = 'Martelo:'
          ExplicitWidth = 62
        end
        object CBVSMrtCad: TdmkDBLookupComboBox
          Left = 85
          Top = 22
          Width = 169
          Height = 47
          Align = alClient
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          KeyField = 'Nome'
          ListField = 'Nome'
          ListSource = DsVSMrtCad
          ParentFont = False
          TabOrder = 1
          TabStop = False
          dmkEditCB = EdVSMrtCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdVSMrtCad: TdmkEditCB
          Left = 1
          Top = 22
          Width = 84
          Height = 49
          Align = alLeft
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnKeyDown = EdVSMrtCadKeyDown
          DBLookupComboBox = CBVSMrtCad
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
      end
      object PnSubClass: TPanel
        Left = 193
        Top = 22
        Width = 83
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        object Label86: TLabel
          Left = 0
          Top = 0
          Width = 64
          Height = 21
          Align = alTop
          Caption = 'SubClas:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdSubClass: TdmkEdit
          Left = 0
          Top = 21
          Width = 83
          Height = 51
          Align = alClient
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          MaxLength = 10
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSubClassChange
          OnExit = EdSubClassExit
          OnKeyDown = EdSubClassKeyDown
        end
      end
    end
    object Panel12: TPanel
      Left = 321
      Top = 1
      Width = 320
      Height = 95
      Align = alLeft
      TabOrder = 1
      object Label15: TLabel
        Left = 1
        Top = 1
        Width = 168
        Height = 21
        Align = alTop
        Alignment = taCenter
        Caption = 'Couros j'#225' classificados'
      end
      object Panel10: TPanel
        Left = 1
        Top = 22
        Width = 120
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label10: TLabel
          Left = 0
          Top = 0
          Width = 90
          Height = 21
          Align = alTop
          Caption = ' Int. ou 1/2:'
          FocusControl = DBEdit23
        end
        object DBEdit23: TDBEdit
          Left = 0
          Top = 21
          Width = 120
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'JaFoi_PECA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
      object Panel9: TPanel
        Left = 121
        Top = 22
        Width = 198
        Height = 72
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label9: TLabel
          Left = 0
          Top = 0
          Width = 46
          Height = 21
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEDAreaT
        end
        object DBEDAreaT: TDBEdit
          Left = 0
          Top = 21
          Width = 198
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'JaFoi_AREA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
    end
    object Panel13: TPanel
      Left = 1
      Top = 1
      Width = 320
      Height = 95
      Align = alLeft
      TabOrder = 2
      object Label19: TLabel
        Left = 1
        Top = 1
        Width = 213
        Height = 21
        Align = alTop
        Caption = 'Couros que faltam classificar'
      end
      object Panel14: TPanel
        Left = 1
        Top = 22
        Width = 120
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label23: TLabel
          Left = 0
          Top = 0
          Width = 90
          Height = 21
          Align = alTop
          Caption = ' Int. ou 1/2:'
          FocusControl = DBEdit21
        end
        object DBEdit21: TDBEdit
          Left = 0
          Top = 21
          Width = 120
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'SdoVrtPeca'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
      object Panel15: TPanel
        Left = 121
        Top = 22
        Width = 198
        Height = 72
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label25: TLabel
          Left = 0
          Top = 0
          Width = 46
          Height = 21
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEdit24
        end
        object DBEdit24: TDBEdit
          Left = 0
          Top = 21
          Width = 198
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'SdoVrtArM2'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
    end
    object BtEncerra: TBitBtn
      Tag = 10134
      Left = 736
      Top = 8
      Width = 216
      Height = 72
      Caption = '&Menu'
      TabOrder = 3
      TabStop = False
      OnClick = BtEncerraClick
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 956
      Top = 8
      Width = 216
      Height = 72
      Caption = '&Imprimir'
      TabOrder = 4
      TabStop = False
      OnClick = BtImprimeClick
    end
    object BtDigitacao: TButton
      Left = 644
      Top = 28
      Width = 89
      Height = 45
      Caption = 'Teste'
      Enabled = False
      TabOrder = 5
      Visible = False
      OnClick = BtDigitacaoClick
    end
    object BtReabre: TBitBtn
      Tag = 18
      Left = 1177
      Top = 8
      Width = 72
      Height = 72
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = BtReabreClick
    end
    object CkMartelo: TCheckBox
      Left = 1256
      Top = 12
      Width = 97
      Height = 20
      Caption = 'Martelo'
      TabOrder = 7
      OnClick = CkMarteloClick
    end
    object CkSubClass: TCheckBox
      Left = 1256
      Top = 36
      Width = 113
      Height = 20
      Caption = 'Sub Classe'
      TabOrder = 8
      OnClick = CkSubClassClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 97
    Width = 233
    Height = 609
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 4
    object PnEqualize: TPanel
      Left = 0
      Top = 0
      Width = 233
      Height = 609
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel70: TPanel
        Left = 0
        Top = 0
        Width = 233
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object SbEqualize: TSpeedButton
          Left = 136
          Top = 8
          Width = 23
          Height = 22
          OnClick = SbEqualizeClick
        end
        object Label88: TLabel
          Left = 8
          Top = 12
          Width = 43
          Height = 13
          Caption = 'Equ'#225'lize:'
        end
        object EdEqualize: TdmkEdit
          Left = 52
          Top = 8
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CkEqualize: TCheckBox
          Left = 164
          Top = 0
          Width = 50
          Height = 17
          Caption = 'Auto.'
          TabOrder = 1
        end
        object CkNota: TCheckBox
          Left = 164
          Top = 16
          Width = 50
          Height = 17
          Caption = 'Nota.'
          TabOrder = 2
          OnClick = CkNotaClick
        end
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 74
        Width = 233
        Height = 535
        Align = alClient
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SubClass'
            Title.Caption = 'Sub classe'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercM2'
            Title.Caption = '% m'#178
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end>
      end
      object PnNota: TPanel
        Left = 0
        Top = 33
        Width = 233
        Height = 41
        Align = alTop
        TabOrder = 2
        Visible = False
        object DBEdit42: TDBEdit
          Left = 1
          Top = 1
          Width = 231
          Height = 39
          Align = alClient
          DataField = 'NotaEqzM2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Font.Quality = fqAntialiased
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
      object Memo1: TMemo
        Left = 20
        Top = 348
        Width = 185
        Height = 89
        Lines.Strings = (
          'Memo1')
        TabOrder = 3
        Visible = False
      end
    end
  end
  object QrVSPaClaCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaClaCabBeforeClose
    AfterScroll = QrVSPaClaCabAfterScroll
    OnCalcFields = QrVSPaClaCabCalcFields
    SQL.Strings = (
      'SELECT vga.GraGruX, vga.Nome,'
      'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP'
      'FROM vspaclacab pcc'
      'LEFT JOIN vsgerart vga ON vga.Codigo=pcc.vsgerart'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vga.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa'
      'WHERE pcc.Codigo=5'
      ''
      ''
      '')
    Left = 1632
    Top = 132
    object QrVSPaClaCabNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSPaClaCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPaClaCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSPaClaCabTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSPaClaCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPaClaCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSPaClaCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaClaCabVSGerArt: TIntegerField
      FieldName = 'VSGerArt'
    end
    object QrVSPaClaCabLstPal01: TIntegerField
      FieldName = 'LstPal01'
    end
    object QrVSPaClaCabLstPal02: TIntegerField
      FieldName = 'LstPal02'
    end
    object QrVSPaClaCabLstPal03: TIntegerField
      FieldName = 'LstPal03'
    end
    object QrVSPaClaCabLstPal04: TIntegerField
      FieldName = 'LstPal04'
    end
    object QrVSPaClaCabLstPal05: TIntegerField
      FieldName = 'LstPal05'
    end
    object QrVSPaClaCabLstPal06: TIntegerField
      FieldName = 'LstPal06'
    end
    object QrVSPaClaCabLstPal07: TIntegerField
      FieldName = 'LstPal07'
    end
    object QrVSPaClaCabLstPal08: TIntegerField
      FieldName = 'LstPal08'
    end
    object QrVSPaClaCabLstPal09: TIntegerField
      FieldName = 'LstPal09'
    end
    object QrVSPaClaCabLstPal10: TIntegerField
      FieldName = 'LstPal10'
    end
    object QrVSPaClaCabLstPal11: TIntegerField
      FieldName = 'LstPal11'
    end
    object QrVSPaClaCabLstPal12: TIntegerField
      FieldName = 'LstPal12'
    end
    object QrVSPaClaCabLstPal13: TIntegerField
      FieldName = 'LstPal13'
    end
    object QrVSPaClaCabLstPal14: TIntegerField
      FieldName = 'LstPal14'
    end
    object QrVSPaClaCabLstPal15: TIntegerField
      FieldName = 'LstPal15'
    end
    object QrVSPaClaCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaClaCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaClaCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaClaCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaClaCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaClaCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaClaCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaClaCabNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPaClaCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPaClaCabVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSPaClaCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
  end
  object DsVSPaClaCab: TDataSource
    DataSet = QrVSPaClaCab
    Left = 1632
    Top = 180
  end
  object QrVSGerArtNew: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSGerArtNewAfterOpen
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet ')
    Left = 1724
    Top = 133
    object QrVSGerArtNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerArtNewControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSGerArtNewMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGerArtNewEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerArtNewMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSGerArtNewGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerArtNewPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGerArtNewDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGerArtNewDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGerArtNewUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGerArtNewUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGerArtNewAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGerArtNewAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArtNewSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSGerArtNewSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSGerArtNewSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSGerArtNewPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSGerArtNewNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerArtNewSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGerArtNewValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSGerArtNewCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrVSGerArtNewMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSGerArtNewTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSGerArtNewCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSGerArtNewLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSGerArtNewFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSGerArtNewLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSGerArtNewMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSGerArtNewCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSGerArtNewValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSGerArtNewDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSGerArtNewDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSGerArtNewQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSGerArtNewNO_FICHA: TWideStringField
      FieldName = 'NO_FICHA'
    end
    object QrVSGerArtNewCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSGerArtNewNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSGerArtNewFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSGerArtNewVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSGerArtNewClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSGerArtNewFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
  end
  object DsVSGerArtNew: TDataSource
    DataSet = QrVSGerArtNew
    Left = 1724
    Top = 181
  end
  object QrVSPallet01: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet01AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 408
    Top = 113
    object QrVSPallet01FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet01Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet01Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet01Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet01DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet01DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet01UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet01UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet01AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet01Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet01Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet01Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet01CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet01GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet01NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet01NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet01NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet01QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet01: TDataSource
    DataSet = QrVSPallet01
    Left = 408
    Top = 157
  end
  object QrVSPallet02: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet02AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 760
    Top = 41
    object QrVSPallet02FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet02Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet02Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet02Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet02DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet02DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet02UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet02UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet02AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet02Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet02Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet02Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet02CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet02GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet02NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet02NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet02NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet02QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet02: TDataSource
    DataSet = QrVSPallet02
    Left = 672
    Top = 121
  end
  object QrVSPallet03: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet03AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 940
    Top = 113
    object QrVSPallet03FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet03Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet03Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet03Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet03DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet03DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet03UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet03UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet03AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet03Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet03Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet03Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet03CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet03GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet03NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet03NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet03NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet03QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet03: TDataSource
    DataSet = QrVSPallet03
    Left = 940
    Top = 157
  end
  object QrVSPallet04: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet04AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1220
    Top = 105
    object QrVSPallet04FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet04Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet04Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet04Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet04DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet04DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet04UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet04UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet04AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet04Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet04Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet04Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet04CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet04GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet04NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet04NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet04NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet04QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet04: TDataSource
    DataSet = QrVSPallet04
    Left = 1220
    Top = 149
  end
  object QrVSPallet05: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet05AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1476
    Top = 109
    object QrVSPallet05FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet05Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet05Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet05Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet05DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet05DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet05UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet05UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet05AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet05Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet05Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet05Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet05CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet05GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet05NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet05NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet05NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet05QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet05: TDataSource
    DataSet = QrVSPallet05
    Left = 1476
    Top = 153
  end
  object QrVSPallet06: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet06AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 408
    Top = 385
    object QrVSPallet06FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet06Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet06Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet06Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet06DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet06DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet06UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet06UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet06AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet06Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet06Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet06Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet06CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet06GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet06NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet06NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet06NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet06QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet06: TDataSource
    DataSet = QrVSPallet06
    Left = 408
    Top = 429
  end
  object QrVSPalClaIts01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 408
    Top = 204
    object QrVSPalClaIts01Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts01DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrVSPalClaIts01VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts01VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
  end
  object DsVSPalClaIts01: TDataSource
    DataSet = QrVSPalClaIts01
    Left = 408
    Top = 252
  end
  object QrVSPalClaIts02: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 676
    Top = 168
    object QrVSPalClaIts02VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts02VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts02Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts02DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts02: TDataSource
    DataSet = QrVSPalClaIts02
    Left = 676
    Top = 216
  end
  object QrVSPalClaIts03: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 940
    Top = 204
    object QrVSPalClaIts03VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts03VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts03Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts03DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts03: TDataSource
    DataSet = QrVSPalClaIts03
    Left = 940
    Top = 252
  end
  object QrVSPalClaIts04: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1220
    Top = 196
    object QrVSPalClaIts04VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts04VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts04Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts04DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts04: TDataSource
    DataSet = QrVSPalClaIts04
    Left = 1220
    Top = 244
  end
  object QrVSPalClaIts05: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1476
    Top = 200
    object QrVSPalClaIts05VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts05VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts05Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts05DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts05: TDataSource
    DataSet = QrVSPalClaIts05
    Left = 1476
    Top = 248
  end
  object QrVSPalClaIts06: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 408
    Top = 476
    object QrVSPalClaIts06VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts06VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts06Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts06DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts06: TDataSource
    DataSet = QrVSPalClaIts06
    Left = 408
    Top = 524
  end
  object QrItens01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 408
    Top = 296
    object QrItens01Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens01Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens01AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens01AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens01: TDataSource
    DataSet = QrItens01
    Left = 408
    Top = 344
  end
  object QrSum01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 460
    Top = 140
    object QrSum01Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum01AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum01AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum01: TDataSource
    DataSet = QrSum01
    Left = 460
    Top = 188
  end
  object QrItens02: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 676
    Top = 260
    object QrItens02Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens02Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens02AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens02AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens02: TDataSource
    DataSet = QrItens02
    Left = 676
    Top = 308
  end
  object QrSum02: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 732
    Top = 148
    object QrSum02Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum02AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum02AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum02: TDataSource
    DataSet = QrSum02
    Left = 732
    Top = 196
  end
  object QrItens03: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 940
    Top = 296
    object QrItens03Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens03Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens03AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens03AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens03: TDataSource
    DataSet = QrItens03
    Left = 940
    Top = 344
  end
  object QrSum03: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 992
    Top = 136
    object QrSum03Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum03AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum03AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum03: TDataSource
    DataSet = QrSum03
    Left = 992
    Top = 184
  end
  object QrItens04: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1220
    Top = 288
    object QrItens04Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens04Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens04AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens04AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens04: TDataSource
    DataSet = QrItens04
    Left = 1220
    Top = 336
  end
  object QrSum04: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1272
    Top = 128
    object QrSum04Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum04AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum04AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum04: TDataSource
    DataSet = QrSum04
    Left = 1272
    Top = 176
  end
  object QrItens05: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1476
    Top = 292
    object QrItens05Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens05Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens05AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens05AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens05: TDataSource
    DataSet = QrItens05
    Left = 1476
    Top = 340
  end
  object QrSum05: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1524
    Top = 132
    object QrSum05Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum05AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum05AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSum05: TDataSource
    DataSet = QrSum05
    Left = 1524
    Top = 180
  end
  object QrItens06: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 408
    Top = 568
    object QrItens06Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens06Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens06AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens06AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens06: TDataSource
    DataSet = QrItens06
    Left = 408
    Top = 616
  end
  object QrSum06: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 456
    Top = 408
    object QrSum06Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum06AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum06AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum06: TDataSource
    DataSet = QrSum06
    Left = 456
    Top = 456
  end
  object QrSumT_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1592
    Top = 248
    object QrSumT_Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumT_AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumT_AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumT_FALTA_PECA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FALTA_PECA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSumT_FALTA_AREA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FALTA_AREA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSumT_IntOuPart: TFloatField
      FieldName = 'IntOuPart'
    end
  end
  object DsSumT: TDataSource
    DataSet = QrSumT
    Left = 1632
    Top = 276
  end
  object QrAll: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'ORDER BY Controle DESC')
    Left = 1632
    Top = 320
    object QrAllControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrAllPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrAllAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAllAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAllBox: TIntegerField
      FieldName = 'Box'
    end
    object QrAllVSPaClaIts: TIntegerField
      FieldName = 'VSPaClaIts'
    end
    object QrAllVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrAllVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrAllVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrAllSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrAllNO_MARTELO: TWideStringField
      FieldName = 'NO_MARTELO'
      Size = 60
    end
  end
  object DsAll: TDataSource
    DataSet = QrAll
    Left = 1632
    Top = 368
  end
  object PMItens01: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 220
    object AdicionarPallet01: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet01: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet01: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados1: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens02: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 264
    object AdicionarPallet02: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet02: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet02: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados2: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object QrSumPal01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 464
    Top = 240
    object QrSumPal01Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal01AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal01AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal01: TDataSource
    DataSet = QrSumPal01
    Left = 464
    Top = 288
  end
  object QrSumPal02: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 732
    Top = 240
    object QrSumPal02Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal02AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal02AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal02: TDataSource
    DataSet = QrSumPal02
    Left = 732
    Top = 288
  end
  object QrSumPal03: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 992
    Top = 232
    object QrSumPal03Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal03AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal03AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal03: TDataSource
    DataSet = QrSumPal03
    Left = 992
    Top = 280
  end
  object QrSumPal04: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1272
    Top = 224
    object QrSumPal04Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal04AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal04AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal04: TDataSource
    DataSet = QrSumPal04
    Left = 1272
    Top = 272
  end
  object QrSumPal05: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1524
    Top = 224
    object QrSumPal05Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal05AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal05AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal05: TDataSource
    DataSet = QrSumPal05
    Left = 1524
    Top = 272
  end
  object QrSumPal06: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 456
    Top = 504
    object QrSumPal06Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal06AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal06AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal06: TDataSource
    DataSet = QrSumPal06
    Left = 456
    Top = 552
  end
  object QrSumVMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1632
    Top = 516
    object QrSumVMIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object PMItens03: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 308
    object AdicionarPallet03: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet03: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet03: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados3: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens04: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 352
    object AdicionarPallet04: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet04: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet04: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados4: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens05: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 396
    object AdicionarPallet05: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet05: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet05: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados5: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens06: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 440
    object AdicionarPallet06: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet06: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet06: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados6: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object QrRevisores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 1636
    Top = 420
    object QrRevisoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRevisoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsRevisores: TDataSource
    DataSet = QrRevisores
    Left = 1636
    Top = 464
  end
  object QrDigitadores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 1716
    Top = 424
    object QrDigitadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDigitadoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsDigitadores: TDataSource
    DataSet = QrDigitadores
    Left = 1716
    Top = 468
  end
  object QrVMIsDePal: TmySQLQuery
    Database = Dmod.MyDB
    Left = 1724
    Top = 232
    object QrVMIsDePalVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
  end
  object PMAll: TPopupMenu
    Left = 1600
    Top = 728
    object Alteraitematual1: TMenuItem
      Caption = '&Altera item atual'
      OnClick = Alteraitematual1Click
    end
    object Excluiitematual1: TMenuItem
      Caption = '&Exclui item atual'
      OnClick = Excluiitematual1Click
    end
  end
  object QrVSMrtCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsmrtcad'
      'ORDER BY Nome')
    Left = 36
    Top = 312
    object QrVSMrtCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMrtCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSMrtCad: TDataSource
    DataSet = QrVSMrtCad
    Left = 36
    Top = 356
  end
  object QrSumDest1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 316
  end
  object QrSumSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 96
    Top = 364
  end
  object QrVMISorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 176
    Top = 316
  end
  object QrPalSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 176
    Top = 364
  end
  object QrNotaAll: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 12
    Top = 216
    object QrNotaAllPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaAllAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaAll: TDataSource
    DataSet = QrNotaAll
    Left = 12
    Top = 260
  end
  object QrNotaCrr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 72
    Top = 216
    object QrNotaCrrPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaCrrAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaCrr: TDataSource
    DataSet = QrNotaCrr
    Left = 72
    Top = 260
  end
  object QrNotas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cia.SubClass, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaM2,'
      'SUM(cia.AreaM2) / 27.0034 PercM2 '
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vdi ON vdi.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vdc ON vdc.Codigo=vdi.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vdc.Codigo=1'
      'GROUP BY cia.SubClass')
    Left = 132
    Top = 216
    object QrNotasSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrNotasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotasAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasNotaM2: TFloatField
      FieldName = 'NotaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasPercM2: TFloatField
      FieldName = 'PercM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsNotas: TDataSource
    DataSet = QrNotas
    Left = 132
    Top = 260
  end
  object QrSumT: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSumTCalcFields
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1632
    Top = 232
    object QrSumTPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumTAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumTSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrSumTSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumTJaFoi_PECA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JaFoi_PECA'
      Calculated = True
    end
    object QrSumTJaFoi_AREA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JaFoi_AREA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object PMIMEI: TPopupMenu
    Left = 588
    Top = 836
    object ImprimirfluxodemovimentodoIMEI2: TMenuItem
      Caption = 'Imprimir fluxo de movimento do IMEI'
      OnClick = ImprimirfluxodemovimentodoIMEI2Click
    end
  end
  object QrVSPallet07: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet07AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 676
    Top = 385
    object QrVSPallet07FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet07Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet07Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet07Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet07DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet07DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet07UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet07UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet07AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet07Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet07Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet07Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet07CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet07GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet07NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet07NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet07NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet07QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet07: TDataSource
    DataSet = QrVSPallet07
    Left = 676
    Top = 429
  end
  object QrVSPalClaIts07: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 676
    Top = 476
    object QrVSPalClaIts07VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts07VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts07Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts07DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts07: TDataSource
    DataSet = QrVSPalClaIts07
    Left = 676
    Top = 524
  end
  object QrItens07: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 676
    Top = 568
    object QrItens07Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens07Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens07AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens07AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens07: TDataSource
    DataSet = QrItens07
    Left = 676
    Top = 616
  end
  object QrSum07: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 724
    Top = 408
    object QrSum07Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum07AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum07AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum07: TDataSource
    DataSet = QrSum07
    Left = 724
    Top = 456
  end
  object QrSumPal07: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 724
    Top = 504
    object QrSumPal07Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal07AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal07AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal07: TDataSource
    DataSet = QrSumPal07
    Left = 724
    Top = 552
  end
  object QrVSPallet08: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet08AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 940
    Top = 389
    object QrVSPallet08FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet08Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet08Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet08Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet08DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet08DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet08UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet08UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet08AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet08Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet08Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet08Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet08CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet08GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet08NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet08NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet08NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet08QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet08: TDataSource
    DataSet = QrVSPallet08
    Left = 940
    Top = 433
  end
  object QrVSPalClaIts08: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 940
    Top = 480
    object QrVSPalClaIts08VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts08VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts08Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts08DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts08: TDataSource
    DataSet = QrVSPalClaIts08
    Left = 940
    Top = 528
  end
  object QrItens08: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 940
    Top = 572
    object QrItens08Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens08Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens08AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens08AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens08: TDataSource
    DataSet = QrItens08
    Left = 940
    Top = 620
  end
  object QrSum08: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 988
    Top = 412
    object QrSum08Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum08AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum08AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum08: TDataSource
    DataSet = QrSum08
    Left = 988
    Top = 460
  end
  object QrSumPal08: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 988
    Top = 508
    object QrSumPal08Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal08AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal08AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal08: TDataSource
    DataSet = QrSumPal08
    Left = 988
    Top = 556
  end
  object QrVSPallet09: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet09AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1224
    Top = 377
    object QrVSPallet09FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet09Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet09Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet09Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet09DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet09DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet09UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet09UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet09AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet09Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet09Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet09Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet09CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet09GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet09NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet09NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet09NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet09QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet09: TDataSource
    DataSet = QrVSPallet09
    Left = 1224
    Top = 421
  end
  object QrVSPalClaIts09: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1224
    Top = 468
    object QrVSPalClaIts09VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts09VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts09Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts09DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts09: TDataSource
    DataSet = QrVSPalClaIts09
    Left = 1224
    Top = 516
  end
  object QrItens09: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1224
    Top = 560
    object QrItens09Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens09Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens09AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens09AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens09: TDataSource
    DataSet = QrItens09
    Left = 1224
    Top = 608
  end
  object QrSum09: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1272
    Top = 400
    object QrSum09Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum09AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum09AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum09: TDataSource
    DataSet = QrSum09
    Left = 1272
    Top = 448
  end
  object QrSumPal09: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1272
    Top = 496
    object QrSumPal09Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal09AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal09AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal09: TDataSource
    DataSet = QrSumPal09
    Left = 1272
    Top = 544
  end
  object QrVSPallet10: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet10AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1480
    Top = 385
    object QrVSPallet10FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet10Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet10Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet10Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet10DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet10DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet10UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet10UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet10AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet10Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet10Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet10Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet10CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet10GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet10NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet10NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet10NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet10QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet10: TDataSource
    DataSet = QrVSPallet10
    Left = 1480
    Top = 429
  end
  object QrVSPalClaIts10: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1480
    Top = 476
    object QrVSPalClaIts10VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts10VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts10Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts10DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts10: TDataSource
    DataSet = QrVSPalClaIts10
    Left = 1480
    Top = 524
  end
  object QrItens10: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1480
    Top = 568
    object QrItens10Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens10Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens10AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens10AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens10: TDataSource
    DataSet = QrItens10
    Left = 1480
    Top = 616
  end
  object QrSum10: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1528
    Top = 408
    object QrSum10Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum10AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum10AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum10: TDataSource
    DataSet = QrSum10
    Left = 1528
    Top = 456
  end
  object QrSumPal10: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1528
    Top = 504
    object QrSumPal10Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal10AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal10AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal10: TDataSource
    DataSet = QrSumPal10
    Left = 1528
    Top = 552
  end
  object QrVSPallet11: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet11AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 408
    Top = 661
    object QrVSPallet11FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet11Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet11Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet11Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet11DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet11DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet11UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet11UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet11AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet11Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet11Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet11Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet11CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet11GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet11NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet11NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet11NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet11QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet11: TDataSource
    DataSet = QrVSPallet11
    Left = 408
    Top = 705
  end
  object QrVSPalClaIts11: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 408
    Top = 752
    object QrVSPalClaIts11VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts11VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts11Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts11DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts11: TDataSource
    DataSet = QrVSPalClaIts11
    Left = 408
    Top = 800
  end
  object QrItens11: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 408
    Top = 844
    object QrItens11Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens11Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens11AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens11AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens11: TDataSource
    DataSet = QrItens11
    Left = 408
    Top = 892
  end
  object QrSum11: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 456
    Top = 684
    object QrSum11Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum11AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum11AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum11: TDataSource
    DataSet = QrSum11
    Left = 456
    Top = 732
  end
  object QrSumPal11: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 456
    Top = 780
    object QrSumPal11Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal11AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal11AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal11: TDataSource
    DataSet = QrSumPal11
    Left = 456
    Top = 828
  end
  object QrVSPallet12: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet12AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 676
    Top = 661
    object QrVSPallet12FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet12Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet12Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet12Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet12DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet12DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet12UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet12UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet12AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet12Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet12Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet12Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet12CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet12GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet12NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet12NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet12NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet12QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet12: TDataSource
    DataSet = QrVSPallet12
    Left = 676
    Top = 705
  end
  object QrVSPalClaIts12: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 676
    Top = 752
    object QrVSPalClaIts12VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts12VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts12Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts12DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts12: TDataSource
    DataSet = QrVSPalClaIts12
    Left = 676
    Top = 800
  end
  object QrItens12: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 676
    Top = 844
    object QrItens12Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens12Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens12AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens12AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens12: TDataSource
    DataSet = QrItens12
    Left = 676
    Top = 892
  end
  object QrSum12: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 724
    Top = 684
    object QrSum12Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum12AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum12AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum12: TDataSource
    DataSet = QrSum12
    Left = 724
    Top = 732
  end
  object QrSumPal12: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 724
    Top = 780
    object QrSumPal12Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal12AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal12AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal12: TDataSource
    DataSet = QrSumPal12
    Left = 724
    Top = 828
  end
  object QrVSPallet13: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet13AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 944
    Top = 657
    object QrVSPallet13FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet13Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet13Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet13Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet13DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet13DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet13UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet13UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet13AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet13Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet13Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet13Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet13CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet13GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet13NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet13NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet13NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet13QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet13: TDataSource
    DataSet = QrVSPallet13
    Left = 944
    Top = 701
  end
  object QrVSPalClaIts13: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 944
    Top = 748
    object QrVSPalClaIts13VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts13VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts13Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts13DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts13: TDataSource
    DataSet = QrVSPalClaIts13
    Left = 944
    Top = 796
  end
  object QrItens13: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 944
    Top = 840
    object QrItens13Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens13Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens13AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens13AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens13: TDataSource
    DataSet = QrItens13
    Left = 944
    Top = 888
  end
  object QrSum13: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 992
    Top = 680
    object QrSum13Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum13AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum13AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum13: TDataSource
    DataSet = QrSum13
    Left = 992
    Top = 728
  end
  object QrSumPal13: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 992
    Top = 776
    object QrSumPal13Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal13AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal13AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal13: TDataSource
    DataSet = QrSumPal13
    Left = 992
    Top = 824
  end
  object QrVSPallet14: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet14AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1228
    Top = 657
    object QrVSPallet14FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet14Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet14Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet14Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet14DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet14DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet14UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet14UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet14AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet14Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet14Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet14Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet14CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet14GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet14NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet14NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet14NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet14QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet14: TDataSource
    DataSet = QrVSPallet14
    Left = 1228
    Top = 701
  end
  object QrVSPalClaIts14: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1228
    Top = 748
    object QrVSPalClaIts14VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts14VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts14Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts14DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts14: TDataSource
    DataSet = QrVSPalClaIts14
    Left = 1228
    Top = 796
  end
  object QrItens14: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1228
    Top = 840
    object QrItens14Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens14Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens14AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens14AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens14: TDataSource
    DataSet = QrItens14
    Left = 1228
    Top = 888
  end
  object QrSum14: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1276
    Top = 680
    object QrSum14Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum14AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum14AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum14: TDataSource
    DataSet = QrSum14
    Left = 1276
    Top = 728
  end
  object QrSumPal14: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1276
    Top = 776
    object QrSumPal14Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal14AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal14AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal14: TDataSource
    DataSet = QrSumPal14
    Left = 1276
    Top = 824
  end
  object QrVSPallet15: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet15AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1484
    Top = 661
    object QrVSPallet15FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet15Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet15Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet15Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet15DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet15DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet15UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet15UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet15AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet15Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet15Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet15Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet15CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet15GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet15NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet15NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet15NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet15QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet15: TDataSource
    DataSet = QrVSPallet15
    Left = 1484
    Top = 705
  end
  object QrVSPalClaIts15: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1484
    Top = 752
    object QrVSPalClaIts15VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts15VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts15Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts15DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts15: TDataSource
    DataSet = QrVSPalClaIts15
    Left = 1484
    Top = 800
  end
  object QrItens15: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1484
    Top = 844
    object QrItens15Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens15Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens15AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens15AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens15: TDataSource
    DataSet = QrItens15
    Left = 1484
    Top = 892
  end
  object QrSum15: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1532
    Top = 684
    object QrSum15Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum15AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum15AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum15: TDataSource
    DataSet = QrSum15
    Left = 1532
    Top = 732
  end
  object QrSumPal15: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1532
    Top = 780
    object QrSumPal15Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal15AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal15AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal15: TDataSource
    DataSet = QrSumPal15
    Left = 1532
    Top = 828
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 792
    Top = 224
    object EstaOCOrdemdeclassificao1: TMenuItem
      Caption = 'Encerra esta OC (Ordem de classifica'#231#227'o)'
      OnClick = EstaOCOrdemdeclassificao1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Palletdobox01: TMenuItem
      Tag = 1
      Caption = 'Encerra o pallet do box 01'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox02: TMenuItem
      Tag = 2
      Caption = 'Encerra o pallet do box 02'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox03: TMenuItem
      Tag = 3
      Caption = 'Encerra o pallet do box 03'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox04: TMenuItem
      Tag = 4
      Caption = 'Encerra o pallet do box 04'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox05: TMenuItem
      Tag = 5
      Caption = 'Encerra o pallet do box 05'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox06: TMenuItem
      Tag = 6
      Caption = 'Encerra o pallet do box 06'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox07: TMenuItem
      Tag = 7
      Caption = 'Encerra o pallet do box 07'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox08: TMenuItem
      Tag = 8
      Caption = 'Encerra o pallet do box 08'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox09: TMenuItem
      Tag = 9
      Caption = 'Encerra o pallet do box 09'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox10: TMenuItem
      Tag = 10
      Caption = 'Encerra o pallet do box 10'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox11: TMenuItem
      Tag = 11
      Caption = 'Encerra o pallet do box 11'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox12: TMenuItem
      Tag = 12
      Caption = 'Encerra o pallet do box 12'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox13: TMenuItem
      Tag = 13
      Caption = 'Encerra o pallet do box 13'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox14: TMenuItem
      Tag = 14
      Caption = 'Encerra o pallet do box 14'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox15: TMenuItem
      Tag = 15
      Caption = 'Encerra o pallet do box 15'
      OnClick = EncerrarPalletClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Mostrartodosboxes1: TMenuItem
      Caption = 'Mostrar todos boxes'
      OnClick = Mostrartodosboxes1Click
    end
    object ImprimirNmeroPallet1: TMenuItem
      Caption = 'Imprimir N'#250'mero Pallet'
      Enabled = False
      OnClick = ImprimirNmeroPallet1Click
    end
    object ImprimirfluxodemovimentodoIMEI1: TMenuItem
      Caption = 'Imprimir fluxo de movimento do IMEI'
      OnClick = ImprimirfluxodemovimentodoIMEI1Click
    end
    object DadosPaletsemWordPad1: TMenuItem
      Caption = 'Dados Palets no Paint'
      OnClick = DadosPaletsemWordPad1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object rocarVichaRMP1: TMenuItem
      Caption = 'Trocar Ficha RMP'
      Enabled = False
      OnClick = rocarVichaRMP1Click
    end
    object rocarIMEI1: TMenuItem
      Caption = 'Trocar IME-I'
      OnClick = rocarIMEI1Click
    end
  end
end
