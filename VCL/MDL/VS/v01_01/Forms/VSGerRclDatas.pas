unit VSGerRclDatas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit,
  dmkEditDateTimePicker, UnProjGroup_Consts, mySQLDbTables, UnGrl_Consts;

type
  TFmVSGerRclDatas = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label11: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    TPDtHrLibCla: TdmkEditDateTimePicker;
    EdDtHrLibCla: TdmkEdit;
    TPDtHrCfgCla: TdmkEditDateTimePicker;
    EdDtHrCfgCla: TdmkEdit;
    TPDtHrFimCla: TdmkEditDateTimePicker;
    EdDtHrFimCla: TdmkEdit;
    QrVSPaRclCab: TmySQLQuery;
    QrVSPaRclCabCodigo: TIntegerField;
    QrVSPaRclCabVSGerRcl: TIntegerField;
    QrVSPaRclCabVSMovIts: TIntegerField;
    QrVSPaRclCabCacCod: TIntegerField;
    QrVSPaRclCabLstPal01: TIntegerField;
    QrVSPaRclCabLstPal02: TIntegerField;
    QrVSPaRclCabLstPal03: TIntegerField;
    QrVSPaRclCabLstPal04: TIntegerField;
    QrVSPaRclCabLstPal05: TIntegerField;
    QrVSPaRclCabLstPal06: TIntegerField;
    QrVSPaRclCabLstPal07: TIntegerField;
    QrVSPaRclCabLstPal08: TIntegerField;
    QrVSPaRclCabLstPal09: TIntegerField;
    QrVSPaRclCabLstPal10: TIntegerField;
    QrVSPaRclCabLstPal11: TIntegerField;
    QrVSPaRclCabLstPal12: TIntegerField;
    QrVSPaRclCabLstPal13: TIntegerField;
    QrVSPaRclCabLstPal14: TIntegerField;
    QrVSPaRclCabLstPal15: TIntegerField;
    QrVSPaRclCabDtHrFimCla: TDateTimeField;
    QrVSPaRclCabLk: TIntegerField;
    QrVSPaRclCabDataCad: TDateField;
    QrVSPaRclCabDataAlt: TDateField;
    QrVSPaRclCabUserCad: TIntegerField;
    QrVSPaRclCabUserAlt: TIntegerField;
    QrVSPaRclCabAlterWeb: TSmallintField;
    QrVSPaRclCabAtivo: TSmallintField;
    QrVSPaRclCabDtHrFimCla_TXT: TWideStringField;
    QrVSPaRclCabFatorInt: TFloatField;
    QrVSPaRclIts: TmySQLQuery;
    QrVSPaRclItsFatorInt: TFloatField;
    QrVSPaRclItsCodigo: TIntegerField;
    QrVSPaRclItsControle: TIntegerField;
    QrVSPaRclItsVSPallet: TIntegerField;
    QrVSPaRclItsVMI_Sorc: TIntegerField;
    QrVSPaRclItsVMI_Baix: TIntegerField;
    QrVSPaRclItsVMI_Dest: TIntegerField;
    QrVSPaRclItsTecla: TIntegerField;
    QrVSPaRclItsDtHrIni: TDateTimeField;
    QrVSPaRclItsDtHrFim: TDateTimeField;
    QrVSPaRclItsLk: TIntegerField;
    QrVSPaRclItsDataCad: TDateField;
    QrVSPaRclItsDataAlt: TDateField;
    QrVSPaRclItsUserCad: TIntegerField;
    QrVSPaRclItsUserAlt: TIntegerField;
    QrVSPaRclItsAlterWeb: TSmallintField;
    QrVSPaRclItsAtivo: TSmallintField;
    QrVSPaRclItsDtHrFim_TXT: TWideStringField;
    QrVSPaMulCab: TmySQLQuery;
    QrVSPaMulCabCodigo: TIntegerField;
    QrVSPaMulCabMovimCod: TIntegerField;
    QrVSPaMulCabCacCod: TIntegerField;
    QrVSPaMulCabEmpresa: TIntegerField;
    QrVSPaMulCabVMI_Sorc: TIntegerField;
    QrVSPaMulCabDataHora: TDateTimeField;
    QrVSPaMulCabPecas: TFloatField;
    QrVSPaMulCabPesoKg: TFloatField;
    QrVSPaMulCabAreaM2: TFloatField;
    QrVSPaMulCabAreaP2: TFloatField;
    QrVSPaMulCabValorT: TFloatField;
    QrVSPaMulCabVSGerArt: TIntegerField;
    QrVSPaMulCabPallet: TIntegerField;
    QrVSPaMulCabTemIMEIMrt: TSmallintField;
    QrVSPaMulCabSerieRem: TSmallintField;
    QrVSPaMulCabNFeRem: TIntegerField;
    QrVSPaMulCabLk: TIntegerField;
    QrVSPaMulCabDataCad: TDateField;
    QrVSPaMulCabDataAlt: TDateField;
    QrVSPaMulCabUserCad: TIntegerField;
    QrVSPaMulCabUserAlt: TIntegerField;
    QrVSPaMulCabAlterWeb: TSmallintField;
    QrVSPaMulCabAtivo: TSmallintField;
    QrVSPaMulCabFatorInt: TFloatField;
    QrVSPaMulIts: TmySQLQuery;
    QrVSPaMulItsCodigo: TLargeintField;
    QrVSPaMulItsControle: TLargeintField;
    QrVSPaMulItsMovimCod: TLargeintField;
    QrVSPaMulItsMovimNiv: TLargeintField;
    QrVSPaMulItsMovimTwn: TLargeintField;
    QrVSPaMulItsEmpresa: TLargeintField;
    QrVSPaMulItsTerceiro: TLargeintField;
    QrVSPaMulItsCliVenda: TLargeintField;
    QrVSPaMulItsMovimID: TLargeintField;
    QrVSPaMulItsDataHora: TDateTimeField;
    QrVSPaMulItsPallet: TLargeintField;
    QrVSPaMulItsGraGruX: TLargeintField;
    QrVSPaMulItsPecas: TFloatField;
    QrVSPaMulItsPesoKg: TFloatField;
    QrVSPaMulItsAreaM2: TFloatField;
    QrVSPaMulItsAreaP2: TFloatField;
    QrVSPaMulItsValorT: TFloatField;
    QrVSPaMulItsSrcMovID: TLargeintField;
    QrVSPaMulItsSrcNivel1: TLargeintField;
    QrVSPaMulItsSrcNivel2: TLargeintField;
    QrVSPaMulItsSrcGGX: TLargeintField;
    QrVSPaMulItsSdoVrtPeca: TFloatField;
    QrVSPaMulItsSdoVrtPeso: TFloatField;
    QrVSPaMulItsSdoVrtArM2: TFloatField;
    QrVSPaMulItsObserv: TWideStringField;
    QrVSPaMulItsSerieFch: TLargeintField;
    QrVSPaMulItsFicha: TLargeintField;
    QrVSPaMulItsMisturou: TLargeintField;
    QrVSPaMulItsFornecMO: TLargeintField;
    QrVSPaMulItsCustoMOKg: TFloatField;
    QrVSPaMulItsCustoMOM2: TFloatField;
    QrVSPaMulItsCustoMOTot: TFloatField;
    QrVSPaMulItsValorMP: TFloatField;
    QrVSPaMulItsDstMovID: TLargeintField;
    QrVSPaMulItsDstNivel1: TLargeintField;
    QrVSPaMulItsDstNivel2: TLargeintField;
    QrVSPaMulItsDstGGX: TLargeintField;
    QrVSPaMulItsQtdGerPeca: TFloatField;
    QrVSPaMulItsQtdGerPeso: TFloatField;
    QrVSPaMulItsQtdGerArM2: TFloatField;
    QrVSPaMulItsQtdGerArP2: TFloatField;
    QrVSPaMulItsQtdAntPeca: TFloatField;
    QrVSPaMulItsQtdAntPeso: TFloatField;
    QrVSPaMulItsQtdAntArM2: TFloatField;
    QrVSPaMulItsQtdAntArP2: TFloatField;
    QrVSPaMulItsNotaMPAG: TFloatField;
    QrVSPaMulItsNO_PALLET: TWideStringField;
    QrVSPaMulItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPaMulItsNO_TTW: TWideStringField;
    QrVSPaMulItsID_TTW: TLargeintField;
    QrVSPaMulItsNO_FORNECE: TWideStringField;
    QrVSPaMulItsNO_SerieFch: TWideStringField;
    QrVSPaMulItsReqMovEstq: TLargeintField;
    QrVSPaMulItsStqCenLoc: TLargeintField;
    QrVSPaMulItsDtCorrApo: TDateTimeField;
    QrVMIs: TmySQLQuery;
    QrVMIsVMI: TIntegerField;
    QrVMIsDataHora: TDateTimeField;
    PB1: TProgressBar;
    Label23: TLabel;
    TPDtHrAberto_: TdmkEditDateTimePicker;
    EdDtHrAberto_: TdmkEdit;
    QrVMIsTipo: TWideStringField;
    QrVM2: TmySQLQuery;
    QrVM2VMI: TIntegerField;
    QrVM2DataHora: TDateTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrVSPaRclCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaRclCabBeforeClose(DataSet: TDataSet);
    procedure QrVSPaMulCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaMulCabBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenVSPaRclCab();
    procedure ReopenVSPaRclIts();
    procedure ReopenVSPaMulCab();
    procedure ReopenVSPaMulIts(Controle: Integer);
  public
    { Public declarations }
    FCodigo, FMovimCod, FMovimID: Integer;
    FDtHrCfgCla, FDtHrFimCla: TDateTime;
  end;

  var
  FmVSGerRclDatas: TFmVSGerRclDatas;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, UnVS_CRC_PF, UnVS_PF;

{$R *.DFM}

procedure TFmVSGerRclDatas.BtOKClick(Sender: TObject);
var
  //DtHrAberto,
  DataHora, DtHrLibCla, DtHrCfgCla, DtHrFimCla, DtHrIni, DtHrFim: String;
  Codigo, Controle: Integer;
  MovimCod, MovimID: Integer;
  NewCfgCla, DifCfgCla, NewFimCla, DifFimCla, HrIni, HrFim: TDateTime;
  SQLType: TSQLType;
  Imeis: String;
begin
  SQLType        := stUpd;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando geração do artigo!');
  Codigo         := FCodigo;
  //DtHrAberto     := Geral.FDT_TP_Ed(TPDtHrAberto.Date, EdDtHrAberto.Text);
  DtHrLibCla     := Geral.FDT_TP_Ed(TPDtHrLibCla.Date, EdDtHrLibCla.Text);
  DtHrCfgCla     := Geral.FDT_TP_Ed(TPDtHrCfgCla.Date, EdDtHrCfgCla.Text);
  DtHrFimCla     := Geral.FDT_TP_Ed(TPDtHrFimCla.Date, EdDtHrFimCla.Text);
  //
  MovimCod := FMovimCod;
  MovimID  := FMovimID;
  //DataHora := DtHrAberto;
  DataHora := DtHrLibCla;
  //
  NewCfgCla      := Trunc(TPDtHrCfgCla.Date) + EdDtHrCfgCla.ValueVariant;
  DifCfgCla      := NewCfgCla - FDtHrCfgCla;
  //
  NewFimCla      := Trunc(TPDtHrFimCla.Date) + EdDtHrFimCla.ValueVariant;
  DifFimCla      := NewFimCla - FDtHrFimCla;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  CO_DATA_HORA_VMI], [
  'Codigo', 'MovimCod', 'MovimID'], [DataHora], [
  Codigo, MovimCod, MovimID], True);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerrcla', False, [
  //'DtHrAberto',
  'DtHrLibCla', 'DtHrCfgCla', 'DtHrFimCla'], [
  'Codigo'], [
  //DtHrAberto,
  DtHrLibCla, DtHrCfgCla, DtHrFimCla], [
  Codigo], True) then
  begin
    ////////////////////////////// Classe Couro a Couro ////////////////////////
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando classificação do artigo!');
    ReopenVSPaRclCab();
    while not QrVSPaRclCab.Eof do
    begin
      Codigo := QrVSPaRclCabCodigo.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsparclcaba', False, [
      'DtHrFimCla'], ['Codigo'], [DtHrFimCla], [Codigo], True);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrVM2, Dmod.MyDB, [
      'SELECT Controle VMI, DataHora',
      'FROM ' + CO_SEL_TAB_VMI + '',
      'WHERE MovimID=15',
      'AND Controle=' + Geral.FF0(QrVSPaRclCabVSMovIts.Value),
      'OR DstNivel2=' + Geral.FF0(QrVSPaRclCabVSMovIts.Value),
      '']);
      //
      PB1.Position := 0;
      PB1.Max := QrVM2.RecordCount;
      QrVM2.First;
      Imeis := '';
      while not QrVM2.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Atualizando IMEIs de preparação!');
        Controle := QrVM2VMI.Value;
        HrIni    := QrVM2DataHora.Value + DifCfgCla;
        DataHora := Geral.FDT(HrIni, 109);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, CO_SEL_TAB_VMI, False, [
        'DataHora'], ['Controle'], [DataHora], [Controle], True);
        //
        QrVM2.Next;
      end;
      //
      PB1.Position := 0;
      PB1.Max := QrVSPaRclIts.RecordCount;
      QrVSPaRclIts.First;
      while not QrVSPaRclIts.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Atualizando classificação do artigo!');
        Controle := QrVSPaRclItsControle.Value;
        HrIni    := QrVSPaRclItsDtHrIni.Value + DifCfgCla;
        DtHrIni  := Geral.FDT(HrIni, 109);
        if QrVSPaRclItsDtHrFim.Value > 2 then
          HrFim    := QrVSPaRclItsDtHrFim.Value + DifCfgCla
        else
          HrFim    := 0;
        //
        DtHrFim  := Geral.FDT(HrFim, 109);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsparclitsa', False, [
        'DtHrIni', 'DtHrFim'], ['Controle'], [
        DtHrIni, DtHrFim], [Controle], True);
        //
        QrVSPaRclIts.Next;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando IMEIs gerados!');
      UnDmkDAC_PF.AbreMySQLQuery0(QrVMIs, Dmod.MyDB, [
      'SELECT DISTINCT pri.VMI_Baix VMI, vmi.DataHora, "Baix" Tipo ',
      'FROM vsparclitsa pri ',
      'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pri.VMI_Baix ',
      'WHERE pri.Codigo=' + Geral.FF0(QrVSPaRclCabCodigo.Value),
      ' ',
      'UNION ',
      ' ',
      'SELECT DISTINCT pri.VMI_Dest VMI, vmi.DataHora, "Dest" Tipo ',
      'FROM vsparclitsa pri ',
      'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pri.VMI_Dest ',
      'WHERE pri.Codigo=' + Geral.FF0(QrVSPaRclCabCodigo.Value),
      ' ',
      'ORDER BY VMI ',
      '']);
      //
      PB1.Position := 0;
      PB1.Max := QrVMIs.RecordCount;
      QrVMIs.First;
      Imeis := '';
      while not QrVMIs.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Atualizando IMEIs gerados!');
        Controle := QrVMIsVMI.Value;
        HrIni    := QrVMIsDataHora.Value + DifCfgCla;
        DataHora := Geral.FDT(HrIni, 109);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, CO_INS_TAB_VMI, False, [
        'DataHora'], ['Controle'], [DataHora], [Controle], True);
        //
{
        if Lowercase(QrVMIsTipo.Value) = Lowercase('Baix') then
        begin
          if Imeis <> '' then
            Imeis := ', ';
          Imeis := Imeis + Geral.FF0(Controle) + sLineBreak;
        end;
        //Geral.MB_Info(Geral.FF0(Controle) + ' - ' + QrVMIsTipo.Value);
}
        QrVMIs.Next;
      end;
{
      if Imeis <> '' then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrVM2, Dmod.MyDB, [
        'SELECT Controle, DataHora',
        'FROM ' + CO_SEL_TAB_VMI + '',
        'WHERE MovimID=15',
        'AND Controle IN (' + Imeis + ')',
        'OR DstNivel2 IN (' + Imeis + ')',
        '']);
        //
        PB1.Position := 0;
        PB1.Max := QrVM2.RecordCount;
        QrVM2.First;
        Imeis := '';
        while not QrVM2.Eof do
        begin
          MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Atualizando IMEIs gerados!');
          Controle := QrVM2VMI.Value;
          HrIni    := QrVM2DataHora.Value + DifCfgCla;
          DataHora := Geral.FDT(HrIni, 109);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, '' + CO_SEL_TAB_VMI + '', False, [
          'DataHora'], ['Controle'], [DataHora], [Controle], True);
          //
          QrVM2.Next;
        end;
        //
       end;
}
      //
      QrVSPaRclCab.Next;
    end;
    //
    /////////////////////////////////// Classe Massiva /////////////////////////
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando classificação massiva!');
    // Já faz manual!
{
    ReopenVSPaMulCab();
    while not QrVSPaRclCab.Eof do
    begin
      Codigo := QrVSPaRclCabCodigo.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vspamulcaba', False, [
      'DtHrFimCla'], ['Codigo'], [DtHrFimCla], [Codigo], True);
      //
      PB1.Position := 0;
      PB1.Max := QrVSPaRclIts.RecordCount;
      QrVSPaRclIts.First;
}
    //
    //
    //////////////////////////////////////// FIM  /////////////////////////
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Close;
  end;
end;

procedure TFmVSGerRclDatas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerRclDatas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSGerRclDatas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSGerRclDatas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerRclDatas.QrVSPaRclCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPaRclIts();
end;

procedure TFmVSGerRclDatas.QrVSPaRclCabBeforeClose(DataSet: TDataSet);
begin
  QrVSPaRclIts.Close;
end;

procedure TFmVSGerRclDatas.QrVSPaMulCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPaMulIts(0);
end;

procedure TFmVSGerRclDatas.QrVSPaMulCabBeforeClose(DataSet: TDataSet);
begin
  QrVSPaMulIts.Close;
end;

procedure TFmVSGerRclDatas.ReopenVSPaRclCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaRclCab, Dmod.MyDB, [
  'SELECT pra.*, IF(pra.DtHrFimCla < "1900-01-01", "",  ',
  '  DATE_FORMAT(pra.DtHrFimCla, "%d/%m/%y %h:%i:%s")) DtHrFimCla_TXT,',
///// Usa apenas CO_SEL_TAB_VMI porque usa cn1.FatorInt apenas para corrigir baixas!
  'cn1.FatorInt  ',
  'FROM vsparclcaba pra',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pra.VSMovIts',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
////////////////////////////////////////////////////////////////////////////////
  'WHERE pra.VSGerRcl=' + Geral.FF0(FCodigo),
  'ORDER BY pra.Codigo ',
  '']);
end;

procedure TFmVSGerRclDatas.ReopenVSPaRclIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaRclIts, Dmod.MyDB, [
  'SELECT pri.*, IF(DtHrFim < "1900-01-01", "",  ',
  '  DATE_FORMAT(DtHrFim, "%d/%m/%y %h:%i:%s")) DtHrFim_TXT, ',
  'cn1.FatorInt  ',
  'FROM vsparclitsa pri ',
  'LEFT JOIN vspalleta  let ON let.Codigo=pri.VSPallet',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE pri.Codigo=' + Geral.FF0(QrVSPaRclCabCodigo.Value),
  'ORDER BY pri.DtHrIni ',
  '']);
end;

procedure TFmVSGerRclDatas.ReopenVSPaMulCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaMulCab, Dmod.MyDB, [
  'SELECT pra.*,',
  'cn1.FatorInt  ',
  'FROM vspamulcabr pra',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pra.VMI_Sorc',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE pra.VSGerArt=' + Geral.FF0(FCodigo),
  'ORDER BY pra.Codigo ',
  '']);
end;

procedure TFmVSGerRclDatas.ReopenVSPaMulIts(Controle: Integer);
var
  TemIMEIMrt, MovimCod: Integer;
begin
{$IfDef sAllVS}
  TemIMEIMrt := QrVSPaMulCabTemIMEIMrt.Value;
  MovimCod   := QrVSPaMulCabMovimCod.Value;
  VS_PF.ReopenVSPaMulIts(QrVSPaMulIts, TemIMEIMrt, MovimCod, Controle);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

end.
