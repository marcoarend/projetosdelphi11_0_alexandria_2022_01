unit VSPalInClaRcl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO;

type
  TFmVSPaInClaRcl = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrPalClaRcl: TmySQLQuery;
    DsPalClaRcl: TDataSource;
    QrPalClaRclClaRcl: TLargeintField;
    QrPalClaRclCacCod: TIntegerField;
    QrPalClaRclVSMovIts: TIntegerField;
    QrPalClaRclVSPallet: TFloatField;
    QrPalClaRclNO_ClaRcl: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FPallet, FCacCod: Integer;
    //
    procedure ReopenPalClaRcl();
  end;

  var
  FmVSPaInClaRcl: TFmVSPaInClaRcl;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSPaInClaRcl.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPaInClaRcl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSPaInClaRcl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSPaInClaRcl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPaInClaRcl.ReopenPalClaRcl();
var
  SQL_OC: String;
begin
  if FCacCod <> 0 then
    SQL_OC :=   'AND CacCod <> ' + Geral.FF0(FCacCod)
  else
    SQL_OC :=   '';
  //
  UnDmkDAC_PF.AbreMySQLQUery0(QrPalClaRcl, Dmod.MyDB, [
  'SELECT 1 ClaRcl, "Classificação" NO_ClaRcl,',
  'CacCod, VSMovIts, 0.000 VSPallet  ',
  'FROM vspaclacaba ',
  'WHERE DtHrFimCla<"1900-01-01"  ',
  SQL_OC,
  'AND (' + Geral.FF0(FPallet)+ ' IN (',
    '             LstPal01, LstPal02, LstPal03, ',
    '             LstPal04, LstPal05, LstPal06, ',
    '             LstPal07, LstPal08, LstPal09, ',
    '             LstPal10, LstPal11, LstPal12, ',
    '             LstPal13, LstPal14, LstPal15) ',
  '     ) ',
  ' ',
  'UNION ',
  ' ',
  'SELECT 2 ClaRcl, "Reclassificação" NO_ClaRcl,',
  'CacCod, VSMovIts, VSPallet  ',
  'FROM vsparclcaba ',
  'WHERE DtHrFimCla<"1900-01-01"  ',
  SQL_OC,
  'AND (' + Geral.FF0(FPallet)+ ' IN (',
    '             LstPal01, LstPal02, LstPal03, ',
    '             LstPal04, LstPal05, LstPal06, ',
    '             LstPal07, LstPal08, LstPal09, ',
    '             LstPal10, LstPal11, LstPal12, ',
    '             LstPal13, LstPal14, LstPal15) ',
  '     ) ',
  '']);
end;

end.
