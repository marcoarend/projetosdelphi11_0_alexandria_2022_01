unit VSReduzConflit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmVSReduzConflit = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmVSReduzConflit: TFmVSReduzConflit;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModVS;

{$R *.DFM}

procedure TFmVSReduzConflit.BtOKClick(Sender: TObject);
var
  sControle, sGraGruX: String;
begin
  Screen.Cursor := crHourGlass;
  try
  DmModVS.QrReduzConflit.First;
  while not DmModVS.QrReduzConflit.Eof do
  begin
    sGraGruX  := Geral.FF0(DmModVS.QrReduzConflitGraGruX.Value);
    sControle := Geral.FF0(DmModVS.QrReduzConflitSrcCtrl.Value);
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      ' UPDATE vsmovits SET DstGGX=' + sGraGruX +
      ' WHERE Controle=' + sControle);
    //
    DmModVS.QrReduzConflit.Next;
  end;
  DmModVS.QrReduzConflit.Close;
  DmModVS.QrReduzConflit.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSReduzConflit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSReduzConflit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSReduzConflit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSReduzConflit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
