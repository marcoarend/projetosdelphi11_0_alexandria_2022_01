object FmVSCfgEstqInNatEFD: TFmVSCfgEstqInNatEFD
  Left = 339
  Top = 185
  Caption = 
    'WET-CURTI-214 :: Confer'#234'ncia de Baixas x Estoque de Couro In Nat' +
    'ura'
  ClientHeight = 735
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 640
        Height = 32
        Caption = 'Confer'#234'ncia de Baixas x Estoque de Couro In Natura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 640
        Height = 32
        Caption = 'Confer'#234'ncia de Baixas x Estoque de Couro In Natura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 640
        Height = 32
        Caption = 'Confer'#234'ncia de Baixas x Estoque de Couro In Natura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 562
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 562
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 562
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 352
          Width = 1004
          Height = 5
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = 10
          ExplicitTop = 253
          ExplicitWidth = 808
        end
        object DGPsq: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 337
          Align = alClient
          DataSource = DsPsq
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
        end
        object DGBxa: TdmkDBGridZTO
          Left = 2
          Top = 357
          Width = 1004
          Height = 153
          Align = alBottom
          DataSource = DsBxa
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
        end
        object Panel5: TPanel
          Left = 2
          Top = 510
          Width = 1004
          Height = 50
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          object BtCorrige: TBitBtn
            Tag = 18
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Caption = '&Corrige'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtCorrigeClick
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 610
    Width = 1008
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 665
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPsq: TMySQLQuery
   
    BeforeClose = QrPsqBeforeClose
    AfterScroll = QrPsqAfterScroll
    SQL.Strings = (
      'DROP TABLE IF EXISTS _LCT_INN_; '
      'CREATE TABLE _LCT_INN_ '
      'SELECT GraGruX GGXInn, Controle IMEIInn,  '
      'DataHora, MovimID, MovimNiv,  '
      'SrcMovID, Pecas PecasInn,  '
      'AreaM2 AreaM2Inn, PesoKg PesoKgInn  '
      'FROM bluederm_colorado.vsmovits '
      'WHERE MovimID=1 '
      'ORDER BY DataHora '
      '; '
      'DROP TABLE IF EXISTS _LCT_BXA_; '
      'CREATE TABLE _LCT_BXA_ '
      'SELECT GraGruX GGXBxa, Controle IMEIBxa,  '
      'DataHora, MovimID, MovimNiv,  '
      'SrcMovID, Pecas, AreaM2, PesoKg,  '
      'SrcNivel2  '
      'FROM bluederm_colorado.vsmovits '
      'WHERE SrcMovID=1 '
      'ORDER BY DataHora '
      '; '
      ' '
      'DROP TABLE IF EXISTS _SUM_BXA_; '
      'CREATE TABLE _SUM_BXA_ '
      'SELECT GraGruX GGXBxa, SrcNivel2,  '
      'SUM(Pecas) PecasBxa,  '
      'SUM(AreaM2) AreaM2Bxa, '
      'SUM(PesoKg) PesoKgBxa '
      'FROM bluederm_colorado.vsmovits '
      'WHERE SrcMovID=1 '
      'GROUP BY SrcNivel2 '
      '; '
      ' '
      ' '
      'DROP TABLE IF EXISTS _SDO_INN_; '
      'CREATE TABLE _SDO_INN_ '
      'SELECT inn.*, bxa.*, '
      'inn.PecasInn+bxa.PecasBxa SdoPc, '
      'inn.AreaM2Inn+bxa.AreaM2Bxa SdoM2, '
      'inn.PesoKgInn+bxa.PesoKgBxa SdoKg  '
      'FROM _LCT_INN_ inn  '
      'LEFT JOIN _SUM_BXA_ bxa ON inn.IMEIInn=bxa.SrcNivel2 '
      '; '
      ' '
      'SELECT sdo.*, ggx.Controle Reduzido, CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR  '
      'FROM _SDO_INN_ sdo '
      
        'LEFT JOIN bluederm_colorado.gragrux    ggx ON ggx.Controle=sdo.G' +
        'GXInn '
      
        'LEFT JOIN bluederm_colorado.gragruy    ggy ON ggy.Codigo=ggx.Con' +
        'trole '
      
        'LEFT JOIN bluederm_colorado.gragruc    ggc ON ggc.Controle=ggx.G' +
        'raGruC  '
      
        'LEFT JOIN bluederm_colorado.gracorcad  gcc ON gcc.Codigo=ggc.Gra' +
        'CorCad  '
      
        'LEFT JOIN bluederm_colorado.gratamits  gti ON gti.Controle=ggx.G' +
        'raTamI  '
      
        'LEFT JOIN bluederm_colorado.gragru1    gg1 ON gg1.Nivel1=ggx.Gra' +
        'Gru1  '
      'WHERE SdoPc <> 0 '
      'OR SdoM2 <> 0 '
      'OR SdoKg <> 0 '
      'ORDER BY DataHora '
      '')
    Left = 328
    Top = 324
    object QrPsqGGXInn: TIntegerField
      FieldName = 'GGXInn'
    end
    object QrPsqIMEIInn: TIntegerField
      FieldName = 'IMEIInn'
    end
    object QrPsqDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPsqMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrPsqMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrPsqSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrPsqPecasInn: TFloatField
      FieldName = 'PecasInn'
    end
    object QrPsqAreaM2Inn: TFloatField
      FieldName = 'AreaM2Inn'
    end
    object QrPsqPesoKgInn: TFloatField
      FieldName = 'PesoKgInn'
    end
    object QrPsqGGXBxa: TIntegerField
      FieldName = 'GGXBxa'
    end
    object QrPsqSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrPsqPecasBxa: TFloatField
      FieldName = 'PecasBxa'
    end
    object QrPsqAreaM2Bxa: TFloatField
      FieldName = 'AreaM2Bxa'
    end
    object QrPsqPesoKgBxa: TFloatField
      FieldName = 'PesoKgBxa'
    end
    object QrPsqSdoPc: TFloatField
      FieldName = 'SdoPc'
    end
    object QrPsqSdoM2: TFloatField
      FieldName = 'SdoM2'
    end
    object QrPsqSdoKg: TFloatField
      FieldName = 'SdoKg'
    end
    object QrPsqReduzido: TIntegerField
      FieldName = 'Reduzido'
      Required = True
    end
    object QrPsqNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsPsq: TDataSource
    DataSet = QrPsq
    Left = 372
    Top = 324
  end
  object QrBxa: TMySQLQuery
   
    AfterOpen = QrBxaAfterOpen
    BeforeClose = QrBxaBeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM _lct_bxa_'
      'WHERE SrcNivel2=116'
      'ORDER BY IMEIBxa DESC')
    Left = 328
    Top = 372
    object QrBxaGGXBxa: TIntegerField
      FieldName = 'GGXBxa'
    end
    object QrBxaIMEIBxa: TIntegerField
      FieldName = 'IMEIBxa'
    end
    object QrBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrBxaMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrBxaMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrBxaSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrBxaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrBxaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrBxaPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrBxaSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object DsBxa: TDataSource
    DataSet = QrBxa
    Left = 372
    Top = 372
  end
  object PMCorrigeID01: TPopupMenu
    OnPopup = PMCorrigeID01Popup
    Left = 24
    Top = 548
    object CorrigePesosBaixaInNatura1: TMenuItem
      Caption = '&1. Corrige pesos baixa In Natura'
      object CorrigePesosBaixaInNatura_Atual1: TMenuItem
        Caption = '&Atual'
        OnClick = CorrigePesosBaixaInNatura_Atual1Click
      end
      object CorrigePesosBaixaInNatura_Selecionadospossveis1: TMenuItem
        Caption = '&Selecionados poss'#237'veis'
        OnClick = CorrigePesosBaixaInNatura_Selecionadospossveis1Click
      end
      object CorrigePesosBaixaInNatura_Todospossveis1: TMenuItem
        Caption = '&Todos poss'#237'veis'
        OnClick = CorrigePesosBaixaInNatura_Todospossveis1Click
      end
    end
    object ID01N2CorrigepeasepesobaizaInNatura1: TMenuItem
      Caption = '2. Corrige pe'#231'as e peso baixa In Natura'
      OnClick = ID01N2CorrigepeasepesobaizaInNatura1Click
    end
  end
end
