unit VSArtCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkMemo, Vcl.Grids,
  Vcl.DBGrids, dmkDBGridZTO, Vcl.Menus;

type
  TFmVSArtCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVSArtCab: TmySQLQuery;
    DsVSArtCab: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrFluxProCab: TMySQLQuery;
    QrFluxProCabCodigo: TIntegerField;
    QrFluxProCabNome: TWideStringField;
    DsFluxProCab: TDataSource;
    Label8: TLabel;
    EdFluxProCab: TdmkEditCB;
    CBFluxProCab: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdReceiRecu: TdmkEditCB;
    CBReceiRecu: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdReceiRefu: TdmkEditCB;
    CBReceiRefu: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdReceiAcab: TdmkEditCB;
    CBReceiAcab: TdmkDBLookupComboBox;
    QrReceiRecu: TmySQLQuery;
    QrReceiRecuNumero: TIntegerField;
    QrReceiRecuNome: TWideStringField;
    DsReceiRecu: TDataSource;
    QrReceirefu: TmySQLQuery;
    DsReceiRefu: TDataSource;
    QrReceirefuNumero: TIntegerField;
    QrReceirefuNome: TWideStringField;
    QrReceiAcab: TmySQLQuery;
    QrReceiAcabNumero: TIntegerField;
    QrReceiAcabNome: TWideStringField;
    DsReceiAcab: TDataSource;
    Label6: TLabel;
    EdTxtMPs: TdmkEdit;
    Label10: TLabel;
    MeObserva: TdmkMemo;
    DBMemo1: TDBMemo;
    QrVSArtCabCodigo: TIntegerField;
    QrVSArtCabNome: TWideStringField;
    QrVSArtCabNO_ReceiRecu: TWideStringField;
    QrVSArtCabNO_ReceiRefu: TWideStringField;
    QrVSArtCabNO_ReceiAcab: TWideStringField;
    QrVSArtCabReceiRecu: TIntegerField;
    QrVSArtCabReceiRefu: TIntegerField;
    QrVSArtCabReceiAcab: TIntegerField;
    QrVSArtCabTxtMPs: TWideStringField;
    QrVSArtCabObserva: TWideMemoField;
    QrVSArtCabLk: TIntegerField;
    QrVSArtCabDataCad: TDateField;
    QrVSArtCabDataAlt: TDateField;
    QrVSArtCabUserCad: TIntegerField;
    QrVSArtCabUserAlt: TIntegerField;
    QrVSArtCabAlterWeb: TSmallintField;
    QrVSArtCabAtivo: TSmallintField;
    Panel6: TPanel;
    GBDados: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    DBGVSArtGGX: TdmkDBGridZTO;
    Label1: TLabel;
    QrVSArtGGX: TmySQLQuery;
    DsVSArtGGX: TDataSource;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    ItsExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrVSArtGGXCodigo: TIntegerField;
    QrVSArtGGXControle: TIntegerField;
    QrVSArtGGXGraGruX: TIntegerField;
    QrVSArtGGXLk: TIntegerField;
    QrVSArtGGXDataCad: TDateField;
    QrVSArtGGXDataAlt: TDateField;
    QrVSArtGGXUserCad: TIntegerField;
    QrVSArtGGXUserAlt: TIntegerField;
    QrVSArtGGXAlterWeb: TSmallintField;
    QrVSArtGGXAtivo: TSmallintField;
    QrVSArtGGXGraGru1: TIntegerField;
    QrVSArtGGXNO_PRD_TAM_COR: TWideStringField;
    QrVSArtGGXSIGLAUNIDMED: TWideStringField;
    QrVSArtGGXCODUSUUNIDMED: TIntegerField;
    QrVSArtGGXNOMEUNIDMED: TWideStringField;
    QrVSArtGGXCouNiv2: TFloatField;
    QrVSArtGGXGrandeza: TFloatField;
    QrVSArtGGXGraGruY: TIntegerField;
    QrFluxPcpCab: TMySQLQuery;
    DsFluxPcpCab: TDataSource;
    Label2: TLabel;
    EdFluxPcpCab: TdmkEditCB;
    CBFluxPcpCab: TdmkDBLookupComboBox;
    QrFluxPcpCabCodigo: TIntegerField;
    QrFluxPcpCabNome: TWideStringField;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label11: TLabel;
    Label19: TLabel;
    QrVSArtCabFluxProCab: TIntegerField;
    QrVSArtCabFluxPcpCab: TIntegerField;
    QrVSArtCabNO_FluxProCab: TWideStringField;
    QrVSArtCabNO_FluxPcpCab: TWideStringField;
    SbFluxPcpCab: TSpeedButton;
    Label20: TLabel;
    EdLinCulReb: TdmkEdit;
    Label21: TLabel;
    EdLinCabReb: TdmkEdit;
    Label22: TLabel;
    EdLinCulSem: TdmkEdit;
    Label23: TLabel;
    EdLinCabSem: TdmkEdit;
    QrVSArtGGXAWServerID: TIntegerField;
    QrVSArtGGXAWStatSinc: TSmallintField;
    QrVSArtCabLinCulReb: TIntegerField;
    QrVSArtCabLinCabReb: TIntegerField;
    QrVSArtCabLinCulSem: TIntegerField;
    QrVSArtCabLinCabSem: TIntegerField;
    QrVSArtCabAWServerID: TIntegerField;
    QrVSArtCabAWStatSinc: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSArtCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSArtCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSArtCabAfterScroll(DataSet: TDataSet);
    procedure QrVSArtCabBeforeClose(DataSet: TDataSet);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure SbFluxProCabClick(Sender: TObject);
    procedure SbFluxPcpCabClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenVSArtGGX(GraGruX: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmVSArtCab: TFmVSArtCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnGrade_Jan, DmkDAC_PF, MyDBCheck, VSArtGGX, UnVS_PF,
  UnApp_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSArtCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSArtCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSArtCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSArtCab, QrVSArtGGX);
end;

procedure TFmVSArtCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSArtCab);
  MyObjects.HabilitaMenuItemItsIns(ItsAltera1, QrVSArtGGX);
  MyObjects.HabilitaMenuItemItsIns(ItsExclui1, QrVSArtGGX);
end;

procedure TFmVSArtCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSArtCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSArtCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsartcab';
  VAR_GOTOMYSQLTABLE := QrVSArtCab;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

(*
  VAR_SQLx.Add('SELECT flu.Nome NO_Fluxo, fo1.Nome NO_ReceiRecu,');
  VAR_SQLx.Add('fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,');
  VAR_SQLx.Add('wac.*,');
  VAR_SQLx.Add('ggx.GraGru1, CONCAT(gg1.Nome,');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  VAR_SQLx.Add('NO_PRD_TAM_COR');
  VAR_SQLx.Add('FROM vsartcab wac');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=wac.GraGruX');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('LEFT JOIN fluxos  flu ON flu.Codigo=wac.Fluxo');
  VAR_SQLx.Add('LEFT JOIN formulas fo1 ON fo1.Numero=wac.ReceiRecu');
  VAR_SQLx.Add('LEFT JOIN formulas fo2 ON fo2.Numero=wac.ReceiRefu');
  VAR_SQLx.Add('LEFT JOIN formulas ti1 ON ti1.Numero=wac.ReceiAcab');
*)
  VAR_SQLx.Add('SELECT fo1.Nome NO_ReceiRecu, ');
  VAR_SQLx.Add('fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab, ');
  VAR_SQLx.Add('fro.Nome NO_FluxProCab, fpc.Nome NO_FluxPcpCab, vac.* ');
  VAR_SQLx.Add('FROM vsartcab vac ');
  VAR_SQLx.Add('LEFT JOIN fluxos       fro ON fro.Codigo=vac.FluxProCab ');
  VAR_SQLx.Add('LEFT JOIN fluxpcpcab   fpc ON fpc.Codigo=vac.FluxPcpCab ');
  VAR_SQLx.Add('LEFT JOIN formulas fo1 ON fo1.Numero=vac.ReceiRecu ');
  VAR_SQLx.Add('LEFT JOIN formulas fo2 ON fo2.Numero=vac.ReceiRefu ');
  VAR_SQLx.Add('LEFT JOIN formulas ti1 ON ti1.Numero=vac.ReceiAcab ');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('WHERE vac.Codigo > 0');
  //
  VAR_SQL1.Add('AND vac.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE Nome LIKE :P0 ');
  //
end;

procedure TFmVSArtCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSArtCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsartcab');
end;

procedure TFmVSArtCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSArtCabCodigo.Value;
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item?', 'vsartcab',
    'Codigo', Codigo, Dmod.MyDB);
  LocCod(Codigo, Codigo);
end;

procedure TFmVSArtCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSArtCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vsartcab');
end;

procedure TFmVSArtCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSArtCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSArtCab.ReopenVSArtGGX(GraGruX: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSArtGGX, Dmod.MyDB, [
  'SELECT vag.*, ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(xco.CouNiv2 IS NULL, -1.000, xco.CouNiv2+0.000) CouNiv2, ',
  'IF(unm.Grandeza IS NULL, -1.000, unm.Grandeza+0.000) Grandeza, ',
  'ggx.GraGruY ',
  'FROM vsartggx vag',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vag.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
  'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
  'WHERE vag.Codigo=' + Geral.FF0(QrVSArtCabCodigo.Value),
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
end;

procedure TFmVSArtCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSArtCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSArtCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSArtCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSArtCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSArtCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSArtCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSArtCabCodigo.Value;
  Close;
end;

procedure TFmVSArtCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSArtCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, FluxProCab, FluxPcpCab, ReceiRecu, ReceiRefu, ReceiAcab, LinCulReb,
  LinCabReb, LinCulSem, LinCabSem: Integer;
  Nome, TxtMPs, Observa: String;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  //Fluxo          := EdFluxo.ValueVariant;
  ReceiRecu      := EdReceiRecu.ValueVariant;
  ReceiRefu      := EdReceiRefu.ValueVariant;
  ReceiAcab      := EdReceiAcab.ValueVariant;
  TxtMPs         := EdTxtMPs.Text;
  Observa        := MeObserva.Text;
  FluxProCab     := EdFluxProCab.ValueVariant;
  FluxPcpCab     := EdFluxPcpCab.ValueVariant;
  LinCulReb      := EdLinCulReb.ValueVariant;
  LinCabReb      := EdLinCabReb.ValueVariant;
  LinCulSem      := EdLinCulSem.ValueVariant;
  LinCabSem      := EdLinCabSem.ValueVariant;
  Codigo := UMyMod.BPGS1I32('vsartcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsartcab', False, [
  'Nome', 'ReceiRecu', 'ReceiRefu',
  'ReceiAcab', 'TxtMPs', 'Observa',
  'FluxProCab', 'FluxPcpCab', 'LinCulReb',
  'LinCabReb', 'LinCulSem', 'LinCabSem'], [
  'Codigo'], [
  Nome, ReceiRecu, ReceiRefu,
  ReceiAcab, TxtMPs, Observa,
  FluxProCab, FluxPcpCab, LinCulReb,
  LinCabReb, LinCulSem, LinCabSem], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSArtCab.BtDesisteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsartcab', CO_CODIGO);
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVSArtCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSArtCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //GBEdita.Align := alClient;
  //GBDados.Align := alClient;
  MeObserva.Align := alClient;
  DBMemo1.Align := alClient;
  CriaOForm;
  //
  UnDmkDAC_PF.AbreQuery(QrFluxProCab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFluxPcpCab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiRecu, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiRefu, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiAcab, Dmod.MyDB);
  //
end;

procedure TFmVSArtCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSArtCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSArtCab.SbFluxPcpCabClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormFluxPcpCab(EdFluxPcpCab.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdFluxPcpCab, CBFluxPcpCab, QrFluxPcpCab, VAR_CADASTRO);
end;

procedure TFmVSArtCab.SbFluxProCabClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_Jan.MostraFormFluxProCab(EdFluxProCab.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdFluxProCab, CBFluxProCab, QrFluxProCab, VAR_CADASTRO);
end;

procedure TFmVSArtCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSArtCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSArtCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSArtCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSArtCab.QrVSArtCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSArtCab.QrVSArtCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSArtGGX(0);
end;

procedure TFmVSArtCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSArtCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSArtCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsartcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSArtCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSArtCab.ItsExclui1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrVSArtGGX, TDBGrid(DBGVSArtGGX),
  'vsartggx', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmVSArtCab.ItsInclui1Click(Sender: TObject);
(*
const
  Aviso  = '...';
  Titulo = 'Reduzidos atrelados a configura��o de Artigo';
  Prompt = 'Selecione os reduzidos a serem atrelados � configura��o do Artigo';
  FieldAtivo = 'Ativo';
  FieldNivel = 'Controle';
  FieldNome  = 'NO_PRD_TAM_COR';
  QrExecSQL = nil;
var
  SQLExec, SQLOpen: String;
*)
begin
  if DBCheck.CriaFm(TFmVSArtGGX, FmVSArtGGX, afmoNegarComAviso) then
  begin
    FmVSArtGGX.FCodigo   := QrVSArtCabCodigo.Value;
    FmVSArtGGX.FControle := QrVSArtGGXGraGruX.Value;
    FmVSArtGGX.ReopenReduzidos();
    FmVSArtGGX.ShowModal;
    ReopenVSArtGGX(FmVSArtGGX.FControle);
    FmVSArtGGX.Destroy;
    //
  end;
(*
  //SQLExec := (['']);
  //SQLOpen :=
  DBCheck.EscolheCodigosMultiplos_0(Aviso, Titulo, Prompt, nil,
  FieldAtivo, FieldNivel, FieldNome, SQLExec, [
  'SELECT vag.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(xco.CouNiv2 IS NULL, -1.000, xco.CouNiv2+0.000) CouNiv2, ',
  'IF(unm.Grandeza IS NULL, -1.000, unm.Grandeza+0.000) Grandeza, ',
  'ggx.GraGruY ',
  'FROM vsartggx vag',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vag.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
  'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
  //'WHERE vag.Codigo=' + Geral.FF0(QrVSArtCabCodigo.Value),
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  ''],
  //SQLOpen,
  QrExecSQL);
*)
end;

procedure TFmVSArtCab.QrVSArtCabBeforeClose(DataSet: TDataSet);
begin
  QrVSArtGGX.Close;
end;

procedure TFmVSArtCab.QrVSArtCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSArtCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

