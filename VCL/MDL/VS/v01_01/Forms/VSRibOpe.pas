unit VSRibOpe;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkMemo, frxClass,
  frxDBSet, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO;

type
  TFmVSRibOpe = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtArtigo: TBitBtn;
    BtMP: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVSRibOpe: TmySQLQuery;
    DsVSRibOpe: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrVSRibOpeGraGruX: TIntegerField;
    QrVSRibOpeLk: TIntegerField;
    QrVSRibOpeDataCad: TDateField;
    QrVSRibOpeDataAlt: TDateField;
    QrVSRibOpeUserCad: TIntegerField;
    QrVSRibOpeUserAlt: TIntegerField;
    QrVSRibOpeAlterWeb: TSmallintField;
    QrVSRibOpeAtivo: TSmallintField;
    QrVSRibOpeGraGru1: TIntegerField;
    QrVSRibOpeNO_PRD_TAM_COR: TWideStringField;
    QrCad: TmySQLQuery;
    QrCadGraGruX: TIntegerField;
    QrCadLk: TIntegerField;
    QrCadDataCad: TDateField;
    QrCadDataAlt: TDateField;
    QrCadUserCad: TIntegerField;
    QrCadUserAlt: TIntegerField;
    QrCadAlterWeb: TSmallintField;
    QrCadAtivo: TSmallintField;
    QrCadGraGru1: TIntegerField;
    QrCadNO_PRD_TAM_COR: TWideStringField;
    QrCadOrdem: TIntegerField;
    QrCadBRLMedM2: TFloatField;
    PMMP: TPopupMenu;
    PMArtigo: TPopupMenu;
    DBGrid1: TdmkDBGridZTO;
    Incluilinkdeprodutos1: TMenuItem;
    Alteralinkdeprodutos1: TMenuItem;
    Excluilinkdeprodutos1: TMenuItem;
    Incluinovoartigoclassificado1: TMenuItem;
    AlteraArtigoClassificadoAtual1: TMenuItem;
    ExcluiArtigoClassificadoAtual1: TMenuItem;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    QrGraGruXCou: TmySQLQuery;
    QrGraGruXCouNO_CouNiv1: TWideStringField;
    QrGraGruXCouNO_CouNiv2: TWideStringField;
    QrGraGruXCouGraGruX: TIntegerField;
    QrGraGruXCouCouNiv1: TIntegerField;
    QrGraGruXCouCouNiv2: TIntegerField;
    QrGraGruXCouLk: TIntegerField;
    QrGraGruXCouDataCad: TDateField;
    QrGraGruXCouDataAlt: TDateField;
    QrGraGruXCouUserCad: TIntegerField;
    QrGraGruXCouUserAlt: TIntegerField;
    QrGraGruXCouAlterWeb: TSmallintField;
    QrGraGruXCouAtivo: TSmallintField;
    DsGraGruXCou: TDataSource;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    GBCouNiv: TGroupBox;
    Label6: TLabel;
    Label4: TLabel;
    EdCouNiv2: TdmkEditCB;
    CBCouNiv2: TdmkDBLookupComboBox;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    EdArtigoImp: TdmkEdit;
    EdClasseImp: TdmkEdit;
    QrVSRibOpeArtigoImp: TWideStringField;
    QrVSRibOpeClasseImp: TWideStringField;
    QrGraGruXCouArtigoImp: TWideStringField;
    QrGraGruXCouClasseImp: TWideStringField;
    Label13: TLabel;
    EdPrevPcPal: TdmkEdit;
    QrGraGruXCouPrevPcPal: TIntegerField;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    QrVSRibOpeMediaMinM2: TFloatField;
    QrVSRibOpeMediaMaxM2: TFloatField;
    Label15: TLabel;
    Label9: TLabel;
    EdNome: TdmkEdit;
    EdTam: TdmkEdit;
    EdCor: TdmkEdit;
    Label16: TLabel;
    Label21: TLabel;
    EdPrevAMPal: TdmkEdit;
    EdPrevKgPal: TdmkEdit;
    Label22: TLabel;
    QrGraGruXCouPrevAMPal: TFloatField;
    QrGraGruXCouPrevKgPal: TFloatField;
    Label19: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label20: TLabel;
    QrGraGruXCouGrandeza: TSmallintField;
    RGBastidao: TdmkRadioGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSRibOpeAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSRibOpeBeforeOpen(DataSet: TDataSet);
    procedure BtMPClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtArtigoClick(Sender: TObject);
    procedure PMArtigoPopup(Sender: TObject);
    procedure QrVSRibOpeBeforeClose(DataSet: TDataSet);
    procedure QrVSRibOpeAfterScroll(DataSet: TDataSet);
    procedure Incluilinkdeprodutos1Click(Sender: TObject);
    procedure Alteralinkdeprodutos1Click(Sender: TObject);
    procedure Excluilinkdeprodutos1Click(Sender: TObject);
    procedure Incluinovoartigoclassificado1Click(Sender: TObject);
    procedure AlteraArtigoClassificadoAtual1Click(Sender: TObject);
    procedure ExcluiArtigoClassificadoAtual1Click(Sender: TObject);
    procedure PMMPPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure MostraVSRibArt(SQLType: TSQLType);
    procedure ReopenVSRibArt(Codigo: Integer);

  public
    { Public declarations }
    FSeq: Integer;
    procedure LocCod(Atual, GraGruX: Integer);
  end;

var
  FmVSRibOpe: TFmVSRibOpe;
const
  FFormatFloat = '00000';
  FCo_Codigo = 'GraGruX';
  FCo_Nome = 'CONCAT(gg1.Nome, ' + sLineBreak +
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ' + sLineBreak +
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))';

implementation

uses UnMyObjects, Module, MeuDBUses, DMkDAC_PF, ModuleGeral, MyDBCheck,
  VSRibArt, UnVS_PF, AppListas, UnGrade_PF, UnGrade_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSRibOpe.LocCod(Atual, GraGruX: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, GraGruX);
end;

procedure TFmVSRibOpe.MostraVSRibArt(SQLType: TSQLType);
begin
(*
  if DBCheck.CriaFm(TFmVSRibArt, FmVSRibArt, afmoNegarComAviso) then
  begin
    FmVSRibArt.ImgTipo.SQLType := SQLType;
    FmVSRibArt.FQrCab := QrVSRibOpe;
    FmVSRibArt.FDsCab := DsVSRibCla;
    FmVSRibArt.FQrIts := QrVSRibArt;
    //
    FmVSRibArt.LaVSRibCla.Enabled := False;
    FmVSRibArt.EdVSRibCla.Enabled := False;
    FmVSRibArt.CBVSRibCla.Enabled := False;
    //
    FmVSRibArt.LaVSRibCad.Enabled := True;
    FmVSRibArt.EdVSRibCad.Enabled := True;
    FmVSRibArt.CBVSRibCad.Enabled := True;
    //

    FmVSRibArt.EdVSRibCla.ValueVariant := QrVSRibOpeGraGruX.Value;
    FmVSRibArt.CBVSRibCla.KeyValue     := QrVSRibOpeGraGruX.Value;
    //
    if SQLType = stIns then
    begin
      FmVSRibArt.FCodigo := 0;
    end else
    begin
      FmVSRibArt.FCodigo := QrVSRibArtCodigo.Value;
      //
      FmVSRibArt.EdVSRibCad.ValueVariant := QrVSRibArtVSRibCad.Value;
      FmVSRibArt.CBVSRibCad.KeyValue     := QrVSRibArtVSRibCad.Value;
      //
    end;
    FmVSRibArt.ShowModal;
    FmVSRibArt.Destroy;
  end;
*)
end;

procedure TFmVSRibOpe.PMArtigoPopup(Sender: TObject);
begin
(*
  MyObjects.HabilitaMenuItemItsIns(Incluilinkdeprodutos1, QrVSRibOpe);
  MyObjects.HabilitaMenuItemItsUpd(Alteralinkdeprodutos1, QrVSRibArt);
  MyObjects.HabilitaMenuItemItsDel(Alteralinkdeprodutos1, QrVSRibArt);
*)
end;

procedure TFmVSRibOpe.PMMPPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(AlteraArtigoClassificadoAtual1, QrVSRibOpe);
  //MyObjects.HabilitaMenuItemCabDel(ExcluiArtigoClassificadoAtual1, QrVSRibOpe);
end;

procedure TFmVSRibOpe.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSRibOpeGraGruX.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSRibOpe.DefParams;
begin
  VAR_GOTOTABELA := 'vsribope';
  VAR_GOTOMYSQLTABLE := QrVSRibOpe;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := FCo_Codigo;
  VAR_GOTONOME := FCo_Nome;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wmp.*, cou.*, ');
  VAR_SQLx.Add('ggx.GraGru1, CONCAT(gg1.Nome,');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  VAR_SQLx.Add('NO_PRD_TAM_COR');
  VAR_SQLx.Add('FROM vsribope wmp');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('WHERE wmp.GraGruX > 0');
  //
  VAR_SQL1.Add('AND wmp.GraGruX=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE ' + FCo_Nome + ' LIKE :P0 ');
  //
end;

procedure TFmVSRibOpe.ExcluiArtigoClassificadoAtual1Click(Sender: TObject);
begin
//
end;

procedure TFmVSRibOpe.Excluilinkdeprodutos1Click(Sender: TObject);
(*
var
  Codigo: Integer;
*)
begin
(*
  VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle,
  Integer(TEstqMotivDel.emtdWetCurti???), Dmod.QrUpd, Dmod.MyDB, CO_MASTER?) then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSRibArt,
      QrVSRibArtCodigo, QrVSRibArtCodigo.Value);
    ReopenVSRibArt(Codigo);
  end;
*)
end;

procedure TFmVSRibOpe.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSRibOpe.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSRibOpe.ReopenVSRibArt(Codigo: Integer);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSRibArt, Dmod.MyDB, [
  'SELECT wmp.GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vna.* ',
  'FROM vsribart vna ',
  'LEFT JOIN vsribcad wmp ON wmp.GraGruX=vna.VSRibCad ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vna.VSRibOpe=' + Geral.FF0(QrVSRibOpeGraGruX.Value),
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
*)
end;

procedure TFmVSRibOpe.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSRibOpe.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSRibOpe.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSRibOpe.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSRibOpe.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSRibOpe.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRibOpe.AlteraArtigoClassificadoAtual1Click(Sender: TObject);
begin
  Grade_PF.DefineNomeTamCorGraGruX(EdNome, EdTam, EdCor, QrVSRibOpeGraGruX.Value);
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGruXCou, [PnDados],
  [PnEdita], EdCouNiv2, ImgTipo, 'gragruxcou');
end;

procedure TFmVSRibOpe.Alteralinkdeprodutos1Click(Sender: TObject);
begin
  MostraVSRibArt(stUpd);
end;

procedure TFmVSRibOpe.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSRibOpeGraGruX.Value;
  Close;
end;

procedure TFmVSRibOpe.BtConfirmaClick(Sender: TObject);
const
  MediaMinM2 = 0;
  MediaMaxM2 = 0;
var
  GraGruX, CouNiv1, CouNiv2, PrevPcPal, GraGru1, Bastidao: Integer;
  ArtigoImp, ClasseImp, Nome: String;
  PrevAMPal, PrevKgPal: Double;
begin
  GraGruX        := QrVSRibOpeGraGruX.Value;
  GraGru1        := QrVSRibOpeGraGru1.Value;
  //Ordem          := EdOrdem.ValueVariant;
  CouNiv1        := EdCouNiv1.ValueVariant;
  CouNiv2        := EdCouNiv2.ValueVariant;
  ArtigoImp      := EdArtigoImp.ValueVariant;
  ClasseImp      := EdClasseImp.ValueVariant;
  PrevPcPal      := EdPrevPcPal.ValueVariant;
  PrevAMPal      := EdPrevAMPal.ValueVariant;
  PrevKgPal      := EdPrevKgPal.ValueVariant;
  Nome           := EdNome.Text;
  Bastidao       := RGBastidao.ItemIndex;
  //
  if MyObjects.FIC(CouNiv2 = 0, EdCouNiv2, 'Informe o "Tipo do material"!') then
    Exit;
  if MyObjects.FIC(CouNiv1 = 0, EdCouNiv1, 'Informe a "Parte do material"!') then
    Exit;
  //
  VS_PF.ReIncluiCouNivs(
    GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1, Bastidao, PrevAMPal,
    PrevKgPal, ArtigoImp, ClasseImp, MediaMinM2, MediaMaxM2);
  //
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  //
  Grade_PF.GraGru1_AlteraNome(GraGru1, Nome);
  //
  if FSeq = 0 then
  begin
    LocCod(GraGruX, GraGruX);
  end else
    Close;
end;

procedure TFmVSRibOpe.BtDesisteClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX        := QrVSRibOpeGraGruX.Value;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(GraGruX, Dmod.MyDB, 'vsribope', FCo_Codigo);
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVSRibOpe.BtArtigoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArtigo, BtArtigo);
end;

procedure TFmVSRibOpe.BtMPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMP, BtMP);
end;

procedure TFmVSRibOpe.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSeq := 0;
  //GBEdita.Align := alClient;
  DBGrid1.Align := alClient;
  VS_PF.ConfiguraRGVSBastidao(RGBastidao, True(*Habilita*), (*Default*)-1);
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  CriaOForm;
end;

procedure TFmVSRibOpe.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSRibOpeGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmVSRibOpe.SbImprimeClick(Sender: TObject);
(*
var
  Ordem: Integer;
  Ord_Txt: String;
*)
begin
(*
  Ordem := MyObjects.SelRadioGroup('Ordem de Impress�o',
  'Selecione a ordem de Impress�o', [
  'Ordem',
  'Nome',
  'Pre�o'], 2);
  case Ordem of
    0: Ord_Txt := 'Ordem';
    1: Ord_Txt := 'NO_PRD_TAM_COR';
    2: Ord_Txt := 'BRLMedM2';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCad, Dmod.MyDB, [
  'SELECT wmp.*, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vsribope wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY ' + Ord_Txt,
  '']);
  MyObjects.frxDefineDataSets(frxWET_RECUR_013, [
  DModG.frxDsDono,
  frxDsCad]);
  MyObjects.frxMostra(frxWET_RECUR_013,
    'Configura��o de Mat�ria-prima para Semi / Acabado');
*)
end;

procedure TFmVSRibOpe.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSRibOpe.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSRibOpeGraGruX.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSRibOpe.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSRibOpe.QrVSRibOpeAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSRibOpe.QrVSRibOpeAfterScroll(DataSet: TDataSet);
begin
  ReopenVSRibArt(0);
  VS_PF.ReopenGraGruXCou(QrGraGruXCou, QrVSRibOPeGraGruX.Value);
end;

procedure TFmVSRibOpe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSRibOpe.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSRibOpeGraGruX.Value,
  CuringaLoc.CriaForm(FCo_Codigo, FCo_Nome, 'vsribope', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSRibOpe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRibOpe.Incluilinkdeprodutos1Click(Sender: TObject);
begin
  MostraVSRibArt(stIns);
end;

procedure TFmVSRibOpe.Incluinovoartigoclassificado1Click(Sender: TObject);
var
  DefinedSQL: String;
  Qry: TmySQLQuery;
  GraGruX: Integer;
begin
  GraGruX := Grade_Jan.ObtemGraGruXSemY(CO_GraGruY_4096_VSRibOpe);
  if GraGruX <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM vsribope ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if Qry.FieldByName('GraGruX').AsInteger = GraGruX then
      begin
        Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
        ' j� estava adicionado � configura��o de mat�ria-prima!');
      end else
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsribope', False, [
        ], [
        'GraGruX'], [
        ], [
        GraGruX], True);
      end;
      //
      LocCod(GraGruX, GraGruX);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmVSRibOpe.QrVSRibOpeBeforeClose(DataSet: TDataSet);
begin
(*
  QrVSRibArt.Close;
*)
  QrGraGruXCou.Close;
end;

procedure TFmVSRibOpe.QrVSRibOpeBeforeOpen(DataSet: TDataSet);
begin
  QrVSRibOpeGraGruX.DisplayFormat := FFormatFloat;
end;

end.

