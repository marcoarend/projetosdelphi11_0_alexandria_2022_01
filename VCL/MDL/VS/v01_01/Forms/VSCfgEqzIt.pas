unit VSCfgEqzIt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmVSCfgEqzIt = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdSubClass: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    DsGraGruX: TDataSource;
    Label2: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    Label1: TLabel;
    EdBasNota: TdmkEdit;
    EdFatorEqz: TdmkEdit;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenVSCfgEqzIt(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSCfgEqzIt: TFmVSCfgEqzIt;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
UnDmkProcFunc, UnVS_PF;

{$R *.DFM}

procedure TFmVSCfgEqzIt.BtOKClick(Sender: TObject);
var
  SubClass: String;
  Codigo, Controle, GraGruX: Integer;
  BasNota, FatorEqz: Double;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  SubClass       := dmkPF.SoTextoLayout(EdSubClass.Text);
  GraGruX        := EdGraGruX.ValueVariant;
  BasNota        := EdBasNota.ValueVariant;
  FatorEqz       := EdFatorEqz.ValueVariant;
  //
  if MyObjects.FIC(Trim(SubClass) = '', EdSubClass, 'Informe uma sub classe!') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe um artigo!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('vscfgeqzit', 'Controle', '', '', tsPos,
    ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vscfgeqzit', False, [
  'Codigo', 'SubClass', 'GraGruX',
  'BasNota', 'FatorEqz'], [
  'Controle'], [
  Codigo, SubClass, GraGruX,
  BasNota, FatorEqz], [
  Controle], True) then
  begin
    VS_PF.AtualizaNotaVSCfgEqzCb(Codigo);
    ReopenVSCfgEqzIt(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdGraGruX.ValueVariant   := 0;
      CBGraGruX.KeyValue       := Null;
      EdSubClass.ValueVariant  := '';
      EdBasNota.ValueVariant   := 0;
      EdFatorEqz.ValueVariant  := 0;
      //
      EdSubClass.SetFocus;
    end else Close;
  end;
end;

procedure TFmVSCfgEqzIt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCfgEqzIt.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmVSCfgEqzIt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
end;

procedure TFmVSCfgEqzIt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCfgEqzIt.ReopenVSCfgEqzIt(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSCfgEqzIt.SpeedButton1Click(Sender: TObject);
begin
(*
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFm_CadSel_, Fm_CadSel_, afmoNegarComAviso) then
  begin
    Fm_CadSel_.ShowModal;
    Fm_CadSel_.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO);
*)
end;

end.
