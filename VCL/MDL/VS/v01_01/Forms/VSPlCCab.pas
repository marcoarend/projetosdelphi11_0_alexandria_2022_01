unit VSPlCCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, frxCross,
  dmkDBGridZTO, UnProjGroup_Consts, UnGrl_Consts, UnGrl_Geral, UnAppEnums;

type
  TFmVSPlCCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSPlCCab: TmySQLQuery;
    DsVSPlCCab: TDataSource;
    QrVSPlCIts: TmySQLQuery;
    DsVSPlCIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtCompra: TdmkEditDateTimePicker;
    EdDtCompra: TdmkEdit;
    Label53: TLabel;
    TPDtViagem: TdmkEditDateTimePicker;
    EdDtViagem: TdmkEdit;
    Label3: TLabel;
    TPDtEntrada: TdmkEditDateTimePicker;
    EdDtEntrada: TdmkEdit;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrTransporta: TmySQLQuery;
    DsTransporta: TDataSource;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    Label4: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSPlCCabCodigo: TIntegerField;
    QrVSPlCCabMovimCod: TIntegerField;
    QrVSPlCCabEmpresa: TIntegerField;
    QrVSPlCCabDtCompra: TDateTimeField;
    QrVSPlCCabDtViagem: TDateTimeField;
    QrVSPlCCabDtEntrada: TDateTimeField;
    QrVSPlCCabFornecedor: TIntegerField;
    QrVSPlCCabTransporta: TIntegerField;
    QrVSPlCCabPecas: TFloatField;
    QrVSPlCCabPesoKg: TFloatField;
    QrVSPlCCabAreaM2: TFloatField;
    QrVSPlCCabAreaP2: TFloatField;
    QrVSPlCCabLk: TIntegerField;
    QrVSPlCCabDataCad: TDateField;
    QrVSPlCCabDataAlt: TDateField;
    QrVSPlCCabUserCad: TIntegerField;
    QrVSPlCCabUserAlt: TIntegerField;
    QrVSPlCCabAlterWeb: TSmallintField;
    QrVSPlCCabAtivo: TSmallintField;
    QrVSPlCCabNO_EMPRESA: TWideStringField;
    QrVSPlCCabNO_FORNECE: TWideStringField;
    QrVSPlCCabNO_TRANSPORTA: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    N2: TMenuItem;
    QrVSPlCCabValorT: TFloatField;
    DBEdit15: TDBEdit;
    BtReclasif: TBitBtn;
    SbFornecedor: TSpeedButton;
    SbTransportador: TSpeedButton;
    N1: TMenuItem;
    Atualizaestoque1: TMenuItem;
    frxDsVSPlCCab: TfrxDBDataset;
    frxDsVSPlCIts: TfrxDBDataset;
    QrClienteMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClienteMO: TDataSource;
    QrProcednc: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsProcednc: TDataSource;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    Label20: TLabel;
    EdClienteMO: TdmkEditCB;
    CBClienteMO: TdmkDBLookupComboBox;
    SbClienteMO: TSpeedButton;
    Label21: TLabel;
    EdProcednc: TdmkEditCB;
    CBProcednc: TdmkDBLookupComboBox;
    SbProcedenc: TSpeedButton;
    Label22: TLabel;
    EdMotorista: TdmkEditCB;
    CBMotorista: TdmkDBLookupComboBox;
    SbMotorista: TSpeedButton;
    Label23: TLabel;
    EdPlaca: TdmkEdit;
    QrVSPlCCabClienteMO: TIntegerField;
    QrVSPlCCabProcednc: TIntegerField;
    QrVSPlCCabMotorista: TIntegerField;
    QrVSPlCCabPlaca: TWideStringField;
    QrVSPlCCabNO_CLIENTEMO: TWideStringField;
    QrVSPlCCabNO_PROCEDNC: TWideStringField;
    QrVSPlCCabNO_MOTORISTA: TWideStringField;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    QrVSMovDif: TmySQLQuery;
    QrVSMovDifControle: TIntegerField;
    QrVSMovDifInfPecas: TFloatField;
    QrVSMovDifInfPesoKg: TFloatField;
    QrVSMovDifInfAreaM2: TFloatField;
    QrVSMovDifInfAreaP2: TFloatField;
    QrVSMovDifInfValorT: TFloatField;
    QrVSMovDifLk: TIntegerField;
    QrVSMovDifDataCad: TDateField;
    QrVSMovDifDataAlt: TDateField;
    QrVSMovDifUserCad: TIntegerField;
    QrVSMovDifUserAlt: TIntegerField;
    QrVSMovDifAlterWeb: TSmallintField;
    QrVSMovDifAtivo: TSmallintField;
    QrVSMovDifDifPecas: TFloatField;
    QrVSMovDifDifPesoKg: TFloatField;
    QrVSMovDifDifAreaM2: TFloatField;
    QrVSMovDifDifAreaP2: TFloatField;
    QrVSMovDifDifValorT: TFloatField;
    Histrico1: TMenuItem;
    PCBottom: TPageControl;
    TabSheet1: TTabSheet;
    Panel6: TPanel;
    GBIts: TGroupBox;
    TabSheet2: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrVSHisFch: TmySQLQuery;
    DsVSHisFch: TDataSource;
    QrVSHisFchCodigo: TIntegerField;
    QrVSHisFchVSMovIts: TIntegerField;
    QrVSHisFchSerieFch: TIntegerField;
    QrVSHisFchFicha: TIntegerField;
    QrVSHisFchDataHora: TDateTimeField;
    QrVSHisFchNome: TWideStringField;
    QrVSHisFchObserv: TWideStringField;
    QrVSHisFchLk: TIntegerField;
    QrVSHisFchDataCad: TDateField;
    QrVSHisFchDataAlt: TDateField;
    QrVSHisFchUserCad: TIntegerField;
    QrVSHisFchUserAlt: TIntegerField;
    QrVSHisFchAlterWeb: TSmallintField;
    QrVSHisFchAtivo: TSmallintField;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    DBMemo1: TDBMemo;
    Exclui1: TMenuItem;
    Splitter2: TSplitter;
    PMVPlcIts: TPopupMenu;
    IrparajaneladegerenciamentodeFichaRMP1: TMenuItem;
    DGDados: TdmkDBGridZTO;
    FichadePallets1: TMenuItem;
    Fichadetodospalletsdestacompra1: TMenuItem;
    FichaCOMnomedoPallet1: TMenuItem;
    FichaSEMnomedoPallet1: TMenuItem;
    FichasCOMnomedoPallet1: TMenuItem;
    FichasSEMnomedoPallet1: TMenuItem;
    QrVSPlCItsCodigo: TLargeintField;
    QrVSPlCItsControle: TLargeintField;
    QrVSPlCItsMovimCod: TLargeintField;
    QrVSPlCItsMovimNiv: TLargeintField;
    QrVSPlCItsMovimTwn: TLargeintField;
    QrVSPlCItsEmpresa: TLargeintField;
    QrVSPlCItsTerceiro: TLargeintField;
    QrVSPlCItsCliVenda: TLargeintField;
    QrVSPlCItsMovimID: TLargeintField;
    QrVSPlCItsDataHora: TDateTimeField;
    QrVSPlCItsPallet: TLargeintField;
    QrVSPlCItsGraGruX: TLargeintField;
    QrVSPlCItsPecas: TFloatField;
    QrVSPlCItsPesoKg: TFloatField;
    QrVSPlCItsAreaM2: TFloatField;
    QrVSPlCItsAreaP2: TFloatField;
    QrVSPlCItsValorT: TFloatField;
    QrVSPlCItsSrcMovID: TLargeintField;
    QrVSPlCItsSrcNivel1: TLargeintField;
    QrVSPlCItsSrcNivel2: TLargeintField;
    QrVSPlCItsSrcGGX: TLargeintField;
    QrVSPlCItsSdoVrtPeca: TFloatField;
    QrVSPlCItsSdoVrtPeso: TFloatField;
    QrVSPlCItsSdoVrtArM2: TFloatField;
    QrVSPlCItsObserv: TWideStringField;
    QrVSPlCItsSerieFch: TLargeintField;
    QrVSPlCItsFicha: TLargeintField;
    QrVSPlCItsMisturou: TLargeintField;
    QrVSPlCItsFornecMO: TLargeintField;
    QrVSPlCItsCustoMOKg: TFloatField;
    QrVSPlCItsCustoMOM2: TFloatField;
    QrVSPlCItsCustoMOTot: TFloatField;
    QrVSPlCItsValorMP: TFloatField;
    QrVSPlCItsDstMovID: TLargeintField;
    QrVSPlCItsDstNivel1: TLargeintField;
    QrVSPlCItsDstNivel2: TLargeintField;
    QrVSPlCItsDstGGX: TLargeintField;
    QrVSPlCItsQtdGerPeca: TFloatField;
    QrVSPlCItsQtdGerPeso: TFloatField;
    QrVSPlCItsQtdGerArM2: TFloatField;
    QrVSPlCItsQtdGerArP2: TFloatField;
    QrVSPlCItsQtdAntPeca: TFloatField;
    QrVSPlCItsQtdAntPeso: TFloatField;
    QrVSPlCItsQtdAntArM2: TFloatField;
    QrVSPlCItsQtdAntArP2: TFloatField;
    QrVSPlCItsNotaMPAG: TFloatField;
    QrVSPlCItsPedItsFin: TLargeintField;
    QrVSPlCItsMarca: TWideStringField;
    QrVSPlCItsStqCenLoc: TLargeintField;
    QrVSPlCItsNO_PALLET: TWideStringField;
    QrVSPlCItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPlCItsNO_TTW: TWideStringField;
    QrVSPlCItsID_TTW: TLargeintField;
    QrVSPlCItsReqMovEstq: TLargeintField;
    QrVSPlCItsRendKgm2: TFloatField;
    QrVSPlCItsNotaMPAG_TXT: TWideStringField;
    QrVSPlCItsRendKgm2_TXT: TWideStringField;
    QrVSPlCItsMisturou_TXT: TWideStringField;
    QrVSPlCItsNOMEUNIDMED: TWideStringField;
    QrVSPlCItsSIGLAUNIDMED: TWideStringField;
    QrVSPlCItsm2_CouroTXT: TWideStringField;
    QrVSPlCItsKgMedioCouro: TFloatField;
    QrVSPlCItsSEQ: TIntegerField;
    QrVSPlCCabTemIMEIMrt: TIntegerField;
    QrVSPlCItsClientMO: TLargeintField;
    Edide_serie: TdmkEdit;
    Label28: TLabel;
    Label29: TLabel;
    Edide_nNF: TdmkEdit;
    QrVSPlCCabide_serie: TIntegerField;
    QrVSPlCCabide_nNF: TIntegerField;
    Label9: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label30: TLabel;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    Edemi_serie: TdmkEdit;
    Label31: TLabel;
    Label32: TLabel;
    Edemi_nNF: TdmkEdit;
    QrVSPlCCabemi_serie: TIntegerField;
    QrVSPlCCabemi_nNF: TIntegerField;
    Label33: TLabel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    Label35: TLabel;
    Rendimentodesemiacabado1: TMenuItem;
    QrVSPlCCabNFeStatus: TIntegerField;
    QrGraGru1: TMySQLQuery;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1IPI_CST: TSmallintField;
    QrGraGru1PIS_CST: TSmallintField;
    QrGraGru1COFINS_CST: TSmallintField;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1IPIRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    BtFiscal: TBitBtn;
    PMFiscal: TPopupMenu;
    IncluiDocumento1: TMenuItem;
    AlteraDocumento1: TMenuItem;
    ExcluiDocumento1: TMenuItem;
    N4: TMenuItem;
    IncluiItemdodocumento1: TMenuItem;
    AlteraoItemselecionadododocumento1: TMenuItem;
    ExcluioItemselecionadododocumento1: TMenuItem;
    N5: TMenuItem;
    otalizarfisicopeloescritural1: TMenuItem;
    TabSheet3: TTabSheet;
    Splitter1: TSplitter;
    QrVSPlCItsGrandeza: TSmallintField;
    QrVSPlCCabUF_EMPRESA: TSmallintField;
    QrVSPlCCabUF_FORNECE: TSmallintField;
    QrEfdInnNFsCab: TMySQLQuery;
    QrEfdInnNFsCabNO_TER: TWideStringField;
    QrEfdInnNFsCabMovFatID: TIntegerField;
    QrEfdInnNFsCabMovFatNum: TIntegerField;
    QrEfdInnNFsCabMovimCod: TIntegerField;
    QrEfdInnNFsCabEmpresa: TIntegerField;
    QrEfdInnNFsCabControle: TIntegerField;
    QrEfdInnNFsCabTerceiro: TIntegerField;
    QrEfdInnNFsCabPecas: TFloatField;
    QrEfdInnNFsCabPesoKg: TFloatField;
    QrEfdInnNFsCabAreaM2: TFloatField;
    QrEfdInnNFsCabAreaP2: TFloatField;
    QrEfdInnNFsCabValorT: TFloatField;
    QrEfdInnNFsCabMotorista: TIntegerField;
    QrEfdInnNFsCabPlaca: TWideStringField;
    QrEfdInnNFsCabCOD_MOD: TSmallintField;
    QrEfdInnNFsCabCOD_SIT: TSmallintField;
    QrEfdInnNFsCabSER: TIntegerField;
    QrEfdInnNFsCabNUM_DOC: TIntegerField;
    QrEfdInnNFsCabCHV_NFE: TWideStringField;
    QrEfdInnNFsCabNFeStatus: TIntegerField;
    QrEfdInnNFsCabDT_DOC: TDateField;
    QrEfdInnNFsCabDT_E_S: TDateField;
    QrEfdInnNFsCabVL_DOC: TFloatField;
    QrEfdInnNFsCabIND_PGTO: TWideStringField;
    QrEfdInnNFsCabVL_DESC: TFloatField;
    QrEfdInnNFsCabVL_ABAT_NT: TFloatField;
    QrEfdInnNFsCabVL_MERC: TFloatField;
    QrEfdInnNFsCabIND_FRT: TWideStringField;
    QrEfdInnNFsCabVL_FRT: TFloatField;
    QrEfdInnNFsCabVL_SEG: TFloatField;
    QrEfdInnNFsCabVL_OUT_DA: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS: TFloatField;
    QrEfdInnNFsCabVL_ICMS: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_IPI: TFloatField;
    QrEfdInnNFsCabVL_PIS: TFloatField;
    QrEfdInnNFsCabVL_COFINS: TFloatField;
    QrEfdInnNFsCabVL_PIS_ST: TFloatField;
    QrEfdInnNFsCabVL_COFINS_ST: TFloatField;
    QrEfdInnNFsCabNFe_FatID: TIntegerField;
    QrEfdInnNFsCabNFe_FatNum: TIntegerField;
    QrEfdInnNFsCabNFe_StaLnk: TSmallintField;
    QrEfdInnNFsCabVSVmcWrn: TSmallintField;
    QrEfdInnNFsCabVSVmcObs: TWideStringField;
    QrEfdInnNFsCabVSVmcSeq: TWideStringField;
    QrEfdInnNFsCabVSVmcSta: TSmallintField;
    QrEfdInnNFsCabCliInt: TIntegerField;
    DsEfdInnNFsCab: TDataSource;
    QrEfdInnNFsIts: TMySQLQuery;
    QrEfdInnNFsItsMovFatID: TIntegerField;
    QrEfdInnNFsItsMovFatNum: TIntegerField;
    QrEfdInnNFsItsMovimCod: TIntegerField;
    QrEfdInnNFsItsEmpresa: TIntegerField;
    QrEfdInnNFsItsMovimNiv: TIntegerField;
    QrEfdInnNFsItsMovimTwn: TIntegerField;
    QrEfdInnNFsItsControle: TIntegerField;
    QrEfdInnNFsItsConta: TIntegerField;
    QrEfdInnNFsItsGraGru1: TIntegerField;
    QrEfdInnNFsItsGraGruX: TIntegerField;
    QrEfdInnNFsItsprod_vProd: TFloatField;
    QrEfdInnNFsItsprod_vFrete: TFloatField;
    QrEfdInnNFsItsprod_vSeg: TFloatField;
    QrEfdInnNFsItsprod_vOutro: TFloatField;
    QrEfdInnNFsItsQTD: TFloatField;
    QrEfdInnNFsItsUNID: TWideStringField;
    QrEfdInnNFsItsVL_ITEM: TFloatField;
    QrEfdInnNFsItsVL_DESC: TFloatField;
    QrEfdInnNFsItsIND_MOV: TWideStringField;
    QrEfdInnNFsItsCFOP: TIntegerField;
    QrEfdInnNFsItsCOD_NAT: TWideStringField;
    QrEfdInnNFsItsVL_BC_ICMS: TFloatField;
    QrEfdInnNFsItsALIQ_ICMS: TFloatField;
    QrEfdInnNFsItsVL_ICMS: TFloatField;
    QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsItsALIQ_ST: TFloatField;
    QrEfdInnNFsItsVL_ICMS_ST: TFloatField;
    QrEfdInnNFsItsIND_APUR: TWideStringField;
    QrEfdInnNFsItsCOD_ENQ: TWideStringField;
    QrEfdInnNFsItsVL_BC_IPI: TFloatField;
    QrEfdInnNFsItsALIQ_IPI: TFloatField;
    QrEfdInnNFsItsVL_IPI: TFloatField;
    QrEfdInnNFsItsVL_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_r: TFloatField;
    QrEfdInnNFsItsVL_PIS: TFloatField;
    QrEfdInnNFsItsVL_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_r: TFloatField;
    QrEfdInnNFsItsVL_COFINS: TFloatField;
    QrEfdInnNFsItsCOD_CTA: TWideStringField;
    QrEfdInnNFsItsVL_ABAT_NT: TFloatField;
    QrEfdInnNFsItsDtCorrApo: TDateTimeField;
    QrEfdInnNFsItsxLote: TWideStringField;
    QrEfdInnNFsItsOri_IPIpIPI: TFloatField;
    QrEfdInnNFsItsOri_IPIvIPI: TFloatField;
    QrEfdInnNFsItsNO_PRD_TAM_COR: TWideStringField;
    QrEfdInnNFsItsSIGLAUNIDMED: TWideStringField;
    QrEfdInnNFsItsGerBxaEstq: TSmallintField;
    QrEfdInnNFsItsNCM: TWideStringField;
    QrEfdInnNFsItsUnidMed: TIntegerField;
    QrEfdInnNFsItsEx_TIPI: TWideStringField;
    QrEfdInnNFsItsGrandeza: TSmallintField;
    QrEfdInnNFsItsTipo_Item: TSmallintField;
    DsEfdInnNFsIts: TDataSource;
    DBGInnNFsCab: TDBGrid;
    DBGInnNFsIts: TDBGrid;
    QrEfdInnCTsCab: TMySQLQuery;
    QrEfdInnCTsCabMovFatID: TIntegerField;
    QrEfdInnCTsCabMovFatNum: TIntegerField;
    QrEfdInnCTsCabMovimCod: TIntegerField;
    QrEfdInnCTsCabEmpresa: TIntegerField;
    QrEfdInnCTsCabControle: TIntegerField;
    QrEfdInnCTsCabIsLinked: TSmallintField;
    QrEfdInnCTsCabSqLinked: TIntegerField;
    QrEfdInnCTsCabCliInt: TIntegerField;
    QrEfdInnCTsCabTerceiro: TIntegerField;
    QrEfdInnCTsCabPecas: TFloatField;
    QrEfdInnCTsCabPesoKg: TFloatField;
    QrEfdInnCTsCabAreaM2: TFloatField;
    QrEfdInnCTsCabAreaP2: TFloatField;
    QrEfdInnCTsCabValorT: TFloatField;
    QrEfdInnCTsCabMotorista: TIntegerField;
    QrEfdInnCTsCabPlaca: TWideStringField;
    QrEfdInnCTsCabIND_OPER: TWideStringField;
    QrEfdInnCTsCabIND_EMIT: TWideStringField;
    QrEfdInnCTsCabCOD_MOD: TWideStringField;
    QrEfdInnCTsCabCOD_SIT: TSmallintField;
    QrEfdInnCTsCabSER: TWideStringField;
    QrEfdInnCTsCabSUB: TWideStringField;
    QrEfdInnCTsCabNUM_DOC: TIntegerField;
    QrEfdInnCTsCabCHV_CTE: TWideStringField;
    QrEfdInnCTsCabCTeStatus: TIntegerField;
    QrEfdInnCTsCabDT_DOC: TDateField;
    QrEfdInnCTsCabDT_A_P: TDateField;
    QrEfdInnCTsCabTP_CT_e: TSmallintField;
    QrEfdInnCTsCabCHV_CTE_REF: TWideStringField;
    QrEfdInnCTsCabVL_DOC: TFloatField;
    QrEfdInnCTsCabVL_DESC: TFloatField;
    QrEfdInnCTsCabIND_FRT: TWideStringField;
    QrEfdInnCTsCabVL_SERV: TFloatField;
    QrEfdInnCTsCabVL_BC_ICMS: TFloatField;
    QrEfdInnCTsCabVL_ICMS: TFloatField;
    QrEfdInnCTsCabVL_NT: TFloatField;
    QrEfdInnCTsCabCOD_INF: TWideStringField;
    QrEfdInnCTsCabCOD_CTA: TWideStringField;
    QrEfdInnCTsCabCOD_MUN_ORIG: TIntegerField;
    QrEfdInnCTsCabCOD_MUN_DEST: TIntegerField;
    QrEfdInnCTsCabNFe_FatID: TIntegerField;
    QrEfdInnCTsCabNFe_FatNum: TIntegerField;
    QrEfdInnCTsCabNFe_StaLnk: TSmallintField;
    QrEfdInnCTsCabVSVmcWrn: TSmallintField;
    QrEfdInnCTsCabVSVmcObs: TWideStringField;
    QrEfdInnCTsCabVSVmcSeq: TWideStringField;
    QrEfdInnCTsCabVSVmcSta: TSmallintField;
    QrEfdInnCTsCabCST_ICMS: TIntegerField;
    QrEfdInnCTsCabCFOP: TIntegerField;
    QrEfdInnCTsCabALIQ_ICMS: TFloatField;
    QrEfdInnCTsCabVL_OPR: TFloatField;
    QrEfdInnCTsCabVL_RED_BC: TFloatField;
    QrEfdInnCTsCabCOD_OBS: TWideStringField;
    QrEfdInnCTsCabNO_TER: TWideStringField;
    DsEfdInnCTsCab: TDataSource;
    TabSheet4: TTabSheet;
    DBGFretes: TDBGrid;
    BtCTe: TBitBtn;
    PMCTe: TPopupMenu;
    IncluiConhecimentodefrete1: TMenuItem;
    AlteraConhecimentodefrete1: TMenuItem;
    ExcluiConhecimentodefrete1: TMenuItem;
    QrEfdInnNFsItsCST_IPI: TWideStringField;
    QrEfdInnNFsItsCST_PIS: TWideStringField;
    QrEfdInnNFsItsCST_COFINS: TWideStringField;
    QrEfdInnNFsItsCST_ICMS: TWideStringField;
    QrEfdInnNFsCabRegrFiscal: TIntegerField;
    QrEfdInnNFsCabTpEntrd: TIntegerField;
    QrEfdInnCTsCabRegrFiscal: TIntegerField;
    QrEfdInnCTsCabIND_NAT_FRT: TWideStringField;
    QrEfdInnCTsCabVL_ITEM: TFloatField;
    QrEfdInnCTsCabCST_PIS: TWideStringField;
    QrEfdInnCTsCabNAT_BC_CRED: TWideStringField;
    QrEfdInnCTsCabVL_BC_PIS: TFloatField;
    QrEfdInnCTsCabALIQ_PIS: TFloatField;
    QrEfdInnCTsCabVL_PIS: TFloatField;
    QrEfdInnCTsCabCST_COFINS: TWideStringField;
    QrEfdInnCTsCabVL_BC_COFINS: TFloatField;
    QrEfdInnCTsCabALIQ_COFINS: TFloatField;
    QrEfdInnCTsCabVL_COFINS: TFloatField;
    QrVSPlCItsCustoM2: TFloatField;
    EdPecas: TdmkEdit;
    LaPecas: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    EdPesoKg: TdmkEdit;
    LaPeso: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSPlCCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPlCCabBeforeOpen(DataSet: TDataSet);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSPlCCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSPlCCabBeforeClose(DataSet: TDataSet);
    procedure QrVSPlCItsBeforeClose(DataSet: TDataSet);
    procedure QrVSPlCItsAfterScroll(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
    procedure SbFornecedorClick(Sender: TObject);
    procedure SbTransportadorClick(Sender: TObject);
    procedure Atualizaestoque1Click(Sender: TObject);
    procedure QrVSItsBxaBeforeClose(DataSet: TDataSet);
    procedure QrVSItsBxaAfterScroll(DataSet: TDataSet);
    procedure AnliseMPAG1Click(Sender: TObject);
    procedure frxWET_CURTI_006_01GetValue(const VarName: string;
      var Value: Variant);
    procedure SbClienteMOClick(Sender: TObject);
    procedure SbProcedencClick(Sender: TObject);
    procedure SbMotoristaClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure IrparajaneladegerenciamentodeFichaRMP1Click(Sender: TObject);
    procedure FichaCOMnomedoPallet1Click(Sender: TObject);
    procedure FichaSEMnomedoPallet1Click(Sender: TObject);
    procedure FichasCOMnomedoPallet1Click(Sender: TObject);
    procedure FichasSEMnomedoPallet1Click(Sender: TObject);
    procedure Rendimentodesemiacabado1Click(Sender: TObject);
    procedure QrEfdInnNFsCabBeforeClose(DataSet: TDataSet);
    procedure QrEfdInnNFsCabAfterScroll(DataSet: TDataSet);
    procedure BtFiscalClick(Sender: TObject);
    procedure PMFiscalPopup(Sender: TObject);
    procedure IncluiDocumento1Click(Sender: TObject);
    procedure AlteraDocumento1Click(Sender: TObject);
    procedure ExcluiDocumento1Click(Sender: TObject);
    procedure IncluiItemdodocumento1Click(Sender: TObject);
    procedure AlteraoItemselecionadododocumento1Click(Sender: TObject);
    procedure ExcluioItemselecionadododocumento1Click(Sender: TObject);
    procedure BtCTeClick(Sender: TObject);
    procedure IncluiConhecimentodefrete1Click(Sender: TObject);
    procedure AlteraConhecimentodefrete1Click(Sender: TObject);
    procedure ExcluiConhecimentodefrete1Click(Sender: TObject);
    procedure PMCTePopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSPlCIts(SQLType: TSQLType);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure MostraFormVSHisFchAdd(SQLType: TSQLType);
    procedure ImprimePallet(InfoNO_Pallet: Boolean);
    procedure ImprimeTodosPallets(InfoNO_PALLET: Boolean);
    // ini EfdInnNFs
    procedure MostraFormEfdInnNFsCab(SQLType: TSQLType);
    procedure MostraFormEfdInnNFsIts(SQLType: TSQLType);
    procedure ReopenEFdInnNFsCab(Controle: Integer);
    procedure ReopenEfdInnNFsIts(Conta: Integer);
    // fim EfdInnNFs
    // ini EfdInnCTs
    procedure MostraFormEfdInnCTsCab(SQLType: TSQLType);
    procedure MostraFormEfdInnCTsIts(SQLType: TSQLType);
    procedure ReopenEFdInnCTsCab(Controle: Integer);
    // fim EfdInnCTs

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure AtualizaNFeItens();
    procedure ReopenVSPlCIts(Controle: Integer);
    procedure ReopenVSItsBxa(Controle: Integer);
    procedure ReopenVSHisFch(Codigo: Integer);

  end;

var
  FmVSPlCCab: TFmVSPlCCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSPlCIts, ModuleGeral,
  {$IfDef sAllVS} VSMovImp, VSHisFchAdd, UnVS_PF, {$EndIf}
  Principal, UnVS_CRC_PF, CreateVS, ModVS_CRC, ModuleNFe_0000,
  EfdInnNFsCab, EfdInnNFsIts, ModProd, EfdInnCTsCab;

{$R *.DFM}

const
  FEFDInnNFSMainFatID = VAR_FATID_1041;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSPlCCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSPlCCab.MostraFormEfdInnCTsCab(SQLType: TSQLType);
const
  Frete = 0.00;
var
  NewControle, Controle: Integer;
begin
{$IfDef sAllVS}
  NewControle := 0;
  Controle := QrEfdInnCTsCabControle.Value;
  //
  if DBCheck.CriaFm(TFmEfdInnCTsCab, FmEfdInnCTsCab, afmoNegarComAviso) then
  begin
    FmEfdInnCTsCab.ImgTipo.SQLType := SQLType;
    FmEfdInnCTsCab.FQrIts                         := QrEfdInnCTsCab;
    FmEfdInnCTsCab.EdMovFatID.ValueVariant        := FEFDInnNFsMainFatID;
    FmEfdInnCTsCab.EdMovFatNum.ValueVariant       := QrVSPlCCabCodigo.Value;
    FmEfdInnCTsCab.EdMovimCod.ValueVariant        := QrVSPlCCabMovimCod.Value; // N�o tem!!! QrPQEMovimCod.Value;
    FmEfdInnCTsCab.EdEmpresa.ValueVariant         := QrVSPlCCabEmpresa.Value;
    //FmEfdInnCTsCab.EdSerieFch.ValueVariant        := QrPQEItsSerieFch.Value;
    //FmEfdInnCTsCab.EdFicha.ValueVariant           := QrPQEItsFicha.Value;
    FmEfdInnCTsCab.EdControle.ValueVariant           := 0;
    if (SQLType = stIns) and (QrVSPlCIts.RecordCount > 0) then
    begin
      FmEfdInnCTsCab.EdControle.ValueVariant      := 0;
      FmEfdInnCTsCab.EdPecas.ValueVariant         := QrVSPlCCabPecas.Value;
      FmEfdInnCTsCab.EdPesoKg.ValueVariant        := QrVSPlCCabPesoKg.Value;
      FmEfdInnCTsCab.EdAreaM2.ValueVariant        := QrVSPlCCabAreaM2.Value;
      FmEfdInnCTsCab.EdAreaP2.ValueVariant        := QrVSPlCCabAreaP2.Value;
      FmEfdInnCTsCab.EdValorT.ValueVariant        :=  QrVSPlCCabValorT.Value;
      FmEfdInnCTsCab.EdMotorista.ValueVariant     := 0;
      FmEfdInnCTsCab.CBMotorista.KeyValue         := 0;
      FmEfdInnCTsCab.EdPlaca.ValueVariant         := '';
      FmEfdInnCTsCab.EdCOD_SIT.ValueVariant       := 0; // Documento regular
      FmEfdInnCTsCab.EdCOD_MOD.ValueVariant       := '57';
      (*if QrPQECI.Value = QrPQEEmpresa.Value then
      begin*)
        FmEfdInnCTsCab.EdSER.ValueVariant           := 0; //QrPQECTe_serie.Value;
        FmEfdInnCTsCab.EdNUM_DOC.ValueVariant       := 0; //QrPQEConhecimento.Value;
        FmEfdInnCTsCab.EdCHV_CTE.ValueVariant       := ''; //QrPQECte_Id.Value;
      (*end else
      begin
        FmEfdInnCTsCab.EdSER.ValueVariant           := 0;
        FmEfdInnCTsCab.EdNUM_DOC.ValueVariant       := ?;
        FmEfdInnCTsCab.EdCHV_NFE.ValueVariant       := '';
      end;*)
      //FmEfdInnCTsCab.EdNFeStatus.ValueVariant
      FmEfdInnCTsCab.TPDT_DOC.Date                := QrVSPlCCabDtCompra.Value;
      FmEfdInnCTsCab.TPDT_A_P.Date                := QrVSPlCCabDtEntrada.Value;

      FmEfdInnCTsCab.EdCliInt.ValueVariant        := QrVSPlCCabClienteMO.Value;
      FmEfdInnCTsCab.CBCliInt.KeyValue            := QrVSPlCCabClienteMO.Value;

      FmEfdInnCTsCab.EdTransportador.ValueVariant    := QrVSPlCCabTransporta.Value;
      FmEfdInnCTsCab.CBTransportador.KeyValue        := QrVSPlCCabTransporta.Value;

      FmEfdInnCTsCab.EdVL_DOC.ValueVariant        := Frete;
      //FmEfdInnCTsCab.EdIND_PGTO.ValueVariant      := 1; //� Prazo
      FmEfdInnCTsCab.EdVL_DESC.ValueVariant       := 0.00;
      //FmEfdInnCTsCab.EdVL_ABAT_NT.ValueVariant    := 0.00;  // Abatimento n�o comercia
      FmEfdInnCTsCab.EdVL_SERV.ValueVariant       := Frete;
      FmEfdInnCTsCab.EdVL_BC_ICMS.ValueVariant      := Frete;
      FmEfdInnCTsCab.EdALIQ_ICMS.ValueVariant     := 0.00;
      //FmEfdInnCTsCab.EdIND_FRT.ValueVariant       := 1; // Contrata��o de frete por conta do destinat�rio
(*
      FmEfdInnCTsCab.EdVL_FRT.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_SEG.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_OUT_DA.ValueVariant     := 0.00;
      FmEfdInnCTsCab.EdVL_BC_ICMS.ValueVariant    := QrVSMovDifInfValorT.Value;;
      FmEfdInnCTsCab.EdVL_ICMS.ValueVariant       := 0.00;
      FmEfdInnCTsCab.EdVL_BC_ICMS_ST.ValueVariant := 0.00;
      FmEfdInnCTsCab.EdVL_ICMS_ST.ValueVariant    := 0.00;
      FmEfdInnCTsCab.EdVL_IPI.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_PIS.ValueVariant        := 0.00;
      FmEfdInnCTsCab.EdVL_COFINS.ValueVariant     := 0.00;
      FmEfdInnCTsCab.EdVL_PIS_ST.ValueVariant     := 0.00;
      FmEfdInnCTsCab.EdVL_COFINS_ST.ValueVariant  := 0.00;
*)
      FmEfdInnCTsCab.EdNFe_FatID.ValueVariant         := 0;
      FmEfdInnCTsCab.EdNFe_FatNum.ValueVariant        := 0;
      FmEfdInnCTsCab.EdNFe_StaLnk.ValueVariant        := 0;
      //
    end else
    begin
      FmEfdInnCTsCab.EdControle.ValueVariant      := QrEfdInnCTsCabControle.Value;
      FmEfdInnCTsCab.EdPecas.ValueVariant         := QrEfdInnCTsCabPecas.Value;
      FmEfdInnCTsCab.EdPesoKg.ValueVariant        := QrEfdInnCTsCabPesoKg.Value;
      FmEfdInnCTsCab.EdAreaM2.ValueVariant        := QrEfdInnCTsCabAreaM2.Value;
      FmEfdInnCTsCab.EdAreaP2.ValueVariant        := QrEfdInnCTsCabAreaP2.Value;
      FmEfdInnCTsCab.EdValorT.ValueVariant        := QrEfdInnCTsCabValorT.Value;
      FmEfdInnCTsCab.EdMotorista.ValueVariant     := QrEfdInnCTsCabMotorista.Value;
      FmEfdInnCTsCab.CBMotorista.KeyValue         := QrEfdInnCTsCabMotorista.Value;
      FmEfdInnCTsCab.EdPlaca.ValueVariant         := QrEfdInnCTsCabPlaca.Value;
      FmEfdInnCTsCab.EdCOD_SIT.ValueVariant       := QrEfdInnCTsCabCOD_SIT.Value;
      //FmEfdInnCTsCab.EdNFeStatus.ValueVariant     := QrVSInnCTsCEdNFeStaab.Value;
      FmEfdInnCTsCab.TPDT_DOC.Date                := QrEfdInnCTsCabDT_DOC.Value;
      FmEfdInnCTsCab.TPDT_A_P.Date                := QrEfdInnCTsCabDT_A_P.Value;
      //
      FmEfdInnCTsCab.EdCliInt.ValueVariant        := QrEfdInnCTsCabCliInt.Value;
      FmEfdInnCTsCab.CBCliInt.KeyValue            := QrEfdInnCTsCabCliInt.Value;
      //
      FmEfdInnCTsCab.EdTransportador.ValueVariant := QrEfdInnCTsCabTerceiro.Value;
      FmEfdInnCTsCab.CBTransportador.KeyValue     := QrEfdInnCTsCabTerceiro.Value;
      //
      FmEfdInnCTsCab.EdCFOP.ValueVariant          := QrEfdInnCTsCabCFOP.Value;
      FmEfdInnCTsCab.CBCFOP.KeyValue              := QrEfdInnCTsCabCFOP.Value;
      //
      FmEfdInnCTsCab.EdICMS_CST.ValueVariant      := QrEfdInnCTsCabCST_ICMS.Value;
      //
      //FmEfdInnCTsCab.EdIND_PGTO.ValueVariant      := QrEfdInnCTsCabIND_PGTO.Value;
      FmEfdInnCTsCab.EdIND_FRT.ValueVariant       := QrEfdInnCTsCabIND_FRT.Value;
      FmEfdInnCTsCab.EdVL_SERV.ValueVariant       := QrEfdInnCTsCabVL_SERV.Value;
      FmEfdInnCTsCab.EdVL_DESC.ValueVariant       := QrEfdInnCTsCabVL_DESC.Value;
      FmEfdInnCTsCab.EdVL_NT.ValueVariant         := QrEfdInnCTsCabVL_NT.Value;  // Abatimento n�o comercia


      FmEfdInnCTsCab.EdVL_RED_BC.ValueVariant     := QrEfdInnCTsCabVL_RED_BC.Value;
      FmEfdInnCTsCab.EdVL_BC_ICMS.ValueVariant    := QrEfdInnCTsCabVL_BC_ICMS.Value;
      FmEfdInnCTsCab.EdALIQ_ICMS.ValueVariant     := QrEfdInnCTsCabALIQ_ICMS.Value;
      FmEfdInnCTsCab.EdVL_ICMS.ValueVariant       := QrEfdInnCTsCabVL_ICMS.Value;
      FmEfdInnCTsCab.EdVL_OPR.ValueVariant        := QrEfdInnCTsCabVL_OPR.Value;
      FmEfdInnCTsCab.EdVL_DOC.ValueVariant        := QrEfdInnCTsCabVL_DOC.Value;

      FmEfdInnCTsCab.EdCOD_MOD.ValueVariant       := QrEfdInnCTsCabCOD_MOD.Value;
      FmEfdInnCTsCab.EdSER.ValueVariant           := QrEfdInnCTsCabSER.Value;
      FmEfdInnCTsCab.EdSUB.ValueVariant           := QrEfdInnCTsCabSUB.Value;
      FmEfdInnCTsCab.EdNUM_DOC.ValueVariant       := QrEfdInnCTsCabNUM_DOC.Value;
      FmEfdInnCTsCab.EdCHV_CTE.ValueVariant       := QrEfdInnCTsCabCHV_CTE.Value;

      FmEfdInnCTsCab.EdCOD_SIT.ValueVariant       := QrEfdInnCTsCabCOD_SIT.Value;

      FmEfdInnCTsCab.EdTP_CT_e.ValueVariant       := QrEfdInnCTsCabTP_CT_e.Value;


      FmEfdInnCTsCab.EdCOD_MUN_ORIG.ValueVariant  := QrEfdInnCTsCabCOD_MUN_ORIG.Value;
      FmEfdInnCTsCab.CBCOD_MUN_ORIG.KeyValue      := QrEfdInnCTsCabCOD_MUN_ORIG.Value;

      FmEfdInnCTsCab.EdCOD_MUN_DEST.ValueVariant  := QrEfdInnCTsCabCOD_MUN_DEST.Value;
      FmEfdInnCTsCab.CBCOD_MUN_DEST.KeyValue      := QrEfdInnCTsCabCOD_MUN_DEST.Value;

      FmEfdInnCTsCab.EdRegrFiscal.ValueVariant  := QrEfdInnCTsCabRegrFiscal.Value;
      FmEfdInnCTsCab.CBRegrFiscal.KeyValue      := QrEfdInnCTsCabRegrFiscal.Value;


      FmEfdInnCTsCab.EdIND_NAT_FRT.ValueVariant     := QrEfdInnCTsCabIND_NAT_FRT.Value;
      //FmEfdInnCTsCab.EdVL_ITEM.ValueVariant         := QrEfdInnCTsCabVL_ITEM.Value;
      FmEfdInnCTsCab.EdCST_PIS.ValueVariant         := QrEfdInnCTsCabCST_PIS.Value;
      FmEfdInnCTsCab.EdNAT_BC_CRED.ValueVariant     := QrEfdInnCTsCabNAT_BC_CRED.Value;
      FmEfdInnCTsCab.EdVL_BC_PIS.ValueVariant       := QrEfdInnCTsCabVL_BC_PIS.Value;
      FmEfdInnCTsCab.EdALIQ_PIS.ValueVariant        := QrEfdInnCTsCabALIQ_PIS.Value;
      FmEfdInnCTsCab.EdVL_PIS.ValueVariant          := QrEfdInnCTsCabVL_PIS.Value;
      FmEfdInnCTsCab.EdCST_COFINS.ValueVariant      := QrEfdInnCTsCabCST_COFINS.Value;
      FmEfdInnCTsCab.EdVL_BC_COFINS.ValueVariant    := QrEfdInnCTsCabVL_BC_COFINS.Value;
      FmEfdInnCTsCab.EdALIQ_COFINS.ValueVariant     := QrEfdInnCTsCabALIQ_COFINS.Value;
      FmEfdInnCTsCab.EdVL_COFINS.ValueVariant       := QrEfdInnCTsCabVL_COFINS.Value;

   end;
    FmEfdInnCTsCab.ShowModal;
    NewControle := FmEfdInnCTsCab.FControle;
    FmEfdInnCTsCab.Destroy;
    //
    ReopenEfdInnCTsCab(NewControle);
    if (SQLType = stIns) and (NewControle > Controle) then
    begin
      if QrEfdInnCTsCabControle.Value = NewControle then
        MostraFormEfdInnCTsIts(stIns);
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPlCCab.MostraFormEfdInnCTsIts(SQLType: TSQLType);
begin
// Fazer no futuro?
end;

procedure TFmVSPlCCab.MostraFormEfdInnNFsCab(SQLType: TSQLType);
var
  NewControle, Controle: Integer;
begin
{$IfDef sAllVS}
  NewControle := 0;
  Controle := QrEfdInnNFsCabControle.Value;
  //
  if DBCheck.CriaFm(TFmEfdInnNFsCab, FmEfdInnNFsCab, afmoNegarComAviso) then
  begin
////////////////////////////////////////////////////////////////////////////////////
    FmEfdInnNFsCab.LaAreaM2.Enabled               := True;
    FmEfdInnNFsCab.EdAreaM2.Enabled               := True;
    FmEfdInnNFsCab.LaAreaP2.Enabled               := True;
    FmEfdInnNFsCab.EdAreaP2.Enabled               := True;
////////////////////////////////////////////////////////////////////////////////////
    FmEfdInnNFsCab.ImgTipo.SQLType := SQLType;
    FmEfdInnNFsCab.FQrIts                         := QrEfdInnNFsCab;
    FmEfdInnNFsCab.EdMovFatID.ValueVariant        := FEFDInnNFSMainFatID;
    FmEfdInnNFsCab.EdMovFatNum.ValueVariant       := QrVSPlCCabCodigo.Value;
    FmEfdInnNFsCab.EdMovimCod.ValueVariant        := QrVSPlCCabMovimCod.Value;
    FmEfdInnNFsCab.EdEmpresa.ValueVariant         := QrVSPlCCabEmpresa.Value;
    FmEfdInnNFsCab.FFornecedor                    := QrVSPlCCabFornecedor.Value;
    //FmEfdInnNFsCab.EdSerieFch.ValueVariant        := QrVSPlCItsSerieFch.Value;
    //FmEfdInnNFsCab.EdFicha.ValueVariant           := QrVSPlCItsFicha.Value;
    FmEfdInnNFsCab.EdControle.ValueVariant           := 0;
    if (SQLType = stIns) and (QrVSPlCIts.RecordCount > 0) then
    begin
      FmEfdInnNFsCab.EdControle.ValueVariant      := 0;
      FmEfdInnNFsCab.EdPecas.ValueVariant         := QrVSMovDifInfPecas.Value;
      FmEfdInnNFsCab.EdPesoKg.ValueVariant        := QrVSMovDifInfPesoKg.Value;
      FmEfdInnNFsCab.EdAreaM2.ValueVariant        := QrVSMovDifInfAreaM2.Value;
      FmEfdInnNFsCab.EdAreaP2.ValueVariant        := QrVSMovDifInfAreaP2.Value;
      FmEfdInnNFsCab.EdValorT.ValueVariant        := QrVSMovDifInfValorT.Value;
      FmEfdInnNFsCab.EdMotorista.ValueVariant     := QrVSPlCCabMotorista.Value;
      FmEfdInnNFsCab.CBMotorista.KeyValue         := QrVSPlCCabMotorista.Value;
      FmEfdInnNFsCab.EdPlaca.ValueVariant         := QrVSPlCCabPlaca.Value;
      FmEfdInnNFsCab.EdCOD_SIT.ValueVariant       := 0; // Documento regular
      FmEfdInnNFsCab.EdCOD_MOD.ValueVariant       := 55;
      FmEfdInnNFsCab.EdSER.ValueVariant           := QrVSPlCCabemi_serie.Value;
      FmEfdInnNFsCab.EdNUM_DOC.ValueVariant       := QrVSPlCCabemi_nNF.Value;
      FmEfdInnNFsCab.EdCHV_NFE.ValueVariant       := '';
      //FmEfdInnNFsCab.EdNFeStatus.ValueVariant
      FmEfdInnNFsCab.TPDT_DOC.Date                := QrVSPlCCabDtCompra.Value;
      FmEfdInnNFsCab.TPDT_E_S.Date                := QrVSPlCCabDtEntrada.Value;

      FmEfdInnNFsCab.EdCliInt.ValueVariant    := QrVSPlCCabClienteMO.Value;
      FmEfdInnNFsCab.CBCliInt.KeyValue        := QrVSPlCCabClienteMO.Value;

      FmEfdInnNFsCab.EdFornecedor.ValueVariant    := QrVSPlCCabFornecedor.Value;
      FmEfdInnNFsCab.CBFornecedor.KeyValue        := QrVSPlCCabFornecedor.Value;

      FmEfdInnNFsCab.EdTransportador.ValueVariant    := QrVSPlCCabTransporta.Value;
      FmEfdInnNFsCab.CBTransportador.KeyValue        := QrVSPlCCabTransporta.Value;

      FmEfdInnNFsCab.EdVL_DOC.ValueVariant        := QrVSMovDifInfValorT.Value;
      FmEfdInnNFsCab.EdIND_PGTO.ValueVariant      := 1; //� Prazo
      FmEfdInnNFsCab.EdVL_DESC.ValueVariant       := 0.00;
      //FmEfdInnNFsCab.EdVL_ABAT_NT.ValueVariant    := 0.00;  // Abatimento n�o comercia
      FmEfdInnNFsCab.EdVL_MERC.ValueVariant       := QrVSMovDifInfValorT.Value;
      //FmEfdInnNFsCab.EdIND_FRT.ValueVariant       := 1; // Contrata��o de frete por conta do destinat�rio
(*
      FmEfdInnNFsCab.EdVL_FRT.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_SEG.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_OUT_DA.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_BC_ICMS.ValueVariant    := QrVSMovDifInfValorT.Value;;
      FmEfdInnNFsCab.EdVL_ICMS.ValueVariant       := 0.00;
      FmEfdInnNFsCab.EdVL_BC_ICMS_ST.ValueVariant := 0.00;
      FmEfdInnNFsCab.EdVL_ICMS_ST.ValueVariant    := 0.00;
      FmEfdInnNFsCab.EdVL_IPI.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_PIS.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_COFINS.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_PIS_ST.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_COFINS_ST.ValueVariant  := 0.00;
*)
      FmEfdInnNFsCab.EdNFe_FatID.ValueVariant         := 0;
      FmEfdInnNFsCab.EdNFe_FatNum.ValueVariant        := 0;
      FmEfdInnNFsCab.EdNFe_StaLnk.ValueVariant        := 0;
      //
    end else
    begin
      FmEfdInnNFsCab.RGTpEntrd.ItemIndex          := QrEfdInnNFsCabTpEntrd.Value;
      FmEfdInnNFsCab.EdControle.ValueVariant      := QrEfdInnNFsCabControle.Value;
      FmEfdInnNFsCab.EdPecas.ValueVariant         := QrEfdInnNFsCabPecas.Value;
      FmEfdInnNFsCab.EdPesoKg.ValueVariant        := QrEfdInnNFsCabPesoKg.Value;
      FmEfdInnNFsCab.EdAreaM2.ValueVariant        := QrEfdInnNFsCabAreaM2.Value;
      FmEfdInnNFsCab.EdAreaP2.ValueVariant        := QrEfdInnNFsCabAreaP2.Value;
      FmEfdInnNFsCab.EdValorT.ValueVariant        := QrEfdInnNFsCabValorT.Value;
      FmEfdInnNFsCab.EdMotorista.ValueVariant     := QrEfdInnNFsCabMotorista.Value;
      FmEfdInnNFsCab.CBMotorista.KeyValue         := QrEfdInnNFsCabMotorista.Value;
      FmEfdInnNFsCab.EdPlaca.ValueVariant         := QrEfdInnNFsCabPlaca.Value;
      FmEfdInnNFsCab.EdCOD_MOD.ValueVariant       := QrEfdInnNFsCabCOD_MOD.Value;
      FmEfdInnNFsCab.EdCOD_SIT.ValueVariant       := QrEfdInnNFsCabCOD_SIT.Value;
      FmEfdInnNFsCab.EdSER.ValueVariant           := QrEfdInnNFsCabSER.Value;
      FmEfdInnNFsCab.EdNUM_DOC.ValueVariant       := QrEfdInnNFsCabNUM_DOC.Value;
      FmEfdInnNFsCab.EdCHV_NFE.ValueVariant       := QrEfdInnNFsCabCHV_NFE.Value;
      //FmEfdInnNFsCab.EdNFeStatus.ValueVariant     := QrVSInnNFsCEdNFeStaab.Value;
      FmEfdInnNFsCab.TPDT_DOC.Date                := QrEfdInnNFsCabDT_DOC.Value;
      FmEfdInnNFsCab.TPDT_E_S.Date                := QrEfdInnNFsCabDT_E_S.Value;
      FmEfdInnNFsCab.EdVL_DOC.ValueVariant        := QrEfdInnNFsCabVL_DOC.Value;
      FmEfdInnNFsCab.EdIND_PGTO.ValueVariant      := QrEfdInnNFsCabIND_PGTO.Value;
      FmEfdInnNFsCab.EdVL_DESC.ValueVariant       := QrEfdInnNFsCabVL_DESC.Value;
      //FmEfdInnNFsCab.EdVL_ABAT_NT.ValueVariant    := 0.00;  // Abatimento n�o comercia
      FmEfdInnNFsCab.EdVL_MERC.ValueVariant       := QrEfdInnNFsCabVL_MERC.Value;
      FmEfdInnNFsCab.EdIND_FRT.ValueVariant       := QrEfdInnNFsCabIND_FRT.Value;
      FmEfdInnNFsCab.EdVL_FRT.ValueVariant        := QrEfdInnNFsCabVL_FRT.Value;
      FmEfdInnNFsCab.EdVL_SEG.ValueVariant        := QrEfdInnNFsCabVL_SEG.Value;
      FmEfdInnNFsCab.EdVL_OUT_DA.ValueVariant     := QrEfdInnNFsCabVL_OUT_DA.Value;
      FmEfdInnNFsCab.EdVL_BC_ICMS.ValueVariant    := QrEfdInnNFsCabVL_BC_ICMS.Value;
      FmEfdInnNFsCab.EdVL_ICMS.ValueVariant       := QrEfdInnNFsCabVL_ICMS.Value;
      FmEfdInnNFsCab.EdVL_BC_ICMS_ST.ValueVariant := QrEfdInnNFsCabVL_BC_ICMS_ST.Value;
      FmEfdInnNFsCab.EdVL_ICMS_ST.ValueVariant    := QrEfdInnNFsCabVL_ICMS_ST.Value;
      FmEfdInnNFsCab.EdVL_IPI.ValueVariant        := QrEfdInnNFsCabVL_IPI.Value;
      FmEfdInnNFsCab.EdVL_PIS.ValueVariant        := QrEfdInnNFsCabVL_PIS.Value;
      FmEfdInnNFsCab.EdVL_COFINS.ValueVariant     := QrEfdInnNFsCabVL_COFINS.Value;
      FmEfdInnNFsCab.EdVL_PIS_ST.ValueVariant     := QrEfdInnNFsCabVL_PIS_ST.Value;
      FmEfdInnNFsCab.EdVL_COFINS_ST.ValueVariant  := QrEfdInnNFsCabVL_COFINS.Value;
      FmEfdInnNFsCab.EdNFe_FatID.ValueVariant         := QrEfdInnNFsCabNFe_FatID.Value;
      FmEfdInnNFsCab.EdNFe_FatNum.ValueVariant        := QrEfdInnNFsCabNFe_FatNum.Value;
      FmEfdInnNFsCab.EdNFe_StaLnk.ValueVariant        := QrEfdInnNFsCabNFe_StaLnk.Value;
      //
      FmEfdInnNFsCab.EdCliInt.ValueVariant        := QrEfdInnNFsCabCliInt.Value;
      FmEfdInnNFsCab.CBCliInt.KeyValue            := QrEfdInnNFsCabCliInt.Value;
      //
      FmEfdInnNFsCab.EdFornecedor.ValueVariant    := QrEfdInnNFsCabTerceiro.Value;
      FmEfdInnNFsCab.CBFornecedor.KeyValue        := QrEfdInnNFsCabTerceiro.Value;
      //
      FmEfdInnNFsCab.EdRegrFiscal.ValueVariant    := QrEfdInnNFsCabRegrFiscal.Value;
      FmEfdInnNFsCab.CBRegrFiscal.KeyValue        := QrEfdInnNFsCabRegrFiscal.Value;
      //
    end;
    FmEfdInnNFsCab.ShowModal;
    NewControle := FmEfdInnNFsCab.FControle;
    FmEfdInnNFsCab.Destroy;
    //
    ReopenEfdInnNFsCab(NewControle);
    if (SQLType = stIns) and (NewControle > Controle) then
    begin
      if QrEfdInnNFsCabControle.Value = NewControle then
        MostraFormEfdInnNFsIts(stIns);
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPlCCab.MostraFormEfdInnNFsIts(SQLType: TSQLType);
var
  NewConta, Conta, GraGru1: Integer;
  CST_B, IPI_CST, PIS_CST, COFINS_CST: Integer;
  ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq, ICMSSTRec_pAliq: Double;
begin
{$IfDef sAllVS}
  //NewConta := 0;
  //Conta := QrEfdInnNFsItsConta.Value;
  //
  if DBCheck.CriaFm(TFmEfdInnNFsIts, FmEfdInnNFsIts, afmoNegarComAviso) then
  begin
    FmEfdInnNFsIts.ImgTipo.SQLType := SQLType;
    FmEfdInnNFsIts.FData           := QrEfdInnNFsCabDT_E_S.Value;
    FmEfdInnNFsIts.FRegrFiscal     := QrEfdInnNFsCabRegrFiscal.Value;

    FmEfdInnNFsIts.FQrCab                         := QrEfdInnNFsCab;
    FmEfdInnNFsIts.FQrIts                         := QrEfdInnNFsIts;
    FmEfdInnNFsIts.EdEmpresa.ValueVariant         := QrVSPlCCabEmpresa.Value;
    FmEfdInnNFsIts.EdMovFatID.ValueVariant        := FEFDInnNFSMainFatID;
    FmEfdInnNFsIts.EdMovFatNum.ValueVariant       := QrVSPlCCabCodigo.Value;
    FmEfdInnNFsIts.EdMovimCod.ValueVariant        := QrVSPlCCabMovimCod.Value;
    FmEfdInnNFsIts.EdMovimNiv.ValueVariant        := 0;
    FmEfdInnNFsIts.EdMovimTwn.ValueVariant        := 0;
    FmEfdInnNFsIts.EdControle.ValueVariant        := QrEfdInnNFsCabControle.Value;
    //FmEfdInnNFsIts.FQrIts                         := QrVSInnNFs;
    if SQLType = stIns then
    begin
      FmEfdInnNFsIts.EdConta.ValueVariant           := 0;

      GraGru1 := DmProd.ObtemGraGru1DeGraGruX(QrVSPlCItsGraGruX.Value);

      FmEfdInnNFsIts.EdGraGruX.ValueVariant                       := QrVSPlCItsGraGruX.Value;
      FmEfdInnNFsIts.CBGraGruX.KeyValue                           := QrVSPlCItsGraGruX.Value;
      case QrVSPlCItsGrandeza.Value of
        0: FmEfdInnNFsIts.EdQTD.ValueVariant                      := QrVSMovDifInfPecas.Value;
        2: FmEfdInnNFsIts.EdQTD.ValueVariant                      := QrVSMovDifInfPesoKg.Value;
        else Geral.MB_Aviso('Grandeza no cadastro grade da mat�ria-prima deve ser Pe�a ou Peso(kg)!');
      end;
      //FmEfdInnNFsIts.EdCFOP.Text                                  := '';
      if QrVSPlCCabUF_Empresa.Value = QrVSPlCCabUF_Fornece.Value then
        FmEfdInnNFsIts.EdCFOP.ValueVariant := Dmod.QrControleCreTrib_MP_CFOP_DE.Value
      else
        FmEfdInnNFsIts.EdCFOP.ValueVariant := Dmod.QrControleCreTrib_MP_CFOP_FE.Value;
      //
      FmEfdInnNFsIts.Edprod_vProd.ValueVariant                    := QrVSMovDifInfValorT.Value;
      FmEfdInnNFsIts.EdVL_ITEM.ValueVariant                       := QrVSMovDifInfValorT.Value;
      FmEfdInnNFsIts.EdVL_DESC.ValueVariant                       := 0.00;
      FmEfdInnNFsIts.EdIND_MOV.ValueVariant                       := '';
      {
      if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
        'SELECT CST_B, IPI_CST, PIS_CST, COFINS_CST, ',
        'ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq,  ',
        'COFINSRec_pAliq  ',
        'FROM gragru1 ',
        'WHERE Nivel1=' + Geral.FF0(GraGru1),
        '']);
        //
        CST_B           := QrGraGru1CST_B.Value;
        IPI_CST         := QrGraGru1IPI_CST.Value;
        PIS_CST         := QrGraGru1PIS_CST.Value;
        COFINS_CST      := QrGraGru1COFINS_CST.Value;
        ICMSRec_pAliq   := QrGraGru1ICMSRec_pAliq.Value;
        ICMSSTRec_pAliq := 0.00;
        IPIRec_pAliq    := QrGraGru1IPIRec_pAliq.Value;
        PISRec_pAliq    := QrGraGru1PISRec_pAliq.Value;
        COFINSRec_pAliq := QrGraGru1COFINSRec_pAliq.Value;
      end else
      begin
      }
        CST_B           := Geral.IMV(Dmod.QrControleCreTrib_MP_ICMS_CST.VAlue);
        IPI_CST         := Geral.IMV(Dmod.QrControleCreTrib_MP_IPI_CST.VAlue);
        PIS_CST         := Geral.IMV(Dmod.QrControleCreTrib_MP_PIS_CST.VAlue);
        COFINS_CST      := Geral.IMV(Dmod.QrControleCreTrib_MP_COFINS_CST.VAlue);
        ICMSRec_pAliq   := Dmod.QrControleCreTrib_MP_ICMS_ALIQ.Value;
        ICMSSTRec_pAliq := Dmod.QrControleCreTrib_MP_ICMS_ALIQ_ST.Value;
        IPIRec_pAliq    := Dmod.QrControleCreTrib_MP_IPI_ALIQ.Value;
        PISRec_pAliq    := Dmod.QrControleCreTrib_MP_PIS_ALIQ.Value;
        COFINSRec_pAliq := Dmod.QrControleCreTrib_MP_COFINS_ALIQ.Value;
      {end;}
      FmEfdInnNFsIts.EdICMS_CST.ValueVariant                      := CST_B;
      //FmEfdInnNFsIts.EdCOD_NAT.ValueVariant                       := COD_NAT
      FmEfdInnNFsIts.EdVL_BC_ICMS.ValueVariant                    := QrVSMovDifInfValorT.Value;
      FmEfdInnNFsIts.EdALIQ_ICMS.ValueVariant                     := ICMSRec_pAliq;
      //FmEfdInnNFsIts.EdVL_ICMS.ValueVariant                       := VL_ICMS
      //FmEfdInnNFsIts.EdVL_BC_ICMS_ST.ValueVariant                 := VL_BC_ICMS_ST
      FmEfdInnNFsIts.EdALIQ_ST.ValueVariant                       := ICMSSTRec_pAliq;
      //FmEfdInnNFsIts.EdVL_ICMS_ST.ValueVariant                    := VL_ICMS_ST
      //FmEfdInnNFsIts.EdIND_APUR  .ValueVariant                    := IND_APUR
      FmEfdInnNFsIts.EdCST_IPI.ValueVariant                       := IPI_CST;
      //FmEfdInnNFsIts.EdCOD_ENQ   .ValueVariant                    := COD_ENQ
      //FmEfdInnNFsIts.EdVL_BC_IPI.ValueVariant                     := VL_BC_IPI
      FmEfdInnNFsIts.EdALIQ_IPI.ValueVariant                      := IPIRec_pAliq;
      //FmEfdInnNFsIts.EdVL_IPI.ValueVariant                        := VL_IPI
      FmEfdInnNFsIts.EdCST_PIS.ValueVariant                       := PIS_CST;
      //FmEfdInnNFsIts.EdVL_BC_PIS .ValueVariant                    := VL_BC_PIS
      FmEfdInnNFsIts.EdALIQ_PIS.ValueVariant                      := PISRec_pAliq;
      //FmEfdInnNFsIts.EdQUANT_BC_PIS.ValueVariant                  := QUANT_BC_PIS
      //FmEfdInnNFsIts.EdALIQ_PIS_r.ValueVariant                    := ALIQ_PIS_r
      //FmEfdInnNFsIts.EdVL_PIS.ValueVariant                        := VL_PIS
      FmEfdInnNFsIts.EdCST_COFINS.ValueVariant                    := COFINS_CST;
      //FmEfdInnNFsIts.EdVL_BC_COFINS.ValueVariant                  := VL_BC_COFINS
      FmEfdInnNFsIts.EdALIQ_COFINS.ValueVariant                   := COFINSRec_pAliq;
      //FmEfdInnNFsIts.EdQUANT_BC_COFINS.ValueVariant               := QUANT_BC_COFINS
      //FmEfdInnNFsIts.EdALIQ_COFINS_r.ValueVariant                 := ALIQ_COFINS_r
      //FmEfdInnNFsIts.EdVL_COFINS.ValueVariant                     := VL_COFINS
      //FmEfdInnNFsIts.EdCOD_CTA.ValueVariant                       := COD_CTA
      //FmEfdInnNFsIts.EdVL_ABAT_NT     .ValueVariant               := VL_ABAT_NT

    end else
    begin
      FmEfdInnNFsIts.EdConta.ValueVariant           := QrEfdInnNFsItsConta.Value;
      FmEfdInnNFsIts.FTpEntrd                       := QrEfdInnNFsCabTpEntrd.Value;

      FmEfdInnNFsIts.EdGraGruX.ValueVariant                       := QrEfdInnNFsItsGraGruX.Value;
      FmEfdInnNFsIts.CBGraGruX.KeyValue                           := QrEfdInnNFsItsGraGruX.Value;
      FmEfdInnNFsIts.EdCFOP.Text                                  := Geral.FormataCFOP(Geral.FF0(QrEfdInnNFsItsCFOP.Value));
      //FmEfdInnNFsIts.EdCOD_NAT.ValueVariant                       :=
      FmEfdInnNFsIts.EdQTD.ValueVariant                           := QrEfdInnNFsItsQTD.Value;
      FmEfdInnNFsIts.EdIND_MOV.ValueVariant                       := QrEfdInnNFsItsIND_MOV.Value;
      FmEfdInnNFsIts.Edprod_vProd.ValueVariant                    := QrEfdInnNFsItsprod_vProd.Value;
      FmEfdInnNFsIts.Edprod_vFrete.ValueVariant                   := QrEfdInnNFsItsprod_vFrete.Value;
      FmEfdInnNFsIts.Edprod_vSeg.ValueVariant                     := QrEfdInnNFsItsprod_vSeg.Value;
      FmEfdInnNFsIts.Edprod_vOutro.ValueVariant                   := QrEfdInnNFsItsprod_vOutro.Value;
      FmEfdInnNFsIts.EdVL_ITEM.ValueVariant                       := QrEfdInnNFsItsVL_ITEM.Value;
      FmEfdInnNFsIts.EdVL_DESC.ValueVariant                       := QrEfdInnNFsItsVL_DESC.Value;
      FmEfdInnNFsIts.EdOri_IPIpIPI.ValueVariant                   := QrEfdInnNFsItsOri_IPIpIPI.Value;
      FmEfdInnNFsIts.EdOri_IPIvIPI.ValueVariant                   := QrEfdInnNFsItsOri_IPIvIPI.Value;
      FmEfdInnNFsIts.EdxLote.ValueVariant                         := QrEfdInnNFsItsxLote.Value;
      FmEfdInnNFsIts.EdICMS_CST.ValueVariant                      := QrEfdInnNFsItsCST_ICMS.Value;
      FmEfdInnNFsIts.EdVL_BC_ICMS.ValueVariant                    := QrEfdInnNFsItsVL_BC_ICMS.Value;
      FmEfdInnNFsIts.EdALIQ_ICMS.ValueVariant                     := QrEfdInnNFsItsALIQ_ICMS.Value;
      FmEfdInnNFsIts.EdVL_ICMS.ValueVariant                       := QrEfdInnNFsItsVL_ICMS.Value;
      FmEfdInnNFsIts.EdVL_BC_ICMS_ST.ValueVariant                 := QrEfdInnNFsItsVL_BC_ICMS_ST.Value;
      FmEfdInnNFsIts.EdALIQ_ST.ValueVariant                       := QrEfdInnNFsItsALIQ_ST.Value;
      FmEfdInnNFsIts.EdVL_ICMS_ST.ValueVariant                    := QrEfdInnNFsItsVL_ICMS_ST.Value;
      //FmEfdInnNFsIts.EdIND_APUR.ValueVariant                      := IND_APUR
      //FmEfdInnNFsIts.EdCOD_ENQ.ValueVariant                       := COD_ENQ


      FmEfdInnNFsIts.EdCST_IPI.ValueVariant                       := QrEfdInnNFsItsCST_IPI.Value;;
      FmEfdInnNFsIts.EdVL_BC_IPI.ValueVariant                     := QrEfdInnNFsItsVL_BC_IPI.Value;
      FmEfdInnNFsIts.EdALIQ_IPI.ValueVariant                      := QrEfdInnNFsItsALIQ_IPI.Value;
      FmEfdInnNFsIts.EdVL_IPI.ValueVariant                        := QrEfdInnNFsItsVL_IPI.Value;
      FmEfdInnNFsIts.EdCST_PIS.ValueVariant                       := QrEfdInnNFsItsCST_PIS.Value;
      FmEfdInnNFsIts.EdVL_BC_PIS .ValueVariant                    := QrEfdInnNFsItsVL_BC_PIS.Value;
      FmEfdInnNFsIts.EdALIQ_PIS.ValueVariant                      := QrEfdInnNFsItsALIQ_PIS_p.Value;
      //FmEfdInnNFsIts.EdQUANT_BC_PIS.ValueVariant                  := QrEfdInnNFsItsQUANT_BC_PIS.Value;
      //FmEfdInnNFsIts.EdALIQ_PIS_r.ValueVariant                    := QrEfdInnNFsItsALIQ_PIS_r.Value;
      FmEfdInnNFsIts.EdVL_PIS.ValueVariant                        := QrEfdInnNFsItsVL_PIS.Value;
      FmEfdInnNFsIts.EdCST_COFINS.ValueVariant                    := QrEfdInnNFsItsCST_COFINS.Value;
      FmEfdInnNFsIts.EdVL_BC_COFINS.ValueVariant                  := QrEfdInnNFsItsVL_BC_COFINS.Value;
      FmEfdInnNFsIts.EdALIQ_COFINS.ValueVariant                   := QrEfdInnNFsItsALIQ_COFINS_p.Value;
      //FmEfdInnNFsIts.EdQUANT_BC_COFINS.ValueVariant               := QrEfdInnNFsItsQUANT_BC_COFINS.Value;
      //FmEfdInnNFsIts.EdALIQ_COFINS_r.ValueVariant                 := QrEfdInnNFsItsALIQ_COFINS_r.Value;
      FmEfdInnNFsIts.EdVL_COFINS.ValueVariant                     := QrEfdInnNFsItsVL_COFINS.Value;
      //FmEfdInnNFsIts.EdCOD_CTA.ValueVariant                       := QrEfdInnNFsItsCOD_CTA.Value;
      //FmEfdInnNFsIts.EdVL_ABAT_NT.ValueVariant                    := QrEfdInnNFsItsVL_ABAT_NT.Value;
    end;
    FmEfdInnNFsIts.ShowModal;
    NewConta := FmEfdInnNFsIts.FConta;
    FmEfdInnNFsIts.Destroy;
        //
    ReopenEfdInnNFsIts(NewConta);
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPlCCab.MostraFormVSHisFchAdd(SQLType: TSQLType);
begin
{$IfDef sAllVS}
  if DBCheck.CriaFm(TFmVSHisFchAdd, FmVSHisFchAdd, afmoNegarComAviso) then
  begin
    FmVSHisFchAdd.ImgTipo.SQLType := SQLType;
    FmVSHisFchAdd.FQrIts                  := QrVSHisFch;
    FmVSHisFchAdd.EdVSMovIts.ValueVariant := QrVSPlCItsControle.Value;
    FmVSHisFchAdd.EdSerieFch.ValueVariant := QrVSPlCItsSerieFch.Value;
    FmVSHisFchAdd.EdFicha.ValueVariant    := QrVSPlCItsFicha.Value;
    if SQLType = stUpd then
    begin
      FmVSHisFchAdd.EdCodigo.ValueVariant := QrVSHisFchCodigo.Value;
      FmVSHisFchAdd.TPDataHora.Date       := QrVSHisFchDataHora.Value;
      FmVSHisFchAdd.EdDataHora.Text       := Geral.FDT(QrVSHisFchDataHora.Value, 100);
      FmVSHisFchAdd.EdNome.Text           := QrVSHisFchNome.Value;
      FmVSHisFchAdd.MeObserv.Text         := QrVSHisFchObserv.Value;
    end else
    begin
      FmVSHisFchAdd.EdCodigo.ValueVariant := 0;
      FmVSHisFchAdd.TPDataHora.Date       := DModG.ObtemAgora();
      FmVSHisFchAdd.EdDataHora.Text       := Geral.FDT(DModG.ObtemAgora(), 100);
      FmVSHisFchAdd.EdNome.Text           := '';
      FmVSHisFchAdd.MeObserv.Text         := '';
    end;
    FmVSHisFchAdd.ShowModal;
    FmVSHisFchAdd.Destroy;
  end;

{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPlCCab.MostraFormVSPlCIts(SQLType: TSQLType);
begin
{
  if DBCheck.CriaFm(TFmVSPlCIts, FmVSPlCIts, afmoNegarComAviso) then
  begin
    FmVSPlCIts.ImgTipo.SQLType := SQLType;
    FmVSPlCIts.FQrCab := QrVSPlCCab;
    FmVSPlCIts.FDsCab := DsVSPlCCab;
    FmVSPlCIts.FQrIts := QrVSPlCIts;
    FmVSPlCIts.FDataHora := QrVSPlCCabDtEntrada.Value;
    FmVSPlCIts.FEmpresa := QrVSPlCCabEmpresa.Value;
    FmVSPlCIts.FFornece := QrVSPlCCabFornecedor.Value;
    if SQLType = stIns then
    begin
      //FmVSPlCIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmVSPlCIts.EdControle.ValueVariant := QrVSPlCItsControle.Value;
      //
      FmVSPlCIts.EdGragruX.ValueVariant  := QrVSPlCItsGraGruX.Value;
      FmVSPlCIts.CBGragruX.KeyValue      := QrVSPlCItsGraGruX.Value;
      FmVSPlCIts.EdPecas.ValueVariant    := QrVSPlCItsPecas.Value;
      FmVSPlCIts.EdPesoKg.ValueVariant   := QrVSPlCItsPesoKg.Value;
      FmVSPlCIts.EdValorT.ValueVariant   := QrVSPlCItsValorT.Value;
      FmVSPlCIts.EdObserv.ValueVariant   := QrVSPlCItsObserv.Value;
      FmVSPlCIts.EdSerieFch.ValueVariant := QrVSPlCItsSerieFch.Value;
      FmVSPlCIts.CBSerieFch.KeyValue     := QrVSPlCItsSerieFch.Value;
      FmVSPlCIts.EdFicha.ValueVariant    := QrVSPlCItsFicha.Value;
      FmVSPlCIts.EdMarca.ValueVariant    := QrVSPlCItsMarca.Value;
      //FmVSPlCIts.RGMisturou.ItemIndex    := QrVSPlCItsMisturou.Value;
      //
      if QrVSPlCItsSdoVrtPeca.Value < QrVSPlCItsPecas.Value then
      begin
        FmVSPlCIts.EdGraGruX.Enabled  := False;
        FmVSPlCIts.CBGraGruX.Enabled  := False;
        FmVSPlCIts.EdSerieFch.Enabled := False;
        FmVSPlCIts.CBSerieFch.Enabled := False;
        FmVSPlCIts.EdFicha.Enabled    := False;
      end;
      //
      FmVSPlCIts.EdInfPecas.ValueVariant    := QrVSMovDifInfPecas.Value;
      FmVSPlCIts.EdInfPesoKg.ValueVariant   := QrVSMovDifInfPesoKg.Value;
      FmVSPlCIts.EdInfValorT.ValueVariant   := QrVSMovDifInfValorT.Value;
    end;
    FmVSPlCIts.ShowModal;
    FmVSPlCIts.Destroy;
  end;
}
  if DBCheck.CriaFm(TFmVSPlCIts, FmVSPlCIts, afmoNegarComAviso) then
  begin
    FmVSPlCIts.ImgTipo.SQLType := SQLType;
    FmVSPlCIts.FQrCab := QrVSPlCCab;
    FmVSPlCIts.FDsCab := DsVSPlcCab;
    FmVSPlCIts.FQrIts := QrVSPlCIts;
    FmVSPlCIts.FDataHora := QrVSPlCCabDtEntrada.Value;
    FmVSPlCIts.FEmpresa  := QrVSPlCCabEmpresa.Value;
    FmVSPlCIts.FClientMO  := QrVSPlCCabClienteMO.Value;
    //
    FmVSPlCIts.EdFornecedor.ValueVariant := QrVSPlCCabFornecedor.Value;
    FmVSPlCIts.CBFornecedor.KeyValue     := QrVSPlCCabFornecedor.Value;
    if SQLType = stIns then
    begin
      //FmVSPlCIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmVSPlCIts.EdControle.ValueVariant := QrVSPlCItsControle.Value;
      //
      FmVSPlCIts.EdGragruX.ValueVariant    := QrVSPlCItsGraGruX.Value;
      FmVSPlCIts.CBGragruX.KeyValue        := QrVSPlCItsGraGruX.Value;
      FmVSPlCIts.EdSerieFch.ValueVariant   := QrVSPlCItsSerieFch.Value;
      FmVSPlCIts.CBSerieFch.KeyValue       := QrVSPlCItsSerieFch.Value;
      FmVSPlCIts.EdFicha.ValueVariant      := QrVSPlCItsFicha.Value;
      FmVSPlCIts.EdPallet.ValueVariant     := QrVSPlCItsPallet.Value;
      FmVSPlCIts.CBPallet.KeyValue         := QrVSPlCItsPallet.Value;
      FmVSPlCIts.EdPecas.ValueVariant      := QrVSPlCItsPecas.Value;
      FmVSPlCIts.EdPesoKg.ValueVariant     := QrVSPlCItsPesoKg.Value;
      FmVSPlCIts.EdAreaM2.ValueVariant     := QrVSPlCItsAreaM2.Value;
      FmVSPlCIts.EdAreaP2.ValueVariant     := QrVSPlCItsAreaP2.Value;
      FmVSPlCIts.EdValorMP.ValueVariant    := QrVSPlCItsValorMP.Value;
      FmVSPlCIts.EdCustoMOKg.ValueVariant  := QrVSPlCItsCustoMOKg.Value;
      FmVSPlCIts.EdFornecMO.ValueVariant   := QrVSPlCItsFornecMO.Value;
      FmVSPlCIts.CBFornecMO.KeyValue       := QrVSPlCItsFornecMO.Value;
      FmVSPlCIts.EdMarca.ValueVariant      := QrVSPlCItsMarca.Value;
      FmVSPlCIts.EdObserv.ValueVariant     := QrVSPlCItsObserv.Value;
      FmVSPlCIts.EdStqCenLoc.ValueVariant  := QrVSPlCItsStqCenLoc.Value;
      FmVSPlCIts.CBStqCenLoc.KeyValue      := QrVSPlCItsStqCenLoc.Value;
      FmVSPlCIts.EdReqMovEstq.ValueVariant := QrVSPlCItsReqMovEstq.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
      FmVSPlCIts.EdCustoMOTot.ValueVariant := QrVSPlCItsCustoMOTot.Value;
      FmVSPlCIts.EdValorT.ValueVariant     := QrVSPlCItsValorT.Value;
    end;
    FmVSPlCIts.ShowModal;
    FmVSPlCIts.Destroy;
  end;

end;

procedure TFmVSPlCCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSPlCCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSPlCCab, QrVSPlCIts);
end;

procedure TFmVSPlCCab.PMCTePopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrVsPlCCab.State <> dsInactive) and (QrVSPlCCab.RecordCount > 0);
  IncluiConhecimentodefrete1.Enabled := Habilita;
  Habilita := Habilita and (QrEfdInnCTsCab.RecordCount > 0);
  AlteraConhecimentodefrete1.Enabled := Habilita;
  ExcluiConhecimentodefrete1.Enabled := Habilita;
end;

procedure TFmVSPlCCab.PMFiscalPopup(Sender: TObject);
begin
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiDocumento1, QrVSPlCCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraDocumento1, QrEfdInnNFsCab);
  MyObjects.HabilitaMenuItemCabDel(ExcluiDocumento1, QrEfdInnNFsCab, QrEfdInnNFsIts);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiItemdodocumento1, QrEfdInnNFsCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraoItemselecionadododocumento1, QrEfdInnNFsIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluioItemselecionadododocumento1, QrEfdInnNFsIts);
  //
end;

procedure TFmVSPlCCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSPlCCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSPlCIts);
  //MyObjects.HabilitaMenuItemCabDel(ItsExclui1, QrVSPlCIts, QrVSItsBxa);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSPlCIts);
  //
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSPlCItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmVSPlCCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSPlCCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSPlCCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsplccab';
  VAR_GOTOMYSQLTABLE := QrVSPlCCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,');
  VAR_SQLx.Add('IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,');
  VAR_SQLx.Add('IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.EUF, ent.PUF) UF_EMPRESA, ');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.EUF, frn.PUF) UF_FORNECE ');
  VAR_SQLx.Add('FROM vsplccab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor');
  VAR_SQLx.Add('LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO');
  VAR_SQLx.Add('LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc');
  VAR_SQLx.Add('LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSPlCCab.Estoque1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSPlCCab.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSHisFchCodigo.Value;
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o deste item de hist�rico?',
  'vshisfch', 'Codigo', Codigo, DMod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSHisFch,
      QrVSHisFchCodigo, QrVSHisFchCodigo.Value);
    ReopenVSHisFch(Codigo);
  end;
end;

procedure TFmVSPlCCab.ExcluiConhecimentodefrete1Click(Sender: TObject);
var
  Controle: Integer;
begin
(*
  if QrEfdInnNFsIts.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada! Documento com item!');
  end else
  begin
*)
    if QrEfdInnCTsCab.RecordCount > 0 then
    begin
      if UMyMod.ExcluiRegistroInt1('Confirma a retirada do CT-e selecionado?',
      'EfdInnCTsCab', 'Controle', QrEfdInnCTsCabControle.Value, Dmod.MyDB) = ID_YES then
      begin
        Controle := GOTOy.LocalizaPriorNextIntQr(QrEfdInnCTsCab,
          QrEfdInnCTsCabControle, QrEfdInnCTsCabControle.Value);
        ReopenEfdInnCTsCab(Controle);
      end;
    end;
  //end;
end;

procedure TFmVSPlCCab.ExcluiDocumento1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if QrEfdInnNFsIts.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada! Documento com item!');
  end else
  begin
    if QrEfdInnNFsCab.RecordCount > 0 then
    begin
      if UMyMod.ExcluiRegistroInt1('Confirma a retirada do DF-e selecionado?',
      'EfdInnNFsCab', 'Controle', QrEfdInnNFsCabControle.Value, Dmod.MyDB) = ID_YES then
      begin
        Controle := GOTOy.LocalizaPriorNextIntQr(QrEfdInnNFsCab,
          QrEfdInnNFsCabControle, QrEfdInnNFsCabControle.Value);
        ReopenEfdInnNFsCab(Controle);
      end;
    end;
  end;
end;

procedure TFmVSPlCCab.ExcluioItemselecionadododocumento1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if QrEfdInnNFsIts.RecordCount > 0 then
  begin
    Controle := QrEfdInnNFsCabControle.Value;
    //
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEfdInnNFsIts, TDBGrid(DBGInnNFsIts),
    'EfdInnNFsIts', ['Conta'], ['Conta'], istPergunta, '');
    //
    DmNFe_0000.AtualizaTotaisEfdInnNFs(Controle);
  end;
end;

procedure TFmVSPlCCab.ImprimePallet(InfoNO_Pallet: Boolean);
var
  Empresa, ClientMO, Pallet: Integer;
  TempTab: String;
begin
  Empresa  := QrVSPlCItsEmpresa.Value;
  ClientMO := QrVSPlCItsClientMO.Value;
  Pallet   := QrVSPlCItsPallet.Value;
  TempTab  := Self.Name;
  VS_CRC_PF.ImprimePallet_Unico(Empresa, ClientMO, Pallet, TempTab, InfoNO_Pallet);
end;

procedure TFmVSPlCCab.ImprimeTodosPallets(InfoNO_PALLET: Boolean);
const
  VSLstPalBox = '';
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
  TempTab: String;
begin
  TempTab :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp4, DModG.QrUpdPID1,
      False, 1, '_vsmovimp4_' + Self.Name);
  Empresa := QrVSPlCItsEmpresa.Value;
  N := 0;
  QrVSPlCIts.First;
  while not QrVSPlCIts.Eof do
  begin
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrVSPlCItsPallet.Value;
    //
    QrVSPlCIts.Next;
  end;
  if N > 0 then
    //VS_CRC_PF.ImprimePallets(Empresa, MyArr, TempTab, VSLstPalBox)
    VS_CRC_PF.ImprimePallet_Varios(Empresa, MyArr, TempTab, InfoNO_PALLET)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSPlCCab.Inclui1Click(Sender: TObject);
begin
  MostraFormVSHisFchAdd(stIns);
end;

procedure TFmVSPlCCab.IncluiConhecimentodefrete1Click(Sender: TObject);
begin
  MostraFormEfdInnCTsCab(stIns);
end;

procedure TFmVSPlCCab.IncluiDocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsCab(stIns);
end;

procedure TFmVSPlCCab.IncluiItemdodocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsIts(stIns);
end;

procedure TFmVSPlCCab.IrparajaneladegerenciamentodeFichaRMP1Click(
  Sender: TObject);
var
  SerieFicha, Ficha: Integer;
begin

{$IfDef sAllVS}
  SerieFicha := QrVSPlCItsSerieFch.Value;
  Ficha      := QrVSPlCItsFicha.Value;
  VS_PF.MostraFormVSFchGerCab(SerieFicha, Ficha);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPlCCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSPlCIts(stUpd);
end;

procedure TFmVSPlCCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSPlCCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSPlCCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSPlCCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod, Controle: Integer;
begin
  Codigo   := QrVSPlCItsCodigo.Value;
  MovimCod := QrVSPlCItsMovimCod.Value;
  Controle := QrVSPlCItsControle.Value;
  //
  if VS_CRC_PF.ExcluiControleVSMovIts(QrVSPlCIts, TIntegerField(QrVSPlCItsControle),
  Controle, CtrlBaix, QrVSPlCItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti076)) then
  begin
    // Exclui tambem da tabela vsmovdif
    VS_CRC_PF.ExcluiVSNaoVMI('', 'vsmovdif', 'Controle', Controle, Dmod.MyDB);
    // Atualiza!
    VS_CRC_PF.AtualizaTotaisVSXxxCab('vsplccab', MovimCod);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSPlCCab.Rendimentodesemiacabado1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  VS_PF.ImprimeRendimentoIMEIS([QrVSPlCItsControle.Value], [False], [False],
  [], []);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPlCCab.ReopenEFdInnCTsCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnCTsCab, Dmod.MyDB, [
  'SELECT vic.*, ',
  'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  ',
  'FROM efdinnctscab vic ',
  'LEFT JOIN entidades ter ON ter.Codigo=vic.Terceiro ',
  'WHERE vic.MovFatID=' + Geral.FF0(FEFDInnNFsMainFatID),
  'AND vic.MovFatNum=' + Geral.FF0(QrVSPlCCabCodigo.Value),
  'AND vic.MovimCod=' + Geral.FF0(QrVSPlCCabMovimCod.Value), // N�o precisa
  ' ']);
  //
  QrEfdInnCTsCab.Locate('Controle', Controle, []);
end;

procedure TFmVSPlCCab.ReopenEFdInnNFsCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnNFsCab, Dmod.MyDB, [
  'SELECT vic.*, ',
  'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  ',
  'FROM efdinnnfscab vic ',
  'LEFT JOIN entidades ter ON ter.Codigo=vic.Terceiro ',
  'WHERE vic.MovFatID=' + Geral.FF0(FEFDInnNFSMainFatID),
  'AND vic.MovFatNum=' + Geral.FF0(QrVSPlCCabCodigo.Value),
  'AND vic.MovimCod=' + Geral.FF0(QrVSPlCCabMovimCod.Value), // N�o precisa
  ' ']);
  //
  QrEfdInnNFsCab.Locate('Controle', Controle, []);
end;

procedure TFmVSPlCCab.ReopenEfdInnNFsIts(Conta: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnNFsIts, Dmod.MyDB, [
  'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM efdinnnfsits     vin',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE vin.Controle=' + Geral.FF0(QrEfdInnNFsCabControle.Value),
  '']);
  //
  QrEfdInnNFsIts.Locate('Conta', Conta, []);
end;

procedure TFmVSPlCCab.ReopenVSHisFch(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSHisFch, Dmod.MyDB, [
  'SELECT * ',
  'FROM vshisfch ',
  'WHERE VSMovIts=' + Geral.FF0(QrVSPlCItsControle.Value),
  'OR ( ',
  '  SerieFch=' + Geral.FF0(QrVSPlCItsSerieFch.Value),
  '  AND ',
  '  Ficha=' + Geral.FF0(QrVSPlCItsFicha.Value),
  ') ',
  '']);
  //
  if Codigo <> 0 then
    QrVSHisFch.Locate('Codigo', Codigo, []);
end;

procedure TFmVSPlCCab.ReopenVSPlCIts(Controle: Integer);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPlCIts, Dmod.MyDB, [
  'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
  'IF(wmi.SdoVrtPeca > 0, 0, ',
  '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2, ',
  'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
  'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(wmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(wmi.Pecas > 0, wmi.QtdGerArM2 / wmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(wmi.Pecas <> 0, wmi.PesoKg / wmi.Pecas, 0) KgMedioCouro ',
  'FROM v s m o v i t s wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrVSPlCCabMovimCod.Value),
  'ORDER BY NO_Pallet, wmi.Controle ',
  '']);
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSPlCCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  //'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(vmi.SdoVrtPeca > 0, 0, ',
  '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3), ',
  '  ",", ""), ".", ",")) RendKgm2_TXT, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
  'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT, ',
  'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,  unm.Grandeza, ',
  'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
  '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3), ',
  '  ",", ""), ".", ",")) m2_CouroTXT, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, ',
  'vmi.ValorT/vmi.AreaM2 CustoM2 ',
  '']);
  //'FROM ' + VS_CRC_PF.TabMovVS_Tab(tab) + ' vmi ',
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  //'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSPlCCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPlCIts, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVSPlCIts);
  //
  QrVSPlCIts.Locate('Controle', Controle, []);
end;


procedure TFmVSPlCCab.ReopenVSItsBxa(Controle: Integer);
begin
  //
end;

procedure TFmVSPlCCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSPlCCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSPlCCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSPlCCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSPlCCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSPlCCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPlCCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSPlCCabCodigo.Value;
  Close;
end;

procedure TFmVSPlCCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSPlCIts(stIns);
end;

procedure TFmVSPlCCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSPlCCab, [PnDados],
  [PnEdita], TPDtCompra, ImgTipo, 'vsplccab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSPlCCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSPlCCab.BtConfirmaClick(Sender: TObject);
var
  DtCompra, DtViagem, DtEntrada, DataHora, Placa: String;
  Codigo, MovimCod, Empresa, Fornecedor, Transporta, Terceiro, ClienteMO,
  Procednc, Motorista, ide_serie, ide_nNF, emi_serie, emi_nNF: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtCompra       := Geral.FDT_TP_Ed(TPDtCompra.Date, EdDtCompra.Text);
  DtViagem       := Geral.FDT_TP_Ed(TPDtViagem.Date, EdDtViagem.Text);
  DtEntrada      := Geral.FDT_TP_Ed(TPDtEntrada.Date, EdDtEntrada.Text);
  Fornecedor     := EdFornecedor.ValueVariant;
  Transporta     := EdTransporta.ValueVariant;
  ClienteMO      := EdClienteMO.ValueVariant;
  Procednc       := EdProcednc.ValueVariant;
  Motorista      := EdMotorista.ValueVariant;
  Placa          := EdPlaca.ValueVariant;
  ide_serie      := Edide_serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  emi_serie      := Edemi_serie.ValueVariant;
  emi_nNF        := Edemi_nNF.ValueVariant;
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(Fornecedor = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  if MyObjects.FIC(TPDtCompra.DateTime < 2, TPDtCompra, 'Defina uma data de compra!') then Exit;
  DataChegadaInvalida := (TPDtEntrada.Date < TPDtViagem.Date);
  if MyObjects.FIC(DataChegadaInvalida, TPDtViagem, 'Defina uma data de viagem v�lida!') then Exit;
  if MyObjects.FIC(ClienteMO = 0, EdClienteMO, 'Defina o dono do couro!') then Exit;
  //
  if not VS_CRC_PF.ValidaCampoNF(10, Edide_nNF, True) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsplccab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsplccab', False, [
  'MovimCod', 'Empresa', 'DtCompra',
  'DtViagem', 'DtEntrada', 'Fornecedor',
  'Transporta',(*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'*)
  'ClienteMO', 'Procednc', 'Motorista',
  'Placa', 'ide_serie', 'ide_nNF',
  'emi_serie', 'emi_nNF'
  ], [
  'Codigo'], [
  MovimCod, Empresa, DtCompra,
  DtViagem, DtEntrada, Fornecedor,
  Transporta, (* (*Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*)
  ClienteMO, Procednc, Motorista,
  Placa, ide_serie, ide_nNF,
  emi_serie, emi_nNF
  ], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidEntradaPlC, Codigo)
    else
    begin
      DataHora := DtEntrada;
      Terceiro := Fornecedor;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', 'Terceiro', CO_DATA_HORA_VMI,
      'ClientMO'], ['MovimCod'], [
      Empresa, Terceiro, DataHora,
      ClienteMO], [MovimCod], True);
      //
      if Edide_nNF.ValueVariant <> 0 then
        AtualizaNFeItens();
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if SQLType = stIns then
      MostraFormVSPlCIts(stIns);
  end;
end;

procedure TFmVSPlCCab.BtCTeClick(Sender: TObject);
begin
  PCBottom.ActivePageIndex := 2;
  MyObjects.MostraPopupDeBotao(PMCTe, BtCTe);
end;

procedure TFmVSPlCCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsplccab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsplccab', 'Codigo');
end;

procedure TFmVSPlCCab.BtFiscalClick(Sender: TObject);
begin
  PCBottom.ActivePageIndex := 1;
  MyObjects.MostraPopupDeBotao(PMFiscal, BtFiscal);
end;

procedure TFmVSPlCCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSPlCCab.BtReclasifClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSGeraArt(0, 0);
end;

procedure TFmVSPlCCab.Altera1Click(Sender: TObject);
begin
  MostraFormVSHisFchAdd(stUpd);
end;

procedure TFmVSPlCCab.AlteraConhecimentodefrete1Click(Sender: TObject);
begin
  MostraFormEfdInnCTsCab(stIns);
end;

procedure TFmVSPlCCab.AlteraDocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsCab(stIns);
end;

procedure TFmVSPlCCab.AlteraoItemselecionadododocumento1Click(Sender: TObject);
begin
  MostraFormEfdInnNFsIts(stUpd);
end;

procedure TFmVSPlCCab.AnliseMPAG1Click(Sender: TObject);
begin
{
  MyObjects.frxDefineDataSets(frxWET_CURTI_006_01, [
    DModG.frxDsDono,
    frxDsVSPlCCab,
    frxDsVSPlCIts,
    frxDsVSItsBxa,
    frxDsVSItsGer
  ]);
  MyObjects.frxMostra(frxWET_CURTI_006_01, 'An�lise MPAG');
}
end;

procedure TFmVSPlCCab.Atualizaestoque1Click(Sender: TObject);
begin
  VS_CRC_PF.AtualizaSaldoIMEI(QrVSPlCItsControle.Value, True);
end;

procedure TFmVSPlCCab.AtualizaNFeItens();
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSPlCCabMovimCod.Value, TEstqMovimID.emidEntradaPlC, [(**)], [eminSemNiv]);
end;

procedure TFmVSPlCCab.BtCabClick(Sender: TObject);
begin
  VS_CRC_PF.HabilitaMenuInsOuAllVSAberto(QrVSPlCCab, QrVSPlCCabDtEntrada.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSPlCCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //GBIts.Align := alClient;
  PCBottom.Align := alClient;
  PCBottom.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
end;

procedure TFmVSPlCCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSPlCCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSPlCCab.SbProcedencClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcednc.Text := IntToStr(VAR_ENTIDADE);
    CBProcednc.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSPlCCab.SbClienteMOClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdClienteMO.Text := IntToStr(VAR_ENTIDADE);
    CBClienteMO.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSPlCCab.SbFornecedorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdFornecedor.Text := IntToStr(VAR_ENTIDADE);
    CBFornecedor.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSPlCCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSPlCCab.SbMotoristaClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdMotorista.Text := IntToStr(VAR_ENTIDADE);
    CBMotorista.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSPlCCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSPlCCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSPlCCab.QrVSPlCCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSPlCCab.QrVSPlCCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPlCIts(0);
  ReopenEfdInnNFsCab(0);
  ReopenEfdInnCTsCab(0);
end;

procedure TFmVSPlCCab.FichaCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimePallet(True);
end;

procedure TFmVSPlCCab.FichasCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(True);
end;

procedure TFmVSPlCCab.FichaSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimePallet(False);
end;

procedure TFmVSPlCCab.FichasSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(False);
end;

procedure TFmVSPlCCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSPlCCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSPlCCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_CRC_PF.MostraFormVSRMPPsq(emidEntradaPlC, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    ReopenVSPlCIts(Controle);
  end;
end;

procedure TFmVSPlCCab.SbTransportadorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdTransporta.Text := IntToStr(VAR_ENTIDADE);
    CBTransporta.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmVSPlCCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPlCCab.frxWET_CURTI_006_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmVSPlCCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSPlCCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSPlCCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsplccab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
  Agora := DModG.ObtemAgora();
  TPDtEntrada.Date := Agora;
  //EdDtCompra.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
end;

procedure TFmVSPlCCab.QrVSPlCCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSPlCIts.Close;
  QrEfdInnNFsCab.Close;
  QrEfdInnCTsCab.Close;
end;

procedure TFmVSPlCCab.QrVSPlCCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSPlCCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSPlCCab.QrVSPlCItsAfterScroll(DataSet: TDataSet);
begin
  ReopenVSItsBxa(0);
{$IfDef sAllVS}
  VS_PF.ReopenVSMovDif(QrVSMovDif, QrVSPlCItsControle.Value);
  ReopenVSHisFch(0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPlCCab.QrVSPlCItsBeforeClose(DataSet: TDataSet);
begin
{
  QrVSItsBxa.Close;
}
  QrVSMovDif.Close;
  QrVSHisFch.Close;
end;

procedure TFmVSPlCCab.QrEfdInnNFsCabAfterScroll(DataSet: TDataSet);
begin
  ReopenEfdInnNFsIts(0);
end;

procedure TFmVSPlCCab.QrEfdInnNFsCabBeforeClose(DataSet: TDataSet);
begin
  QrEfdInnNFsIts.Close;
end;

procedure TFmVSPlCCab.QrVSItsBxaAfterScroll(DataSet: TDataSet);
begin
{
    VS_CRC_PF.ReopenVSIts_Controle_If(QrVSItsGer, QrVSItsBxaDstNivel2.Value);
}
end;

procedure TFmVSPlCCab.QrVSItsBxaBeforeClose(DataSet: TDataSet);
begin
{
  QrVSItsGer.Close;
}
end;

(*
object QrEfdInnNFsCab: TMySQLQuery
  Database = Dmod.MyDB
  BeforeClose = QrEfdInnNFsCabBeforeClose
  AfterScroll = QrEfdInnNFsCabAfterScroll
  SQL.Strings = (
    'SELECT vin.*, '
    'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  '
    'FROM efdinnnfscab vin '
    'LEFT JOIN entidades ter ON ter.Codigo=vin.Terceiro '
    ' ')
  Left = 808
  Top = 290
  object QrEfdInnNFsCabNO_TER: TWideStringField
    FieldName = 'NO_TER'
    Size = 100
  end
  object QrEfdInnNFsCabMovFatID: TIntegerField
    FieldName = 'MovFatID'
    Required = True
  end
  object QrEfdInnNFsCabMovFatNum: TIntegerField
    FieldName = 'MovFatNum'
    Required = True
  end
  object QrEfdInnNFsCabMovimCod: TIntegerField
    FieldName = 'MovimCod'
    Required = True
  end
  object QrEfdInnNFsCabEmpresa: TIntegerField
    FieldName = 'Empresa'
    Required = True
  end
  object QrEfdInnNFsCabControle: TIntegerField
    FieldName = 'Controle'
    Required = True
  end
  object QrEfdInnNFsCabTerceiro: TIntegerField
    FieldName = 'Terceiro'
    Required = True
  end
  object QrEfdInnNFsCabPecas: TFloatField
    FieldName = 'Pecas'
    Required = True
  end
  object QrEfdInnNFsCabPesoKg: TFloatField
    FieldName = 'PesoKg'
    Required = True
  end
  object QrEfdInnNFsCabAreaM2: TFloatField
    FieldName = 'AreaM2'
    Required = True
  end
  object QrEfdInnNFsCabAreaP2: TFloatField
    FieldName = 'AreaP2'
    Required = True
  end
  object QrEfdInnNFsCabValorT: TFloatField
    FieldName = 'ValorT'
    Required = True
  end
  object QrEfdInnNFsCabMotorista: TIntegerField
    FieldName = 'Motorista'
    Required = True
  end
  object QrEfdInnNFsCabPlaca: TWideStringField
    FieldName = 'Placa'
  end
  object QrEfdInnNFsCabCOD_MOD: TSmallintField
    FieldName = 'COD_MOD'
    Required = True
  end
  object QrEfdInnNFsCabCOD_SIT: TSmallintField
    FieldName = 'COD_SIT'
    Required = True
  end
  object QrEfdInnNFsCabSER: TIntegerField
    FieldName = 'SER'
    Required = True
  end
  object QrEfdInnNFsCabNUM_DOC: TIntegerField
    FieldName = 'NUM_DOC'
    Required = True
  end
  object QrEfdInnNFsCabCHV_NFE: TWideStringField
    FieldName = 'CHV_NFE'
    Size = 44
  end
  object QrEfdInnNFsCabNFeStatus: TIntegerField
    FieldName = 'NFeStatus'
    Required = True
  end
  object QrEfdInnNFsCabDT_DOC: TDateField
    FieldName = 'DT_DOC'
    Required = True
  end
  object QrEfdInnNFsCabDT_E_S: TDateField
    FieldName = 'DT_E_S'
    Required = True
  end
  object QrEfdInnNFsCabVL_DOC: TFloatField
    FieldName = 'VL_DOC'
    Required = True
  end
  object QrEfdInnNFsCabIND_PGTO: TWideStringField
    FieldName = 'IND_PGTO'
    Size = 1
  end
  object QrEfdInnNFsCabVL_DESC: TFloatField
    FieldName = 'VL_DESC'
    Required = True
  end
  object QrEfdInnNFsCabVL_ABAT_NT: TFloatField
    FieldName = 'VL_ABAT_NT'
    Required = True
  end
  object QrEfdInnNFsCabVL_MERC: TFloatField
    FieldName = 'VL_MERC'
    Required = True
  end
  object QrEfdInnNFsCabIND_FRT: TWideStringField
    FieldName = 'IND_FRT'
    Size = 1
  end
  object QrEfdInnNFsCabVL_FRT: TFloatField
    FieldName = 'VL_FRT'
    Required = True
  end
  object QrEfdInnNFsCabVL_SEG: TFloatField
    FieldName = 'VL_SEG'
    Required = True
  end
  object QrEfdInnNFsCabVL_OUT_DA: TFloatField
    FieldName = 'VL_OUT_DA'
    Required = True
  end
  object QrEfdInnNFsCabVL_BC_ICMS: TFloatField
    FieldName = 'VL_BC_ICMS'
    Required = True
  end
  object QrEfdInnNFsCabVL_ICMS: TFloatField
    FieldName = 'VL_ICMS'
    Required = True
  end
  object QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField
    FieldName = 'VL_BC_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsCabVL_ICMS_ST: TFloatField
    FieldName = 'VL_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsCabVL_IPI: TFloatField
    FieldName = 'VL_IPI'
    Required = True
  end
  object QrEfdInnNFsCabVL_PIS: TFloatField
    FieldName = 'VL_PIS'
    Required = True
  end
  object QrEfdInnNFsCabVL_COFINS: TFloatField
    FieldName = 'VL_COFINS'
    Required = True
  end
  object QrEfdInnNFsCabVL_PIS_ST: TFloatField
    FieldName = 'VL_PIS_ST'
    Required = True
  end
  object QrEfdInnNFsCabVL_COFINS_ST: TFloatField
    FieldName = 'VL_COFINS_ST'
    Required = True
  end
  object QrEfdInnNFsCabNFe_FatID: TIntegerField
    FieldName = 'NFe_FatID'
    Required = True
  end
  object QrEfdInnNFsCabNFe_FatNum: TIntegerField
    FieldName = 'NFe_FatNum'
    Required = True
  end
  object QrEfdInnNFsCabNFe_StaLnk: TSmallintField
    FieldName = 'NFe_StaLnk'
    Required = True
  end
  object QrEfdInnNFsCabVSVmcWrn: TSmallintField
    FieldName = 'VSVmcWrn'
    Required = True
  end
  object QrEfdInnNFsCabVSVmcObs: TWideStringField
    FieldName = 'VSVmcObs'
    Size = 60
  end
  object QrEfdInnNFsCabVSVmcSeq: TWideStringField
    FieldName = 'VSVmcSeq'
    Size = 25
  end
  object QrEfdInnNFsCabVSVmcSta: TSmallintField
    FieldName = 'VSVmcSta'
    Required = True
  end
  object QrEfdInnNFsCabCliInt: TIntegerField
    FieldName = 'CliInt'
    Required = True
  end
end
object DsEfdInnNFsCab: TDataSource
  DataSet = QrEfdInnNFsCab
  Left = 808
  Top = 340
end
object QrEfdInnNFsIts: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  '

      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
      'e)), '
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, '
    'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, '
    'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, '
    'pgt.Tipo_Item) Tipo_Item '
    'FROM efdinnnfsits     vin'
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX'
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
    'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2'
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip'
    'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ')
  Left = 896
  Top = 290
  object QrEfdInnNFsItsMovFatID: TIntegerField
    FieldName = 'MovFatID'
    Required = True
  end
  object QrEfdInnNFsItsMovFatNum: TIntegerField
    FieldName = 'MovFatNum'
    Required = True
  end
  object QrEfdInnNFsItsMovimCod: TIntegerField
    FieldName = 'MovimCod'
    Required = True
  end
  object QrEfdInnNFsItsEmpresa: TIntegerField
    FieldName = 'Empresa'
    Required = True
  end
  object QrEfdInnNFsItsMovimNiv: TIntegerField
    FieldName = 'MovimNiv'
    Required = True
  end
  object QrEfdInnNFsItsMovimTwn: TIntegerField
    FieldName = 'MovimTwn'
    Required = True
  end
  object QrEfdInnNFsItsControle: TIntegerField
    FieldName = 'Controle'
    Required = True
  end
  object QrEfdInnNFsItsConta: TIntegerField
    FieldName = 'Conta'
    Required = True
  end
  object QrEfdInnNFsItsGraGru1: TIntegerField
    FieldName = 'GraGru1'
    Required = True
  end
  object QrEfdInnNFsItsGraGruX: TIntegerField
    FieldName = 'GraGruX'
    Required = True
  end
  object QrEfdInnNFsItsprod_vProd: TFloatField
    FieldName = 'prod_vProd'
    Required = True
  end
  object QrEfdInnNFsItsprod_vFrete: TFloatField
    FieldName = 'prod_vFrete'
    Required = True
  end
  object QrEfdInnNFsItsprod_vSeg: TFloatField
    FieldName = 'prod_vSeg'
    Required = True
  end
  object QrEfdInnNFsItsprod_vOutro: TFloatField
    FieldName = 'prod_vOutro'
    Required = True
  end
  object QrEfdInnNFsItsQTD: TFloatField
    FieldName = 'QTD'
    Required = True
  end
  object QrEfdInnNFsItsUNID: TWideStringField
    FieldName = 'UNID'
    Size = 6
  end
  object QrEfdInnNFsItsVL_ITEM: TFloatField
    FieldName = 'VL_ITEM'
    Required = True
  end
  object QrEfdInnNFsItsVL_DESC: TFloatField
    FieldName = 'VL_DESC'
    Required = True
  end
  object QrEfdInnNFsItsIND_MOV: TWideStringField
    FieldName = 'IND_MOV'
    Size = 1
  end
  object QrEfdInnNFsItsCST_ICMS: TIntegerField
    FieldName = 'CST_ICMS'
  end
  object QrEfdInnNFsItsCFOP: TIntegerField
    FieldName = 'CFOP'
  end
  object QrEfdInnNFsItsCOD_NAT: TWideStringField
    FieldName = 'COD_NAT'
    Size = 10
  end
  object QrEfdInnNFsItsVL_BC_ICMS: TFloatField
    FieldName = 'VL_BC_ICMS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_ICMS: TFloatField
    FieldName = 'ALIQ_ICMS'
    Required = True
  end
  object QrEfdInnNFsItsVL_ICMS: TFloatField
    FieldName = 'VL_ICMS'
    Required = True
  end
  object QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField
    FieldName = 'VL_BC_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_ST: TFloatField
    FieldName = 'ALIQ_ST'
    Required = True
  end
  object QrEfdInnNFsItsVL_ICMS_ST: TFloatField
    FieldName = 'VL_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsItsIND_APUR: TWideStringField
    FieldName = 'IND_APUR'
    Size = 1
  end
  object QrEfdInnNFsItsCST_IPI: TWideStringField
    FieldName = 'CST_IPI'
    Size = 2
  end
  object QrEfdInnNFsItsCOD_ENQ: TWideStringField
    FieldName = 'COD_ENQ'
    Size = 3
  end
  object QrEfdInnNFsItsVL_BC_IPI: TFloatField
    FieldName = 'VL_BC_IPI'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_IPI: TFloatField
    FieldName = 'ALIQ_IPI'
    Required = True
  end
  object QrEfdInnNFsItsVL_IPI: TFloatField
    FieldName = 'VL_IPI'
    Required = True
  end
  object QrEfdInnNFsItsCST_PIS: TSmallintField
    FieldName = 'CST_PIS'
  end
  object QrEfdInnNFsItsVL_BC_PIS: TFloatField
    FieldName = 'VL_BC_PIS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_PIS_p: TFloatField
    FieldName = 'ALIQ_PIS_p'
    Required = True
  end
  object QrEfdInnNFsItsQUANT_BC_PIS: TFloatField
    FieldName = 'QUANT_BC_PIS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_PIS_r: TFloatField
    FieldName = 'ALIQ_PIS_r'
    Required = True
  end
  object QrEfdInnNFsItsVL_PIS: TFloatField
    FieldName = 'VL_PIS'
    Required = True
  end
  object QrEfdInnNFsItsCST_COFINS: TSmallintField
    FieldName = 'CST_COFINS'
  end
  object QrEfdInnNFsItsVL_BC_COFINS: TFloatField
    FieldName = 'VL_BC_COFINS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_COFINS_p: TFloatField
    FieldName = 'ALIQ_COFINS_p'
    Required = True
  end
  object QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField
    FieldName = 'QUANT_BC_COFINS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_COFINS_r: TFloatField
    FieldName = 'ALIQ_COFINS_r'
    Required = True
  end
  object QrEfdInnNFsItsVL_COFINS: TFloatField
    FieldName = 'VL_COFINS'
    Required = True
  end
  object QrEfdInnNFsItsCOD_CTA: TWideStringField
    FieldName = 'COD_CTA'
    Size = 255
  end
  object QrEfdInnNFsItsVL_ABAT_NT: TFloatField
    FieldName = 'VL_ABAT_NT'
    Required = True
  end
  object QrEfdInnNFsItsDtCorrApo: TDateTimeField
    FieldName = 'DtCorrApo'
    Required = True
  end
  object QrEfdInnNFsItsxLote: TWideStringField
    FieldName = 'xLote'
  end
  object QrEfdInnNFsItsOri_IPIpIPI: TFloatField
    FieldName = 'Ori_IPIpIPI'
    Required = True
  end
  object QrEfdInnNFsItsOri_IPIvIPI: TFloatField
    FieldName = 'Ori_IPIvIPI'
    Required = True
  end
  object QrEfdInnNFsItsNO_PRD_TAM_COR: TWideStringField
    FieldName = 'NO_PRD_TAM_COR'
    Size = 157
  end
  object QrEfdInnNFsItsSIGLAUNIDMED: TWideStringField
    FieldName = 'SIGLAUNIDMED'
    Size = 6
  end
  object QrEfdInnNFsItsGerBxaEstq: TSmallintField
    FieldName = 'GerBxaEstq'
  end
  object QrEfdInnNFsItsNCM: TWideStringField
    FieldName = 'NCM'
    Size = 10
  end
  object QrEfdInnNFsItsUnidMed: TIntegerField
    FieldName = 'UnidMed'
  end
  object QrEfdInnNFsItsEx_TIPI: TWideStringField
    FieldName = 'Ex_TIPI'
    Size = 3
  end
  object QrEfdInnNFsItsGrandeza: TSmallintField
    FieldName = 'Grandeza'
  end
  object QrEfdInnNFsItsTipo_Item: TSmallintField
    FieldName = 'Tipo_Item'
  end
end
object DsEfdInnNFsIts: TDataSource
  DataSet = QrEfdInnNFsIts
  Left = 896
  Top = 340
end
*)
(*
object DBGInnNFsCab: TDBGrid
  Left = 0
  Top = 0
  Width = 1000
  Height = 117
  Align = alTop
  TabOrder = 0
  TitleFont.Charset = DEFAULT_CHARSET
  TitleFont.Color = clWindowText
  TitleFont.Height = -12
  TitleFont.Name = 'MS Sans Serif'
  TitleFont.Style = []
end
object DBGInnNFsIts: TDBGrid
  Left = 0
  Top = 122
  Width = 1000
  Height = 135
  Align = alClient
  TabOrder = 1
  TitleFont.Charset = DEFAULT_CHARSET
  TitleFont.Color = clWindowText
  TitleFont.Height = -12
  TitleFont.Name = 'MS Sans Serif'
  TitleFont.Style = []
end
*)
end.

