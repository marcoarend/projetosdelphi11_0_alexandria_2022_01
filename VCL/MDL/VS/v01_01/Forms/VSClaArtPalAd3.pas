unit VSClaArtPalAd3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, Vcl.Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnProjGroup_Vars, UnProjGroup_Consts,
  UnAppEnums;

type
  TFmVSClaArtPalAd3 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrVSPallet: TmySQLQuery;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    QrVSPalletLk: TIntegerField;
    QrVSPalletDataCad: TDateField;
    QrVSPalletDataAlt: TDateField;
    QrVSPalletUserCad: TIntegerField;
    QrVSPalletUserAlt: TIntegerField;
    QrVSPalletAlterWeb: TSmallintField;
    QrVSPalletAtivo: TSmallintField;
    QrVSPalletEmpresa: TIntegerField;
    QrVSPalletNO_EMPRESA: TWideStringField;
    QrVSPalletStatus: TIntegerField;
    QrVSPalletCliStat: TIntegerField;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletNO_CLISTAT: TWideStringField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletNO_STATUS: TWideStringField;
    DsVSPallet: TDataSource;
    PnPartida: TPanel;
    LaVSRibCad: TLabel;
    Label30: TLabel;
    EdIMEI: TdmkEdit;
    EdCodigo: TdmkEdit;
    GBTecla: TGroupBox;
    Painel: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    LaVSRibCla: TLabel;
    SbPallet1: TSpeedButton;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    PnTecla: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit1: TDBEdit;
    PnBox: TPanel;
    EdNO_PRD_TAM_COR: TdmkEdit;
    Label31: TLabel;
    EdMovimCod: TdmkEdit;
    EdSrcPallet: TdmkEdit;
    Label5: TLabel;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    SBPallet: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPalletChange(Sender: TObject);
    procedure SbPallet1Click(Sender: TObject);
    procedure SBPalletClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FExigeSrcPallet: Boolean;
    FMovimIDGer: TEstqMovimID;
    FBxaGraGruX, FBxaMovimID, FBxaMovimNiv, FBxaSrcNivel1, FBxaSorcNivel2, FBox,
    FEmpresa, FFornecMO, FClientMO, FFornecedor, FVSMulFrnCab: Integer;
    FPal: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of Integer;
    FMovimID: TEstqMovimID;
  end;

  var
  FmVSClaArtPalAd3: TFmVSClaArtPalAd3;

implementation

uses
  UnMyObjects, Module, DmkDAC_PF, UMySQLModule, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSClaArtPalAd3.BtOKClick(Sender: TObject);
var
  VMI_Baix, CtrlSorc, CtrlDest, VSPallet, Codigo, MovimCod, Controle, GraGruX,
  Ficha, SrcPallet, StqCenLoc: Integer;
  Campo, Tabela: String;
begin
  VMI_Baix := EdIMEI.ValueVariant;
  VSPallet := EdPallet.ValueVariant;
  SrcPallet := EdSrcPallet.ValueVariant;
  if FExigeSrcPallet then
    if MyObjects.FIC(SrcPallet = 0, nil, 'Pallet de origem exigido mas n�o informado!') then
      Exit;
  if MyObjects.FIC(VSPallet = 0, EdPallet, 'Informe o pallet!') then
    Exit;
  FPal[FBox] := VSPallet;
  //
  case FMovimID of
    emidReclasXXUni:
      Tabela := 'vsparclcaba';
    emidClassArtXXUni:
      Tabela := 'vspaclacaba';
    else
    begin
      Geral.MB_Erro('A vari�vel FMovimIDGer n�o foi definida! Avise a Dermatek!');
      Exit;
    end;
  end;
  //
  Codigo    := EdCodigo.ValueVariant;
  MovimCod  := EdMovimCod.ValueVariant;
  Campo     := VS_CRC_PF.CampoLstPal(FBox);
  GraGruX   := QrVSPalletGraGruX.Value;
  StqCenLoc := EdStqCenLoc.ValueVariant;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local! (2)') then
    Exit;
  //
  if VS_CRC_PF.PalletDuplicad3(VAR_CLA_ART_RIB_MAX_BOX_15, FPal) then
      Exit;
  //
  case FMovimID of
    emidClassArtXXUni:
    begin
      Controle := VS_CRC_PF.InsAltVSPalCla(FEmpresa, FClientMO, FFornecedor, FVSMulFrnCab, FMovimID, Codigo,
                    // 2015-05-13
                    // Errado!!!
                    //FBxaGraGruX, FBxaMovimID, FBxaMovimNiv, FBxaSrcNivel1, FBxaSorcNivel2, MovimCod,
                    // certo !!!
                    MovimCod, FBxaGraGruX, FBxaMovimID, FBxaMovimNiv, FBxaSrcNivel1, FBxaSorcNivel2,
                    // FIM 2015-05-13
                    VMI_Baix, FBox, VSPallet, GraGruX, StqCenLoc,
                    iuvpei033(*Classifica��o de artigo re ribeira (UNI) 4/4*),
                    iuvpei034(*Baixa de artigo de ribeira em classifica��o (UNI) 4/4*),
                    LaAviso1, LaAviso2, CtrlSorc, CtrlDest);
    end;
    emidReclasXXUni:
    begin
      Controle := VS_CRC_PF.InsAltVSPalRclNewUni(FEmpresa, FFornecMO, FClientMO,
                    FFornecedor, FVSMulFrnCab, FMovimID, Codigo, MovimCod,
                    FBxaGraGruX, FBxaMovimID, FBxaMovimNiv, FBxaSrcNivel1,
                    FBxaSorcNivel2, VMI_Baix, FBox, VSPallet, GragruX,
                    SrcPallet, StqCenLoc,
                    iuvpei048(*Reclassifica��o de artigo de ribeira (UNI) 4/4*),
                    iuvpei049(*Baixa de artigo de ribeira em reclassifica��o (UNI) 4/4*),
                    LaAviso1, LaAviso2, CtrlSorc,
                    CtrlDest);
(*
      Controle := VS_CRC_PF.InsAltVSPalRclOld(FEmpresa, FFornecedor, FMovimID, Codigo,
                    FBxaGraGruX, FBxaMovimNiv, FBxaSrcNivel1, FBxaSorcNivel2,
                    MovimCod, VMI_Baix, FBox, VSPallet, GraGruX, LaAviso1,
                    LaAviso2, CtrlSorc, CtrlDest);
*)
    end;
    else
    begin
      Geral.MB_Erro(
      '"TEstqMovimID" n�o definido em "TFmVSClaArtPalAdd.BtOKClick()"');
      Exit;
    end;
  end;
  if Controle <> 0 then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
      Campo], ['Codigo'], [Controle], [Codigo], True)
    then
      Close;
  end;
end;

procedure TFmVSClaArtPalAd3.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSClaArtPalAd3.EdPalletChange(Sender: TObject);
begin
  PnTecla.Visible := EdPallet.ValueVariant <> 0;
end;

procedure TFmVSClaArtPalAd3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSClaArtPalAd3.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
  FExigeSrcPallet := True;
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSClaArtPalAd3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSClaArtPalAd3.SbPallet1Click(Sender: TObject);
const
  GraGruX = 0;
begin
  VS_CRC_PF.CadastraPalletRibCla(FEmpresa, FClientMO, EdPallet, CBPallet,
    QrVSPallet, FMovimID, GraGruX);
end;

procedure TFmVSClaArtPalAd3.SBPalletClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSPallet(EdPallet.ValueVariant);
end;

end.
