unit VSGerArtItsCurMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, AppListas, dmkCheckGroup, UnProjGroup_Consts, dmkDBGridZTO,
  UnAppEnums;

type
  TFmVSGerArtItsCurMul = class(TForm)
    GroupBox1: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrItensFicha: TmySQLQuery;
    DsItensFicha: TDataSource;
    QrItensFichaCodigo: TIntegerField;
    QrItensFichaControle: TIntegerField;
    QrItensFichaMovimCod: TIntegerField;
    QrItensFichaMovimNiv: TIntegerField;
    QrItensFichaMovimTwn: TIntegerField;
    QrItensFichaEmpresa: TIntegerField;
    QrItensFichaTerceiro: TIntegerField;
    QrItensFichaCliVenda: TIntegerField;
    QrItensFichaMovimID: TIntegerField;
    QrItensFichaLnkNivXtr1: TIntegerField;
    QrItensFichaLnkNivXtr2: TIntegerField;
    QrItensFichaDataHora: TDateTimeField;
    QrItensFichaPallet: TIntegerField;
    QrItensFichaGraGruX: TIntegerField;
    QrItensFichaPecas: TFloatField;
    QrItensFichaPesoKg: TFloatField;
    QrItensFichaAreaM2: TFloatField;
    QrItensFichaAreaP2: TFloatField;
    QrItensFichaSrcMovID: TIntegerField;
    QrItensFichaSrcNivel1: TIntegerField;
    QrItensFichaSrcNivel2: TIntegerField;
    QrItensFichaObserv: TWideStringField;
    QrItensFichaLk: TIntegerField;
    QrItensFichaDataCad: TDateField;
    QrItensFichaDataAlt: TDateField;
    QrItensFichaUserCad: TIntegerField;
    QrItensFichaUserAlt: TIntegerField;
    QrItensFichaAlterWeb: TSmallintField;
    QrItensFichaAtivo: TSmallintField;
    QrItensFichaFicha: TIntegerField;
    QrItensFichaMisturou: TSmallintField;
    QrItensFichaCustoMOKg: TFloatField;
    QrItensFichaCustoMOTot: TFloatField;
    QrItensFichaNO_PRD_TAM_COR: TWideStringField;
    GBAptos: TGroupBox;
    DBGItensFicha: TdmkDBGridZTO;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    QrItensFichaSerieFch: TIntegerField;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    QrItensFichaNO_SerieFch: TWideStringField;
    QrItensFichaMarca: TWideStringField;
    QrNiv1: TmySQLQuery;
    QrNiv1FatorInt: TFloatField;
    GroupBox2: TGroupBox;
    Panel3: TPanel;
    Label6: TLabel;
    EdControleBxa: TdmkEdit;
    LaPecasBxa: TLabel;
    EdTotSrcPeca: TdmkEdit;
    LaPesoKgBxa: TLabel;
    EdTotSrcPeso: TdmkEdit;
    SbPesoKgBxa: TSpeedButton;
    LaQtdGerArM2Bxa: TLabel;
    EdTotSrcArM2: TdmkEditCalc;
    LaQtdGerArP2Bxa: TLabel;
    EdTotSrcArP2: TdmkEditCalc;
    Label9: TLabel;
    EdObservBxa: TdmkEdit;
    CGTpCalcAutoBxa: TdmkCheckGroup;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    LaQtdGerArM2Src: TLabel;
    LaQtdGerArP2Src: TLabel;
    Label12: TLabel;
    EdControleNew: TdmkEdit;
    EdTotDstPeca: TdmkEdit;
    EdTotDstPeso: TdmkEdit;
    EdTotDstArM2: TdmkEditCalc;
    EdTotDstArP2: TdmkEditCalc;
    EdObservNew: TdmkEdit;
    QrNiv1MediaMinM2: TFloatField;
    QrNiv1MediaMaxM2: TFloatField;
    Label8: TLabel;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    QrItensFichaValorMP: TFloatField;
    QrItensFichaCustoPQ: TFloatField;
    QrItensFichaSdoPeca: TFloatField;
    QrItensFichaSdoPeso: TFloatField;
    QrItensFichaSdoArM2: TFloatField;
    QrItensFichaSdoArP2: TFloatField;
    QrSumFicha: TmySQLQuery;
    DsSumFicha: TDataSource;
    DBGSumFicha: TDBGrid;
    QrSumFichaSerieFch: TIntegerField;
    QrSumFichaNO_SerFicha: TWideStringField;
    QrSumFichaFicha: TIntegerField;
    QrSumFichaSdoPeca: TFloatField;
    QrSumFichaSdoPeso: TFloatField;
    QrSumFichaSdoArM2: TFloatField;
    QrSumFichaSdoArP2: TFloatField;
    QrSumFichaCustoArM2: TFloatField;
    QrSumFichaCustoPeca: TFloatField;
    QrSumFichaCustoPeso: TFloatField;
    CkContinuar: TCheckBox;
    Panel1: TPanel;
    Label5: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    EdSrcGGX: TdmkEdit;
    Label2: TLabel;
    Label14: TLabel;
    EdSrcMovID: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    Label1: TLabel;
    Label17: TLabel;
    EdSrcNivel2: TdmkEdit;
    EdMovimTwn: TdmkEdit;
    Label13: TLabel;
    Panel5: TPanel;
    Label10: TLabel;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    Label11: TLabel;
    Panel7: TPanel;
    Label15: TLabel;
    EdSumPeca: TdmkEdit;
    Label16: TLabel;
    EdSumPeso: TdmkEdit;
    Label18: TLabel;
    EdSumArM2: TdmkEditCalc;
    EdSumArP2: TdmkEditCalc;
    Label19: TLabel;
    Splitter1: TSplitter;
    QrItensFichaNFeSer: TSmallintField;
    QrItensFichaNFeNum: TIntegerField;
    QrItensFichaVSMulNFeCab: TIntegerField;
    QrSumFichaMarca: TWideStringField;
    QrGGXJmp: TmySQLQuery;
    QrGGXJmpGraGru1: TIntegerField;
    QrGGXJmpControle: TIntegerField;
    QrGGXJmpNO_PRD_TAM_COR: TWideStringField;
    QrGGXJmpSIGLAUNIDMED: TWideStringField;
    QrGGXJmpCODUSUUNIDMED: TIntegerField;
    QrGGXJmpNOMEUNIDMED: TWideStringField;
    DsGGXJmp: TDataSource;
    Label20: TLabel;
    EdJmpGGX: TdmkEditCB;
    CBJmpGGX: TdmkDBLookupComboBox;
    QrItensFichaDstGGX: TIntegerField;
    SbStqCenLoc: TSpeedButton;
    QrSumFichaValorMP: TFloatField;
    QrSumFichaValMPPeso: TFloatField;
    QrSumFichaValMPArM2: TFloatField;
    QrSumFichaValMPPeca: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure DBGItensFichaDblClick(Sender: TObject);
    procedure EdTotSrcPecaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTotSrcPecaChange(Sender: TObject);
    procedure EdSerieFchChange(Sender: TObject);
    procedure SbPesoKgBxaClick(Sender: TObject);
    procedure EdTotDstPecaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGSumFichaDblClick(Sender: TObject);
    procedure QrSumFichaBeforeClose(DataSet: TDataSet);
    procedure QrSumFichaAfterScroll(DataSet: TDataSet);
    procedure QrItensFichaAfterOpen(DataSet: TDataSet);
    procedure DBGItensFichaAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure QrGGXJmpAfterOpen(DataSet: TDataSet);
    procedure SbStqCenLocClick(Sender: TObject);
  private
    { Private declarations }
    FFatorIntSrc, FFatorIntDst: Integer;
    FMediaMinM2, FMediaMaxM2: Double;
    //
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure FechaPesquisa();
    procedure ReopenVSGerArtSrc(Controle: Integer);
    function  ValorMPParcial(): Double;
    function  ValorTParcial(): Double;
    function  ValorMOTot(): Double;
    //procedure LiberaEdicaoUni(Libera: Boolean);
    procedure LiberaEdicaoMul(Libera: Boolean);
    procedure SomaItens();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FOrigMovimNiv: TEstqMovimNiv;
    FEmpresa, FClientMO, FFornecMO, FOrigMovimCod, FOrigCodigo, FNewGraGruX, FTipoArea: Integer;
    FCustoMOKg: Double;
    FDataHora: TDateTime;
    //
    procedure ReopenItensAptos();
    procedure DefineTipoArea();
  end;

  var
  FmVSGerArtItsCurMul: TFmVSGerArtItsCurMul;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSGerArtCab, UnVS_CRC_PF, GetValor;

{$R *.DFM}

procedure TFmVSGerArtItsCurMul.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmVSGerArtItsCurMul.BtOKClick(Sender: TObject);
const
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  //MovimTwn   = 0; // Nao usar aqui! usar na classificacao???
  Pallet     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  //Misturou   = 0;
  AptoUso    = 1;
  //
  ExigeFornecedor = True;
  ExigeAreaouPeca = True;
  //
  EdPallet    = nil;
  EdValorT    = nil;
  EdAreaM2    = nil;
  //
  PedItsLib   = 0;
  PedItsFin   = 0;
  PedItsVda   = 0;
  //
  GSPSrcMovID  = TEStqMovimID(0);
  GSPSrcNiv2  = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  //
  RmsMovID = emidAjuste;
  RmsNivel1 = 0;
  RmsNivel2= 0;
  GSPJmpMovID = emidAjuste;
  GSPJmpNiv2 = 0;
var
  DataHora, Observ, Marca: String;
  Codigo, Controle, MovimCod, Empresa, ClienteMO, GraGruX, Fornecedor, SrcNivel1,
  SrcNivel2, DstNivel1, DstNivel2, SerieFch, Ficha, SrcGGX, DstGGX,
  MovimTwn, StqCenLoc, ReqMovEstq: Integer;
  //AreaM2, AreaP2, Pecas, PesoKg, ValorMP, ValorT, QtdGerArM2, QtdGerArP2, CustoMOTot,
  FatorMP, FatorAR, NotaMPAG, FatNotaVNC, FatNotaVRC: Double;
  DstMovID, SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  TpCalcAuto, ClientMO: Integer;
  Qry: TmySQLQuery;
  //
  BxaTotSrcPeca, BxaTotSrcPeso, BxaTotSrcArM2, BxaTotSrcArP2, BxaTotDstArM2,
  BxaTotDstArP2, BxaTotValorMP, BxaTotCuMOTot, BxaTotValorT: Double;
  //
  BxaItmSrcPeca, BxaItmSrcPeso, BxaItmSrcArM2, BxaItmSrcArP2, BxaItmDstArM2,
  BxaItmDstArP2, BxaItmValorMP, BxaItmCuMOTot, BxaItmValorT: Double;
  //
  BxaResSrcPeca, BxaResSrcPeso, BxaResSrcArM2, BxaResSrcArP2, BxaResDstArM2,
  BxaResDstArP2, BxaResValorMP, BxaResCuMOTot, BxaResValorT: Double;
  //
  GerTotSrcArM2, GerTotSrcArP2,
  GerResSrcArM2, GerResSrcArP2,
  GerItmSrcArM2, GerItmSrcArP2: Double;
  GerTotDstPeca, GerTotDstPeso, GerTotDstArM2, GerTotDstArP2,
  GerResDstPeca, GerResDstPeso, GerResDstArM2, GerResDstArP2,
  GerItmDstPeca, GerItmDstPeso, GerItmDstArM2, GerItmDstArP2: Double;
  GerTotValorMP, GerTotCuMOTot, GerTotValorT: Double;
  GerResValorMP, GerResCuMOTot, GerResValorT: Double;
  GerItmValorMP, GerItmCuMOTot, GerItmValorT: Double;
  //
  FtrPeso, FtrArM2, FtrPeca, FrtPecaDst,
  SdoPeso, SdoPeca, SdoArM2, SdoArP2, AreaM2, AreaP2: Double;
  I: Integer;
  JmpMovID: TEstqMovimID;
  JmpNivel1, JmpNivel2, JmpGGX, RmsGGX, MovCodPai, FornecMO: Integer;
begin
  DBGItensFicha.Enabled := False;
  SdoPeso        := EdSumPeso.ValueVariant;
  SdoPeca        := EdSumPeca.ValueVariant;
  SdoArM2        := EdSumArM2.ValueVariant;
  SdoArP2        := EdSumArP2.ValueVariant;
  //
  Codigo         := EdCodigo.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  Controle       := EdControleBxa.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := FEmpresa;
  ClienteMO      := FClientMO;
  FornecMO       := FFornecMO;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidIndsXX;
  //MovimNiv       := eminBaixCurtiXX;
  Observ         := EdObservBxa.Text;
  // BAIXA
  BxaTotSrcPeca  := -EdTotSrcPeca.ValueVariant;
  BxaTotSrcPeso  := -EdTotSrcPeso.ValueVariant;
  BxaTotSrcArM2  := -EdTotSrcArM2.ValueVariant;
  BxaTotSrcArP2  := -EdTotSrcArP2.ValueVariant;
  BxaTotDstArM2  := -EdTotDstArM2.ValueVariant;
  BxaTotDstArP2  := -EdTotDstArP2.ValueVariant;
  // ini 2023-04-15
  //BxaTotValorMP        := -ValorTParcial();
  //BxaTotValorT         := ValorMP;
  BxaTotValorMP        := -ValorMPParcial();
  BxaTotValorT         := -ValorTParcial();
  // fim 2023-04-15
  BxaTotCuMOTot  := 0;
  //
  BxaResSrcPeca  := BxaTotSrcPeca;
  BxaResSrcPeso  := BxaTotSrcPeso;
  BxaResSrcArM2  := BxaTotSrcArM2;
  BxaResSrcArP2  := BxaTotSrcArP2;
  BxaResDstArM2  := BxaTotDstArM2;
  BxaResDstArP2  := BxaTotDstArP2;
  BxaResValorMP  := BxaTotValorMP;
  BxaResCuMOTot  := BxaTotCuMOTot;
  BxaResValorT   := BxaTotValorT;
  // GERACAO
  GerTotDstPeca  := EdTotDstPeca.ValueVariant;
  GerTotDstPeso  := EdTotSrcPeso.ValueVariant;
  GerTotDstArM2  := 0;
  GerTotDstArP2  := 0;
  GerTotDstArM2  := EdTotDstArM2.ValueVariant;
  GerTotDstArP2  := EdTotDstArP2.ValueVariant;
  // ini 2023-04-15
  //GerTotValorMP  := ValorTParcial();
  //GerTotValorT   := GerTotValorMP + GerTotCuMOTot;
  GerTotValorMP  := ValorMPParcial();
  //GerTotValorT   := GerTotValorMP + GerTotCuMOTot;
  GerTotValorT   := ValorTParcial();
  // fim 2023-04-15
  GerTotCuMOTot  := ValorMOTot();
  //
  GerResDstPeca  := GerTotDstPeca;
  GerResDstPeso  := GerTotDstPeso;
  GerResSrcArM2  := GerTotSrcArM2;
  GerResSrcArP2  := GerTotSrcArP2;
  GerResDstArM2  := GerTotDstArM2;
  GerResDstArP2  := GerTotDstArP2;
  GerResValorMP  := GerTotValorMP;
  GerResCuMOTot  := GerTotCuMOTot;
  GerResValorT   := GerTotValorT;
  //
  FatorMP  := VS_CRC_PF.FatorNotaCC(QrItensFichaSrcNivel2.Value, QrItensFichaMovimID.Value);
  FatorAR  := VS_CRC_PF.FatorNotaAR(FNewGraGruX);
  NotaMPAG := VS_CRC_PF.NotaCouroRibeiraApuca(-BxaTotSrcPeca, -BxaTotSrcPeso, -BxaTotDstArM2, FatorMP, FatorAR);
  FatNotaVRC := FatorAR;
  FatNotaVNC := FatorMP;
  //
  if GerTotDstPeca > SdoPeca then
  begin
    if Geral.MB_Pergunta(
      'A quantidade de pe�as a ser baixada � superior ao estoque dispon�vel!' +
      sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES
    then
      Exit;
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
  end;
  //QrItensFicha.First;
  //while not QrItensFicha.Eof do
  //begin
  with DBGItensFicha.DataSource.DataSet do
  for I := 0 to DBGItensFicha.SelectedRows.Count - 1 do
  begin
    //GotoBookmark(pointer(DBGItensFicha.SelectedRows.Items[I]));
    GotoBookmark(DBGItensFicha.SelectedRows.Items[I]);
    //
    FtrPeso := QrItensFichaSdoPeso.Value / SdoPeso;
    FtrArM2 := FtrPeso; //SdoArM2.Value / SdoArM2;
    FtrPeca := QrItensFichaSdoPeca.Value / SdoPeca;
    //
    //if QrItensFicha.RecNo = QrItensFicha.RecordCount then
    if I = DBGItensFicha.SelectedRows.Count - 1 then
    begin
      BxaItmSrcPeca := BxaResSrcPeca;
      BxaItmSrcPeso := BxaResSrcPeso;
      BxaItmSrcArM2 := BxaResSrcArM2;
      BxaItmSrcArP2 := BxaResSrcArP2;
      BxaItmDstArM2 := BxaResDstArM2;
      BxaItmDstArP2 := BxaResDstArP2;
      BxaItmValorMP := BxaResValorMP;
      BxaItmCuMOTot := BxaResCuMOTot;
      BxaItmValorT  := BxaResValorT;
    end else
    begin
      BxaItmSrcPeca  := Trunc(Geral.RoundC(BxaTotSrcPeca * FtrPeca, 0));
      BxaItmSrcPeso  := Geral.RoundC(BxaTotSrcPeso * FtrPeso, 3);
      BxaItmSrcArM2  := Geral.RoundC(BxaTotSrcArM2 * FtrArM2, 2);
      BxaItmSrcArP2  := Geral.RoundC(BxaTotSrcArP2 * FtrArM2, 2);
      BxaItmDstArM2  := Geral.RoundC(BxaTotDstArM2 * FtrArM2, 2);
      BxaItmDstArP2  := Geral.RoundC(BxaTotDstArP2 * FtrArM2, 2);
      BxaItmValorMP  := Geral.RoundC(BxaTotValorMP * FtrPeso, 2);
      BxaItmCuMOTot  := Geral.RoundC(BxaTotCuMOTot * FtrPeso, 2);
      BxaItmValorT   := Geral.RoundC(BxaTotValorT  * FtrPeso, 2);
      //
      BxaResSrcPeca  := BxaResSrcPeca - BxaItmSrcPeca;
      BxaResSrcPeso  := BxaResSrcPeso - BxaItmSrcPeso;
      BxaResSrcArM2  := BxaResSrcArM2 - BxaItmSrcArM2;
      BxaResSrcArP2  := BxaResSrcArP2 - BxaItmSrcArP2;
      BxaResDstArM2  := BxaResDstArM2 - BxaItmDstArM2;
      BxaResDstArP2  := BxaResDstArP2 - BxaItmDstArP2;
      BxaResValorMP  := BxaResValorMP - BxaItmValorMP;
      BxaResCuMOTot  := BxaResCuMOTot - BxaItmCuMOTot;
      BxaResValorT   := BxaResValorT  - BxaItmValorT;
    end;
    //
    Fornecedor     := QrItensFichaTerceiro.Value;
    GraGruX        := QrItensFichaGraGruX.Value;
    SerieFch       := QrItensFichaSerieFch.Value;
    Ficha          := QrItensFichaFicha.Value;
    Marca          := QrItensFichaMarca.Value;
    //
    (*DstMovID       := TEstqMovimID(EdSrcMovID.ValueVariant);
    DstNivel1      := EdSrcNivel1.ValueVariant;
    DstNivel2      := EdSrcNivel2.ValueVariant;
    DstGGX         := EdSrcGGX.ValueVariant;*)
    //
    DstMovID       := TEstqMovimID(0);
    DstNivel1      := 0;
    DstNivel2      := 0;
    DstGGX         := 0;
    //
    SrcMovID       := TEstqMovimID(QrItensFichaMovimID.Value);
    SrcNivel1      := QrItensFichaCodigo.Value;
    SrcNivel2      := QrItensFichaControle.Value;
    SrcGGX         := QrItensFichaGraGruX.Value;
    //
    StqCenLoc      := EdStqCenLoc.ValueVariant;
    ReqMovEstq     := 0;
    ClientMO       := FClientMO;
    //2017-11-14
    JmpMovID       := TEstqMovimID.emidCurtido;
    JmpNivel1      := QrItensFichaCodigo.Value;
    JmpNivel2      := 0;
    JmpGGX         := EdJmpGGX.ValueVariant;
    RmsGGX         := QrItensFichaDstGGX.Value;
    if MyObjects.FIC(JmpGGX = 0, EdJmpGGX, 'Informe a mat�ria-prima jumped!') then
      Exit;
    MovCodPai      := QrItensFichaMovimCod.Value;
    //Fim 2017-11-14
    //
    //if not CkSemArea.Checked then
      if MyObjects.FIC(GerTotDstArM2 < 0.01, EdTotDstArM2, 'Informe a  �rea!') then
        Exit;
    if not VS_CRC_PF.AreaEstaNaMedia(EdTotDstPeca.ValueVariant, GerTotDstArM2, FMediaMinM2, FMediaMaxM2, True) then
        Exit;
    //
    //
    if VS_CRC_PF.VSFic(GraGruX, Empresa, ClienteMO, Fornecedor, Pallet, Ficha,
      GerTotDstPeca, GerTotDstArM2, BxaTotSrcPeso, GerTotValorT, (*EdGraGruX*)nil,
      EdPallet, (*EdFicha*) nil, EdTotSrcPeca, EdAreaM2, EdTotSrcPeso, EdValorT,
      ExigeFornecedor, CO_GraGruY_2048_VSRibCad, ExigeAreaouPeca, EdStqCenLoc)
    then
      Exit;
    //
    //
    TpCalcAuto := CGTpCalcAutoBxa.Value;
    // Baixa couro In Natura
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
    // 2015-05-09 Calcular Nota MPAG
    DstMovID       := MovimID;
    DstNivel1      := EdSrcNivel1.ValueVariant;
    DstNivel2      := EdControleNew.ValueVariant;
    DstNivel2      := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, DstNivel2);
    DstGGX         := EdSrcGGX.ValueVariant;
    // FIM 2015-05-09 Calcular Nota MPAG
    //
    MovimNiv       := eminBaixCurtiXX; //2023-05-09
    //
   if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
    Empresa, Fornecedor, MovimID, MovimNiv, Pallet, GraGruX,
    BxaItmSrcPeca, BxaItmSrcPeso, BxaItmSrcArM2, BxaItmSrcArP2, BxaItmValorT,
    DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
    CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2,
    BxaItmCuMOTot, BxaItmValorMP, DstMovID,
    DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, BxaItmDstArM2, BxaItmDstArP2,
    AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    JmpMovID, JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2,
    RmsGGX, GSPJmpMovID, GSPJmpNiv2, MovCodPai, CO_0_VmiPai,
    (*
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    *)
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iuvpei021(*Gera��o de artigo (MUL) de ribeira de curtimento*)) then
    begin
      //FtrPeso := QrItensFichaSdoPeso.Value / EdTotDstPeso.ValueVariant;
      FtrArM2 := FtrPeso; //QrItensFichaSdoArM2.Value / EdTotDstArM2.ValueVariant;
      FtrPeca := QrItensFichaSdoPeca.Value / EdTotDstPeca.ValueVariant;
      //
      FrtPecaDst := 0;
      if EdTotSrcPeca.ValueVariant <> 0 then
        FrtPecaDst := EdTotDstPeca.ValueVariant / EdTotSrcPeca.ValueVariant;
      if FrtPecaDst < 0 then
        FrtPecaDst := - FrtPecaDst;
      if FrtPecaDst = 0 then
        FrtPecaDst := 1;
      //
      VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
      VS_CRC_PF.AtualizaSerieNFeVMI(Controle, QrItensFichaNFeSer.Value,
        QrItensFichaNFeNum.Value, QrItensFichaVSMulNFeCab.Value);
      // Geracao Couro Curtido
      Controle       := DstNivel2;
      //
      MovimNiv       := eminSorcCurtiXX;
      GraGruX        := EdSrcGGX.ValueVariant;
      //
      Observ         := EdObservNew.Text;
      //
      DstMovID       := TEstqMovimID(EdSrcMovID.ValueVariant);
      DstNivel1      := EdSrcNivel1.ValueVariant;
      DstNivel2      := EdSrcNivel2.ValueVariant;
      DstGGX         := EdSrcGGX.ValueVariant;
      //
      SrcMovID       := TEstqMovimID(0);
      SrcNivel1      := 0;
      SrcNivel2      := 0;
      SrcGGX         := 0;
      //
      TpCalcAuto     := 0;
      //
      StqCenLoc      := EdStqCenLoc.ValueVariant;
      ReqMovEstq     := EdReqMovEstq.ValueVariant;
      //
      //if QrItensFicha.RecNo = QrItensFicha.RecordCount then
      if I = DBGItensFicha.SelectedRows.Count - 1 then
      begin
        GerItmDstPeca := GerResDstPeca;
        GerItmDstPeso := GerResDstPeso;
        GerItmSrcArM2 := GerResSrcArM2;
        GerItmSrcArP2 := GerResSrcArP2;
        GerItmDstArM2 := GerResDstArM2;
        GerItmDstArP2 := GerResDstArP2;
        GerItmValorMP := GerResValorMP;
        GerItmCuMOTot := GerResCuMOTot;
        GerItmValorT  := GerResValorT;
      end else begin
        GerItmDstPeca  := Trunc(Geral.RoundC(GerTotDstPeca * FtrPeca * FrtPecaDst, 0));
        if (GerItmDstPeca = 0) and (GerTotDstPeca > 0) then
          GerItmDstPeca := 1;
        GerItmDstPeso  := Geral.RoundC(GerTotDstPeso * FtrPeso, 3);
        GerItmSrcArM2  := Geral.RoundC(GerTotSrcArM2 * FtrArM2, 2);
        GerItmSrcArP2  := Geral.RoundC(GerTotSrcArP2 * FtrArM2, 2);
        GerItmDstArM2  := Geral.RoundC(GerTotDstArM2 * FtrArM2, 2);
        GerItmDstArP2  := Geral.RoundC(GerTotDstArP2 * FtrArM2, 2);
        GerItmValorMP  := Geral.RoundC(GerTotValorMP * FtrArM2, 2);
        GerItmCuMOTot  := Geral.RoundC(GerTotCuMOTot * FtrPeso, 2);
        GerItmValorT   := Geral.RoundC(GerTotValorT  * FtrPeso, 2);
        //
        GerResDstPeca  := GerResDstPeca - GerItmDstPeca;
        GerResDstPeso  := GerResDstPeso - GerItmDstPeso;
        GerResSrcArM2  := GerResSrcArM2 - GerItmSrcArM2;
        GerResSrcArP2  := GerResSrcArP2 - GerItmSrcArP2;
        GerResDstArM2  := GerResDstArM2 - GerItmDstArM2;
        GerResDstArP2  := GerResDstArP2 - GerItmDstArP2;
        GerResValorMP  := GerResValorMP - GerItmValorMP;
        GerResCuMOTot  := GerResCuMOTot - GerItmCuMOTot;
        GerResValorT   := GerResValorT  - GerItmValorT;
      end;
      AreaM2 := 0.00;
      AreaP2 := 0.00;
      if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
      Empresa, Fornecedor, MovimID, MovimNiv, Pallet, GraGruX,
      GerItmDstPeca, GerItmDstPeso, AreaM2, AreaP2, GerItmValorT,
      DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
      CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2,
      GerItmCuMOTot, GerItmValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca,
      QtdGerPeso, GerItmDstArM2, GerItmDstArP2,
      AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
      TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
      CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
      CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
      CO_0_JmpMovID,
      CO_0_JmpNivel1,
      CO_0_JmpNivel2,
      CO_0_JmpGGX,
      CO_0_RmsMovID,
      CO_0_RmsNivel1,
      CO_0_RmsNivel2,
      CO_0_RmsGGX,
      CO_0_GSPJmpMovID,
      CO_0_GSPJmpNiv2,
      CO_0_MovCodPai,
      CO_0_VmiPai,
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      iuvpei022(*Baixa de mat�ria prima na gera��o de artigo (MUL) de ribeira de curtimento*)) then
      begin
        VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
        VS_CRC_PF.AtualizaSerieNFeVMI(Controle, QrItensFichaNFeSer.Value,
          QrItensFichaNFeNum.Value, QrItensFichaVSMulNFeCab.Value);
        // Nao se aplica!
        //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
        // Atualizar dados de baixa do In Natura selecionado
        //VS_CRC_PF.AtualizaSaldoIMEI(QrAptosControle.Value, True);
        VS_CRC_PF.AtualizaSaldoItmCur(QrItensFichaControle.Value);
        VS_CRC_PF.AtualizaTotaisVSCurCab(QrItensFichaMovimCod.Value);
        // Aumentar Saldo e custo do Artigo de Ribeira gerado
        VS_CRC_PF.DistribuiCustoIndsVS(FOrigMovimNiv, FOrigMovimCod, FOrigCodigo,
          EdSrcNivel2.ValueVariant);
        //
        FmVSGerArtCab.AtualizaNFeItens();
        //
        FmVSGerArtCab.LocCod(Codigo, Codigo);
        ReopenVSGerArtSrc(Controle);
        //
      end;
    end;
    //QrItensFicha.Next;
    // Next i
  end;
  if CkContinuar.Checked then
  begin
    EdMovimTwn.ValueVariant       := 0;
    CGTpCalcAutoBxa.Value         := 2;  // So PesoKg a principio!
    //
    ImgTipo.SQLType               := stIns;
    LiberaEdicaoMul(False);
    ReopenItensAptos();
    //
    //EdGraGruX.ValueVariant        := 0;
    //CBGraGruX.KeyValue            := Null;
    //
    EdControleBxa.ValueVariant    := 0;
    EdTotSrcPeca.ValueVariant     := 0;
    EdTotSrcPeso.ValueVariant     := 0;
    EdTotSrcArM2.ValueVariant     := 0;
    EdTotSrcArP2.ValueVariant     := 0;
    EdObservBxa.Text              := '';
    //
    EdControleNew.ValueVariant    := 0;
    EdTotDstPeca.ValueVariant     := 0;
    EdTotDstPeso.ValueVariant     := 0;
    EdTotDstArM2.ValueVariant     := 0;
    EdTotDstArP2.ValueVariant     := 0;
    EdObservNew.Text              := '';
    //
    DBGItensFicha.Enabled := True;
    DBGSumFicha.Enabled := True;
  end else
    Close;
end;

procedure TFmVSGerArtItsCurMul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerArtItsCurMul.DBGItensFichaAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
begin
  SomaItens();
end;

procedure TFmVSGerArtItsCurMul.DBGItensFichaDblClick(Sender: TObject);
begin
  //LiberaEdicaoUni(True);
end;

procedure TFmVSGerArtItsCurMul.DBGSumFichaDblClick(Sender: TObject);
begin
  LiberaEdicaoMul(True);
end;

procedure TFmVSGerArtItsCurMul.DefineTipoArea();
begin
  LaQtdGerArM2Src.Enabled := FTipoArea = 0;
  EdTotDstArM2.Enabled := FTipoArea = 0;
  //
  LaQtdGerArP2Src.Enabled := FTipoArea = 1;
  EdTotDstArP2.Enabled := FTipoArea = 1;
end;

procedure TFmVSGerArtItsCurMul.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsCurMul.EdGraGruXChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsCurMul.EdTotSrcPecaChange(Sender: TObject);
  procedure CalculaPesoParcial();
  var
    Pecas, PesoKg, AreaM2: Double;
  begin
    Pecas  := EdTotSrcPeca.ValueVariant;
    PesoKg := 0;
    //if QrSumFichaSdoPeca.Value > 0 then
    if EdSumPeca.ValueVariant > 0 then
      //PesoKg := Pecas / QrSumFichaSdoPeca.Value * QrSumFichaSdoPeso.Value;
      PesoKg := Pecas / EdSumPeca.ValueVariant * EdSumPeso.ValueVariant;
    EdTotSrcPeso.ValueVariant := PesoKg;
    //
    AreaM2 := 0;
    //if QrSumFichaSdoArM2.Value > 0 then
    if EdSumArM2.ValueVariant > 0 then
      //AreaM2 := Pecas / QrSumFichaSdoPeca.Value * QrSumFichaSdoArM2.Value;
      AreaM2 := Pecas / EdSumPeca.ValueVariant * EdSumArM2.ValueVariant;
    EdTotSrcArM2.ValueVariant  := AreaM2;
  end;
var
  Pecas: Double;
begin
  // ver pelas pecas se eh o resto e pegar todo resto do peso
  Pecas := EdTotSrcPeca.ValueVariant;
  if (QrSumFicha.State <> dsInactive) and (QrSumFicha.RecordCount > 0)
  //and (Pecas >= QrSumFichaSdoPeca.Value) then
  and (Pecas >= EdSumPeca.ValueVariant) then
  begin
    //EdTotSrcPeso.ValueVariant  := QrSumFichaSdoPeso.Value;
    EdTotSrcPeso.ValueVariant  := EdSumPeso.ValueVariant;
    LaPesoKgBxa.Enabled  := False;
    EdTotSrcPeso.Enabled  := False;
    //EdTotSrcArM2.ValueVariant  := QrSumFichaSdoArM2.Value;
    EdTotSrcArM2.ValueVariant  := EdSumArM2.ValueVariant;
  end else
  begin
    //LaPesoKg.Enabled  := GBGerar.Visible;
    //EdPesoKg.Enabled  := GBGerar.Visible;
    CalculaPesoParcial();
  end;
  if (FFatorIntSrc <> 0) and (FFatorIntDst <> 0) then
    EdTotDstPeca.ValueVariant := Pecas * FFatorIntSrc / FFatorIntDst;
end;

procedure TFmVSGerArtItsCurMul.EdTotSrcPecaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Txt: String;
  Pecas: Double;
begin
  if Geral.IntInConjunto(1, CGTpCalcAutoBxa.Value) then
    CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value - 1;
  //
  if Key = VK_F4 then
  begin
    //EdTotSrcPeca.ValueVariant := QrSumFichaSdoPeca.Value;
    EdTotSrcPeca.ValueVariant := EdSumPeca.ValueVariant;
    //EdTotSrcPeso.ValueVariant := QrSumFichaSdoPeso.Value;
    EdTotSrcPeso.ValueVariant := EdSumPeso.ValueVariant;
    if not Geral.IntInConjunto(1, CGTpCalcAutoBxa.Value) then
      CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value + 1;
  end else
  if Key = VK_F5 then
  begin
    Txt := '0,00';
    if InputQuery('Pe�as', 'Informe as pe�as a gerar:', Txt) then
    begin
      Pecas := Geral.DMV(Txt);
      EdTotSrcPeca.ValueVariant := Pecas / 2;
      EdTotDstPeca.ValueVariant := Pecas;
    end;
  end;
end;

procedure TFmVSGerArtItsCurMul.EdTotDstPecaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Fator: Integer;
  Num: String;
begin
  if Key = VK_F3 then
  begin
    Num := InputBox('Gera��o de couros', 'Informe a quantidade de pe�as:', EdTotDstPeca.Text);
    EdTotDstPeca.ValueVariant := Geral.DMV(Num);
  end else
  if Key = VK_F5 then
  begin
    Fator := MyObjects.SelRadioGroup('Fator de divis�o',
    'Escolha a forma de divis�o', [
    'Sem reparti��o ao meio',
    'Foi repartido de inteiros para meios'],
    -1);
    case Fator of
      0: Fator := 1;
      1: Fator := 2;
      else Fator := 0;
    end;
    if Fator <> 0 then
      EdTotDstPeca.ValueVariant := EdTotSrcPeca.ValueVariant * Fator;
  end;
end;

procedure TFmVSGerArtItsCurMul.EdSerieFchChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsCurMul.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtItsCurMul.FechaPesquisa();
begin
  QrSumFicha.Close;
end;

procedure TFmVSGerArtItsCurMul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSGerArtItsCurMul.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VS_CRC_PF.AbreVSSerFch(QrVSSerFch);
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1024_VSNatCad));
  VS_CRC_PF.AbreGraGruXY(QrGGXJmp,
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_1877_VSCouCur) + //',' +
    ')');
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  CGTpCalcAutoBxa.Value := 2; // so peso a principio
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0,
    TEstqMovimID.emidCurtido);
end;

procedure TFmVSGerArtItsCurMul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerArtItsCurMul.LiberaEdicaoMul(Libera: Boolean);
var
  Status: Boolean;
  FatorIntSrc, FatDif, FatIgu: Integer;
begin
  FatDif := 0;
  FatIgu := 0;
  //
  if Libera then
    Status := (QrSumFicha.RecordCount > 0) and (QrItensFicha.RecordCount > 0)
  else
    Status := False;
  //
  if Status then
  begin
    BtOK.Enabled := True;
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(EdSrcGGX.ValueVariant),
    '']);
    FFatorIntDst := Trunc(QrNiv1FatorInt.Value * 1000);
    FMediaMinM2  := QrNiv1MediaMinM2.Value;
    FMediaMaxM2  := QrNiv1MediaMaxM2.Value;
    //
    QrItensFicha.First;
    while not QrItensFicha.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
      'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
      'FROM couNiv1 co1 ',
      'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
      'WHERE ggc.GraGruX=' + Geral.FF0(QrItensFichaGraGruX.Value),
      '']);
      //FFatorIntSrc := Trunc(QrNiv1FatorInt.Value * 1000);
      FatorIntSrc := Trunc(QrNiv1FatorInt.Value * 1000);
      //
      //if (FFatorIntSrc = 0) or (FFatorIntDst = 0) then
      if (FatorIntSrc = 0) or (FFatorIntDst = 0) then
      begin
        Geral.MB_Aviso('Sele��o abortada!' + sLineBreak +
        'O Artigo "' + FmVSGerArtCab.QrVSGerArtNewNO_PRD_TAM_COR.Value +
        '" n�o est� configurado completamente!' + sLineBreak +
        'Termine sua configura��o antes de continuar!');
        //
        Close;
        Exit;
      end;
      //if FFatorIntSrc <> FFatorIntDst then
      if FatorIntSrc <> FFatorIntDst then
        FatDif := FatDif + 1
      else
        FatIgu := FatIgu + 1;
      //
      QrItensFicha.Next;
    end;
    QrItensFicha.First;
    if (FatDif > 0) and (FatIgu > 0) then
    begin
      Geral.MB_Aviso('Sele��o abortada!' + sLineBreak +
      'Existem Artigos com fator de corte divergentes!');
      //
      Close;
      Exit;
    end;
    FFatorIntSrc := FatorIntSrc;
    if FatDif > 0 then
      Geral.MB_Aviso('CUIDADO!!!' + sLineBreak +
      'Ao informar a quantidade leve em considera��o a possibilidade de transform��o de inteiros para meios!!!');
  end else
    BtOK.Enabled := False;
  //
  //GBAptos.Enabled := not Status;
  DBGSumFicha.Enabled := not Status;

  GBGerar.Visible := Status;
  //
  //
  if Libera and (EdTotSrcPeca.Enabled) and (EdTotSrcPeca.Visible) then
    EdTotSrcPeca.SetFocus;
  //
end;

procedure TFmVSGerArtItsCurMul.QrGGXJmpAfterOpen(DataSet: TDataSet);
begin
  if QrGGXJmp.RecordCount = 1 then
  begin
    EdJmpGGX.ValueVariant := QrGGXJmpControle.Value;
    CBJmpGGX.KeyValue     := QrGGXJmpControle.Value;
  end;
end;

procedure TFmVSGerArtItsCurMul.QrItensFichaAfterOpen(DataSet: TDataSet);
begin
  MyObjects.DBGridSelectAll(TDBGrid(DBGItensFicha));
  SomaItens();
end;

procedure TFmVSGerArtItsCurMul.QrSumFichaAfterScroll(DataSet: TDataSet);
var
  SQL_GraGruX, SQL_Terceiro, SQL_SerieFch, SQL_Ficha: String;
begin
  SQL_GraGruX  := '';
  SQL_Terceiro := '';
(*
  if EdGraGruX.ValueVariant <> 0 then
    SQL_GraGruX  := 'AND vmi.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant);
  if EdTerceiro.ValueVariant <> 0 then
    SQL_Terceiro  := 'AND vmi.Terceiro=' + Geral.FF0(EdTerceiro.ValueVariant);
*)
  //
  SQL_SerieFch  := 'AND vmi.SerieFch=' + Geral.FF0(QrSumFichaSerieFch.Value);
  SQL_Ficha  := 'AND vmi.Ficha=' + Geral.FF0(QrSumFichaFicha.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrItensFicha, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'QtdAntPeca-QtdGerPeca SdoPeca, ',
  'QtdAntPeso-QtdGerPeso SdoPeso, ',
  'QtdAntArM2-QtdGerArM2 SdoArM2, ',
  'QtdAntArP2-QtdGerArP2 SdoArP2 ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vsmovcab vmc ON vmc.Codigo=vmi.Codigo ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  //'LEFT JOIN vsnatart   vna ON vna.VSNatCad=vmi.GraGruX',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidEmProcCur)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCur)),
  'AND QtdAntPeca>QtdGerPeca ',
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND ClientMO=' + Geral.FF0(FClientMO),
  SQL_GraGruX,
  SQL_Terceiro,
  SQL_SerieFch,
  SQL_Ficha,
  'ORDER BY vmi.Controle ',
  '']);
  //
  //Geral.MB_Teste(QrItensFicha.SQL.Text);
end;

procedure TFmVSGerArtItsCurMul.QrSumFichaBeforeClose(DataSet: TDataSet);
begin
  QrItensFicha.Close;
end;

{
procedure TFmVSGerArtItsCurMul.LiberaEdicaoUni(Libera: Boolean);
var
  Status: Boolean;
begin
  if Libera then
    Status := QrAptos.RecordCount > 0
  else
    Status := False;
  //
  if Status then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(EdSrcGGX.ValueVariant),
    '']);
    FFatorIntDst := Trunc(QrNiv1FatorInt.Value * 1000);
    FMediaMinM2  := QrNiv1MediaMinM2.Value;
    FMediaMaxM2  := QrNiv1MediaMaxM2.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(QrAptosGraGruX.Value),
    '']);
    FFatorIntSrc := Trunc(QrNiv1FatorInt.Value * 1000);
    //
    if (FFatorIntSrc = 0) or (FFatorIntDst = 0) then
    begin
      Geral.MB_Aviso('Sele��o abortada!' + sLineBreak +
      'O Artigo "' + FmVSGerArtCab.QrVSGerArtNewNO_PRD_TAM_COR.Value +
      '" n�o est� configurado completamente!' + sLineBreak +
      'Termine sua configura��o antes de continuar!');
      //
      Close;
      Exit;
    end;
    if FFatorIntSrc <> FFatorIntDst then
      Geral.MB_Aviso('CUIDADO!!!' + sLineBreak +
      'Ao informar a quantidade leve em considera��o a possibilidade de transform��o de inteiros para meios!!!');
  end;
  //
  GBAptos.Enabled := not Status;

  GBGerar.Visible := Status;
  //
(*
  LaFicha.Enabled := not Status;
  EdFicha.Enabled := not Status;
  LaGraGruX.Enabled := not Status;
  EdGraGruX.Enabled := not Status;
  CBGraGruX.Enabled := not Status;
  LaTerceiro.Enabled := not Status;
  EdTerceiro.Enabled := not Status;
  CBTerceiro.Enabled := not Status;
  BtReabre.Enabled := not Status;
*)
  //
  if Libera and (EdTotSrcPeca.Enabled) and (EdTotSrcPeca.Visible) then
    EdTotSrcPeca.SetFocus;
  //
end;
}

procedure TFmVSGerArtItsCurMul.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmVSGerArtItsCurMul.ReopenItensAptos();
var
  SQL_GraGruX, SQL_Terceiro, SQL_SerieFch, SQL_Ficha: String;
begin
  SQL_GraGruX  := '';
  SQL_Terceiro := '';
  SQL_Ficha    := '';
  SQL_SerieFch := '';
(*
  if EdGraGruX.ValueVariant <> 0 then
    SQL_GraGruX  := 'AND vmi.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant);
  if EdTerceiro.ValueVariant <> 0 then
    SQL_Terceiro  := 'AND vmi.Terceiro=' + Geral.FF0(EdTerceiro.ValueVariant);
  if EdSerieFch.ValueVariant <> 0 then
    SQL_SerieFch  := 'AND vmi.SerieFch=' + Geral.FF0(EdSerieFch.ValueVariant);
  if EdFicha.ValueVariant <> 0 then
    SQL_Ficha  := 'AND vmi.Ficha=' + Geral.FF0(EdFicha.ValueVariant);
*)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumFicha, Dmod.MyDB, [
  'SELECT vmi.Marca, vmi.SerieFch, vsf.Nome NO_SerFicha,',
  'Ficha, SUM(QtdAntPeca-QtdGerPeca) SdoPeca,',
  'SUM(QtdAntPeso-QtdGerPeso) SdoPeso,',
  'SUM(QtdAntArM2-QtdGerArM2) SdoArM2,',
  'SUM(QtdAntArP2-QtdGerArP2) SdoArP2,',
  // ini 2023-05-13
  // ValorT deve ser igual a ValorMP + CustoPQ + CustoMOTot
(*
  '(SUM(ValorMP + CustoPQ) / SUM(QtdAntPeso)) CustoPeso, ',
  '(SUM(ValorMP + CustoPQ) / SUM(QtdAntArM2)) CustoArM2, ',
  '(SUM(ValorMP + CustoPQ) / SUM(QtdAntPeca)) CustoPeca, ',
*)
  '(SUM(ValorT) / SUM(QtdAntPeso)) CustoPeso, ',
  '(SUM(ValorT) / SUM(QtdAntArM2)) CustoArM2, ',
  '(SUM(ValorT) / SUM(QtdAntPeca)) CustoPeca, ',
  // fim 2023-05-13
  //
  '(SUM(ValorMP) / SUM(QtdAntPeso)) ValMPPeso, ',
  '(SUM(ValorMP) / SUM(QtdAntArM2)) ValMPArM2, ',
  '(SUM(ValorMP) / SUM(QtdAntPeca)) ValMPPeca, ',
  'SUM(ValorMP) ValorMP ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vsmovcab vmc ON vmc.Codigo=vmi.Codigo ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  //'LEFT JOIN vsnatart   vna ON vna.VSNatCad=vmi.GraGruX',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidEmProcCur)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcCur)),
  'AND QtdAntPeca>QtdGerPeca ',
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND ClientMO=' + Geral.FF0(FClientMO),
  SQL_GraGruX,
  SQL_Terceiro,
  SQL_SerieFch,
  SQL_Ficha,
  'GROUP BY SerieFch, Ficha',
  '']);
  //Geral.MB_Teste(QrSumFicha.SQL.Text);
end;

procedure TFmVSGerArtItsCurMul.ReopenVSGerArtSrc(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSGerArtItsCurMul.SbPesoKgBxaClick(Sender: TObject);
var
  ValVar: Variant;
  PesoKg: Double;
begin
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, 0, 3, 0,
  '0,000', EdTotSrcPeso.Text, True,
  'Peso kg', 'Informe o peso (kg): ', 0, ValVar) then
  begin
    EdTotSrcPeso.ValueVariant := Geral.DMV(ValVar);
    //FTpCalcAuto := UnAppListas.DefineTpCalcAutoCouro(False, True, False, False);
    if Geral.IntInConjunto(2, CGTpCalcAutoBxa.Value) then
      CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value - 2;
  end else;
end;

procedure TFmVSGerArtItsCurMul.SbStqCenLocClick(Sender: TObject);
begin
  VS_CRC_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, TEstqMovimID.emidCurtido);
end;

procedure TFmVSGerArtItsCurMul.SomaItens();
var
  I: Integer;
  Peca, Peso, ArM2, ArP2: Double;
begin
  Peca := 0;
  Peso := 0;
  ArM2 := 0;
  ArP2 := 0;
  //
  Screen.Cursor := crHourGlass;
  with DBGItensFicha.DataSource.DataSet do
  for I := 0 to DBGItensFicha.SelectedRows.Count - 1 do
  begin
    //GotoBookmark(pointer(DBGItensFicha.SelectedRows.Items[I]));
    GotoBookmark(DBGItensFicha.SelectedRows.Items[I]);
    Peca := Peca + QrItensFichaSdoPeca.Value;
    Peso := Peso + QrItensFichaSdoPeso.Value;
    ArM2 := ArM2 + QrItensFichaSdoArM2.Value;
    ArP2 := ArP2 + QrItensFichaSdoArP2.Value;
  end;
  EdSumPeca.ValueVariant := Peca;
  EdSumPeso.ValueVariant := Peso;
  EdSumArM2.ValueVariant := ArM2;
  EdSumArP2.ValueVariant := ArP2;
  //
  Screen.Cursor := crDefault;
end;

function TFmVSGerArtItsCurMul.ValorMOTot(): Double;
begin
  Result := EdTotSrcPeso.ValueVariant * FCustoMOKg;
end;

function TFmVSGerArtItsCurMul.ValorMPParcial(): Double;
begin
(* n�o pode! � o peso curtido e n�o o peso VS!!!
  if EdSumPeso.ValueVariant > 0 then
    Result := EdTotSrcPeso.ValueVariant * -QrSumFichaValMPPeso.Value
  else
  if EdSumArM2.ValueVariant > 0 then
    Result := EdTotSrcArM2.ValueVariant * -QrSumFichaValMPArM2.Value
  else
*)
  if EdSumPeca.ValueVariant > 0 then
    Result := EdTotSrcPeca.ValueVariant * -QrSumFichaValMPPeca.Value
  else
    Result := 0;
end;

function TFmVSGerArtItsCurMul.ValorTParcial(): Double;
begin
(* n�o pode! � o peso curtido e n�o o peso VS!!!
  if EdSumPeso.ValueVariant > 0 then
    Result := EdTotSrcPeso.ValueVariant * -QrSumFichaCustoPeso.Value
  else
  if EdSumArM2.ValueVariant > 0 then
    Result := EdTotSrcArM2.ValueVariant * -QrSumFichaCustoArM2.Value
  else
*)
  if EdSumPeca.ValueVariant > 0 then
    Result := EdTotSrcPeca.ValueVariant * -QrSumFichaCustoPeca.Value
  else
    Result := 0;
end;

(*
object QrGGXJmp: TmySQLQuery
  Database = Dmod.MyDB
  AfterOpen = QrGGXJmpAfterOpen
  SQL.Strings = (
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), '
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
    'FROM vsnatcad wmp'
    'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
  Left = 656
  Top = 216
  object QrGGXJmpGraGru1: TIntegerField
    FieldName = 'GraGru1'
  end
  object QrGGXJmpControle: TIntegerField
    FieldName = 'Controle'
  end
  object QrGGXJmpNO_PRD_TAM_COR: TWideStringField
    FieldName = 'NO_PRD_TAM_COR'
    Size = 157
  end
  object QrGGXJmpSIGLAUNIDMED: TWideStringField
    FieldName = 'SIGLAUNIDMED'
    Size = 6
  end
  object QrGGXJmpCODUSUUNIDMED: TIntegerField
    FieldName = 'CODUSUUNIDMED'
  end
  object QrGGXJmpNOMEUNIDMED: TWideStringField
    FieldName = 'NOMEUNIDMED'
    Size = 30
  end
end
object DsGGXJmp: TDataSource
  DataSet = QrGGXJmp
  Left = 656
  Top = 264
end


////////////////////////////////////////////////////////////////////////////////

object Label20: TLabel
  Left = 416
  Top = 44
  Width = 103
  Height = 13
  Caption = 'Mat'#233'ria-prima jumped:'
end
object EdJmpGGX: TdmkEditCB
  Left = 416
  Top = 60
  Width = 56
  Height = 21
  Alignment = taRightJustify
  TabOrder = 9
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ValMin = '-2147483647'
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  ValWarn = False
  DBLookupComboBox = CBJmpGGX
  IgnoraDBLookupComboBox = False
end
object CBJmpGGX: TdmkDBLookupComboBox
  Left = 472
  Top = 60
  Width = 377
  Height = 21
  KeyField = 'Controle'
  ListField = 'NO_PRD_TAM_COR'
  TabOrder = 10
  dmkEditCB = EdJmpGGX
  UpdType = utYes
  LocF7SQLMasc = '$#'
end

*)
end.
