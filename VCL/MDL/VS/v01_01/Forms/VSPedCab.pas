unit VSPedCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  frxClass, frxDBSet, UnDmkProcFunc, UnDmkENums, Variants, Vcl.ComCtrls,
  dmkMemo, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB,
  UnProjGroup_Consts, UnDmkListas;

type
  TFmVSPedCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrMPs: TmySQLQuery;
    QrMPsCodigo: TIntegerField;
    QrMPsNome: TWideStringField;
    DsMPs: TDataSource;
    QrFluxosIts: TmySQLQuery;
    QrFluxosItsCodigo: TIntegerField;
    QrFluxosItsControle: TIntegerField;
    QrFluxosItsOrdem: TIntegerField;
    QrFluxosItsOperacao: TIntegerField;
    QrFluxosItsAcao1: TWideStringField;
    QrFluxosItsAcao2: TWideStringField;
    QrFluxosItsAcao3: TWideStringField;
    QrFluxosItsAcao4: TWideStringField;
    QrFluxosItsLk: TIntegerField;
    QrFluxosItsDataCad: TDateField;
    QrFluxosItsDataAlt: TDateField;
    QrFluxosItsUserCad: TIntegerField;
    QrFluxosItsUserAlt: TIntegerField;
    QrFluxosItsNOMEOPERACAO: TWideStringField;
    QrFluxosItsSEQ: TIntegerField;
    DsFluxosIts: TDataSource;
    frxDsFluxosIts: TfrxDBDataset;
    QrVSPedCab: TmySQLQuery;
    DsVSPedCab: TDataSource;
    frxDsVSPedCab: TfrxDBDataset;
    QrVSPedIts: TmySQLQuery;
    DsVSPedIts: TDataSource;
    frxDsVSPedIts: TfrxDBDataset;
    QrPesqV: TmySQLQuery;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    frxOS_3: TfrxReport;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DsPediPrzCab: TDataSource;
    QrTransp: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsTransp: TDataSource;
    QrFornecedor: TmySQLQuery;
    QrFornecedorFAX_TXT: TWideStringField;
    QrFornecedorCEL_TXT: TWideStringField;
    QrFornecedorCPF_TXT: TWideStringField;
    QrFornecedorTEL_TXT: TWideStringField;
    QrFornecedorCEP_TXT: TWideStringField;
    QrFornecedorNOME: TWideStringField;
    QrFornecedorRUA: TWideStringField;
    QrFornecedorNUMERO: TLargeintField;
    QrFornecedorCOMPL: TWideStringField;
    QrFornecedorBAIRRO: TWideStringField;
    QrFornecedorCIDADE: TWideStringField;
    QrFornecedorPAIS: TWideStringField;
    QrFornecedorTELEFONE: TWideStringField;
    QrFornecedorFAX: TWideStringField;
    QrFornecedorCelular: TWideStringField;
    QrFornecedorCNPJ: TWideStringField;
    QrFornecedorIE: TWideStringField;
    QrFornecedorCEP: TLargeintField;
    QrFornecedorContato: TWideStringField;
    QrFornecedorUF: TLargeintField;
    QrFornecedorNOMEUF: TWideStringField;
    QrFornecedorEEMail: TWideStringField;
    QrFornecedorPEmail: TWideStringField;
    QrFornecedorNUMEROTXT: TWideStringField;
    QrFornecedorENDERECO: TWideStringField;
    QrFornecedorLOGRAD: TWideStringField;
    QrTransportador: TmySQLQuery;
    QrTransportadorTEL_TXT: TWideStringField;
    QrTransportadorCEL_TXT: TWideStringField;
    QrTransportadorCPF_TXT: TWideStringField;
    QrTransportadorFAX_TXT: TWideStringField;
    QrTransportadorCEP_TXT: TWideStringField;
    QrTransportadorNOME: TWideStringField;
    QrTransportadorRUA: TWideStringField;
    QrTransportadorNUMERO: TLargeintField;
    QrTransportadorCOMPL: TWideStringField;
    QrTransportadorBAIRRO: TWideStringField;
    QrTransportadorCIDADE: TWideStringField;
    QrTransportadorPAIS: TWideStringField;
    QrTransportadorTELEFONE: TWideStringField;
    QrTransportadorFAX: TWideStringField;
    QrTransportadorCelular: TWideStringField;
    QrTransportadorCNPJ: TWideStringField;
    QrTransportadorIE: TWideStringField;
    QrTransportadorCEP: TLargeintField;
    QrTransportadorContato: TWideStringField;
    QrTransportadorUF: TLargeintField;
    QrTransportadorNOMEUF: TWideStringField;
    QrTransportadorNUMEROTXT: TWideStringField;
    QrTransportadorENDERECO: TWideStringField;
    QrTransportadorEEMail: TWideStringField;
    QrTransportadorPEmail: TWideStringField;
    QrTransportadorLOGRAD: TWideStringField;
    QrTotais: TmySQLQuery;
    QrTotaisQtde: TFloatField;
    QrTotaisValor: TFloatField;
    QrClientes: TMySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    QrWBMovIts: TmySQLQuery;
    QrWBMovItsCodigo: TIntegerField;
    QrWBMovItsControle: TIntegerField;
    QrWBMovItsMovimCod: TIntegerField;
    QrWBMovItsEmpresa: TIntegerField;
    QrWBMovItsMovimID: TIntegerField;
    QrWBMovItsGraGruX: TIntegerField;
    QrWBMovItsPecas: TFloatField;
    QrWBMovItsPesoKg: TFloatField;
    QrWBMovItsAreaM2: TFloatField;
    QrWBMovItsAreaP2: TFloatField;
    QrWBMovItsLk: TIntegerField;
    QrWBMovItsDataCad: TDateField;
    QrWBMovItsDataAlt: TDateField;
    QrWBMovItsUserCad: TIntegerField;
    QrWBMovItsUserAlt: TIntegerField;
    QrWBMovItsAlterWeb: TSmallintField;
    QrWBMovItsAtivo: TSmallintField;
    QrWBMovItsNO_PRD_TAM_COR: TWideStringField;
    QrWBMovItsSrcMovID: TIntegerField;
    QrWBMovItsSrcNivel1: TIntegerField;
    QrWBMovItsSrcNivel2: TIntegerField;
    QrWBMovItsPallet: TIntegerField;
    QrWBMovItsNO_PALLET: TWideStringField;
    QrWBMovItsSdoVrtArM2: TFloatField;
    QrWBMovItsSdoVrtPeca: TFloatField;
    QrWBMovItsObserv: TWideStringField;
    QrWBMovItsValorT: TFloatField;
    QrWBMovItsLnkNivXtr1: TIntegerField;
    QrWBMovItsLnkNivXtr2: TIntegerField;
    DsWBMovIts: TDataSource;
    QrEmitCus: TmySQLQuery;
    QrEmitCusCodigo: TIntegerField;
    QrEmitCusDataEmis: TDateTimeField;
    QrEmitCusNumero: TIntegerField;
    QrEmitCusSetor: TSmallintField;
    QrEmitCusNOMESETOR: TWideStringField;
    QrEmitCusNOME: TWideStringField;
    QrEmitCusControle: TIntegerField;
    QrEmitCusCusto: TFloatField;
    QrEmitCusPecas: TFloatField;
    QrEmitCusPeso: TFloatField;
    QrEmitCusAreaM2: TFloatField;
    QrEmitCusPercTotCus: TFloatField;
    DsEmitCus: TDataSource;
    QrVSPedCabNOMEVENDEDOR: TWideStringField;
    QrVSPedCabNOMECLIENTE: TWideStringField;
    QrVSPedCabNOMETRANSP: TWideStringField;
    QrVSPedCabNO_CONDICAOPG: TWideStringField;
    QrVSPedCabCodigo: TIntegerField;
    QrVSPedCabEmpresa: TIntegerField;
    QrVSPedCabCliente: TIntegerField;
    QrVSPedCabVendedor: TIntegerField;
    QrVSPedCabDataF: TDateField;
    QrVSPedCabValor: TFloatField;
    QrVSPedCabObz: TWideStringField;
    QrVSPedCabTransp: TIntegerField;
    QrVSPedCabCondicaoPg: TIntegerField;
    QrVSPedCabPedidCli: TWideStringField;
    QrVSPedCabNome: TWideStringField;
    QrVSPedCabLk: TIntegerField;
    QrVSPedCabDataCad: TDateField;
    QrVSPedCabDataAlt: TDateField;
    QrVSPedCabUserCad: TIntegerField;
    QrVSPedCabUserAlt: TIntegerField;
    QrVSPedCabAlterWeb: TSmallintField;
    QrVSPedCabAtivo: TSmallintField;
    QrVSPedItsCodigo: TIntegerField;
    QrVSPedItsControle: TIntegerField;
    QrVSPedItsVSMovIts: TIntegerField;
    QrVSPedItsPecas: TFloatField;
    QrVSPedItsPesoKg: TFloatField;
    QrVSPedItsAreaM2: TFloatField;
    QrVSPedItsAreaP2: TFloatField;
    QrVSPedItsPrecoTipo: TSmallintField;
    QrVSPedItsPrecoMoeda: TIntegerField;
    QrVSPedItsPrecoVal: TFloatField;
    QrVSPedItsPercDesco: TFloatField;
    QrVSPedItsValor: TFloatField;
    QrVSPedItsTexto: TWideStringField;
    QrVSPedItsEntrega: TDateField;
    QrVSPedItsPronto: TDateField;
    QrVSPedItsStatus: TIntegerField;
    QrVSPedItsFluxo: TIntegerField;
    QrVSPedItsObserv: TWideMemoField;
    QrVSPedItsTipoProd: TSmallintField;
    QrVSPedItsCustoPQ: TFloatField;
    QrVSPedItsGraGruX: TIntegerField;
    QrVSPedItsReceiRecu: TIntegerField;
    QrVSPedItsReceiRefu: TIntegerField;
    QrVSPedItsReceiAcab: TIntegerField;
    QrVSPedItsCustoWB: TFloatField;
    QrVSPedItsLk: TIntegerField;
    QrVSPedItsDataCad: TDateField;
    QrVSPedItsDataAlt: TDateField;
    QrVSPedItsUserCad: TIntegerField;
    QrVSPedItsUserAlt: TIntegerField;
    QrVSPedItsAlterWeb: TSmallintField;
    QrVSPedItsAtivo: TSmallintField;
    QrVSPedItsNOMEFLUXO: TWideStringField;
    QrVSPedItsNO_Fluxo: TWideStringField;
    QrVSPedItsNO_ReceiRecu: TWideStringField;
    QrVSPedItsNO_ReceiRefu: TWideStringField;
    QrVSPedItsNO_ReceiAcab: TWideStringField;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsNOMETIPOPROD: TWideStringField;
    QrVSPedItsPRONTO_TXT: TWideStringField;
    QrVSPedItsSubTo: TFloatField;
    GBEdita: TGroupBox;
    PnEdit: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label5: TLabel;
    Label22: TLabel;
    Label38: TLabel;
    Label19: TLabel;
    LaCondicaoPG: TLabel;
    Label23: TLabel;
    EdCodigo: TdmkEdit;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    TPDataF: TdmkEditDateTimePicker;
    EdVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    MeObz: TdmkMemo;
    EdTransp: TdmkEditCB;
    CBTransp: TdmkDBLookupComboBox;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    EdPedidCli: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSPedCabPecas: TFloatField;
    QrVSPedCabPesoKg: TFloatField;
    QrVSPedCabAreaM2: TFloatField;
    QrVSPedCabAreaP2: TFloatField;
    EdNome: TdmkEdit;
    Label3: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label24: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit02: TDBEdit;
    DBEdit2: TDBEdit;
    DBMemo2: TDBMemo;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    GroupBox2: TGroupBox;
    DBMemo1: TDBMemo;
    DBGVsPedIts: TDBGrid;
    Label4: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label7: TLabel;
    DBEdit9: TDBEdit;
    Label11: TLabel;
    DBEdit10: TDBEdit;
    Label12: TLabel;
    DBEdit11: TDBEdit;
    Label13: TLabel;
    DBEdit12: TDBEdit;
    QrVSPedCabNOMEEMP: TWideStringField;
    DBEdit1: TDBEdit;
    DBGrid1: TDBGrid;
    Panel7: TPanel;
    DGDados: TDBGrid;
    Panel6: TPanel;
    QrEntregaEnti: TMySQLQuery;
    QrEntregaEntiCodigo: TIntegerField;
    QrEntregaEntiNOMEENTIDADE: TWideStringField;
    DsEntregaEnti: TDataSource;
    Label14: TLabel;
    EdEntregaEnti: TdmkEditCB;
    CBEntregaEnti: TdmkDBLookupComboBox;
    QrVSPedCabEntregaEnti: TIntegerField;
    QrVSPedCabNOMEEntregaEnti: TWideStringField;
    Label15: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    QrVSPedItsPerctrib: TFloatField;
    QrVSPedItsLibPecas: TFloatField;
    QrVSPedItsLibPesoKg: TFloatField;
    QrVSPedItsLibAreaM2: TFloatField;
    QrVSPedItsLibAreaP2: TFloatField;
    QrVSPedItsFinPecas: TFloatField;
    QrVSPedItsFinPesoKg: TFloatField;
    QrVSPedItsFinAreaM2: TFloatField;
    QrVSPedItsFinAreaP2: TFloatField;
    QrVSPedItsVdaPecas: TFloatField;
    QrVSPedItsVdaPesoKg: TFloatField;
    QrVSPedItsVdaAreaM2: TFloatField;
    QrVSPedItsVdaAreaP2: TFloatField;
    QrVSPedItsAWServerID: TIntegerField;
    QrVSPedItsAWStatSinc: TSmallintField;
    QrVSPedItsTipoSaldo_TXT: TWideStringField;
    QrVSPedItsSaldo: TFloatField;
    QrVSPedItsSigla: TWideStringField;
    QrVSPedItsSimbolo: TWideStringField;
    QrVSPedItsNO_Status: TWideStringField;
    QrVSPedItsNaoFinaliza: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGruYAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGruYBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGraGruYAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrGraGruYBeforeClose(DataSet: TDataSet);
    procedure QrVSPedCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPedCabBeforeOpen(DataSet: TDataSet);
    procedure QrVSPedItsAfterScroll(DataSet: TDataSet);
    procedure QrVSPedItsBeforeClose(DataSet: TDataSet);
    procedure QrVSPedItsCalcFields(DataSet: TDataSet);
    procedure frxOS_2GetValue(const VarName: string; var Value: Variant);
    procedure QrFornecedorCalcFields(DataSet: TDataSet);
    procedure QrTransportadorCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraVSPedIts(SQLType: TSQLType);
    procedure ImprimeOS_3();
    procedure ReopenFluxosIts();
    procedure ReopenVSMovIts();
    procedure ReopenEmitCus();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSPedIts(Controle: Integer);

  end;

var
  FmVSPedCab: TFmVSPedCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, DmkDAC_PF, VSPedIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSPedCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSPedCab.MostraVSPedIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSPedIts, FmVSPedIts, afmoNegarComAviso) then
  begin
    FmVSPedIts.ImgTipo.SQLType := SQLType;
    FmVSPedIts.FQrCab := QrVSPedCab;
    FmVSPedIts.FDsCab := DsVSPedCab;
    FmVSPedIts.FQrIts := QrVSPedIts;
    if SQLType = stIns then
      //
    else
    begin
      FmVSPedIts.EdControle.ValueVariant := QrVSPedItsControle.Value;
      //
      //
      FmVSPedIts.EdPecas.ValueVariant      := QrVSPedItsPecas.Value;
      FmVSPedIts.EdAreaM2.ValueVariant     := QrVSPedItsAreaM2.Value;
      FmVSPedIts.EdAreaP2.ValueVariant     := QrVSPedItsAreaP2.Value;
      FmVSPedIts.EdPesoKg.ValueVariant     := QrVSPedItsPesoKg.Value;
      FmVSPedIts.RGPrecoTipo.ItemIndex     := QrVSPedItsPrecoTipo.Value;
      FmVSPedIts.EdPrecoMoeda.ValueVariant := QrVSPedItsPrecoMoeda.Value;
      FmVSPedIts.CBPrecoMoeda.KeyValue     := QrVSPedItsPrecoMoeda.Value;
      FmVSPedIts.TPEntrega.Date            := QrVSPedItsEntrega.Value;
      FmVSPedIts.EdPrecoVal.ValueVariant   := QrVSPedItsPrecoVal.Value;
      FmVSPedIts.EdPercDesco.ValueVariant  := QrVSPedItsPercDesco.Value;
      FmVSPedIts.EdPercTrib.ValueVariant   := QrVSPedItsPercTrib.Value;
      FmVSPedIts.EdGraGruX.ValueVariant    := QrVSPedItsGraGruX.Value;
      FmVSPedIts.CBGraGruX.KeyValue        := QrVSPedItsGraGruX.Value;
      FmVSPedIts.EdFluxo.ValueVariant      := QrVSPedItsFluxo.Value;
      FmVSPedIts.CBFluxo.KeyValue          := QrVSPedItsFluxo.Value;
      FmVSPedIts.EdReceiRecu.ValueVariant  := QrVSPedItsReceiRecu.Value;
      FmVSPedIts.CBReceiRecu.KeyValue      := QrVSPedItsReceiRecu.Value;
      FmVSPedIts.EdReceiRefu.ValueVariant  := QrVSPedItsReceiRefu.Value;
      FmVSPedIts.CBReceiRefu.KeyValue      := QrVSPedItsReceiRefu.Value;
      FmVSPedIts.EdReceiAcab.ValueVariant  := QrVSPedItsReceiAcab.Value;
      FmVSPedIts.CBReceiAcab.KeyValue      := QrVSPedItsReceiAcab.Value;
      FmVSPedIts.EdTexto.ValueVariant      := QrVSPedItsTexto.Value;
      FmVSPedIts.MeObserv.Text             := QrVSPedItsObserv.Value;
      FmVSPedIts.RGTipoProd.ItemIndex      := QrVSPedItsTipoProd.Value;
      FmVSPedIts.EdStatus.ValueVariant     := QrVSPedItsStatus.Value;
      FmVSPedIts.CkNaoFinaliza.Checked     := Geral.IntToBool(QrVSPedItsNaoFinaliza.Value);
    end;
    FmVSPedIts.ShowModal;
    FmVSPedIts.Destroy;
  end;
end;

procedure TFmVSPedCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSPedCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSPedCab, QrVSPedIts);
end;

procedure TFmVSPedCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSPedCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSPedIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSPedIts);
end;

procedure TFmVSPedCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSPedCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSPedCab.DefParams;
begin
  VAR_GOTOTABELA := 'vspedcab';
  VAR_GOTOMYSQLTABLE := QrVSPedCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,');
  VAR_SQLx.Add('IF(ve.Tipo=0, ve.RazaoSocial, ve.Nome) NOMEVENDEDOR,');
  VAR_SQLx.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,');
  VAR_SQLx.Add('IF(tr.Tipo=0, tr.RazaoSocial, tr.Nome) NOMETRANSP,');
  VAR_SQLx.Add('IF(ee.Tipo=0, ee.RazaoSocial, ee.Nome) NOMEEntregaEnti,');
  VAR_SQLx.Add('pc.Nome NO_CONDICAOPG,');
  VAR_SQLx.Add('vpc.*');
  VAR_SQLx.Add('FROM vspedcab vpc');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=vpc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cl ON cl.Codigo=vpc.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades ve ON ve.Codigo=vpc.Vendedor');
  VAR_SQLx.Add('LEFT JOIN entidades tr ON tr.Codigo=vpc.Transp');
  VAR_SQLx.Add('LEFT JOIN entidades ee ON ee.Codigo=vpc.EntregaEnti');
  VAR_SQLx.Add('LEFT JOIN pediprzcab pc ON pc.Codigo=vpc.CondicaoPg');
  VAR_SQLx.Add('WHERE vpc.Codigo > 0');
  //
  VAR_SQL1.Add('AND vpc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vpc.Nome Like :P0');
  //
end;

procedure TFmVSPedCab.ImprimeOS_3;
begin
  MyObjects.frxDefineDataSets(frxOS_3, [
  DModG.frxDsDono,
  frxDsFluxosIts,
  frxDsVSPedCab,
  frxDsVSPedIts
  ]);
  MyObjects.frxMostra(frxOS_3, 'Ordem de produ��o n� ' + Geral.FF0(QrVSPedItsControle.Value));
end;

procedure TFmVSPedCab.ItsAltera1Click(Sender: TObject);
begin
  MostraVSPedIts(stUpd);
end;

procedure TFmVSPedCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSPedCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSPedCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSPedCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle,
  Integer(TEstqMotivDel.emtdWetCurti???), Dmod.QrUpd, Dmod.MyDB, CO_MASTER?) then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSPedIts,
      QrVSPedItsControle, QrVSPedItsControle.Value);
    ReopenVSPedIts(Controle);
  end;
}
end;

procedure TFmVSPedCab.ReopenEmitCus();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitCus, Dmod.MyDB, [
  'SELECT emt.Codigo, emt.DataEmis, emt.Numero, emt.Setor, ',
  'emt.NOMESETOR, emt.NOME, cus.Controle, cus.Custo, ',
  'cus.Pecas, cus.Peso, cus.AreaM2, cus.PercTotCus ',
  'FROM emitcus cus ',
  'LEFT JOIN emit emt ON emt.Codigo=cus.Codigo ',
  'WHERE cus.VSPedIts=' + Geral.FF0(QrVSPedItsControle.Value),
  '']);
end;

procedure TFmVSPedCab.ReopenFluxosIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFluxosIts, Dmod.MyDB,[
  'SELECT ope.Nome NOMEOPERACAO, fli.* ',
  'FROM fluxosits fli ',
  'LEFT JOIN operacoes ope ON ope.Codigo=fli.Operacao ',
  'WHERE fli.Codigo=' + Geral.FF0(QrVSPedItsFluxo.Value),
  'ORDER BY fli.Ordem, fli.Controle DESC ',
  '']);
end;

procedure TFmVSPedCab.ReopenVSPedIts(Controle: Integer);
var
  ATT_Status: String;
begin
  ATT_Status := dmkPF.SQL_ELT('Status', 'NO_Status', TPosVirgula.pvPos, True,
    sStausPedVda, 0);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPedIts, Dmod.MyDB, [
  'SELECT vmi.*,',
  ATT_Status,
  'flu.Nome NOMEFLUXO, ',
  'flu.Nome NO_Fluxo, fo1.Nome NO_ReceiRecu, ',
  'fo2.Nome NO_ReceiRefu, ti1.Nome NO_ReceiAcab,  ',
  'CONCAT(gg1.Nome, ',
  '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR,',
  'CASE vmi.PrecoTipo',
  '  WHEN 1 THEN "Pe�as"',
  '  WHEN 2 THEN "Peso kg"',
  '  WHEN 3 THEN "�rea m2"',
  '  WHEN 4 THEN "�rea ft2"',
  '  ELSE "? ? ?" ',
  'END TipoSaldo_TXT, ',
  'IF(Status <> ' + Geral.FF0(Integer(TStausPedVda.spvLiberado)) + ', 0.000, ',
  '  CASE vmi.PrecoTipo',
  '    WHEN 1 THEN vmi.Pecas-vmi.VdaPecas',
  '    WHEN 2 THEN vmi.PesoKg-vmi.VdaPesoKg',
  '    WHEN 3 THEN vmi.AreaM2-vmi.VdaAreaM2',
  '    WHEN 4 THEN vmi.AreaP2-vmi.AreaP2',
  '    ELSE 0.00 ',
  '  END',
  ') Saldo, cmd.Sigla, cmd.Simbolo  ',
  'FROM vspedits vmi ',
  'LEFT JOIN fluxos     flu ON flu.Codigo=vmi.Fluxo ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN formulas   fo1 ON fo1.Numero=vmi.ReceiRecu ',
  'LEFT JOIN formulas   fo2 ON fo2.Numero=vmi.ReceiRefu ',
  'LEFT JOIN tintascab  ti1 ON ti1.Numero=vmi.ReceiAcab ',
  'LEFT JOIN cambiomda  cmd ON cmd.Codigo=vmi.PrecoMoeda ',
  'WHERE vmi.Codigo=' + Geral.FF0(QrVSPedCabCodigo.Value),
  '']);
  //
  if Controle > 0 then
    QrVSPedIts.Locate('Controle', Controle, []);
end;


procedure TFmVSPedCab.ReopenVSMovIts();
begin
(*  Ver como fazer!!!
  UnDmkDAC_PF.AbreMySQLQuery0(QrWBMovIts, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet ',
  FROM  v s m o v i t s vmi ,
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspallet   wbp ON wbp.Codigo=vmi.Pallet ',
  'WHERE vmi.LnkIDXtr=' + Geral.FF0(Integer(lixPedVenda)),
  'AND vmi.LnkNivXtr2=' + Geral.FF0(QrVSPedItsControle.Value),
  '']);
*)
end;

procedure TFmVSPedCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSPedCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSPedCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSPedCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSPedCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSPedCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPedCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSPedCabCodigo.Value;
  Close;
end;

procedure TFmVSPedCab.ItsInclui1Click(Sender: TObject);
begin
  MostraVSPedIts(stIns);
end;

procedure TFmVSPedCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSPedCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vspedcab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSPedCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
  //
end;

procedure TFmVSPedCab.BtConfirmaClick(Sender: TObject);
var
  DataF, Obz, PedidCli, Nome: String;
  Codigo, Empresa, Cliente, Vendedor, Transp, CondicaoPg, EntregaEnti: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Cliente        := EdCliente.ValueVariant;
  Vendedor       := EdVendedor.ValueVariant;
  DataF          := Geral.FDT(TPDataF.Date, 1);
  Obz            := MeObz.Text;
  Transp         := EdTransp.ValueVariant;
  CondicaoPg     := EdCondicaoPG.ValueVariant;
  PedidCli       := EdPedidCli.ValueVariant;
  Nome           := EdNome.Text;
  EntregaEnti    := EdEntregaEnti.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma Empresa.') then
    Exit;
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Defina um cliente.') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('vspedcab', 'Codigo', '', '', tsPos,
    ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vspedcab', False, [
  'Empresa', 'Cliente', 'Vendedor',
  'DataF', 'Obz', 'Transp',
  'CondicaoPg', 'PedidCli', 'Nome',
  'EntregaEnti'], [
  'Codigo'], [
  Empresa, Cliente, Vendedor,
  DataF, Obz, Transp,
  CondicaoPg, PedidCli, Nome,
  EntregaEnti], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    {{{
    if FDireto then
    begin
      if ImgTipo.SQLType = stIns then
        Incluinovamercadoria1Click(Self)
      else
      begin
        BtTravaClick(Self);
        BtSaidaClick(Self);
      end;
    end;
    }
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSPedCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vspedcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vspedcab', 'Codigo');
end;

procedure TFmVSPedCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSPedCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSPedCab.FormCreate(Sender: TObject);
const
  Aplicacao = '1';
begin
  ImgTipo.SQLType   := stLok;
  GBEdita.Align     := alClient;
  DBGVsPedIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  UnDmkDAC_PF.AbreQuery(QrEntregaEnti, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMPs, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreQuery(QrTransp, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, MaxDesco, ',
  'JurosMes, Parcelas, Percent1, Percent2 ',
  'FROM pediprzcab ',
  'WHERE Aplicacao & ' + Aplicacao + ' <> 0 ',
  'ORDER BY Nome ',
  '']);
  //
  TPDataF.Date := Date;
end;

procedure TFmVSPedCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSPedCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSPedCab.SbImprimeClick(Sender: TObject);
begin
  ImprimeOS_3();
end;

procedure TFmVSPedCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSPedCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSPedCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSPedCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSPedCab.QrGraGruYAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSPedCab.QrGraGruYAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPedIts(0);
end;

procedure TFmVSPedCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSPedCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSPedCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSPedCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vspedcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSPedCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPedCab.frxOS_2GetValue(const VarName: string; var Value: Variant);
begin
  if VarName = 'VAR_OSImpInfWB' then
    Value := Dmod.QrControleOSImpInfWB.Value = 1;
end;

procedure TFmVSPedCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSPedCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vspedcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  TPDataF.Date := DmodG.ObtemAgora();
end;

procedure TFmVSPedCab.QrGraGruYBeforeClose(
  DataSet: TDataSet);
begin
  QrVSPedIts.Close;
end;

procedure TFmVSPedCab.QrGraGruYBeforeOpen(DataSet: TDataSet);
begin
  QrVSPedCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSPedCab.QrVSPedCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPedIts(0);
end;

procedure TFmVSPedCab.QrVSPedCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSPedCabCodigo.DisplayFormat := FFormatFloat;
  case QrVSPedItsPrecoTipo.Value of
    1: QrVSPedItsSaldo.DisplayFormat := '#,###,###,##0.0;-#,###,###,##0.0; ';
    2: QrVSPedItsSaldo.DisplayFormat := '#,###,###,##0.000;-#,###,###,##0.000; ';
    3: QrVSPedItsSaldo.DisplayFormat := '#,###,###,##0.00;-#,###,###,##0.00; ';
    4: QrVSPedItsSaldo.DisplayFormat := '#,###,###,##0.00;-#,###,###,##0.00; ';
    else QrVSPedItsSaldo.DisplayFormat := '#,###,###,##0.000;-#,###,###,##0.000; ';
  end;
end;

procedure TFmVSPedCab.QrVSPedItsAfterScroll(DataSet: TDataSet);
begin
  ReopenFluxosIts();
  ReopenVSMovIts();
  ReopenEmitCus();
end;

procedure TFmVSPedCab.QrVSPedItsBeforeClose(DataSet: TDataSet);
begin
  QrWBMovIts.Close;
  QrEmitCus.Close;
end;

procedure TFmVSPedCab.QrVSPedItsCalcFields(DataSet: TDataSet);
begin
  // ini 2023-04-29
  //QrVSPedItsSubTo.Value := QrVSPedItsValor.Value + QrVSPedItsDesco.Value;
  QrVSPedItsSubTo.Value := QrVSPedItsValor.Value * (1-(QrVSPedItsPercDesco.Value / 100));
  // ini 2023-04-29
  if QrVSPedItsPronto.Value < 2 then QrVSPedItsPRONTO_TXT.Value := ' * N�O * '
  else QrVSPedItsPRONTO_TXT.Value := Geral.FDT(QrVSPedItsPronto.Value, 2);
  //
  if QrVSPedItsTipoProd.Value = 0 then
    QrVSPedItsNOMETIPOPROD.Value := 'VENDA'
  else
    QrVSPedItsNOMETIPOPROD.Value := 'M�O-DE-OBRA';
end;

procedure TFmVSPedCab.QrTransportadorCalcFields(DataSet: TDataSet);
var
  Endereco: String;
begin
  QrTransportadorTEL_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorTelefone.Value);
  QrTransportadorFAX_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorFax.Value);
  QrTransportadorCEL_TXT.Value := Geral.FormataTelefone_TT(QrTransportadorCelular.Value);
  QrTransportadorCPF_TXT.Value := Geral.FormataCNPJ_TT(QrTransportadorCNPJ.Value);
  QrTransportadorCEP_TXT.Value := Geral.FormataCEP_TT(FloatToStr(QrTransportadorCEP.Value));
  //
  Endereco := QrTransportadorRUA.Value;
  if Trim(QrTransportadorLOGRAD.Value) <> '' then
  begin
    if Uppercase(Copy(QrTransportadorRUA.Value, 1, Length(QrTransportadorLOGRAD.Value))) <>
      Uppercase(QrTransportadorLOGRAD.Value) then
    Endereco := QrTransportadorLOGRAD.Value + ' ' + Endereco;
  end;
  //
  if QrTransportadorNUMERO.Value = 0 then
    QrTransportadorNUMEROTXT.Value := 'S/N' else
    QrTransportadorNUMEROTXT.Value :=
    FloatToStr(QrTransportadorNUMERO.Value);
  QrTransportadorENDERECO.Value :=
    Endereco + ', '+
    QrTransportadorNUMEROTXT.Value + '  '+
    QrTransportadorCOMPL.Value;
end;

procedure TFmVSPedCab.QrFornecedorCalcFields(DataSet: TDataSet);
var
  Endereco: String;
begin
  QrFornecedorTEL_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorTelefone.Value);
  QrFornecedorFAX_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorFax.Value);
  QrFornecedorCEL_TXT.Value := Geral.FormataTelefone_TT(QrFornecedorCelular.Value);
  QrFornecedorCPF_TXT.Value := Geral.FormataCNPJ_TT(QrFornecedorCNPJ.Value);
  QrFornecedorCEP_TXT.Value := Geral.FormataCEP_TT(FloatToStr(QrFornecedorCEP.Value));
  //
  Endereco := QrFornecedorRUA.Value;
  if Trim(QrFornecedorLOGRAD.Value) <> '' then
  begin
    if Uppercase(Copy(QrFornecedorRUA.Value, 1, Length(QrFornecedorLOGRAD.Value))) <>
      Uppercase(QrFornecedorLOGRAD.Value) then
    Endereco := QrFornecedorLOGRAD.Value + ' ' + Endereco;
  end;
  //
  if QrFornecedorNUMERO.Value = 0 then
    QrFornecedorNUMEROTXT.Value := 'S/N' else
    QrFornecedorNUMEROTXT.Value :=
    FloatToStr(QrFornecedorNUMERO.Value);
  QrFornecedorENDERECO.Value :=
    Endereco + ', '+
    QrFornecedorNUMEROTXT.Value + '  '+
    QrFornecedorCOMPL.Value;
end;

end.

