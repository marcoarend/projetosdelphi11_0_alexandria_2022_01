unit VSPcPosNegXValTNegPos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, UnProjGroup_Consts,
  AppListas;

type
  THackDBGrid = class(TDBGrid);
  TFmVSVSPcPosNegXValTNegPos = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DGDados: TdmkDBGridZTO;
    PnPesquisa: TPanel;
    BtTeste2: TBitBtn;
    CkTemIMEiMrt: TCheckBox;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure BtTeste2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmVSVSPcPosNegXValTNegPos: TFmVSVSPcPosNegXValTNegPos;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModVS, UnVS_PF;

{$R *.DFM}

procedure TFmVSVSPcPosNegXValTNegPos.BtOKClick(Sender: TObject);
var
  sControle, sVmiPai, Tabela: String;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
begin
  Screen.Cursor := crHourGlass;
  try
  DmModVS.QrPcPosNegXValTNegPos.First;
  PB1.Position := 0;
  PB1.Max := DmModVS.QrPcPosNegXValTNegPos.RecordCount;
  while not DmModVS.QrPcPosNegXValTNegPos.Eof do
  begin
    case DmModVS.QrPcPosNegXValTNegPosID_TTW.Value of
      0: Tabela := CO_TAB_VMI;
      1: Tabela := CO_TAB_VMB;
      else Tabela := '???';
    end;
    sControle := Geral.FF0(DmModVS.QrPcPosNegXValTNegPosControle.Value);
    if (
      (DmModVS.QrPcPosNegXValTNegPosPecas.Value > 0) and
      (
        (DmModVS.QrPcPosNegXValTNegPosPesoKg.Value > 0)
        or
        (DmModVS.QrPcPosNegXValTNegPosAreaM2.Value > 0)
      ) and
      (DmModVS.QrPcPosNegXValTNegPosValorT.Value < 0)
    ) or (
      (DmModVS.QrPcPosNegXValTNegPosPecas.Value < 0) and
      (
        (DmModVS.QrPcPosNegXValTNegPosPesoKg.Value < 0)
        or
        (DmModVS.QrPcPosNegXValTNegPosAreaM2.Value < 0)
      ) and
      (DmModVS.QrPcPosNegXValTNegPosValorT.Value > 0)
    ) then
    begin
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      ' UPDATE ' + Tabela + ' SET ' +
      ' ValorT = -ValorT ' +
      ' WHERE Controle=' + sControle);
      //
    end else ;

    DmModVS.QrPcPosNegXValTNegPos.Next;
  end;
  //DmModVS.QrPcPosNegXValTNegPos.Close;
  //DmModVS.QrPcPosNegXValTNegPos.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSVSPcPosNegXValTNegPos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSVSPcPosNegXValTNegPos.BtTeste2Click(Sender: TObject);
const
  ForcaMostrarForm = False;
  SelfCall = True;
  LaAviso1 = nil;
  LaAviso2 = nil;
  Empresa = 0;
  GraGruX = 0;
  MargemErro = 0.00;
  Avisa = False;
var
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEiMrt.Checked);
  DmModVS.VSPcPosNegXValTNegPos(Empresa, GraGruX, TemIMEIMrt, MargemErro,
    Avisa, ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Foram encontrados ' +
  Geral.FF0(DmModVS.QrPcPosNegXValTNegPos.RecordCount) +
  ' registros das Pe�as positivas/negativas X Valor total negativos/positivos!');
end;

procedure TFmVSVSPcPosNegXValTNegPos.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  if Campo = 'Controle' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrPcPosNegXValTNegPosControle.Value)
end;

procedure TFmVSVSPcPosNegXValTNegPos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSVSPcPosNegXValTNegPos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSVSPcPosNegXValTNegPos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
