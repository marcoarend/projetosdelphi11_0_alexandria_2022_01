object FmVSOutAltVMI: TFmVSOutAltVMI
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-166 :: Altera'#231#227'o de IME-I de Sa'#237'da'
  ClientHeight = 404
  ClientWidth = 558
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 558
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 510
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 462
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 342
        Height = 32
        Caption = 'Altera'#231#227'o de IME-I de Sa'#237'da'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 342
        Height = 32
        Caption = 'Altera'#231#227'o de IME-I de Sa'#237'da'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 342
        Height = 32
        Caption = 'Altera'#231#227'o de IME-I de Sa'#237'da'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 290
    Width = 558
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 554
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 334
    Width = 558
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 412
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 410
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 136
        Top = 5
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtReabreClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 185
    Width = 558
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Label15: TLabel
      Left = 8
      Top = 0
      Width = 61
      Height = 13
      Caption = 'Item da NFe:'
    end
    object LaPecas: TLabel
      Left = 76
      Top = 0
      Width = 33
      Height = 13
      Caption = 'Pe'#231'as:'
    end
    object LaAreaM2: TLabel
      Left = 152
      Top = 0
      Width = 60
      Height = 13
      Caption = #193'rea m'#178' [F3]:'
    end
    object LaAreaP2: TLabel
      Left = 228
      Top = 0
      Width = 37
      Height = 13
      Caption = #193'rea ft'#178':'
    end
    object LaPeso: TLabel
      Left = 312
      Top = 0
      Width = 48
      Height = 13
      Caption = 'Peso [F4]:'
    end
    object LaValorT: TLabel
      Left = 385
      Top = 0
      Width = 53
      Height = 13
      Caption = 'Custo total:'
      Enabled = False
    end
    object SbValorT: TSpeedButton
      Left = 460
      Top = 16
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbValorTClick
    end
    object Label18: TLabel
      Left = 484
      Top = 0
      Width = 63
      Height = 13
      Caption = 'M'#233'dia m'#178'/p'#231':'
      Enabled = False
    end
    object EdItemNFe: TdmkEdit
      Left = 8
      Top = 16
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '1'
      ValMax = '999'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
    end
    object EdPecas: TdmkEdit
      Left = 76
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPecasKeyDown
      OnRedefinido = EdPecasRedefinido
    end
    object EdAreaM2: TdmkEditCalc
      Left = 152
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaM2'
      UpdCampo = 'AreaM2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPecasKeyDown
      OnRedefinido = EdAreaM2Redefinido
      dmkEditCalcA = EdAreaP2
      CalcType = ctM2toP2
      CalcFrac = cfQuarto
    end
    object EdAreaP2: TdmkEditCalc
      Left = 228
      Top = 16
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaP2'
      UpdCampo = 'AreaP2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      dmkEditCalcA = EdAreaM2
      CalcType = ctP2toM2
      CalcFrac = cfCento
    end
    object EdPesoKg: TdmkEdit
      Left = 312
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'PesoKg'
      UpdCampo = 'PesoKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPesoKgKeyDown
    end
    object EdValorT: TdmkEdit
      Left = 387
      Top = 16
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorT'
      UpdCampo = 'ValorT'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdValorTKeyDown
    end
    object EdMediaM2Pc: TdmkEdit
      Left = 484
      Top = 16
      Width = 65
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'PesoKg'
      UpdCampo = 'PesoKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 48
    Width = 558
    Height = 80
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 558
      Height = 80
      Align = alClient
      Caption = ' Dados do cabe'#231'alho:'
      Enabled = False
      TabOrder = 0
      object Label5: TLabel
        Left = 12
        Top = 20
        Width = 53
        Height = 13
        Caption = 'ID entrada:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 96
        Top = 20
        Width = 55
        Height = 13
        Caption = 'ID estoque:'
        FocusControl = DBEdMovimCod
      end
      object Label3: TLabel
        Left = 180
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdEmpresa
      end
      object Label7: TLabel
        Left = 228
        Top = 20
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        FocusControl = DBEdDtEntrada
      end
      object Label8: TLabel
        Left = 344
        Top = 20
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdCliVenda
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdMovimCod: TdmkDBEdit
        Left = 96
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdEmpresa: TdmkDBEdit
        Left = 180
        Top = 36
        Width = 45
        Height = 21
        TabStop = False
        DataField = 'Empresa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdDtEntrada: TdmkDBEdit
        Left = 228
        Top = 36
        Width = 112
        Height = 21
        TabStop = False
        DataField = 'DtVenda'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 3
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdCliVenda: TdmkDBEdit
        Left = 344
        Top = 36
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 4
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
    end
  end
  object Panel9: TPanel
    Left = 0
    Top = 136
    Width = 558
    Height = 49
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 5
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 28
      Height = 13
      Caption = 'IME-I:'
    end
    object EdControle: TdmkEdit
      Left = 8
      Top = 23
      Width = 72
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
      OnRedefinido = EdControleRedefinido
    end
  end
  object Panel10: TPanel
    Left = 0
    Top = 225
    Width = 558
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 6
    object Label19: TLabel
      Left = 8
      Top = 0
      Width = 149
      Height = 13
      Caption = 'Material usado para emitir NF-e:'
    end
    object EdGGXRcl: TdmkEditCB
      Left = 8
      Top = 16
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'GGXRcl'
      UpdCampo = 'GGXRcl'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdGraGruXRedefinido
      DBLookupComboBox = CBGGXRcl
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGGXRcl: TdmkDBLookupComboBox
      Left = 64
      Top = 16
      Width = 457
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGGXRcl
      TabOrder = 1
      dmkEditCB = EdGGXRcl
      QryCampo = 'GGXRcl'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 265
    Width = 558
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 7
  end
  object QrGraGruX_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 488
    Top = 36
    object QrGraGruX_GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruX_Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruX_NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruX_SIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruX_CODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruX_NOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX_
    Left = 488
    Top = 88
  end
  object QrGGXRcl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 100
    Top = 100
    object QrGGXRclGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXRclControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXRclNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXRclSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXRclCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXRclNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXRcl: TDataSource
    DataSet = QrGGXRcl
    Left = 100
    Top = 152
  end
  object QrVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM')
    Left = 500
    Top = 200
    object QrVMINO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVMIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVMIMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVMIMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVMIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVMITerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVMIMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVMIDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVMIPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVMIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVMISrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVMISrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVMISrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVMISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMINO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVMISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVMIValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVMIFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVMIMisturou: TIntegerField
      FieldName = 'Misturou'
    end
    object QrVMISerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVMIMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVMICliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVMILnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVMILnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVMISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVMIObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVMIFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVMICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVMICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVMIValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVMIDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVMIDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVMIDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVMIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVMIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVMIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVMIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVMIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVMIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVMIAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVMINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVMISrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVMIDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVMIMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVMIPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrVMIPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrVMIPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrVMIMediaM2: TFloatField
      FieldName = 'MediaM2'
      DisplayFormat = '#,##0.00'
    end
    object QrVMIVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVMIStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVMINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 512
    end
    object QrVMIClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVMIGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
end
