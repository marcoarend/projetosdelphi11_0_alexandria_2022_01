object FmVSMOEnvCTeImp: TFmVSMOEnvCTeImp
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-229 :: Autoriza'#231#227'o de Fretes VS'
  ClientHeight = 449
  ClientWidth = 662
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 662
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 614
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 566
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 310
        Height = 32
        Caption = 'Autoriza'#231#227'o de Fretes VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 310
        Height = 32
        Caption = 'Autoriza'#231#227'o de Fretes VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 310
        Height = 32
        Caption = 'Autoriza'#231#227'o de Fretes VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 662
    Height = 287
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 662
      Height = 287
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 662
        Height = 287
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 658
          Height = 126
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 12
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            FocusControl = DBEdit1
          end
          object Label2: TLabel
            Left = 12
            Top = 44
            Width = 69
            Height = 13
            Caption = 'Transportador:'
            FocusControl = DBEdit3
          end
          object Label3: TLabel
            Left = 12
            Top = 84
            Width = 81
            Height = 13
            Caption = 'S'#233'rie e n'#176' do CT:'
            FocusControl = DBEdit5
          end
          object Label4: TLabel
            Left = 116
            Top = 84
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
            FocusControl = DBEdit7
          end
          object Label5: TLabel
            Left = 192
            Top = 84
            Width = 43
            Height = 13
            Caption = 'Peso Kg:'
            FocusControl = DBEdit8
          end
          object Label6: TLabel
            Left = 268
            Top = 84
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
            FocusControl = DBEdit9
          end
          object Label7: TLabel
            Left = 344
            Top = 84
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
            FocusControl = DBEdit10
          end
          object Label8: TLabel
            Left = 420
            Top = 84
            Width = 66
            Height = 13
            Caption = 'Kg transporte:'
            FocusControl = DBEdit11
          end
          object Label9: TLabel
            Left = 496
            Top = 84
            Width = 51
            Height = 13
            Caption = '$/Kg frete:'
            FocusControl = DBEdit12
          end
          object Label10: TLabel
            Left = 572
            Top = 84
            Width = 51
            Height = 13
            Caption = 'Valor frete:'
            FocusControl = DBEdit13
          end
          object DBEdit1: TDBEdit
            Left = 12
            Top = 20
            Width = 56
            Height = 21
            DataField = 'Empresa'
            DataSource = DsCTe
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 72
            Top = 20
            Width = 573
            Height = 21
            DataField = 'NO_Empresa'
            DataSource = DsCTe
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 12
            Top = 60
            Width = 56
            Height = 21
            DataField = 'Terceiro'
            DataSource = DsCTe
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 72
            Top = 60
            Width = 573
            Height = 21
            DataField = 'NO_Transp'
            DataSource = DsCTe
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Left = 12
            Top = 100
            Width = 28
            Height = 21
            DataField = 'SerCT'
            DataSource = DsCTe
            TabOrder = 4
          end
          object DBEdit6: TDBEdit
            Left = 44
            Top = 100
            Width = 66
            Height = 21
            DataField = 'nCT'
            DataSource = DsCTe
            TabOrder = 5
          end
          object DBEdit7: TDBEdit
            Left = 116
            Top = 100
            Width = 72
            Height = 21
            DataField = 'Pecas'
            DataSource = DsCTe
            TabOrder = 6
          end
          object DBEdit8: TDBEdit
            Left = 192
            Top = 100
            Width = 72
            Height = 21
            DataField = 'PesoKg'
            DataSource = DsCTe
            TabOrder = 7
          end
          object DBEdit9: TDBEdit
            Left = 268
            Top = 100
            Width = 72
            Height = 21
            DataField = 'AreaM2'
            DataSource = DsCTe
            TabOrder = 8
          end
          object DBEdit10: TDBEdit
            Left = 344
            Top = 100
            Width = 72
            Height = 21
            DataField = 'AreaP2'
            DataSource = DsCTe
            TabOrder = 9
          end
          object DBEdit11: TDBEdit
            Left = 420
            Top = 100
            Width = 72
            Height = 21
            DataField = 'PesTrKg'
            DataSource = DsCTe
            TabOrder = 10
          end
          object DBEdit12: TDBEdit
            Left = 496
            Top = 100
            Width = 72
            Height = 21
            DataField = 'CusTrKg'
            DataSource = DsCTe
            TabOrder = 11
          end
          object DBEdit13: TDBEdit
            Left = 572
            Top = 100
            Width = 72
            Height = 21
            DataField = 'ValorT'
            DataSource = DsCTe
            TabOrder = 12
          end
        end
        object DBGrid2: TDBGrid
          Left = 2
          Top = 141
          Width = 658
          Height = 144
          Align = alClient
          DataSource = DsCTeIts
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'DataCad'
              Title.Caption = 'Data Cad.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso Kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = 'Area m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TipoFrete'
              Title.Caption = 'Tipo de Frete'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesTrKg'
              Title.Caption = 'Kg transporte'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CusTrKg'
              Title.Caption = '$ / kg frete'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorT'
              Title.Caption = 'Valor total'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 335
    Width = 662
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 658
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 379
    Width = 662
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 516
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 514
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 5
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object frxWET_CURTI_229_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38009.439963981500000000
    ReportOptions.LastChange = 39098.923307291700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 76
    Top = 220
    Datasets = <
      item
        DataSet = frxDsCTe
        DataSetName = 'frxDsCTe'
      end
      item
        DataSet = frxDsCTeIts
        DataSetName = 'frxDsCTeIts'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 551.811380000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape8: TfrxShapeView
          Width = 699.213050000000000000
          Height = 249.448980000000000000
          Fill.BackColor = 14342874
        end
        object Shape1: TfrxShapeView
          Left = 11.338590000000000000
          Top = 56.692949999999990000
          Width = 260.000000000000000000
          Height = 48.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo2: TfrxMemoView
          Left = 40.220470000000000000
          Top = 57.133890000000010000
          Width = 226.897650000000000000
          Height = 39.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              '[frxDsCTe."Terceiro"][frxDsCTe."Empresa"].[frxDsCTe."SerCT"].[fr' +
              'xDsCTe."nCT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 15.338590000000000000
          Top = 56.692949999999990000
          Width = 24.000000000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 428.661410000000000000
          Top = 56.692949999999990000
          Width = 260.000000000000000000
          Height = 48.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo29: TfrxMemoView
          Left = 472.661410000000000000
          Top = 61.133890000000010000
          Width = 208.000000000000000000
          Height = 39.118120000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'R$ [frxDsCTe."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 432.661410000000000000
          Top = 60.692950000000000000
          Width = 36.000000000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Valor:')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Left = 11.338590000000000000
          Top = 108.692950000000000000
          Width = 677.322820000000000000
          Height = 28.850340000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Shape5: TfrxShapeView
          Left = 11.338590000000000000
          Top = 141.732220000000000000
          Width = 170.865800000000000000
          Height = 91.779530000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Shape6: TfrxShapeView
          Left = 359.055350000000000000
          Top = 141.732283460000000000
          Width = 329.606060000000000000
          Height = 32.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo55: TfrxMemoView
          Left = 363.275820000000000000
          Top = 151.243970000000000000
          Width = 320.944650000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Local e data')
          ParentFont = False
        end
        object Shape7: TfrxShapeView
          Left = 359.055350000000000000
          Top = 177.637795280000000000
          Width = 329.606060000000000000
          Height = 56.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo27: TfrxMemoView
          Left = 362.015770000000000000
          Top = 215.023500000000000000
          Width = 322.645640000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Respos'#225'vel: [frxDsDono."NOMEDONO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 7.559060000000000000
          Top = 22.677180000000000000
          Width = 684.094930000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'AUTORIZA'#199#195'O DE CARREGAMENTO E TRANSPORTE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 18.897650000000000000
          Top = 3.779530000000001000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 15.118120000000000000
          Top = 113.385900000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Transportador: [frxDsCTe."NO_Transp"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 22.677180000000000000
          Top = 166.299320000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Pe'#231'as:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 22.677180000000000000
          Top = 185.196970000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Peso Kg:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 22.677180000000000000
          Top = 204.094620000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            #193'rea m'#178':')
          ParentFont = False
        end
        object Shape4: TfrxShapeView
          Left = 185.196970000000000000
          Top = 141.842610000000000000
          Width = 170.865800000000000000
          Height = 91.779530000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo7: TfrxMemoView
          Left = 196.535560000000000000
          Top = 166.409710000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Peso Kg:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 196.535560000000000000
          Top = 185.307360000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'R$/Kg:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 196.535560000000000000
          Top = 204.205010000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'R$ Frete:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 15.118120000000000000
          Top = 147.401670000000000000
          Width = 159.621830000000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Dados Couros:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 192.756030000000000000
          Top = 147.401670000000000000
          Width = 159.621830000000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Dados Frete:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 241.889758900000000000
          Top = 166.299320000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."PesTrKg"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 241.889758900000000000
          Top = 185.196970000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."CusTrKg"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 241.889758900000000000
          Top = 204.094620000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."ValorT"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 68.031540000000010000
          Top = 166.299320000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."Pecas"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 68.031540000000010000
          Top = 185.196970000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."PesoKg"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 68.031540000000010000
          Top = 204.094620000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."AreaM2"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 264.567100000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
        end
        object Shape9: TfrxShapeView
          Top = 275.905690000000000000
          Width = 699.213050000000000000
          Height = 249.448980000000000000
          Fill.BackColor = 14342874
        end
        object Shape10: TfrxShapeView
          Left = 11.338590000000000000
          Top = 332.598640000000000000
          Width = 260.000000000000000000
          Height = 48.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo18: TfrxMemoView
          Left = 40.220470000000000000
          Top = 333.039580000000000000
          Width = 226.897650000000000000
          Height = 39.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              '[frxDsCTe."Terceiro"][frxDsCTe."Empresa"].[frxDsCTe."SerCT"].[fr' +
              'xDsCTe."nCT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 15.338590000000000000
          Top = 332.598640000000000000
          Width = 24.000000000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Shape11: TfrxShapeView
          Left = 428.661410000000000000
          Top = 332.598640000000000000
          Width = 260.000000000000000000
          Height = 48.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo20: TfrxMemoView
          Left = 472.661410000000000000
          Top = 337.039580000000000000
          Width = 208.000000000000000000
          Height = 39.118120000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe."ValorT"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 432.661410000000000000
          Top = 336.598640000000000000
          Width = 36.000000000000000000
          Height = 24.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Valor:')
          ParentFont = False
        end
        object Shape12: TfrxShapeView
          Left = 11.338590000000000000
          Top = 384.598640000000000000
          Width = 677.322820000000000000
          Height = 28.850340000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Shape13: TfrxShapeView
          Left = 11.338590000000000000
          Top = 417.637910000000000000
          Width = 170.865800000000000000
          Height = 91.779530000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Shape14: TfrxShapeView
          Left = 359.055350000000000000
          Top = 417.637973460000000000
          Width = 329.606060000000000000
          Height = 32.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo22: TfrxMemoView
          Left = 363.275820000000000000
          Top = 427.149660000000000000
          Width = 320.944650000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Local e data:')
          ParentFont = False
        end
        object Shape15: TfrxShapeView
          Left = 359.055350000000000000
          Top = 453.543485280000000000
          Width = 329.606060000000000000
          Height = 56.000000000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo23: TfrxMemoView
          Left = 362.015770000000000000
          Top = 490.929190000000000000
          Width = 322.645640000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Respos'#225'vel: [frxDsCTe."NO_Transp"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 7.559060000000000000
          Top = 298.582870000000000000
          Width = 684.094930000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'AUTORIZA'#199#195'O DE CARREGAMENTO E TRANSPORTE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 18.897650000000000000
          Top = 279.685220000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 15.118120000000000000
          Top = 389.291590000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Transportador: [frxDsCTe."NO_Transp"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 22.677180000000000000
          Top = 442.205010000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Pe'#231'as:')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 22.677180000000000000
          Top = 461.102660000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Peso Kg:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 22.677180000000000000
          Top = 480.000310000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            #193'rea m'#178':')
          ParentFont = False
        end
        object Shape16: TfrxShapeView
          Left = 185.196970000000000000
          Top = 417.748300000000000000
          Width = 170.865800000000000000
          Height = 91.779530000000000000
          Fill.BackColor = clWhite
          Shape = skRoundRectangle
        end
        object Memo34: TfrxMemoView
          Left = 196.535560000000000000
          Top = 442.315400000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Peso Kg:')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 196.535560000000000000
          Top = 461.213050000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'R$/Kg:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 196.535560000000000000
          Top = 480.110700000000000000
          Width = 45.354330710000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'R$ Frete:')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 15.118120000000000000
          Top = 423.307360000000000000
          Width = 159.621830000000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Dados Couros:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 192.756030000000000000
          Top = 423.307360000000000000
          Width = 159.621830000000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Dados Frete:')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 241.889758900000000000
          Top = 442.205010000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."PesTrKg"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 241.889758900000000000
          Top = 461.102660000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."CusTrKg"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 241.889758900000000000
          Top = 480.000310000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."ValorT"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 68.031540000000010000
          Top = 442.205010000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."Pecas"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 68.031540000000010000
          Top = 461.102660000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."PesoKg"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 68.031540000000010000
          Top = 480.000310000000000000
          Width = 102.047244090000000000
          Height = 18.661410000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe."AreaM2"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Top = 532.913730000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data cad.')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 68.031540000000010000
          Top = 532.913730000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as ')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 136.063080000000000000
          Top = 532.913730000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg ')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 204.094620000000000000
          Top = 532.913730000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' ')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 272.126160000000000000
          Top = 532.913730000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Area ft'#178' ')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 340.157700000000000000
          Top = 532.913730000000000000
          Width = 124.724409450000000000
          Height = 18.897650000000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo frete')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 464.882190000000000000
          Top = 532.913730000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Kg Frete ')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 532.913730000000000000
          Top = 532.913730000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'R$/Kg frete ')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 600.945270000000000000
          Top = 532.913730000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total frete ')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 631.181510000000000000
        Width = 699.213050000000000000
        DataSet = frxDsCTeIts
        DataSetName = 'frxDsCTeIts'
        RowCount = 0
        object Memo45: TfrxMemoView
          Width = 68.031496060000000000
          Height = 15.118110236220470000
          DataField = 'DataCad'
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeIts."DataCad"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo47: TfrxMemoView
          Left = 68.031540000000010000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.0;-#,###,##0.0; '#39', <frxDsCTeIts."Pecas">' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo48: TfrxMemoView
          Left = 136.063080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsCTeIts."Pes' +
              'oKg">)]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo49: TfrxMemoView
          Left = 204.094620000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsCTeIts."AreaM' +
              '2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Left = 272.126160000000000000
          Width = 68.031496060000000000
          Height = 15.118110236220470000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsCTeIts."AreaP' +
              '2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo51: TfrxMemoView
          Left = 340.157700000000000000
          Width = 124.724409450000000000
          Height = 15.118110236220470000
          DataField = 'TipoFrete'
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTeIts."TipoFrete"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo52: TfrxMemoView
          Left = 464.882190000000000000
          Width = 68.031496060000000000
          Height = 15.118110236220470000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.000;-#,###,##0.000; '#39', <frxDsCTeIts."Pes' +
              'TrKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo53: TfrxMemoView
          Left = 532.913730000000000000
          Width = 68.031496060000000000
          Height = 15.118110236220470000
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.0000;-#,###,##0.0000; '#39', <frxDsCTeIts."C' +
              'usTrKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo57: TfrxMemoView
          Left = 600.945270000000000000
          Width = 98.267716540000000000
          Height = 15.118110236220470000
          DataField = 'ValorT'
          DataSet = frxDsCTeIts
          DataSetName = 'frxDsCTeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTeIts."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
    end
  end
  object QrCTe: TmySQLQuery
   
    SQL.Strings = (
      'SELECT ecg.Empresa, ecg.Terceiro,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_Transp,'
      'ecg.SerCT, ecg.nCT,'
      'SUM(ecg.Pecas) Pecas, SUM(ecg.PesoKg) PesoKg,'
      'SUM(ecg.AreaM2) AreaM2, SUM(ecg.AreaP2) AreaP2,'
      'SUM(ecg. PesTrKg)  PesTrKg, SUM(ecg.CusTrKg) CusTrKg,'
      'SUM(ecg.ValorT) ValorT'
      'FROM _vs_mo_env_cte_ger ecg'
      'LEFT JOIN bluederm_meza.entidades tra ON tra.Codigo=ecg.Terceiro'
      'GROUP BY Empresa, Terceiro, SerCT, nCT'
      'ORDER BY ecg.DataCad DESC')
    Left = 168
    Top = 220
    object QrCTeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrCTeSerCT: TIntegerField
      FieldName = 'SerCT'
    end
    object QrCTenCT: TIntegerField
      FieldName = 'nCT'
    end
    object QrCTeNO_Transp: TWideStringField
      FieldName = 'NO_Transp'
      Size = 100
    end
    object QrCTePecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCTePesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCTeAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCTeAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCTePesTrKg: TFloatField
      FieldName = 'PesTrKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCTeCusTrKg: TFloatField
      FieldName = 'CusTrKg'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrCTeValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCTeNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
  end
  object DsCTe: TDataSource
    DataSet = QrCTe
    Left = 168
    Top = 268
  end
  object QrCTeIts: TmySQLQuery
   
    SQL.Strings = (
      'SELECT *'
      'FROM _vs_mo_env_cte_ger'
      'WHERE Empresa=-11'
      'AND Terceiro=9'
      'AND SerCT=0'
      'AND nCT=1258')
    Left = 252
    Top = 220
    object QrCTeItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeItsTipoFrete: TWideStringField
      FieldName = 'TipoFrete'
      Required = True
      Size = 10
    end
    object QrCTeItsTabela: TWideStringField
      FieldName = 'Tabela'
      Required = True
      Size = 15
    end
    object QrCTeItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrCTeItsSerCT: TIntegerField
      FieldName = 'SerCT'
    end
    object QrCTeItsnCT: TIntegerField
      FieldName = 'nCT'
    end
    object QrCTeItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrCTeItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCTeItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCTeItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCTeItsPesTrKg: TFloatField
      FieldName = 'PesTrKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrCTeItsCusTrKg: TFloatField
      FieldName = 'CusTrKg'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrCTeItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsCTeIts: TDataSource
    DataSet = QrCTeIts
    Left = 252
    Top = 268
  end
  object frxDsCTe: TfrxDBDataset
    UserName = 'frxDsCTe'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'SerCT=SerCT'
      'nCT=nCT'
      'NO_Transp=NO_Transp'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'PesTrKg=PesTrKg'
      'CusTrKg=CusTrKg'
      'ValorT=ValorT'
      'NO_Empresa=NO_Empresa')
    DataSet = QrCTe
    BCDToCurrency = False
    Left = 168
    Top = 320
  end
  object frxDsCTeIts: TfrxDBDataset
    UserName = 'frxDsCTeIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataCad=DataCad'
      'TipoFrete=TipoFrete'
      'Tabela=Tabela'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'SerCT=SerCT'
      'nCT=nCT'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'PesTrKg=PesTrKg'
      'CusTrKg=CusTrKg'
      'ValorT=ValorT')
    DataSet = QrCTeIts
    BCDToCurrency = False
    Left = 252
    Top = 320
  end
end
