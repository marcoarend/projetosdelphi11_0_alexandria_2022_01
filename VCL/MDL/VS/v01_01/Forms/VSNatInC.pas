unit VSNatInC;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkMemo, frxClass,
  frxDBSet, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO, UnAppEnums, UnGrade_Jan;

type
  TFmVSNatInC = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtArtigo: TBitBtn;
    BtMP: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVSNatInc: TMySQLQuery;
    DsVSNatInc: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrVSNatIncGraGruX: TIntegerField;
    QrVSNatIncLk: TIntegerField;
    QrVSNatIncDataCad: TDateField;
    QrVSNatIncDataAlt: TDateField;
    QrVSNatIncUserCad: TIntegerField;
    QrVSNatIncUserAlt: TIntegerField;
    QrVSNatIncAlterWeb: TSmallintField;
    QrVSNatIncAtivo: TSmallintField;
    QrVSNatIncGraGru1: TIntegerField;
    QrVSNatIncNO_PRD_TAM_COR: TWideStringField;
    frxWET_RECUR_013: TfrxReport;
    frxDsCad: TfrxDBDataset;
    QrCad: TmySQLQuery;
    QrCadGraGruX: TIntegerField;
    QrCadLk: TIntegerField;
    QrCadDataCad: TDateField;
    QrCadDataAlt: TDateField;
    QrCadUserCad: TIntegerField;
    QrCadUserAlt: TIntegerField;
    QrCadAlterWeb: TSmallintField;
    QrCadAtivo: TSmallintField;
    QrCadGraGru1: TIntegerField;
    QrCadNO_PRD_TAM_COR: TWideStringField;
    QrCadOrdem: TIntegerField;
    QrCadBRLMedM2: TFloatField;
    PMMP: TPopupMenu;
    IncluiMP1: TMenuItem;
    AlteraMP1: TMenuItem;
    ExcluiMP1: TMenuItem;
    PMArtigo: TPopupMenu;
    Incluilinkdeprodutos1: TMenuItem;
    Alteralinkdeprodutos1: TMenuItem;
    Excluilinkdeprodutos1: TMenuItem;
    EdFatorNota: TdmkEdit;
    Label5: TLabel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    GBCouNiv: TGroupBox;
    Label6: TLabel;
    Label4: TLabel;
    EdCouNiv2: TdmkEditCB;
    CBCouNiv2: TdmkDBLookupComboBox;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    QrGraGruXCou: TmySQLQuery;
    DsGraGruXCou: TDataSource;
    QrGraGruXCouNO_CouNiv1: TWideStringField;
    QrGraGruXCouNO_CouNiv2: TWideStringField;
    QrGraGruXCouGraGruX: TIntegerField;
    QrGraGruXCouCouNiv1: TIntegerField;
    QrGraGruXCouCouNiv2: TIntegerField;
    QrGraGruXCouLk: TIntegerField;
    QrGraGruXCouDataCad: TDateField;
    QrGraGruXCouDataAlt: TDateField;
    QrGraGruXCouUserCad: TIntegerField;
    QrGraGruXCouUserAlt: TIntegerField;
    QrGraGruXCouAlterWeb: TSmallintField;
    QrGraGruXCouAtivo: TSmallintField;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    EdArtigoImp: TdmkEdit;
    EdClasseImp: TdmkEdit;
    QrVSNatIncArtigoImp: TWideStringField;
    QrVSNatIncClasseImp: TWideStringField;
    QrGraGruXCouArtigoImp: TWideStringField;
    QrGraGruXCouClasseImp: TWideStringField;
    Label13: TLabel;
    EdPrevPcPal: TdmkEdit;
    QrGraGruXCouPrevPcPal: TIntegerField;
    DBEdit8: TDBEdit;
    Label14: TLabel;
    QrVSNatIncMediaMinM2: TFloatField;
    QrVSNatIncMediaMaxM2: TFloatField;
    Memo1: TMemo;
    DBEdit2: TDBEdit;
    Label15: TLabel;
    Label9: TLabel;
    EdNome: TdmkEdit;
    EdTam: TdmkEdit;
    EdCor: TdmkEdit;
    Label16: TLabel;
    Label17: TLabel;
    EdPrevAMPal: TdmkEdit;
    Label18: TLabel;
    EdPrevKgPal: TdmkEdit;
    QrGraGruXCouPrevAMPal: TFloatField;
    QrGraGruXCouPrevKgPal: TFloatField;
    DBEdit9: TDBEdit;
    Label19: TLabel;
    DBEdit10: TDBEdit;
    Label20: TLabel;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    DBGrid1: TdmkDBGridZTO;
    QrGraGruXCouGrandeza: TSmallintField;
    QrGGXPronto: TmySQLQuery;
    QrGGXProntoGraGru1: TIntegerField;
    QrGGXProntoControle: TIntegerField;
    QrGGXProntoNO_PRD_TAM_COR: TWideStringField;
    QrGGXProntoSIGLAUNIDMED: TWideStringField;
    QrGGXProntoCODUSUUNIDMED: TIntegerField;
    QrGGXProntoNOMEUNIDMED: TWideStringField;
    DsGGXPronto: TDataSource;
    Panel7: TPanel;
    Label21: TLabel;
    EdGGXPronto: TdmkEditCB;
    CBGGXPronto: TdmkDBLookupComboBox;
    QrVSNatIncGGXPronto: TIntegerField;
    Label22: TLabel;
    DBEdit11: TDBEdit;
    QrGraGruXCouGGXPronto: TIntegerField;
    QrVSNatIncNO_GGXPronto: TWideStringField;
    SbProxCad: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSNatIncAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSNatIncBeforeOpen(DataSet: TDataSet);
    procedure BtMPClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMMPPopup(Sender: TObject);
    procedure IncluiMP1Click(Sender: TObject);
    procedure AlteraMP1Click(Sender: TObject);
    procedure ExcluiMP1Click(Sender: TObject);
    procedure BtArtigoClick(Sender: TObject);
    procedure PMArtigoPopup(Sender: TObject);
    procedure QrVSNatIncBeforeClose(DataSet: TDataSet);
    procedure QrVSNatIncAfterScroll(DataSet: TDataSet);
    procedure Incluilinkdeprodutos1Click(Sender: TObject);
    procedure Alteralinkdeprodutos1Click(Sender: TObject);
    procedure Excluilinkdeprodutos1Click(Sender: TObject);
    procedure SbProxCadClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure MostraVSNatArt(SQLType: TSQLType);
    procedure ReopenVSNatArt(Codigo: Integer);

  public
    { Public declarations }
    FSeq: Integer;
    procedure LocCod(Atual, GraGruX: Integer);
  end;

var
  FmVSNatInC: TFmVSNatInC;
const
  FFormatFloat = '00000';
  FCo_Codigo = 'GraGruX';
  FCo_Nome = 'CONCAT(gg1.Nome, ' + sLineBreak +
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ' + sLineBreak +
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))';

implementation

uses UnMyObjects, Module, MeuDBUses, DMkDAC_PF, ModuleGeral, MyDBCheck,
  VSNatArt, UnVS_PF, AppListas, UnGrade_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSNatInC.LocCod(Atual, GraGruX: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, GraGruX);
end;

procedure TFmVSNatInC.MostraVSNatArt(SQLType: TSQLType);
begin
{
  if DBCheck.CriaFm(TFmVSNatArt, FmVSNatArt, afmoNegarComAviso) then
  begin
    FmVSNatArt.ImgTipo.SQLType := SQLType;
    FmVSNatArt.FQrCab := QrVSNatInC;
    FmVSNatArt.FDsCab := DsVSNatInC;
    FmVSNatArt.FQrIts := QrVSNatArt;
    //
    FmVSNatArt.LaVSNatInC.Enabled := False;
    FmVSNatArt.EdVSNatInC.Enabled := False;
    FmVSNatArt.CBVSNatInC.Enabled := False;
    //
    FmVSNatArt.LaVSRibCad.Enabled := True;
    FmVSNatArt.EdVSRibCad.Enabled := True;
    FmVSNatArt.CBVSRibCad.Enabled := True;
    //

    FmVSNatArt.EdVSNatInC.ValueVariant := QrVSNatInCGraGruX.Value;
    FmVSNatArt.CBVSNatInC.KeyValue     := QrVSNatInCGraGruX.Value;
    //
    if SQLType = stIns then
    begin
      FmVSNatArt.FCodigo := 0;
    end else
    begin
      FmVSNatArt.FCodigo := QrVSNatArtCodigo.Value;
      //
      FmVSNatArt.EdVSRibCad.ValueVariant := QrVSNatArtVSRibCad.Value;
      FmVSNatArt.CBVSRibCad.KeyValue     := QrVSNatArtVSRibCad.Value;
      //
    end;
    FmVSNatArt.ShowModal;
    FmVSNatArt.Destroy;
  end;
}
end;

procedure TFmVSNatInC.PMArtigoPopup(Sender: TObject);
begin
{
  MyObjects.HabilitaMenuItemItsIns(Incluilinkdeprodutos1, QrVSNatInC);
  MyObjects.HabilitaMenuItemItsUpd(Alteralinkdeprodutos1, QrVSNatArt);
  MyObjects.HabilitaMenuItemItsDel(Alteralinkdeprodutos1, QrVSNatArt);
}
end;

procedure TFmVSNatInC.PMMPPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(AlteraMP1, QrVSNatInC);
  MyObjects.HabilitaMenuItemCabUpd(ExcluiMP1, QrVSNatInC);
end;

procedure TFmVSNatInC.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSNatInCGraGruX.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSNatInC.DefParams;
begin
  VAR_GOTOTABELA := 'vsnatinc';
  VAR_GOTOMYSQLTABLE := QrVSNatInC;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := FCo_Codigo;
  VAR_GOTONOME := FCo_Nome;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wmp.*, cou.*, ');
  VAR_SQLx.Add('ggx.GraGru1, CONCAT(gg1.Nome,');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  VAR_SQLx.Add('NO_PRD_TAM_COR, CONCAT(gg12.Nome,');
  VAR_SQLx.Add('IF(gti2.PrintTam=0, "", CONCAT(" ", gti2.Nome)),');
  VAR_SQLx.Add('IF(gcc2.PrintCor=0,"", CONCAT(" ", gcc2.Nome)))');
  VAR_SQLx.Add('NO_GGXPronto');
  VAR_SQLx.Add('FROM vsnatinc wmp');
  VAR_SQLx.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=wmp.GraGruX');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle');
  VAR_SQLx.Add('LEFT JOIN gragrux    ggx2 ON ggx2.Controle=cou.GGXPronto');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc2 ON ggc2.Controle=ggx2.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc2 ON gcc2.Codigo=ggc2.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti2 ON gti2.Controle=ggx2.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg12 ON gg12.Nivel1=ggx2.GraGru1');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('WHERE wmp.GraGruX > 0');
  //
  VAR_SQL1.Add('AND wmp.GraGruX=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE ' + FCo_Nome + ' LIKE :P0 ');
  //
end;

procedure TFmVSNatInC.Excluilinkdeprodutos1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{
  if VS_PF.ExcluiVSNaoVMI(
  'Confirma a retirada do link de artigo selecionado?',
  'VSNatArt', 'Codigo', QrVSNatArtCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSNatArt,
      QrVSNatArtCodigo, QrVSNatArtCodigo.Value);
    ReopenVSNatArt(Codigo);
  end;
}
end;

procedure TFmVSNatInC.ExcluiMP1Click(Sender: TObject);
const
  GraGruY = 0;
var
  Controle: Integer;
begin
  Controle := QrVSNatInCGraGruX.Value;
  if VS_PF.ExcluiVSNaoVMI('Confirma a remo��o do reduzido selecionado?',
  'vsnatinc', 'GraGruX', QrVSNatInCGraGruX.Value, Dmod.MyDB) = ID_YES then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
    'GraGruY'], ['Controle'], [
    GraGruY], [Controle], True);
    //
    LocCod(Controle, Controle);
  end;
end;

procedure TFmVSNatInC.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSNatInC.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSNatInC.ReopenVSNatArt(Codigo: Integer);
begin
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSNatArt, Dmod.MyDB, [
  'SELECT wmp.GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vna.* ',
  'FROM vsnatart vna ',
  'LEFT JOIN vsribcad wmp ON wmp.GraGruX=vna.VSRibCad ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vna.VSNatInC=' + Geral.FF0(QrVSNatInCGraGruX.Value),
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
}
end;

procedure TFmVSNatInC.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSNatInC.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSNatInC.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSNatInC.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSNatInC.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSNatInC.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSNatInC.Alteralinkdeprodutos1Click(Sender: TObject);
begin
  MostraVSNatArt(stUpd);
end;

procedure TFmVSNatInC.AlteraMP1Click(Sender: TObject);
begin
{
  EdFatorNota.ValueVariant := QrVSNatInCFatorNota.Value;
}
  Grade_PF.DefineNomeTamCorGraGruX(EdNome, EdTam, EdCor, QrVSNatInCGraGruX.Value);
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGruXCou, [PnDados],
  [PnEdita], EdFatorNota, ImgTipo, 'gragruxcou');
end;

procedure TFmVSNatInC.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSNatInCGraGruX.Value;
  Close;
end;

procedure TFmVSNatInC.BtConfirmaClick(Sender: TObject);
const
  MediaMinM2 = 0;
  MediaMaxM2 = 0;
var
  GraGruX, CouNiv1, CouNiv2, PrevPcPal, GraGru1, GGXPronto: Integer;
  //FatorNota,
  PrevAMPal, PrevKgPal: Double;
  ArtigoImp, ClasseImp, Nome: String;
begin
  GraGruX        := QrVSNatInCGraGruX.Value;
  GraGru1        := QrVSNatInCGraGru1.Value;
  //Ordem          := EdOrdem.ValueVariant;
  //FatorNota      := EdFatorNota.ValueVariant;
  CouNiv1        := EdCouNiv1.ValueVariant;
  CouNiv2        := EdCouNiv2.ValueVariant;
  ArtigoImp      := EdArtigoImp.Text;
  ClasseImp      := EdClasseImp.Text;
  PrevPcPal      := EdPrevPcPal.ValueVariant;
  PrevAMPal      := EdPrevAMPal.ValueVariant;
  PrevKgPal      := EdPrevKgPal.ValueVariant;
  Nome           := EdNome.Text;
  GGXPronto      := EdGGXPronto.ValueVariant;
  //
  //if MyObjects.FIC(FatorNota <= 0, EdFatorNota, 'Informe o "Fator Nota MPAG"!') then
    //Exit;
  if MyObjects.FIC(CouNiv2 = 0, EdCouNiv2, 'Informe o "Tipo do material"!') then
    Exit;
  if MyObjects.FIC(CouNiv1 = 0, EdCouNiv1, 'Informe a "Parte do material"!') then
    Exit;
  if MyObjects.FIC(GGXPronto = 0, EdGGXPronto, 'Informe o "Artigo que ir� gerar"!') then
    Exit;
  //
(*
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsnatinc', False, [
  'FatorNota'], [
  'GraGruX'], [
  FatorNota], [
  GraGruX], True) then
*)
  begin
    VS_PF.ReIncluiCouNivs(GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1,
    (*Bastidao*) Integer(TVSBastidao.vsbstdIntegral), PrevAMPal, PrevKgPal,
    ArtigoImp, ClasseImp, MediaMinM2, MediaMaxM2, 0, 0, GGXPronto);
    //
    Grade_PF.GraGru1_AlteraNome(GraGru1, Nome);
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    if FSeq = 0 then
    begin
      LocCod(GraGruX, GraGruX);
    end else
      Close;
  end;
end;

procedure TFmVSNatInC.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVSNatInC.BtArtigoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArtigo, BtArtigo);
end;

procedure TFmVSNatInC.BtMPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMP, BtMP);
end;

procedure TFmVSNatInC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSeq := 0;
  //GBEdita.Align := alClient;
  DBGrid1.Align := alClient;
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  VS_PF.AbreGraGruXY(QrGGXPronto,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1088_VSNatCon));
  CriaOForm;
end;

procedure TFmVSNatInC.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSNatInCGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmVSNatInC.SbProxCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruY1(CO_GraGruY_1088_VSNatCon, 0, 'Couro Conservado');
  VS_PF.AbreGraGruXY(QrGGXPronto,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1088_VSNatCon));
  if QrGGXPronto.RecordCount = 1 then
  begin
    EdGGXPronto.ValueVariant := QrGGXProntoControle.Value;
    CBGGXPronto.KeyValue     := QrGGXProntoControle.Value;
  end;
end;

procedure TFmVSNatInC.SbImprimeClick(Sender: TObject);
var
  Ordem: Integer;
  Ord_Txt: String;
begin
  Ordem := MyObjects.SelRadioGroup('Ordem de Impress�o',
  'Selecione a ordem de Impress�o', [
  'Ordem',
  'Nome',
  'Pre�o'], 2);
  case Ordem of
    0: Ord_Txt := 'Ordem';
    1: Ord_Txt := 'NO_PRD_TAM_COR';
    2: Ord_Txt := 'BRLMedM2';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCad, Dmod.MyDB, [
  'SELECT wmp.*, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vsnatinc wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY ' + Ord_Txt,
  '']);
  MyObjects.frxDefineDataSets(frxWET_RECUR_013, [
  DModG.frxDsDono,
  frxDsCad]);
  MyObjects.frxMostra(frxWET_RECUR_013,
    'Configura��o de Mat�ria-prima para Semi / Acabado');
end;

procedure TFmVSNatInC.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSNatInC.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSNatInCGraGruX.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSNatInC.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSNatInC.QrVSNatIncAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSNatInC.QrVSNatIncAfterScroll(DataSet: TDataSet);
begin
  ReopenVSNatArt(0);
  VS_PF.ReopenGraGruXCou(QrGraGruXCou, QrVSNatInCGraGruX.Value);
end;

procedure TFmVSNatInC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSNatInC.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSNatInCGraGruX.Value,
  CuringaLoc.CriaForm(FCo_Codigo, FCo_Nome, 'vsnatinc', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSNatInC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSNatInC.Incluilinkdeprodutos1Click(Sender: TObject);
begin
  MostraVSNatArt(stIns);
end;

procedure TFmVSNatInC.IncluiMP1Click(Sender: TObject);
var
  DefinedSQL: String;
  Qry: TmySQLQuery;
  GraGruX: Integer;
begin
  GraGruX := Grade_Jan.ObtemGraGruXSemY(CO_GraGruY_1072_VSNatInC);
  if GraGruX <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM vsnatinc ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if Qry.FieldByName('GraGruX').AsInteger = GraGruX then
      begin
        Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
        ' j� estava adicionado � configura��o de mat�ria-prima!');
      end else
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsnatinc', False, [
        ], [
        'GraGruX'], [
        ], [
        GraGruX], True);
      end;
      //
      LocCod(GraGruX, GraGruX);
      AlteraMP1Click(Self);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmVSNatInC.QrVSNatIncBeforeClose(DataSet: TDataSet);
begin
  //QrVSNatArt.Close;
  QrGraGruXCou.Close;
end;

procedure TFmVSNatInC.QrVSNatIncBeforeOpen(DataSet: TDataSet);
begin
  QrVSNatInCGraGruX.DisplayFormat := FFormatFloat;
end;

end.

