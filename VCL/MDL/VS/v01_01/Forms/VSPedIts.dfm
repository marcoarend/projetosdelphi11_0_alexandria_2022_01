object FmVSPedIts: TFmVSPedIts
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-092 :: Item de Pedido de Venda'
  ClientHeight = 667
  ClientWidth = 849
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 533
    Width = 849
    Height = 20
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitLeft = 168
    ExplicitTop = 513
    ExplicitHeight = 81
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 849
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object Label2: TLabel
      Left = 608
      Top = 20
      Width = 28
      Height = 13
      Caption = 'IME-I:'
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 533
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object EdControle: TdmkEdit
      Left = 608
      Top = 36
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 849
    Height = 421
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object PainelIts: TPanel
      Left = 2
      Top = 15
      Width = 845
      Height = 404
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      ExplicitHeight = 388
      object GroupBox3: TGroupBox
        Left = 0
        Top = 341
        Width = 845
        Height = 63
        Align = alBottom
        TabOrder = 1
        ExplicitTop = 325
        object BtConfirma2: TBitBtn
          Tag = 14
          Left = 12
          Top = 17
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object Panel3: TPanel
          Left = 704
          Top = 15
          Width = 139
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste2: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
          end
        end
      end
      object GBItem: TGroupBox
        Left = 0
        Top = 0
        Width = 845
        Height = 401
        Align = alTop
        TabOrder = 0
        object PnItem_: TPanel
          Left = 2
          Top = 15
          Width = 841
          Height = 384
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 364
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 841
            Height = 384
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitHeight = 364
            object GroupBox4: TGroupBox
              Left = 0
              Top = 0
              Width = 841
              Height = 117
              Align = alTop
              Caption = ' Ordem de produ'#231#227'o: '
              TabOrder = 0
              object Label32: TLabel
                Left = 528
                Top = 56
                Width = 40
                Height = 13
                Caption = 'Entrega:'
              end
              object LaPrecoVal: TLabel
                Left = 244
                Top = 56
                Width = 50
                Height = 13
                Caption = 'Pre'#231'o N/I:'
              end
              object LaPecas: TLabel
                Left = 12
                Top = 16
                Width = 33
                Height = 13
                Caption = 'Pe'#231'as:'
              end
              object LaAreaM2: TLabel
                Left = 104
                Top = 16
                Width = 39
                Height = 13
                Caption = #193'rea m'#178':'
              end
              object LaAreaP2: TLabel
                Left = 200
                Top = 16
                Width = 37
                Height = 13
                Caption = #193'rea ft'#178':'
              end
              object LaPeso: TLabel
                Left = 304
                Top = 16
                Width = 27
                Height = 13
                Caption = 'Peso:'
              end
              object Label22: TLabel
                Left = 12
                Top = 57
                Width = 36
                Height = 13
                Caption = 'Moeda:'
              end
              object SpeedButton9: TSpeedButton
                Left = 218
                Top = 72
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton9Click
              end
              object Label1: TLabel
                Left = 348
                Top = 56
                Width = 82
                Height = 13
                Caption = '% Desconto item:'
              end
              object Label6: TLabel
                Left = 628
                Top = 56
                Width = 54
                Height = 13
                Caption = 'Status: [F4]'
              end
              object Label7: TLabel
                Left = 436
                Top = 56
                Width = 52
                Height = 13
                Caption = '% Tributos:'
              end
              object TPEntrega: TdmkEditDateTimePicker
                Left = 528
                Top = 72
                Width = 97
                Height = 21
                Date = 39013.000000000000000000
                Time = 0.576212557898543300
                TabOrder = 10
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object EdPrecoVal: TdmkEdit
                Left = 244
                Top = 72
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 7
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryCampo = 'PrecoVal'
                UpdCampo = 'PrecoVal'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdPecas: TdmkEdit
                Left = 12
                Top = 32
                Width = 88
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 3
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000'
                QryCampo = 'Pecas'
                UpdCampo = 'Pecas'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdAreaM2: TdmkEditCalc
                Left = 104
                Top = 32
                Width = 92
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'AreaM2'
                UpdCampo = 'AreaM2'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                dmkEditCalcA = EdAreaP2
                CalcType = ctM2toP2
                CalcFrac = cfQuarto
              end
              object EdAreaP2: TdmkEditCalc
                Left = 200
                Top = 32
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'AreaP2'
                UpdCampo = 'AreaP2'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                dmkEditCalcA = EdAreaM2
                CalcType = ctP2toM2
                CalcFrac = cfCento
              end
              object EdPesoKg: TdmkEdit
                Left = 304
                Top = 32
                Width = 92
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 3
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000'
                QryCampo = 'PesoKg'
                UpdCampo = 'PesoKg'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object RGPrecoTipo: TdmkRadioGroup
                Left = 400
                Top = 8
                Width = 357
                Height = 44
                Caption = ' Aplica'#231#227'o do pre'#231'o: '
                Columns = 5
                ItemIndex = 0
                Items.Strings = (
                  'N/I'
                  'Pe'#231'as'
                  'Peso kg'
                  #193'rea m'#178
                  #193'rea ft'#178)
                TabOrder = 4
                OnClick = RGPrecoTipoClick
                QryCampo = 'PrecoTipo'
                UpdCampo = 'PrecoTipo'
                UpdType = utYes
                OldValor = 0
              end
              object EdPrecoMoeda: TdmkEditCB
                Left = 12
                Top = 72
                Width = 44
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBPrecoMoeda
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBPrecoMoeda: TdmkDBLookupComboBox
                Left = 58
                Top = 72
                Width = 157
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                ListSource = DsCambioMda
                TabOrder = 6
                dmkEditCB = EdPrecoMoeda
                UpdType = utNil
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdPercDesco: TdmkEdit
                Left = 348
                Top = 72
                Width = 85
                Height = 21
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '99'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                QryCampo = 'PercDesco'
                UpdCampo = 'PercDesco'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdStatus: TdmkEdit
                Left = 628
                Top = 72
                Width = 21
                Height = 21
                Alignment = taRightJustify
                TabOrder = 11
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '9'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Status'
                UpdCampo = 'Status'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdStatusChange
                OnKeyDown = EdStatusKeyDown
              end
              object EdStatus_TXT: TdmkEdit
                Left = 652
                Top = 72
                Width = 129
                Height = 21
                TabStop = False
                ReadOnly = True
                TabOrder = 12
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdPercTrib: TdmkEdit
                Left = 436
                Top = 72
                Width = 85
                Height = 21
                Alignment = taRightJustify
                TabOrder = 9
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '99'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                QryCampo = 'PercTrib'
                UpdCampo = 'PercTrib'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object CkNaoFinaliza: TdmkCheckBox
                Left = 12
                Top = 96
                Width = 261
                Height = 17
                Caption = 'Manter pedido sempre aberto (nunca finalizar).'
                TabOrder = 13
                QryCampo = 'NaoFinaliza'
                UpdCampo = 'NaoFinaliza'
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
            end
            object GroupBox5: TGroupBox
              Left = 0
              Top = 253
              Width = 841
              Height = 131
              Align = alClient
              Caption = ' Outras informa'#231#245'es:'
              TabOrder = 2
              ExplicitLeft = 128
              ExplicitTop = 249
              ExplicitHeight = 135
              object Label4: TLabel
                Left = 8
                Top = 16
                Width = 264
                Height = 13
                Caption = 'Observa'#231#245'es que ser'#227'o impressas na ordem de servi'#231'o:'
              end
              object MeObserv: TdmkMemo
                Left = 8
                Top = 32
                Width = 501
                Height = 89
                TabOrder = 0
                UpdType = utYes
              end
              object RGTipoProd: TRadioGroup
                Left = 512
                Top = 28
                Width = 105
                Height = 93
                Caption = ' Tipo: '
                ItemIndex = 0
                Items.Strings = (
                  'Venda'
                  'M'#227'o-de-obra')
                TabOrder = 1
              end
            end
            object GroupBox6: TGroupBox
              Left = 0
              Top = 117
              Width = 841
              Height = 136
              Align = alTop
              Caption = ' Artigo:'
              TabOrder = 1
              ExplicitTop = 101
              object Label11: TLabel
                Left = 8
                Top = 12
                Width = 30
                Height = 13
                Caption = 'Artigo:'
              end
              object Label12: TLabel
                Left = 8
                Top = 52
                Width = 116
                Height = 13
                Caption = 'Receita de recurtimento:'
              end
              object Label15: TLabel
                Left = 8
                Top = 92
                Width = 117
                Height = 13
                Caption = 'Receita de acabamento:'
              end
              object Label8: TLabel
                Left = 424
                Top = 12
                Width = 91
                Height = 13
                Caption = 'Fluxo de produ'#231#227'o:'
              end
              object Label14: TLabel
                Left = 424
                Top = 52
                Width = 189
                Height = 13
                Caption = 'Receita de recurtimento - segunda fase:'
              end
              object Label13: TLabel
                Left = 424
                Top = 92
                Width = 30
                Height = 13
                Caption = 'Texto:'
              end
              object EdGraGruX: TdmkEditCB
                Left = 8
                Top = 28
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Cargo'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBGraGruX
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBGraGruX: TdmkDBLookupComboBox
                Left = 64
                Top = 28
                Width = 352
                Height = 21
                KeyField = 'Controle'
                ListField = 'NO_PRD_TAM_COR'
                ListSource = DsGraGruX
                TabOrder = 1
                dmkEditCB = EdGraGruX
                QryCampo = 'Cargo'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdReceiRecu: TdmkEditCB
                Left = 8
                Top = 68
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ReceiRecu'
                UpdCampo = 'ReceiRecu'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBReceiRecu
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBReceiRecu: TdmkDBLookupComboBox
                Left = 64
                Top = 68
                Width = 352
                Height = 21
                KeyField = 'Numero'
                ListField = 'Nome'
                ListSource = DsReceiRecu
                TabOrder = 5
                dmkEditCB = EdReceiRecu
                QryCampo = 'ReceiRecu'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdReceiAcab: TdmkEditCB
                Left = 8
                Top = 108
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ReceiAcab'
                UpdCampo = 'ReceiAcab'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBReceiAcab
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBReceiAcab: TdmkDBLookupComboBox
                Left = 64
                Top = 108
                Width = 352
                Height = 21
                KeyField = 'Numero'
                ListField = 'Nome'
                ListSource = DsReceiAcab
                TabOrder = 9
                dmkEditCB = EdReceiAcab
                QryCampo = 'ReceiAcab'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdFluxo: TdmkEditCB
                Left = 424
                Top = 28
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBFluxo
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBFluxo: TdmkDBLookupComboBox
                Left = 480
                Top = 28
                Width = 348
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsFluxos
                TabOrder = 3
                dmkEditCB = EdFluxo
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdReceiRefu: TdmkEditCB
                Left = 424
                Top = 68
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ReceiRefu'
                UpdCampo = 'ReceiRefu'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBReceiRefu
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBReceiRefu: TdmkDBLookupComboBox
                Left = 480
                Top = 68
                Width = 348
                Height = 21
                KeyField = 'Numero'
                ListField = 'Nome'
                ListSource = DsReceiRefu
                TabOrder = 7
                dmkEditCB = EdReceiRefu
                QryCampo = 'ReceiRefu'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdTexto: TdmkEdit
                Left = 424
                Top = 108
                Width = 405
                Height = 21
                MaxLength = 50
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 849
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 801
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 753
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 304
        Height = 32
        Caption = 'Item de Pedido de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 304
        Height = 32
        Caption = 'Item de Pedido de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 304
        Height = 32
        Caption = 'Item de Pedido de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 553
    Width = 849
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitTop = 541
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 845
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 597
    Width = 849
    Height = 70
    Align = alBottom
    TabOrder = 5
    ExplicitTop = 585
    object PnSaiDesis: TPanel
      Left = 703
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 701
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrReceiRecu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 84
    Top = 412
    object QrReceiRecuNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrReceiRecuNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsReceiRecu: TDataSource
    DataSet = QrReceiRecu
    Left = 84
    Top = 460
  end
  object QrReceirefu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 148
    Top = 412
    object QrReceirefuNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrReceirefuNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsReceiRefu: TDataSource
    DataSet = QrReceirefu
    Left = 148
    Top = 460
  end
  object QrFluxos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM fluxos'
      'ORDER BY Nome')
    Left = 284
    Top = 412
    object QrFluxosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFluxosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsFluxos: TDataSource
    DataSet = QrFluxos
    Left = 284
    Top = 460
  end
  object QrReceiAcab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome'
      'FROM tintascab'
      'ORDER BY Nome'
      ''
      '')
    Left = 216
    Top = 412
    object QrReceiAcabNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrReceiAcabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsReceiAcab: TDataSource
    DataSet = QrReceiAcab
    Left = 216
    Top = 460
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 24
    Top = 412
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 24
    Top = 460
  end
  object VuCambioMda: TdmkValUsu
    dmkEditCB = EdPrecoMoeda
    QryCampo = 'Moeda'
    UpdCampo = 'Moeda'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 220
    Top = 72
  end
  object QrCambioMda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM cambiomda'
      'ORDER BY Nome')
    Left = 352
    Top = 412
    object QrCambioMdaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCambioMdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCambioMdaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 5
    end
    object QrCambioMdaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsCambioMda: TDataSource
    DataSet = QrCambioMda
    Left = 352
    Top = 460
  end
end
