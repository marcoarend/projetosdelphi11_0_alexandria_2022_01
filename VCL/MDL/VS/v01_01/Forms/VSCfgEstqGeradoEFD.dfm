object FmVSCfgEstqGeradoEFD: TFmVSCfgEstqGeradoEFD
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-216 :: Confer'#234'ncia de Gera'#231#227'o x Estoque de Couros '
  ClientHeight = 735
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 558
        Height = 32
        Caption = 'Confer'#234'ncia de Gera'#231#227'o x Estoque de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 558
        Height = 32
        Caption = 'Confer'#234'ncia de Gera'#231#227'o x Estoque de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 558
        Height = 32
        Caption = 'Confer'#234'ncia de Gera'#231#227'o x Estoque de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 562
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 562
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 562
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 352
          Width = 1004
          Height = 5
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = 10
          ExplicitTop = 253
          ExplicitWidth = 808
        end
        object DGPsq: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 337
          Align = alClient
          DataSource = DsPsq
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'IMEIInn'
              Title.Caption = 'IME-I Gerado'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data Hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Reduzido'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Nome / tamanho / cor'
              Width = 400
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimID'
              Title.Caption = 'ID Mov.'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimID'
              Title.Caption = 'Movimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimNiv'
              Title.Caption = 'ID N'#237'vel'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimNiv'
              Title.Caption = 'N'#237'vel movimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKgInn'
              Title.Caption = 'Kg gerado'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PecasInn'
              Title.Caption = 'P'#231' gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2Ger'
              Title.Caption = 'm'#178' gerado'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKgGer'
              Title.Caption = 'Kg itens'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PecasGer'
              Title.Caption = 'P'#231' itens'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2Inn'
              Title.Caption = 'm'#178' itens'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoKg'
              Title.Caption = 'Saldo kg'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoPc'
              Title.Caption = 'Saldo P'#231
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoM2'
              Title.Caption = 'Saldo m'#178
              Width = 72
              Visible = True
            end>
        end
        object DGGer: TdmkDBGridZTO
          Left = 2
          Top = 357
          Width = 1004
          Height = 153
          Align = alBottom
          DataSource = DsGer
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'IMEIGer'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data Hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Nome / tamanho / cor'
              Width = 400
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimID'
              Title.Caption = 'ID Mov.'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimID'
              Title.Caption = 'Movimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimNiv'
              Title.Caption = 'ID N'#237'vel'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimNiv'
              Title.Caption = 'N'#237'vel movimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso Kg'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Width = 72
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 510
          Width = 1004
          Height = 50
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          object BtCorrige: TBitBtn
            Tag = 56
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Caption = '&Corrige'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtCorrigeClick
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 610
    Width = 1008
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 665
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 22
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtReopen: TBitBtn
        Tag = 18
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Reopen'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtReopenClick
      end
    end
  end
  object QrPsq: TmySQLQuery
   
    BeforeClose = QrPsqBeforeClose
    AfterScroll = QrPsqAfterScroll
    SQL.Strings = (
      'SELECT sdo.*, '
      
        ' ELT(sdo.MovimID + 1,"[ND]","Entrada","Sa'#237'da","Reclasse","Baixa"' +
        ',"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado"' +
        ',"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","' +
        'Pr'#233' reclasse","Compra de Classificado","Baixa extra","Saldo ante' +
        'rior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub P' +
        'roduto","Reclasse Mul.","Transfer. Local","Em proc. caleiro","Em' +
        ' proc. curtimento","Desclassifica'#231#227'o","Caleado","Couro PDA","Cou' +
        'ro DTA","Subproduto em proc.","Reprocesso / reparo","Curtido","D' +
        'ilu./Mist. insumos","Entrada sem Cobertura","Sa'#237'da sem Cobertura' +
        '") NO_MovimID,'
      
        ' ELT(sdo.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto","Origem de transf. de local","Destino de ' +
        'transf. de local","Origem caleiro em processo","Caleiro em proce' +
        'sso","Destino caleiro em processo","Baixa de caleiro em processo' +
        '","Artigo de caleiro","Origem curtimento em processo","Curtiment' +
        'o em processo","Destino curtimento em processo","Baixa de curtim' +
        'ento em processo","Artigo de curtimento","Origem PDA em opera'#231#227'o' +
        '","PDA em opera'#231#227'o","Destino PDA em opera'#231#227'o","Baixa de PDA em o' +
        'pera'#231#227'o","Artigo de PDA","Origem DTA em opera'#231#227'o","DTA opera'#231#227'o"' +
        ',"Destino DTA em opera'#231#227'o","Baixa de DTA em opera'#231#227'o","Artigo de' +
        ' DTA","Origem PSP em processo","PSP processo","Destino PSP em pr' +
        'ocesso","Baixa de PSP em processo","Artigo de PSP","Origem RRM e' +
        'm reprocesso","RRM reprocesso","Destino RRM em reprocesso","Baix' +
        'a de RRM em reprocesso","Artigo de Reprocesso / reparo","Baixa d' +
        'e insumo em mistura","Gera'#231#227'o de insumo em mistura") NO_MovimNiv' +
        ','
      'ggx.Controle Reduzido, CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR '
      'FROM _SDO_GER_ sdo '
      
        'LEFT JOIN bluederm_colorado.gragrux    ggx ON ggx.Controle=sdo.G' +
        'GXInn '
      
        'LEFT JOIN bluederm_colorado.gragruy    ggy ON ggy.Codigo=ggx.Con' +
        'trole '
      
        'LEFT JOIN bluederm_colorado.gragruc    ggc ON ggc.Controle=ggx.G' +
        'raGruC  '
      
        'LEFT JOIN bluederm_colorado.gracorcad  gcc ON gcc.Codigo=ggc.Gra' +
        'CorCad  '
      
        'LEFT JOIN bluederm_colorado.gratamits  gti ON gti.Controle=ggx.G' +
        'raTamI  '
      
        'LEFT JOIN bluederm_colorado.gragru1    gg1 ON gg1.Nivel1=ggx.Gra' +
        'Gru1  '
      'WHERE SdoPc <= 0 '
      'AND ( '
      '  SdoM2 <> 0 '
      '  OR '
      '  SdoKg <> 0 '
      ') '
      'ORDER BY DataHora ')
    Left = 328
    Top = 324
    object QrPsqSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrPsqDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrPsqGGXRcl: TIntegerField
      FieldName = 'GGXRcl'
    end
    object QrPsqJmpGGX: TIntegerField
      FieldName = 'JmpGGX'
    end
    object QrPsqRmsGGX: TIntegerField
      FieldName = 'RmsGGX'
    end
    object QrPsqGGXInn: TIntegerField
      FieldName = 'GGXInn'
    end
    object QrPsqIMEIInn: TIntegerField
      FieldName = 'IMEIInn'
    end
    object QrPsqDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPsqMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrPsqMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrPsqSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrPsqPecasInn: TFloatField
      FieldName = 'PecasInn'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrPsqAreaM2Inn: TFloatField
      FieldName = 'AreaM2Inn'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPsqPesoKgInn: TFloatField
      FieldName = 'PesoKgInn'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPsqDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrPsqPecasGer: TFloatField
      FieldName = 'PecasGer'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrPsqPesoKgGer: TFloatField
      FieldName = 'PesoKgGer'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPsqAreaM2Ger: TFloatField
      FieldName = 'AreaM2Ger'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPsqSdoPc: TFloatField
      FieldName = 'SdoPc'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrPsqSdoM2: TFloatField
      FieldName = 'SdoM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPsqSdoKg: TFloatField
      FieldName = 'SdoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPsqNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrPsqNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object QrPsqReduzido: TIntegerField
      FieldName = 'Reduzido'
      Required = True
    end
    object QrPsqNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsPsq: TDataSource
    DataSet = QrPsq
    Left = 372
    Top = 324
  end
  object QrGer: TmySQLQuery
   
    AfterOpen = QrGerAfterOpen
    BeforeClose = QrGerBeforeClose
    SQL.Strings = (
      'SELECT '
      'CONCAT(gg1.Nome,   '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR,  '
      
        ' ELT(ger.MovimID + 1,"[ND]","Entrada","Sa'#237'da","Reclasse","Baixa"' +
        ',"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado"' +
        ',"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","' +
        'Pr'#233' reclasse","Compra de Classificado","Baixa extra","Saldo ante' +
        'rior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub P' +
        'roduto","Reclasse Mul.","Transfer. Local","Em proc. caleiro","Em' +
        ' proc. curtimento","Desclassifica'#231#227'o","Caleado","Couro PDA","Cou' +
        'ro DTA","Subproduto em proc.","Reprocesso / reparo","Curtido","D' +
        'ilu./Mist. insumos","Entrada sem Cobertura","Sa'#237'da sem Cobertura' +
        '") NO_MovimID, '
      
        ' ELT(ger.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto","Origem de transf. de local","Destino de ' +
        'transf. de local","Origem caleiro em processo","Caleiro em proce' +
        'sso","Destino caleiro em processo","Baixa de caleiro em processo' +
        '","Artigo de caleiro","Origem curtimento em processo","Curtiment' +
        'o em processo","Destino curtimento em processo","Baixa de curtim' +
        'ento em processo","Artigo de curtimento","Origem PDA em opera'#231#227'o' +
        '","PDA em opera'#231#227'o","Destino PDA em opera'#231#227'o","Baixa de PDA em o' +
        'pera'#231#227'o","Artigo de PDA","Origem DTA em opera'#231#227'o","DTA opera'#231#227'o"' +
        ',"Destino DTA em opera'#231#227'o","Baixa de DTA em opera'#231#227'o","Artigo de' +
        ' DTA","Origem PSP em processo","PSP processo","Destino PSP em pr' +
        'ocesso","Baixa de PSP em processo","Artigo de PSP","Origem RRM e' +
        'm reprocesso","RRM reprocesso","Destino RRM em reprocesso","Baix' +
        'a de RRM em reprocesso","Artigo de Reprocesso / reparo","Baixa d' +
        'e insumo em mistura","Gera'#231#227'o de insumo em mistura") NO_MovimNiv' +
        ', '
      'ger.*  '
      'FROM _lct_ger_ ger '
      
        'LEFT JOIN bluederm_colorado.gragrux    ggx ON ggx.Controle=ger.G' +
        'raGruX '
      
        'LEFT JOIN bluederm_colorado.gragruy    ggy ON ggy.Codigo=ggx.Con' +
        'trole  '
      
        'LEFT JOIN bluederm_colorado.gragruc    ggc ON ggc.Controle=ggx.G' +
        'raGruC   '
      
        'LEFT JOIN bluederm_colorado.gracorcad  gcc ON gcc.Codigo=ggc.Gra' +
        'CorCad   '
      
        'LEFT JOIN bluederm_colorado.gratamits  gti ON gti.Controle=ggx.G' +
        'raTamI   '
      
        'LEFT JOIN bluederm_colorado.gragru1    gg1 ON gg1.Nivel1=ggx.Gra' +
        'Gru1   '
      'WHERE ger.DstNivel2=83972 '
      'ORDER BY ger.IMEIGer DESC ')
    Left = 328
    Top = 372
    object QrGerNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGerNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrGerNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object QrGerIMEIGer: TIntegerField
      FieldName = 'IMEIGer'
    end
    object QrGerDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrGerDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrGerPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrGerPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrGerAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGerMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrGerMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrGerGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DsGer: TDataSource
    DataSet = QrGer
    Left = 372
    Top = 372
  end
  object PMCorrige: TPopupMenu
    OnPopup = PMCorrigePopup
    Left = 196
    Top = 484
    object CorrigeIMEIDestinodagerao1: TMenuItem
      Caption = 'Corrige IME-I &Destino da gera'#231#227'o (grade acima)'
      OnClick = CorrigeIMEIDestinodagerao1Click
    end
    object CorrigeIMEIOrigemdagerao1: TMenuItem
      Caption = 'Corrige IME-I &Origem da gera'#231#227'o (grade abaixo)'
      OnClick = CorrigeIMEIOrigemdagerao1Click
    end
    object RecalculaGeraoesaldo1: TMenuItem
      Caption = '&Recalcula Destino (Gera'#231#227'o e Saldo)'
      OnClick = RecalculaGeraoesaldo1Click
    end
  end
end
