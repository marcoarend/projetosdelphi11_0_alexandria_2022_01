unit VSGerArtAltInN;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, AppListas, dmkCheckGroup, UnProjGroup_Consts, UnAppEnums,
  BlueDermConsts;

type
  TFmVSGerArtAltInN = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrAptos: TmySQLQuery;
    DsAptos: TDataSource;
    QrAptosCodigo: TIntegerField;
    QrAptosControle: TIntegerField;
    QrAptosMovimCod: TIntegerField;
    QrAptosMovimNiv: TIntegerField;
    QrAptosMovimTwn: TIntegerField;
    QrAptosEmpresa: TIntegerField;
    QrAptosTerceiro: TIntegerField;
    QrAptosCliVenda: TIntegerField;
    QrAptosMovimID: TIntegerField;
    QrAptosLnkNivXtr1: TIntegerField;
    QrAptosLnkNivXtr2: TIntegerField;
    QrAptosDataHora: TDateTimeField;
    QrAptosPallet: TIntegerField;
    QrAptosGraGruX: TIntegerField;
    QrAptosPecas: TFloatField;
    QrAptosPesoKg: TFloatField;
    QrAptosAreaM2: TFloatField;
    QrAptosAreaP2: TFloatField;
    QrAptosValorT: TFloatField;
    QrAptosSrcMovID: TIntegerField;
    QrAptosSrcNivel1: TIntegerField;
    QrAptosSrcNivel2: TIntegerField;
    QrAptosSdoVrtPeca: TFloatField;
    QrAptosSdoVrtArM2: TFloatField;
    QrAptosObserv: TWideStringField;
    QrAptosLk: TIntegerField;
    QrAptosDataCad: TDateField;
    QrAptosDataAlt: TDateField;
    QrAptosUserCad: TIntegerField;
    QrAptosUserAlt: TIntegerField;
    QrAptosAlterWeb: TSmallintField;
    QrAptosAtivo: TSmallintField;
    QrAptosFicha: TIntegerField;
    QrAptosMisturou: TSmallintField;
    QrAptosCustoMOKg: TFloatField;
    QrAptosCustoMOTot: TFloatField;
    QrAptosNO_PRD_TAM_COR: TWideStringField;
    QrAptosSdoVrtPeso: TFloatField;
    GBAptos: TGroupBox;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    LaTerceiro: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    LaFicha: TLabel;
    EdFicha: TdmkEdit;
    DBGAptos: TDBGrid;
    BtReabre: TBitBtn;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    EdSrcNivel2: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    QrAptosSerieFch: TIntegerField;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    QrAptosNO_SerieFch: TWideStringField;
    Label2: TLabel;
    EdSrcGGX: TdmkEdit;
    QrAptosMarca: TWideStringField;
    QrNiv1: TmySQLQuery;
    QrNiv1FatorInt: TFloatField;
    GroupBox2: TGroupBox;
    Panel3: TPanel;
    Label6: TLabel;
    EdControleBxa: TdmkEdit;
    LaPecasBxa: TLabel;
    EdPecasBxa: TdmkEdit;
    LaPesoKgBxa: TLabel;
    EdPesoKgBxa: TdmkEdit;
    SbPesoKgBxa: TSpeedButton;
    LaQtdGerArM2Bxa: TLabel;
    EdQtdGerArM2Bxa: TdmkEditCalc;
    LaQtdGerArP2Bxa: TLabel;
    EdQtdGerArP2Bxa: TdmkEditCalc;
    Label9: TLabel;
    EdObservBxa: TdmkEdit;
    CGTpCalcAutoBxa: TdmkCheckGroup;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    LaQtdGerArM2Src: TLabel;
    LaQtdGerArP2Src: TLabel;
    Label12: TLabel;
    EdControleSrc: TdmkEdit;
    EdPecasSrc: TdmkEdit;
    EdPesoKgSrc: TdmkEdit;
    EdQtdGerArM2Src: TdmkEditCalc;
    EdQtdGerArP2Src: TdmkEditCalc;
    EdObservSrc: TdmkEdit;
    Label13: TLabel;
    EdMovimTwn: TdmkEdit;
    QrNiv1MediaMinM2: TFloatField;
    QrNiv1MediaMaxM2: TFloatField;
    Qr013: TmySQLQuery;
    Qr013Ficha: TIntegerField;
    Qr013SerieFch: TIntegerField;
    Qr013Marca: TWideStringField;
    Qr013Terceiro: TIntegerField;
    QrDescendentes: TmySQLQuery;
    QrDescendentesControle: TIntegerField;
    QrDescendentesMovimNiv: TIntegerField;
    QrTwns: TmySQLQuery;
    QrTwnsMovimTwn: TIntegerField;
    Qr007: TmySQLQuery;
    Qr007Controle: TIntegerField;
    Qr007MovimNiv: TIntegerField;
    Qr007MovimID: TIntegerField;
    QrVMIFixos: TmySQLQuery;
    QrVMIFixosCodigo: TIntegerField;
    QrVMIFixosControle: TIntegerField;
    QrVMIFixosMovimCod: TIntegerField;
    QrVMIFixosMovimNiv: TIntegerField;
    QrVMIFixosMovimTwn: TIntegerField;
    QrVMIFixosEmpresa: TIntegerField;
    QrVMIFixosTerceiro: TIntegerField;
    QrVMIFixosCliVenda: TIntegerField;
    QrVMIFixosMovimID: TIntegerField;
    QrVMIFixosLnkIDXtr: TIntegerField;
    QrVMIFixosLnkNivXtr1: TIntegerField;
    QrVMIFixosLnkNivXtr2: TIntegerField;
    QrVMIFixosDataHora: TDateTimeField;
    QrVMIFixosPallet: TIntegerField;
    QrVMIFixosGraGruX: TIntegerField;
    QrVMIFixosPecas: TFloatField;
    QrVMIFixosPesoKg: TFloatField;
    QrVMIFixosAreaM2: TFloatField;
    QrVMIFixosAreaP2: TFloatField;
    QrVMIFixosValorT: TFloatField;
    QrVMIFixosSrcMovID: TIntegerField;
    QrVMIFixosSrcNivel1: TIntegerField;
    QrVMIFixosSrcNivel2: TIntegerField;
    QrVMIFixosSrcGGX: TIntegerField;
    QrVMIFixosSdoVrtPeca: TFloatField;
    QrVMIFixosSdoVrtPeso: TFloatField;
    QrVMIFixosSdoVrtArM2: TFloatField;
    QrVMIFixosObserv: TWideStringField;
    QrVMIFixosSerieFch: TIntegerField;
    QrVMIFixosFicha: TIntegerField;
    QrVMIFixosMisturou: TSmallintField;
    QrVMIFixosFornecMO: TIntegerField;
    QrVMIFixosCustoMOKg: TFloatField;
    QrVMIFixosCustoMOTot: TFloatField;
    QrVMIFixosValorMP: TFloatField;
    QrVMIFixosDstMovID: TIntegerField;
    QrVMIFixosDstNivel1: TIntegerField;
    QrVMIFixosDstNivel2: TIntegerField;
    QrVMIFixosDstGGX: TIntegerField;
    QrVMIFixosQtdGerPeca: TFloatField;
    QrVMIFixosQtdGerPeso: TFloatField;
    QrVMIFixosQtdGerArM2: TFloatField;
    QrVMIFixosQtdGerArP2: TFloatField;
    QrVMIFixosQtdAntPeca: TFloatField;
    QrVMIFixosQtdAntPeso: TFloatField;
    QrVMIFixosQtdAntArM2: TFloatField;
    QrVMIFixosQtdAntArP2: TFloatField;
    QrVMIFixosAptoUso: TSmallintField;
    QrVMIFixosNotaMPAG: TFloatField;
    QrVMIFixosMarca: TWideStringField;
    QrVMIFixosTpCalcAuto: TIntegerField;
    QrVMIFixosZerado: TSmallintField;
    QrVMIFixosEmFluxo: TSmallintField;
    QrVMIFixosNotFluxo: TIntegerField;
    QrVMIFixosFatNotaVNC: TFloatField;
    QrVMIFixosFatNotaVRC: TFloatField;
    QrVMIFixosPedItsLib: TIntegerField;
    QrVMIFixosPedItsFin: TIntegerField;
    QrVMIFixosPedItsVda: TIntegerField;
    QrVMIFixosLk: TIntegerField;
    QrVMIFixosDataCad: TDateField;
    QrVMIFixosDataAlt: TDateField;
    QrVMIFixosUserCad: TIntegerField;
    QrVMIFixosUserAlt: TIntegerField;
    QrVMIFixosAlterWeb: TSmallintField;
    QrVMIFixosAtivo: TSmallintField;
    QrVMIFixosCustoMOM2: TFloatField;
    QrVMIFixosReqMovEstq: TIntegerField;
    QrVMIFixosStqCenLoc: TIntegerField;
    QrVMIFixosItemNFe: TIntegerField;
    QrVMIFixosVSMorCab: TIntegerField;
    QrVMIFixosVSMulFrnCab: TIntegerField;
    QrVMIFixosGSPSrcNiv2: TIntegerField;
    QrVMIFixosGSPSrcMovID: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure DBGAptosDblClick(Sender: TObject);
    procedure EdPecasBxaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPecasBxaChange(Sender: TObject);
    procedure EdPesoKgBxaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSerieFchChange(Sender: TObject);
    procedure SbPesoKgBxaClick(Sender: TObject);
    procedure EdPecasSrcKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FFatorIntSrc, FFatorIntDst: Integer;
    FMediaMinM2, FMediaMaxM2: Double;
    FItensPsq, FItensAtz: Integer;
    FItensAtualizados, FItensPesquisados: array of Integer;
    //
    procedure AtualizaIMEIAtual(const Controle, SerieFch, Ficha, Terceiro:
              Integer; const Marca: String; var Atualizados: Integer);
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure FechaPesquisa();
    procedure ReopenVSGerArtSrc(Controle: Integer);
    function  ValorMPParcial(): Double;
    function  ValorTParcial(): Double;
    procedure LiberaEdicao(Libera: Boolean);
    procedure AlteraDadosDescendentes_1();
    procedure AlteraDadosDescendentes_2();
    procedure AlteraDescendenteAtual(IMEI_Pai, New013SerieFch, New013Ficha,
              New013Terceiro: Integer; New013Marca: String);
    //function  IMEI_JaPesquisado(IMEI: Integer): Boolean;
    //function  IMEI_JaLocalizado(const IMEI: Integer; const Lista:
    //          array of Integer; var ItensLoc: Integer): Boolean;
    procedure AlteraDadosMovimCod(const IMEI, MovimID, MovimNiv, New013SerieFch,
              New013Ficha, New013Terceiro: Integer; const New013Marca: String;
              var Atualizados: Integer);


  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FOrigMovimNiv: TEstqMovimNiv;
    FEmpresa, FClientMO, FFornecMO, FStqCenLoc, FMovimTwn, FOrigMovimCod,
    FOrigCodigo, FNewGraGruX, FTipoArea: Integer;
    FDataHora: TDateTime;
    //
    FFator, FAntPecas, FAntPesoKg, FAntValorT: Double;
    FInnGraGruX, FControleInn, FControleNew, FAnt013SerieFch, FAnt013Ficha,
    FAnt013Terceiro: Integer;
    FAntMarca: String;
    //
    procedure ReopenItensAptos();
    procedure DefineTipoArea();
  end;

  var
  FmVSGerArtAltInN: TFmVSGerArtAltInN;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSGerArtCab, UnVS_CRC_PF, GetValor;

{$R *.DFM}

procedure TFmVSGerArtAltInN.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmVSGerArtAltInN.AlteraDadosDescendentes_1();
var
  New013SerieFch, New013Ficha, New013Terceiro: Integer;
  New013Marca, IMEIS: String;
  //
  IMEI_Pai, Controle, GraGruX, SrcGGX, Atualizados, MovimID, MovimNiv: Integer;
  //
begin
  Atualizados := 0;
  SetLength(FItensAtualizados, 0);
  IMEI_Pai := EdSrcNivel2.ValueVariant;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr013, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle=' + Geral.FF0(IMEI_Pai),
  '']);
  //
  New013SerieFch := Qr013SerieFch.Value;
  New013Ficha    := Qr013Ficha.Value;
  New013Terceiro := Qr013Terceiro.Value;
  New013Marca    := Qr013Marca.Value;
  //GraGruX        := Qr013(*QrAptos*)GraGruX.Value;
  //SrcGGX         := Qr013(*QrAptos*)GraGruX.Value;
  // Atualizar todo descendentes diretos
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrDescendentes, Dmod.MyDB, [
  'SELECT Controle, Pecas, PesoKg, AreaM2,  ',
  'SdoVrtPeca, SdoVrtArM2, SrcNivel2, ',
  'GraGruX, MovimTwn, SrcGGX,  ',
  'SerieFch, Ficha, MovimID, MovimNiv  ',
  'FROM v s m o v i t s ',
  'WHERE MovimTwn IN ',
  '( ',
  'SELECT MovimTwn ',
  'FROM v s m o v i t s ',
  'WHERE SrcNivel2=' + Geral.FF0(IMEI_Pai),
  ') ',
  'ORDER BY Controle ',
  '']);
  QrDescendentes.First;
  while not QrDescendentes.Eof do
  begin
    Controle := QrDescendentesControle.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, CO_UPD_TAB_VMI, False, [
    'SerieFch', 'Ficha', 'Terceiro',
    'Marca'], [
    'Controle'], [
    New013SerieFch, New013Ficha, New013Terceiro,
    New013Marca], [Controle], True); // then
    //
    QrDescendentes.Next;
  end;
*)
  // Abrir IME-Gs(Gemeos) (MovimID = 7) filhos do IME-I Pai (MovimID = 6)
  UnDmkDAC_PF.AbreMySQLQuery0(QrTwns, Dmod.MyDB, [
  'SELECT DISTINCT MovimTwn ',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE SrcNivel2=' + Geral.FF0(IMEI_Pai),
  'ORDER BY MovimTwn',
  '']);
  QrTwns.First;
  while not QrTwns.Eof do
  begin
    // 4487 a 4490
    // Abrir IME-Is (MovimID = 7) filhos do IME-I Pai (MovimID = 6) do IME-G do registro atual
    UnDmkDAC_PF.AbreMySQLQuery0(Qr007, Dmod.MyDB, [
    'SELECT Controle, Pecas, PesoKg, AreaM2,  ',
    'SdoVrtPeca, SdoVrtArM2, SrcNivel2, ',
    'GraGruX, MovimTwn, SrcGGX,  ',
    'SerieFch, Ficha, MovimID, MovimNiv  ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE MovimTwn=' + Geral.FF0(QrTwnsMovimTwn.Value),
    'ORDER BY MovimNiv',
    '']);
    //
    Qr007.First;
    while not Qr007.Eof do
    begin
      Controle := Qr007Controle.Value;
      MovimNiv := Qr007MovimNiv.Value;
      MovimID  := Qr007MovimID.Value;
      //
      AtualizaIMEIAtual(Controle, New013SerieFch, New013Ficha, New013Terceiro,
      New013Marca, Atualizados);
      //
      if MovimNiv = Integer(TEstqMovimNiv.eminDestClass) then
      begin
        AlteraDadosMovimCod(Controle, MovimID, MovimNiv, New013SerieFch,
          New013Ficha, New013Terceiro, New013Marca, Atualizados);
      end;
      Qr007.Next;
    end;
    QrTwns.Next;
  end;
  //
  IMEIS := VS_CRC_PF.CordaIMEIS(FItensAtualizados);
  Geral.MB_Info('Itens atualizados: ' + Geral.FF0(Atualizados) +
  sLineBreak + 'IMEIS atualizados: ' + (*sLineBreak +*) IMEIs);
{

// Parei Aqui!

// Encontrar o Gemeo de baixa de In Natura e o controle (SrcNivel2) do In Natura
// Result.Controle = gemeo e Result.SrcNivel2 = In Natura de origem

SELECT * FROM v s m o v i t s
WHERE MovimTwn=4482
AND MovimNiv=15
    // Se alterar Pecas tem que alterar recalcular o saldo do In Natura de origem
    // alterando tambem o IMEI Gemeo MovimID=15
    ... codigo...

    // Se alterar IMEI de origem tem que recalcular o In Natura de Origem
    // anterior, o IMEI novo, o IMEI do Artigo Gerado e TODOS IMEIs descendentes
    // do IMEI de Artigo Gerado!!!!!


  Para os descendentes diretos!

SELECT Codigo, Controle, MovimCod, MovimID,
MovimNiv, MovimTwn, Terceiro, SerieFch, Ficha,
Pallet, GraGruX, Pecas,
SrcMovID, SrcNivel2, DstMovID, DstNivel2
FROM v s m o v i t s
WHERE MovimTwn <> 0
AND MovimTwn IN (
     SELECT MovimTwn
     FROM v s m o v i t s
     WHERE SrcNivel2=11983
)

  Para os descendentes dos descendentes

  depois...
  SELECT *
FROM v s m o v i t s
WHERE SrcNivel2=11993

SELECT *
FROM v s m o v i t s
WHERE MovimCod <> 0
AND MovimCod IN (
  SELECT MovimCod
  FROM v s m o v i t s
  WHERE Controle=12018
)
}

end;

procedure TFmVSGerArtAltInN.AlteraDadosDescendentes_2();
var
  New013SerieFch, New013Ficha, New013Terceiro: Integer;
  New013Marca, IMEIs: String;
  //
  I, IMEI_Pai, Controle: Integer;
begin
  FItensPsq  := 0;
  FItensAtz  := 0;
  SetLength(FItensPesquisados, 0);
  SetLength(FItensAtualizados, 0);
  //
  IMEI_Pai := EdSrcNivel2.ValueVariant;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr013, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle=' + Geral.FF0(IMEI_Pai),
  '']);
  //
  New013SerieFch := Qr013SerieFch.Value;
  New013Ficha    := Qr013Ficha.Value;
  New013Terceiro := Qr013Terceiro.Value;
  New013Marca    := Qr013Marca.Value;
  //GraGruX        := Qr013(*QrAptos*)GraGruX.Value;
  //SrcGGX         := Qr013(*QrAptos*)GraGruX.Value;
  // Atualizar todos descendentes de todas geracoes
  AlteraDescendenteAtual(IMEI_Pai, New013SerieFch, New013Ficha, New013Terceiro,
    New013Marca);
  IMEIS := VS_CRC_PF.CordaIMEIS(FItensAtualizados);
  Geral.MB_Info('Itens pesquisados: ' + Geral.FF0(Length(FItensPesquisados)) +
  sLineBreak + 'Itens atualizados: ' + Geral.FF0(Length(FItensAtualizados)) +
  sLineBreak + 'Itens desviados Psq: ' + Geral.FF0(FItensPsq) +
  sLineBreak + 'Itens desviados Atz: ' + Geral.FF0(FItensAtz) +
  sLineBreak + 'IMEIS atualizados: ' + (*sLineBreak +*) IMEIs);
end;

procedure TFmVSGerArtAltInN.AlteraDadosMovimCod(const IMEI, MovimID, MovimNiv,
  New013SerieFch, New013Ficha, New013Terceiro: Integer; const
  New013Marca: String; var Atualizados: Integer);
var
  QryCab, QryIts, QryMix: TmySQLQuery;
  MovimCod, Controle, IDFilho, NivFilho: Integer;
  SerieFch, Ficha, Terceiro: Integer;
  Marca: String;
begin
  QryCab := TmySQLQuery.Create(Dmod);
  try
  QryIts := TmySQLQuery.Create(Dmod);
  try
  QryMix := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryCab, Dmod.MyDB, [
    'SELECT DISTINCT MovimCod',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    QryCab.First;
    while not QryCab.Eof do
    begin
      MovimCod := QryCab.FieldByName('MovimCod').AsInteger;
      //
      if MovimNiv = Integer(TEstqMovimNiv.eminSorcPreReclas) then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QryIts, Dmod.MyDB, [
        'SELECT  ',
        'COUNT(DISTINCT(SerieFch)) ItensSerieFch, ',
        'COUNT(DISTINCT(Ficha)) ItensFicha, ',
        'COUNT(DISTINCT(Terceiro)) ItensTerceiro, ',
        'COUNT(DISTINCT(Marca)) ItensMarca ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidPreReclasse)), //15
        'AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcPreReclas)), //11
        'AND MovimCodMovimCod=' + Geral.FF0(MovimCod),
        '']);
        //
        SerieFch  := QryIts.FieldByName('SerieFch').AsInteger;
        Ficha     := QryIts.FieldByName('Ficha').AsInteger;
        Terceiro  := QryIts.FieldByName('Terceiro').AsInteger;
        Marca     := QryIts.FieldByName('Marca').AsString;
      end else
      begin
        SerieFch  := New013SerieFch;
        Ficha     := New013Ficha;
        Terceiro  := New013Terceiro;
        Marca     := New013Marca;
      end;
      UnDmkDAC_PF.AbreMySQLQuery0(QryIts, Dmod.MyDB, [
      'SELECT *  ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      '']);
      QryIts.First;
      while not QryIts.Eof do
      begin
        Controle := QryIts.FieldByName('Controle').AsInteger;
        IDFilho  := QryIts.FieldByName('MovimID').AsInteger;
        NivFilho := QryIts.FieldByName('MovimNiv').AsInteger;
        //
        AtualizaIMEIAtual(Controle, SerieFch, Ficha, Terceiro, Marca,
          Atualizados);
        //
        if (MovimNiv = Integer(TEstqMovimNiv.eminDestClass))
        or (MovimNiv = Integer(TEstqMovimNiv.eminDestPreReclas)) then
        begin
          AlteraDadosMovimCod(Controle, IDFilho, NivFilho, SerieFch, Ficha,
            Terceiro, Marca, Atualizados);
        end;
        //
        QryIts.Next;
      end;
      //
      QryCab.Next;
    end;
  finally
    QryMix.Free;
  end;
  finally
    QryIts.Free;
  end;
  finally
    QryCab.Free;
  end;
end;

procedure TFmVSGerArtAltInN.AlteraDescendenteAtual(IMEI_Pai, New013SerieFch,
  New013Ficha, New013Terceiro: Integer; New013Marca: String);
var
  Qry: TmySQLQuery;
  Controle, I: Integer;
  JaFoi: Boolean;
  //
  procedure CorreQryAtual();
  begin
    Qry.First;
    while not Qry.Eof do
    begin
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      if not VS_CRC_PF.IMEI_JaEstaNoArray(Controle, FItensAtualizados, FItensPsq) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, CO_UPD_TAB_VMI, False, [
        'SerieFch', 'Ficha', 'Terceiro',
        'Marca'], [
        'Controle'], [
        New013SerieFch, New013Ficha, New013Terceiro,
        New013Marca], [Controle], True) then
        begin
          SetLength(FItensAtualizados, Length(FItensAtualizados) + 1);
          FItensAtualizados[Length(FItensAtualizados)-1] := Controle;
          //
        end;
      end;
      AlteraDescendenteAtual(Controle, New013SerieFch, New013Ficha,
        New013Terceiro, New013Marca);
      //
      //FItensAtualizados := FItensAtualizados + 1;
      //
      Qry.Next;
    end;
  end;
begin
  // Verificar se o IMEI j� foi atualizado
  //if IMEI_JaPesquisado(IMEI_Pai) then
  if VS_CRC_PF.IMEI_JaEstaNoArray(IMEI_Pai, FItensPesquisados, FItensPsq) then
    Exit;
  SetLength(FItensPesquisados, Length(FItensPesquisados) + 1);
  FItensPesquisados[Length(FItensPesquisados)-1] := IMEI_Pai;
  // Caso nao foi, atualizar ele e seus descendentes
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle, MovimNiv ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE SrcNivel2=' + Geral.FF0(IMEI_Pai),
    '']);
    CorreQryAtual();
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE MovimCod <> 0 ',
    'AND MovimCod IN ( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI,
    '  WHERE Controle=' + Geral.FF0(IMEI_Pai),
    ') ',
    '']);
    CorreQryAtual();
  finally
    Qry.Free;
  end;
end;

procedure TFmVSGerArtAltInN.AtualizaIMEIAtual(const Controle, SerieFch, Ficha,
  Terceiro: Integer; const Marca: String; var Atualizados: Integer);
begin
  if not VS_CRC_PF.IMEI_JaEstaNoArray(Controle, FItensAtualizados, FItensPsq) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, CO_UPD_TAB_VMI, False, [
    'SerieFch', 'Ficha', 'Terceiro',
    'Marca'], [
    'Controle'], [
    SerieFch, Ficha, Terceiro,
    Marca], [Controle], True) then
    begin
      Atualizados := Atualizados + 1;
      //
      SetLength(FItensAtualizados, Length(FItensAtualizados) + 1);
      FItensAtualizados[Length(FItensAtualizados)-1] := Controle;
    end;
  end;
end;

procedure TFmVSGerArtAltInN.BtOKClick(Sender: TObject);
var
  GSPSrcMovID: TEstqMovimID;
  Pallet, LnkNivXtr1, LnkNivXtr2, CliVenda, AptoUso, FornecMO, PedItsLib,
  PedItsFin, PedItsVda, GSPSrcNiv2, ReqMovEstq, StqCenLoc,
  ItemNFe, VSMulFrnCab: Integer;
  AreaM2, AreaP2, CustoMOKg, CustoMOM2, CustoMOTot, QtdGerPeca,
  QtdGerPeso: Double;
  //
  procedure DefineAnterioresFixos(Controle: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVMIFixos, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
    Pallet       := QrVMIFixosPallet.Value;
    AreaM2       := QrVMIFixosAreaM2.Value;
    AreaP2       := QrVMIFixosAreaP2.Value;
    LnkNivXtr1   := QrVMIFixosLnkNivXtr1.Value;
    LnkNivXtr2   := QrVMIFixosLnkNivXtr2.Value;
    CliVenda     := QrVMIFixosCliVenda.Value;
    CustoMOKg    := QrVMIFixosCustoMOKg.Value;
    CustoMOM2    := QrVMIFixosCustoMOM2.Value;
    CustoMOTot   := QrVMIFixosCustoMOTot.Value;
    QtdGerPeca   := QrVMIFixosQtdGerPeca.Value;
    QtdGerPeso   := QrVMIFixosQtdGerPeso.Value;
    AptoUso      := QrVMIFixosAptoUso.Value;
    //FornecMO     := QrVMIFixosFornecMO.Value;
    PedItsLib    := QrVMIFixosPedItsLib.Value;
    PedItsFin    := QrVMIFixosPedItsFin.Value;
    PedItsVda    := QrVMIFixosPedItsVda.Value;
    GSPSrcMovID  := TEstqMovimID(QrVMIFixosGSPSrcMovID.Value);
    GSPSrcNiv2   := QrVMIFixosGSPSrcNiv2.Value;
    ReqMovEstq   := QrVMIFixosReqMovEstq.Value;
    //StqCenLoc    := QrVMIFixosStqCenLoc.Value;
    ItemNFe      := QrVMIFixosItemNFe.Value;
    VSMulFrnCab  := QrVMIFixosVSMulFrnCab.Value;
  end;
const
  ExigeFornecedor = True;
  ExigeAreaouPeca = True;
  //
  EdPallet = nil;
  EdValorT = nil;
  EdAreaM2 = nil;
  //
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  DataHora, Observ, Marca: String;
  Codigo, Controle, MovimCod, Empresa, ClientMO, GraGruX, Fornecedor,
  SrcNivel1, SrcNivel2, DstNivel1, DstNivel2, SerieFch, Ficha, SrcGGX, DstGGX,
  MovimTwn: Integer;
  Pecas, PesoKg, ValorMP, ValorT, QtdGerArM2, QtdGerArP2,
  FatorMP, FatorAR, NotaMPAG, FatNotaVNC, FatNotaVRC: Double;
  DstMovID, SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  TpCalcAuto: Integer;
  Qry: TmySQLQuery;
begin
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControleBxa.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  MovimTwn       := FMovimTwn;
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  FornecMO       := FFornecMO;
  StqCenLoc      := FStqCenLoc;
  Fornecedor     := QrAptosTerceiro.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidIndsXX;
  MovimNiv       := eminBaixCurtiXX;
  GraGruX        := QrAptosGraGruX.Value;
  Pecas          := -EdPecasBxa.ValueVariant;
  PesoKg         := -EdPesoKgBxa.ValueVariant;
  QtdGerArM2     := -EdQtdGerArM2Src.ValueVariant;
  QtdGerArP2     := -EdQtdGerArP2Src.ValueVariant;
  // ini 2023-04-15
  //ValorMP        := -ValorTParcial();
  //ValorT         := ValorMP;
  ValorMP        := -ValorMPParcial();
  ValorT         := -ValorTParcial();
  // fim 2023-04-15
  Observ         := EdObservBxa.Text;
  //
  SerieFch       := QrAptosSerieFch.Value;
  Ficha          := QrAptosFicha.Value;
  Marca          := QrAptosMarca.Value;
  //
  (*DstMovID       := TEstqMovimID(EdSrcMovID.ValueVariant);
  DstNivel1      := EdSrcNivel1.ValueVariant;
  DstNivel2      := EdSrcNivel2.ValueVariant;
  DstGGX         := EdSrcGGX.ValueVariant;*)
  //
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  SrcMovID       := TEstqMovimID(QrAptosMovimID.Value);
  SrcNivel1      := QrAptosCodigo.Value;
  SrcNivel2      := QrAptosControle.Value;
  SrcGGX         := QrAptosGraGruX.Value;
  //
  if MyObjects.FIC(-QtdGerArM2 < 0.01, EdQtdGerArM2Src, 'Informe a  �rea!') then
    Exit;
  if not VS_CRC_PF.AreaEstaNaMedia(EdPecasSrc.ValueVariant, -QtdGerArM2, FMediaMinM2, FMediaMaxM2, True) then
      Exit;
  //
  if VS_CRC_PF.VSFic(GraGruX, Empresa, ClientMO, Fornecedor, Pallet, Ficha, Pecas,
    AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, (*EdFicha*) nil, EdPecasBxa,
    EdAreaM2, EdPesoKgBxa, EdValorT, ExigeFornecedor, CO_GraGruY_2048_VSRibCad,
    ExigeAreaouPeca, nil)
  then
    Exit;
  //
  if -Pecas > QrAptosSdoVrtPeca.Value then
  begin
    if Geral.MB_Pergunta(
    'A quantidade de pe�as a ser baixada � superior ao estoque dispon�vel!' +
    'Deseja continuar assim mesmo?') <> ID_YES then
      Exit;
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
  end;
  //
  FatorMP  := VS_CRC_PF.FatorNotaMP(GraGruX);
  FatorAR  := VS_CRC_PF.FatorNotaAR(FNewGraGruX);
  NotaMPAG := VS_CRC_PF.NotaCouroRibeiraApuca(-Pecas, -PesoKg, -QtdGerArM2, FatorMP, FatorAR);
  FatNotaVRC := FatorAR;
  FatNotaVNC := FatorMP;

  //
  TpCalcAuto := CGTpCalcAutoBxa.Value;
  // Baixa couro In Natura
  MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  // 2015-05-09 Calcular Nota MPAG
    DstMovID       := MovimID;
    DstNivel1      := EdSrcNivel1.ValueVariant;
    DstNivel2      := EdControleSrc.ValueVariant;
    DstNivel2      := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, DstNivel2);
    DstGGX         := EdSrcGGX.ValueVariant;
    // FIM 2015-05-09 Calcular Nota MPAG

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Excluir Registros Antigos para poder gerar os novos
    VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(EdControleBxa.ValueVariant,
      Integer(TEstqMotivDel.emtdWetCurti104), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
    VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(EdControleSrc.ValueVariant,
      Integer(TEstqMotivDel.emtdWetCurti104), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
    // Aumentar Saldo do In Natura selecionado
    VS_CRC_PF.AtualizaSaldoIMEI(FControleInn, True);
    // Reduzir Saldo e custo do Artigo de Ribeira gerado
    VS_CRC_PF.DistribuiCustoIndsVS(MovimNiv, MovimCod, Codigo, FControleNew);
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
  DefineAnterioresFixos(Controle);
  if VS_CRC_PF.InsUpdVSMovIts3((*ImgTipo.SQLType*) stIns, Codigo, MovimCod, MovimTwn,
  Empresa, Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg,
  AreaM2, AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
  LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg,
  CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca,
  QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG,
  SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei025(*Gera��o de artigo de ribeira (Altera��o de In Natura)*)) then
  begin
    VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
    // 2015-05-09 Calcular Nota MPAG
    // Geracao Couro Curtido
    //Controle       := EdControleSrc.ValueVariant;
    Controle       := DstNivel2;
    // FIM 2015-05-09 Calcular Nota MPAG
    //
    MovimNiv       := eminSorcCurtiXX;
    GraGruX        := EdSrcGGX.ValueVariant;
    Pecas          := EdPecasSrc.ValueVariant;
    PesoKg         := EdPesoKgBxa.ValueVariant;
    QtdGerArM2     := EdQtdGerArM2Src.ValueVariant;
    QtdGerArP2     := EdQtdGerArP2Src.ValueVariant;
    // ini 2023-04-15
    //ValorMP        := -ValorTParcial();
    //ValorT         := ValorMP;
    ValorMP        := -ValorMPParcial();
    ValorT         := -ValorTParcial();
    // fim 2023-04-15
    Observ         := EdObservSrc.Text;
    //
    DstMovID       := TEstqMovimID(EdSrcMovID.ValueVariant);
    DstNivel1      := EdSrcNivel1.ValueVariant;
    DstNivel2      := EdSrcNivel2.ValueVariant;
    DstGGX         := EdSrcGGX.ValueVariant;
    //
    SrcMovID       := TEstqMovimID(0);
    SrcNivel1      := 0;
    SrcNivel2      := 0;
    SrcGGX         := 0;
    //
    TpCalcAuto     := 0;
    //
    // 2015-05-09 Calcular Nota MPAG
    //Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
    // FIM 2015-05-09 Calcular Nota MPAG
    DefineAnterioresFixos(Controle);
    if VS_CRC_PF.InsUpdVSMovIts3((*ImgTipo.SQLType*)stIns, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
    MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
    DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
    CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID,
    DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
    AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    CO_0_VmiPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iuvpei026(*Baixa de mat�ria prima na gera��o de artigo de ribeira (Altera��o de In Natura)*)) then
    begin
      VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Atualizar dados de baixa do In Natura selecionado
      VS_CRC_PF.AtualizaSaldoIMEI(QrAptosControle.Value, True);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
      VS_CRC_PF.DistribuiCustoIndsVS(FOrigMovimNiv, FOrigMovimCod, FOrigCodigo,
        EdSrcNivel2.ValueVariant);
      //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
      //Rastrear e alterar dados dos IMEIS descendentes!!!
      //SerieFch, Ficha, GraGruX, SrcGGX?????
      //
      AlteraDadosDescendentes_1();
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

      //
      FmVSGerArtCab.LocCod(Codigo, Codigo);
      ReopenVSGerArtSrc(Controle);
      //
      Close;
    end;
  end;
end;

procedure TFmVSGerArtAltInN.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerArtAltInN.DBGAptosDblClick(Sender: TObject);
begin
  LiberaEdicao(True);
end;

procedure TFmVSGerArtAltInN.DefineTipoArea();
begin
  LaQtdGerArM2Src.Enabled := FTipoArea = 0;
  EdQtdGerArM2Src.Enabled := FTipoArea = 0;
  //
  LaQtdGerArP2Src.Enabled := FTipoArea = 1;
  EdQtdGerArP2Src.Enabled := FTipoArea = 1;
end;

procedure TFmVSGerArtAltInN.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtAltInN.EdGraGruXChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtAltInN.EdPecasBxaChange(Sender: TObject);
  procedure CalculaPesoParcial();
  var
    Pecas, PesoKg: Double;
  begin
    Pecas  := EdPecasBxa.ValueVariant;
    PesoKg := 0;
    if QrAptosSdoVrtPeca.Value > 0 then
      PesoKg := Pecas / QrAptosSdoVrtPeca.Value * QrAptosSdoVrtPeso.Value;
    //
(*
    if PesoKg > QrAptosSdoVrtPeso.Value then
      PesoKg := QrAptosSdoVrtPeso.Value;
*)
    //
    EdPesoKgBxa.ValueVariant := PesoKg;
  end;
var
  Pecas: Double;
begin
  // ver pelas pecas se eh o resto e pegar todo resto do peso
  Pecas := EdPecasBxa.ValueVariant;
  if (QrAptos.State <> dsInactive) and (QrAptos.RecordCount > 0)
  and (Pecas >= QrAptosSdoVrtPeca.Value) then
  begin
    EdPesoKgBxa.ValueVariant  := QrAptosSdoVrtPeso.Value;
    LaPesoKgBxa.Enabled  := False;
    EdPesoKgBxa.Enabled  := False;
  end else
  begin
    //LaPesoKg.Enabled  := GBGerar.Visible;
    //EdPesoKg.Enabled  := GBGerar.Visible;
    CalculaPesoParcial();
  end;
  if (FFatorIntSrc <> 0) and (FFatorIntDst <> 0) then
    EdPecasSrc.ValueVariant := Pecas * FFatorIntSrc / FFatorIntDst;
end;

procedure TFmVSGerArtAltInN.EdPecasBxaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Txt: String;
  Pecas: Double;
begin
  if Geral.IntInConjunto(1, CGTpCalcAutoBxa.Value) then
    CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value - 1;
  //
  if Key = VK_F4 then
  begin
    EdPecasBxa.ValueVariant := QrAptosSdoVrtPeca.Value;
    EdPesoKgBxa.ValueVariant := QrAptosSdoVrtPeso.Value;
    if not Geral.IntInConjunto(1, CGTpCalcAutoBxa.Value) then
      CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value + 1;
  end else
  if Key = VK_F5 then
  begin
    Txt := '0,00';
    if InputQuery('Pe�as', 'Informe as pe�as a gerar:', Txt) then
    begin
      Pecas := Geral.DMV(Txt);
      EdPecasBxa.ValueVariant := Pecas / 2;
      EdPecasSrc.ValueVariant := Pecas;
    end;
  end;
end;

procedure TFmVSGerArtAltInN.EdPecasSrcKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Fator: Integer;
begin
  if Key = VK_F5 then
  begin
    Fator := MyObjects.SelRadioGroup('Fator de divis�o',
    'Escolha a forma de divis�o', [
    'Sem reparti��o ao meio',
    'Foi repartido de inteiros para meios'],
    -1);
    case Fator of
      0: Fator := 1;
      1: Fator := 2;
      else Fator := 0;
    end;
    if Fator <> 0 then
      EdPecasSrc.ValueVariant := EdPecasBxa.ValueVariant * Fator;
  end;
end;

procedure TFmVSGerArtAltInN.EdPesoKgBxaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  Pecas, PesoKg: Double;
*)
begin
(*
  if Key = VK_F4 then
  begin
    Pecas  := EdPecas.ValueVariant;
    PesoKg := 0;
    if QrAptosSdoVrtPeca.Value > 0 then
      PesoKg := Pecas / QrAptosSdoVrtPeca.Value * QrAptosSdoVrtPeso.Value;
    //
    EdPesoKg.ValueVariant := PesoKg;
  end;
*)
end;

procedure TFmVSGerArtAltInN.EdSerieFchChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtAltInN.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmVSGerArtAltInN.FechaPesquisa();
begin
  QrAptos.Close;
end;

procedure TFmVSGerArtAltInN.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSGerArtAltInN.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VS_CRC_PF.AbreVSSerFch(QrVSSerFch);
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1024_VSNatCad));

  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  CGTpCalcAutoBxa.Value := 2; // so peso a principio
end;

procedure TFmVSGerArtAltInN.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

{
function TFmVSGerArtAltInN.IMEI_JaLocalizado(const IMEI: Integer;
  const Lista: array of Integer; var ItensLoc: Integer): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Length(Lista) -1 do
  begin
    if Lista[I] = IMEI then
    begin
      Result := True;
      ItensLoc := ItensLoc + 1;
      Exit;
    end;
  end;
end;
}

(*
function TFmVSGerArtAltInN.IMEI_JaPesquisado(IMEI: Integer): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Length(FItensPesquisados) -1 do
  begin
    if FItensPesquisados[I] = IMEI then
    begin
      Result := True;
      FItensPsq := FItensPsq + 1;
      Exit;
    end;
  end;
end;
*)

procedure TFmVSGerArtAltInN.LiberaEdicao(Libera: Boolean);
var
  Status: Boolean;
begin
  if Libera then
    Status := QrAptos.RecordCount > 0
  else
    Status := False;
  //
  if Status then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(EdSrcGGX.ValueVariant),
    '']);
    FFatorIntDst := Trunc(QrNiv1FatorInt.Value * 1000);
    FMediaMinM2  := QrNiv1MediaMinM2.Value;
    FMediaMaxM2  := QrNiv1MediaMaxM2.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(QrAptosGraGruX.Value),
    '']);
    FFatorIntSrc := Trunc(QrNiv1FatorInt.Value * 1000);
    //
    if (FFatorIntSrc = 0) or (FFatorIntDst = 0) then
    begin
      Geral.MB_Aviso('Sele��o abortada!' + sLineBreak +
      'O Artigo "' + FmVSGerArtCab.QrVSGerArtNewNO_PRD_TAM_COR.Value +
      '" n�o est� configurado completamente!' + sLineBreak +
      'Termine sua configura��o antes de continuar!');
      //
      Close;
      Exit;
    end;
    if FFatorIntSrc <> FFatorIntDst then
      Geral.MB_Aviso('CUIDADO!!!' + sLineBreak +
      'Ao informar a quantidade leve em considera��o a possibilidade de transform��o de inteiros para meios!!!');
  end;
  //
  GBAptos.Enabled := not Status;

  GBGerar.Visible := Status;
  //
  LaFicha.Enabled := not Status;
  EdFicha.Enabled := not Status;
  LaGraGruX.Enabled := not Status;
  EdGraGruX.Enabled := not Status;
  CBGraGruX.Enabled := not Status;
  LaTerceiro.Enabled := not Status;
  EdTerceiro.Enabled := not Status;
  CBTerceiro.Enabled := not Status;
  BtReabre.Enabled := not Status;
  //
  if Libera and (EdPecasBxa.Enabled) and (EdPecasBxa.Visible) then
    EdPecasBxa.SetFocus;
  //
(*
  if GBGerar.Visible then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(EdSrcGGX.ValueVariant),
    '']);
    FFatorIntDst := Trunc(QrNiv1FatorInt.Value * 1000);
    FMediaMinM2  := QrNiv1MediaMinM2.Value;
    FMediaMaxM2  := QrNiv1MediaMaxM2.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
    'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couNiv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(QrAptosGraGruX.Value),
    '']);
    FFatorIntSrc := Trunc(QrNiv1FatorInt.Value * 1000);
    //
    if FFatorIntSrc <> FFatorIntDst then
      Geral.MB_Aviso('CUIDADO!!!' + sLineBreak +
      'Ao informar a quantidade leve em considera��o a possibilidade de transform��o de inteiros para meios!!!');
  end;
*)
end;

procedure TFmVSGerArtAltInN.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmVSGerArtAltInN.ReopenItensAptos();
var
  SQL_GraGruX, SQL_Terceiro, SQL_SerieFch, SQL_Ficha: String;
begin
  SQL_GraGruX  := '';
  SQL_Terceiro := '';
  SQL_Ficha    := '';
  if EdGraGruX.ValueVariant <> 0 then
    SQL_GraGruX  := 'AND wmi.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant);
  if EdTerceiro.ValueVariant <> 0 then
    SQL_Terceiro  := 'AND wmi.Terceiro=' + Geral.FF0(EdTerceiro.ValueVariant);
  if EdSerieFch.ValueVariant <> 0 then
    SQL_SerieFch  := 'AND wmi.SerieFch=' + Geral.FF0(EdSerieFch.ValueVariant);
  if EdFicha.ValueVariant <> 0 then
    SQL_Ficha  := 'AND wmi.Ficha=' + Geral.FF0(EdFicha.ValueVariant);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAptos, Dmod.MyDB, [
  'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM ' + CO_SEL_TAB_VMI + ' wmi ',
  'LEFT JOIN vsmovcab vmc ON vmc.Codigo=wmi.Codigo ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vsnatart   vna ON vna.VSNatCad=wmi.GraGruX',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ',
  'WHERE wmi.MovimID=' + Geral.FF0(Integer(emidCompra)),
  'AND SdoVrtPeca>0 ',
  'AND vna.VSRibCad=' + Geral.FF0(FNewGraGruX),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  SQL_GraGruX,
  SQL_Terceiro,
  SQL_SerieFch,
  SQL_Ficha,
  'ORDER BY wmi.Controle ',
  '']);
  //Geral.MB_SQL(self, QrAptos);
end;

procedure TFmVSGerArtAltInN.ReopenVSGerArtSrc(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSGerArtAltInN.SbPesoKgBxaClick(Sender: TObject);
var
  ValVar: Variant;
  PesoKg: Double;
begin
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, 0, 3, 0,
  '0,000', EdPesoKgBxa.Text, True,
  'Peso kg', 'Informe o peso (kg): ', 0, ValVar) then
  begin
    EdPesoKgBxa.ValueVariant := Geral.DMV(ValVar);
    //FTpCalcAuto := UnAppListas.DefineTpCalcAutoCouro(False, True, False, False);
    if Geral.IntInConjunto(2, CGTpCalcAutoBxa.Value) then
      CGTpCalcAutoBxa.Value := CGTpCalcAutoBxa.Value - 2;
  end else;
end;

function TFmVSGerArtAltInN.ValorMPParcial(): Double;
begin
  if QrAptosPesoKg.Value > 0 then
    Result := EdPesoKgBxa.ValueVariant / QrAptosPesoKg.Value * QrAptosValorT.Value
  else
    Result := 0;
end;

function TFmVSGerArtAltInN.ValorTParcial(): Double;
begin
  if QrAptosPesoKg.Value > 0 then
    Result := EdPesoKgBxa.ValueVariant / QrAptosPesoKg.Value * QrAptosValorT.Value
  else
    Result := 0;
end;

end.
