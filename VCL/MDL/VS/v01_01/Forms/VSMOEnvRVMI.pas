unit VSMOEnvRVMI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditCalc;

type
  TFmVSMOEnvRVMI = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    Panel6: TPanel;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    EdPesoKg: TdmkEdit;
    LaPeso: TLabel;
    Label5: TLabel;
    EdVSMOEnvEnv: TdmkEdit;
    EdNFeSer: TdmkEdit;
    Label10: TLabel;
    EdNFeNum: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdValorT: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }

  public
    { Public declarations }
    FTabVMIQtdEnvRVMI: String;
  end;

  var
  FmVSMOEnvRVMI: TFmVSMOEnvRVMI;

implementation

uses UnMyObjects, UMySQLModule, ModuleGeral, GetValor;

{$R *.DFM}

procedure TFmVSMOEnvRVMI.BtOKClick(Sender: TObject);
var
  VSMOEnvEnv, NFeSer, NFeNum: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  SQLType: TSQLType;
begin
  SQLType         := ImgTipo.SQLType;
  VSMOEnvEnv      := EdVSMOEnvEnv.ValueVariant;
  //NFeSer          := EdNFeSer.ValueVariant;
  //NFeNum          := EdNFeNum.ValueVariant;
  Pecas           := EdPecas.ValueVariant;
  PesoKg          := EdPesoKg.ValueVariant;
  AreaM2          := EdAreaM2.ValueVariant;
  AreaP2          := EdAreaP2.ValueVariant;
  ValorT          := EdValorT.ValueVariant;
  //
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, SQLType, FTabVMIQtdEnvRVMI, False, [
  //'NFeSer', 'NFeNum',
  'Pecas',
  'PesoKg', 'AreaM2', 'AreaP2',
  'ValorT'], [
  'VSMOEnvEnv'], [
  //NFeSer, NFeNum,
  Pecas,
  PesoKg, AreaM2, AreaP2,
  ValorT], [
  VSMOEnvEnv], False) then
  begin
    Close;
  end;
end;

procedure TFmVSMOEnvRVMI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMOEnvRVMI.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  PecaAnt, PesoAnt, AreaAnt, PecaDep, PesoDep, AreaDep, ValrAnt, ValrDep,
  Fator: Double;
var
  ResVar: Variant;
  CasasDecimais: Integer;
begin
  if Key = VK_F4 then
  begin
    PecaAnt := EdPecas.ValueVariant;
    if PecaAnt = 0 then
      Exit;
    PesoAnt := EdPesoKg.ValueVariant;
    AreaAnt := EdAreaM2.ValueVariant;
    ValrAnt := EdValorT.ValueVariant;
    //
    CasasDecimais := 1;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, PecaAnt,
    CasasDecimais, 0, '', '', True, 'Quantidade de Pe�as',
    'Informe a quantidade de pe�as: ', 0, ResVar) then
    begin
      PecaDep := Geral.DMV(ResVar);
      Fator   := PecaDep / PecaAnt;
      PesoDep := PesoAnt * Fator;
      AreaDep := AreaAnt * Fator;
      ValrDep := ValrAnt * Fator;
      //
      EdPecas.ValueVariant  := PecaDep;
      EdPesoKg.ValueVariant := PesoDep;
      EdAreaM2.ValueVariant := AreaDep;
      EdValorT.ValueVariant := ValrDep;
    end;
  end;
end;

procedure TFmVSMOEnvRVMI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMOEnvRVMI.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSMOEnvRVMI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
