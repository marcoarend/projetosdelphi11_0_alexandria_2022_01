object FmVSCOPCab: TFmVSCOPCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-155 :: Configura'#231#227'o de Opera'#231#245'es e Processos'
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 535
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitHeight = 618
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 297
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label10: TLabel
        Left = 516
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label11: TLabel
        Left = 16
        Top = 56
        Width = 95
        Height = 13
        Caption = 'Artigo em opera'#231#227'o:'
      end
      object Label12: TLabel
        Left = 468
        Top = 56
        Width = 105
        Height = 13
        Caption = 'Artigo a ser produzido:'
      end
      object Label13: TLabel
        Left = 16
        Top = 96
        Width = 240
        Height = 13
        Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
      end
      object Label14: TLabel
        Left = 468
        Top = 96
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object Label15: TLabel
        Left = 244
        Top = 180
        Width = 50
        Height = 13
        Caption = 'Opera'#231#227'o:'
      end
      object Label16: TLabel
        Left = 16
        Top = 136
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object Label17: TLabel
        Left = 16
        Top = 180
        Width = 68
        Height = 13
        Caption = 'ID movimento:'
      end
      object Label20: TLabel
        Left = 560
        Top = 180
        Width = 114
        Height = 13
        Caption = 'Item de pedido atrelado:'
      end
      object Label18: TLabel
        Left = 468
        Top = 136
        Width = 109
        Height = 13
        Caption = 'Pre'#231'o de M'#227'o-de-obra:'
      end
      object Label6: TLabel
        Left = 16
        Top = 220
        Width = 232
        Height = 13
        Caption = 'Configura'#231#227'o de artigo semi-acabado / acabado:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSCOPCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 437
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsVSCOPCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkEditCB1: TdmkEditCB
        Left = 516
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = dmkDBLookupComboBox1
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object dmkDBLookupComboBox1: TdmkDBLookupComboBox
        Left = 576
        Top = 32
        Width = 425
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 3
        dmkEditCB = dmkEditCB1
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object DBEdit1: TDBEdit
        Left = 72
        Top = 196
        Width = 169
        Height = 21
        DataField = 'NO_MovimID'
        DataSource = DsVSCOPCab
        TabOrder = 4
      end
      object DBEdit2: TDBEdit
        Left = 16
        Top = 196
        Width = 56
        Height = 21
        DataField = 'MovimID'
        DataSource = DsVSCOPCab
        TabOrder = 5
      end
      object DBEdit3: TDBEdit
        Left = 304
        Top = 196
        Width = 253
        Height = 21
        DataField = 'NO_Operacoes'
        DataSource = DsVSCOPCab
        TabOrder = 6
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsVSCOPCab
        TabOrder = 7
      end
      object DBEdit5: TDBEdit
        Left = 468
        Top = 72
        Width = 56
        Height = 21
        DataField = 'GGXDst'
        DataSource = DsVSCOPCab
        TabOrder = 8
      end
      object DBEdit6: TDBEdit
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        DataField = 'ClienteMO'
        DataSource = DsVSCOPCab
        TabOrder = 9
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 944
        Top = 60
        Width = 57
        Height = 77
        Caption = ' '#193'rea:'
        DataField = 'TipoArea'
        DataSource = DsVSCOPCab
        Items.Strings = (
          'm'#178
          'ft'#178)
        TabOrder = 10
        Values.Strings = (
          '0'
          '1')
      end
      object DBEdit9: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'ForneceMO'
        DataSource = DsVSCOPCab
        TabOrder = 11
      end
      object DBEdit10: TDBEdit
        Left = 468
        Top = 112
        Width = 56
        Height = 21
        DataField = 'StqCenLoc'
        DataSource = DsVSCOPCab
        TabOrder = 12
      end
      object DBEdit11: TDBEdit
        Left = 560
        Top = 196
        Width = 56
        Height = 21
        DataField = 'PedItsLib'
        DataSource = DsVSCOPCab
        TabOrder = 13
      end
      object DBEdit12: TDBEdit
        Left = 244
        Top = 196
        Width = 56
        Height = 21
        DataField = 'Operacoes'
        DataSource = DsVSCOPCab
        TabOrder = 14
      end
      object DBEdit13: TDBEdit
        Left = 72
        Top = 72
        Width = 393
        Height = 21
        DataField = 'NO_PRD_TAM_COR_INN'
        DataSource = DsVSCOPCab
        TabOrder = 15
      end
      object DBEdit14: TDBEdit
        Left = 524
        Top = 72
        Width = 417
        Height = 21
        DataField = 'NO_PRD_TAM_COR_DST'
        DataSource = DsVSCOPCab
        TabOrder = 16
      end
      object DBEdit15: TDBEdit
        Left = 72
        Top = 152
        Width = 393
        Height = 21
        DataField = 'NO_CLIENTEMO'
        DataSource = DsVSCOPCab
        TabOrder = 17
      end
      object DBEdit16: TDBEdit
        Left = 72
        Top = 112
        Width = 393
        Height = 21
        DataField = 'NO_FORNECEMO'
        DataSource = DsVSCOPCab
        TabOrder = 18
      end
      object DBEdit17: TDBEdit
        Left = 524
        Top = 112
        Width = 417
        Height = 21
        DataField = 'NO_LOC_CEN'
        DataSource = DsVSCOPCab
        TabOrder = 19
      end
      object DBEdit18: TDBEdit
        Left = 616
        Top = 196
        Width = 385
        Height = 21
        DataField = 'NO_PRD_TAM_COR_PED'
        DataSource = DsVSCOPCab
        TabOrder = 20
      end
      object DBEdit20: TDBEdit
        Left = 468
        Top = 152
        Width = 56
        Height = 21
        DataField = 'VSPMOCab'
        DataSource = DsVSCOPCab
        TabOrder = 21
      end
      object DBEdit19: TDBEdit
        Left = 524
        Top = 152
        Width = 477
        Height = 21
        DataField = 'NO_VSPMOCab'
        DataSource = DsVSCOPCab
        TabOrder = 22
      end
      object DBEdit7: TDBEdit
        Left = 16
        Top = 236
        Width = 56
        Height = 21
        DataField = 'VSArtCab'
        DataSource = DsVSCOPCab
        TabOrder = 23
      end
      object DBEdit8: TDBEdit
        Left = 72
        Top = 236
        Width = 485
        Height = 21
        DataField = 'NO_VSArtCab'
        DataSource = DsVSCOPCab
        TabOrder = 24
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 471
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 554
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 263
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Configura'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 590
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Origens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 297
      Width = 1008
      Height = 148
      Align = alTop
      DataSource = DsVSCOPIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 535
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitHeight = 618
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 269
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 516
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 95
        Height = 13
        Caption = 'Artigo em opera'#231#227'o:'
      end
      object Label54: TLabel
        Left = 468
        Top = 56
        Width = 105
        Height = 13
        Caption = 'Artigo a ser produzido:'
      end
      object Label35: TLabel
        Left = 16
        Top = 96
        Width = 240
        Height = 13
        Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
      end
      object Label49: TLabel
        Left = 468
        Top = 96
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object Label55: TLabel
        Left = 244
        Top = 180
        Width = 50
        Height = 13
        Caption = 'Opera'#231#227'o:'
      end
      object LaPedItsLib: TLabel
        Left = 560
        Top = 180
        Width = 114
        Height = 13
        Caption = 'Item de pedido atrelado:'
      end
      object Label62: TLabel
        Left = 16
        Top = 136
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object Label4: TLabel
        Left = 16
        Top = 180
        Width = 68
        Height = 13
        Caption = 'ID movimento:'
      end
      object Label5: TLabel
        Left = 468
        Top = 136
        Width = 109
        Height = 13
        Caption = 'Pre'#231'o de M'#227'o-de-obra:'
      end
      object Label66: TLabel
        Left = 16
        Top = 220
        Width = 232
        Height = 13
        Caption = 'Configura'#231#227'o de artigo semi-acabado / acabado:'
      end
      object SbGraGruX: TSpeedButton
        Left = 444
        Top = 72
        Width = 21
        Height = 22
        Caption = '...'
        OnClick = SbGraGruXClick
      end
      object SpeedButton5: TSpeedButton
        Left = 920
        Top = 72
        Width = 21
        Height = 22
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object SbVSArtCab: TSpeedButton
        Left = 620
        Top = 236
        Width = 21
        Height = 22
        Caption = '...'
        OnClick = SbVSArtCabClick
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 437
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 516
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 576
        Top = 32
        Width = 425
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 3
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object RGTipoArea: TdmkRadioGroup
        Left = 944
        Top = 60
        Width = 57
        Height = 77
        Caption = ' '#193'rea:'
        ItemIndex = 0
        Items.Strings = (
          'm'#178
          'ft'#178)
        TabOrder = 12
        QryCampo = 'TipoArea'
        UpdCampo = 'TipoArea'
        UpdType = utYes
        OldValor = 0
      end
      object EdGraGruX: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraGruX'
        UpdCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 373
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 5
        dmkEditCB = EdGraGruX
        QryCampo = 'GraGruX'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdGGXDst: TdmkEditCB
        Left = 468
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GGXDst'
        UpdCampo = 'GGXDst'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGGXDst
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGGXDst: TdmkDBLookupComboBox
        Left = 524
        Top = 72
        Width = 397
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGGXDst
        TabOrder = 7
        dmkEditCB = EdGGXDst
        QryCampo = 'GGXDst'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdForneceMO: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ForneceMO'
        UpdCampo = 'ForneceMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecMO: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 393
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsPrestador
        TabOrder = 9
        dmkEditCB = EdForneceMO
        QryCampo = 'ForneceMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 468
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'StqCenLoc'
        UpdCampo = 'StqCenLoc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 524
        Top = 112
        Width = 417
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_LOC_CEN'
        ListSource = DsStqCenLoc
        TabOrder = 11
        dmkEditCB = EdStqCenLoc
        QryCampo = 'StqCenLoc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdOperacoes: TdmkEditCB
        Left = 244
        Top = 196
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 19
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Operacoes'
        UpdCampo = 'Operacoes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBOperacoes
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBOperacoes: TdmkDBLookupComboBox
        Left = 300
        Top = 196
        Width = 257
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOperacoes
        TabOrder = 20
        dmkEditCB = EdOperacoes
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPedItsLib: TdmkEditCB
        Left = 560
        Top = 196
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 21
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PedItsLib'
        UpdCampo = 'PedItsLib'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPedItsLib
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPedItsLib: TdmkDBLookupComboBox
        Left = 616
        Top = 196
        Width = 385
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsVSPedIts
        TabOrder = 22
        dmkEditCB = EdPedItsLib
        QryCampo = 'PedItsLib'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdClienteMO: TdmkEditCB
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClienteMO'
        UpdCampo = 'ClienteMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClienteMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClienteMO: TdmkDBLookupComboBox
        Left = 72
        Top = 152
        Width = 393
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientMO
        TabOrder = 14
        dmkEditCB = EdClienteMO
        QryCampo = 'ClienteMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMovimID: TdmkEditCB
        Left = 16
        Top = 196
        Width = 36
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimID'
        UpdCampo = 'MovimID'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMovimID
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMovimID: TdmkDBLookupComboBox
        Left = 56
        Top = 196
        Width = 185
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSMovimID
        TabOrder = 18
        dmkEditCB = EdMovimID
        QryCampo = 'MovimID'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdVSPMOCab: TdmkEditCB
        Left = 468
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'VSPMOCab'
        UpdCampo = 'VSPMOCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVSPMOCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBVSPMOCab: TdmkDBLookupComboBox
        Left = 524
        Top = 152
        Width = 477
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSPMOCab
        TabOrder = 16
        dmkEditCB = EdVSPMOCab
        QryCampo = 'VSPMOCab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdVSArtCab: TdmkEditCB
        Left = 16
        Top = 236
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 23
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'VSArtCab'
        UpdCampo = 'VSArtCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVSArtCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBVSArtCab: TdmkDBLookupComboBox
        Left = 72
        Top = 236
        Width = 549
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSArtCab
        TabOrder = 24
        dmkEditCB = EdVSArtCab
        QryCampo = 'VSArtCab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkCpermitirCrust: TCheckBox
        Left = 584
        Top = 54
        Width = 141
        Height = 17
        Caption = 'Permitir semi e acabado.'
        Checked = True
        State = cbChecked
        TabOrder = 25
        OnClick = CkCpermitirCrustClick
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 472
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 555
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 498
        Height = 32
        Caption = 'Configura'#231#227'o de Opera'#231#245'es e Processos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 498
        Height = 32
        Caption = 'Configura'#231#227'o de Opera'#231#245'es e Processos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 498
        Height = 32
        Caption = 'Configura'#231#227'o de Opera'#231#245'es e Processos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSCOPCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSCOPCabBeforeOpen
    AfterOpen = QrVSCOPCabAfterOpen
    BeforeClose = QrVSCOPCabBeforeClose
    AfterScroll = QrVSCOPCabAfterScroll
    SQL.Strings = (
      'SELECT "" NO_MovimID, ope.Nome NO_Operacoes, vcc.*,  '
      'CONCAT(gg1i.Nome,  '
      'IF(gtii.PrintTam=0, "", CONCAT(" ", gtii.Nome)),  '
      'IF(gcci.PrintCor=0,"", CONCAT(" ", gcci.Nome)))  '
      'NO_PRD_TAM_COR_INN,  '
      'CONCAT(gg1d.Nome,  '
      'IF(gtid.PrintTam=0, "", CONCAT(" ", gtid.Nome)),  '
      'IF(gccd.PrintCor=0,"", CONCAT(" ", gccd.Nome)))  '
      'NO_PRD_TAM_COR_DST,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_Empresa,  '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,  '
      'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FORNECEMO  '
      '  '
      'FROM vscopcab vcc  '
      'LEFT JOIN entidades  emp  ON emp.Codigo=vcc.Empresa  '
      'LEFT JOIN entidades  cli  ON cli.Codigo=vcc.ClienteMO  '
      'LEFT JOIN entidades  fmo  ON fmo.Codigo=vcc.ForneceMO  '
      '  '
      'LEFT JOIN gragrux    ggxi ON ggxi.Controle=vcc.GraGruX  '
      'LEFT JOIN gragruc    ggci ON ggci.Controle=ggxi.GraGruC  '
      'LEFT JOIN gracorcad  gcci ON gcci.Codigo=ggci.GraCorCad  '
      'LEFT JOIN gratamits  gtii ON gtii.Controle=ggxi.GraTamI  '
      'LEFT JOIN gragru1    gg1i ON gg1i.Nivel1=ggxi.GraGru1  '
      'LEFT JOIN unidmed    unmi ON unmi.Codigo=gg1i.UnidMed  '
      '  '
      'LEFT JOIN gragrux    ggxd ON ggxd.Controle=vcc.GGXDst  '
      'LEFT JOIN gragruc    ggcd ON ggcd.Controle=ggxd.GraGruC  '
      'LEFT JOIN gracorcad  gccd ON gccd.Codigo=ggcd.GraCorCad  '
      'LEFT JOIN gratamits  gtid ON gtid.Controle=ggxd.GraTamI  '
      'LEFT JOIN gragru1    gg1d ON gg1d.Nivel1=ggxd.GraGru1  '
      'LEFT JOIN unidmed    unmd ON unmd.Codigo=gg1d.UnidMed  '
      '  '
      'LEFT JOIN stqcenloc  scl ON scl.Controle=vcc.StqCenLoc  '
      'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo  '
      'LEFT JOIN operacoes  ope ON ope.Codigo=vcc.Operacoes  '
      '')
    Left = 772
    Top = 1
    object QrVSCOPCabNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Required = True
      Size = 60
    end
    object QrVSCOPCabNO_Operacoes: TWideStringField
      FieldName = 'NO_Operacoes'
      Size = 30
    end
    object QrVSCOPCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCOPCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSCOPCabMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSCOPCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSCOPCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSCOPCabGGXDst: TIntegerField
      FieldName = 'GGXDst'
    end
    object QrVSCOPCabClienteMO: TIntegerField
      FieldName = 'ClienteMO'
    end
    object QrVSCOPCabTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSCOPCabForneceMO: TIntegerField
      FieldName = 'ForneceMO'
    end
    object QrVSCOPCabStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVSCOPCabPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrVSCOPCabOperacoes: TIntegerField
      FieldName = 'Operacoes'
    end
    object QrVSCOPCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSCOPCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSCOPCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSCOPCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSCOPCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSCOPCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSCOPCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSCOPCabNO_PRD_TAM_COR_PED: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_PED'
      Size = 255
    end
    object QrVSCOPCabNO_PRD_TAM_COR_INN: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_INN'
      Size = 157
    end
    object QrVSCOPCabNO_PRD_TAM_COR_DST: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_DST'
      Size = 157
    end
    object QrVSCOPCabNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrVSCOPCabNO_CLIENTEMO: TWideStringField
      FieldName = 'NO_CLIENTEMO'
      Size = 100
    end
    object QrVSCOPCabNO_FORNECEMO: TWideStringField
      FieldName = 'NO_FORNECEMO'
      Size = 100
    end
    object QrVSCOPCabNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 255
    end
    object QrVSCOPCabVSPMOCab: TIntegerField
      FieldName = 'VSPMOCab'
    end
    object QrVSCOPCabNO_VSPMOCab: TWideStringField
      FieldName = 'NO_VSPMOCab'
      Size = 60
    end
    object QrVSCOPCabVSArtCab: TIntegerField
      FieldName = 'VSArtCab'
    end
    object QrVSCOPCabNO_VSArtCab: TWideStringField
      FieldName = 'NO_VSArtCab'
      Size = 255
    end
  end
  object DsVSCOPCab: TDataSource
    DataSet = QrVSCOPCab
    Left = 772
    Top = 45
  end
  object QrVSCOPIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,'
      'vci.*'
      'FROM vscopits vci'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vci.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 868
    Top = 5
    object QrVSCOPItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSCOPItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSCOPItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCOPItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrVSCOPItsCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrVSCOPItsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrVSCOPItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCOPItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSCOPItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSCOPItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSCOPItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSCOPItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSCOPItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSCOPItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSCOPItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSCOPIts: TDataSource
    DataSet = QrVSCOPIts
    Left = 868
    Top = 49
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 348
    Top = 660
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 220
    Top = 660
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 260
    Top = 96
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.GraGruY > 0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 256
    Top = 48
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object QrGGXDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.GraGruY > 0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 340
    Top = 48
    object QrGGXDstGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXDstControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXDstSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXDstCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXDstNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXDst: TDataSource
    DataSet = QrGGXDst
    Left = 340
    Top = 96
  end
  object QrClientMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 192
    Top = 44
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 192
    Top = 92
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 504
    Top = 44
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 504
    Top = 92
  end
  object QrOperacoes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM operacoes'
      'ORDER BY Nome')
    Left = 584
    Top = 48
    object QrOperacoesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOperacoesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsOperacoes: TDataSource
    DataSet = QrOperacoes
    Left = 584
    Top = 100
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 420
    Top = 56
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 420
    Top = 100
  end
  object QrVSMovimID: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM vsmovimid'
      'ORDER BY Nome')
    Left = 692
    Top = 40
    object QrVSMovimIDCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovimIDNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSMovimID: TDataSource
    DataSet = QrVSMovimID
    Left = 692
    Top = 92
  end
  object QrVSPedIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.VSWetEnd=0')
    Left = 940
    Top = 8
    object QrVSPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrVSPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsVSPedIts: TDataSource
    DataSet = QrVSPedIts
    Left = 940
    Top = 52
  end
  object QrVSPMOCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM vspmocab'
      'ORDER BY Nome')
    Left = 776
    Top = 100
    object QrVSPMOCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPMOCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSPMOCab: TDataSource
    DataSet = QrVSPMOCab
    Left = 776
    Top = 152
  end
  object QrVSArtCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM vsartcab'
      'ORDER BY Nome')
    Left = 312
    Top = 192
    object QrVSArtCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSArtCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsVSArtCab: TDataSource
    DataSet = QrVSArtCab
    Left = 312
    Top = 236
  end
end
