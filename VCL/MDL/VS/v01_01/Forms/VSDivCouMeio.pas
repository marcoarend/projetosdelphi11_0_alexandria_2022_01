unit VSDivCouMeio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, UnProjGroup_Vars;

type
  TFmVSDivCouMeio = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnInfoBig: TPanel;
    PnDigitacao: TPanel;
    PnArea: TPanel;
    Label1: TLabel;
    LaTipoArea: TLabel;
    EdArea: TdmkEdit;
    BtReabre: TBitBtn;
    Panel5: TPanel;
    BitBtn1: TBitBtn;
    Panel6: TPanel;
    Panel9: TPanel;
    Label86: TLabel;
    EdSubClassA: TdmkEdit;
    Panel7: TPanel;
    Label3: TLabel;
    EdBoxA: TdmkEdit;
    Label12: TLabel;
    Panel8: TPanel;
    Panel10: TPanel;
    Label2: TLabel;
    EdSubClassB: TdmkEdit;
    Panel11: TPanel;
    Label4: TLabel;
    EdBoxB: TdmkEdit;
    LaBoxA: TLabel;
    LaBoxB: TLabel;
    LaAreaA: TLabel;
    LaAreaB: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdSubClassBKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    //FMovimID: TEstqMovimID;
    //FBoxes: array [1..VAR_CLA_ART_RIB_MAX_BOX] of Boolean;
    EhReclas: Boolean;
    //
    //function BoxInBoxes(Box: Integer): Boolean;

  end;

  var
  FmVSDivCouMeio: TFmVSDivCouMeio;

implementation

uses UnMyObjects, Module, VSReclassifOneNw3;

{$R *.DFM}

(*
function TFmVSDivCouMeio.BoxInBoxes(Box: Integer): Boolean;
begin
  if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX) then
    Result := FBoxes[Box]
  else
    Result := False;
end;
*)

procedure TFmVSDivCouMeio.BtOKClick(Sender: TObject);
var
  AreaA, AreaB: Double;
  BoxA, BoxB: Integer;
  SubClassA, SubClassB: String;
begin
  if EhReclas then
  begin
  //case FMovimID of
    //emidReclasVS:
    //begin
      AreaA := Trunc(EdArea.ValueVariant div 2);
      AreaB := EdArea.ValueVariant - AreaA;
      //
      LaBoxA.Caption := 'Box ' + Geral.FF0(EdBoxA.ValueVariant) + ':';
      LaBoxB.Caption := 'Box ' + Geral.FF0(EdBoxB.ValueVariant) + ':';
      LaAreaA.Caption := Geral.FFT(AreaA / 100, 2, siPositivo) + ' ' + LaTipoArea.Caption;
      LaAreaB.Caption := Geral.FFT(AreaB / 100, 2, siPositivo) + ' ' + LaTipoArea.Caption;
      BoxA      := EdBoxA.ValueVariant;
      BoxB      := EdBoxB.ValueVariant;
      SubClassA := EdSubClassA.ValueVariant;
      SubClassB := EdSubClassB.ValueVariant;
      Application.ProcessMessages;
      //
      if FmVSReclassifOneNw3.BoxInBoxes(BoxA) and FmVSReclassifOneNw3.BoxInBoxes(BoxB) then
      begin
        FmVSReclassifOneNw3.EdArea.ValueVariant := AreaA;
        FmVSReclassifOneNw3.EdBox.ValueVariant  := BoxA;
        FmVSReclassifOneNw3.EdSubClass.ValueVariant  := SubClassA;
        FmVSReclassifOneNw3.InsereCouroAtual();
        if FmVSReclassifOneNw3.EdArea.ValueVariant = 0 then
        begin
          FmVSReclassifOneNw3.EdArea.ValueVariant := AreaB;
          FmVSReclassifOneNw3.EdBox.ValueVariant  := BoxB;
          FmVSReclassifOneNw3.EdSubClass.ValueVariant  := SubClassB;
          FmVSReclassifOneNw3.InsereCouroAtual();
          if FmVSReclassifOneNw3.EdArea.ValueVariant = 0 then
          begin
             EdArea.ValueVariant := 0;
             EdBoxA.ValueVariant := 0;
             EdBoxB.ValueVariant := 0;
             EdSubClassA.ValueVariant := '';
             EdSubClassB.ValueVariant := '';
             //
             EdArea.SetFocus;
          end else
          begin
            Geral.MB_ERRO('ERRO ao inserir couros!');
            Close;
          end;
        end else
        begin
          Geral.MB_ERRO('ERRO ao inserir couros!');
          Close;
        end;
      end else
      begin
        Geral.MB_Aviso('Os dois Box precisam ser informados!');
        if FmVSReclassifOneNw3.BoxInBoxes(BoxA) then
          EdBoxB.SetFocus
        else
          EdBoxA.SetFocus;
      end;
    //end;
    end else Geral.MB_Aviso('Movimento n�o implementado!');
  //end;
end;

procedure TFmVSDivCouMeio.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSDivCouMeio.EdSubClassBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  BtOKClick(Self);
end;

procedure TFmVSDivCouMeio.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSDivCouMeio.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType := stLok;
  //
(*
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    FBoxes[I] := False;
*)
end;

procedure TFmVSDivCouMeio.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
