unit VSLstPal;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, Vcl.ComCtrls,
  dmkEditDateTimePicker, Vcl.Menus, UnProjGroup_Consts;

type
  TFmVSLstPal = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVSLstPal: TmySQLQuery;
    QrVSLstPalCodigo: TIntegerField;
    DsVSLstPal: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdResponsa: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    TPDataHora: TdmkEditDateTimePicker;
    Label52: TLabel;
    EdDataHora: TdmkEdit;
    Label53: TLabel;
    EdNumIni: TdmkEdit;
    EdNumFim: TdmkEdit;
    Label55: TLabel;
    QrVSLstPalEmpresa: TIntegerField;
    QrVSLstPalDataHora: TDateTimeField;
    QrVSLstPalNumIni: TIntegerField;
    QrVSLstPalNumFim: TIntegerField;
    QrVSLstPalLk: TIntegerField;
    QrVSLstPalDataCad: TDateField;
    QrVSLstPalDataAlt: TDateField;
    QrVSLstPalUserCad: TIntegerField;
    QrVSLstPalUserAlt: TIntegerField;
    QrVSLstPalAlterWeb: TSmallintField;
    QrVSLstPalAtivo: TSmallintField;
    QrVSLstPalResponsa: TWideStringField;
    QrVSLstPalNO_EMP: TWideStringField;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrVSLstPalQuantidade: TFloatField;
    Label10: TLabel;
    DBEdit7: TDBEdit;
    PMImprime: TPopupMenu;
    N1vianormal1: TMenuItem;
    N1viareduzido1: TMenuItem;
    N2viasnormal1: TMenuItem;
    N2viasreduzido1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSLstPalAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSLstPalBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure N1vianormal1Click(Sender: TObject);
    procedure N1viareduzido1Click(Sender: TObject);
    procedure N2viasnormal1Click(Sender: TObject);
    procedure N2viasreduzido1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ImprimeRMEs(Vias: Integer; Reduzido: Boolean);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmVSLstPal: TFmVSLstPal;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, VSImpRequis;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSLstPal.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSLstPal.N1vianormal1Click(Sender: TObject);
begin
  ImprimeRMEs(1, False);
end;

procedure TFmVSLstPal.N1viareduzido1Click(Sender: TObject);
begin
  ImprimeRMEs(1, True);
end;

procedure TFmVSLstPal.N2viasnormal1Click(Sender: TObject);
begin
  ImprimeRMEs(2, False);
end;

procedure TFmVSLstPal.N2viasreduzido1Click(Sender: TObject);
begin
  ImprimeRMEs(2, True);
end;

procedure TFmVSLstPal.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSLstPalCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSLstPal.DefParams;
begin
  VAR_GOTOTABELA := 'vslstpal';
  VAR_GOTOMYSQLTABLE := QrVSLstPal;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := 'Responsa';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vrm.*, vrm.NumFim - vrm.NumIni + 1.000 Quantidade, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP');
  VAR_SQLx.Add('FROM vslstpal vrm');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=vrm.Empresa ');
  VAR_SQLx.Add('WHERE vrm.Codigo > 0');
  //
  VAR_SQL1.Add('AND vrm.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND vrm.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vrm.Responsa LIKE :P0');
  //
end;

procedure TFmVSLstPal.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSLstPal.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSLstPal.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmVSLstPal.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSLstPal.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSLstPal.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSLstPal.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSLstPal.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSLstPal.BtAlteraClick(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSLstPal, [PnDados],
  [PnEdita], EdResponsa, ImgTipo, 'vslstpal');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSLstPalEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSLstPal.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSLstPalCodigo.Value;
  Close;
end;

procedure TFmVSLstPal.BtConfirmaClick(Sender: TObject);
var
  DataHora, Responsa: String;
  Codigo, Empresa, NumIni, NumFim: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DataHora       := Geral.FDT_TP_Ed(TPDataHora.Date, EdDataHora.Text);
  NumIni         := EdNumIni.ValueVariant;
  NumFim         := EdNumFim.ValueVariant;
  Responsa       := EdResponsa.Text;
  //
  if MyObjects.FIC(Length(Responsa) = 0, EdResponsa, 'Informe o responsável!') then
    Exit;
  Codigo := UMyMod.BPGS1I32('vslstpal', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vslstpal', False, [
  'Empresa', CO_DATA_HORA_GRL, 'NumIni',
  'NumFim', 'Responsa'], [
  'Codigo'], [
  Empresa, DataHora, NumIni,
  NumFim, Responsa], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSLstPal.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vslstpal', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVSLstPal.BtIncluiClick(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSLstPal, [PnDados],
  [PnEdita], EdResponsa, ImgTipo, 'vslstpal');
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDataHora.Date := Agora;
  EdDataHora.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDataHora.SetFocus;
end;

procedure TFmVSLstPal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmVSLstPal.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSLstPalCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSLstPal.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSLstPal.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSLstPal.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSLstPalCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSLstPal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSLstPal.QrVSLstPalAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSLstPal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSLstPal.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSLstPalCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vslstpal', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSLstPal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSLstPal.ImprimeRMEs(Vias: Integer; Reduzido: Boolean);
begin
  Application.CreateForm(TFmVSImpRequis, FmVSImpRequis);
  FmVSImpRequis.FEmpresa_Cod   := QrVSLstPalEmpresa.Value;
  FmVSImpRequis.FEmpresa_Txt   := QrVSLstPalNO_EMP.Value;
  FmVSImpRequis.FResponsa      := QrVSLstPalResponsa.Value;
  //
  FmVSImpRequis.FQuantidade    :=  Trunc(QrVSLstPalQuantidade.Value);
  FmVSImpRequis.FNumInicial    :=  QrVSLstPalNumIni.Value;
  FmVSImpRequis.FNumVias       :=  Vias;
  FmVSImpRequis.FReduzido      :=  Reduzido;
  //
  FmVSImpRequis.ImprimeLSP();
  //
  FmVSImpRequis.Destroy;
end;

procedure TFmVSLstPal.QrVSLstPalBeforeOpen(DataSet: TDataSet);
begin
  QrVSLstPalCodigo.DisplayFormat := FFormatFloat;
end;

end.

