unit VSOutAltVMI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.Grids, Vcl.DBGrids,
  UnProjGroup_Consts, dmkDBGridZTO, BlueDermConsts, UnAppEnums;

type
  TFmVSOutAltVMI = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX_: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruX_GraGru1: TIntegerField;
    QrGraGruX_Controle: TIntegerField;
    QrGraGruX_NO_PRD_TAM_COR: TWideStringField;
    QrGraGruX_SIGLAUNIDMED: TWideStringField;
    QrGraGruX_CODUSUUNIDMED: TIntegerField;
    QrGraGruX_NOMEUNIDMED: TWideStringField;
    Panel3: TPanel;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdDtEntrada: TdmkDBEdit;
    DBEdCliVenda: TdmkDBEdit;
    BtReabre: TBitBtn;
    Panel9: TPanel;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Panel10: TPanel;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdControle: TdmkEdit;
    EdItemNFe: TdmkEdit;
    Label15: TLabel;
    EdPecas: TdmkEdit;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    EdValorT: TdmkEdit;
    LaValorT: TLabel;
    SbValorT: TSpeedButton;
    EdMediaM2Pc: TdmkEdit;
    Label18: TLabel;
    QrVMI: TmySQLQuery;
    QrVMINO_Pallet: TWideStringField;
    QrVMICodigo: TIntegerField;
    QrVMIControle: TIntegerField;
    QrVMIMovimCod: TIntegerField;
    QrVMIMovimNiv: TIntegerField;
    QrVMIEmpresa: TIntegerField;
    QrVMITerceiro: TIntegerField;
    QrVMIMovimID: TIntegerField;
    QrVMIDataHora: TDateTimeField;
    QrVMIPallet: TIntegerField;
    QrVMIGraGruX: TIntegerField;
    QrVMIPecas: TFloatField;
    QrVMIPesoKg: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIAreaP2: TFloatField;
    QrVMISrcMovID: TIntegerField;
    QrVMISrcNivel1: TIntegerField;
    QrVMISrcNivel2: TIntegerField;
    QrVMILk: TIntegerField;
    QrVMIDataCad: TDateField;
    QrVMIDataAlt: TDateField;
    QrVMIUserCad: TIntegerField;
    QrVMIUserAlt: TIntegerField;
    QrVMIAlterWeb: TSmallintField;
    QrVMIAtivo: TSmallintField;
    QrVMISdoVrtPeca: TFloatField;
    QrVMINO_FORNECE: TWideStringField;
    QrVMISdoVrtArM2: TFloatField;
    QrVMIValorT: TFloatField;
    QrVMIFicha: TIntegerField;
    QrVMIMisturou: TIntegerField;
    QrVMISerieFch: TIntegerField;
    QrVMIMovimTwn: TIntegerField;
    QrVMICliVenda: TIntegerField;
    QrVMILnkNivXtr1: TIntegerField;
    QrVMILnkNivXtr2: TIntegerField;
    QrVMISdoVrtPeso: TFloatField;
    QrVMIObserv: TWideStringField;
    QrVMIFornecMO: TIntegerField;
    QrVMICustoMOKg: TFloatField;
    QrVMICustoMOTot: TFloatField;
    QrVMIValorMP: TFloatField;
    QrVMIDstMovID: TIntegerField;
    QrVMIDstNivel1: TIntegerField;
    QrVMIDstNivel2: TIntegerField;
    QrVMIQtdGerPeca: TFloatField;
    QrVMIQtdGerPeso: TFloatField;
    QrVMIQtdGerArM2: TFloatField;
    QrVMIQtdGerArP2: TFloatField;
    QrVMIQtdAntPeca: TFloatField;
    QrVMIQtdAntPeso: TFloatField;
    QrVMIQtdAntArM2: TFloatField;
    QrVMIQtdAntArP2: TFloatField;
    QrVMIAptoUso: TSmallintField;
    QrVMINotaMPAG: TFloatField;
    QrVMISrcGGX: TIntegerField;
    QrVMIDstGGX: TIntegerField;
    QrVMIMarca: TWideStringField;
    QrVMIPedItsLib: TIntegerField;
    QrVMIPedItsFin: TIntegerField;
    QrVMIPedItsVda: TIntegerField;
    QrVMIMediaM2: TFloatField;
    QrVMIVSMulFrnCab: TIntegerField;
    QrVMIStqCenLoc: TIntegerField;
    QrVMINO_PRD_TAM_COR: TWideStringField;
    QrVMIClientMO: TIntegerField;
    Panel1: TPanel;
    QrVMIGraGruY: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPsqPalletChange(Sender: TObject);
    procedure EdPsqFichaChange(Sender: TObject);
    procedure EdPecasRedefinido(Sender: TObject);
    procedure EdValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAreaM2Redefinido(Sender: TObject);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbValorTClick(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdStqCenCadRedefinido(Sender: TObject);
    procedure EdImeiSrcRedefinido(Sender: TObject);
    procedure EdPesoKgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdControleRedefinido(Sender: TObject);
  private
    { Private declarations }
    //
    procedure CalculaParcial();
    procedure CopiaTotaisIMEI();
    procedure ReopenVSInnIts(Controle: Integer);
    procedure ReopenVSMovIts((*GraGruX: Integer*));
    procedure ReopenGGXY();
    procedure ReopenVMI();
    procedure CalculaValorT();

  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSOutAltVMI: TFmVSOutAltVMI;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
{$IfDef sAllVS} UnVS_PF, {$EndIf}
VSOutCab, UnVS_CRC_PF, CalcParc4Val;

{$R *.DFM}

procedure TFmVSOutAltVMI.BtOKClick(Sender: TObject);
const
  Observ = '';
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  MovimTwn   = 0;
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  EdFicha    = nil;
  //EdPesoKg   = nil;
  CustoMOKg   = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  QtdGerPeca  = 0;
  QtdGerPeso  = 0;
  QtdGerArM2  = 0;
  QtdGerArP2  = 0;
  AptoUso     = 0; // Eh venda!
  FornecMO    = 0;
  NotaMPAG    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib   = 0;
  PedItsFin   = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, CliVenda,
  SerieFch, Ficha, (*Misturou,*) DstNivel1, DstNivel2, GraGruY, SrcGGX, DstGGX,
  PedItsVda, ItemNFe, VSMulFrnCab, ClientMO, GGXRcl, StqCenLoc: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
begin
  if MyObjects.FIC(ImgTipo.SQLType <> stUpd, nil,
    'Status SQL deve ser altera��o!') then
      Exit;
  SrcMovID       := QrVMISrcMovID.Value;
  SrcNivel1      := QrVMISrcNivel1.Value;
  SrcNivel2      := QrVMISrcNivel2.Value;
  SrcGGX         := QrVMISrcGGX.Value;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClientMO       := QrVMIClientMO.Value;
  StqCenLoc      := CO_STQCENLOC_ZERO;
  Terceiro       := QrVMITerceiro.Value;
  VSMulFrnCab    := QrVMIVSMulFrnCab.Value;
  CliVenda       := Geral.IMV(DBEdCliVenda.Text);
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidVenda;
  MovimNiv       := eminSemNiv;
  Pallet         := QrVMIPallet.Value;
  GraGruX        := QrVMIGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not VS_CRC_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  Pecas          := -EdPecas.ValueVariant;
  PesoKg         := -EdPesoKg.ValueVariant;
  AreaM2         := -EdAreaM2.ValueVariant;
  AreaP2         := -EdAreaP2.ValueVariant;
  ValorMP        := -EdValorT.ValueVariant;
  ValorT         := ValorMP;
  //
  SerieFch       := QrVMISerieFch.Value;
  Ficha          := QrVMIFicha.Value;
  Marca          := QrVMIMarca.Value;
  //Misturou       := QrVMIMisturou.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
//  GraGruY        := VS_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex);
  GraGruY        := QrVMIGraGruY.Value;
  if QrVMIPedItsFin.Value <> 0 then
    PedItsVda := QrVMIPedItsFin.Value
  else
    PedItsVda := QrVMIPedItsLib.Value;
  //
  if VS_CRC_PF.VSFic(GraGruX, Empresa, 0, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, nil, nil(*EdPallet*), EdFicha, EdPecas,
  nil, nil, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca,
  nil) then
    Exit;
  if MyObjects.FIC((QrVMISdoVrtArM2.Value > 0) and (AreaM2 = 0),
  EdAreaM2, 'Informe a �rea!') then
    Exit else
  if MyObjects.FIC((QrVMISdoVrtPeso.Value > 0) and (PesoKg = 0),
  EdPesoKg, 'Informe o peso!') then
    Exit;
  //
  if MyObjects.FIC(GGXRcl = 0, EdGGXRcl,
  'Informe o material usado para emitir NFe!') then
    Exit;
  //
  ItemNFe     := EdItemNFe.ValueVariant;
  if MyObjects.FIC(ItemNFe = 0, EdItemNFe,
  'Informe o item da NFe!') then
    Exit;
(*
  if Dmod.VSFic(GraGruX, Empresa, Terceiro, Pallet, Pecas, AreaM2, PesoKg, ValorT,
  EdGraGruX, EdPallet, EdPecas, EdAreaM2, EdValorT) then
    Exit;
*)


  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);

  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_FALSE_ExigeStqLoc,
  iuvpei095(*Altera��o de item de venda de produto*)) then
  begin
    VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    VS_CRC_PF.AtualizaTotaisVSXxxCab('vsoutcab', MovimCod);
{$IfDef sAllVS}
    VS_PF.AtualizaVSPedIts_Vda(PedItsVda);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
    FmVSOutCab.AtualizaNFeItens(MovimCod);
    FmVSOutCab.LocCod(Codigo, Codigo);
    ReopenVSInnIts(Controle);
    Close;
  end;
end;

procedure TFmVSOutAltVMI.BtReabreClick(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutAltVMI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOutAltVMI.CalculaParcial();
var
  Item, AreaTotal: String;
  It: Integer;
  ArIt: Double;
begin
  AreaTotal := '0,00';
  if InputQuery('Item da NFe', 'Informe o item da NFe:', Item) then
  begin
    if InputQuery('Area total', 'Informe a area total:', AreaTotal) then
    begin
      It := Geral.IMV(Item);
      ArIt := 0;
      with FmVSOutCab do
      begin
        QrVSOutIts.First;
        while not QrVSOutIts.Eof do
        begin
          if QrVSOutItsItemNFe.Value = It then
            ArIt := ArIt + QrVSOutItsAreaM2.Value;
          QrVSOutIts.Next;
        end;
      end;
      // Parei Aqui!
      Application.CreateForm(TFmCalcParc4Val, FmCalcParc4Val);
      FmCalcParc4Val.FCalcExec := calcexec1;
      FmCalcParc4Val.EdA2.Text := AreaTotal;
      FmCalcParc4Val.EdB2.ValueVariant := -ArIt;//-FQrCab.FieldByName('AreaM2').AsFloat;
      FmCalcParc4Val.Ed04Peca.ValueVariant := QrVMISdoVrtPeca.Value;
      FmCalcParc4Val.Ed04ArM2.ValueVariant := QrVMISdoVrtArM2.Value;
      FmCalcParc4Val.ShowModal;
      //
      if FmCalcParc4Val.FUsaDados then
      begin
        EdPecas.ValueVariant := FmCalcParc4Val.Ed03Peca.ValueVariant;
        EdAreaM2.ValueVariant := FmCalcParc4Val.Ed03ArM2.ValueVariant;
      end;
      FmCalcParc4Val.Destroy;
    end;
  end;
end;

procedure TFmVSOutAltVMI.CalculaValorT();
var
  Pecas, AreaM2, PesoKg, (*Valor, *)Media: Double;
begin
  begin
    Pecas  := EdPecas.ValueVariant;
    AreaM2 := EdAreaM2.ValueVariant;
    PesoKg := EdPesoKg.ValueVariant;
(*
    Valor := 0;
    if (QrVMIAreaM2.Value > 0) and (QrVMIAreaM2.Value > 0) then
      Valor := AreaM2 * (QrVMIValorT.Value / QrVMIAreaM2.Value)
    else
    if QrVMIPesoKg.Value > 0 then
      Valor := PesoKg * (QrVMIValorT.Value / QrVMIPesoKg.Value)
    else
    if QrVMIPecas.Value > 0 then
      Valor := Pecas * (QrVMIValorT.Value / QrVMIPecas.Value);
    EdValorT.ValueVariant := Valor;
*)
    //
    if Pecas > 0 then
      Media := AreaM2 / Pecas
    else
      Media := 0;
    EdMediaM2Pc.ValueVariant := Media;
  end;
end;

procedure TFmVSOutAltVMI.CopiaTotaisIMEI();
var
  Valor: Double;
begin
  EdPesoKg.ValueVariant := 0;
  EdAreaP2.ValueVariant := 0;
  // Deve ser depois dos zerados!
  EdPecas.ValueVariant  := QrVMISdoVrtPeca.Value;
  EdAreaM2.ValueVariant := QrVMISdoVrtArM2.Value;
  //
  if (QrVMISdoVrtArM2.Value > 0) and (QrVMIAreaM2.Value > 0) then
    Valor := QrVMISdoVrtArM2.Value *
    (QrVMIValorT.Value / QrVMIAreaM2.Value)
  else
  if QrVMIPecas.Value > 0 then
    Valor := QrVMISdoVrtPeca.Value *
    (QrVMIValorT.Value / QrVMIPecas.Value);
  //
  EdValorT.ValueVariant := Valor;
end;

procedure TFmVSOutAltVMI.EdStqCenCadRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutAltVMI.EdValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    CalculaValorT();
end;

procedure TFmVSOutAltVMI.EdAreaM2Redefinido(Sender: TObject);
begin
  //EdValorT.ValueVariant := 0;
  CalculaValorT();
end;

procedure TFmVSOutAltVMI.EdControleRedefinido(Sender: TObject);
begin
  ReopenVMI();
end;

procedure TFmVSOutAltVMI.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutAltVMI.EdImeiSrcRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutAltVMI.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaTotaisIMEI();
  if Key = VK_F3 then
    CalculaParcial();
end;

procedure TFmVSOutAltVMI.EdPecasRedefinido(Sender: TObject);
begin
  //EdValorT.ValueVariant := 0;
  CalculaValorT();
end;

procedure TFmVSOutAltVMI.EdPesoKgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  PecasOri, PecasDst, PesoDst, PesoOri: Double;
begin
  if Key = VK_F4 then
  begin
    PesoOri  := QrVMISdoVrtPeso.Value;
    PecasOri := QrVMISdoVrtPeca.Value;
    PecasDst := EdPecas.ValueVariant;
    PesoDst  := 0;
    //
    if PecasOri > 0 then
    begin
      PesoDst := PesoOri * (PecasDst / PecasOri);
      EdPesoKg.ValueVariant := PesoDst;
    end;
  end;
end;

procedure TFmVSOutAltVMI.EdPsqFichaChange(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutAltVMI.EdPsqPalletChange(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutAltVMI.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdCliVenda.DataSource   := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSOutAltVMI.FormCreate(Sender: TObject);
const
  Colunas = 2;
  Default = 2;
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenGGXY();
  //
end;

procedure TFmVSOutAltVMI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOutAltVMI.ReopenGGXY();
begin
(*
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(VS_CRC_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex)));
  //
  EdGraGruX.ValueVariant := 0;
  CBGraGruX.KeyValue     := 0;
  //
*)
  VS_CRC_PF.AbreGraGruXY(QrGGXRcl, '');
end;

procedure TFmVSOutAltVMI.ReopenVMI();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
  'SELECT ggx.GraGruY, pal.Nome NO_Pallet, vmi.*,',
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(vmi.SdoVrtPeca=0, 0, vmi.SdoVrtArM2 / vmi.SdoVrtPeca) MediaM2, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  pal ON pal.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.Controle=' + Geral.FF0(EdControle.ValueVariant),
  '']);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
  'SELECT ggx.GraGruY, vmi.*  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'WHERE vmi.Controle=' + Geral.FF0(EdControle.ValueVariant),
  '']);
*)
end;

procedure TFmVSOutAltVMI.ReopenVSInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSOutAltVMI.ReopenVSMovIts();
var
  Data: TDateTime;
  GraGruX, Empresa, Pallet, Ficha, IMEI, StqCenCad, ImeiSrc: Integer;
  //SQL_GraGruX, SQL_Pallet, SQL_Ficha, SQL_IMEI: String;
begin
{
  GraGruX    := EdGraGruX.ValueVariant;
  Empresa    := Geral.IMV(DBEdEmpresa.Text);
  (*SQL_Pallet := '';
  SQL_Ficha  := '';
  SQL_IMEI   := '';*)
  Pallet     := EdPsqPallet.ValueVariant;
  Ficha      := EdPsqFicha.ValueVariant;
  IMEI       := EdPsqIMEI.ValueVariant;
  StqCenCad  := EdStqCenCad.ValueVariant;
  ImeiSrc    := EdImeiSrc.ValueVariant;

  if CkDataMinima.Checked then
    Data := 0
  else
    Data := FmVSOutCab.QrVSOutCabDtVenda.Value;
  //
  VS_CRC_PF.ReopenIMEIsPositivos(QrVMI, Empresa, GraGruX, Pallet, Ficha, IMEI,
    RGTipoCouro.ItemIndex, StqCenCad, ImeiSrc, Data);
  //Geral.MB_SQL(Self, QrVMI);
}
end;

procedure TFmVSOutAltVMI.SbValorTClick(Sender: TObject);
begin
  EdValorT.Enabled := True;
  LaValorT.Enabled := True;
  EdValorT.SetFocus;
end;

end.
