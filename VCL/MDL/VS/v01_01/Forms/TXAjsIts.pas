unit TXAjsIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, UnProjGroup_Consts,
  UnProjGroup_Vars, Vcl.Menus, UnAppEnums;

type
  TFmTXAjsIts = class(TForm)
    GroupBox1: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    QrTXPallet: TmySQLQuery;
    DsTXPallet: TDataSource;
    DsFornecedor: TDataSource;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    Panel3: TPanel;
    Label5: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdMovimCod: TdmkDBEdit;
    Label3: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    Panel5: TPanel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrGraGruXGraGruY: TIntegerField;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    QrTXSerTal: TmySQLQuery;
    QrTXSerTalCodigo: TIntegerField;
    QrTXSerTalNome: TWideStringField;
    DsTXSerTal: TDataSource;
    QrTXPalletCodigo: TIntegerField;
    QrTXPalletGraGruX: TIntegerField;
    QrTXPalletNO_PRD_TAM_COR: TWideStringField;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    QrSrcNivel2: TmySQLQuery;
    DsSrcNivel2: TDataSource;
    QrSrcNivel2NO_PRD_TAM_COR: TWideStringField;
    QrSrcNivel2Controle: TIntegerField;
    QrSrcNivel2Codigo: TIntegerField;
    QrSrcNivel2MovimID: TIntegerField;
    QrSrcNivel2GraGruX: TIntegerField;
    QrSrcNivel2MovimNiv: TIntegerField;
    Panel6: TPanel;
    PnIMEIOrigem: TPanel;
    RGMovimentacao: TRadioGroup;
    Label18: TLabel;
    EdNivel2: TdmkEditCB;
    CBNivel2: TdmkDBLookupComboBox;
    Panel7: TPanel;
    GBDados: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    LaQtde: TLabel;
    Label4: TLabel;
    SBPallet: TSpeedButton;
    Label7: TLabel;
    Label10: TLabel;
    Label8: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label11: TLabel;
    Label15: TLabel;
    Label9: TLabel;
    Label16: TLabel;
    SBNewPallet: TSpeedButton;
    Label49: TLabel;
    Label50: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    EdQtde: TdmkEdit;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    EdValorT: TdmkEdit;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    EdValorMP: TdmkEdit;
    EdCustoMOUni: TdmkEdit;
    EdCustoMOTot: TdmkEdit;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    EdSerieTal: TdmkEditCB;
    CBSerieTal: TdmkDBLookupComboBox;
    EdTalao: TdmkEdit;
    EdMarca: TdmkEdit;
    EdObserv: TdmkEdit;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    SbImei: TSpeedButton;
    PMImei: TPopupMenu;
    Pallet1: TMenuItem;
    IMEI1: TMenuItem;
    Panel8: TPanel;
    RGIxxMovIX: TRadioGroup;
    EdIxxFolha: TdmkEdit;
    Label19: TLabel;
    EdIxxLinha: TdmkEdit;
    Label20: TLabel;
    SBNewByInfo: TSpeedButton;
    Panel1: TPanel;
    RGModoContinuarInserindo: TRadioGroup;
    Panel9: TPanel;
    CkContinuar: TCheckBox;
    alo1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBPalletClick(Sender: TObject);
    procedure EdValorMPChange(Sender: TObject);
    procedure EdTalaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPesoKgChange(Sender: TObject);
    procedure EdCustoMOUniChange(Sender: TObject);
    procedure SBNewPalletClick(Sender: TObject);
    procedure EdValorMPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdPalletRedefinido(Sender: TObject);
    procedure EdQtdeRedefinido(Sender: TObject);
    procedure RGMovimentacaoClick(Sender: TObject);
    procedure SbImeiClick(Sender: TObject);
    procedure Pallet1Click(Sender: TObject);
    procedure IMEI1Click(Sender: TObject);
    procedure SBNewByInfoClick(Sender: TObject);
    procedure EdCustoMOUniRedefinido(Sender: TObject);
    procedure EdValorMPRedefinido(Sender: TObject);
    procedure alo1Click(Sender: TObject);
  private
    { Private declarations }
    FForcaIMEI, FUltGGX: Integer;
    //
    procedure ReopenSrcNivel2();
    procedure ReopenTXPallet();
    procedure ReopenTXAjsIts(Controle: Integer);
    procedure SetaUltimoGGX();
    procedure CalculaCustos();
    procedure PreencheDadosIMEI(IMEI: Integer);

  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO: Integer;
    FPreco: Double;
  end;

  var
  FmTXAjsIts: TFmTXAjsIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  TXAjsCab, Principal, UnTX_PF, ModuleGeral, AppListas, UnGrade_PF, UnTX_Jan;

{$R *.DFM}

procedure TFmTXAjsIts.alo1Click(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  VAR_IMEI_SEL := 0;
  Localizou := TX_Jan.MostraFormTXTalPsq(emidAjuste, [], stPsq, Codigo, Controle,
  True);
  if VAR_IMEI_SEL <> 0 then
    PreencheDadosIMEI(VAR_IMEI_SEL);
  VAR_IMEI_SEL := 0;
end;

procedure TFmTXAjsIts.BtOKClick(Sender: TObject);
const
  ExigeFornecedor = True;
  CliVenda   = 0;
  MovimTwn   = 0;
  QtdGer     = 0;
  AptoUso    = 0;
  //
  TpCalcAuto = -1;
  //
  EdPalletMP = nil;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  ItemNFe     = 0;
  TXMulFrnCab = 0;
  //
  QtdAnt      = 0;
  //
  MovCodPai: Integer = 0;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, ClienteMO, GraGruX, Terceiro, Talao,
  GraGruY, FornecMO, SerieTal, StqCenLoc, ReqMovEstq, ClientMO: Integer;
  Qtde, CustoMOUni, CustoMOTot, ValorMP, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  EdPalletX: TdmkEdit;
  PodePallet: Boolean;
  //
  SrcMovID, DstMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2, DstNivel1, DstNivel2, SrcGGX, DstGGX, SrcMoviNiv,
  DstMoviNiv: Integer;
  MyEdTalao: TdmkEdit;
  Qry: TmySQLQuery;
  EstqMovWay: TEstqMovWay;
  IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha: Integer;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClienteMO      := FClientMO;
  ClientMO       := FClientMO;
  Terceiro       := EdFornecedor.ValueVariant;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidInventario;
  MovimNiv       := eminSemNiv;
  Pallet         := EdPallet.ValueVariant;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Qtde           := EdQtde.ValueVariant;
  CustoMOUni     := EdCustoMOUni.ValueVariant;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  ValorMP        := EdValorMP.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  Talao          := EdTalao.ValueVariant;
  GraGruY        := QrGraGruXGraGruY.Value;
  FornecMO       := EdFornecMO.ValueVariant;
  SerieTal       := EdSerieTal.ValueVariant;
  Marca          := EdMarca.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  IxxMovIX       := TEstqMovInfo(RGIxxMovIX.ItemIndex);
  IxxFolha       := EdIxxFolha.ValueVariant;
  IxxLinha       := EdIxxLinha.ValueVariant;
  //
  case GraGruY of
    CO_GraGruY_1024_TXCadNat,
    CO_GraGruY_2048_TXCadInd:
    begin
      EdPalletX := nil;
    end;
    CO_GraGruY_4096_TXCadInt,
    CO_GraGruY_6144_TXCadFcc:
    begin
      EdPalletX := EdPallet;
    end;
    else
    begin
      Geral.MB_Erro('"GraGruY" n�o implementado em "FmTXAjsIts.BtOKClick()"');
    end;
  end;
  //
  if RGMovimentacao.ItemIndex <> 1 then
  begin
    MyEdTalao := EdTalao;
    if TX_PF.TalaoErro(EdSerieTal, Empresa, Controle, Talao, True) then
      Exit;
  end else
    MyEdTalao := nil;
  //
  //
  if Pallet <> 0 then
  begin
    PodePallet :=
      (QrGraGruXGraGruY.Value = CO_GraGruY_6144_TXCadFcc)
    or
      (QrGraGruXGraGruY.Value = CO_GraGruY_4096_TXCadInt);
    //
    if MyObjects.FIC(not PodePallet, nil,
    'Somente artigo classificado ou acabado permite informa��o de pallet!') then
      Exit;
    //
    //
    if MyObjects.FIC(GraGruX <> QrTXPalletGraGruX.Value, EdPallet,
    'Artigo do pallet selecionado difere do artigo selecionado!') then
      Exit;
  end;
  //
  if MyObjects.FIC(FornecMO = 0, EdFornecMO,
  'Informe o fornecedor da m�o de obra!') then
    Exit;
  //
  if RGMovimentacao.ItemIndex = 1 then
  begin
    if Qtde >=0 then
      if Geral.MB_Pergunta(
      'Foi informado "Movimenta��o = Baixa", mas a quantidade de pe�as n�o � negativa.'
      + sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
      Exit;
  end;
  if TX_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha) then
    Exit;
  if MyObjects.FIC_MCI(CkContinuar, RGModoContinuarInserindo) then
    Exit;
  //
  if TX_PF.TXFic(GraGruX, Empresa, Terceiro, Pallet, Qtde,
  ValorT, EdGraGruX, EdPalletX, EdQtde, EdValorT, ExigeFornecedor,
  GraGruY, EdStqCenLoc, SerieTal, Talao, EdSerieTal, EdTalao)
  then
    Exit;
  //
  SrcMovID   := TEstqMovimID(0);
  SrcNivel1  := 0;
  SrcNivel2  := 0;
  SrcGGX     := 0;
  SrcMoviNiv := 0;
  //
  DstMovID   := TEstqMovimID(0);
  DstNivel1  := 0;
  DstNivel2  := 0;
  DstGGX     := 0;
  DstMoviNiv := 0;
  //if QrSrcNivel2.State <> dsInactive then
  if PnIMEIOrigem.Visible then
  begin
    if MyObjects.FIC(EdNivel2.ValueVariant = 0, EdNivel2, 'Informe o IME-I de baixa!') then
      Exit;
    EstqMovWay := TEstqMovWay.emwyNon;
    case TEstqMovimID(QrSrcNivel2MovimID.Value) of
      (*00*)emidAjuste,
      (*01*)emidCompra,
      (*02*)emidVenda,
      (*03*)emidReclasWE,
      (*04*)emidBaixa,
      (*05*)emidIndsWE,
      (*06*)emidIndsXX,
      (*07*)emidClassArtXXUni,
      (*08*)emidReclasXXUni,
      (*09*)emidForcado,
      (*10*)emidSemOrigem,
      (*12*)emidResiduoReclas,
      (*13*)emidInventario,
      (*14*)emidClassArtXXMul,
      (*15*)emidPreReclasse,
      (*16*)emidEntradaPlC,
      (*17*)emidExtraBxa,
      (*18*)emidSaldoAnterior,
      (*20*)emidFinished,
      (*21*)emidDevolucao,
      (*22*)emidRetrabalho,
      (*23*)emidGeraSubProd,
      (*24*)emidReclasXXMul,
      (*25*)emidTransfLoc,
      (*28*)emidDesclasse,
      (*29*)emidCaleado,
      (*30*)emidEmRibPDA,
      (*31*)emidEmRibDTA: EstqMovWay := TEstqMovWay.emwySrc;
      //
      (*11*)emidEmOperacao,
      (*19*)emidEmProcWE,
      (*26*)emidEmProcCal,
      (*27*)emidEmProcCur,
      (*32*)emidEmProcSP,
      (*33*)emidEmReprRM: EstqMovWay := TEstqMovWay.emwyDst;
      //
      else
      begin
        Geral.MB_Erro(
          '"EstqMovWay" indefinido em "OK de item ajuste". Avise a Dermatek!');
        Exit;
      end;
    end;
    case EstqMovWay of
      TEstqMovWay.emwySrc:
      begin
        SrcNivel2  := EdNivel2.ValueVariant;
        //
        SrcMovID   := TEstqMovimID(QrSrcNivel2MovimID.Value);
        SrcNivel1  := QrSrcNivel2Codigo.Value;
        SrcGGX     := QrSrcNivel2GraGruX.Value;
        SrcMoviNiv := QrSrcNivel2MovimNiv.Value;
      end;
      TEstqMovWay.emwyDst:
      begin
        SrcNivel2  := EdNivel2.ValueVariant;
        //
        SrcMovID   := TEstqMovimID(QrSrcNivel2MovimID.Value);
        SrcNivel1  := QrSrcNivel2Codigo.Value;
        SrcGGX     := QrSrcNivel2GraGruX.Value;
        SrcMoviNiv := QrSrcNivel2MovimNiv.Value;
      end;
    end;
  end;
////////////////////////////////////////////////////////////////////////////////
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos,
    ImgTipo.SQLType, Controle);
  //
  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
  CliVenda, Controle, CustoMOUni, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGer,
  AptoUso, FornecMO, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAnt, SerieTal, Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei055(*Ajuste de estoque*)) then
  begin
    SBNewPallet.Enabled := True;
    FForcaIMEI := 0;
    TX_PF.AtualizaTotaisTXXxxCab('txajscab', MovimCod);
    TX_PF.AtualizaSaldoIMEI(Controle, True);
    FmTXAjsCab.LocCod(Codigo, Codigo);
    FmTXAjsCab.ReopenTXAjsIts(Controle);
    if SrcNivel2 <> 0 then
      TX_PF.AtualizaSaldoVirtualTXMovIts_Generico(SrcNivel2, Integer(SrcMovID),
      SrcMoviNiv);
    if DstNivel2 <> 0 then
      TX_PF.AtualizaSaldoVirtualTXMovIts_Generico(DstNivel2, Integer(DstMovID),
      DstMoviNiv);

    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      if not TX_PF.ConfigContinuarInserindoFolhaLinha(
        RGModoContinuarInserindo, EdIxxFolha, EdIxxLinha) then
      begin
        EdGraGruX.ValueVariant     := 0;
        CBGraGruX.KeyValue         := Null;
        EdTalao.ValueVariant       := 0;
        EdPallet.ValueVariant      := 0;
        CBPallet.KeyValue          := Null;
        RGIxxMovIX.ItemIndex       := 0;
        EdIxxFolha.ValueVariant    := 0;
        EdIxxLinha.ValueVariant    := 0;
      end;
      EdValorT.ValueVariant      := 0;
      EdCustoMOTot.ValueVariant  := 0;
      EdQtde.ValueVariant        := 0;
      EdCustoMOUni.ValueVariant  := 0;
      EdObserv.Text              := '';
      //
      EdValorT.ValueVariant      := 0;
      EdValorMP.ValueVariant     := 0;
      EdCustoMOTot.ValueVariant  := 0;
      //
      SBNewPallet.Enabled := True;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmTXAjsIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXAjsIts.CalculaCustos();
var
  ValorMP, CustoMOUni, CustoMOTot, ValorT, Qtde: Double;
begin
  ValorMP      := EdValorMP.ValueVariant;
  CustoMOUni   := EdCustoMOUni.ValueVariant;
  Qtde         := EdQtde.ValueVariant;
  //
  CustoMOTot := CustoMOUni * Qtde;
  //
  EdCustoMOTot.ValueVariant := CustoMOTot;
  ValorT := ValorMP + CustoMOTot;
  EdValorT.ValueVariant := ValorT;
end;

procedure TFmTXAjsIts.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmTXAjsIts.EdCustoMOUniChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmTXAjsIts.EdCustoMOUniRedefinido(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmTXAjsIts.EdTalaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Talao: Integer;
begin
  if Key = VK_F4 then
    if TX_PF.ObtemProximoTalao(FEmpresa, EdSerieTal, Talao) then
      EdTalao.ValueVariant := Talao;
end;

procedure TFmTXAjsIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmTXAjsIts.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenSrcNivel2();
end;

procedure TFmTXAjsIts.EdPalletRedefinido(Sender: TObject);
begin
  ReopenSrcNivel2();
end;

procedure TFmTXAjsIts.EdQtdeRedefinido(Sender: TObject);
begin
  ReopenSrcNivel2();
  //
  CalculaCustos();
end;

procedure TFmTXAjsIts.EdPesoKgChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmTXAjsIts.EdValorMPChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmTXAjsIts.EdValorMPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Preco_TXT: String;
  Qtde: Double;
begin
  if (Key = VK_F4) then
  begin
    if InputQuery('Pre�o', 'Informe o pre�o', Preco_TXT) then
    begin
      FPreco := Geral.DMV(Preco_TXT);
      Qtde   := EdQtde.ValueVariant;
      EdValorMP.ValueVariant := Qtde * FPreco;
    end;
  end;
end;

procedure TFmTXAjsIts.EdValorMPRedefinido(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmTXAjsIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmTXAjsIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  FForcaIMEI := 0;
  FPreco := 0;
  //
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' + CO_GraGruY_ALL_TX + ') ');
  ReopenTXPallet();
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  TX_PF.AbreTXSerTal(QrTXSerTal);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmTXAjsIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXAjsIts.IMEI1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  TX_Jan.MostraFormTXMovIts(EdNivel2.ValueVariant);
  if VAR_CADASTRO <> 0 then
  begin
    if Geral.MB_Pergunta('Confirma a sele��o do IME-I ' + Geral.FF0(
    VAR_CADASTRO) + '?') = ID_YES then
      PreencheDadosIMEI(VAR_CADASTRO);
  end;
  VAR_CADASTRO := 0;
end;

procedure TFmTXAjsIts.Pallet1Click(Sender: TObject);
begin
  VAR_IMEI_SEL := 0;
  TX_Jan.MostraFormTXPallet(0);
  if VAR_IMEI_SEL <> 0 then
    PreencheDadosIMEI(VAR_IMEI_SEL);
  VAR_IMEI_SEL := 0;
end;

procedure TFmTXAjsIts.PreencheDadosIMEI(IMEI: Integer);
var
  Qry: TmySQLQuery;
  //
  function ValorMP(): Double;
  var
    SdoVrtQtd, ValorT, Qtde: Double;
  begin
    Result    := 0;
    ValorT    := Qry.FieldByName('ValorT').AsFloat;
    Qtde      := Qry.FieldByName('Qtde').AsFloat;
    SdoVrtQtd := -EdQtde.ValueVariant;
    //
    if Qtde <> 0 then
      Result := SdoVrtQtd * (ValorT / Qtde)
    else
      Result := 0;
    Result := -Result;
  end;
begin
  FForcaIMEI := IMEI;
  if IMEI = 0 then Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_XMI,
    'WHERE Controle=' + Geral.FF0(IMEI),
    '']);
    if Qry.RecordCount > 0 then
    begin
      ReopenSrcNivel2();
      EdNivel2.ValueVariant     := FForcaIMEI;
      CBNivel2.KeyValue         := FForcaIMEI;
      //
      EdGraGruX.ValueVariant    := Qry.FieldByName('GraGruX').AsInteger;
      CBGraGruX.KeyValue        := Qry.FieldByName('GraGruX').AsInteger;
      //
      EdSerieTal.ValueVariant   := Qry.FieldByName('SerieTal').AsInteger;
      CBSerieTal.KeyValue       := Qry.FieldByName('SerieTal').AsInteger;
      EdTalao.ValueVariant      := Qry.FieldByName('Talao').AsInteger;
      //
      EdPallet.ValueVariant     := Qry.FieldByName('Pallet').AsInteger;
      CBPallet.KeyValue         := Qry.FieldByName('Pallet').AsInteger;
      //
      EdQtde.ValueVariant       := -Qry.FieldByName('SdoVrtQtd').AsFloat;
      EdValorMP.ValueVariant    := ValorMP();
      //
      EdFornecedor.ValueVariant := Qry.FieldByName('Terceiro').AsInteger;
      CBFornecedor.KeyValue     := Qry.FieldByName('Terceiro').AsInteger;
      //
      EdFornecMO.ValueVariant   := Qry.FieldByName('FornecMO').AsInteger;
      CBFornecMO.KeyValue       := Qry.FieldByName('FornecMO').AsInteger;
      //
      EdStqCenLoc.ValueVariant  := Qry.FieldByName('StqCenLoc').AsInteger;
      CBStqCenLoc.KeyValue      := Qry.FieldByName('StqCenLoc').AsInteger;
      //
      EdGraGruX.SetFocus;
      // ????
      EdNivel2.ValueVariant     := FForcaIMEI;
      CBNivel2.KeyValue         := FForcaIMEI;
      //
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmTXAjsIts.RGMovimentacaoClick(Sender: TObject);
begin
  PnIMEIOrigem.Visible := RGMovimentacao.ItemIndex = 1;
  GBDados.Visible := RGMovimentacao.ItemIndex > 0;
end;

procedure TFmTXAjsIts.ReopenSrcNivel2();
var
  GraGruY, GraGruX, Pallet: Integer;
  SQL_IMEI: String;
  IMEISel: Integer;
begin
  QrSrcNivel2.Close;
  //
  GraGruX := EdGraGruX.ValueVariant;
  if (GraGruX <> 0) or (FForcaIMEI <> 0) then
  begin
    GraGruY := QrGraGruXGraGruY.Value;
    Pallet  := EdPallet.ValueVariant;
    if (EdQtde.ValueVariant < 0) or (FForcaIMEI <> 0) then
    begin
      if FForcaIMEI = 0 then
        SQL_IMEI := ''
      else
        SQL_IMEI := 'OR (tmi.Controle=' + Geral.FF0(FForcaIMEI) + ')';
      //
      IMEISel := EdNivel2.ValueVariant;
      UnDmkDAC_PF.AbreMySQLQuery0(QrSrcNivel2, Dmod.MyDB, [
      'SELECT tmi.Controle, tmi.Codigo, tmi.MovimID,  ',
      'tmi.MovimNiv, tmi.GraGruX, CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
      'NO_PRD_TAM_COR',
      'FROM txmovits tmi',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=tmi.GraGruX',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
      'WHERE (tmi.Qtde>0 ',
      'AND tmi.GraGruX=' + Geral.FF0(GraGruX),
      'AND tmi.Pallet=' + Geral.FF0(Pallet),
      ')',
      SQL_IMEI,
      'ORDER BY tmi.Controle DESC',
      '']);
      //Geral.MB_SQL(Self, QrSrcNivel2);
      if IMEISel <> 0 then
      begin
        if QrSrcNivel2.Locate('Controle', IMEISel, []) then
        begin
          EdNivel2.ValueVariant := IMEISel;
          CBNivel2.KeyValue     := IMEISel;
        end;
      end;
    end;
  end;
end;

procedure TFmTXAjsIts.ReopenTXAjsIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTXAjsIts.ReopenTXPallet();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXPallet, Dmod.MyDB, [
  'SELECT pla.Codigo, pla.GraGruX, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), ',
  '" ", pla.Nome)  ',
  'NO_PRD_TAM_COR  ',
  'FROM txpalleta pla  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE pla.Ativo=1  ',
  'ORDER BY NO_PRD_TAM_COR, pla.Codigo DESC ',
  '']);
end;

procedure TFmTXAjsIts.SBNewByInfoClick(Sender: TObject);
begin
  TX_PF.CadastraPalletInfo(FEmpresa, FClientMO, EdGraGruX.ValueVariant,
  QrGraGruXGraGruY.Value, QrTXPallet, EdPallet, CBPallet, SBNewPallet,
  EdQtde);
end;

procedure TFmTXAjsIts.SBNewPalletClick(Sender: TObject);
var
  Nome, DtHrEndAdd: String;
  Codigo, Empresa, ClientMO, Status, CliStat, GraGruX, MovimIDGer, QtdPrevPc: Integer;
  PodePallet: Boolean;
begin
  Codigo         := 0;
  Nome           := '';
  //(*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  Status         := 0; //EdStatus.ValueVariant;
  CliStat        := 0; //EdCliStat.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  DtHrEndAdd     := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimIDGer     := 0; // Altera depois?
  QtdPrevPc      := 0;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  //
  PodePallet :=
    (QrGraGruXGraGruY.Value = CO_GraGruY_6144_TXCadFcc)
  or
    (QrGraGruXGraGruY.Value = CO_GraGruY_4096_TXCadInt);
  //
  if MyObjects.FIC(not PodePallet, nil,
  'Somente artigo intermedi�rio ou acabado permite informa��o de pallet!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('TXPalleta', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'TXPalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'DtHrEndAdd',
  'MovimIDGer', 'QtdPrevPc', 'ClientMO'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, DtHrEndAdd,
  MovimIDGer, QtdPrevPc, ClientMO], [
  Codigo], True) then
  begin
    TX_PF.AtualizaStatPall(Codigo);
    ReopenTXPallet();
    EdPallet.ValueVariant := Codigo;
    CBPallet.KeyValue := Codigo;
    //
    SBNewPallet.Enabled := False;
    EdQtde.SetFocus;
  end;
end;

procedure TFmTXAjsIts.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  TX_Jan.MostraFormTXPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPallet, CBPallet, QrTXPallet, VAR_CADASTRO);
  EdQtde.SetFocus;
end;

procedure TFmTXAjsIts.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;


procedure TFmTXAjsIts.SbImeiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImei, SbImei);
end;

end.
