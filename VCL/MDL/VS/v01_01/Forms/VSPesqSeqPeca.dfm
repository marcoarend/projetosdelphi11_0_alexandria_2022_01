object FmVSPesqSeqPeca: TFmVSPesqSeqPeca
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-102 :: Pesquisa Sequ'#234'ncia de Pe'#231'as'
  ClientHeight = 416
  ClientWidth = 541
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 541
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 493
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 445
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 368
        Height = 32
        Caption = 'Pesquisa Sequ'#234'ncia de Pe'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 368
        Height = 32
        Caption = 'Pesquisa Sequ'#234'ncia de Pe'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 368
        Height = 32
        Caption = 'Pesquisa Sequ'#234'ncia de Pe'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 541
    Height = 254
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 541
      Height = 254
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 541
        Height = 254
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 812
        ExplicitHeight = 467
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 241
          Top = 15
          Width = 298
          Height = 237
          Align = alClient
          DataSource = DsVSPsqSeqCac
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'VSPallet'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Title.Caption = 'Pallet'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Posicao'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Title.Caption = 'Posi'#231#227'o'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Itens'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 51
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 239
          Height = 237
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitHeight = 450
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 239
            Height = 237
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitLeft = 1
            ExplicitTop = 1
            ExplicitWidth = 237
            ExplicitHeight = 204
            object Label1: TLabel
              Left = 8
              Top = 0
              Width = 58
              Height = 13
              Caption = 'Pallet inicial:'
            end
            object Label2: TLabel
              Left = 8
              Top = 44
              Width = 51
              Height = 13
              Caption = 'Pallet final:'
            end
            object Label3: TLabel
              Left = 124
              Top = 0
              Width = 40
              Height = 13
              Caption = 'Couro 1:'
            end
            object Label4: TLabel
              Left = 124
              Top = 40
              Width = 40
              Height = 13
              Caption = 'Couro 2:'
            end
            object Label5: TLabel
              Left = 124
              Top = 84
              Width = 40
              Height = 13
              Caption = 'Couro 3:'
            end
            object Label6: TLabel
              Left = 124
              Top = 124
              Width = 40
              Height = 13
              Caption = 'Couro 4:'
            end
            object Label7: TLabel
              Left = 124
              Top = 164
              Width = 40
              Height = 13
              Caption = 'Couro 5:'
            end
            object EdPalletIni: TdmkEdit
              Left = 8
              Top = 16
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdPalletFim: TdmkEdit
              Left = 8
              Top = 60
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '999999999'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 999999999
              ValWarn = False
            end
            object EdCouro1: TdmkEdit
              Left = 124
              Top = 16
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '4,09'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 4.090000000000000000
              ValWarn = False
            end
            object EdCouro2: TdmkEdit
              Left = 124
              Top = 56
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '4,85'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 4.850000000000000000
              ValWarn = False
            end
            object EdCouro3: TdmkEdit
              Left = 124
              Top = 100
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '4,95'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 4.950000000000000000
              ValWarn = False
            end
            object EdCouro4: TdmkEdit
              Left = 124
              Top = 140
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '4,81'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 4.810000000000000000
              ValWarn = False
            end
            object EdCouro5: TdmkEdit
              Left = 124
              Top = 180
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '4,87'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 4.870000000000000000
              ValWarn = False
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 302
    Width = 541
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 537
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 346
    Width = 541
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 395
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 393
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn1: TBitBtn
        Tag = 12
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Parar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn1Click
      end
    end
  end
  object QrVsCacItsA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, AreaM2'
      ' FROM vscacitsa')
    Left = 388
    Top = 124
    object QrVsCacItsAAreaM2: TFloatField
      FieldName = 'AreaM2'
      Origin = 'vscacitsa.AreaM2'
    end
    object QrVsCacItsAControle: TLargeintField
      FieldName = 'Controle'
      Origin = 'vscacitsa.Controle'
    end
  end
  object QrVSPsqSeqCac: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 296
    Top = 120
    object QrVSPsqSeqCacVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSPsqSeqCacPosicao: TIntegerField
      FieldName = 'Posicao'
    end
    object QrVSPsqSeqCacItens: TIntegerField
      FieldName = 'Itens'
    end
  end
  object DsVSPsqSeqCac: TDataSource
    DataSet = QrVSPsqSeqCac
    Left = 296
    Top = 168
  end
  object DsVsCacItsA: TDataSource
    DataSet = QrVsCacItsA
    Left = 388
    Top = 176
  end
  object QrCIA: TMySQLQuery
    Database = Dmod.MyDB
    Left = 408
    Top = 256
    object QrCIAAreaM2: TFloatField
      FieldName = 'AreaM2'
      Origin = 'vscacitsa.AreaM2'
    end
    object QrCIAControle: TLargeintField
      FieldName = 'Controle'
      Origin = 'vscacitsa.Controle'
    end
  end
end
