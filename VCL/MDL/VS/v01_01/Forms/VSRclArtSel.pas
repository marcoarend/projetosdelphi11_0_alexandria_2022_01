unit VSRclArtSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnAppEnums;

type
  TFmVSRclArtSel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnPartida: TPanel;
    LaVSRibCad: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    QrVSPaRclCab: TmySQLQuery;
    QrVSPaRclCabCodigo: TIntegerField;
    QrVSPaRclCabCacCod: TIntegerField;
    QrPallet: TmySQLQuery;
    DsPallet: TDataSource;
    QrPalletVSPallet: TIntegerField;
    QrPalletCacCod: TIntegerField;
    QrPalletNO_PRD_TAM_COR: TWideStringField;
    Label35: TLabel;
    EdVSPwdDdClas: TEdit;
    CkSomentePositivos: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CkSomentePositivosClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenAbertos();
  public
    { Public declarations }
    FCodigo, FCacCod, FStqCenLoc, FPallet: Integer;
    FMovimID: TEstqMovimID;
  end;

  var
  FmVSRclArtSel: TFmVSRclArtSel;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSRclArtSel.BtOKClick(Sender: TObject);
var
  //VSGerRcl,
  CacCod: Integer;
begin
  FPallet := EdPallet.ValueVariant;
  if MyObjects.FIC(FPallet = 0, EdPallet, 'Informe o Pallet!') then
    Exit;
  if VS_CRC_PF.SenhaVSPwdDdNaoConfere(EdVSPwdDdClas.Text) then
    Exit;
  //VSGerRcl := QrVSPalletGerRclCab.Value;
  CacCod := QrPalletCacCod.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaRclCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsparclcaba ',
  //'WHERE VSGerRcl=' + Geral.FF0(VSGerRcl),
  'WHERE CacCod=' + Geral.FF0(CacCod),
  'AND VSPallet=' + Geral.FF0(FPallet),
  '']);
  //
  FCodigo    := 0;
  FCacCod    := 0;
  //FStqCenLoc := 0;
  case QrVSPaRclCab.RecordCount of
    0: Geral.MB_Aviso('Configuração de reclassificação não localizada!');
    1:
    begin
      FCodigo    := QrVSPaRclCabCodigo.Value;
      FCacCod    := QrVSPaRclCabCacCod.Value;
      //FStqCenLoc := QrVSPaRclCabStqCenLoc.Value;
      FMovimID   := emidReclasXXUni;
      //
    end;
    else Geral.MB_Aviso('Foram localizadas ' + Geral.FF0(QrVSPaRclCab.RecordCount) +
    ' configurações de reclassificação!' + sLineBreak +
    'Por segurança nenhuma será ulilizada' + sLineBreak +
    'Avise a DERMATEK!');
  end;
  if FCodigo <> 0 then
    Close;
end;

procedure TFmVSRclArtSel.BtSaidaClick(Sender: TObject);
begin
  FCodigo    := 0;
  FCacCod    := 0;
  FStqCenLoc := 0;
  //FMovimID := 0;
  Close;
end;

procedure TFmVSRclArtSel.CkSomentePositivosClick(Sender: TObject);
begin
  ReopenAbertos();
end;

procedure TFmVSRclArtSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSRclArtSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo    := 0;
  FCacCod    := 0;
  FStqCenLoc := 0;
  //
  ReopenAbertos();
end;

procedure TFmVSRclArtSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRclArtSel.ReopenAbertos;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallet, Dmod.MyDB, [
  'SELECT VSPallet, prc.CacCod,',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), ',
  '" ( Pallet ", vsp.Codigo, " )", ',
  '" ( OC ", prc.CacCod, " )",',
  'IF(vsp.Nome <> "", CONCAT(" (", vsp.Nome, ")"), ""))  ',
  'NO_PRD_TAM_COR ',
  'FROM vsparclcaba prc',
  'LEFT JOIN vspalleta vsp ON vsp.Codigo=prc.VSPallet',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vsp.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE DtHrFimCla < "1900-01-01"',
  'ORDER BY NO_PRD_TAM_COR, prc.CacCod ',
  '']);
end;

end.
