object FmVSMovItbAdd: TFmVSMovItbAdd
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-119 :: Adi'#231#227'o ao Arquivo Morto VS'
  ClientHeight = 354
  ClientWidth = 574
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 574
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 526
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 478
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 340
        Height = 32
        Caption = 'Adi'#231#227'o ao Arquivo Morto VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 340
        Height = 32
        Caption = 'Adi'#231#227'o ao Arquivo Morto VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 340
        Height = 32
        Caption = 'Adi'#231#227'o ao Arquivo Morto VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 574
    Height = 192
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 574
      Height = 192
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 574
        Height = 192
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 570
          Height = 175
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 570
            Height = 61
            Align = alTop
            Caption = ' Configura'#231#227'o: '
            TabOrder = 0
            object Panel6: TPanel
              Left = 2
              Top = 15
              Width = 566
              Height = 44
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 12
                Top = 4
                Width = 52
                Height = 13
                Caption = 'Data limite:'
              end
              object SpeedButton1: TSpeedButton
                Left = 200
                Top = 20
                Width = 23
                Height = 22
                Caption = '...'
                OnClick = SpeedButton1Click
              end
              object Label2: TLabel
                Left = 228
                Top = 4
                Width = 55
                Height = 13
                Caption = 'IMEI Limite:'
              end
              object TPDataLimite: TdmkEditDateTimePicker
                Left = 12
                Top = 20
                Width = 186
                Height = 21
                Date = 45030.000000000000000000
                Time = 0.611485474539222200
                Enabled = False
                TabOrder = 0
                OnClick = TPDataLimiteChange
                OnChange = TPDataLimiteChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object EdIMEILimite: TdmkEdit
                Left = 228
                Top = 20
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 61
            Width = 570
            Height = 112
            Align = alTop
            Caption = ' Informa'#231#245'es: '
            TabOrder = 1
            object Panel7: TPanel
              Left = 2
              Top = 15
              Width = 566
              Height = 95
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label3: TLabel
                Left = 172
                Top = 4
                Width = 74
                Height = 13
                Caption = 'IMEIs afetados:'
              end
              object Label4: TLabel
                Left = 256
                Top = 4
                Width = 72
                Height = 13
                Caption = 'CaCs afetados:'
              end
              object Label5: TLabel
                Left = 12
                Top = 48
                Width = 96
                Height = 13
                Caption = 'Registros copiados: '
              end
              object Label6: TLabel
                Left = 12
                Top = 72
                Width = 99
                Height = 13
                Caption = 'Registros exclu'#237'dos: '
              end
              object Label7: TLabel
                Left = 12
                Top = 24
                Width = 112
                Height = 13
                Caption = 'Registros pesquisados: '
              end
              object EdIMEIsCopiados: TdmkEdit
                Left = 172
                Top = 44
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                ReadOnly = True
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdCaCsCopiados: TdmkEdit
                Left = 256
                Top = 44
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdIMEIsExcluidos: TdmkEdit
                Left = 172
                Top = 68
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdCaCsExcluidos: TdmkEdit
                Left = 256
                Top = 68
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdIMEIsPesquisados: TdmkEdit
                Left = 172
                Top = 20
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                ReadOnly = True
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdCaCsPesquisados: TdmkEdit
                Left = 256
                Top = 20
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                ReadOnly = True
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 240
    Width = 574
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 570
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 284
    Width = 574
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 428
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 426
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 18
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pequisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtExecuta: TBitBtn
        Tag = 10034
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Executa'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtExecutaClick
      end
      object BtAtualiza: TBitBtn
        Tag = 20
        Left = 256
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Atualiza'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAtualizaClick
      end
    end
  end
  object QrIMEIs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 356
    Top = 88
  end
  object QrCaCs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 356
    Top = 136
  end
end
