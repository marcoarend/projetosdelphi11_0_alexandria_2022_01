object FmVSGerArtMarcaImpBar: TFmVSGerArtMarcaImpBar
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-244 :: Gera'#231#227'o de Artigos por Marca - BAR'
  ClientHeight = 563
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 389
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 389
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 149
        Height = 389
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 149
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object CkMarcas: TCheckBox
            Left = 29
            Top = 2
            Width = 49
            Height = 17
            Caption = 'Marcas:'
            TabOrder = 0
          end
        end
        object MeMarcas: TMemo
          Left = 0
          Top = 21
          Width = 149
          Height = 368
          Align = alClient
          Lines.Strings = (
            'CH100323'
            'CH110323')
          TabOrder = 1
        end
      end
      object Panel8: TPanel
        Left = 205
        Top = 0
        Width = 803
        Height = 389
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 803
          Height = 145
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object SbMesAnt: TSpeedButton
            Left = 252
            Top = 12
            Width = 23
            Height = 23
            Caption = '<'
            OnClick = SbMesAntClick
          end
          object SpeedButton2: TSpeedButton
            Left = 252
            Top = 40
            Width = 23
            Height = 23
            Caption = '>'
            OnClick = SpeedButton2Click
          end
          object GroupBox2: TGroupBox
            Left = 4
            Top = 1
            Width = 245
            Height = 69
            Caption = 'Data / hora chegada:'
            TabOrder = 0
            object TPEntradaIni: TdmkEditDateTimePicker
              Left = 8
              Top = 40
              Width = 112
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.000000000000000000
              Time = 0.777157974502188200
              TabOrder = 1
              OnClick = TPEntradaIniClick
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
              OnRedefInPlace = TPEntradaIniRedefInPlace
            end
            object CkEntradaIni: TCheckBox
              Left = 8
              Top = 20
              Width = 112
              Height = 17
              Caption = 'Data inicial'
              Checked = True
              State = cbChecked
              TabOrder = 0
              OnClick = CkEntradaIniClick
            end
            object CkEntradaFim: TCheckBox
              Left = 124
              Top = 20
              Width = 112
              Height = 17
              Caption = 'Data final'
              Checked = True
              State = cbChecked
              TabOrder = 2
              OnClick = CkEntradaFimClick
            end
            object TPEntradaFim: TdmkEditDateTimePicker
              Left = 124
              Top = 40
              Width = 112
              Height = 21
              Date = 37636.000000000000000000
              Time = 0.777203761601413100
              TabOrder = 3
              OnClick = TPEntradaFimClick
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
              OnRedefInPlace = TPEntradaFimRedefInPlace
            end
          end
          object CkTemIMEIMrt: TCheckBox
            Left = 8
            Top = 72
            Width = 149
            Height = 17
            Caption = 'Incluir arquivo morto.'
            Checked = True
            State = cbChecked
            TabOrder = 1
            OnClick = CkTemIMEIMrtClick
          end
          object GroupBox3: TGroupBox
            Left = 280
            Top = 1
            Width = 521
            Height = 144
            Caption = ' Impress'#227'o: '
            TabOrder = 2
            object Panel11: TPanel
              Left = 2
              Top = 84
              Width = 517
              Height = 58
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitTop = 15
              object RGFonte: TRadioGroup
                Left = 0
                Top = 0
                Width = 323
                Height = 58
                Align = alLeft
                Caption = ' Fonte: '
                Columns = 9
                ItemIndex = 3
                Items.Strings = (
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9'
                  '10'
                  '11'
                  '12')
                TabOrder = 0
                ExplicitLeft = 2
                ExplicitTop = 15
                ExplicitHeight = 52
              end
              object RGSepGrup: TRadioGroup
                Left = 323
                Top = 0
                Width = 194
                Height = 58
                Align = alClient
                Caption = ' Separador para agrupar marcas*:'
                Columns = 2
                Enabled = False
                Items.Strings = (
                  'Espa'#231'o'
                  'Tra'#231'o -'
                  'Barra /'
                  'Barra \')
                TabOrder = 1
                ExplicitLeft = 325
                ExplicitTop = 15
                ExplicitHeight = 52
              end
            end
            object RGRelatorio: TRadioGroup
              Left = 2
              Top = 15
              Width = 517
              Height = 69
              Align = alClient
              Caption = ' Relat'#243'rio: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'N'#227'o definido'
                'Agrupar por mat'#233'ria prima'
                'Agrupar por grupo de marcas*'
                'Agrupar por dia')
              TabOrder = 1
              OnClick = RGRelatorioClick
              ExplicitLeft = 4
              ExplicitTop = 79
              ExplicitWidth = 421
              ExplicitHeight = 65
            end
          end
          object RadioGroup1: TRadioGroup
            Left = 4
            Top = 96
            Width = 245
            Height = 49
            Caption = ' Origem do artigo gerado:'
            Columns = 3
            Enabled = False
            ItemIndex = 0
            Items.Strings = (
              'N/D'
              'Barrca'
              'Curtimento')
            TabOrder = 3
          end
        end
        object PCPesquisas: TPageControl
          Left = 0
          Top = 145
          Width = 803
          Height = 244
          ActivePage = TabSheet2
          Align = alClient
          TabOrder = 1
          ExplicitTop = 181
          ExplicitHeight = 208
          object TabSheet1: TTabSheet
            Caption = 'Pesquisa'
            object DBGGafid: TDBGrid
              Left = 0
              Top = 0
              Width = 795
              Height = 144
              Align = alClient
              DataSource = DsGafid
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
            object DBGrid2: TDBGrid
              Left = 0
              Top = 144
              Width = 795
              Height = 117
              Align = alBottom
              DataSource = DsClasses
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Descend'#234'ncia'
            ImageIndex = 1
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 795
              Height = 216
              Align = alClient
              DataSource = DsFaltaMarca
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBGrid1DblClick
            end
          end
        end
      end
      object Panel9: TPanel
        Left = 149
        Top = 0
        Width = 56
        Height = 389
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 56
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object CkLetras: TCheckBox
            Left = 1
            Top = 2
            Width = 49
            Height = 17
            Caption = 'Letras:'
            TabOrder = 0
          end
        end
        object MeLetras: TMemo
          Left = 0
          Top = 21
          Width = 56
          Height = 368
          Align = alClient
          Alignment = taCenter
          Lines.Strings = (
            'H')
          TabOrder = 1
          WordWrap = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 446
        Height = 32
        Caption = 'Gera'#231#227'o de Artigos por Marca - BAR'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 446
        Height = 32
        Caption = 'Gera'#231#227'o de Artigos por Marca - BAR'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 446
        Height = 32
        Caption = 'Gera'#231#227'o de Artigos por Marca - BAR'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 437
    Width = 1008
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 493
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 18
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BitBtn1: TBitBtn
        Left = 256
        Top = 4
        Width = 157
        Height = 40
        Caption = 'Corrigir espa'#231'os de marcas'
        TabOrder = 2
        OnClick = BitBtn1Click
      end
      object BtDescendencia: TBitBtn
        Left = 416
        Top = 4
        Width = 157
        Height = 40
        Caption = 'Descend'#234'ncia'
        Enabled = False
        TabOrder = 3
        OnClick = BtDescendenciaClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 65523
  end
  object QrExec: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 472
    Top = 4
  end
  object QrDia: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 536
    Top = 4
    object QrDiaInnPecas: TFloatField
      FieldName = 'InnPecas'
    end
  end
  object QrArtGeComodty: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT gci.ArtGeComodty, agc.Sigla, agc.VisuRel,'
      'agc.VisuRel & 1 ShowPecas, agc.VisuRel & 2 ShowPesoKg,'
      'agc.VisuRel & 4 ShowAreaM2, agc.VisuRel & 8 ShowPercPc,'
      'agc.VisuRel & 16 ShowKgM2, agc.VisuRel & 32 ShowMediaM2'
      'FROM _vs_ga_cab_e_its gci'
      
        'LEFT JOIN bluederm_ciaanterior.artgecomodty agc ON agc.Codigo=gc' +
        'i.ArtGeComodty'
      'GROUP BY ArtGeComodty'
      'ORDER BY ArtGeComOrd, ArtGeComodty ')
    Left = 496
    Top = 116
    object QrArtGeComodtyArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
      Required = True
    end
    object QrArtGeComodtySigla: TWideStringField
      DisplayWidth = 12
      FieldName = 'Sigla'
      Size = 12
    end
    object QrArtGeComodtyVisuRel: TIntegerField
      FieldName = 'VisuRel'
    end
    object QrArtGeComodtyShowPecas: TLargeintField
      FieldName = 'ShowPecas'
    end
    object QrArtGeComodtyShowPesoKg: TLargeintField
      FieldName = 'ShowPesoKg'
    end
    object QrArtGeComodtyShowAreaM2: TLargeintField
      FieldName = 'ShowAreaM2'
    end
    object QrArtGeComodtyShowPercPc: TLargeintField
      FieldName = 'ShowPercPc'
    end
    object QrArtGeComodtyShowKgM2: TLargeintField
      FieldName = 'ShowKgM2'
    end
    object QrArtGeComodtyShowMediaM2: TLargeintField
      FieldName = 'ShowMediaM2'
    end
    object QrArtGeComodtyTipoTab: TFloatField
      FieldName = 'TipoTab'
    end
  end
  object frxWET_CURTI_242_1_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44981.814375798610000000
    ReportOptions.LastChange = 44981.814375798610000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 172
    Top = 148
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
    end
  end
  object QrGafid: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT gg1.Nome  NO_GGXInn, afi.* '
      'FROM _vs_ger_art_from_in_dd afi'
      
        'LEFT JOIN bluederm_ciaanterior.gragrux ggx ON ggx.Controle=afi.G' +
        'GXInn'
      
        'LEFT JOIN bluederm_ciaanterior.gragru1 gg1 ON gg1.Nivel1=ggx.Gra' +
        'Gru1')
    Left = 50
    Top = 151
  end
  object frxDsGafid: TfrxDBDataset
    UserName = 'frxDBGafid'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_GGXInn=NO_GGXInn'
      'GGXInn=GGXInn'
      'AnoMesDiaIn=AnoMesDiaIn'
      'Data=Data'
      'InnPecas=InnPecas'
      'InnPesoKg=InnPesoKg'
      'InnKgPeca=InnKgPeca'
      'InnSdoVrtPeca=InnSdoVrtPeca'
      'InnSdoVrtPeso=InnSdoVrtPeso'
      'NFePecas=NFePecas'
      'CalPecas=CalPecas'
      'CalPesoKg=CalPesoKg'
      'CalPercCo=CalPercCo'
      'CurPecas=CurPecas'
      'CurPesoKg=CurPesoKg'
      'CurPercCo=CurPercCo'
      'Pecas_1=Pecas_1'
      'PesoKg_1=PesoKg_1'
      'AreaM2_1=AreaM2_1'
      'PercCo_1=PercCo_1'
      'RendKgM2_1=RendKgM2_1'
      'MediaM2_1=MediaM2_1'
      'Pecas_2=Pecas_2'
      'PesoKg_2=PesoKg_2'
      'AreaM2_2=AreaM2_2'
      'PercCo_2=PercCo_2'
      'RendKgM2_2=RendKgM2_2'
      'MediaM2_2=MediaM2_2'
      'Pecas_3=Pecas_3'
      'PesoKg_3=PesoKg_3'
      'AreaM2_3=AreaM2_3'
      'PercCo_3=PercCo_3'
      'RendKgM2_3=RendKgM2_3'
      'MediaM2_3=MediaM2_3'
      'Pecas_4=Pecas_4'
      'PesoKg_4=PesoKg_4'
      'AreaM2_4=AreaM2_4'
      'PercCo_4=PercCo_4'
      'RendKgM2_4=RendKgM2_4'
      'MediaM2_4=MediaM2_4'
      'Pecas_5=Pecas_5'
      'PesoKg_5=PesoKg_5'
      'AreaM2_5=AreaM2_5'
      'PercCo_5=PercCo_5'
      'RendKgM2_5=RendKgM2_5'
      'MediaM2_5=MediaM2_5'
      'Pecas_6=Pecas_6'
      'PesoKg_6=PesoKg_6'
      'AreaM2_6=AreaM2_6'
      'PercCo_6=PercCo_6'
      'RendKgM2_6=RendKgM2_6'
      'MediaM2_6=MediaM2_6')
    DataSet = QrGafid
    BCDToCurrency = False
    DataSetOptions = []
    Left = 48
    Top = 200
  end
  object QrEmids: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT DISTINCT dst.MovimID DstMovID '
      'FROM _vs_in_cab_e_its inn'
      'LEFT JOIN _vs_ga_cab_e_its dst ON '
      '  dst.SrcMovID=inn.MovimID'
      '  AND dst.SrcNivel1=inn.Codigo'
      '  AND dst.SrcNivel2=inn.Controle'
      ''
      'WHERE '
      '  dst.MovimID IS NULL'
      '  OR dst.MovimID <> 6'
      ''
      'ORDER BY DstMovID ')
    Left = 388
    Top = 248
    object QrEmidsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 60
    end
    object QrEmidsDstMovID: TLargeintField
      FieldName = 'DstMovID'
    end
  end
  object QrVSInn: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT CAST(GraGruX AS SIGNED) GGXInn, AnoMesDiaIn, '
      'DATE_FORMAT(DtEntrada, "%Y-%m-%d") Data,'
      'SUM(Pecas) InnPecas, '
      'SUM(PesoKg) InnPesoKg, '
      'SUM(PesoKg) / SUM(Pecas) InnKgPeca, '
      'SUM(InfPecas) NFePecas, SUM(Pecas) - SUM(SdoVrtPeca) CalPecas  '
      'FROM _vs_in_cab_e_its'
      'GROUP BY GGXInn, AnoMesDiaIn'
      'ORDER BY GGXInn, AnoMesDiaIn')
    Left = 496
    Top = 380
    object QrVSInnGGXInn: TLargeintField
      FieldName = 'GGXInn'
      Required = True
    end
    object QrVSInnAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
      Required = True
    end
    object QrVSInnData: TWideStringField
      FieldName = 'Data'
      Size = 10
    end
    object QrVSInnInnPecas: TFloatField
      FieldName = 'InnPecas'
    end
    object QrVSInnInnPesoKg: TFloatField
      FieldName = 'InnPesoKg'
    end
    object QrVSInnInnKgPeca: TFloatField
      FieldName = 'InnKgPeca'
    end
    object QrVSInnNFePecas: TFloatField
      FieldName = 'NFePecas'
    end
    object QrVSInnCalPecas: TFloatField
      FieldName = 'CalPecas'
    end
    object QrVSInnInnSdoVrtPeso: TFloatField
      FieldName = 'InnSdoVrtPeso'
    end
    object QrVSInnInnSdoVrtPeca: TFloatField
      FieldName = 'InnSdoVrtPeca'
    end
    object QrVSInnMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object QrNaoProc: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 300
    Top = 328
    object QrNaoProcInnSdoVrtPeca: TFloatField
      FieldName = 'InnSdoVrtPeca'
    end
  end
  object QrOrfaos: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 300
    Top = 380
    object QrOrfaosGraGruY: TIntegerField
      DisplayLabel = 'Grupo de Estoque'
      FieldName = 'GraGruY'
    end
    object QrOrfaosGraGruX: TIntegerField
      DisplayLabel = 'Reduzido'
      FieldName = 'GraGruX'
    end
    object QrOrfaosNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsOrfaos: TDataSource
    DataSet = QrOrfaos
    Left = 300
    Top = 428
  end
  object QrVSGerCalCon: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,  '
      'CAST(dst.DstGGX AS SIGNED) GraGruX, '
      'CAST(dst.MovimID AS SIGNED) DstMovID, '
      'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, '
      'dst.AnoMesDiaIn, dst.ArtGeComodty,'
      'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg,'
      
        'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2' +
        ','
      'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2,'
      'SUM(-dst.QtdGerArM2) / SUM(-dst.Pecas) MediaM2'
      'FROM _vs_ga_cab_e_its dst'
      'WHERE SrcMovID=27 /* n'#227'o pode ser 1 */'
      'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty'
      'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty')
    Left = 494
    Top = 175
    object QrVSGerCalConGGXInn: TLargeintField
      FieldName = 'GGXInn'
      Required = True
    end
    object QrVSGerCalConGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSGerCalConDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSGerCalConDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSGerCalConAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
      Required = True
    end
    object QrVSGerCalConArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
      Required = True
    end
    object QrVSGerCalConPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSGerCalConPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSGerCalConQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSGerCalConQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSGerCalConRendKgM2: TFloatField
      FieldName = 'RendKgM2'
    end
    object QrVSGerCalConMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
    object QrVSGerCalConInnPecas: TFloatField
      FieldName = 'InnPecas'
    end
    object QrVSGerCalConInnPesoKg: TFloatField
      FieldName = 'InnPesoKg'
    end
    object QrVSGerCalConMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSGerCalConMovimID: TLargeintField
      FieldName = 'MovimID'
    end
    object QrVSGerCalConFuloes: TWideMemoField
      FieldName = 'Fuloes'
      BlobType = ftWideMemo
      Size = 511
    end
  end
  object QrVSCalDst: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,   '
      'CAST(dst.DstGGX AS SIGNED) GraGruX,  '
      'CAST(dst.MovimID AS SIGNED) DstMovID,  '
      'CAST(dst.DstNivel1 AS SIGNED) DstNivel1,  '
      'dst.AnoMesDiaIn, dst.ArtGeComodty, '
      'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg, '
      
        'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2' +
        ', '
      'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2, '
      'SUM(inn.Pecas) InnPecas  '
      'FROM _vs_in_cab_e_its inn  '
      'LEFT JOIN _vs_ca_cab_e_its dst ON   '
      '  dst.SrcMovID=inn.MovimID  '
      '  AND dst.SrcNivel1=inn.Codigo  '
      '  AND dst.SrcNivel2=inn.Controle  '
      'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty '
      'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty ')
    Left = 494
    Top = 275
    object QrVSCalDstGGXInn: TLargeintField
      FieldName = 'GGXInn'
    end
    object QrVSCalDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSCalDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
    end
    object QrVSCalDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
    end
    object QrVSCalDstAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
    end
    object QrVSCalDstArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
    end
    object QrVSCalDstPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCalDstPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSCalDstMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object QrVSEmC: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,   '
      'CAST(dst.DstGGX AS SIGNED) GraGruX,  '
      'CAST(dst.MovimID AS SIGNED) DstMovID,  '
      'CAST(dst.DstNivel1 AS SIGNED) DstNivel1,  '
      'dst.AnoMesDiaIn, dst.ArtGeComodty, '
      'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg, '
      
        'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2' +
        ', '
      'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2, '
      'SUM(inn.Pecas) InnPecas  '
      'FROM _vs_in_cab_e_its inn  '
      'LEFT JOIN _vs_ca_cab_e_its dst ON   '
      '  dst.SrcMovID=inn.MovimID  '
      '  AND dst.SrcNivel1=inn.Codigo  '
      '  AND dst.SrcNivel2=inn.Controle  '
      'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty '
      'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty ')
    Left = 498
    Top = 327
    object QrVSEmCGGXInn: TLargeintField
      FieldName = 'GGXInn'
    end
    object QrVSEmCGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSEmCDstMovID: TLargeintField
      FieldName = 'DstMovID'
    end
    object QrVSEmCDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
    end
    object QrVSEmCAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
    end
    object QrVSEmCArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
    end
    object QrVSEmCPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSEmCPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSEmCMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object QrG13Err: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT DISTINCT g13.Codigo, g13.MovimCod '
      'FROM _vs_ga_cab_e_its dst '
      'LEFT JOIN bluederm_jurubeba.vsmovits g13 ON '
      '  dst.MovimID=g13.MovimID  '
      '  AND dst.Codigo=g13.Codigo  '
      '  AND g13.MovimNiv=13 '
      'WHERE g13.GraGruX<>dst.DstGGX  ')
    Left = 688
    Top = 324
    object QrG13ErrCodigo: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'Codigo'
    end
    object QrG13ErrMovimCod: TIntegerField
      DisplayLabel = 'IME-C'
      FieldName = 'MovimCod'
    end
  end
  object DsG13Err: TDataSource
    DataSet = QrG13Err
    Left = 688
    Top = 372
  end
  object QrClasses: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.GraGruX Reduzido, gg1.Nome,'
      'SUM(cia.Pecas) Pecas,'
      'SUM(cia.AreaM2) AreaM2,'
      'SUM(cia.AreaP2) AreaP2,'
      'SUM(cia.AreaM2) / 0.000000 PercM2,'
      
        'SUM(cia.Pecas) * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Intei' +
        'ros'
      'FROM bluederm_jurubeba.vscacitsa cia'
      
        'LEFT JOIN bluederm_jurubeba.vsmrtcad mrt ON mrt.Codigo=cia.Marte' +
        'lo'
      
        'LEFT JOIN bluederm_jurubeba.vsmovits vmi ON vmi.Controle=cia.VMI' +
        '_Dest'
      
        'LEFT JOIN bluederm_jurubeba.gragrux  ggx ON ggx.Controle=vmi.Gra' +
        'GruX'
      
        'LEFT JOIN bluederm_jurubeba.gragru1  gg1 ON gg1.Nivel1=ggx.GraGr' +
        'u1'
      
        'LEFT JOIN bluederm_jurubeba.gragruxcou  xco ON xco.GraGruX=ggx.C' +
        'ontrole'
      
        'LEFT JOIN bluederm_jurubeba.couniv1     cn1 ON cn1.Codigo=xco.Co' +
        'uNiv1'
      'WHERE VMI_Sorc IN ('
      '3764,3831,4319,5294,6341,6549,6647,7037,7311'
      ')'
      'AND mrt.Nome=""'
      'GROUP BY vmi.GraGruX')
    Left = 45
    Top = 308
    object QrClassesReduzido: TIntegerField
      FieldName = 'Reduzido'
    end
    object QrClassesNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrClassesPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrClassesAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrClassesAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrClassesPercM2: TFloatField
      FieldName = 'PercM2'
    end
    object QrClassesInteiros: TFloatField
      FieldName = 'Inteiros'
    end
  end
  object DsClasses: TDataSource
    DataSet = QrClasses
    Left = 45
    Top = 356
  end
  object frxDsClasses: TfrxDBDataset
    UserName = 'frxDsClasses'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Reduzido=Reduzido'
      'Nome=Nome'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'PercM2=PercM2'
      'Inteiros=Inteiros')
    DataSet = QrClasses
    BCDToCurrency = False
    DataSetOptions = []
    Left = 44
    Top = 408
  end
  object QrInnCon_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _vs_mpc_cab_e_it_src_con;'
      'CREATE TABLE _vs_mpc_cab_e_it_src_con'
      'SELECT Controle, SrcNivel2'
      'FROM vsmovits'
      'WHERE Controle IN (10871, 10967)'
      ';'
      'SELECT cns.*, inn.DataHora,'
      'DATE_FORMAT(inn.DataHora, "%d/%m/%Y") DtEntrada_TXT, '
      'YEAR(inn.DataHora)*10000 + MONTH(inn.DataHora)*100 + '
      'DAY(inn.DataHora) AnoMesDiaIn '
      ''
      'FROM _vs_mpc_cab_e_it_src_con cns'
      'LEFT JOIN bluederm.vsmovits inn ON inn.Controle=cns.SrcNivel2')
    Left = 865
    Top = 168
    object QrInnCon_Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrInnCon_SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrInnCon_DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrInnCon_DtEntrada_TXT: TWideStringField
      FieldName = 'DtEntrada_TXT'
      Size = 10
    end
    object QrInnCon_AnoMesDiaIn: TLargeintField
      FieldName = 'AnoMesDiaIn'
    end
  end
  object QrVSMpC: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,   '
      'CAST(dst.DstGGX AS SIGNED) GraGruX,  '
      'CAST(dst.MovimID AS SIGNED) DstMovID,  '
      'CAST(dst.DstNivel1 AS SIGNED) DstNivel1,  '
      'dst.AnoMesDiaIn, dst.ArtGeComodty, '
      'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg, '
      
        'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2' +
        ', '
      'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2, '
      'SUM(inn.Pecas) InnPecas  '
      'FROM _vs_in_cab_e_its inn  '
      'LEFT JOIN _vs_ca_cab_e_its dst ON   '
      '  dst.SrcMovID=inn.MovimID  '
      '  AND dst.SrcNivel1=inn.Codigo  '
      '  AND dst.SrcNivel2=inn.Controle  '
      'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty '
      'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty ')
    Left = 574
    Top = 275
    object QrVSMpCGGXInn: TLargeintField
      FieldName = 'GGXInn'
    end
    object QrVSMpCGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSMpCDstMovID: TLargeintField
      FieldName = 'DstMovID'
    end
    object QrVSMpCDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
    end
    object QrVSMpCAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
    end
    object QrVSMpCArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
    end
    object QrVSMpCPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSMpCPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSMpCMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object Query: TMySQLQuery
    Database = Dmod.MyDB
    Left = 717
    Top = 272
  end
  object QrInnNat_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.Controle, GraGruX, Marca,  '
      'DATE_FORMAT(cab.DtEntrada, "%d/%m/%Y") DtEntrada_TXT, '
      'YEAR(cab.DtEntrada)*10000 + MONTH(cab.DtEntrada)*100 + '
      'DAY(cab.DtEntrada) AnoMesDiaIn '
      'FROM vsmovits vmi'
      'LEFT JOIN vsinncab cab ON cab.MovimCod=vmi.MovimCod'
      
        'WHERE vmi.Controle IN (10849, 10850, 10855, 10872, 10889, 10896,' +
        ' 10904, '
      '10905, 10916, 11003, 11004, 11012, 11021, 11077, 11082, 11083, '
      
        '11143, 11183, 11184, 11218, 11235, 11269, 11292, 11321, 11332, 1' +
        '1344)')
    Left = 865
    Top = 216
    object QrInnNat_Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrInnNat_GraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrInnNat_Marca: TWideStringField
      FieldName = 'Marca'
    end
    object QrInnNat_DtEntrada_TXT: TWideStringField
      FieldName = 'DtEntrada_TXT'
      Size = 10
    end
    object QrInnNat_AnoMesDiaIn: TLargeintField
      FieldName = 'AnoMesDiaIn'
    end
  end
  object QrVSCalSub: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,   '
      'CAST(dst.DstGGX AS SIGNED) GraGruX,  '
      'CAST(dst.MovimID AS SIGNED) DstMovID,  '
      'CAST(dst.DstNivel1 AS SIGNED) DstNivel1,  '
      'dst.AnoMesDiaIn, dst.ArtGeComodty, '
      'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg, '
      
        'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2' +
        ', '
      'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2, '
      'SUM(inn.Pecas) InnPecas  '
      'FROM _vs_in_cab_e_its inn  '
      'LEFT JOIN _vs_ca_cab_e_its dst ON   '
      '  dst.SrcMovID=inn.MovimID  '
      '  AND dst.SrcNivel1=inn.Codigo  '
      '  AND dst.SrcNivel2=inn.Controle  '
      'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty '
      'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty ')
    Left = 574
    Top = 327
    object QrVSCalSubGGXInn: TLargeintField
      FieldName = 'GGXInn'
    end
    object QrVSCalSubGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSCalSubDstMovID: TLargeintField
      FieldName = 'DstMovID'
    end
    object QrVSCalSubDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
    end
    object QrVSCalSubAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
    end
    object QrVSCalSubArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
    end
    object QrVSCalSubPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCalSubPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSCalSubMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object QrEmissoes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT VSMovCod,'
      'GROUP_CONCAT(DISTINCT(emi.Fulao)) Fuloes'
      'FROM emit emi '
      'WHERE emi.VSMovCod IN ('
      
        '4632, 4643, 4651, 4657, 4662, 4667, 4709, 4713, 4717, 4720, 4744' +
        ', 4758, 4776, 4793, 4818, 4830, 4849, 4853, 4858, 4861'
      ')'
      'GROUP BY VSMovCod ')
    Left = 865
    Top = 288
    object QrEmissoesVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
      Required = True
    end
    object QrEmissoesFuloes: TWideMemoField
      FieldName = 'Fuloes'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrVSEmCurtim: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,   '
      'CAST(dst.DstGGX AS SIGNED) GraGruX,  '
      'CAST(dst.MovimID AS SIGNED) DstMovID,  '
      'CAST(dst.DstNivel1 AS SIGNED) DstNivel1,  '
      'dst.AnoMesDiaIn, dst.ArtGeComodty, '
      'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg, '
      
        'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2' +
        ', '
      'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2, '
      'SUM(inn.Pecas) InnPecas  '
      'FROM _vs_in_cab_e_its inn  '
      'LEFT JOIN _vs_ca_cab_e_its dst ON   '
      '  dst.SrcMovID=inn.MovimID  '
      '  AND dst.SrcNivel1=inn.Codigo  '
      '  AND dst.SrcNivel2=inn.Controle  '
      'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty '
      'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty ')
    Left = 574
    Top = 379
    object QrVSEmCurtimGGXInn: TLargeintField
      FieldName = 'GGXInn'
    end
    object QrVSEmCurtimGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSEmCurtimDstMovID: TLargeintField
      FieldName = 'DstMovID'
    end
    object QrVSEmCurtimDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
    end
    object QrVSEmCurtimAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
    end
    object QrVSEmCurtimArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
    end
    object QrVSEmCurtimPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSEmCurtimPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSEmCurtimMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSEmCurtimFuloes: TWideMemoField
      FieldName = 'Fuloes'
      BlobType = ftWideMemo
      Size = 511
    end
  end
  object QrVSGerDeEmCur: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT CAST(dst.GraGruX AS SIGNED) GGXInn,  '
      'CAST(dst.DstGGX AS SIGNED) GraGruX, '
      'CAST(dst.MovimID AS SIGNED) DstMovID, '
      'CAST(dst.DstNivel1 AS SIGNED) DstNivel1, '
      'dst.AnoMesDiaIn, dst.ArtGeComodty,'
      'SUM(-dst.Pecas) Pecas, SUM(-dst.PesoKg) PesoKg,'
      
        'SUM(-dst.QtdGerArM2) QtdGerArM2, SUM(-dst.QtdGerArP2) QtdGerArP2' +
        ','
      'SUM(-dst.PesoKg) / SUM(-dst.QtdGerArM2) RendKgM2,'
      'SUM(-dst.QtdGerArM2) / SUM(-dst.Pecas) MediaM2'
      'FROM _vs_ga_cab_e_its dst'
      'WHERE SrcMovID=27 /* n'#227'o pode ser 1 */'
      'GROUP BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty'
      'ORDER BY AnoMesDiaIn, dst.MovimID, GGXInn, dst.ArtGeComodty')
    Left = 586
    Top = 175
    object QrVSGerDeEmCurArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
      Required = True
    end
    object QrVSGerDeEmCurGGXInn: TLargeintField
      FieldName = 'GGXInn'
      Required = True
    end
    object QrVSGerDeEmCurGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSGerDeEmCurDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSGerDeEmCurDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSGerDeEmCurMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSGerDeEmCurAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
      Required = True
    end
    object QrVSGerDeEmCurPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSGerDeEmCurPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSGerDeEmCurQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSGerDeEmCurQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSGerDeEmCurMediaM2: TFloatField
      FieldName = 'MediaM2'
    end
    object QrVSGerDeEmCurRendKgM2: TFloatField
      FieldName = 'RendKgM2'
    end
  end
  object DsGafid: TDataSource
    DataSet = QrGafid
    Left = 49
    Top = 248
  end
  object QrFaltaMarca: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT a.Controle, p.Marca, vmi.MovimID, vmi.MovimNiv, '
      'vmi.GraGruX, ggx.GraGruY'
      'FROM _vs_marcas_desc_all_ a'
      'LEFT JOIN _vs_marcas_desc_psq_ p ON p.Controle=a.Controle'
      'LEFT JOIN bluederm_panorama.vsmovits vmi '
      '  ON vmi.Controle=a.Controle'
      'LEFT JOIN bluederm_panorama.gragrux ggx'
      '  ON ggx.Controle=vmi.GraGruX'
      'WHERE ('
      '  a.Marca <> p.Marca'
      '  OR p.Controle IS NULL'
      '  OR p.Marca=""'
      ')'
      'AND (NOT vmi.MovimNiv IN (32))'
      '/* Subproduto em processo de caleiro */'
      'AND (NOT (vmi.MovimNiv=29 AND (ggx.GraGruY=512)))'
      'ORDER BY a.Controle')
    Left = 749
    Top = 172
    object QrFaltaMarcaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFaltaMarcaMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrFaltaMarcaMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrFaltaMarcaMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrFaltaMarcaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrFaltaMarcaGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
  object DsFaltaMarca: TDataSource
    DataSet = QrFaltaMarca
    Left = 748
    Top = 220
  end
  object QrVSClaDeCur: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(i.AreaM2) / SUM(IF(i.AreaM2 <> 0, '
      '  i.Pecas, 0.00)) MediaM2Peca, inn.PesoKg InnKg, '
      'inn.Pecas InnPc, inn.PesoKg / inn.Pecas KgCouro, '
      '(inn.PesoKg / inn.Pecas) / '
      '('
      'SUM(i.AreaM2) / SUM(IF(i.AreaM2 <> 0, '
      '  i.Pecas, 0.00)) '
      ') RendKgm2, SUM(i.AreaM2) SumAreaM2,'
      'SUM(IF(i.AreaM2 <> 0, i.Pecas, 0.00)) SumPecas, '
      'SUM(IF(i.AreaM2 <> 0, i.PesoKg, 0.00)) SumPesoKg,'
      'i.DataHora, i.DtEntrada_TXT, i.AnoMesDiaIn,'
      'j.SrcNivel2, inn.GraGruX GGXInn, inn.Marca,'
      'cla.ArtGeComodty, cla.MovimID'
      'FROM _vs_artcla_de_gerart_de_emcurt_cab_e_its cla'
      'LEFT JOIN _vs_gerartdecur_cab_e_it_j j'
      '  ON j.Controle=cla.Controle'
      'LEFT JOIN _vs_gerartdecur_cab_e_it_i i'
      '  ON i.ClaCtrl=j.SrcNivel2'
      'LEFT JOIN _vs_entrada_in_natura_cab_e_its inn'
      '  ON inn.Controle=i.SrcNiv2Inn'
      'GROUP BY AnoMesDiaIn, MovimID, GGXInn, ArtGeComodty, Marca')
    Left = 781
    Top = 360
    object QrVSClaDeCurSumPecas: TFloatField
      FieldName = 'SumPecas'
    end
    object QrVSClaDeCurSumAreaM2: TFloatField
      FieldName = 'SumAreaM2'
    end
    object QrVSClaDeCurDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSClaDeCurDtEntrada_TXT: TWideStringField
      FieldName = 'DtEntrada_TXT'
      Size = 10
    end
    object QrVSClaDeCurAnoMesDiaIn: TIntegerField
      FieldName = 'AnoMesDiaIn'
    end
    object QrVSClaDeCurSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSClaDeCurGGXInn: TLargeintField
      FieldName = 'GGXInn'
    end
    object QrVSClaDeCurMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSClaDeCurArtGeComodty: TIntegerField
      FieldName = 'ArtGeComodty'
      Required = True
    end
    object QrVSClaDeCurMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSClaDeCurSumPesoKg: TFloatField
      FieldName = 'SumPesoKg'
    end
    object QrVSClaDeCurMediaM2Peca: TFloatField
      FieldName = 'MediaM2Peca'
    end
    object QrVSClaDeCurInnKg: TFloatField
      FieldName = 'InnKg'
    end
    object QrVSClaDeCurInnPc: TFloatField
      FieldName = 'InnPc'
    end
    object QrVSClaDeCurKgCouro: TFloatField
      FieldName = 'KgCouro'
    end
    object QrVSClaDeCurRendKgm2: TFloatField
      FieldName = 'RendKgm2'
    end
  end
end
