unit VSPesqSeqPeca;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  dmkDBGridZTO, mySQLDirectQuery;

type
  TFmVSPesqSeqPeca = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrVsCacItsA: TmySQLQuery;
    QrVsCacItsAAreaM2: TFloatField;
    BitBtn1: TBitBtn;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrVSPsqSeqCac: TmySQLQuery;
    DsVSPsqSeqCac: TDataSource;
    DsVsCacItsA: TDataSource;
    QrVsCacItsAControle: TLargeintField;
    QrVSPsqSeqCacVSPallet: TIntegerField;
    QrCIA: TmySQLQuery;
    QrCIAAreaM2: TFloatField;
    QrCIAControle: TLargeintField;
    Panel5: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdPalletIni: TdmkEdit;
    Label2: TLabel;
    EdPalletFim: TdmkEdit;
    Label3: TLabel;
    EdCouro1: TdmkEdit;
    Label4: TLabel;
    EdCouro2: TdmkEdit;
    Label5: TLabel;
    EdCouro3: TdmkEdit;
    Label6: TLabel;
    EdCouro4: TdmkEdit;
    Label7: TLabel;
    EdCouro5: TdmkEdit;
    QrVSPsqSeqCacItens: TIntegerField;
    QrVSPsqSeqCacPosicao: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    //procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    FParar: Boolean;
    FVSPsqSeqCac: String;
  public
    { Public declarations }
  end;

  var
  FmVSPesqSeqPeca: TFmVSPesqSeqPeca;

implementation

uses UnMyObjects, Module, DmkDAC_PF, CreateVS, ModuleGeral,
UMySQLModule;

{$R *.DFM}

procedure TFmVSPesqSeqPeca.BitBtn1Click(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmVSPesqSeqPeca.BtOKClick(Sender: TObject);

var
  I, K, Pallet1, Pallet2, PallIni, PallFim, Itens, Certos, VSPallet, Posicao: Integer;
  Continua: Boolean;
  SomaIni, SomaAtu: Double;
  Couros: array of double;
  SQL_Couro: array of String;
  SQL_Couros: String;
  //
  procedure SomaEdit(Edit: TdmkEdit);
  begin
    if Edit.ValueVariant <> 0 then
    begin
      SomaIni := SomaIni + Edit.ValueVariant;
      Itens := Itens + 1;
      //
      SetLength(Couros, Itens);
      Couros[Itens - 1] := Edit.ValueVariant;
      //
      SetLength(SQL_Couro, Itens);
      SQL_Couro[Itens - 1] := '  AreaM2=' + Geral.FFT_Dot(Edit.ValueVariant, 2, siPositivo);
    end;
  end;
begin
  FParar := False;
  Screen.Cursor := crHourGlass;
  try
    //MePallets.Lines.Clear;
    FVSPsqSeqCac := UnCreateVS.RecriaTempTableNovo(ntrttVSPsqSeqCac,
          DModG.QrUpdPID1, False);
    SomaIni := 0;
    Itens   := 0;
    SetLength(Couros, 0);
    SomaEdit(EdCouro1);
    SomaEdit(EdCouro2);
    SomaEdit(EdCouro3);
    SomaEdit(EdCouro4);
    SomaEdit(EdCouro5);
    if MyObjects.FIC(Length(Couros) < 2, nil,
      'Informe pelo menos a �rea de dois couros!') then
        Exit;
    SQL_Couros := '';
    for I := 1 to Length(SQL_Couro) - 1 do
    begin
      SQL_Couros := SQL_Couros + ' OR ' + SQL_Couro[I] + sLineBreak;
    end;
    if EdPalletFim.ValueVariant > EdPalletIni.ValueVariant then
    begin
      Pallet1 := EdPalletIni.ValueVariant;
      Pallet2 := EdPalletFim.ValueVariant;
    end else
    begin
      Pallet1 := EdPalletFim.ValueVariant;
      Pallet2 := EdPalletIni.ValueVariant;
    end;
     UnDmkDAC_PF.AbreMySQLQuery0(QrVSPsqSeqCac, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _TESTE_LOC_SEQ_CIA_1; ',
    'CREATE TABLE _TESTE_LOC_SEQ_CIA_1 ',
    'SELECT VSPallet, AreaM2, COUNT(AreaM2) ITENS  ',
    'FROM ' + TMeuDB + '.vscacitsa  ',
    'WHERE VSPallet BETWEEN 0 AND 1500 ',
    'AND ( ',
    '  AreaM2=' + Geral.FFT_Dot(Couros[0], 2, siPositivo),
    SQL_Couros,
    ') ',
    'GROUP BY VSPallet, AreaM2 ',
    'ORDER BY VSPallet, AreaM2; ',
    ' ',
    'INSERT INTO ' + FVSPsqSeqCac,
    'SELECT VSPallet, COUNT(AreaM2) ITENS, ',
    '0 Posicao, 1 Ativo ',
    'FROM _TESTE_LOC_SEQ_CIA_1 ',
    'GROUP BY VSPallet; ',
    ' ',
    'SELECT * ',
    'FROM ' + FVSPsqSeqCac,
    'WHERE Itens=' + Geral.FF0(Itens),
    'ORDER BY VSPallet DESC ',
    ' ']);
    if FParar then
    begin
      FParar := False;
      Exit;
    end;
    QrVSPsqSeqCac.First;
    while not QrVSPsqSeqCac.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCIA, Dmod.MyDB, [
      'SELECT Controle, AreaM2  ',
      'FROM vscacitsa ',
      'WHERE VSPallet=' + Geral.FF0(QrVSPsqSeqCacVSPallet.Value),
      'ORDER BY Controle DESC ',
      '']);
      QrCIA.First;
      while not QrCIA.Eof do
      begin
        if FParar then
        begin
          FParar := False;
          Exit;
        end;
        if QrCIAAreaM2.Value = Couros[0] then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrVsCacItsA, Dmod.MyDB, [
          'SELECT Controle, AreaM2  ',
          'FROM vscacitsa ',
          'WHERE VSPallet=' + Geral.FF0(QrVSPsqSeqCacVSPallet.Value),
          'ORDER BY Controle DESC ',
          'LIMIT '+ Geral.FF0(QrCIA.RecNo-1) + ', ' + Geral.FF0(Itens),
          '']);
          if QrVsCacItsA.RecordCount = Itens then
          begin
            Certos := 0;
            QrVsCacItsA.First;
            while not QrVsCacItsA.Eof do
            begin
              if QrVsCacItsAAreaM2.Value = Couros[QrVsCacItsA.RecNo -1] then
                Certos := Certos + 1
              else
                QrVsCacItsA.Last;
              //
              Application.ProcessMessages;
              QrVsCacItsA.Next;
            end;
            if Certos = Itens then
            begin
              VSPallet := QrVSPsqSeqCacVSPallet.Value;
              Posicao  := QrCIA.RecNo;
              //MePallets.Lines.Add(Geral.FF0(VSPallet) +
              //' [' + Geral.FF0(QrCIA.RecNo) + ']');
              UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FVSPsqSeqCac, False, [
              'Itens'], [
              'VSPallet', 'Posicao'], [
              Itens], [
              VSPallet, Posicao], False);
            end;
          end else
            Continua := False;
        end;
        //
        QrCIA.Next;
      end;
      //
      QrVSPsqSeqCac.Next;
    end;
     UnDmkDAC_PF.AbreMySQLQuery0(QrVSPsqSeqCac, DModG.MyPID_DB, [
    'SELECT *  ',
    'FROM ' + FVSPsqSeqCac,
    'WHERE Posicao>0',
    'ORDER BY VSPallet DESC ',
    '']);
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSPesqSeqPeca.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

{
object Button1: TButton
  Left = 348
  Top = 16
  Width = 75
  Height = 25
  Caption = 'Button1'
  TabOrder = 2
  OnClick = Button1Click
end
procedure TFmVSPesqSeqPeca.Button1Click(Sender: TObject);
var
  I, K, Pallet1, Pallet2, PallIni, PallFim, Itens, Certos: Integer;
  Continua: Boolean;
  SomaIni, SomaAtu: Double;
  Couros: array of double;
  procedure SomaEdit(Edit: TdmkEdit);
  begin
    if Edit.ValueVariant <> 0 then
    begin
      SomaIni := SomaIni + Edit.ValueVariant;
      Itens := Itens + 1;
      SetLength(Couros, Itens);
      Couros[Itens - 1] := Edit.ValueVariant;
    end;
  end;
begin
  FParar := False;
  Screen.Cursor := crHourGlass;
  try
    MePallets.Lines.Clear;
    SomaIni := 0;
    Itens   := 0;
    SetLength(Couros, 0);
    SomaEdit(EdCouro1);
    SomaEdit(EdCouro2);
    SomaEdit(EdCouro3);
    SomaEdit(EdCouro4);
    SomaEdit(EdCouro5);
    if EdPalletFim.ValueVariant > EdPalletIni.ValueVariant then
    begin
      Pallet1 := EdPalletIni.ValueVariant;
      Pallet2 := EdPalletFim.ValueVariant;
    end else
    begin
      Pallet1 := EdPalletFim.ValueVariant;
      Pallet2 := EdPalletIni.ValueVariant;
    end;
    for I := Pallet2 downto Pallet1 do
    begin
      if FParar then
      begin
        FParar := False;
        Exit;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Pesquisando Pallet ' + Geral.FF0(I));
      Continua := True;
      K := 0;
      while Continua do
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrVsCacItsA, Dmod.MyDB, [
        'SELECT Controle, AreaM2  ',
        'FROM vscacitsa ',
        'WHERE VSPallet=' + Geral.FF0(I),
        'ORDER BY Controle DESC ',
        'LIMIT '+ Geral.FF0(K) + ', ' + Geral.FF0(Itens),
        '']);
        if QrVsCacItsA.RecordCount = Itens then
        begin
          Certos := 0;
          QrVsCacItsA.First;
          while not QrVsCacItsA.Eof do
          begin
            if QrVsCacItsAAreaM2.Value = Couros[QrVsCacItsA.RecNo -1] then
              Certos := Certos + 1
            else
              QrVsCacItsA.Last;
            //
            Application.ProcessMessages;
            QrVsCacItsA.Next;
          end;
          if Certos = Itens then
            MePallets.Lines.Add(Geral.FF0(I));
        end else
          Continua := False;
        //
        K := K + 1;
        if FParar then
        begin
          FParar := False;
          Exit;
        end;
      end;
      Application.ProcessMessages;
    end;
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmVSPesqSeqPeca.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSPesqSeqPeca.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSPesqSeqPeca.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
