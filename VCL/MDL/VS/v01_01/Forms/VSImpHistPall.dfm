object FmVSImpHistPall: TFmVSImpHistPall
  Left = 0
  Top = 0
  Caption = 'WET-CURTI-130 :: Hist'#243'rico de Pallets'
  ClientHeight = 349
  ClientWidth = 438
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object QrMovHist: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Compra","Venda","Reclasse","Baixa",' +
        '"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado",' +
        '"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","P' +
        'r'#233' reclasse","Compra de Classificado","Baixa extra","Saldo anter' +
        'ior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub Pr' +
        'oduto","Reclasse Mul.") NO_MovimID,'
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto") NO_MovimNiv,'
      'MIN(DataHora) MinDataHora, '
      'MAX(DataHora) MaxDataHora, MovimID, '
      'MovimNiv, COUNT(Controle) ITENS, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, '
      'SUM(PesoKg) PesoKg '
      'FROM vsmovitb vmi '
      'WHERE Pallet=2202'
      'GROUP BY MovimID, MovimNiv '
      ''
      'UNION'
      ''
      ''
      ''
      'SELECT '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Compra","Venda","Reclasse","Baixa",' +
        '"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado",' +
        '"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","P' +
        'r'#233' reclasse","Compra de Classificado","Baixa extra","Saldo anter' +
        'ior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub Pr' +
        'oduto","Reclasse Mul.") NO_MovimID,'
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto") NO_MovimNiv,'
      'MIN(DataHora) MinDataHora, '
      'MAX(DataHora) MaxDataHora, MovimID, '
      'MovimNiv, COUNT(Controle) ITENS, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, '
      'SUM(PesoKg) PesoKg '
      'FROM vsmovits vmi '
      'WHERE Pallet=2202'
      'GROUP BY MovimID, MovimNiv '
      ''
      ''
      ''
      'ORDER BY MinDataHora, MaxDataHora ')
    Left = 68
    Top = 156
    object QrMovHistNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrMovHistNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object QrMovHistMinDataHora: TDateTimeField
      FieldName = 'MinDataHora'
    end
    object QrMovHistMaxDataHora: TDateTimeField
      FieldName = 'MaxDataHora'
    end
    object QrMovHistMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrMovHistMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrMovHistITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrMovHistPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMovHistAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrMovHistPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object QrPallets: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPalletsAfterScroll
    SQL.Strings = (
      'SELECT wmi.Pallet, wmi.GraGruX, wmi.Ficha, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, '
      'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg,'
      'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet'
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch '
      'WHERE wmi.MovimCod=662'
      'AND wmi.MovimNiv=7'
      'GROUP BY Pallet'
      'ORDER BY NO_Pallet')
    Left = 68
    Top = 57
    object QrPalletsPallet: TLargeintField
      FieldName = 'Pallet'
    end
    object QrPalletsGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrPalletsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrPalletsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPalletsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPalletsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPalletsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPalletsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrPalletsNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrPalletsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrPalletsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrPalletsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrPalletsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPalletsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object frxWET_CURTI_130_01: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_130_01GetValue
    Left = 68
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMovHist
        DataSetName = 'frxDsMovHist'
      end
      item
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 60.472480000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Hist'#243'rico de Pallets')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 177.637910000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
        KeepChild = True
        KeepFooter = True
        KeepHeader = True
        KeepTogether = True
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 98.267780000000000000
          Width = 204.094424720000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 68.031496060000000000
          Height = 30.236230240000000000
          DataField = 'Pallet'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 476.220780000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPallets."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreap2: TfrxMemoView
          Left = 574.488560000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPallets."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 521.575140000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPallets."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPallets."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 302.362400000000000000
          Width = 173.858331180000000000
          Height = 15.118110240000000000
          DataField = 'NO_Pallet'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."NO_Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 68.031540000000000000
          Width = 30.236196060000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 68.031540000000000000
          Top = 15.118120000000000000
          Width = 612.283664720000000000
          Height = 15.118110240000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_FORNECEDORES]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        DataSet = frxDsMovHist
        DataSetName = 'frxDsMovHist'
        KeepChild = True
        KeepFooter = True
        KeepHeader = True
        KeepTogether = True
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 15.118120000000000000
          Width = 75.590551181102360000
          Height = 13.228346460000000000
          DataField = 'NO_MovimID'
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovHist."NO_MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 105.826840000000000000
          Width = 94.488188976377950000
          Height = 13.228346460000000000
          DataField = 'NO_MovimNiv'
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovHist."NO_MovimNiv"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 200.315090000000000000
          Width = 79.370078740157480000
          Height = 13.228346460000000000
          DataField = 'MinDataHora'
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovHist."MinDataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 279.685220000000000000
          Width = 79.370078740000000000
          Height = 13.228346460000000000
          DataField = 'MaxDataHora'
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovHist."MaxDataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 359.055350000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'Pecas'
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovHist."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 457.323130000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovHist."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 404.409710000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovHist."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Width = 15.117924720000000000
          Height = 13.228346460000000000
          DataField = 'MovimID'
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovHist."MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 90.708720000000000000
          Width = 15.117924720000000000
          Height = 13.228346460000000000
          DataField = 'MovimNiv'
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovHist."MovimNiv"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 510.236550000000000000
          Width = 170.078654720000000000
          Height = 13.228346460000000000
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CLASSIFICADORES]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 139.842610000000000000
        Width = 680.315400000000000000
        KeepChild = True
        object MeTitNome: TfrxMemoView
          Left = 68.031540000000000000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPecas: TfrxMemoView
          Left = 476.220780000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaP2: TfrxMemoView
          Left = 574.488560000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 521.575140000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 302.362400000000000000
          Width = 173.858331180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o do pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 11.338590000000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        KeepChild = True
        object Memo16: TfrxMemoView
          Width = 90.708524720000000000
          Height = 13.228346460000000000
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo de Movimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 90.708720000000000000
          Width = 109.606174720000000000
          Height = 13.228346460000000000
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#237'vel no movimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 200.315090000000000000
          Width = 79.370078740157480000
          Height = 13.228346460000000000
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora inicial')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 279.685220000000000000
          Width = 79.370078740000000000
          Height = 13.228346460000000000
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora final')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 359.055350000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 457.323130000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 404.409710000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 510.236550000000000000
          Width = 170.078654720000000000
          Height = 13.228346460000000000
          DataSet = frxDsMovHist
          DataSetName = 'frxDsMovHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classificadores e seus percentuais')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPallets: TfrxDBDataset
    UserName = 'frxDsPallets'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'PesoKg=PesoKg'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'ValorT=ValorT'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2')
    DataSet = QrPallets
    BCDToCurrency = False
    Left = 68
    Top = 108
  end
  object QrMulFrn_: TmySQLQuery
    Database = Dmod.MyDB
    Left = 196
    Top = 8
    object QrMulFrn_SiglaVS: TWideStringField
      FieldName = 'SiglaVS'
      Size = 60
    end
    object QrMulFrn_FrnCod: TLargeintField
      FieldName = 'FrnCod'
    end
    object QrMulFrn_Pecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrTotFrn_: TmySQLQuery
    Database = Dmod.MyDB
    Left = 196
    Top = 56
    object QrTotFrn_Pecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object frxDsMovHist: TfrxDBDataset
    UserName = 'frxDsMovHist'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_MovimID=NO_MovimID'
      'NO_MovimNiv=NO_MovimNiv'
      'MinDataHora=MinDataHora'
      'MaxDataHora=MaxDataHora'
      'MovimID=MovimID'
      'MovimNiv=MovimNiv'
      'ITENS=ITENS'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'PesoKg=PesoKg')
    DataSet = QrMovHist
    BCDToCurrency = False
    Left = 68
    Top = 204
  end
  object QrRevIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 196
    Top = 108
    object QrRevItsRevisor: TLargeintField
      FieldName = 'Revisor'
    end
    object QrRevItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrRevItsNO_Revisor: TWideStringField
      FieldName = 'NO_Revisor'
      Size = 15
    end
  end
end
