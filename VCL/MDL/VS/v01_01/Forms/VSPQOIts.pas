unit VSPQOIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls,
  UnProjGroup_Consts, dmkEditDateTimePicker;

type
  TFmVSPQOIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesCodUsu: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOMECI: TWideStringField;
    QrPQ: TmySQLQuery;
    QrPQCodigo: TIntegerField;
    QrPQNome: TWideStringField;
    DsPQ: TDataSource;
    DsCI: TDataSource;
    QrSaldo: TmySQLQuery;
    QrSaldoPeso: TFloatField;
    QrSaldoValor: TFloatField;
    QrSaldoCUSTO: TFloatField;
    QrSaldoCI: TIntegerField;
    QrSaldoPQ: TIntegerField;
    DsSaldo: TDataSource;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    EdOriCodi: TdmkEdit;
    PnInsumos: TPanel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdPQ: TdmkEditCB;
    CBPQ: TdmkDBLookupComboBox;
    EdCI: TdmkEditCB;
    CBCI: TdmkDBLookupComboBox;
    EdPesoAdd: TdmkEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    EdSaldoFut: TdmkEdit;
    Panel2: TPanel;
    CkContinuar: TCheckBox;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Label21: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPQChange(Sender: TObject);
    procedure EdCIChange(Sender: TObject);
    procedure EdPesoAddExit(Sender: TObject);
    procedure EdPesoAddRedefinido(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaSaldoFuturo();
  public
    { Public declarations }
    FDataX: TDateTime;
    FControle: Integer;

  end;

  var
  FmVSPQOIts: TFmVSPQOIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral, UnPQ_PF, PQx;

{$R *.DFM}

procedure TFmVSPQOIts.BtOKClick(Sender: TObject);
const
  Unitario = True;
var
  Controle: Integer;
  KG, RS, SF: Double;
  //
  DataX, DtCorrApo: String;
  OriCodi, OriCtrl, OriTipo, CliOrig, CliDest, Insumo, Empresa: Integer;
  Peso, Valor: Double;
begin
  if MyObjects.FIC(QrSaldo.RecordCount = 0, nil,
    'Insumo / cliente interno n�o definido!') then Exit;
  SF := Geral.DMV(EdSaldoFut.Text);
  if MyObjects.FIC(SF < 0, EdPesoAdd,
    'Quantidade insuficiente no estoque!') then Exit;
  KG := Geral.DMV(EdPesoAdd.Text);
  RS := KG * QrSaldoCUSTO.Value;
  CliOrig := EdCI.ValueVariant;
  CliDest := EdCI.ValueVariant;
  Empresa := EdCI.ValueVariant;
  if MyObjects.FIC(KG <= 0, EdPesoAdd, 'Quantidade inv�lida!') then Exit;
  //
  Controle := DModG.BuscaProximoInteiro(CO_UPD_TAB_PQX, 'OrigemCtrl', 'Tipo', VAR_FATID_0190);
  //
  DataX      := Geral.FDT(FDataX, 1);
  OriCodi    := EdOriCodi.ValueVariant;
  OriCtrl    := Controle;
  OriTipo    := VAR_FATID_0190;
  CliOrig    := EdCI.ValueVariant;
  CliDest    := EdCI.ValueVariant;
  Insumo     := QrSaldoPQ.Value;
  Peso       := -KG;
  Valor      := -RS;
  DtCorrApo  := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
  //
  if not DmodG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  PQ_PF.InserePQx_Bxa(DataX, CliOrig, CliDest, Insumo, Peso, Valor, OriCodi,
    OriCtrl, OriTipo, Unitario, DtCorrApo, Empresa);
  FControle := Controle;
  //
  UnPQx.AtualizaEstoquePQ(QrSaldoCI.Value, QrSaldoPQ.Value, Empresa, aeMsg, '');
  QrSaldo.Close;
  UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
  PQ_PF.PQO_AtualizaCusto(OriCodi);
  if CkContinuar.Checked then
  begin
    ImgTipo.SQLType := stIns;
    EdPesoAdd.Text := '';
    EdPQ.Text := '';
    CBPQ.KeyValue := NULL;
    EdPQ.SetFocus;
  end else
    Close;
end;

procedure TFmVSPQOIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPQOIts.CalculaSaldoFuturo();
var
  CI, PQ: Integer;
  PesoAdd, SaldoFut: Double;
begin
  CI := Geral.IMV(EdCI.Text);
  PQ := Geral.IMV(EdPQ.Text);
  QrSaldo.Close;
  QrSaldo.Params[0].AsInteger := CI;
  QrSaldo.Params[1].AsInteger := PQ;
  UnDmkDAC_PF.AbreQuery(QrSaldo, Dmod.MyDB);
  //
  PesoAdd := Geral.DMV(EdPesoAdd.Text);
  SaldoFut := QrSaldoPeso.Value - PesoAdd;
  EdSaldoFut.Text := Geral.FFT(SaldoFut, 3, siNegativo);
end;

procedure TFmVSPQOIts.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmVSPQOIts.EdCIChange(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmVSPQOIts.EdPesoAddExit(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmVSPQOIts.EdPesoAddRedefinido(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmVSPQOIts.EdPQChange(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmVSPQOIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSPQOIts.FormCreate(Sender: TObject);
begin
  FControle := 0;
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrCI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPQ, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmVSPQOIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
