object FmVSRRMCab: TFmVSRRMCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-178 :: Reproceso / Reparo de Material'
  ClientHeight = 766
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 661
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 485
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 461
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label1: TLabel
        Left = 940
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        FocusControl = DBEdit0
      end
      object Label2: TLabel
        Left = 80
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit1
      end
      object Label11: TLabel
        Left = 592
        Top = 16
        Width = 107
        Height = 13
        Caption = 'Liberado p/ classificar:'
        FocusControl = DBEdit3
      end
      object Label5: TLabel
        Left = 344
        Top = 56
        Width = 174
        Height = 13
        Caption = 'Observa'#231#227'o sobre a industrializa'#231#227'o:'
        FocusControl = DBEdit4
      end
      object Label22: TLabel
        Left = 440
        Top = 16
        Width = 25
        Height = 13
        Caption = #193'rea:'
        FocusControl = DBEdit14
      end
      object Label23: TLabel
        Left = 476
        Top = 16
        Width = 94
        Height = 13
        Caption = 'Data/hora gera'#231#227'o:'
        FocusControl = DBEdit15
      end
      object Label32: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = dmkDBEdit1
      end
      object Label33: TLabel
        Left = 708
        Top = 16
        Width = 94
        Height = 13
        Caption = 'Configur. p/ classif.:'
        FocusControl = DBEdit25
      end
      object Label34: TLabel
        Left = 824
        Top = 16
        Width = 83
        Height = 13
        Caption = 'Fim classifica'#231#227'o:'
        FocusControl = DBEdit26
      end
      object Label55: TLabel
        Left = 16
        Top = 56
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdit42
      end
      object Label58: TLabel
        Left = 680
        Top = 56
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label59: TLabel
        Left = 888
        Top = 56
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label67: TLabel
        Left = 20
        Top = 164
        Width = 88
        Height = 13
        Caption = 'Grupo de emiss'#227'o:'
      end
      object DBEdit0: TdmkDBEdit
        Left = 940
        Top = 32
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        DataSource = DsVSRRMCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 7
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 80
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSRRMCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 136
        Top = 32
        Width = 301
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSRRMCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 592
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrLibOpe'
        DataSource = DsVSRRMCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 344
        Top = 72
        Width = 333
        Height = 21
        DataField = 'Nome'
        DataSource = DsVSRRMCab
        TabOrder = 4
      end
      object DBEdit14: TDBEdit
        Left = 440
        Top = 32
        Width = 32
        Height = 21
        DataField = 'NO_TIPO'
        DataSource = DsVSRRMCab
        TabOrder = 5
      end
      object DBEdit15: TDBEdit
        Left = 476
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtHrAberto'
        DataSource = DsVSRRMCab
        TabOrder = 6
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSRRMCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit25: TDBEdit
        Left = 708
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrCfgOpe'
        DataSource = DsVSRRMCab
        TabOrder = 8
      end
      object DBEdit26: TDBEdit
        Left = 824
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrFimOpe'
        DataSource = DsVSRRMCab
        TabOrder = 9
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 96
        Width = 245
        Height = 61
        Caption = ' Origem:'
        TabOrder = 10
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label36: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label37: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label38: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit27: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasINI'
            DataSource = DsVSRRMCab
            TabOrder = 0
          end
          object DBEdit28: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgINI'
            DataSource = DsVSRRMCab
            TabOrder = 1
          end
          object DBEdit29: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaINIM2'
            DataSource = DsVSRRMCab
            TabOrder = 2
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 264
        Top = 96
        Width = 245
        Height = 61
        Caption = 'Destino:'
        TabOrder = 11
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label40: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label41: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label42: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit31: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasDst'
            DataSource = DsVSRRMCab
            TabOrder = 0
          end
          object DBEdit32: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgDst'
            DataSource = DsVSRRMCab
            TabOrder = 1
          end
          object DBEdit33: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaDstM2'
            DataSource = DsVSRRMCab
            TabOrder = 2
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 760
        Top = 96
        Width = 245
        Height = 61
        Caption = 'Saldo em opera'#231#227'o:'
        TabOrder = 12
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label44: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label45: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label46: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit35: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasSdo'
            DataSource = DsVSRRMCab
            TabOrder = 0
          end
          object DBEdit36: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgSdo'
            DataSource = DsVSRRMCab
            TabOrder = 1
          end
          object DBEdit37: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaSdoM2'
            DataSource = DsVSRRMCab
            TabOrder = 2
          end
        end
      end
      object GroupBox6: TGroupBox
        Left = 512
        Top = 96
        Width = 245
        Height = 61
        Caption = 'Baixas normais:'
        TabOrder = 13
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label39: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label43: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label47: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit30: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasBxa'
            DataSource = DsVSRRMCab
            TabOrder = 0
          end
          object DBEdit34: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgBxa'
            DataSource = DsVSRRMCab
            TabOrder = 1
          end
          object DBEdit38: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaBxaM2'
            DataSource = DsVSRRMCab
            TabOrder = 2
          end
        end
      end
      object DBEdit42: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsVSRRMCab
        TabOrder = 14
      end
      object DBEdit43: TDBEdit
        Left = 72
        Top = 72
        Width = 269
        Height = 21
        DataField = 'NO_Cliente'
        DataSource = DsVSRRMCab
        TabOrder = 15
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 920
        Top = 72
        Width = 81
        Height = 21
        TabStop = False
        DataField = 'NFeRem'
        DataSource = DsVSRRMCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 16
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit44: TDBEdit
        Left = 680
        Top = 72
        Width = 205
        Height = 21
        DataField = 'LPFMO'
        DataSource = DsVSRRMCab
        TabOrder = 17
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 888
        Top = 72
        Width = 29
        Height = 21
        TabStop = False
        DataField = 'SerieRem'
        DataSource = DsVSRRMCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 18
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit49: TDBEdit
        Left = 112
        Top = 160
        Width = 56
        Height = 21
        DataField = 'EmitGru'
        DataSource = DsVSRRMCab
        TabOrder = 19
      end
      object DBEdit50: TDBEdit
        Left = 168
        Top = 160
        Width = 829
        Height = 21
        DataField = 'NO_EmitGru'
        DataSource = DsVSRRMCab
        TabOrder = 20
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 597
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 202
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 376
        Top = 15
        Width = 630
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 497
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 4
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 296
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Em Reprocesso'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object BtOri: TBitBtn
          Tag = 421
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Origem'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtOriClick
        end
        object BtDst: TBitBtn
          Tag = 447
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Destino'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtDstClick
        end
        object BtPesagem: TBitBtn
          Tag = 194
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pesagem'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtPesagemClick
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 185
      Width = 1008
      Height = 96
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label13: TLabel
        Left = 16
        Top = 16
        Width = 28
        Height = 13
        Caption = 'IME-I:'
      end
      object Label14: TLabel
        Left = 100
        Top = 16
        Width = 95
        Height = 13
        Caption = 'Artigo em opera'#231#227'o:'
      end
      object Label15: TLabel
        Left = 576
        Top = 16
        Width = 45
        Height = 13
        Caption = '$/m'#178' MO:'
      end
      object Label16: TLabel
        Left = 672
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object Label17: TLabel
        Left = 748
        Top = 16
        Width = 67
        Height = 13
        Caption = 'Peso (origem):'
        Enabled = False
      end
      object Label20: TLabel
        Left = 836
        Top = 16
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object Label21: TLabel
        Left = 916
        Top = 16
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object Label24: TLabel
        Left = 132
        Top = 56
        Width = 137
        Height = 13
        Caption = 'Fornecedor da met'#233'ria prima:'
        FocusControl = DBEdit16
      end
      object Label26: TLabel
        Left = 16
        Top = 56
        Width = 91
        Height = 13
        Caption = 'S'#233'rie / Ficha RMP:'
        FocusControl = DBEdit18
      end
      object Label51: TLabel
        Left = 648
        Top = 56
        Width = 148
        Height = 13
        Caption = 'Local do (e) centro de estoque:'
        FocusControl = DBEdit39
      end
      object Label52: TLabel
        Left = 932
        Top = 56
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        FocusControl = DBEdit41
      end
      object Label60: TLabel
        Left = 532
        Top = 16
        Width = 39
        Height = 13
        Caption = 'Arquivo:'
        FocusControl = DBEdit45
      end
      object Label61: TLabel
        Left = 432
        Top = 57
        Width = 92
        Height = 13
        Caption = 'Fornecedor de MO:'
        FocusControl = DBEdit47
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 32
        Width = 81
        Height = 21
        DataField = 'Controle'
        DataSource = DsVSRRMAtu
        TabOrder = 0
      end
      object DBEdit6: TDBEdit
        Left = 100
        Top = 32
        Width = 57
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsVSRRMAtu
        TabOrder = 1
      end
      object DBEdit7: TDBEdit
        Left = 156
        Top = 32
        Width = 373
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVSRRMAtu
        TabOrder = 2
      end
      object DBEdit8: TDBEdit
        Left = 576
        Top = 32
        Width = 94
        Height = 21
        DataField = 'CustoMOM2'
        DataSource = DsVSRRMAtu
        TabOrder = 3
      end
      object DBEdit9: TDBEdit
        Left = 672
        Top = 32
        Width = 72
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSRRMAtu
        TabOrder = 4
      end
      object DBEdit10: TDBEdit
        Left = 748
        Top = 32
        Width = 84
        Height = 21
        DataField = 'QtdAntPeso'
        DataSource = DsVSRRMAtu
        TabOrder = 5
      end
      object DBEdit12: TDBEdit
        Left = 836
        Top = 32
        Width = 76
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVSRRMAtu
        TabOrder = 6
      end
      object DBEdit13: TDBEdit
        Left = 916
        Top = 32
        Width = 84
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVSRRMAtu
        TabOrder = 7
      end
      object DBEdit16: TDBEdit
        Left = 132
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Terceiro'
        DataSource = DsVSRRMAtu
        TabOrder = 8
      end
      object DBEdit17: TDBEdit
        Left = 188
        Top = 72
        Width = 240
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsVSRRMAtu
        TabOrder = 9
      end
      object DBEdit18: TDBEdit
        Left = 16
        Top = 72
        Width = 113
        Height = 21
        DataField = 'NO_FICHA'
        DataSource = DsVSRRMAtu
        TabOrder = 10
      end
      object DBEdit39: TDBEdit
        Left = 704
        Top = 72
        Width = 225
        Height = 21
        DataField = 'NO_LOC_CEN'
        DataSource = DsVSRRMAtu
        TabOrder = 11
      end
      object DBEdit40: TDBEdit
        Left = 648
        Top = 72
        Width = 56
        Height = 21
        DataField = 'StqCenLoc'
        DataSource = DsVSRRMAtu
        TabOrder = 12
      end
      object DBEdit41: TDBEdit
        Left = 932
        Top = 72
        Width = 68
        Height = 21
        DataField = 'ReqMovEstq'
        DataSource = DsVSRRMAtu
        TabOrder = 13
      end
      object DBEdit45: TDBEdit
        Left = 532
        Top = 32
        Width = 40
        Height = 21
        DataField = 'NO_TTW'
        DataSource = DsVSRRMAtu
        TabOrder = 14
      end
      object DBEdit46: TDBEdit
        Left = 432
        Top = 72
        Width = 56
        Height = 21
        DataField = 'FornecMO'
        DataSource = DsVSRRMAtu
        TabOrder = 15
      end
      object DBEdit47: TDBEdit
        Left = 488
        Top = 72
        Width = 157
        Height = 21
        DataField = 'NO_FORNEC_MO'
        DataSource = DsVSRRMAtu
        TabOrder = 16
      end
    end
    object PCRRMOri: TPageControl
      Left = 0
      Top = 341
      Width = 1008
      Height = 144
      ActivePage = TsRetornoMO
      Align = alTop
      TabOrder = 3
      object TabSheet1: TTabSheet
        Caption = 'Palltets'
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 728
          Height = 92
          Align = alClient
          DataSource = DsVSRRMOriPallet
          PopupMenu = PMOri
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos origem da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end>
        end
        object GroupBox7: TGroupBox
          Left = 728
          Top = 0
          Width = 272
          Height = 92
          Align = alRight
          Caption = ' Baixas for'#231'adas: '
          TabOrder = 1
          object DBGrid3: TDBGrid
            Left = 2
            Top = 15
            Width = 268
            Height = 75
            Align = alClient
            DataSource = DsForcados
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso Kg'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 56
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'IME-Is'
        ImageIndex = 1
        object DGDadosOri: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 92
          Align = alClient
          DataSource = DsVSRRMOriIMEI
          PopupMenu = PMOri
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'N'#176' RME'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos origem da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaMPAG'
              Title.Caption = 'Nota MPAG'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = 'm'#178' gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaP2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Pesagens de recurtimento'
        ImageIndex = 2
        object DBGrid6: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 92
          Align = alClient
          DataSource = DsEmit
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Pesagem'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataEmis'
              Title.Caption = 'Emiss'#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Pe'#231'as'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Title.Caption = 'Peso KG'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fulao'
              Title.Caption = 'Ful'#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Numero'
              Title.Caption = 'Receita'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME'
              Title.Caption = 'Nome da Receita'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESETOR'
              Title.Caption = 'Setor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECI'
              Title.Caption = 'Dono dos produtos'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Custo'
              Title.Caption = 'Custo Total'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Obs'
              Width = 600
              Visible = True
            end>
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'Outras baixas de insumos'
        ImageIndex = 3
        object DBGrid7: TDBGrid
          Left = 0
          Top = 0
          Width = 517
          Height = 92
          Align = alLeft
          DataSource = DsPQO
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataB'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoTotal'
              Title.Caption = 'Custo'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EmitGru'
              Title.Caption = 'Grupo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EMITGRU'
              Title.Caption = 'Nome Grupo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESETOR'
              Title.Caption = 'Setor'
              Width = 90
              Visible = True
            end>
        end
        object DBGIts: TDBGrid
          Left = 517
          Top = 0
          Width = 483
          Height = 92
          Align = alClient
          DataSource = DsPQOIts
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'OrigemCtrl'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Insumo'
              Title.Alignment = taCenter
              Title.Caption = 'C'#243'digo'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliDest'
              Title.Caption = 'Empresa'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPQ'
              Title.Caption = 'Mercadoria'
              Width = 366
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Title.Alignment = taRightJustify
              Title.Caption = 'Quantidade'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor total'
              Width = 92
              Visible = True
            end>
        end
      end
      object TsEnvioMO: TTabSheet
        Caption = ' Dados do envio para M.O. '
        ImageIndex = 4
        object PnEnvioMO: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 92
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitHeight = 108
          object DBGVSMOEnvEnv: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 529
            Height = 92
            Align = alLeft
            DataSource = DsVSMOEnvEnv
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFEMP_nNF'
                Title.Caption = 'N'#186' NF'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_nCT'
                Title.Caption = 'N'#186' CT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end>
          end
          object DBGVSMOEnvEVmi: TdmkDBGridZTO
            Left = 529
            Top = 0
            Width = 471
            Height = 92
            Align = alClient
            DataSource = DsVSMOEnvEVMI
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VSMovIts'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorFrete'
                Title.Caption = '$ Frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end>
          end
        end
      end
      object TsRetornoMO: TTabSheet
        Caption = ' Retorno da M.O. '
        ImageIndex = 5
        object PnRetornoMO: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 116
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGVSMOEnvRet: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 661
            Height = 116
            Align = alLeft
            DataSource = DsVSMOEnvRet
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_nCT'
                Title.Caption = 'N'#186' CT'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_AreaM2'
                Title.Caption = 'Area m'#178
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_PesoKg'
                Title.Caption = 'Peso kg'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFRMP_SerNF'
                Title.Caption = 'S'#233'r. Ret'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFRMP_nNF'
                Title.Caption = 'NF Retorno'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFCMO_SerNF'
                Title.Caption = 'S'#233'r. M.O.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFCMO_nNF'
                Title.Caption = 'NF M.O.'
                Visible = True
              end>
          end
          object PnItensRetMO: TPanel
            Left = 661
            Top = 0
            Width = 339
            Height = 116
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Splitter2: TSplitter
              Left = 0
              Top = 31
              Width = 339
              Height = 5
              Cursor = crVSplit
              Align = alBottom
              ExplicitLeft = 585
              ExplicitTop = 1
              ExplicitWidth = 208
            end
            object GroupBox8: TGroupBox
              Left = 0
              Top = 0
              Width = 339
              Height = 31
              Align = alClient
              Caption = ' Itens de NFes enviados para M.O.: '
              TabOrder = 0
              object DBGVSMOEnvRVmi: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 335
                Height = 14
                TabStop = False
                Align = alClient
                DataSource = DsVSMOEnvRVmi
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NFEMP_SerNF'
                    Title.Caption = 'S'#233'r. NF'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFEMP_nNF'
                    Title.Caption = 'NF'
                    Width = 65
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor Total'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorFrete'
                    Title.Caption = 'Valor Frete'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID Item'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VSMOEnvEnv'
                    Title.Caption = 'ID Envio'
                    Visible = True
                  end>
              end
            end
            object GroupBox9: TGroupBox
              Left = 0
              Top = 36
              Width = 339
              Height = 80
              Align = alBottom
              Caption = ' IME-Is de couros prontos retornados: '
              TabOrder = 1
              object DBGVSMOEnvGVmi: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 335
                Height = 63
                TabStop = False
                Align = alClient
                DataSource = DsVSMOEnvGVmi
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID Item'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VSMovIts'
                    Title.Caption = 'IME-I'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorFrete'
                    Visible = True
                  end>
              end
            end
          end
        end
      end
    end
    object Panel11: TPanel
      Left = 0
      Top = 281
      Width = 1008
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object Label18: TLabel
        Left = 752
        Top = 20
        Width = 164
        Height = 13
        Caption = 'Observa'#231#227'o sobre o artigo gerado:'
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 745
        Height = 60
        Align = alLeft
        Caption = ' Valores: '
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 741
          Height = 43
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label25: TLabel
            Left = 12
            Top = 0
            Width = 62
            Height = 13
            Caption = '$/kg M.obra:'
          end
          object Label27: TLabel
            Left = 116
            Top = 0
            Width = 53
            Height = 13
            Caption = 'Total M.O.:'
            FocusControl = DBEdit20
          end
          object Label28: TLabel
            Left = 428
            Top = 0
            Width = 50
            Height = 13
            Caption = 'Valor total:'
            FocusControl = DBEdit21
          end
          object Label29: TLabel
            Left = 220
            Top = 0
            Width = 49
            Height = 13
            Caption = 'Valor M.P.'
            FocusControl = DBEdit22
          end
          object Label30: TLabel
            Left = 532
            Top = 0
            Width = 39
            Height = 13
            Caption = 'R$ / m'#178':'
            FocusControl = DBEdit23
          end
          object Label31: TLabel
            Left = 636
            Top = 0
            Width = 37
            Height = 13
            Caption = 'R$ / ft'#178':'
            FocusControl = DBEdit24
          end
          object Label65: TLabel
            Left = 324
            Top = 0
            Width = 48
            Height = 13
            Caption = 'Custo PQ:'
            FocusControl = DBEdit48
          end
          object DBEdit19: TDBEdit
            Left = 12
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CustoMOKg'
            DataSource = DsVSRRMAtu
            TabOrder = 0
          end
          object DBEdit20: TDBEdit
            Left = 116
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CustoMOTot'
            DataSource = DsVSRRMAtu
            TabOrder = 1
          end
          object DBEdit21: TDBEdit
            Left = 428
            Top = 16
            Width = 100
            Height = 21
            DataField = 'ValorT'
            DataSource = DsVSRRMAtu
            TabOrder = 2
          end
          object DBEdit22: TDBEdit
            Left = 220
            Top = 16
            Width = 100
            Height = 21
            DataField = 'ValorMP'
            DataSource = DsVSRRMAtu
            TabOrder = 3
          end
          object DBEdit23: TDBEdit
            Left = 532
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CUSTO_M2'
            DataSource = DsVSRRMAtu
            TabOrder = 4
          end
          object DBEdit24: TDBEdit
            Left = 636
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CUSTO_P2'
            DataSource = DsVSRRMAtu
            TabOrder = 5
          end
          object DBEdit48: TDBEdit
            Left = 324
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CustoPQ'
            DataSource = DsVSRRMAtu
            TabOrder = 6
          end
        end
      end
      object DBEdit11: TDBEdit
        Left = 752
        Top = 36
        Width = 249
        Height = 21
        DataField = 'Observ'
        DataSource = DsVSRRMAtu
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
    object PCRRMDst: TPageControl
      Left = 0
      Top = 497
      Width = 1008
      Height = 100
      ActivePage = TabSheet3
      Align = alBottom
      TabOrder = 5
      object TabSheet3: TTabSheet
        Caption = ' Destino Semi Acabado'
        object Panel12: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 72
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DGDadosDst: TDBGrid
            Left = 0
            Top = 0
            Width = 593
            Height = 72
            Align = alClient
            DataSource = DsVSRRMDst
            PopupMenu = PMDst
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Arquivo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ReqMovEstq'
                Title.Caption = 'N'#176' RME'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PALLET'
                Title.Caption = 'Hist'#243'rico'
                Width = 145
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_SerieFch'
                Title.Caption = 'S'#233'rie RMP'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Artigos destino da opera'#231#227'o'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaMPAG'
                Title.Caption = 'Nota MPAG'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = 'm'#178' gerado'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Valor total'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FORNECE'
                Title.Caption = 'Fornecedor'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Observ'
                Title.Caption = 'Observa'#231#245'es'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end>
          end
          object Panel14: TPanel
            Left = 593
            Top = 0
            Width = 407
            Height = 72
            Align = alRight
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object PCRRMDestSub: TPageControl
              Left = 0
              Top = 0
              Width = 407
              Height = 72
              ActivePage = TabSheet9
              Align = alClient
              TabOrder = 0
              object TabSheet8: TTabSheet
                Caption = ' Baixa destino'
                object DBGrid1: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 399
                  Height = 44
                  Align = alClient
                  DataSource = DsVSRRMBxa
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Title.Caption = 'IME-I'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Pallet'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Pecas'
                      Title.Caption = 'Pe'#231'as'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaM2'
                      Title.Caption = 'm'#178' gerado'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PesoKg'
                      Title.Caption = 'Peso kg'
                      Width = 60
                      Visible = True
                    end>
                end
              end
              object TabSheet9: TTabSheet
                Caption = ' Cobran'#231'a MO'
                ImageIndex = 1
                object dmkDBGridZTO1: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 399
                  Height = 44
                  Align = alClient
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Destino Desclassificado'
        ImageIndex = 1
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 72
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object DBGrid4: TDBGrid
            Left = 1
            Top = 1
            Width = 642
            Height = 70
            Align = alClient
            DataSource = DsVSRRMDesclDst
            PopupMenu = PMDst
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Arquivo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ReqMovEstq'
                Title.Caption = 'N'#176' RME'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PALLET'
                Title.Caption = 'Hist'#243'rico'
                Width = 145
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_SerieFch'
                Title.Caption = 'S'#233'rie RMP'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Artigos destino da opera'#231#227'o'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaMPAG'
                Title.Caption = 'Nota MPAG'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = 'm'#178' gerado'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FORNECE'
                Title.Caption = 'Fornecedor'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Observ'
                Title.Caption = 'Observa'#231#245'es'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end>
          end
          object DBGrid5: TDBGrid
            Left = 643
            Top = 1
            Width = 356
            Height = 70
            Align = alRight
            DataSource = DsVSRRMDesclBxa
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = 'm'#178' gerado'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 60
                Visible = True
              end>
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = 'Destino Sub-produto WB'
        ImageIndex = 2
        object DBGrid8: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 72
          Align = alClient
          DataSource = DsVSRRMSubPrd
          PopupMenu = PMDst
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'N'#176' RME'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PALLET'
              Title.Caption = 'Hist'#243'rico'
              Width = 145
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos destino da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaMPAG'
              Title.Caption = 'Nota MPAG'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = 'm'#178' gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 661
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 252
    ExplicitTop = -7
    object GBEdita1: TGroupBox
      Left = 0
      Top = 56
      Width = 1008
      Height = 137
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label53: TLabel
        Left = 560
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 712
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 182
        Height = 13
        Caption = 'Observa'#231#227'o sobre a opera'#231#227'o (IMEC):'
      end
      object Label56: TLabel
        Left = 448
        Top = 56
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label57: TLabel
        Left = 644
        Top = 56
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label66: TLabel
        Left = 16
        Top = 96
        Width = 88
        Height = 13
        Caption = 'Grupo de emiss'#227'o:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 425
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPData: TdmkEditDateTimePicker
        Left = 560
        Top = 32
        Width = 108
        Height = 21
        Date = 44879.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdHora: TdmkEdit
        Left = 668
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 712
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 429
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrVSGerArt'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGTipoArea: TdmkRadioGroup
        Left = 776
        Top = 16
        Width = 57
        Height = 77
        Caption = ' '#193'rea:'
        ItemIndex = 0
        Items.Strings = (
          'm'#178
          'ft'#178)
        TabOrder = 10
        QryCampo = 'TipoArea'
        UpdCampo = 'TipoArea'
        UpdType = utYes
        OldValor = 0
      end
      object EdLPFMO: TdmkEdit
        Left = 448
        Top = 72
        Width = 193
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNFeRem: TdmkEdit
        Left = 676
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSerieRem: TdmkEdit
        Left = 644
        Top = 72
        Width = 29
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmitGru: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmitGru
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmitGru: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 761
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEmitGru
        TabOrder = 12
        dmkEditCB = EdEmitGru
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 598
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 4
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita2: TGroupBox
      Left = 0
      Top = 193
      Width = 1008
      Height = 184
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object Label3: TLabel
        Left = 16
        Top = 16
        Width = 161
        Height = 13
        Caption = 'Artigo em reprocesso/reparo  [F4]:'
      end
      object Label10: TLabel
        Left = 324
        Top = 136
        Width = 56
        Height = 13
        Caption = '$ Total MO:'
        Enabled = False
      end
      object LaPecas: TLabel
        Left = 112
        Top = 136
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object LaPeso: TLabel
        Left = 188
        Top = 136
        Width = 27
        Height = 13
        Caption = 'Peso:'
        Enabled = False
      end
      object Label12: TLabel
        Left = 372
        Top = 96
        Width = 455
        Height = 13
        Caption = 
          'Observa'#231#227'o sobre o artigo em reprocesso/reparo (IMEI - Ser'#225' impr' +
          'esso na Ordem de Opera'#231#227'o):'
      end
      object Label19: TLabel
        Left = 264
        Top = 136
        Width = 56
        Height = 13
        Caption = 'ID G'#234'meos:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label35: TLabel
        Left = 16
        Top = 56
        Width = 240
        Height = 13
        Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
      end
      object Label48: TLabel
        Left = 16
        Top = 136
        Width = 66
        Height = 13
        Caption = '$/m'#178' MO [F4]:'
      end
      object Label49: TLabel
        Left = 372
        Top = 56
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object Label50: TLabel
        Left = 836
        Top = 56
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label54: TLabel
        Left = 16
        Top = 96
        Width = 56
        Height = 13
        Caption = 'Cliente [F4]:'
      end
      object LaGGXDst: TLabel
        Left = 552
        Top = 16
        Width = 75
        Height = 13
        Caption = 'Artigo reparado:'
      end
      object EdGraGruX: TdmkEditCB
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdGraGruXKeyDown
        OnRedefinido = EdGraGruXRedefinido
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 72
        Top = 32
        Width = 477
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 1
        OnKeyDown = CBGraGruXKeyDown
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCustoMOTot: TdmkEdit
        Left = 324
        Top = 152
        Width = 94
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 17
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPecas: TdmkEdit
        Left = 112
        Top = 152
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPesoKg: TdmkEdit
        Left = 188
        Top = 152
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 15
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdObserv: TdmkEdit
        Left = 372
        Top = 112
        Width = 557
        Height = 21
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMovimTwn: TdmkEdit
        Left = 264
        Top = 152
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdFornecMO: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecMO: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 297
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsPrestador
        TabOrder = 6
        dmkEditCB = EdFornecMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCustoMOM2: TdmkEdit
        Left = 16
        Top = 152
        Width = 94
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnKeyDown = EdCustoMOM2KeyDown
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 372
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 428
        Top = 72
        Width = 405
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_LOC_CEN'
        ListSource = DsStqCenLoc
        TabOrder = 8
        dmkEditCB = EdStqCenLoc
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdReqMovEstq: TdmkEdit
        Left = 836
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdClienteKeyDown
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 297
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCliente
        TabOrder = 11
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkCpermitirCrust: TCheckBox
        Left = 668
        Top = 14
        Width = 141
        Height = 17
        Caption = 'Permitir semi e acabado.'
        TabOrder = 2
        OnClick = CkCpermitirCrustClick
      end
      object EdGGXDst: TdmkEditCB
        Left = 552
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GGXDst'
        UpdCampo = 'GGXDst'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGGXDst
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGGXDst: TdmkDBLookupComboBox
        Left = 608
        Top = 32
        Width = 392
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGGXDst
        TabOrder = 4
        dmkEditCB = EdGGXDst
        QryCampo = 'GGXDst'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBVSPedIts: TGroupBox
      Left = 0
      Top = 377
      Width = 1008
      Height = 64
      Align = alTop
      TabOrder = 3
      object LaPedItsLib: TLabel
        Left = 16
        Top = 12
        Width = 114
        Height = 13
        Caption = 'Item de pedido atrelado:'
      end
      object Label62: TLabel
        Left = 456
        Top = 12
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object EdPedItsLib: TdmkEditCB
        Left = 16
        Top = 28
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPedItsLib
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPedItsLib: TdmkDBLookupComboBox
        Left = 72
        Top = 28
        Width = 381
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsVSPedIts
        TabOrder = 1
        dmkEditCB = EdPedItsLib
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdClientMO: TdmkEditCB
        Left = 456
        Top = 28
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClientMO: TdmkDBLookupComboBox
        Left = 512
        Top = 28
        Width = 489
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientMO
        TabOrder = 3
        dmkEditCB = EdClientMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBConfig: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 56
      Align = alTop
      TabOrder = 0
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 28
        Height = 13
        Caption = 'IME-I:'
      end
      object Label64: TLabel
        Left = 100
        Top = 16
        Width = 123
        Height = 13
        Caption = 'Configura'#231#227'o de inclus'#227'o:'
      end
      object SbVSCOPCabCad: TSpeedButton
        Left = 956
        Top = 32
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbVSCOPCabCadClick
      end
      object SbVSCOPCabCpy: TSpeedButton
        Left = 976
        Top = 32
        Width = 21
        Height = 21
        Caption = '>'
        OnClick = SbVSCOPCabCpyClick
      end
      object EdControle: TdmkEdit
        Left = 16
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdVSCOPCab: TdmkEditCB
        Left = 100
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVSCOPCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBVSCOPCab: TdmkDBLookupComboBox
        Left = 156
        Top = 32
        Width = 797
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSCOPCab
        TabOrder = 2
        dmkEditCB = EdVSCOPCab
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 388
        Height = 32
        Caption = 'Reproceso / Reparo de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 388
        Height = 32
        Caption = 'Reproceso / Reparo de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 388
        Height = 32
        Caption = 'Reproceso / Reparo de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSRRMCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSRRMCabBeforeOpen
    AfterOpen = QrVSRRMCabAfterOpen
    BeforeClose = QrVSRRMCabBeforeClose
    AfterScroll = QrVSRRMCabAfterScroll
    OnCalcFields = QrVSRRMCabCalcFields
    SQL.Strings = (
      'SELECT '
      'IF(PecasMan<>0, PecasMan, PecasSrc) PecasINI,'
      'IF(AreaManM2<>0, AreaManM2, AreaSrcM2) AreaINIM2,'
      'IF(AreaManP2<>0, AreaManP2, AreaSrcM2) AreaINIP2,'
      'IF(PesoKgMan<>0, PesoKgMan, PesoKgSrc) PesoKgINI,'
      'voc.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA'
      'FROM vsopecab voc'
      'LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa'
      'WHERE voc.Codigo > 0')
    Left = 120
    Top = 461
    object QrVSRRMCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'vsopecab.Codigo'
    end
    object QrVSRRMCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Origin = 'vsopecab.MovimCod'
    end
    object QrVSRRMCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'vsopecab.Empresa'
    end
    object QrVSRRMCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSRRMCabDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
      Origin = 'vsopecab.DtHrAberto'
    end
    object QrVSRRMCabDtHrLibOpe: TDateTimeField
      FieldName = 'DtHrLibOpe'
      Origin = 'vsopecab.DtHrLibOpe'
    end
    object QrVSRRMCabDtHrCfgOpe: TDateTimeField
      FieldName = 'DtHrCfgOpe'
      Origin = 'vsopecab.DtHrCfgOpe'
    end
    object QrVSRRMCabDtHrFimOpe: TDateTimeField
      FieldName = 'DtHrFimOpe'
      Origin = 'vsopecab.DtHrFimOpe'
    end
    object QrVSRRMCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'vsopecab.Nome'
      Size = 100
    end
    object QrVSRRMCabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'vsopecab.Lk'
    end
    object QrVSRRMCabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'vsopecab.DataCad'
    end
    object QrVSRRMCabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'vsopecab.DataAlt'
    end
    object QrVSRRMCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'vsopecab.UserCad'
    end
    object QrVSRRMCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'vsopecab.UserAlt'
    end
    object QrVSRRMCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'vsopecab.AlterWeb'
    end
    object QrVSRRMCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'vsopecab.Ativo'
    end
    object QrVSRRMCabPecasMan: TFloatField
      FieldName = 'PecasMan'
      Origin = 'vsopecab.PecasMan'
    end
    object QrVSRRMCabAreaManM2: TFloatField
      FieldName = 'AreaManM2'
      Origin = 'vsopecab.AreaManM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabAreaManP2: TFloatField
      FieldName = 'AreaManP2'
      Origin = 'vsopecab.AreaManP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabTipoArea: TSmallintField
      FieldName = 'TipoArea'
      Origin = 'vsopecab.TipoArea'
    end
    object QrVSRRMCabNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSRRMCabNO_DtHrFimOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimOpe'
      Calculated = True
    end
    object QrVSRRMCabNO_DtHrLibOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibOpe'
      Calculated = True
    end
    object QrVSRRMCabNO_DtHrCfgOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgOpe'
      Calculated = True
    end
    object QrVSRRMCabCacCod: TIntegerField
      FieldName = 'CacCod'
      Origin = 'vsopecab.CacCod'
    end
    object QrVSRRMCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'vsopecab.GraGruX'
    end
    object QrVSRRMCabCustoManMOKg: TFloatField
      FieldName = 'CustoManMOKg'
      Origin = 'vsopecab.CustoManMOKg'
    end
    object QrVSRRMCabCustoManMOTot: TFloatField
      FieldName = 'CustoManMOTot'
      Origin = 'vsopecab.CustoManMOTot'
    end
    object QrVSRRMCabValorManMP: TFloatField
      FieldName = 'ValorManMP'
      Origin = 'vsopecab.ValorManMP'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabValorManT: TFloatField
      FieldName = 'ValorManT'
      Origin = 'vsopecab.ValorManT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabPecasSrc: TFloatField
      FieldName = 'PecasSrc'
      Origin = 'vsopecab.PecasSrc'
    end
    object QrVSRRMCabAreaSrcM2: TFloatField
      FieldName = 'AreaSrcM2'
      Origin = 'vsopecab.AreaSrcM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabAreaSrcP2: TFloatField
      FieldName = 'AreaSrcP2'
      Origin = 'vsopecab.AreaSrcP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabPecasDst: TFloatField
      FieldName = 'PecasDst'
      Origin = 'vsopecab.PecasDst'
    end
    object QrVSRRMCabAreaDstM2: TFloatField
      FieldName = 'AreaDstM2'
      Origin = 'vsopecab.AreaDstM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabAreaDstP2: TFloatField
      FieldName = 'AreaDstP2'
      Origin = 'vsopecab.AreaDstP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabPecasSdo: TFloatField
      FieldName = 'PecasSdo'
      Origin = 'vsopecab.PecasSdo'
    end
    object QrVSRRMCabAreaSdoM2: TFloatField
      FieldName = 'AreaSdoM2'
      Origin = 'vsopecab.AreaSdoM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabAreaSdoP2: TFloatField
      FieldName = 'AreaSdoP2'
      Origin = 'vsopecab.AreaSdoP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabPesoKgSrc: TFloatField
      FieldName = 'PesoKgSrc'
      Origin = 'vsopecab.PesoKgSrc'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSRRMCabPesoKgMan: TFloatField
      FieldName = 'PesoKgMan'
      Origin = 'vsopecab.PesoKgMan'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSRRMCabPesoKgDst: TFloatField
      FieldName = 'PesoKgDst'
      Origin = 'vsopecab.PesoKgDst'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSRRMCabPesoKgSdo: TFloatField
      FieldName = 'PesoKgSdo'
      Origin = 'vsopecab.PesoKgSdo'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSRRMCabValorTMan: TFloatField
      FieldName = 'ValorTMan'
      Origin = 'vsopecab.ValorTMan'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabValorTSrc: TFloatField
      FieldName = 'ValorTSrc'
      Origin = 'vsopecab.ValorTSrc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabValorTSdo: TFloatField
      FieldName = 'ValorTSdo'
      Origin = 'vsopecab.ValorTSdo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabPecasINI: TFloatField
      FieldName = 'PecasINI'
      Required = True
    end
    object QrVSRRMCabAreaINIM2: TFloatField
      FieldName = 'AreaINIM2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabAreaINIP2: TFloatField
      FieldName = 'AreaINIP2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabPesoKgINI: TFloatField
      FieldName = 'PesoKgINI'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSRRMCabPesoKgBxa: TFloatField
      FieldName = 'PesoKgBxa'
      Origin = 'vsopecab.PesoKgBxa'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSRRMCabPecasBxa: TFloatField
      FieldName = 'PecasBxa'
      Origin = 'vsopecab.PecasBxa'
    end
    object QrVSRRMCabAreaBxaM2: TFloatField
      FieldName = 'AreaBxaM2'
      Origin = 'vsopecab.AreaBxaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabAreaBxaP2: TFloatField
      FieldName = 'AreaBxaP2'
      Origin = 'vsopecab.AreaBxaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabValorTBxa: TFloatField
      FieldName = 'ValorTBxa'
      Origin = 'vsopecab.ValorTBxa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSRRMCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSRRMCabNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrVSRRMCabNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrVSRRMCabLPFMO: TWideStringField
      FieldName = 'LPFMO'
      Size = 30
    end
    object QrVSRRMCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSRRMCabGGXDst: TIntegerField
      FieldName = 'GGXDst'
    end
    object QrVSRRMCabSerieRem: TSmallintField
      FieldName = 'SerieRem'
    end
    object QrVSRRMCabVSCOPCab: TIntegerField
      FieldName = 'VSCOPCab'
    end
    object QrVSRRMCabNO_VSCOPCab: TWideStringField
      FieldName = 'NO_VSCOPCab'
      Size = 60
    end
    object QrVSRRMCabEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrVSRRMCabNO_EmitGru: TWideStringField
      FieldName = 'NO_EmitGru'
      Size = 60
    end
  end
  object DsVSRRMCab: TDataSource
    DataSet = QrVSRRMCab
    Left = 120
    Top = 509
  end
  object QrVSRRMAtu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 204
    Top = 461
    object QrVSRRMAtuCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRRMAtuControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRRMAtuMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRRMAtuMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRRMAtuMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRRMAtuEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRRMAtuTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRRMAtuCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRRMAtuMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRRMAtuDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRRMAtuPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRRMAtuGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRRMAtuPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMAtuPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMAtuAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMAtuAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMAtuValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMAtuSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRRMAtuSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRRMAtuSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRRMAtuSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRRMAtuSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRRMAtuSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMAtuSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMAtuObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRRMAtuSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRRMAtuFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRRMAtuMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRRMAtuFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRRMAtuCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSRRMAtuCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSRRMAtuCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMAtuValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMAtuDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRRMAtuDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRRMAtuDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRRMAtuDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRRMAtuQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRRMAtuQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMAtuQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMAtuQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMAtuQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRRMAtuQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMAtuQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMAtuQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMAtuNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRRMAtuNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRRMAtuNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRRMAtuNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRRMAtuID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRRMAtuNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRRMAtuReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRRMAtuCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSRRMAtuCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSRRMAtuNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSRRMAtuMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRRMAtuPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVSRRMAtuStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSRRMAtuNO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
    object QrVSRRMAtuNO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrVSRRMAtuClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSRRMAtuCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMAtuCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
    end
  end
  object DsVSRRMAtu: TDataSource
    DataSet = QrVSRRMAtu
    Left = 200
    Top = 509
  end
  object PMOri: TPopupMenu
    OnPopup = PMOriPopup
    Left = 632
    Top = 516
    object ItsIncluiOri: TMenuItem
      Caption = '&Adiciona artigo de origem'
      Enabled = False
      object porPallet1: TMenuItem
        Caption = 'por &Pallet'
        object Parcial1: TMenuItem
          Caption = '&Parcial'
          OnClick = Parcial1Click
        end
        object otal1: TMenuItem
          Caption = '&Total'
          OnClick = otal1Click
        end
      end
      object porIMEI1: TMenuItem
        Caption = 'por &IME-I'
        object Parcial2: TMenuItem
          Caption = '&Parcial'
          OnClick = Parcial2Click
        end
        object otal2: TMenuItem
          Caption = '&Total'
          OnClick = otal2Click
        end
      end
    end
    object ItsAlteraOri: TMenuItem
      Caption = '&Edita artigo de origem'
      Enabled = False
      Visible = False
      OnClick = ItsAlteraOriClick
    end
    object ItsExcluiOriIMEI: TMenuItem
      Caption = '&Remove IMEI de origem'
      Enabled = False
      OnClick = ItsExcluiOriIMEIClick
    end
    object ItsExcluiOriPallet: TMenuItem
      Caption = '&Remove Pallet de origem'
      Enabled = False
      OnClick = ItsExcluiOriPalletClick
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object AtrelamentoNFsdeMO1: TMenuItem
      Caption = 'Atrelamento NFs de MO'
      object IncluiAtrelamento1: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento1Click
      end
      object AlteraAtrelamento1: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento1Click
      end
      object ExcluiAtrelamento1: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento1Click
      end
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object InformaNmerodaRME1: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      Enabled = False
      OnClick = InformaNmerodaRME1Click
    end
    object IrparacadastrodoPallet1: TMenuItem
      Caption = 'Ir para cadastro do &Pallet'
      OnClick = IrparacadastrodoPallet1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 632
    Top = 468
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CabLibera1: TMenuItem
      Caption = '&Libera classifica'#231#227'o'
      OnClick = CabLibera1Click
    end
    object CabReeditar1: TMenuItem
      Caption = '&Volta a editar'
      Enabled = False
      OnClick = CabReeditar1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object AtualizaestoqueEmProcesso1: TMenuItem
      Caption = 'Atualiza estoque "Em Re&Processo"'
      OnClick = AtualizaestoqueEmProcesso1Click
    end
    object Definiodequantidadesmanualmente1: TMenuItem
      Caption = '&Defini'#231#227'o de quantidades manualmente'
      OnClick = Definiodequantidadesmanualmente1Click
    end
    object CorrigirFornecedor1: TMenuItem
      Caption = 'Corrigir Fornecedor'
      OnClick = CorrigirFornecedor1Click
    end
    object AlteraFornecedorMO1: TMenuItem
      Caption = 'Altera'#231#227'o Parcial'
      object FornecedorMO1: TMenuItem
        Caption = 'Fornecedor M&O'
        OnClick = FornecedorMO1Click
      end
      object Localdoestoque1: TMenuItem
        Caption = '&Local do estoque'
        OnClick = Localdoestoque1Click
      end
      object Cliente1: TMenuItem
        Caption = '&Cliente'
        OnClick = Cliente1Click
      end
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 396
    Top = 36
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 396
    Top = 88
  end
  object QrVSRRMOriIMEI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 288
    Top = 461
    object QrVSRRMOriIMEICodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRRMOriIMEIControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRRMOriIMEIMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRRMOriIMEIMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRRMOriIMEIMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRRMOriIMEIEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRRMOriIMEITerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRRMOriIMEICliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRRMOriIMEIMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRRMOriIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRRMOriIMEIPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRRMOriIMEIGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRRMOriIMEIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMOriIMEIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMOriIMEIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEIValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEISrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRRMOriIMEISrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRRMOriIMEISrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRRMOriIMEISrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRRMOriIMEISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRRMOriIMEISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMOriIMEISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRRMOriIMEISerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRRMOriIMEIFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRRMOriIMEIMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRRMOriIMEIFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRRMOriIMEICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEIDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRRMOriIMEIDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRRMOriIMEIDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRRMOriIMEIDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRRMOriIMEIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRRMOriIMEIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMOriIMEIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRRMOriIMEIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMOriIMEIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriIMEINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRRMOriIMEINO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRRMOriIMEINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRRMOriIMEINO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRRMOriIMEIID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRRMOriIMEINO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRRMOriIMEINO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRRMOriIMEIReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRRMOriIMEIDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object DsVSRRMOriIMEI: TDataSource
    DataSet = QrVSRRMOriIMEI
    Left = 288
    Top = 509
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 312
    Top = 36
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 312
    Top = 80
  end
  object PMNumero: TPopupMenu
    Left = 100
    Top = 65532
    object PeloCdigo1: TMenuItem
      Caption = 'Pela &OP (Ordem de Produ'#231#227'o)'
      OnClick = PeloCdigo1Click
    end
    object PeloIMEC1: TMenuItem
      Caption = 'Pelo I&ME-C'
      OnClick = PeloIMEC1Click
    end
    object PeloIMEI1: TMenuItem
      Caption = 'Pelo IME-&I'
      OnClick = PeloIMEI1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 36
    Top = 12
    object IMEIArtigodeRibeiragerado1: TMenuItem
      Caption = '&IME-I  Ordem de Opera'#231#227'o'
      OnClick = IMEIArtigodeRibeiragerado1Click
    end
    object OPpreenchida1: TMenuItem
      Caption = 'OP preenchida'
      OnClick = OPpreenchida1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object OrdensdeProduoemAberto1: TMenuItem
      Caption = 'Ordens de produ'#231#227'o em &aberto'
      OnClick = OrdensdeProduoemAberto1Click
    end
    object Outrasimpresses1: TMenuItem
      Caption = '&Outras impress'#245'es (outra janela)'
      OnClick = Outrasimpresses1Click
    end
    object CobranadeMO1: TMenuItem
      Caption = 'Cobran'#231'a de MO'
      OnClick = CobranadeMO1Click
    end
  end
  object QrVSRRMDst: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSRRMDstBeforeClose
    AfterScroll = QrVSRRMDstAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 468
    Top = 465
    object QrVSRRMDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRRMDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRRMDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRRMDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRRMDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRRMDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRRMDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRRMDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRRMDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRRMDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRRMDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRRMDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRRMDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRRMDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRRMDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRRMDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRRMDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRRMDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRRMDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRRMDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRRMDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRRMDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRRMDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRRMDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRRMDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRRMDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRRMDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRRMDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRRMDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRRMDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRRMDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRRMDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRRMDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRRMDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRRMDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRRMDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRRMDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSRRMDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRRMDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSRRMDstDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSRRMDstCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
    end
    object QrVSRRMDstCustoMOPc: TFloatField
      FieldName = 'CustoMOPc'
    end
  end
  object DsVSRRMDst: TDataSource
    DataSet = QrVSRRMDst
    Left = 468
    Top = 513
  end
  object PMDst: TPopupMenu
    OnPopup = PMDstPopup
    Left = 632
    Top = 564
    object ItsIncluiDst: TMenuItem
      Caption = '&Adiciona artigo de destino'
      Enabled = False
      OnClick = ItsIncluiDstClick
    end
    object ItsAlteraDst: TMenuItem
      Caption = '&Edita artigo de destino'
      Enabled = False
      OnClick = ItsAlteraDstClick
    end
    object Editaea1: TMenuItem
      Caption = 'Edita '#225'rea de baixa'
      OnClick = Editaea1Click
    end
    object Corrigetodasbaixas1: TMenuItem
      Caption = 'Corrige todas baixas'
      OnClick = Corrigetodasbaixas1Click
    end
    object ItsExcluiDst: TMenuItem
      Caption = '&Remove artigo de destino'
      Enabled = False
      OnClick = ItsExcluiDstClick
    end
    object InformaNmerodaRME2: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      Enabled = False
      OnClick = InformaNmerodaRME2Click
    end
    object AtrelamentoNFsdeMO2: TMenuItem
      Caption = 'Atrelamento NFs de MO'
      object IncluiAtrelamento2: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento2Click
      end
      object AlteraAtrelamento2: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento2Click
      end
      object ExcluiAtrelamento2: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento2Click
      end
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object AdicionasubprodutoWBRaspa1: TMenuItem
      Caption = 'Adiciona sub-produto WB (Ra&spa)'
      OnClick = AdicionasubprodutoWBRaspa1Click
    end
    object RemovesubprodutoWBRaspa1: TMenuItem
      Caption = 'Remove sub-produto WB (Raspa)'
      OnClick = RemovesubprodutoWBRaspa1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Adicionadesclassificado1: TMenuItem
      Caption = 'Adiciona desclassificado'
      OnClick = Adicionadesclassificado1Click
    end
    object Removedesclassificado1: TMenuItem
      Caption = '&Remove desclassificado'
      OnClick = Removedesclassificado1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object IrparajaneladoPallet1: TMenuItem
      Caption = 'Ir para janela do &Pallet'
      OnClick = IrparajaneladoPallet1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object MudarodestinoparaoutraOP1: TMenuItem
      Caption = 'Mudar o destino para outra OP'
      OnClick = MudarodestinoparaoutraOP1Click
    end
  end
  object QrTwn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 840
    Top = 384
    object QrTwnControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrVSRRMBxa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 548
    Top = 465
    object QrVSRRMBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRRMBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRRMBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRRMBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRRMBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRRMBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRRMBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRRMBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRRMBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRRMBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRRMBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRRMBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRRMBxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMBxaPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMBxaAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRRMBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRRMBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRRMBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRRMBxaSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRRMBxaSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMBxaSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRRMBxaSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRRMBxaFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRRMBxaMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRRMBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRRMBxaCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRRMBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRRMBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRRMBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRRMBxaQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRRMBxaQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMBxaQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRRMBxaQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMBxaQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMBxaNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRRMBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRRMBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRRMBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRRMBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRRMBxaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRRMBxaNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRRMBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsVSRRMBxa: TDataSource
    DataSet = QrVSRRMBxa
    Left = 544
    Top = 513
  end
  object QrVSRRMOriPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Pallet, wmi.GraGruX, wmi.Ficha, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, '
      'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg,'
      'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet'
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch '
      'WHERE wmi.MovimCod=662'
      'AND wmi.MovimNiv=7'
      'GROUP BY Pallet'
      'ORDER BY NO_Pallet')
    Left = 376
    Top = 461
    object QrVSRRMOriPalletPallet: TLargeintField
      FieldName = 'Pallet'
    end
    object QrVSRRMOriPalletGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSRRMOriPalletPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSRRMOriPalletAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriPalletAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMOriPalletPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMOriPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRRMOriPalletNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrVSRRMOriPalletSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrVSRRMOriPalletFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrVSRRMOriPalletNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrVSRRMOriPalletID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrVSRRMOriPalletSeries_E_Fichas: TWideStringField
      FieldName = 'Series_E_Fichas'
      Size = 1
    end
    object QrVSRRMOriPalletTerceiro: TLargeintField
      FieldName = 'Terceiro'
    end
    object QrVSRRMOriPalletMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRRMOriPalletNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRRMOriPalletNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRRMOriPalletValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSRRMOriPalletSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRRMOriPalletSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMOriPalletSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsVSRRMOriPallet: TDataSource
    DataSet = QrVSRRMOriPallet
    Left = 376
    Top = 509
  end
  object QrForcados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmovits'
      'WHERE MovimID=9 '
      'AND SrcMovID=11'
      'AND SrcNivel1=5'
      '')
    Left = 908
    Top = 420
    object QrForcadosCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrForcadosControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrForcadosMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrForcadosMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrForcadosMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrForcadosEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrForcadosTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrForcadosCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrForcadosMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrForcadosDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrForcadosPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrForcadosGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrForcadosPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrForcadosSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrForcadosSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrForcadosSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrForcadosSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrForcadosSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrForcadosSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrForcadosFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrForcadosMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrForcadosFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrForcadosCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrForcadosDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrForcadosDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrForcadosDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrForcadosQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrForcadosQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrForcadosQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrForcadosNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrForcadosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrForcadosNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrForcadosID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object DsForcados: TDataSource
    DataSet = QrForcados
    Left = 908
    Top = 468
  end
  object QrVSPedIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.VSWetEnd=0')
    Left = 708
    Top = 468
    object QrVSPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrVSPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsVSPedIts: TDataSource
    DataSet = QrVSPedIts
    Left = 708
    Top = 512
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 236
    Top = 84
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 236
    Top = 36
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object QrCliente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 172
    Top = 36
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 172
    Top = 84
  end
  object PMNovo: TPopupMenu
    Left = 68
    Top = 56
    object CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem
      Caption = 'Corrige &Fornecedores destino a partir deste processo'
      OnClick = CorrigeFornecedoresdestinoapartirdestaoperao1Click
    end
    object CorrigeMO1: TMenuItem
      Caption = 'Corrige &MO'
      OnClick = CorrigeMO1Click
    end
  end
  object QrClientMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 40
    Top = 464
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 40
    Top = 512
  end
  object QrGGXDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 960
    Top = 96
    object QrGGXDstGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXDstControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXDstSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXDstCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXDstNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXDst: TDataSource
    DataSet = QrGGXDst
    Left = 960
    Top = 148
  end
  object QrVSCOPCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM operacoes'
      'ORDER BY Nome')
    Left = 400
    Top = 188
    object QrVSCOPCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCOPCabNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSCOPCab: TDataSource
    DataSet = QrVSCOPCab
    Left = 400
    Top = 240
  end
  object QrVSRRMDesclDst: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrVSRRMDesclDstAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 812
    Top = 461
    object QrVSRRMDesclDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRRMDesclDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRRMDesclDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRRMDesclDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRRMDesclDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRRMDesclDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRRMDesclDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRRMDesclDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRRMDesclDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRRMDesclDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRRMDesclDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRRMDesclDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRRMDesclDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDesclDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDesclDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRRMDesclDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRRMDesclDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRRMDesclDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRRMDesclDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRRMDesclDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDesclDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRRMDesclDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRRMDesclDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRRMDesclDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRRMDesclDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRRMDesclDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRRMDesclDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRRMDesclDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRRMDesclDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRRMDesclDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRRMDesclDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDesclDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRRMDesclDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDesclDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRRMDesclDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRRMDesclDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRRMDesclDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRRMDesclDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRRMDesclDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRRMDesclDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRRMDesclDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRRMDesclDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSRRMDesclDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRRMDesclDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
  end
  object DsVSRRMDesclDst: TDataSource
    DataSet = QrVSRRMDesclDst
    Left = 812
    Top = 509
  end
  object QrVSRRMDesclBxa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 912
    Top = 517
    object QrVSRRMDesclBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRRMDesclBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRRMDesclBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRRMDesclBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRRMDesclBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRRMDesclBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRRMDesclBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRRMDesclBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRRMDesclBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRRMDesclBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRRMDesclBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRRMDesclBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRRMDesclBxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDesclBxaPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDesclBxaAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRRMDesclBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRRMDesclBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRRMDesclBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRRMDesclBxaSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRRMDesclBxaSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDesclBxaSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRRMDesclBxaSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRRMDesclBxaFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRRMDesclBxaMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRRMDesclBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRRMDesclBxaCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRRMDesclBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRRMDesclBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRRMDesclBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRRMDesclBxaQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRRMDesclBxaQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDesclBxaQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRRMDesclBxaQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMDesclBxaQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMDesclBxaNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRRMDesclBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRRMDesclBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRRMDesclBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRRMDesclBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRRMDesclBxaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRRMDesclBxaNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRRMDesclBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsVSRRMDesclBxa: TDataSource
    DataSet = QrVSRRMDesclBxa
    Left = 908
    Top = 565
  end
  object QrEmit: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM emit')
    Left = 756
    Top = 448
    object QrEmitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitDataEmis: TDateTimeField
      FieldName = 'DataEmis'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmitStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrEmitNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrEmitNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEmitNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrEmitTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrEmitNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrEmitClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrEmitTipificacao: TSmallintField
      FieldName = 'Tipificacao'
    end
    object QrEmitTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrEmitTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrEmitSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrEmitTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrEmitEspessura: TWideStringField
      FieldName = 'Espessura'
      Size = 15
    end
    object QrEmitDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Size = 15
    end
    object QrEmitPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEmitQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmitAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEmitFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrEmitObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrEmitSetrEmi: TSmallintField
      FieldName = 'SetrEmi'
    end
    object QrEmitSourcMP: TSmallintField
      FieldName = 'SourcMP'
    end
    object QrEmitCod_Espess: TIntegerField
      FieldName = 'Cod_Espess'
    end
    object QrEmitCodDefPeca: TIntegerField
      FieldName = 'CodDefPeca'
    end
    object QrEmitCustoTo: TFloatField
      FieldName = 'CustoTo'
    end
    object QrEmitCustoKg: TFloatField
      FieldName = 'CustoKg'
    end
    object QrEmitCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
    object QrEmitCusUSM2: TFloatField
      FieldName = 'CusUSM2'
    end
    object QrEmitLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmitDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmitUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmitAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrEmitRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
    object QrEmitSemiCodPeca: TIntegerField
      FieldName = 'SemiCodPeca'
    end
    object QrEmitSemiTxtPeca: TWideStringField
      FieldName = 'SemiTxtPeca'
      Size = 15
    end
    object QrEmitSemiPeso: TFloatField
      FieldName = 'SemiPeso'
    end
    object QrEmitSemiQtde: TFloatField
      FieldName = 'SemiQtde'
    end
    object QrEmitSemiAreaM2: TFloatField
      FieldName = 'SemiAreaM2'
    end
    object QrEmitSemiRendim: TFloatField
      FieldName = 'SemiRendim'
    end
    object QrEmitSemiCodEspe: TIntegerField
      FieldName = 'SemiCodEspe'
    end
    object QrEmitSemiTxtEspe: TWideStringField
      FieldName = 'SemiTxtEspe'
      Size = 15
    end
    object QrEmitBRL_USD: TFloatField
      FieldName = 'BRL_USD'
    end
    object QrEmitBRL_EUR: TFloatField
      FieldName = 'BRL_EUR'
    end
    object QrEmitDtaCambio: TDateField
      FieldName = 'DtaCambio'
    end
    object QrEmitVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
    end
  end
  object DsEmit: TDataSource
    DataSet = QrEmit
    Left = 756
    Top = 496
  end
  object PMPesagem: TPopupMenu
    OnPopup = PMPesagemPopup
    Left = 568
    Top = 636
    object EmitePesagem2: TMenuItem
      Caption = '&Pesagem por receita'
      OnClick = EmitePesagem2Click
      object Emitepesagem1: TMenuItem
        Caption = '&Emite pesagem'
        object Caleirocurtimento1: TMenuItem
          Caption = '&Caleiro/curtimento'
          OnClick = Caleirocurtimento1Click
        end
        object Recurtimento1: TMenuItem
          Caption = '&Recurtimento'
          OnClick = Recurtimento1Click
        end
        object Acabamento1: TMenuItem
          Caption = '&Acabamento'
          OnClick = Acabamento1Click
        end
      end
      object Reimprimereceita1: TMenuItem
        Caption = '&Reimprime receita'
        OnClick = Reimprimereceita1Click
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object ExcluiPesagem1: TMenuItem
        Caption = 'E&xclui Pesagem'
        OnClick = ExcluiPesagem1Click
      end
    end
    object Emiteoutrasbaixas1: TMenuItem
      Caption = '&Outras baixas'
      OnClick = Emiteoutrasbaixas1Click
      object Novogrupo1: TMenuItem
        Caption = 'Nova pesagem'
        OnClick = Novogrupo1Click
      end
      object Novoitem1: TMenuItem
        Caption = 'Novo item'
        OnClick = Novoitem1Click
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object Excluiitem1: TMenuItem
        Caption = 'Exclui item'
        OnClick = Excluiitem1Click
      end
      object Excluigrupo1: TMenuItem
        Caption = 'Exclui toda pesagem'
        OnClick = Excluigrupo1Click
      end
    end
    object Recalculacusto1: TMenuItem
      Caption = 'Recalcula custo'
      OnClick = Recalculacusto1Click
    end
  end
  object QrEmitGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM emitgru'
      'WHERE Ativo=1'
      'ORDER BY Nome'
      '')
    Left = 468
    Top = 37
    object QrEmitGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEmitGru: TDataSource
    DataSet = QrEmitGru
    Left = 468
    Top = 81
  end
  object QrPQO: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPQOBeforeClose
    AfterScroll = QrPQOAfterScroll
    SQL.Strings = (
      'SELECT lse.Nome NOMESETOR,'
      'emg.Nome NO_EMITGRU, pqo.*'
      'FROM pqo pqo'
      'LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor'
      'LEFT JOIN emitgru emg ON emg.Codigo=pqo.EmitGru'
      'WHERE pqo.Codigo > 0'
      '')
    Left = 548
    Top = 221
    object QrPQOCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQOSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrPQOCustoInsumo: TFloatField
      FieldName = 'CustoInsumo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQOCustoTotal: TFloatField
      FieldName = 'CustoTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQODataB: TDateField
      FieldName = 'DataB'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQOLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQODataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQODataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQOUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQOUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQONOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrPQONO_EMITGRU: TWideStringField
      FieldName = 'NO_EMITGRU'
      Size = 60
    end
    object QrPQOAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQOAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQOEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrPQONome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQO: TDataSource
    DataSet = QrPQO
    Left = 548
    Top = 265
  end
  object QrPQOIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico, '
      'pqg.Nome NOMEGRUPO'
      'FROM pqx pqx'
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo'
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ'
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico'
      'WHERE pqx.Tipo=190'
      'AND pqx.OrigemCodi=:P0')
    Left = 596
    Top = 225
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQOItsDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQOItsTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPQOItsCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQOItsCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQOItsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQOItsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQOItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQOItsOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQOItsOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQOItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQOItsGrupoQuimico: TIntegerField
      FieldName = 'GrupoQuimico'
    end
    object QrPQOItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
  end
  object DsPQOIts: TDataSource
    DataSet = QrPQOIts
    Left = 596
    Top = 273
  end
  object QrVSRRMSubPrd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 772
    Top = 573
    object QrVSRRMSubPrdCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRRMSubPrdControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRRMSubPrdMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRRMSubPrdMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRRMSubPrdMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRRMSubPrdEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRRMSubPrdTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRRMSubPrdCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRRMSubPrdMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRRMSubPrdDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRRMSubPrdPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRRMSubPrdGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRRMSubPrdPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMSubPrdPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMSubPrdAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRRMSubPrdSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRRMSubPrdSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRRMSubPrdSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRRMSubPrdSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRRMSubPrdSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMSubPrdSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRRMSubPrdSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRRMSubPrdFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRRMSubPrdMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRRMSubPrdFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRRMSubPrdCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRRMSubPrdDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRRMSubPrdDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRRMSubPrdDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRRMSubPrdQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRRMSubPrdQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMSubPrdQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRRMSubPrdQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRRMSubPrdQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRRMSubPrdNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRRMSubPrdNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRRMSubPrdNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRRMSubPrdNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRRMSubPrdID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRRMSubPrdNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRRMSubPrdNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSRRMSubPrdReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRRMSubPrdPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSRRMSubPrdMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRRMSubPrdStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
  end
  object DsVSRRMSubPrd: TDataSource
    DataSet = QrVSRRMSubPrd
    Left = 772
    Top = 621
  end
  object QrVSMOEnvEnv: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvEnvBeforeClose
    AfterScroll = QrVSMOEnvEnvAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 600
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEnvNFEMP_FatID: TIntegerField
      FieldName = 'NFEMP_FatID'
    end
    object QrVSMOEnvEnvNFEMP_FatNum: TIntegerField
      FieldName = 'NFEMP_FatNum'
    end
    object QrVSMOEnvEnvNFEMP_Empresa: TIntegerField
      FieldName = 'NFEMP_Empresa'
    end
    object QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField
      FieldName = 'NFEMP_Terceiro'
    end
    object QrVSMOEnvEnvNFEMP_nItem: TIntegerField
      FieldName = 'NFEMP_nItem'
    end
    object QrVSMOEnvEnvNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrVSMOEnvEnvNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrVSMOEnvEnvNFEMP_Pecas: TFloatField
      FieldName = 'NFEMP_Pecas'
    end
    object QrVSMOEnvEnvNFEMP_PesoKg: TFloatField
      FieldName = 'NFEMP_PesoKg'
    end
    object QrVSMOEnvEnvNFEMP_AreaM2: TFloatField
      FieldName = 'NFEMP_AreaM2'
    end
    object QrVSMOEnvEnvNFEMP_AreaP2: TFloatField
      FieldName = 'NFEMP_AreaP2'
    end
    object QrVSMOEnvEnvNFEMP_ValorT: TFloatField
      FieldName = 'NFEMP_ValorT'
    end
    object QrVSMOEnvEnvCFTMP_FatID: TIntegerField
      FieldName = 'CFTMP_FatID'
    end
    object QrVSMOEnvEnvCFTMP_FatNum: TIntegerField
      FieldName = 'CFTMP_FatNum'
    end
    object QrVSMOEnvEnvCFTMP_Empresa: TIntegerField
      FieldName = 'CFTMP_Empresa'
    end
    object QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField
      FieldName = 'CFTMP_Terceiro'
    end
    object QrVSMOEnvEnvCFTMP_nItem: TIntegerField
      FieldName = 'CFTMP_nItem'
    end
    object QrVSMOEnvEnvCFTMP_SerCT: TIntegerField
      FieldName = 'CFTMP_SerCT'
    end
    object QrVSMOEnvEnvCFTMP_nCT: TIntegerField
      FieldName = 'CFTMP_nCT'
    end
    object QrVSMOEnvEnvCFTMP_Pecas: TFloatField
      FieldName = 'CFTMP_Pecas'
    end
    object QrVSMOEnvEnvCFTMP_PesoKg: TFloatField
      FieldName = 'CFTMP_PesoKg'
    end
    object QrVSMOEnvEnvCFTMP_AreaM2: TFloatField
      FieldName = 'CFTMP_AreaM2'
    end
    object QrVSMOEnvEnvCFTMP_AreaP2: TFloatField
      FieldName = 'CFTMP_AreaP2'
    end
    object QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField
      FieldName = 'CFTMP_PesTrKg'
    end
    object QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField
      FieldName = 'CFTMP_CusTrKg'
    end
    object QrVSMOEnvEnvCFTMP_ValorT: TFloatField
      FieldName = 'CFTMP_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvEnvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEnvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEnvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEnvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEnvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEnvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEnvAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEnvAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEnvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvEnv: TDataSource
    DataSet = QrVSMOEnvEnv
    Left = 600
    Top = 56
  end
  object QrVSMOEnvEVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmoenvevmi'
      'WHERE VSMOEnvEnv=:p0')
    Left = 684
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvEVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
    object QrVSMOEnvEVMIVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvEVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEVMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEVMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvEVMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrVSMOEnvEVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSMOEnvEVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSMOEnvEVMI: TDataSource
    DataSet = QrVSMOEnvEVMI
    Left = 684
    Top = 56
  end
  object QrVSMOEnvRVmi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vee.NFEMP_SerNF, vee.NFEMP_nNF, mev.* '
      'FROM vsmoenvrvmi mev'
      'LEFT JOIN vsmoenvenv vee ON vee.Codigo=mev.VSMOEnvEnv')
    Left = 852
    Top = 8
    object QrVSMOEnvRVmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvRVmiVSMOEnvRet: TIntegerField
      FieldName = 'VSMOEnvRet'
    end
    object QrVSMOEnvRVmiVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
    object QrVSMOEnvRVmiPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRVmiPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRVmiAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvRVmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvRVmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvRVmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvRVmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvRVmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvRVmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvRVmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvRVmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvRVmiNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrVSMOEnvRVmiNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrVSMOEnvRVmiValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsVSMOEnvRVmi: TDataSource
    DataSet = QrVSMOEnvRVmi
    Left = 852
    Top = 56
  end
  object QrVSMOEnvGVmi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vsmoenvgvmi')
    Left = 940
    Top = 8
    object QrVSMOEnvGVmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvGVmiVSMOEnvRet: TIntegerField
      FieldName = 'VSMOEnvRet'
    end
    object QrVSMOEnvGVmiVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvGVmiPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvGVmiPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvGVmiAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvGVmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvGVmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvGVmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvGVmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvGVmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvGVmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvGVmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvGVmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvGVmiVSMovimCod: TIntegerField
      FieldName = 'VSMovimCod'
    end
  end
  object DsVSMOEnvGVmi: TDataSource
    DataSet = QrVSMOEnvGVmi
    Left = 940
    Top = 56
  end
  object QrVSMOEnvRet: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvRetBeforeClose
    AfterScroll = QrVSMOEnvRetAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvret cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 768
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvRetNFCMO_FatID: TIntegerField
      FieldName = 'NFCMO_FatID'
    end
    object QrVSMOEnvRetNFCMO_FatNum: TIntegerField
      FieldName = 'NFCMO_FatNum'
    end
    object QrVSMOEnvRetNFCMO_Empresa: TIntegerField
      FieldName = 'NFCMO_Empresa'
    end
    object QrVSMOEnvRetNFCMO_nItem: TIntegerField
      FieldName = 'NFCMO_nItem'
    end
    object QrVSMOEnvRetNFCMO_SerNF: TIntegerField
      FieldName = 'NFCMO_SerNF'
    end
    object QrVSMOEnvRetNFCMO_nNF: TIntegerField
      FieldName = 'NFCMO_nNF'
    end
    object QrVSMOEnvRetNFCMO_Pecas: TFloatField
      FieldName = 'NFCMO_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetNFCMO_PesoKg: TFloatField
      FieldName = 'NFCMO_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetNFCMO_AreaM2: TFloatField
      FieldName = 'NFCMO_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_AreaP2: TFloatField
      FieldName = 'NFCMO_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_CusMOM2: TFloatField
      FieldName = 'NFCMO_CusMOM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_CusMOKG: TFloatField
      FieldName = 'NFCMO_CusMOKG'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_ValorT: TFloatField
      FieldName = 'NFCMO_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_Terceiro: TIntegerField
      FieldName = 'NFCMO_Terceiro'
    end
    object QrVSMOEnvRetNFRMP_FatID: TIntegerField
      FieldName = 'NFRMP_FatID'
    end
    object QrVSMOEnvRetNFRMP_FatNum: TIntegerField
      FieldName = 'NFRMP_FatNum'
    end
    object QrVSMOEnvRetNFRMP_Empresa: TIntegerField
      FieldName = 'NFRMP_Empresa'
    end
    object QrVSMOEnvRetNFRMP_nItem: TIntegerField
      FieldName = 'NFRMP_nItem'
    end
    object QrVSMOEnvRetNFRMP_SerNF: TIntegerField
      FieldName = 'NFRMP_SerNF'
    end
    object QrVSMOEnvRetNFRMP_nNF: TIntegerField
      FieldName = 'NFRMP_nNF'
    end
    object QrVSMOEnvRetNFRMP_Pecas: TFloatField
      FieldName = 'NFRMP_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetNFRMP_PesoKg: TFloatField
      FieldName = 'NFRMP_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetNFRMP_AreaM2: TFloatField
      FieldName = 'NFRMP_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_AreaP2: TFloatField
      FieldName = 'NFRMP_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_ValorT: TFloatField
      FieldName = 'NFRMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_Terceiro: TIntegerField
      FieldName = 'NFRMP_Terceiro'
    end
    object QrVSMOEnvRetCFTPA_FatID: TIntegerField
      FieldName = 'CFTPA_FatID'
    end
    object QrVSMOEnvRetCFTPA_FatNum: TIntegerField
      FieldName = 'CFTPA_FatNum'
    end
    object QrVSMOEnvRetCFTPA_Empresa: TIntegerField
      FieldName = 'CFTPA_Empresa'
    end
    object QrVSMOEnvRetCFTPA_Terceiro: TIntegerField
      FieldName = 'CFTPA_Terceiro'
    end
    object QrVSMOEnvRetCFTPA_nItem: TIntegerField
      FieldName = 'CFTPA_nItem'
    end
    object QrVSMOEnvRetCFTPA_SerCT: TIntegerField
      FieldName = 'CFTPA_SerCT'
    end
    object QrVSMOEnvRetCFTPA_nCT: TIntegerField
      FieldName = 'CFTPA_nCT'
    end
    object QrVSMOEnvRetCFTPA_Pecas: TFloatField
      FieldName = 'CFTPA_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetCFTPA_PesoKg: TFloatField
      FieldName = 'CFTPA_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetCFTPA_AreaM2: TFloatField
      FieldName = 'CFTPA_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_AreaP2: TFloatField
      FieldName = 'CFTPA_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_PesTrKg: TFloatField
      FieldName = 'CFTPA_PesTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_CusTrKg: TFloatField
      FieldName = 'CFTPA_CusTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_ValorT: TFloatField
      FieldName = 'CFTPA_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsVSMOEnvRet: TDataSource
    DataSet = QrVSMOEnvRet
    Left = 768
    Top = 56
  end
end
