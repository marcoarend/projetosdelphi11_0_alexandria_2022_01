unit VSCPMRSBCb;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker;

type
  TFmVSCPMRSBCb = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrVSCPMRSBCb: TmySQLQuery;
    DsVSCPMRSBCb: TDataSource;
    QrVSCPMRSBIt: TmySQLQuery;
    DsVSCPMRSBIt: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrVSCPMRSBCbCodigo: TIntegerField;
    QrVSCPMRSBCbNome: TWideStringField;
    QrVSCPMRSBCbEmpresa: TIntegerField;
    QrVSCPMRSBCbDataIni: TDateField;
    QrVSCPMRSBCbDataFim: TDateField;
    QrVSCPMRSBCbLk: TIntegerField;
    QrVSCPMRSBCbDataCad: TDateField;
    QrVSCPMRSBCbDataAlt: TDateField;
    QrVSCPMRSBCbUserCad: TIntegerField;
    QrVSCPMRSBCbUserAlt: TIntegerField;
    QrVSCPMRSBCbAlterWeb: TSmallintField;
    QrVSCPMRSBCbAtivo: TSmallintField;
    QrVSCPMRSBCbNO_EMP: TWideStringField;
    QrVSCPMRSBItCodigo: TIntegerField;
    QrVSCPMRSBItControle: TIntegerField;
    QrVSCPMRSBItGraGruX: TIntegerField;
    QrVSCPMRSBItDivKgSPpcCou: TFloatField;
    QrVSCPMRSBItPercKgSPKgCou: TFloatField;
    QrVSCPMRSBItLk: TIntegerField;
    QrVSCPMRSBItDataCad: TDateField;
    QrVSCPMRSBItDataAlt: TDateField;
    QrVSCPMRSBItUserCad: TIntegerField;
    QrVSCPMRSBItUserAlt: TIntegerField;
    QrVSCPMRSBItAlterWeb: TSmallintField;
    QrVSCPMRSBItAtivo: TSmallintField;
    QrVSCPMRSBItNO_PRD_TAM_COR: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrVSCPMRSBCbDataIni_TXT: TWideStringField;
    QrVSCPMRSBCbDataFim_TXT: TWideStringField;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    Label6: TLabel;
    TPDataFim: TdmkEditDateTimePicker;
    Label10: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSCPMRSBCbAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSCPMRSBCbBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSCPMRSBCbAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSCPMRSBCbBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraVSCPMRSBIt(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenVSCPMRSBIt(Controle: Integer);

  end;

var
  FmVSCPMRSBCb: TFmVSCPMRSBCb;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSCPMRSBIt, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSCPMRSBCb.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSCPMRSBCb.MostraVSCPMRSBIt(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSCPMRSBIt, FmVSCPMRSBIt, afmoNegarComAviso) then
  begin
    FmVSCPMRSBIt.ImgTipo.SQLType := SQLType;
    FmVSCPMRSBIt.FQrCab := QrVSCPMRSBCb;
    FmVSCPMRSBIt.FDsCab := DsVSCPMRSBCb;
    FmVSCPMRSBIt.FQrIts := QrVSCPMRSBIt;
    if SQLType = stIns then
    else
    begin
      FmVSCPMRSBIt.EdControle.ValueVariant := QrVSCPMRSBItControle.Value;
      FmVSCPMRSBIt.EdGraGruX.ValueVariant := QrVSCPMRSBItGragruX.Value;
      FmVSCPMRSBIt.CBGraGruX.KeyValue := QrVSCPMRSBItGragruX.Value;
      FmVSCPMRSBIt.EdDivKgSPpcCou.ValueVariant := QrVSCPMRSBItDivKgSPpcCou.Value;
      FmVSCPMRSBIt.EdPercKgSPKgCou.ValueVariant := QrVSCPMRSBItPercKgSPKgCou.Value;
    end;
    FmVSCPMRSBIt.ShowModal;
    FmVSCPMRSBIt.Destroy;
  end;
end;

procedure TFmVSCPMRSBCb.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSCPMRSBCb);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSCPMRSBCb, nil);
end;

procedure TFmVSCPMRSBCb.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSCPMRSBCb);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSCPMRSBIt);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSCPMRSBIt);
end;

procedure TFmVSCPMRSBCb.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSCPMRSBCbCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSCPMRSBCb.DefParams;
begin
  VAR_GOTOTABELA := 'vscpmrsbcb';
  VAR_GOTOMYSQLTABLE := QrVSCPMRSBCb;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cab.*, ');
  VAR_SQLx.Add('IF(DataIni<"1900-01-01", "", DATE_FORMAT(DataIni, "%d/%m/%Y")) DataIni_TXT, ');
  VAR_SQLx.Add('IF(DataFim<"1900-01-01", "", DATE_FORMAT(DataFIm, "%d/%m/%Y")) DataFIm_TXT, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, RazaoSocial, emp.Nome) NO_EMP  ');
  VAR_SQLx.Add('FROM vscpmrsbcb cab ');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=cab.Empresa ');
  VAR_SQLx.Add('WHERE cab.Codigo > 0');
  //
  VAR_SQL1.Add('AND cab.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cab.Nome Like :P0');
  //
end;

procedure TFmVSCPMRSBCb.ItsAltera1Click(Sender: TObject);
begin
  MostraVSCPMRSBIt(stUpd);
end;

procedure TFmVSCPMRSBCb.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSCPMRSBCbCodigo.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o de toda configura��o selecionada?',
  'VSCPMRSBIt', 'Codigo', Codigo, Dmod.MyDB) = ID_YES then
  begin
    UMyMod.ExcluiRegistroInt1('', 'VSCPMRSBCb', 'Codigo', Codigo, Dmod.MyDB);
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSCPMRSBCb,
      QrVSCPMRSBCbCodigo, QrVSCPMRSBCbCodigo.Value);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSCPMRSBCb.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSCPMRSBCb.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSCPMRSBCb.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'VSCPMRSBIt', 'Controle', QrVSCPMRSBItControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSCPMRSBIt,
      QrVSCPMRSBItControle, QrVSCPMRSBItControle.Value);
    ReopenVSCPMRSBIt(Controle);
  end;
end;

procedure TFmVSCPMRSBCb.ReopenVSCPMRSBIt(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCPMRSBIt, Dmod.MyDB, [
  'SELECT its.*, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR ',
  'FROM vscpmrsbit its ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=its.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE its.Codigo=' + Geral.FF0(QrVSCPMRSBCbCodigo.Value),
  '']);
  //
  QrVSCPMRSBIt.Locate('Controle', Controle, []);
end;


procedure TFmVSCPMRSBCb.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSCPMRSBCb.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSCPMRSBCb.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSCPMRSBCb.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSCPMRSBCb.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSCPMRSBCb.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCPMRSBCb.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSCPMRSBCbCodigo.Value;
  Close;
end;

procedure TFmVSCPMRSBCb.ItsInclui1Click(Sender: TObject);
begin
  MostraVSCPMRSBIt(stIns);
end;

procedure TFmVSCPMRSBCb.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSCPMRSBCb, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vscpmrsbcb');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSCPMRSBCbEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSCPMRSBCb.BtConfirmaClick(Sender: TObject);
var
  Nome, DataIni, DataFim: String;
  Codigo, Empresa: Integer;
  SQLType: TSQLType;
  Erro: Boolean;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DataIni        := Geral.FDT(TPDataIni.Date, 1);
  DataFim        := Geral.FDT(TPDataFim.Date, 1);
  //
  Erro := Trunc(TPDataIni.Date) > Trunc(TPDataFim.Date);
  Erro := Erro and (Trunc(TPDataFim.Date) > 0);
  //
  // Empresa = 0 = todas!
  //if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Empresa n�o definida!') then Exit;
  if MyObjects.FIC(Erro, TPDataIni,
  'Data inicial n�o pode ser maior que data final!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vscpmrsbcb', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscpmrsbcb', False, [
  'Nome', 'Empresa', 'DataIni',
  'DataFim'], [
  'Codigo'], [
  Nome, Empresa, DataIni,
  DataFim], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if SQLType = stIns then
        MostraVSCPMRSBIt(stIns);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSCPMRSBCb.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vscpmrsbcb', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vscpmrsbcb', 'Codigo');
end;

procedure TFmVSCPMRSBCb.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSCPMRSBCb.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSCPMRSBCb.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  TPDataIni.Date := DmodG.ObtemAgora();
  TPDataFim.Date := 0;
end;

procedure TFmVSCPMRSBCb.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSCPMRSBCbCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSCPMRSBCb.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSCPMRSBCb.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSCPMRSBCbCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSCPMRSBCb.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSCPMRSBCb.QrVSCPMRSBCbAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSCPMRSBCb.QrVSCPMRSBCbAfterScroll(DataSet: TDataSet);
begin
  ReopenVSCPMRSBIt(0);
end;

procedure TFmVSCPMRSBCb.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSCPMRSBCbCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSCPMRSBCb.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSCPMRSBCbCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vscpmrsbcb', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSCPMRSBCb.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCPMRSBCb.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSCPMRSBCb, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vscpmrsbcb');
end;

procedure TFmVSCPMRSBCb.QrVSCPMRSBCbBeforeClose(
  DataSet: TDataSet);
begin
  QrVSCPMRSBIt.Close;
end;

procedure TFmVSCPMRSBCb.QrVSCPMRSBCbBeforeOpen(DataSet: TDataSet);
begin
  QrVSCPMRSBCbCodigo.DisplayFormat := FFormatFloat;
end;

end.

