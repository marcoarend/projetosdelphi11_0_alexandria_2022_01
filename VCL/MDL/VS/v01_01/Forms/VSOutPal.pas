unit VSOutPal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO,
  Vcl.Mask, dmkDBEdit, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UnProjGroup_Consts, BlueDermConsts, dmkEditCalc, UnAppEnums;

type
  TFmVSOutPal = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBG04Estq: TdmkDBGridZTO;
    QrEstqR4: TmySQLQuery;
    QrEstqR4Empresa: TIntegerField;
    QrEstqR4GraGruX: TIntegerField;
    QrEstqR4Pecas: TFloatField;
    QrEstqR4PesoKg: TFloatField;
    QrEstqR4AreaM2: TFloatField;
    QrEstqR4AreaP2: TFloatField;
    QrEstqR4GraGru1: TIntegerField;
    QrEstqR4NO_PRD_TAM_COR: TWideStringField;
    QrEstqR4Terceiro: TIntegerField;
    QrEstqR4NO_FORNECE: TWideStringField;
    QrEstqR4Pallet: TIntegerField;
    QrEstqR4NO_PALLET: TWideStringField;
    QrEstqR4ValorT: TFloatField;
    QrEstqR4Ativo: TSmallintField;
    QrEstqR4OrdGGX: TIntegerField;
    QrEstqR4NO_STATUS: TWideStringField;
    DsEstqR4: TDataSource;
    Panel7: TPanel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdDtEntrada: TdmkDBEdit;
    DBEdCliVenda: TdmkDBEdit;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsNO_Pallet: TWideStringField;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TIntegerField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsGraGruY: TIntegerField;
    QrEstqR4SdoVrtPeca: TFloatField;
    QrEstqR4SdoVrtPeso: TFloatField;
    QrEstqR4SdoVrtArM2: TFloatField;
    QrVSMovItsPedItsLib: TIntegerField;
    QrVSMovItsPedItsFin: TIntegerField;
    QrVSMovItsPedItsVda: TIntegerField;
    QrEstqR4MediaM2: TFloatField;
    QrEstqR4DataHora: TDateTimeField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    LaTerceiro: TLabel;
    LaFicha: TLabel;
    Label11: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    EdFicha: TdmkEdit;
    BtReabre: TBitBtn;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    CkEspecificos: TCheckBox;
    EdEspecificos: TdmkEdit;
    QrVSMovItsClientMO: TIntegerField;
    Panel8: TPanel;
    LaPecas: TLabel;
    EdSdoVrtPeca: TdmkEdit;
    LaAreaM2: TLabel;
    EdSdoVrtArM2: TdmkEditCalc;
    LaPeso: TLabel;
    EdSdoVrtPeso: TdmkEdit;
    Label4: TLabel;
    EdSdoVrtValr: TdmkEdit;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    Label53: TLabel;
    CBStqCenCad: TdmkDBLookupComboBox;
    EdStqCenCad: TdmkEditCB;
    QrEstqR4NFeSer: TSmallintField;
    QrEstqR4NFeNum: TIntegerField;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    EdStqCenLoc: TdmkEditCB;
    Label49: TLabel;
    CBStqCenLoc: TdmkDBLookupComboBox;
    QrVSMovimID: TmySQLQuery;
    QrVSMovimIDCodigo: TIntegerField;
    QrVSMovimIDNome: TWideStringField;
    DsVSMovimID: TDataSource;
    EdMovimID: TdmkEditCB;
    Label10: TLabel;
    CBMovimID: TdmkDBLookupComboBox;
    EdIMEC: TdmkEdit;
    Label12: TLabel;
    Panel9: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    RGIxxMovIX: TRadioGroup;
    EdIxxFolha: TdmkEdit;
    EdIxxLinha: TdmkEdit;
    ListBox1: TListBox;
    QrVSPedIts: TMySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    DsVSPedIts: TDataSource;
    Label48: TLabel;
    EdPedItsVda: TdmkEditCB;
    CBPedItsVda: TdmkDBLookupComboBox;
    EdItemNFe: TdmkEdit;
    Label1: TLabel;
    EdPrecoVal: TdmkEdit;
    Label6: TLabel;
    EdMdaTotal: TdmkEdit;
    Label9: TLabel;
    QrVSPedItsGraGruX: TIntegerField;
    QrCambioMda: TMySQLQuery;
    QrCambioMdaCodigo: TIntegerField;
    QrCambioMdaCodUsu: TIntegerField;
    QrCambioMdaSigla: TWideStringField;
    QrCambioMdaNome: TWideStringField;
    DsCambioMda: TDataSource;
    Label22: TLabel;
    EdPrecoMoeda: TdmkEditCB;
    CBPrecoMoeda: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdCambio: TdmkEdit;
    Label14: TLabel;
    EdNacTotal: TdmkEdit;
    QrVSPedItsPrecoTipo: TSmallintField;
    QrVSPedItsPrecoMoeda: TIntegerField;
    QrVSPedItsPrecoVal: TFloatField;
    QrVSPedItsPercDesco: TFloatField;
    QrVSPedItsSaldoQtd: TFloatField;
    EdPercDesco: TdmkEdit;
    Label15: TLabel;
    EdPercTrib: TdmkEdit;
    Label16: TLabel;
    QrVSPedItsPercTrib: TFloatField;
    EdMdaBruto: TdmkEdit;
    Label17: TLabel;
    EdMdaLiqui: TdmkEdit;
    Label18: TLabel;
    EdNacBruto: TdmkEdit;
    Label23: TLabel;
    EdNacLiqui: TdmkEdit;
    Label24: TLabel;
    EdSdoVrtArP2: TdmkEditCalc;
    Label25: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure DBG04EstqAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure QrStqCenCadBeforeClose(DataSet: TDataSet);
    procedure QrStqCenCadAfterScroll(DataSet: TDataSet);
    procedure EdPrecoValChange(Sender: TObject);
    procedure EdPercDescoChange(Sender: TObject);
    procedure EdPercTribChange(Sender: TObject);
    procedure EdCambioChange(Sender: TObject);
    procedure EdSdoVrtPecaChange(Sender: TObject);
    procedure EdSdoVrtArM2Change(Sender: TObject);
    procedure EdSdoVrtArP2Change(Sender: TObject);
    procedure EdSdoVrtPesoChange(Sender: TObject);
    procedure EdPedItsVdaRedefinido(Sender: TObject);
    procedure EdPrecoMoedaRedefinido(Sender: TObject);
    procedure EdPrecoValKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPercDescoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPercTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdItemNFeRedefinido(Sender: TObject);
  private
    { Private declarations }
    FVSMovImp4(*, FVSMovImp5*): String;
    FOrdemSelecionados: array of Integer;
    procedure AdicionaPallet();
    procedure InsereIMEI_Atual();
    procedure ReopenVSMovIts();
    procedure CalculaValorFaturado();
    procedure DefineDadosItemPedido();
    procedure InsereVSOutFat(Controle: Integer; Pecas, AreaM2, AreaP2, PesoKg:
              Double);
  public
    { Public declarations }
    FEmpresa, FPedido: Integer;
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure PesquisaPallets();
    procedure SomaSelecionados();
  end;

  var
  FmVSOutPal: TFmVSOutPal;

implementation

uses UnMyObjects, Module, ModuleGeral, UnVS_CRC_PF, VSOutCab, UMySQLModule,
{$IfDef sAllVS}UnVS_PF, {$EndIf}
DmkDAC_PF, UnDmkProcFunc, MyDBCheck;

{$R *.DFM}

procedure TFmVSOutPal.AdicionaPallet();
begin
  ReopenVSMovIts();
  QrVSMovIts.First;
  while not QrVSMovIts.Eof do
  begin
    InsereIMEI_Atual();
    QrVSMovIts.Next;
  end;
end;

procedure TFmVSOutPal.BtOKClick(Sender: TObject);
var
  N, I, GGXRcl, ItemNFe, QtdDifGGX: Integer;
  IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha, PedItsVda: Integer;
begin
  //
  GGXRcl := EdGGXRcl.ValueVariant;
  if MyObjects.FIC(GGXRcl = 0, EdGGXRcl,
  'Informe o material usado para emitir NFe!') then
    Exit;
  //
  ItemNFe := EdItemNFe.ValueVariant;
  if MyObjects.FIC(ItemNFe = 0, EdItemNFe,
  'Informe o item da NFe!') then
    Exit;
  //
  PedItsVda      := EdPedItsVda.ValueVariant;
  if MyObjects.FIC((FPedido <> 0) and (PedItsVda = 0), EdPedItsVda,
  'Item de pedido n�o informado para o pedido desta sa�da!') then
  begin
    //if Geral.MB_Pergunta('Deseja continuar sem informar o item do pedido!') <> ID_YES then
      Exit;
  end;
  if PedItsVda > 0 then
  begin
    QtdDifGGX := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      //
      if QrEstqR4GraGruX.Value <> QrVSPedItsGraGruX.Value then
        QtdDifGGX := QtdDifGGX + 1;
    end;
    if QtdDifGGX > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(QtdDifGGX) +
      ' itens em que o reduzido difere entre o pedido e o selecionado!' +
      sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then Exit;
    end;
  end;
  IxxMovIX       := TEstqMovInfo(RGIxxMovIX.ItemIndex);
  IxxFolha       := EdIxxFolha.ValueVariant;
  IxxLinha       := EdIxxLinha.ValueVariant;
  if VS_CRC_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha) then
    Exit;
  //
(*  Deixar fechar a janela
  GraGruX        := QrVSMovItsGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not VS_CRC_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  //
*)
  QrEstqR4.DisableControls;
  try
    N := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      N := N + 1;
      //
      AdicionaPallet();
    end;
    FmVSOutCab.AtualizaNFeItens(FmVSOutCab.QrVSOutCabMovimCod.Value);
    if N > 0 then
      Close;
  finally
    QrEstqR4.EnableControls;
  end;
end;

procedure TFmVSOutPal.BtReabreClick(Sender: TObject);
begin
  PesquisaPallets();
end;

procedure TFmVSOutPal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOutPal.CalculaValorFaturado();
var
  PrecoTipo: Integer;
  //
  PercTrib, MdaTotal, MdaBruto, MdaLiqui, Cambio, NacTotal, NacBruto, NacLiqui,
  Pecas, PesoKg, AreaM2, AreaP2, ValTot, PrecoVal, PercDesco: Double;
begin
  PrecoTipo := QrVSPedItsPrecoTipo.Value;
  Pecas     := EdSdoVrtPeca.ValueVariant;
  PesoKg    := EdSdoVrtPeso.ValueVariant;
  AreaM2    := EdSdoVrtArM2.ValueVariant;
  AreaP2    := EdSdoVrtArP2.ValueVariant;
  ValTot    := EdSdoVrtValr.ValueVariant;
  PrecoVal  := EdPrecoVal.ValueVariant;
  PercDesco := EdPercDesco.ValueVariant;
  //
  PercTrib := EdPercTrib.ValueVariant;
  Cambio   := EdCambio.ValueVariant;
  //
  MdaTotal := VS_PF.CalculaValorCouros(TTipoCalcCouro(PrecoTipo), PrecoVal,
    Pecas, PesoKg, AreaM2, AreaP2, ValTot);
  MdaBruto := Geral.RoundC(MdaTotal * (1 - (PercDesco / 100)), 2);
  MdaLiqui := Geral.RoundC(MdaBruto * (1 - (PercTrib / 100)), 2);
  //
  NacTotal := Geral.RoundC(MdaTotal * Cambio, 2);
  NacBruto := Geral.RoundC(MdaBruto * Cambio, 2);
  NacLiqui := Geral.RoundC(MdaLiqui * Cambio, 2);
  //
  EdMdaTotal.ValueVariant := MdaTotal;
  EdMdaBruto.ValueVariant := MdaBruto;
  EdMdaLiqui.ValueVariant := MdaLiqui;
  //
  EdNacTotal.ValueVariant := NacTotal;
  EdNacBruto.ValueVariant := NacBruto;
  EdNacLiqui.ValueVariant := NacLiqui;
end;

procedure TFmVSOutPal.CBPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    EdPrecoMoeda.ReadOnly := False;
    CBPrecoMoeda.ReadOnly := False;
  end;
end;

procedure TFmVSOutPal.DBG04EstqAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
var
  I, Pallet: Integer;
begin
  Pallet := QrEstqR4Pallet.Value;
  if DBG04Estq.SelectedRows.CurrentRowSelected then
  //FOrdemSelecionados
    //Geral.MB_Info(Geral.FF0(Pallet) + '+')
    ListBox1.Items.Add(Geral.FF0(Pallet))
  else
  begin
    //Geral.MB_Info(Geral.FF0(Pallet) + '-');
 // will delete 'My Item' string if exists in ListBox1
    I := ListBox1.Items.IndexOf(Geral.FF0(Pallet));
    if I > -1 then
      ListBox1.Items.Delete(I);
  end;
  //vMyItemIndex := ListBox1.Items.IndexOf('My Item');
  //
  SomaSelecionados();
end;

procedure TFmVSOutPal.DefineDadosItemPedido();
begin
  if EdPedItsVda.ValueVariant <> 0 then
  begin
    //FDefinindoDadosItemPedido := True;
    EdPrecoMoeda.ValueVariant := QrVSPedItsPrecoMoeda.Value;
    CBPrecoMoeda.KeyValue     := QrVSPedItsPrecoMoeda.Value;
    EdPrecoVal.ValueVariant   := QrVSPedItsPrecoVal.Value;
    EdPercDesco.ValueVariant  := QrVSPedItsPercDesco.Value;
    EdPercTrib.ValueVariant   := QrVSPedItsPercTrib.Value;
  end;
end;

procedure TFmVSOutPal.EdCambioChange(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPal.EdItemNFeRedefinido(Sender: TObject);
var
  ItemNFe: Integer;
begin
  ItemNFe := EdItemNFe.ValueVariant;
  if FmVSOutCab.QrVSOutIts.Locate('ItemNFe', ItemNFe, []) then
  begin
    EdGGXRcl.ValueVariant := FmVSOutCab.QrVSOutItsGGXRcl.Value;
    CBGGXRcl.KeyValue := FmVSOutCab.QrVSOutItsGGXRcl.Value;
    EdGGXRcl.Enabled := False;
    CBGGXRcl.Enabled := False;
  end else
  begin
    EdGGXRcl.Enabled := True;
    CBGGXRcl.Enabled := True;
  end;
end;

procedure TFmVSOutPal.EdPedItsVdaRedefinido(Sender: TObject);
begin
  DefineDadosItemPedido();
end;

procedure TFmVSOutPal.EdPercDescoChange(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPal.EdPercDescoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    TdmkEdit(Sender).ReadOnly := False;
  end;
end;

procedure TFmVSOutPal.EdPercTribChange(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPal.EdPercTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    TdmkEdit(Sender).ReadOnly := False;
  end;
end;

procedure TFmVSOutPal.EdPrecoMoedaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    EdPrecoMoeda.ReadOnly := False;
    CBPrecoMoeda.ReadOnly := False;
  end;
end;

procedure TFmVSOutPal.EdPrecoMoedaRedefinido(Sender: TObject);
begin
  if EdPrecoMoeda.ValueVariant = Dmod.QrControle.FieldByName('MoedaBr').AsInteger then
  begin
    EdCambio.Enabled := False;
    EdCambio.ValueVariant := 1.0000000000;
  end else
    EdCambio.Enabled := True;
end;

procedure TFmVSOutPal.EdPrecoValChange(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPal.EdPrecoValKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    TdmkEdit(Sender).ReadOnly := False;
  end;
end;

procedure TFmVSOutPal.EdSdoVrtArM2Change(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPal.EdSdoVrtArP2Change(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPal.EdSdoVrtPecaChange(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPal.EdSdoVrtPesoChange(Sender: TObject);
begin
  CalculaValorFaturado();
end;

procedure TFmVSOutPal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  DBEdCodigo.DataSource    := FDsCab;
  DBEdMovimCod.DataSource  := FDsCab;
  DBEdEMpresa.DataSource   := FDsCab;
  DBEdDtEntrada.DataSource := FDsCab;
  DBEdCliVenda.DataSource  := FDsCab;
end;

procedure TFmVSOutPal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VS_CRC_PF.AbreVSSerFch(QrVSSerFch);
  VS_CRC_PF.AbreGraGruXY(QrGraGruX, 'AND ggx.GraGruY <> 0');
  VS_CRC_PF.AbreGraGruXY(QrGGXRcl, '');
    //'AND ggx.GraGruY <> 0');
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSMovimID, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCambioMda, Dmod.MyDB);
end;

procedure TFmVSOutPal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOutPal.InsereIMEI_Atual();
const
  Observ = '';
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  MovimTwn   = 0;
  ExigeFornecedor = False;
  EdFicha    = nil;
  //EdPesoKg   = nil;
  CustoMOKg   = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  QtdGerPeca  = 0;
  QtdGerPeso  = 0;
  QtdGerArM2  = 0;
  QtdGerArP2  = 0;
  AptoUso     = 0; // Eh venda!
  NotaMPAG    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib   = 0;
  PedItsFin   = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  //
  JmpMovID: TEstqMovimID = emidAjuste;
  JmpNivel1: Integer = 0;
  JmpNivel2: Integer = 0;
  JmpGGX: Integer = 0;
  RmsMovID: TEstqMovimID = emidAjuste;
  RmsNivel1: Integer = 0;
  RmsNivel2: Integer = 0;
  RmsGGX: Integer = 0;
  GSPJmpMovID: TEstqMovimID = emidAjuste;
  GSPJmpNiv2: Integer = 0;
  MovCodPai: Integer = 0;
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, CliVenda,
  SerieFch, Ficha, (*Misturou,*) DstNivel1, DstNivel2, GraGruY, SrcGGX,
  DstGGX, PedItsVda, ItemNFe, VSMulFrnCab, ClientMO, GGXRcl, FornecMO,
  StqCenLoc: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Valor: Double;
  IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha, IxxIndex: Integer;
begin
  SrcMovID       := QrVSMovItsMovimID.Value;
  SrcNivel1      := QrVSMovItsCodigo.Value;
  SrcNivel2      := QrVSMovItsControle.Value;
  SrcGGX         := QrVSMovItsGraGruX.Value;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := 0; //EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClientMO       := QrVSMovItsClientMO.Value;
  FornecMO       := 0; // ver
  StqCenLoc      := CO_STQCENLOC_ZERO;
  Terceiro       := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  CliVenda       := Geral.IMV(DBEdCliVenda.Text);
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidVenda;
  MovimNiv       := eminSemNiv;
  Pallet         := QrVSMovItsPallet.Value; //EdPallet.ValueVariant;
  GraGruX        := QrVSMovItsGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not VS_CRC_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  Pecas          := -QrVSMovItsSdoVrtPeca.Value;
  AreaM2         := -QrVSMovItsSdoVrtArM2.Value;
  AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  PesoKg         := -0;
  if (AreaM2 = 0) and (QrVSMovItsSdoVrtPeso.Value > 0) then
      PesoKg     := -QrVSMovItsSdoVrtPeso.Value;
  if (QrVSMovItsSdoVrtArM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
    Valor := QrVSMovItsSdoVrtArM2.Value *
    (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
  else
  if QrVSMovItsPesoKg.Value > 0 then
    Valor := QrVSMovItsSdoVrtPeso.Value *
    (QrVSMovItsValorT.Value / QrVSMovItsPesoKg.Value)
  else
  if QrVSMovItsPecas.Value > 0 then
    Valor := QrVSMovItsSdoVrtPeca.Value *
    (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value);
  ValorMP        := -Valor;
  ValorT         := ValorMP;
  //
  SerieFch       := 0; //QrVSMovItsSerieFch.Value;
  Ficha          := 0;  //QrVSMovItsFicha.Value;
  Marca          := ''; //QrVSMovItsMarca.Value;
  //Misturou       := QrVSMovItsMisturou.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  GraGruY        := QrVSMovItsGraGruY.Value;
(*
  if QrVSMovItsPedItsFin.Value <> 0 then
    PedItsVda := QrVSMovItsPedItsFin.Value
  else
    PedItsVda := QrVSMovItsPedItsLib.Value;
*)
  PedItsVda      := EdPedItsVda.ValueVariant;
  //
  ItemNFe        := EdItemNFe.ValueVariant;
  //
  IxxMovIX       := TEstqMovInfo(RGIxxMovIX.ItemIndex);
  IxxFolha       := EdIxxFolha.ValueVariant;
  IxxLinha       := EdIxxLinha.ValueVariant;
  if VS_CRC_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha) then
    Exit;
  //
  if IxxMovIX <> TEstqMovInfo.eminfIndef  then
  IxxIndex := ListBox1.Items.IndexOf(Geral.FF0(Pallet));
  IxxLinha := IxxLinha + IxxIndex;
  while IxxLinha > 24 do
  begin
    IxxFolha := IxxFolha + 1;
    IxxLinha := IxxLinha - 24;
  end;
(*
  if VS_CRC_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY) then
    Exit;
*)
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);

  if VS_CRC_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab,
  ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2,
  RmsGGX, GSPJmpMovID, GSPJmpNiv2, MovCodPai, CO_0_VmiPai,
  IxxMovIX, IxxFolha, IxxLinha,
(*
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
*)
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_FALSE_ExigeStqLoc,
  iuvpei093(*Item de venda de produto por pallet*)) then
  begin
    VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    VS_CRC_PF.AtualizaTotaisVSXxxCab('vsoutcab', MovimCod);
{$IfDef sAllVS}
    InsereVSOutFat(Controle, Pecas, AreaM2, AreaP2, PesoKg);
    VS_PF.AtualizaVSPedIts_Vda(PedItsVda);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
    FmVSOutCab.LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSOutPal.InsereVSOutFat(Controle: Integer; Pecas, AreaM2, AreaP2,
  PesoKg: Double);
var
  PrecoTipo, PrecoMoeda: Integer;
  Qtde, PrecoVal, PercDesco, PercTrib, MdaTotal, MdaBruto, MdaLiqui, Cambio,
  NacTotal, NacBruto, NacLiqui, TPecas, TPesoKg, TAreaM2, TAreaP2, QTot, Fator:
  Double;
  SQLType: TSQLType;
begin
  if EdPedItsVda.ValueVariant <> 0 then
  begin
    SQLType        := ImgTipo.SQLType;
    //
    TPecas         := EdSdoVrtPeca.ValueVariant;
    TPesoKg        := EdSdoVrtPeso.ValueVariant;
    TAreaM2        := EdSdoVrtArM2.ValueVariant;
    TAreaP2        := EdSdoVrtArP2.ValueVariant;
    PrecoTipo      := QrVSPedItsPrecoTipo.Value;
    QTot           := ABS(VS_PF.DefineQtdePedIts(TTipoCalcCouro(PrecoTipo),
      TPecas, TPesoKg, TAreaM2, TAreaP2));
    Qtde           := ABS(VS_PF.DefineQtdePedIts(TTipoCalcCouro(PrecoTipo),
      Pecas, PesoKg, AreaM2, AreaP2));
    //
    if QTot <> 0 then
      Fator := ABS(Qtde / QTot)
    else
      Fator  := 0;
    //
    PrecoMoeda     := EdPrecoMoeda.ValueVariant;
    PrecoVal       := EdPrecoVal.ValueVariant;
    PercDesco      := EdPercDesco.ValueVariant;
    PercTrib       := EdPercTrib.ValueVariant;
    MdaTotal       := EdMdaTotal.ValueVariant * Fator;
    MdaBruto       := EdMdaBruto.ValueVariant * Fator;
    MdaLiqui       := EdMdaLiqui.ValueVariant * Fator;
    Cambio         := EdCambio.ValueVariant;
    NacTotal       := EdNacTotal.ValueVariant * Fator;
    NacBruto       := EdNacBruto.ValueVariant * Fator;
    NacLiqui       := EdNacLiqui.ValueVariant * Fator;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsoutfat', False, [
    'Qtde', 'PrecoTipo', 'PrecoMoeda',
    'PrecoVal', 'PercDesco', 'PercTrib',
    'MdaTotal', 'MdaBruto', 'MdaLiqui',
    'Cambio', 'NacTotal', 'NacBruto',
    'NacLiqui'], [
    'Controle'], [
    Qtde, PrecoTipo, PrecoMoeda,
    PrecoVal, PercDesco, PercTrib,
    MdaTotal, MdaBruto, MdaLiqui,
    Cambio, NacTotal, NacBruto,
    NacLiqui], [
    Controle], True);
  end;
end;

procedure TFmVSOutPal.PesquisaPallets();
const
  Pallet   = 0;
  Terceiro = 0;
  CouNiv2  = 0;
  CouNiv1  = 0;
  GraGruYs = '';
  ClientMO = 0;
var
  GraGruX: TPallArr;
  SQL_Especificos: String;
  StqCenCad, StqCenLoc, MovimID, MovimCod: Integer;
begin
  VS_CRC_PF.SetaGGXUnicoEmLista(EdGraGruX.ValueVariant, GraGruX);
  if CkEspecificos.Checked then
    DmkPF.SQL_ExplodeInBetween(EmptyStr, 'vmi.Pallet', EdEspecificos.Text, SQL_Especificos)
  else
    SQL_Especificos := '';
  //
  StqCenCad := EdStqCenCad.ValueVariant;
  StqCenLoc := EdStqCenLoc.ValueVariant;
  MovimID   := EdMovimID.ValueVariant;
  MovimCod  := EdIMEC.ValueVariant;
  if VS_CRC_PF.PesquisaPallets(FEmpresa, ClientMO, CouNiv2, CouNiv1, StqCenCad,
  StqcenLoc, Self.Name, GraGruYs, GraGruX, [], SQL_Especificos, TEstqMovimID(
  MovimID), MovimCod, FVSMovImp4) then
    VS_CRC_PF.ReopenVSListaPallets(QrEstqR4, FVSMovImp4,
    FEmpresa, EdGraGruX.ValueVariant, Pallet, Terceiro, '');
end;

procedure TFmVSOutPal.QrStqCenCadAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, QrStqCenCadCodigo.Value, 0);
end;

procedure TFmVSOutPal.QrStqCenCadBeforeClose(DataSet: TDataSet);
begin
  QrStqCenLoc.Close;
end;

procedure TFmVSOutPal.ReopenVSMovIts();
{
var
  GraGruX, Empresa, Pallet: Integer;
begin
  ////////////////////////////////////////////////////////////
  //QrEstqR4 >> GROUP BY wmi.Empresa, wmi.Pallet, wmi.GraGruX
  /////////////////////////////////////////////////////////////
  GraGruX    := QrEstqR4GraGruX.Value;
  Empresa    := QrEstqR4Empresa.Value;
  Pallet     := QrEstqR4Pallet.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT pal.Nome NO_Pallet, wmi.*, ggx.GraGruY, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  ',
  'FROM v s m o v i t s wmi ',
  'LEFT JOIN vspalleta pal ON pal.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=wmi.Terceiro',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX',
  'WHERE wmi.Empresa=' + Geral.FF0(Empresa),
  'AND wmi.GraGruX=' + Geral.FF0(GraGruX),
  'AND ( ',
  '  wmi.MovimID=' + Geral.FF0(Integer(emidCompra)),
  '  OR  ',
  '  wmi.SrcMovID<>0 ',
  ') ',
  'AND (wmi.SdoVrtPeca > 0 ',
  // Precisa?
  //'OR wmi.SdoVrtArM2 > 0 ',
  ') ',
  'AND wmi.Pallet=' + Geral.FF0(Pallet),
  'ORDER BY DataHora, Pallet',
  '']);
}
begin
  VS_CRC_PF.ReopenVSMovIts_Pallet2(QrVSMovIts, QrEstqR4);
end;

procedure TFmVSOutPal.SomaSelecionados();
var
  I: Integer;
  SdoVrtPeca, SdoVrtArM2, SdoVrtPeso: Double;
begin
  QrEstqR4.DisableControls;
  try
    SdoVrtPeca := 0;
    SdoVrtArM2 := 0;
    SdoVrtPeso := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      SdoVrtPeca := SdoVrtPeca + QrEstqR4SdoVrtPeca.Value;
      SdoVrtArM2 := SdoVrtArM2 + QrEstqR4SdoVrtArM2.Value;;
      SdoVrtPeso := SdoVrtPeso + QrEstqR4SdoVrtPeso.Value;;
      //
      EdSdoVrtPeca.ValueVariant := SdoVrtPeca;
      EdSdoVrtArM2.ValueVariant := SdoVrtArM2;
      EdSdoVrtPeso.ValueVariant := SdoVrtPeso;
    end;
  finally
    QrEstqR4.EnableControls;
  end;
end;

end.
