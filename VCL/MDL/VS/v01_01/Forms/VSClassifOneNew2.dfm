object FmVSClassifOneNew2: TFmVSClassifOneNew2
  Left = 0
  Top = 0
  Caption = 
    'WET-CURTI-015 :: Classifica'#231#227'o de Artigo de Ribeira Couro a Cour' +
    'o'
  ClientHeight = 1041
  ClientWidth = 1904
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PanelOC: TPanel
    Left = 0
    Top = 940
    Width = 1904
    Height = 101
    Align = alBottom
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Panel42: TPanel
      Left = 0
      Top = 0
      Width = 1436
      Height = 101
      Align = alClient
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label3: TLabel
        Left = 4
        Top = 8
        Width = 35
        Height = 23
        Caption = 'OC:'
        FocusControl = DBEdCodigo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 172
        Top = 8
        Width = 80
        Height = 23
        Caption = 'Empresa:'
        FocusControl = DBEdit19
      end
      object Label22: TLabel
        Left = 520
        Top = 8
        Width = 45
        Height = 23
        Caption = #193'rea:'
        FocusControl = DBEdit14
      end
      object Label13: TLabel
        Left = 612
        Top = 8
        Width = 54
        Height = 23
        Caption = 'IME-I:'
      end
      object Label14: TLabel
        Left = 792
        Top = 8
        Width = 142
        Height = 23
        Caption = 'Artigo de ribeira:'
      end
      object Label16: TLabel
        Left = 4
        Top = 40
        Width = 54
        Height = 23
        Caption = 'Pe'#231'as:'
      end
      object Label17: TLabel
        Left = 192
        Top = 40
        Width = 123
        Height = 23
        Caption = 'Peso (origem):'
      end
      object Label20: TLabel
        Left = 444
        Top = 40
        Width = 76
        Height = 23
        Caption = #193'rea m'#178':'
      end
      object Label21: TLabel
        Left = 680
        Top = 40
        Width = 72
        Height = 23
        Caption = #193'rea ft'#178':'
      end
      object Label26: TLabel
        Left = 920
        Top = 40
        Width = 94
        Height = 23
        Caption = 'Ficha RMP:'
        FocusControl = DBEdit18
      end
      object Label24: TLabel
        Left = 1216
        Top = 40
        Width = 102
        Height = 23
        Caption = 'Fornecedor:'
        FocusControl = DBEdit16
      end
      object Label18: TLabel
        Left = 4
        Top = 72
        Width = 291
        Height = 23
        Caption = 'Observa'#231#227'o sobre o artigo gerado:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 44
        Top = 4
        Width = 120
        Height = 32
        TabStop = False
        DataField = 'CacCod'
        DataSource = DsVSPaClaCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit19: TDBEdit
        Left = 252
        Top = 4
        Width = 60
        Height = 31
        DataField = 'Empresa'
        DataSource = DsVSPaClaCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 312
        Top = 4
        Width = 205
        Height = 31
        DataField = 'NO_EMPRESA'
        DataSource = DsVSPaClaCab
        TabOrder = 2
      end
      object DBEdit14: TDBEdit
        Left = 568
        Top = 4
        Width = 40
        Height = 31
        DataField = 'NO_TIPO'
        DataSource = DsVSPaClaCab
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 668
        Top = 4
        Width = 120
        Height = 31
        DataField = 'Controle'
        DataSource = DsVSGerArtNew
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 940
        Top = 4
        Width = 60
        Height = 31
        DataField = 'GraGruX'
        DataSource = DsVSGerArtNew
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 1000
        Top = 4
        Width = 429
        Height = 31
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVSGerArtNew
        TabOrder = 6
      end
      object DBEdit9: TDBEdit
        Left = 64
        Top = 36
        Width = 120
        Height = 31
        DataField = 'Pecas'
        DataSource = DsVSGerArtNew
        TabOrder = 7
      end
      object DBEdit10: TDBEdit
        Left = 320
        Top = 36
        Width = 120
        Height = 31
        DataField = 'QtdAntPeso'
        DataSource = DsVSGerArtNew
        TabOrder = 8
      end
      object DBEdit12: TDBEdit
        Left = 524
        Top = 36
        Width = 150
        Height = 31
        DataField = 'AreaM2'
        DataSource = DsVSGerArtNew
        TabOrder = 9
      end
      object DBEdit13: TDBEdit
        Left = 760
        Top = 36
        Width = 150
        Height = 31
        DataField = 'AreaP2'
        DataSource = DsVSGerArtNew
        TabOrder = 10
      end
      object DBEdit18: TDBEdit
        Left = 1020
        Top = 36
        Width = 190
        Height = 31
        DataField = 'NO_FICHA'
        DataSource = DsVSGerArtNew
        TabOrder = 11
      end
      object DBEdit16: TDBEdit
        Left = 1324
        Top = 36
        Width = 105
        Height = 31
        DataField = 'Terceiro'
        DataSource = DsVSGerArtNew
        TabOrder = 12
      end
      object DBEdit11: TDBEdit
        Left = 300
        Top = 68
        Width = 1129
        Height = 31
        DataField = 'Observ'
        DataSource = DsVSGerArtNew
        TabOrder = 13
      end
    end
    object Panel43: TPanel
      Left = 1436
      Top = 0
      Width = 468
      Height = 101
      Align = alRight
      BevelOuter = bvLowered
      TabOrder = 1
      object Label71: TLabel
        Left = 4
        Top = 8
        Width = 83
        Height = 23
        Caption = 'Digitador:'
      end
      object Label70: TLabel
        Left = 4
        Top = 40
        Width = 109
        Height = 23
        Caption = 'Classificador:'
      end
      object Label85: TLabel
        Left = 292
        Top = 72
        Width = 65
        Height = 23
        Caption = 'Tempo:'
      end
      object CBRevisor: TdmkDBLookupComboBox
        Left = 180
        Top = 36
        Width = 285
        Height = 31
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsRevisores
        TabOrder = 0
        dmkEditCB = EdRevisor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBDigitador: TdmkDBLookupComboBox
        Left = 180
        Top = 4
        Width = 285
        Height = 31
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDigitadores
        TabOrder = 1
        dmkEditCB = EdDigitador
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDigitador: TdmkEditCB
        Left = 124
        Top = 4
        Width = 56
        Height = 31
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdDigitadorRedefinido
        DBLookupComboBox = CBDigitador
        IgnoraDBLookupComboBox = False
      end
      object EdRevisor: TdmkEditCB
        Left = 124
        Top = 36
        Width = 56
        Height = 31
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdRevisorRedefinido
        DBLookupComboBox = CBRevisor
        IgnoraDBLookupComboBox = False
      end
      object EdTempo: TEdit
        Left = 368
        Top = 68
        Width = 96
        Height = 31
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 4
        Text = '0,000'
      end
    end
  end
  object PnBoxesAll: TPanel
    Left = 0
    Top = 97
    Width = 1584
    Height = 843
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PnBoxesT01: TPanel
      Left = 0
      Top = 0
      Width = 1584
      Height = 422
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object PnBoxT1L1: TPanel
        Left = 0
        Top = 0
        Width = 528
        Height = 422
        Align = alLeft
        TabOrder = 0
        object Panel4: TPanel
          Left = 1
          Top = 1
          Width = 526
          Height = 64
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel8: TPanel
            Left = 41
            Top = 0
            Width = 485
            Height = 64
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel11: TPanel
              Left = 0
              Top = 31
              Width = 485
              Height = 31
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object DBEdit3: TDBEdit
                Left = 100
                Top = 0
                Width = 385
                Height = 31
                Align = alClient
                DataField = 'NO_CLISTAT'
                DataSource = DsVSPallet1
                TabOrder = 0
              end
              object DBEdit8: TDBEdit
                Left = 0
                Top = 0
                Width = 100
                Height = 31
                Align = alLeft
                DataField = 'NO_STATUS'
                DataSource = DsVSPallet1
                TabOrder = 1
              end
            end
            object DBEdit1: TDBEdit
              Left = 0
              Top = 0
              Width = 485
              Height = 31
              Align = alTop
              DataField = 'NO_PRD_TAM_COR'
              DataSource = DsVSPallet1
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel50: TPanel
            Left = 0
            Top = 0
            Width = 41
            Height = 64
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object Label79: TLabel
              Left = 0
              Top = 0
              Width = 30
              Height = 23
              Align = alTop
              Alignment = taCenter
              Caption = 'Box'
            end
            object Panel51: TPanel
              Left = 0
              Top = 23
              Width = 41
              Height = 41
              Align = alClient
              BevelOuter = bvNone
              Caption = '1'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -37
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object DBGPallet1: TdmkDBGridZTO
          Left = 299
          Top = 65
          Width = 228
          Height = 356
          Align = alClient
          DataSource = DsItens1
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          OptionsEx = []
          PopupMenu = PMItens01
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -19
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea'
              Width = 100
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 1
          Top = 65
          Width = 298
          Height = 356
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 298
            Height = 189
            Align = alTop
            Caption = ' Do artigo na classe: '
            TabOrder = 0
            object Panel3: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 162
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label4: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit4
              end
              object Label7: TLabel
                Left = 4
                Top = 68
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdit20
              end
              object Label8: TLabel
                Left = 4
                Top = 100
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea1
              end
              object Label11: TLabel
                Left = 4
                Top = 132
                Width = 96
                Height = 23
                Caption = '% da '#225'rea:'
                FocusControl = DBEdArea1
              end
              object Label54: TLabel
                Left = 6
                Top = 36
                Width = 54
                Height = 23
                Caption = 'IME-I:'
                FocusControl = DBEdit4
              end
              object DBEdit4: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Controle'
                DataSource = DsVSPalClaIts1
                TabOrder = 0
              end
              object DBEdit20: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSum1
                TabOrder = 1
              end
              object DBEdArea1: TDBEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSum1
                TabOrder = 2
              end
              object EdPercent1: TdmkEdit
                Left = 112
                Top = 128
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object DBEdit50: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'VMI_Dest'
                DataSource = DsVSPalClaIts1
                TabOrder = 4
              end
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 189
            Width = 298
            Height = 167
            Align = alClient
            Caption = ' Do Pallet: '
            TabOrder = 1
            object Panel16: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 140
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label52: TLabel
                Left = 4
                Top = 38
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdSumPcPal01
              end
              object Label55: TLabel
                Left = 4
                Top = 8
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit57
              end
              object Label53: TLabel
                Left = 4
                Top = 64
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea1
              end
              object Label73: TLabel
                Left = 4
                Top = 99
                Width = 90
                Height = 23
                Caption = 'm'#178' / Pe'#231'a:'
                FocusControl = DBEdArea1
              end
              object DBEdSumPcPal01: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSumPal1
                TabOrder = 0
                OnChange = DBEdSumPcPal01Change
              end
              object DBEdit43: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSumPal1
                TabOrder = 1
              end
              object DBEdit57: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Codigo'
                DataSource = DsVSPallet1
                TabOrder = 2
              end
              object EdMedia1: TdmkEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
        object Memo1: TMemo
          Left = 308
          Top = 132
          Width = 185
          Height = 89
          Lines.Strings = (
            'Memo1')
          TabOrder = 3
          Visible = False
        end
      end
      object PnBoxT1L2: TPanel
        Left = 528
        Top = 0
        Width = 528
        Height = 422
        Align = alClient
        TabOrder = 1
        object Panel17: TPanel
          Left = 1
          Top = 1
          Width = 526
          Height = 64
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel18: TPanel
            Left = 41
            Top = 0
            Width = 485
            Height = 64
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel44: TPanel
              Left = 0
              Top = 31
              Width = 485
              Height = 31
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object DBEdit25: TDBEdit
                Left = 100
                Top = 0
                Width = 385
                Height = 31
                Align = alClient
                DataField = 'NO_CLISTAT'
                DataSource = DsVSPallet2
                TabOrder = 0
              end
              object DBEdit26: TDBEdit
                Left = 0
                Top = 0
                Width = 100
                Height = 31
                Align = alLeft
                DataField = 'NO_STATUS'
                DataSource = DsVSPallet2
                TabOrder = 1
              end
            end
            object DBEdit22: TDBEdit
              Left = 0
              Top = 0
              Width = 485
              Height = 31
              Align = alTop
              DataField = 'NO_PRD_TAM_COR'
              DataSource = DsVSPallet2
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel45: TPanel
            Left = 0
            Top = 0
            Width = 41
            Height = 64
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object Label80: TLabel
              Left = 0
              Top = 0
              Width = 30
              Height = 23
              Align = alTop
              Alignment = taCenter
              Caption = 'Box'
            end
            object Panel46: TPanel
              Left = 0
              Top = 23
              Width = 41
              Height = 41
              Align = alClient
              BevelOuter = bvNone
              Caption = '2'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -37
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object DBGPallet2: TdmkDBGridZTO
          Left = 299
          Top = 65
          Width = 228
          Height = 356
          Align = alClient
          DataSource = DsItens2
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          OptionsEx = []
          PopupMenu = PMItens02
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -19
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea'
              Width = 100
              Visible = True
            end>
        end
        object Panel19: TPanel
          Left = 1
          Top = 65
          Width = 298
          Height = 356
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object GroupBox3: TGroupBox
            Left = 0
            Top = 0
            Width = 298
            Height = 189
            Align = alTop
            Caption = ' Do artigo na classe: '
            TabOrder = 0
            object Panel26: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 162
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label5: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit15
              end
              object Label27: TLabel
                Left = 4
                Top = 68
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdit27
              end
              object Label28: TLabel
                Left = 4
                Top = 100
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea2
              end
              object Label29: TLabel
                Left = 4
                Top = 132
                Width = 96
                Height = 23
                Caption = '% da '#225'rea:'
                FocusControl = DBEdArea2
              end
              object Label30: TLabel
                Left = 6
                Top = 36
                Width = 54
                Height = 23
                Caption = 'IME-I:'
                FocusControl = DBEdit15
              end
              object DBEdit15: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Controle'
                DataSource = DsVSPalClaIts2
                TabOrder = 0
              end
              object DBEdit27: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSum2
                TabOrder = 1
              end
              object DBEdArea2: TDBEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSum2
                TabOrder = 2
              end
              object EdPercent2: TdmkEdit
                Left = 112
                Top = 128
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object DBEdit29: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'VMI_Dest'
                DataSource = DsVSPalClaIts2
                TabOrder = 4
              end
            end
          end
          object GroupBox4: TGroupBox
            Left = 0
            Top = 189
            Width = 298
            Height = 167
            Align = alClient
            Caption = ' Do Pallet: '
            TabOrder = 1
            object Panel30: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 140
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label31: TLabel
                Left = 4
                Top = 38
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdSumPcPal02
              end
              object Label56: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit60
              end
              object Label57: TLabel
                Left = 4
                Top = 68
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea2
              end
              object Label74: TLabel
                Left = 4
                Top = 99
                Width = 90
                Height = 23
                Caption = 'm'#178' / Pe'#231'a:'
                FocusControl = DBEdArea1
              end
              object DBEdSumPcPal02: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSumPal2
                TabOrder = 0
                OnChange = DBEdSumPcPal02Change
              end
              object DBEdit59: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSumPal2
                TabOrder = 1
              end
              object DBEdit60: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Codigo'
                DataSource = DsVSPallet2
                TabOrder = 2
              end
              object EdMedia2: TdmkEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
      end
      object PnBoxT1L3: TPanel
        Left = 1056
        Top = 0
        Width = 528
        Height = 422
        Align = alRight
        TabOrder = 2
        object Panel20: TPanel
          Left = 1
          Top = 1
          Width = 526
          Height = 64
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel21: TPanel
            Left = 41
            Top = 0
            Width = 485
            Height = 64
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel55: TPanel
              Left = 0
              Top = 31
              Width = 485
              Height = 31
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object DBEdit31: TDBEdit
                Left = 100
                Top = 0
                Width = 385
                Height = 31
                Align = alClient
                DataField = 'NO_CLISTAT'
                DataSource = DsVSPallet3
                TabOrder = 0
              end
              object DBEdit32: TDBEdit
                Left = 0
                Top = 0
                Width = 100
                Height = 31
                Align = alLeft
                DataField = 'NO_STATUS'
                DataSource = DsVSPallet3
                TabOrder = 1
              end
            end
            object DBEdit30: TDBEdit
              Left = 0
              Top = 0
              Width = 485
              Height = 31
              Align = alTop
              DataField = 'NO_PRD_TAM_COR'
              DataSource = DsVSPallet3
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel56: TPanel
            Left = 0
            Top = 0
            Width = 41
            Height = 64
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object Label83: TLabel
              Left = 0
              Top = 0
              Width = 30
              Height = 23
              Align = alTop
              Alignment = taCenter
              Caption = 'Box'
            end
            object Panel57: TPanel
              Left = 0
              Top = 23
              Width = 41
              Height = 41
              Align = alClient
              BevelOuter = bvNone
              Caption = '3'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -37
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object DBGPallet3: TdmkDBGridZTO
          Left = 299
          Top = 65
          Width = 228
          Height = 356
          Align = alClient
          DataSource = DsItens3
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          OptionsEx = []
          PopupMenu = PMItens03
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -19
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea'
              Width = 100
              Visible = True
            end>
        end
        object Panel22: TPanel
          Left = 1
          Top = 65
          Width = 298
          Height = 356
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object GroupBox5: TGroupBox
            Left = 0
            Top = 0
            Width = 298
            Height = 189
            Align = alTop
            Caption = ' Do artigo na classe: '
            TabOrder = 0
            object Panel34: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 162
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label32: TLabel
                Left = 4
                Top = 0
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit33
              end
              object Label33: TLabel
                Left = 4
                Top = 64
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdit34
              end
              object Label34: TLabel
                Left = 4
                Top = 96
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea3
              end
              object Label35: TLabel
                Left = 4
                Top = 128
                Width = 96
                Height = 23
                Caption = '% da '#225'rea:'
                FocusControl = DBEdArea3
              end
              object Label36: TLabel
                Left = 6
                Top = 32
                Width = 54
                Height = 23
                Caption = 'IME-I:'
                FocusControl = DBEdit33
              end
              object DBEdit33: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Controle'
                DataSource = DsVSPalClaIts3
                TabOrder = 0
              end
              object DBEdit34: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSum3
                TabOrder = 1
              end
              object DBEdArea3: TDBEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSum3
                TabOrder = 2
              end
              object EdPercent3: TdmkEdit
                Left = 112
                Top = 128
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object DBEdit61: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'VMI_Dest'
                DataSource = DsVSPalClaIts3
                TabOrder = 4
              end
            end
          end
          object GroupBox6: TGroupBox
            Left = 0
            Top = 189
            Width = 298
            Height = 167
            Align = alClient
            Caption = ' Do Pallet: '
            TabOrder = 1
            object Panel35: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 140
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label58: TLabel
                Left = 4
                Top = 36
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdSumPcPal03
              end
              object Label59: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit64
              end
              object Label60: TLabel
                Left = 4
                Top = 68
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea3
              end
              object Label75: TLabel
                Left = 4
                Top = 99
                Width = 89
                Height = 23
                Caption = 'M'#178' / Pe'#231'a:'
                FocusControl = DBEdArea1
              end
              object DBEdSumPcPal03: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSumPal3
                TabOrder = 0
                OnChange = DBEdSumPcPal03Change
              end
              object DBEdit63: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSumPal3
                TabOrder = 1
              end
              object DBEdit64: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Codigo'
                DataSource = DsVSPallet3
                TabOrder = 2
              end
              object EdMedia3: TdmkEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
      end
    end
    object PnBoxesT02: TPanel
      Left = 0
      Top = 422
      Width = 1584
      Height = 421
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object PnBoxT2L3: TPanel
        Left = 1056
        Top = 0
        Width = 528
        Height = 421
        Align = alRight
        TabOrder = 2
        object Panel23: TPanel
          Left = 1
          Top = 1
          Width = 526
          Height = 64
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel24: TPanel
            Left = 41
            Top = 0
            Width = 485
            Height = 64
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel58: TPanel
              Left = 0
              Top = 31
              Width = 485
              Height = 31
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object DBEdit38: TDBEdit
                Left = 100
                Top = 0
                Width = 385
                Height = 31
                Align = alClient
                DataField = 'NO_CLISTAT'
                DataSource = DsVSPallet6
                TabOrder = 0
              end
              object DBEdit39: TDBEdit
                Left = 0
                Top = 0
                Width = 100
                Height = 31
                Align = alLeft
                DataField = 'NO_STATUS'
                DataSource = DsVSPallet6
                TabOrder = 1
              end
            end
            object DBEdit37: TDBEdit
              Left = 0
              Top = 0
              Width = 485
              Height = 31
              Align = alTop
              DataField = 'NO_PRD_TAM_COR'
              DataSource = DsVSPallet6
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel59: TPanel
            Left = 0
            Top = 0
            Width = 41
            Height = 64
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object Label84: TLabel
              Left = 0
              Top = 0
              Width = 30
              Height = 23
              Align = alTop
              Alignment = taCenter
              Caption = 'Box'
            end
            object Panel60: TPanel
              Left = 0
              Top = 23
              Width = 41
              Height = 41
              Align = alClient
              BevelOuter = bvNone
              Caption = '6'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -37
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object DBGPallet6: TdmkDBGridZTO
          Left = 299
          Top = 65
          Width = 228
          Height = 355
          Align = alClient
          DataSource = DsItens6
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          OptionsEx = []
          PopupMenu = PMItens06
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -19
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea'
              Width = 100
              Visible = True
            end>
        end
        object Panel25: TPanel
          Left = 1
          Top = 65
          Width = 298
          Height = 355
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object GroupBox11: TGroupBox
            Left = 0
            Top = 0
            Width = 298
            Height = 189
            Align = alTop
            Caption = ' Do artigo na classe: '
            TabOrder = 0
            object Panel40: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 162
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label37: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit40
              end
              object Label38: TLabel
                Left = 4
                Top = 68
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdSumPcIMEI06
              end
              object Label39: TLabel
                Left = 4
                Top = 100
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea6
              end
              object Label40: TLabel
                Left = 4
                Top = 132
                Width = 96
                Height = 23
                Caption = '% da '#225'rea:'
                FocusControl = DBEdArea6
              end
              object Label41: TLabel
                Left = 6
                Top = 36
                Width = 54
                Height = 23
                Caption = 'IME-I:'
                FocusControl = DBEdit40
              end
              object DBEdit40: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Controle'
                DataSource = DsVSPalClaIts6
                TabOrder = 0
              end
              object DBEdSumPcIMEI06: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSum6
                TabOrder = 1
              end
              object DBEdArea6: TDBEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSum6
                TabOrder = 2
              end
              object EdPercent6: TdmkEdit
                Left = 112
                Top = 128
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object DBEdit72: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'VMI_Dest'
                DataSource = DsVSPalClaIts6
                TabOrder = 4
              end
            end
          end
          object GroupBox12: TGroupBox
            Left = 0
            Top = 189
            Width = 298
            Height = 166
            Align = alClient
            Caption = ' Do Pallet: '
            TabOrder = 1
            object Panel41: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 139
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label67: TLabel
                Left = 4
                Top = 36
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdSumPcPal06
              end
              object Label68: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit75
              end
              object Label69: TLabel
                Left = 4
                Top = 68
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea6
              end
              object Label78: TLabel
                Left = 4
                Top = 99
                Width = 89
                Height = 23
                Caption = 'M'#178' / Pe'#231'a:'
                FocusControl = DBEdArea1
              end
              object DBEdSumPcPal06: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSumPal6
                TabOrder = 0
                OnChange = DBEdSumPcPal06Change
              end
              object DBEdit74: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSumPal6
                TabOrder = 1
              end
              object DBEdit75: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Codigo'
                DataSource = DsVSPallet6
                TabOrder = 2
              end
              object EdMedia6: TdmkEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
      end
      object PnBoxT2L2: TPanel
        Left = 528
        Top = 0
        Width = 528
        Height = 421
        Align = alClient
        TabOrder = 1
        object Panel27: TPanel
          Left = 1
          Top = 1
          Width = 526
          Height = 64
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel28: TPanel
            Left = 41
            Top = 0
            Width = 485
            Height = 64
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel52: TPanel
              Left = 0
              Top = 31
              Width = 485
              Height = 31
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object DBEdit45: TDBEdit
                Left = 100
                Top = 0
                Width = 385
                Height = 31
                Align = alClient
                DataField = 'NO_CLISTAT'
                DataSource = DsVSPallet5
                TabOrder = 0
              end
              object DBEdit46: TDBEdit
                Left = 0
                Top = 0
                Width = 100
                Height = 31
                Align = alLeft
                DataField = 'NO_STATUS'
                DataSource = DsVSPallet5
                TabOrder = 1
              end
            end
            object DBEdit44: TDBEdit
              Left = 0
              Top = 0
              Width = 485
              Height = 31
              Align = alTop
              DataField = 'NO_PRD_TAM_COR'
              DataSource = DsVSPallet5
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel53: TPanel
            Left = 0
            Top = 0
            Width = 41
            Height = 64
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object Label82: TLabel
              Left = 0
              Top = 0
              Width = 30
              Height = 23
              Align = alTop
              Alignment = taCenter
              Caption = 'Box'
            end
            object Panel54: TPanel
              Left = 0
              Top = 23
              Width = 41
              Height = 41
              Align = alClient
              BevelOuter = bvNone
              Caption = '5'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -37
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object DBGPallet5: TdmkDBGridZTO
          Left = 299
          Top = 65
          Width = 228
          Height = 355
          Align = alClient
          DataSource = DsItens5
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          OptionsEx = []
          PopupMenu = PMItens05
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -19
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea'
              Width = 100
              Visible = True
            end>
        end
        object Panel29: TPanel
          Left = 1
          Top = 65
          Width = 298
          Height = 355
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object GroupBox9: TGroupBox
            Left = 0
            Top = 0
            Width = 298
            Height = 189
            Align = alTop
            Caption = ' Do artigo na classe: '
            TabOrder = 0
            object Panel38: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 162
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label42: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit47
              end
              object Label43: TLabel
                Left = 4
                Top = 68
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdit48
              end
              object Label44: TLabel
                Left = 4
                Top = 100
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea5
              end
              object Label45: TLabel
                Left = 4
                Top = 132
                Width = 96
                Height = 23
                Caption = '% da '#225'rea:'
                FocusControl = DBEdArea5
              end
              object Label46: TLabel
                Left = 6
                Top = 36
                Width = 54
                Height = 23
                Caption = 'IME-I:'
                FocusControl = DBEdit47
              end
              object DBEdit47: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Controle'
                DataSource = DsVSPalClaIts5
                TabOrder = 0
              end
              object DBEdit48: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSum5
                TabOrder = 1
              end
              object DBEdArea5: TDBEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSum5
                TabOrder = 2
              end
              object EdPercent5: TdmkEdit
                Left = 112
                Top = 128
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object DBEdit68: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'VMI_Dest'
                DataSource = DsVSPalClaIts5
                TabOrder = 4
              end
            end
          end
          object GroupBox10: TGroupBox
            Left = 0
            Top = 189
            Width = 298
            Height = 166
            Align = alClient
            Caption = ' Do Pallet: '
            TabOrder = 1
            object Panel39: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 139
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label64: TLabel
                Left = 4
                Top = 36
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdSumPcPal05
              end
              object Label65: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit71
              end
              object Label66: TLabel
                Left = 4
                Top = 68
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea5
              end
              object Label77: TLabel
                Left = 4
                Top = 99
                Width = 90
                Height = 23
                Caption = 'm'#178' / Pe'#231'a:'
                FocusControl = DBEdArea1
              end
              object DBEdSumPcPal05: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSumPal5
                TabOrder = 0
                OnChange = DBEdSumPcPal05Change
              end
              object DBEdit70: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSumPal5
                TabOrder = 1
              end
              object DBEdit71: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Codigo'
                DataSource = DsVSPallet5
                TabOrder = 2
              end
              object EdMedia5: TdmkEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
      end
      object PnBoxT2L1: TPanel
        Left = 0
        Top = 0
        Width = 528
        Height = 421
        Align = alLeft
        TabOrder = 0
        object Panel31: TPanel
          Left = 1
          Top = 1
          Width = 526
          Height = 64
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel32: TPanel
            Left = 41
            Top = 0
            Width = 485
            Height = 64
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel47: TPanel
              Left = 0
              Top = 31
              Width = 485
              Height = 31
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object DBEdit52: TDBEdit
                Left = 100
                Top = 0
                Width = 385
                Height = 31
                Align = alClient
                DataField = 'NO_CLISTAT'
                DataSource = DsVSPallet4
                TabOrder = 0
              end
              object DBEdit53: TDBEdit
                Left = 0
                Top = 0
                Width = 100
                Height = 31
                Align = alLeft
                DataField = 'NO_STATUS'
                DataSource = DsVSPallet4
                TabOrder = 1
              end
            end
            object DBEdit51: TDBEdit
              Left = 0
              Top = 0
              Width = 485
              Height = 31
              Align = alTop
              DataField = 'NO_PRD_TAM_COR'
              DataSource = DsVSPallet4
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel48: TPanel
            Left = 0
            Top = 0
            Width = 41
            Height = 64
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object Label81: TLabel
              Left = 0
              Top = 0
              Width = 30
              Height = 23
              Align = alTop
              Alignment = taCenter
              Caption = 'Box'
            end
            object Panel49: TPanel
              Left = 0
              Top = 23
              Width = 41
              Height = 41
              Align = alClient
              BevelOuter = bvNone
              Caption = '4'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -37
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object DBGPallet4: TdmkDBGridZTO
          Left = 299
          Top = 65
          Width = 228
          Height = 355
          Align = alClient
          DataSource = DsItens4
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          OptionsEx = []
          PopupMenu = PMItens04
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -19
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea'
              Width = 100
              Visible = True
            end>
        end
        object Panel33: TPanel
          Left = 1
          Top = 65
          Width = 298
          Height = 355
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object GroupBox7: TGroupBox
            Left = 0
            Top = 0
            Width = 298
            Height = 189
            Align = alTop
            Caption = ' Do artigo na classe: '
            TabOrder = 0
            object Panel36: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 162
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label47: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit28
              end
              object Label48: TLabel
                Left = 4
                Top = 68
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdSumPcIMEI04
              end
              object Label49: TLabel
                Left = 4
                Top = 100
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea4
              end
              object Label50: TLabel
                Left = 4
                Top = 132
                Width = 96
                Height = 23
                Caption = '% da '#225'rea:'
                FocusControl = DBEdArea4
              end
              object Label51: TLabel
                Left = 6
                Top = 36
                Width = 54
                Height = 23
                Caption = 'IME-I:'
                FocusControl = DBEdit28
              end
              object DBEdit28: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Controle'
                DataSource = DsVSPalClaIts4
                TabOrder = 0
              end
              object DBEdSumPcIMEI04: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSum4
                TabOrder = 1
              end
              object DBEdArea4: TDBEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSum4
                TabOrder = 2
              end
              object EdPercent4: TdmkEdit
                Left = 112
                Top = 128
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object DBEdit56: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'VMI_Dest'
                DataSource = DsVSPalClaIts4
                TabOrder = 4
              end
            end
          end
          object GroupBox8: TGroupBox
            Left = 0
            Top = 189
            Width = 298
            Height = 166
            Align = alClient
            Caption = ' Do Pallet: '
            TabOrder = 1
            object Panel37: TPanel
              Left = 2
              Top = 25
              Width = 294
              Height = 139
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label61: TLabel
                Left = 4
                Top = 36
                Width = 54
                Height = 23
                Caption = 'Pe'#231'as:'
                FocusControl = DBEdSumPcPal04
              end
              object Label62: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 23
                Caption = 'ID:'
                FocusControl = DBEdit67
              end
              object Label63: TLabel
                Left = 4
                Top = 68
                Width = 45
                Height = 23
                Caption = #193'rea:'
                FocusControl = DBEdArea4
              end
              object Label76: TLabel
                Left = 4
                Top = 99
                Width = 90
                Height = 23
                Caption = 'm'#178' / Pe'#231'a:'
                FocusControl = DBEdArea1
              end
              object DBEdSumPcPal04: TDBEdit
                Left = 112
                Top = 32
                Width = 180
                Height = 31
                DataField = 'Pecas'
                DataSource = DsSumPal4
                TabOrder = 0
                OnChange = DBEdSumPcPal04Change
              end
              object DBEdit66: TDBEdit
                Left = 112
                Top = 64
                Width = 180
                Height = 31
                DataField = 'AreaM2'
                DataSource = DsSumPal4
                TabOrder = 1
              end
              object DBEdit67: TDBEdit
                Left = 112
                Top = 0
                Width = 180
                Height = 31
                DataField = 'Codigo'
                DataSource = DsVSPallet4
                TabOrder = 2
              end
              object EdMedia4: TdmkEdit
                Left = 112
                Top = 96
                Width = 180
                Height = 31
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 1584
    Top = 97
    Width = 320
    Height = 843
    Align = alRight
    Caption = 'Panel2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object DBGAll: TdmkDBGridZTO
      Left = 1
      Top = 1
      Width = 318
      Height = 841
      Align = alClient
      DataSource = DsAll
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      OptionsEx = []
      PopupMenu = PMAll
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -19
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Box'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VMI_Dest'
          Title.Caption = 'IME-I'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea'
          Width = 100
          Visible = True
        end>
    end
  end
  object PnInfoBig: TPanel
    Left = 0
    Top = 0
    Width = 1904
    Height = 97
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PnDigitacao: TPanel
      Left = 1360
      Top = 1
      Width = 543
      Height = 95
      Align = alRight
      Enabled = False
      TabOrder = 0
      object Label12: TLabel
        Left = 1
        Top = 1
        Width = 541
        Height = 23
        Align = alTop
        Alignment = taCenter
        Caption = ' Digita'#231#227'o'
        ExplicitWidth = 83
      end
      object Panel7: TPanel
        Left = 225
        Top = 24
        Width = 55
        Height = 70
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label2: TLabel
          Left = 0
          Top = 0
          Width = 37
          Height = 23
          Align = alTop
          Caption = 'Box:'
        end
        object EdBox: TdmkEdit
          Left = 0
          Top = 23
          Width = 55
          Height = 47
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdBoxChange
        end
      end
      object Panel6: TPanel
        Left = 1
        Top = 24
        Width = 224
        Height = 70
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 0
          Top = 0
          Width = 45
          Height = 23
          Align = alTop
          Caption = #193'rea:'
        end
        object LaTipoArea: TLabel
          Left = 189
          Top = 23
          Width = 35
          Height = 39
          Align = alRight
          Caption = '?'#178
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdArea: TdmkEdit
          Left = 0
          Top = 23
          Width = 189
          Height = 47
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object Panel1: TPanel
        Left = 280
        Top = 24
        Width = 262
        Height = 70
        Align = alRight
        TabOrder = 2
        object Label72: TLabel
          Left = 1
          Top = 1
          Width = 260
          Height = 23
          Align = alTop
          Caption = 'Martelo:'
          ExplicitWidth = 69
        end
        object CBVSMrtCad: TdmkDBLookupComboBox
          Left = 93
          Top = 24
          Width = 168
          Height = 47
          Align = alClient
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          KeyField = 'Nome'
          ListField = 'Nome'
          ListSource = DsVSMrtCad
          ParentFont = False
          TabOrder = 1
          TabStop = False
          dmkEditCB = EdVSMrtCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdVSMrtCad: TdmkEditCB
          Left = 1
          Top = 24
          Width = 92
          Height = 45
          Align = alLeft
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnKeyDown = EdVSMrtCadKeyDown
          DBLookupComboBox = CBVSMrtCad
          IgnoraDBLookupComboBox = False
        end
      end
    end
    object Panel12: TPanel
      Left = 321
      Top = 1
      Width = 320
      Height = 95
      Align = alLeft
      TabOrder = 1
      object Label15: TLabel
        Left = 1
        Top = 1
        Width = 185
        Height = 23
        Align = alTop
        Alignment = taCenter
        Caption = 'Couros j'#225' classificados'
      end
      object Panel10: TPanel
        Left = 1
        Top = 24
        Width = 120
        Height = 70
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label10: TLabel
          Left = 0
          Top = 0
          Width = 60
          Height = 23
          Align = alTop
          Caption = ' Pe'#231'as:'
          FocusControl = DBEdit23
        end
        object DBEdit23: TDBEdit
          Left = 0
          Top = 23
          Width = 120
          Height = 47
          Align = alClient
          DataField = 'Pecas'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
      end
      object Panel9: TPanel
        Left = 121
        Top = 24
        Width = 198
        Height = 70
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label9: TLabel
          Left = 0
          Top = 0
          Width = 51
          Height = 23
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEDAreaT
        end
        object DBEDAreaT: TDBEdit
          Left = 0
          Top = 23
          Width = 198
          Height = 47
          Align = alClient
          DataField = 'AreaM2'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
      end
    end
    object Panel13: TPanel
      Left = 1
      Top = 1
      Width = 320
      Height = 95
      Align = alLeft
      TabOrder = 2
      object Label19: TLabel
        Left = 1
        Top = 1
        Width = 238
        Height = 23
        Align = alTop
        Caption = 'Couros que faltam classificar'
      end
      object Panel14: TPanel
        Left = 1
        Top = 24
        Width = 120
        Height = 70
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label23: TLabel
          Left = 0
          Top = 0
          Width = 60
          Height = 23
          Align = alTop
          Caption = ' Pe'#231'as:'
          FocusControl = DBEdit21
        end
        object DBEdit21: TDBEdit
          Left = 0
          Top = 23
          Width = 120
          Height = 47
          Align = alClient
          DataField = 'FALTA_PECA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
      end
      object Panel15: TPanel
        Left = 121
        Top = 24
        Width = 198
        Height = 70
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label25: TLabel
          Left = 0
          Top = 0
          Width = 51
          Height = 23
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEdit24
        end
        object DBEdit24: TDBEdit
          Left = 0
          Top = 23
          Width = 198
          Height = 47
          Align = alClient
          DataField = 'FALTA_AREA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
      end
    end
    object BtEncerra: TBitBtn
      Tag = 10134
      Left = 744
      Top = 8
      Width = 216
      Height = 72
      Caption = '&Menu'
      TabOrder = 3
      OnClick = BtEncerraClick
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 964
      Top = 8
      Width = 216
      Height = 72
      Caption = '&Imprimir'
      TabOrder = 4
      OnClick = BtImprimeClick
    end
    object BtDigitacao: TButton
      Left = 652
      Top = 28
      Width = 89
      Height = 45
      Caption = 'Teste'
      Enabled = False
      TabOrder = 5
      Visible = False
      OnClick = BtDigitacaoClick
    end
    object BtReabre: TBitBtn
      Tag = 18
      Left = 1185
      Top = 8
      Width = 72
      Height = 72
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = BtReabreClick
    end
  end
  object QrVSPaClaCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaClaCabBeforeClose
    AfterScroll = QrVSPaClaCabAfterScroll
    OnCalcFields = QrVSPaClaCabCalcFields
    SQL.Strings = (
      'SELECT vga.GraGruX, vga.Nome,'
      'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP'
      'FROM vspaclacab pcc'
      'LEFT JOIN vsgerart vga ON vga.Codigo=pcc.vsgerart'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vga.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa'
      'WHERE pcc.Codigo=5'
      ''
      ''
      '')
    Left = 1380
    Top = 140
    object QrVSPaClaCabNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSPaClaCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPaClaCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSPaClaCabTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSPaClaCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPaClaCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSPaClaCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaClaCabVSGerArt: TIntegerField
      FieldName = 'VSGerArt'
    end
    object QrVSPaClaCabLstPal01: TIntegerField
      FieldName = 'LstPal01'
    end
    object QrVSPaClaCabLstPal02: TIntegerField
      FieldName = 'LstPal02'
    end
    object QrVSPaClaCabLstPal03: TIntegerField
      FieldName = 'LstPal03'
    end
    object QrVSPaClaCabLstPal04: TIntegerField
      FieldName = 'LstPal04'
    end
    object QrVSPaClaCabLstPal05: TIntegerField
      FieldName = 'LstPal05'
    end
    object QrVSPaClaCabLstPal06: TIntegerField
      FieldName = 'LstPal06'
    end
    object QrVSPaClaCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaClaCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaClaCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaClaCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaClaCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaClaCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaClaCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaClaCabNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPaClaCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPaClaCabVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSPaClaCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
  end
  object DsVSPaClaCab: TDataSource
    DataSet = QrVSPaClaCab
    Left = 1380
    Top = 188
  end
  object QrVSGerArtNew: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet ')
    Left = 1472
    Top = 141
    object QrVSGerArtNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerArtNewControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSGerArtNewMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGerArtNewEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerArtNewMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSGerArtNewGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerArtNewPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGerArtNewDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGerArtNewDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGerArtNewUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGerArtNewUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGerArtNewAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGerArtNewAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArtNewSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSGerArtNewSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSGerArtNewSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSGerArtNewPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSGerArtNewNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerArtNewSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGerArtNewValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSGerArtNewCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrVSGerArtNewMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSGerArtNewTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSGerArtNewCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSGerArtNewLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSGerArtNewFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSGerArtNewLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSGerArtNewMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSGerArtNewCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSGerArtNewValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSGerArtNewDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSGerArtNewDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSGerArtNewQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrVSGerArtNewNO_FICHA: TWideStringField
      FieldName = 'NO_FICHA'
    end
    object QrVSGerArtNewCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSGerArtNewNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
  end
  object DsVSGerArtNew: TDataSource
    DataSet = QrVSGerArtNew
    Left = 1472
    Top = 189
  end
  object QrVSPallet1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1548
    Top = 141
    object QrVSPallet1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet1AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet1Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet1Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet1CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet1GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet1NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet1NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet1NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet1QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet1: TDataSource
    DataSet = QrVSPallet1
    Left = 1548
    Top = 185
  end
  object QrVSPallet2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1612
    Top = 141
    object QrVSPallet2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet2UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet2UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet2AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet2Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet2Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet2CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet2NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet2NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet2NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet2QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet2: TDataSource
    DataSet = QrVSPallet2
    Left = 1612
    Top = 185
  end
  object QrVSPallet3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1676
    Top = 141
    object QrVSPallet3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet3Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet3DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet3DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet3UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet3UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet3AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet3Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet3Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet3Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet3CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet3GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet3NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet3NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet3NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet3QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet3: TDataSource
    DataSet = QrVSPallet3
    Left = 1676
    Top = 185
  end
  object QrVSPallet4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1740
    Top = 141
    object QrVSPallet4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet4Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet4DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet4DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet4UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet4UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet4AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet4Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet4Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet4Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet4CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet4GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet4NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet4NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet4NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet4QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet4: TDataSource
    DataSet = QrVSPallet4
    Left = 1740
    Top = 185
  end
  object QrVSPallet5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1804
    Top = 141
    object QrVSPallet5Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet5Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet5Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet5DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet5DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet5UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet5UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet5AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet5Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet5Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet5Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet5CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet5GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet5NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet5NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet5NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet5QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet5: TDataSource
    DataSet = QrVSPallet5
    Left = 1804
    Top = 185
  end
  object QrVSPallet6: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 1868
    Top = 141
    object QrVSPallet6Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet6Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet6Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet6DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet6DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet6UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet6UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet6AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet6Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet6Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet6Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet6CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet6GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet6NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet6NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet6NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet6QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet6: TDataSource
    DataSet = QrVSPallet6
    Left = 1868
    Top = 185
  end
  object QrVSPalClaIts1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1548
    Top = 232
    object QrVSPalClaIts1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts1DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrVSPalClaIts1VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts1VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
  end
  object DsVSPalClaIts1: TDataSource
    DataSet = QrVSPalClaIts1
    Left = 1548
    Top = 280
  end
  object QrVSPalClaIts2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1616
    Top = 232
    object QrVSPalClaIts2VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts2VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts2DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts2: TDataSource
    DataSet = QrVSPalClaIts2
    Left = 1616
    Top = 280
  end
  object QrVSPalClaIts3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1676
    Top = 232
    object QrVSPalClaIts3VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts3VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts3Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts3DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts3: TDataSource
    DataSet = QrVSPalClaIts3
    Left = 1676
    Top = 280
  end
  object QrVSPalClaIts4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1740
    Top = 232
    object QrVSPalClaIts4VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts4VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts4Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts4DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts4: TDataSource
    DataSet = QrVSPalClaIts4
    Left = 1740
    Top = 280
  end
  object QrVSPalClaIts5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1804
    Top = 232
    object QrVSPalClaIts5VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts5VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts5Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts5DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts5: TDataSource
    DataSet = QrVSPalClaIts5
    Left = 1804
    Top = 280
  end
  object QrVSPalClaIts6: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, DtHrIni '
      'FROM vspaclaits'
      'WHERE Codigo=5'
      'AND Tecla=1'
      'AND VSPallet=1'
      'ORDER BY DtHrIni DESC, Controle DESC'
      'LIMIT 1')
    Left = 1868
    Top = 232
    object QrVSPalClaIts6VMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPalClaIts6VMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPalClaIts6Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPalClaIts6DtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
  end
  object DsVSPalClaIts6: TDataSource
    DataSet = QrVSPalClaIts6
    Left = 1868
    Top = 280
  end
  object QrItens1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1548
    Top = 324
    object QrItens1Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens1Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens1AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens1AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens1: TDataSource
    DataSet = QrItens1
    Left = 1548
    Top = 372
  end
  object QrSum1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1548
    Top = 416
    object QrSum1Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum1AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum1AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum1: TDataSource
    DataSet = QrSum1
    Left = 1548
    Top = 464
  end
  object QrItens2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1616
    Top = 324
    object QrItens2Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens2AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens2AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens2: TDataSource
    DataSet = QrItens2
    Left = 1616
    Top = 372
  end
  object QrSum2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1616
    Top = 416
    object QrSum2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum2: TDataSource
    DataSet = QrSum2
    Left = 1616
    Top = 464
  end
  object QrItens3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1676
    Top = 324
    object QrItens3Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens3Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens3AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens3AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens3: TDataSource
    DataSet = QrItens3
    Left = 1676
    Top = 372
  end
  object QrSum3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1676
    Top = 416
    object QrSum3Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum3AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum3AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum3: TDataSource
    DataSet = QrSum3
    Left = 1676
    Top = 464
  end
  object QrItens4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1740
    Top = 324
    object QrItens4Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens4Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens4AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens4AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens4: TDataSource
    DataSet = QrItens4
    Left = 1740
    Top = 372
  end
  object QrSum4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1740
    Top = 416
    object QrSum4Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum4AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum4AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum4: TDataSource
    DataSet = QrSum4
    Left = 1740
    Top = 464
  end
  object QrItens5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1804
    Top = 324
    object QrItens5Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens5Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens5AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens5AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens5: TDataSource
    DataSet = QrItens5
    Left = 1804
    Top = 372
  end
  object QrSum5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1804
    Top = 416
    object QrSum5Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum5AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum5AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSum5: TDataSource
    DataSet = QrSum5
    Left = 1804
    Top = 464
  end
  object QrItens6: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1868
    Top = 324
    object QrItens6Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItens6Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrItens6AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrItens6AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItens6: TDataSource
    DataSet = QrItens6
    Left = 1868
    Top = 372
  end
  object QrSum6: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1868
    Top = 416
    object QrSum6Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSum6AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSum6AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSum6: TDataSource
    DataSet = QrSum6
    Left = 1868
    Top = 464
  end
  object QrSumT: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSumTCalcFields
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1380
    Top = 236
    object QrSumTPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumTAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumTAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumTFALTA_PECA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FALTA_PECA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSumTFALTA_AREA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FALTA_AREA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsSumT: TDataSource
    DataSet = QrSumT
    Left = 1380
    Top = 284
  end
  object QrAll: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'ORDER BY Controle DESC')
    Left = 1380
    Top = 328
    object QrAllControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrAllPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrAllAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAllAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAllBox: TIntegerField
      FieldName = 'Box'
    end
    object QrAllVSPaClaIts: TIntegerField
      FieldName = 'VSPaClaIts'
    end
    object QrAllVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrAllVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrAllVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
  end
  object DsAll: TDataSource
    DataSet = QrAll
    Left = 1380
    Top = 376
  end
  object PMItens01: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 220
    object AdicionarPallet01: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet01: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet01: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados1: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens02: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 264
    object AdicionarPallet02: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet02: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet02: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados2: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object QrSumPal1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1552
    Top = 516
    object QrSumPal1Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal1AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal1AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal1: TDataSource
    DataSet = QrSumPal1
    Left = 1552
    Top = 564
  end
  object QrSumPal2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1616
    Top = 508
    object QrSumPal2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal2AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal2AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal2: TDataSource
    DataSet = QrSumPal2
    Left = 1616
    Top = 556
  end
  object QrSumPal3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1676
    Top = 512
    object QrSumPal3Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal3AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal3AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal3: TDataSource
    DataSet = QrSumPal3
    Left = 1676
    Top = 560
  end
  object QrSumPal4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1740
    Top = 512
    object QrSumPal4Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal4AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal4AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal4: TDataSource
    DataSet = QrSumPal4
    Left = 1740
    Top = 560
  end
  object QrSumPal5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1804
    Top = 508
    object QrSumPal5Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal5AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumPal5AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsSumPal5: TDataSource
    DataSet = QrSumPal5
    Left = 1804
    Top = 556
  end
  object QrSumPal6: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1868
    Top = 512
    object QrSumPal6Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPal6AreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPal6AreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPal6: TDataSource
    DataSet = QrSumPal6
    Left = 1868
    Top = 560
  end
  object QrSumVMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1552
    Top = 612
    object QrSumVMIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object PMItens03: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 308
    object AdicionarPallet03: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet03: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet03: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados3: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens04: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 352
    object AdicionarPallet04: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet04: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet04: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados4: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens05: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 396
    object AdicionarPallet05: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet05: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet05: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados5: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMItens06: TPopupMenu
    OnPopup = PMitensPopup
    Left = 352
    Top = 440
    object AdicionarPallet06: TMenuItem
      Caption = '&Adicionar pallet'
      Enabled = False
      OnClick = AdicionarPalletClick
    end
    object EncerrarPallet06: TMenuItem
      Caption = '&Encerrar pallet'
      Enabled = False
      OnClick = EncerrarPalletClick
    end
    object RemoverPallet06: TMenuItem
      Caption = '&Remover pallet'
      Enabled = False
      OnClick = RemoverPalletClick
    end
    object Mensagemdedados6: TMenuItem
      Caption = '&Mensagem de dados'
      OnClick = Mensagemdedados1Click
    end
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 792
    Top = 224
    object EstaOCOrdemdeclassificao1: TMenuItem
      Caption = 'Encerra esta OC (Ordem de classifica'#231#227'o)'
      OnClick = EstaOCOrdemdeclassificao1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Palletdobox01: TMenuItem
      Tag = 1
      Caption = 'Encerra o pallet do box &1'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox02: TMenuItem
      Tag = 2
      Caption = 'Encerra o pallet do box &2'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox03: TMenuItem
      Tag = 3
      Caption = 'Encerra o pallet do box &3'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox04: TMenuItem
      Tag = 4
      Caption = 'Encerra o pallet do box &4'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox05: TMenuItem
      Tag = 5
      Caption = 'Encerra o pallet do box &5'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox06: TMenuItem
      Tag = 6
      Caption = 'Encerra o pallet do box &6'
      OnClick = EncerrarPalletClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Mostrartodosboxes1: TMenuItem
      Caption = 'Mostrar todos boxes'
      OnClick = Mostrartodosboxes1Click
    end
    object ImprimirNmeroPallet1: TMenuItem
      Caption = 'Imprimir N'#250'mero Pallet'
      Enabled = False
      OnClick = ImprimirNmeroPallet1Click
    end
    object DadosPaletsemWordPad1: TMenuItem
      Caption = 'Dados Palets no Paint'
      OnClick = DadosPaletsemWordPad1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object rocarVichaRMP1: TMenuItem
      Caption = 'Trocar Ficha RMP'
      OnClick = rocarVichaRMP1Click
    end
  end
  object QrRevisores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 1384
    Top = 428
    object QrRevisoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRevisoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsRevisores: TDataSource
    DataSet = QrRevisores
    Left = 1384
    Top = 472
  end
  object QrDigitadores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 1464
    Top = 432
    object QrDigitadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDigitadoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsDigitadores: TDataSource
    DataSet = QrDigitadores
    Left = 1464
    Top = 476
  end
  object QrVMIsDePal: TmySQLQuery
    Database = Dmod.MyDB
    Left = 1472
    Top = 240
    object QrVMIsDePalVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
  end
  object PMAll: TPopupMenu
    Left = 1600
    Top = 728
    object Alteraitematual1: TMenuItem
      Caption = '&Altera item atual'
      OnClick = Alteraitematual1Click
    end
    object Excluiitematual1: TMenuItem
      Caption = '&Exclui item atual'
      OnClick = Excluiitematual1Click
    end
  end
  object QrVSMrtCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsmrtcad'
      'ORDER BY Nome')
    Left = 332
    Top = 672
    object QrVSMrtCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMrtCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSMrtCad: TDataSource
    DataSet = QrVSMrtCad
    Left = 332
    Top = 716
  end
  object QrSumDest1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 852
    Top = 696
  end
  object QrSumSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 856
    Top = 744
  end
  object QrVMISorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 936
    Top = 696
  end
  object QrPalSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 936
    Top = 744
  end
end
