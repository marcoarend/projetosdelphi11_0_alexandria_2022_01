unit VSMOEnvCTeGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, UnProjGroup_Consts;

type
  TFmVSMOEnvCTeGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtReabre: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    BtImprime: TBitBtn;
    QrCTes: TmySQLQuery;
    DsCTes: TDataSource;
    QrCTesEmpresa: TIntegerField;
    QrCTesTerceiro: TIntegerField;
    QrCTesSerCT: TIntegerField;
    QrCTesnCT: TIntegerField;
    QrCTesNO_Transp: TWideStringField;
    QrCTeIts: TmySQLQuery;
    DsCTeIts: TDataSource;
    DBGrid2: TDBGrid;
    QrCTesPecas: TFloatField;
    QrCTesPesoKg: TFloatField;
    QrCTesAreaM2: TFloatField;
    QrCTesAreaP2: TFloatField;
    QrCTesPesTrKg: TFloatField;
    QrCTesCusTrKg: TFloatField;
    QrCTesValorT: TFloatField;
    QrCTeItsDataCad: TDateField;
    QrCTeItsTipoFrete: TWideStringField;
    QrCTeItsTabela: TWideStringField;
    QrCTeItsEmpresa: TIntegerField;
    QrCTeItsTerceiro: TIntegerField;
    QrCTeItsSerCT: TIntegerField;
    QrCTeItsnCT: TIntegerField;
    QrCTeItsPecas: TFloatField;
    QrCTeItsPesoKg: TFloatField;
    QrCTeItsAreaM2: TFloatField;
    QrCTeItsAreaP2: TFloatField;
    QrCTeItsPesTrKg: TFloatField;
    QrCTeItsCusTrKg: TFloatField;
    QrCTeItsValorT: TFloatField;
    CkLimitar: TCheckBox;
    Ed1: TdmkEdit;
    Ed2: TdmkEdit;
    CkDESC: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure QrCTesBeforeClose(DataSet: TDataSet);
    procedure QrCTesAfterScroll(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
  private
    { Private declarations }
    FVSMOEnvCTeGer: String;
    //
    procedure ReopenCTes();
  public
    { Public declarations }
  end;

  var
  FmVSMOEnvCTeGer: TFmVSMOEnvCTeGer;

implementation

uses UnMyObjects, ModuleGeral, CreateVS, DmkDAC_PF, UnVS_PF;

{$R *.DFM}

procedure TFmVSMOEnvCTeGer.BtImprimeClick(Sender: TObject);
begin
  VS_PF.MostraFormVSMOEnvCTeImp(QrCTesEmpresa.Value, QrCTesTerceiro.Value,
  QrCTesSerCT.Value, QrCTesnCT.Value);
end;

procedure TFmVSMOEnvCTeGer.BtReabreClick(Sender: TObject);
var
  SQL_Limit, xOrdem: String;
begin
  FVSMOEnvCTeGer :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSMOEnvCTeGer, DModG.QrUpdPID1, False);
  //
  SQL_Limit := '';
  xOrdem := '';
  if CkLimitar.Checked then
  begin
    SQL_Limit := 'LIMIT ' +
    Geral.FF0(Ed1.Valuevariant) + ', ' +
    Geral.FF0(Ed2.Valuevariant) + ' ';
  end;
  if CkDESC.Checked then
    xOrdem := 'DESC';
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FVSMOEnvCTeGer,
  '',
  'SELECT DataCad, "Simples" TipoFrete, ',
  '"' + CO_TAB_VSMOEnvavu + '" Tabela, CFTMA_Empresa Empresa, ',
  'CFTMA_Terceiro Terceiro, CFTMA_SerCT SerCT, CFTMA_nCT nCT, ',
  'CFTMA_Pecas Pecas, CFTMA_PesoKg PesoKg,  ',
  'CFTMA_AreaM2 AreaM2, CFTMA_AreaP2 AreaP2,  ',
  'CFTMA_PesTrKg PesTrKg, CFTMA_CusTrKg CusTrKg,  ',
  'CFTMA_ValorT ValorT  ',
  'FROM ' + TMeuDB + '.vsmoenvavu ',
  ' ',
  'UNION ',
  ' ',
  'SELECT DataCad, "Envio" TipoFrete, ',
  '"' + CO_TAB_VSMOEnvenv + '" Tabela, CFTMP_Empresa Empresa, ',
  'CFTMP_Terceiro Terceiro, CFTMP_SerCT SerCT, CFTMP_nCT nCT, ',
  'CFTMP_Pecas Pecas, CFTMP_PesoKg PesoKg, ',
  'CFTMP_AreaM2 AreaM2, CFTMP_AreaP2 AreaP2, ',
  'CFTMP_PesTrKg PesTrKg, CFTMP_CusTrKg CusTrKg, ',
  'CFTMP_ValorT ValorT ',
  'FROM ' + TMeuDB + '.vsmoenvenv ',
  ' ',
  'UNION ',
  ' ',
  'SELECT DataCad, "Retorno" TipoFrete, ',
  '"' + CO_TAB_VSMOEnvret + '" Tabela, CFTPA_Empresa Empresa, ',
  'CFTPA_Terceiro Terceiro, CFTPA_SerCT SerCT, CFTPA_nCT nCT, ',
  'CFTPA_Pecas Pecas, CFTPA_PesoKg PesoKg, ',
  'CFTPA_AreaM2 AreaM2, CFTPA_AreaP2 AreaP2, ',
  'CFTPA_PesTrKg PesTrKg, CFTPA_CusTrKg CusTrKg, ',
  'CFTPA_ValorT ValorT ',
  'FROM ' + TMeuDB + '.vsmoenvret ',
  ' ',
  'ORDER BY DataCad ' + xOrdem + ', SerCT ' + xOrdem + ', nCT ' + xOrdem + ' ',
  ' ',
  SQL_Limit,
  '']);
  //
  ReopenCTes();
end;

procedure TFmVSMOEnvCTeGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMOEnvCTeGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMOEnvCTeGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSMOEnvCTeGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMOEnvCTeGer.QrCTesAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIts, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FVSMOEnvCTeGer,
  'WHERE Empresa=' + Geral.FF0(QrCTesEmpresa.Value),
  'AND Terceiro=' + Geral.FF0(QrCTesTerceiro.Value),
  'AND SerCT=' + Geral.FF0(QrCTesSerCT.Value),
  'AND nCT=' + Geral.FF0(QrCTesnCT.Value),
  '']);
end;

procedure TFmVSMOEnvCTeGer.QrCTesBeforeClose(DataSet: TDataSet);
begin
  QrCTeIts.Close;
end;

procedure TFmVSMOEnvCTeGer.ReopenCTes();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTes, DModG.MyPID_DB, [
  'SELECT ecg.Empresa, ecg.Terceiro,',
  'IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_Transp,',
  'ecg.SerCT, ecg.nCT,',
  'SUM(ecg.Pecas) Pecas, SUM(ecg.PesoKg) PesoKg,',
  'SUM(ecg.AreaM2) AreaM2, SUM(ecg.AreaP2) AreaP2,',
  'SUM(ecg. PesTrKg)  PesTrKg, SUM(ecg.CusTrKg) CusTrKg,',
  'SUM(ecg.ValorT) ValorT',
  'FROM ' + FVSMOEnvCTeGer + ' ecg',
  'LEFT JOIN ' + TMeuDB + '.entidades tra ON tra.Codigo=ecg.Terceiro',
  'GROUP BY Empresa, Terceiro, SerCT, nCT',
  'ORDER BY ecg.DataCad DESC',
  '']);
end;

end.
