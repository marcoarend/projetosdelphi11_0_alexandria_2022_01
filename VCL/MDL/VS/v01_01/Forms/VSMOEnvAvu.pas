unit VSMOEnvAvu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditCalc, mySQLDbTables, UnProjGroup_Vars, dmkDBLookupComboBox, dmkEditCB,
  dmkDBGridZTO, dmkMemo, dmkEditDateTimePicker;
type
  TFmVSMOEnvAvu = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrCabA: TmySQLQuery;
    DsCabA: TDataSource;
    GroupBox5: TGroupBox;
    Panel6: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    SbCF: TSpeedButton;
    Label43: TLabel;
    Label44: TLabel;
    EdCFTMA_FatID: TdmkEdit;
    EdCFTMA_FatNum: TdmkEdit;
    EdCFTMA_Empresa: TdmkEdit;
    EdCFTMA_SerCT: TdmkEdit;
    EdCFTMA_nCT: TdmkEdit;
    EdCFTMA_nItem: TdmkEdit;
    EdCFTMA_Pecas: TdmkEdit;
    EdCFTMA_AreaM2: TdmkEditCalc;
    EdCFTMA_AreaP2: TdmkEditCalc;
    EdCFTMA_PesoKg: TdmkEdit;
    EdCFTMA_ValorT: TdmkEdit;
    EdCFTMA_PesTrKg: TdmkEdit;
    EdCFTMA_CusTrKg: TdmkEdit;
    QrTransporta: TmySQLQuery;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    DsTransporta: TDataSource;
    Label45: TLabel;
    EdCFTMA_Terceiro: TdmkEditCB;
    CBCFTMA_Terceiro: TdmkDBLookupComboBox;
    QrTerceiro: TmySQLQuery;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroNOMEENTIDADE: TWideStringField;
    DsTerceiro: TDataSource;
    QrVMI: TmySQLQuery;
    DsVMI: TDataSource;
    DBGIMEIs: TdmkDBGridZTO;
    QrVMIControle: TIntegerField;
    QrVMIGraGruX: TIntegerField;
    QrVMIPecas: TFloatField;
    QrVMIPesoKg: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIAreaP2: TFloatField;
    QrVMINO_PRDA_TAM_COR: TWideStringField;
    QrVMIAtivo: TSmallintField;
    Panel5: TPanel;
    Panel8: TPanel;
    BtItem: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    QrSum: TmySQLQuery;
    QrSumPecas: TFloatField;
    QrSumPesoKg: TFloatField;
    QrSumAreaM2: TFloatField;
    QrSumAreaP2: TFloatField;
    QrSumAreaKg: TFloatField;
    EdVSVMI_MovimCod: TdmkEdit;
    Label2: TLabel;
    QrVMIValorT: TFloatField;
    QrSumValorT: TFloatField;
    QrVMIIDItem: TIntegerField;
    QrVMIPallet: TIntegerField;
    QrVMIMovimCod: TIntegerField;
    TPData: TdmkEditDateTimePicker;
    Label4: TLabel;
    LaHora: TLabel;
    EdHora: TdmkEdit;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    Label14: TLabel;
    SbRMP: TSpeedButton;
    Label31: TLabel;
    Panel9: TPanel;
    Label3: TLabel;
    MeNome: TdmkMemo;
    EdNFEMA_SerNF: TdmkEdit;
    EdNFEMA_nNF: TdmkEdit;
    EdNFEMA_Terceiro: TdmkEditCB;
    CBNFEMA_Terceiro: TdmkDBLookupComboBox;
    EdNFEMA_Empresa: TdmkEdit;
    EdNFEMA_FatID: TdmkEdit;
    EdNFEMA_FatNum: TdmkEdit;
    EdNFEMA_nItem: TdmkEdit;
    EdNFEMA_Pecas: TdmkEdit;
    EdNFEMA_AreaM2: TdmkEditCalc;
    EdNFEMA_AreaP2: TdmkEditCalc;
    EdNFEMA_PesoKg: TdmkEdit;
    EdNFEMA_ValorT: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbRMPClick(Sender: TObject);
    procedure EdCFTMA_PesTrKgRedefinido(Sender: TObject);
    procedure EdCFTMA_CusTrKgRedefinido(Sender: TObject);
    procedure BtItemClick(Sender: TObject);
    procedure QrVMIAfterOpen(DataSet: TDataSet);
    procedure DBGIMEIsAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure DBGIMEIsExit(Sender: TObject);
    procedure EdCFTMA_nCTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MeNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    function  IncluiVSMOEnvAVMIs(): Boolean;
    procedure ValorTotalPeloPeso();

  public
    { Public declarations }
    FCodigo: Integer;
    FTabVMIQtdEnvAvu, FSQL_WHERE_VMI: String;
    procedure ReopenVMI(Controle: Integer);
  end;

  var
  FmVSMOEnvAvu: TFmVSMOEnvAvu;

implementation

uses UnMyObjects, UMySQLModule, Module, MyDBCheck, DmkDAC_PF, NFe_PF, ModVS_CRC,
  ModuleGeral, VSMOEnvAVMI, UnDmkProcFunc;

{$R *.DFM}

procedure TFmVSMOEnvAvu.BtItemClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrVMIControle.Value;
  if DBCheck.CriaFm(TFmVSMOEnvAVMI, FmVSMOEnvAVMI, afmoNegarComAviso) then
  begin
    FmVSMOEnvAVMI.ImgTipo.SQLType  := stUpd;
    FmVSMOEnvAVMI.FTabVMIQtdEnvAvu := FTabVMIQtdEnvAvu;
    FmVSMOEnvAVMI.EdControle.ValueVariant := Controle;
    FmVSMOEnvAVMI.EdPallet.ValueVariant   := QrVMIPallet.Value;
    FmVSMOEnvAVMI.EdGraGruX.ValueVariant  := QrVMIGraGruX.Value;
    FmVSMOEnvAVMI.EdNO_PRD_TAM_COR.Text   := QrVMINO_PRDA_TAM_COR.Value;
    FmVSMOEnvAVMI.EdPecas.ValueVariant    := QrVMIPecas.Value;
    FmVSMOEnvAVMI.EdPesoKg.ValueVariant   := QrVMIPesoKg.Value;
    FmVSMOEnvAVMI.EdAreaM2.ValueVariant   := QrVMIAreaM2.Value;
    FmVSMOEnvAVMI.EdAreaP2.ValueVariant   := QrVMIAreaP2.Value;
    //
    FmVSMOEnvAVMI.ShowModal;
    //
    FmVSMOEnvAVMI.Destroy;
    //
    ReopenVMI(Controle);
  end;
end;

procedure TFmVSMOEnvAvu.BtOKClick(Sender: TObject);
var
  Codigo, VSVMI_MovimCod, CFTMA_FatID, CFTMA_FatNum, CFTMA_Empresa,
  CFTMA_Terceiro, CFTMA_nItem, CFTMA_SerCT, CFTMA_nCT: Integer;
  CFTMA_Pecas, CFTMA_PesoKg, CFTMA_AreaM2, CFTMA_AreaP2, CFTMA_PesTrKg,
  CFTMA_CusTrKg, CFTMA_ValorT: Double;
  Nome, DataHora: String;
  SQLType: TSQLType;
  NFEMA_FatID, NFEMA_FatNum, NFEMA_Empresa, NFEMA_Terceiro, NFEMA_nItem,
  NFEMA_SerNF, NFEMA_nNF: Integer;
  NFEMA_Pecas, NFEMA_PesoKg, NFEMA_AreaM2, NFEMA_AreaP2, NFEMA_ValorT: Double;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := dmkPF.LimpaSeSohCRLF(MeNome.Text);
  //
  VSVMI_MovimCod := EdVSVMI_MovimCod.ValueVariant;
  //
  NFEMA_FatID    := EdNFEMA_FatID.ValueVariant;
  NFEMA_FatNum   := EdNFEMA_FatNum.ValueVariant;
  NFEMA_Empresa  := EdNFEMA_Empresa.ValueVariant;
  NFEMA_Terceiro := EdNFEMA_Terceiro.ValueVariant;
  NFEMA_nItem    := EdNFEMA_nItem.ValueVariant;
  NFEMA_SerNF    := EdNFEMA_SerNF.ValueVariant;
  NFEMA_nNF      := EdNFEMA_nNF.ValueVariant;
  NFEMA_Pecas    := EdNFEMA_Pecas.ValueVariant;
  NFEMA_PesoKg   := EdNFEMA_PesoKg.ValueVariant;
  NFEMA_AreaM2   := EdNFEMA_AreaM2.ValueVariant;
  NFEMA_AreaP2   := EdNFEMA_AreaP2.ValueVariant;
  NFEMA_ValorT   := EdNFEMA_ValorT.ValueVariant;
  //
  CFTMA_FatID    := EdCFTMA_FatID.ValueVariant;
  CFTMA_FatNum   := EdCFTMA_FatNum.ValueVariant;
  CFTMA_Empresa  := EdCFTMA_Empresa.ValueVariant;
  CFTMA_Terceiro := EdCFTMA_Terceiro.ValueVariant;
  CFTMA_nItem    := EdCFTMA_nItem.ValueVariant;
  CFTMA_SerCT    := EdCFTMA_SerCT.ValueVariant;
  CFTMA_nCT      := EdCFTMA_nCT.ValueVariant;
  CFTMA_Pecas    := EdCFTMA_Pecas.ValueVariant;
  CFTMA_PesoKg   := EdCFTMA_PesoKg.ValueVariant;
  CFTMA_AreaM2   := EdCFTMA_AreaM2.ValueVariant;
  CFTMA_AreaP2   := EdCFTMA_AreaP2.ValueVariant;
  CFTMA_PesTrKg  := EdCFTMA_PesTrKg.ValueVariant;
  CFTMA_CusTrKg  := EdCFTMA_CusTrKg.ValueVariant;
  CFTMA_ValorT   := EdCFTMA_ValorT.ValueVariant;
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  //
  if MyObjects.FIC(TPData.Date < 2, TPData, 'Data/hora n�o definida!')
  or MyObjects.FIC(CFTMA_nCT = 0, EdCFTMA_nCT, 'Informe o n�mero do CT do envio da mat�ria-prima!')
  or MyObjects.FIC(CFTMA_Empresa = 0, EdCFTMA_Empresa, 'Informe a empresa para o frete do couro!')
  or MyObjects.FIC(CFTMA_FatID = 0, EdCFTMA_FatID, 'Informe o ID do frete do couro!')
  or MyObjects.FIC(CFTMA_FatNum = 0, EdCFTMA_FatNum, 'Informe o c�digo do frete do couro!')
  or MyObjects.FIC(CFTMA_Terceiro = 0, EdCFTMA_Terceiro, 'Informe o transportador do couro!')
  then Exit;
  //
  if Codigo = 0 then
  begin
    Codigo := UMyMod.BPGS1I32('vsmoenvavu', 'Codigo', '', '', tsPos, SQLType, Codigo);
    EdCodigo.ValueVariant := Codigo;
  end;
  //
  if not IncluiVSMOEnvAVMIs() then exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmoenvavu', False, [
  'Nome', 'DataHora',
  'VSVMI_MovimCod', 'CFTMA_FatID',
  'CFTMA_FatNum', 'CFTMA_Empresa', 'CFTMA_Terceiro',
  'CFTMA_nItem', 'CFTMA_SerCT', 'CFTMA_nCT',
  'CFTMA_Pecas', 'CFTMA_PesoKg', 'CFTMA_AreaM2',
  'CFTMA_AreaP2', 'CFTMA_PesTrKg', 'CFTMA_CusTrKg',
  'CFTMA_ValorT', 'NFEMA_FatID',
  'NFEMA_FatNum', 'NFEMA_Empresa', 'NFEMA_Terceiro',
  'NFEMA_nItem', 'NFEMA_SerNF', 'NFEMA_nNF',
  'NFEMA_Pecas', 'NFEMA_PesoKg', 'NFEMA_AreaM2',
  'NFEMA_AreaP2', 'NFEMA_ValorT'], [
  'Codigo'], [
  Nome, DataHora,
  VSVMI_MovimCod, CFTMA_FatID,
  CFTMA_FatNum, CFTMA_Empresa, CFTMA_Terceiro,
  CFTMA_nItem, CFTMA_SerCT, CFTMA_nCT,
  CFTMA_Pecas, CFTMA_PesoKg, CFTMA_AreaM2,
  CFTMA_AreaP2, CFTMA_PesTrKg, CFTMA_CusTrKg,
  CFTMA_ValorT, NFEMA_FatID,
  NFEMA_FatNum, NFEMA_Empresa, NFEMA_Terceiro,
  NFEMA_nItem, NFEMA_SerNF, NFEMA_nNF,
  NFEMA_Pecas, NFEMA_PesoKg, NFEMA_AreaM2,
  NFEMA_AreaP2, NFEMA_ValorT
  ], [
  Codigo], True) then
  begin
    FCodigo := Codigo;
    DmModVS_CRC.AtualizaSaldoVSMOEnvAvu(Codigo);
    //
    DmModVS_CRC.CalculaFreteVMIdeMOEnvAvu(VSVMI_MovimCod);
    //
    Close;
  end;
end;

procedure TFmVSMOEnvAvu.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMOEnvAvu.DBGIMEIsAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
var
  Ativo, Controle: Integer;
begin
  Controle := QrVMIControle.Value;
  //
  if DBGIMEIs.SelectedRows.CurrentRowSelected = True then
    Ativo := 1
  else
    Ativo := 0;
  //
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FTabVMIQtdEnvAvu, False, [
  'Ativo'], [
  'Controle'], [
  Ativo], [
  Controle], False) then
    ReopenVMI(Controle);
end;

procedure TFmVSMOEnvAvu.DBGIMEIsExit(Sender: TObject);
begin
  ReopenVMI(QrVMIControle.Value);
end;

procedure TFmVSMOEnvAvu.EdCFTMA_CusTrKgRedefinido(Sender: TObject);
begin
  ValorTotalPeloPeso();
end;

procedure TFmVSMOEnvAvu.EdCFTMA_nCTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nCTe: Integer;
begin
  if Key = VK_F4 then
  begin
    EdCFTMA_nCT.ValueVariant := DmModVS_CRC.ObtemProximoCTe(
      EdCFTMA_Terceiro.ValueVariant, EdCFTMA_SerCT.ValueVariant);
    if nCTe > 0 then
      EdCFTMA_nCT.ValueVariant := nCTe;
  end;
end;

procedure TFmVSMOEnvAvu.EdCFTMA_PesTrKgRedefinido(Sender: TObject);
begin
  ValorTotalPeloPeso();
end;

procedure TFmVSMOEnvAvu.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMOEnvAvu.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  FCodigo := 0;
  FSQL_WHERE_VMI := '';
  //
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTerceiro, Dmod.MyDB);
end;

procedure TFmVSMOEnvAvu.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmVSMOEnvAvu.IncluiVSMOEnvAVMIs(): Boolean;
var
  VSMOEnvAvu: Integer;
  FListaIMECs: TMyGrlArrInt;
  //
  procedure IncluiAtual();
  var
    Codigo, VSMovIts: Integer;
    ValorFrete, Pecas, PesoKg, AreaM2, AreaP2: Double;
    SQLType: TSQLType;
  begin
    if QrVMIAtivo.Value = 0 then
      Exit;
    SQLType        := stIns;
    Codigo         := QrVMIIDItem.Value;
    VSMovIts       := QrVMIControle.Value;
    Pecas          := QrVMIPecas.Value;
    PesoKg         := QrVMIPesoKg.Value;
    AreaM2         := QrVMIAreaM2.Value;
    AreaP2         := QrVMIAreaP2.Value;
    if QrSumAreaKg.Value = 0 then
      ValorFrete     := 0
    else
      ValorFrete := ((AreaM2 * 4) + PesoKg) / QrSumAreaKg.Value *
        EdCFTMA_ValorT.ValueVariant;
    //
    if Codigo = 0 then
      Codigo := UMyMod.BPGS1I32('vsmoenvavmi', 'Codigo', '', '', tsPos, stIns, Codigo);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmoenvavmi', False, [
    'VSMOEnvAvu', 'VSMovIts', 'ValorFrete',
    'Pecas', 'PesoKg', 'AreaM2',
    'AreaP2'], [
    'Codigo'], [
    VSMOEnvAvu, VSMovIts, ValorFrete,
    Pecas, PesoKg, AreaM2,
    AreaP2], [
    Codigo], True);
  end;
const
  sProcName = 'TFmVSMOEnvAvu.IncluiVSMOEnvAVMIs()';
var
  J: Integer;
begin
  Result := False;
  SetLength(FListaIMECs, 0);
  VSMOEnvAvu     := EdCodigo.ValueVariant;
  Dmod.MyDB.Execute('DELETE FROM vsmoenvavmi WHERE VSMOEnvAvu=' + Geral.FF0(VSMOEnvAvu));
  QrVMI.First;
  while not QrVMI.Eof do
  begin
    IncluiAtual();
    DmModVS_CRC.AtualizaFreteVMIdeMOEnvAvu(QrVMIControle.Value);
    dmkPF.IncrementaListaDeInteiros(FListaIMECs, QrVMIMovimCod.Value);
    //
    QrVMI.Next;
  end;
  //
  for J := Low(FListaIMECs) to High(FListaIMECs) do
    DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(FListaIMECs[J]);
  //
  Result := True;
end;

procedure TFmVSMOEnvAvu.MeNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    Key := 0;
    BtOK.SetFocus;
  end;
end;

procedure TFmVSMOEnvAvu.QrVMIAfterOpen(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, DModG.MyPID_DB, [
  'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg,',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
  'SUM(ValorT) ValorT, SUM((AreaM2*4) + PesoKg) AreaKg ',
  'FROM ' + FTabVMIQtdEnvAvu,
  'WHERE Ativo=1',
  '']);
  //
  EdCFTMA_Pecas.ValueVariant  := QrSumPecas.Value;
  EdCFTMA_PesoKg.ValueVariant := QrSumPesoKg.Value;
  EdCFTMA_AreaM2.ValueVariant := QrSumAreaM2.Value;
  EdCFTMA_AreaP2.ValueVariant := QrSumAreaP2.Value;
  //
  QrVMI.DisableControls;
  try
    MyObjects.SetaItensBookmarkAtivos(Self, TDBGrid(DBGIMEIs), 'Ativo');
  finally
    QrVMI.EnableControls;
  end;
end;

procedure TFmVSMOEnvAvu.ReopenVMI(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, DModG.MyPID_DB, [
  'SELECT * FROM ' + FTabVMIQtdEnvAvu,
  '']);
  if Controle <> 0 then
    QrVMI.Locate('Controle', Controle, []);
end;

procedure TFmVSMOEnvAvu.SbRMPClick(Sender: TObject);
begin
  DmModVS_CRC.PesquisaNFe(EdNFEMA_FatID, EdNFEMA_FatNum, EdNFEMA_Empresa,
  EdNFEMA_Terceiro, EdNFEMA_SerNF, EdNFEMA_nNF);
end;

procedure TFmVSMOEnvAvu.ValorTotalPeloPeso();
var
  CFTPA_CusTrKg, CFTPA_PesTrKg: Double;
begin
  CFTPA_CusTrKg := EdCFTMA_CusTrKg.ValueVariant;
  CFTPA_PesTrKg := EdCFTMA_PesTrKg.ValueVariant;
  if (CFTPA_CusTrKg >= 0.000001) and (CFTPA_PesTrKg >= 0.001) then
  begin
    //EdNFCMO_CusMOM2.ValueVariant := 0;
    EdCFTMA_ValorT.ValueVariant := CFTPA_PesTrKg * CFTPA_CusTrKg;
  end;
end;

end.
