unit VSRclArtPrpNw3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, Vcl.Mask, UnProjGroup_Vars, dmkRadioGroup,
  UnProjGroup_Consts, UnAppEnums;

type
  TFmVSRclArtPrpNw3 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    PnPartida: TPanel;
    Panel13: TPanel;
    LaVSRibCad: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SbPallet: TSpeedButton;
    Panel17: TPanel;
    EdCodigo: TdmkEdit;
    Label30: TLabel;
    Label31: TLabel;
    EdMovimCod: TdmkEdit;
    EdCacCod: TdmkEdit;
    Label32: TLabel;
    BtReclasif: TBitBtn;
    QrVSMovIts: TmySQLQuery;
    DsVSMovIts: TDataSource;
    EdVSGerRcl: TdmkEdit;
    Label33: TLabel;
    RGTipoArea: TdmkRadioGroup;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsVMI_Codi: TIntegerField;
    QrVSMovItsVMI_Ctrl: TIntegerField;
    QrVSMovItsVMI_MovimNiv: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsNO_Pallet: TWideStringField;
    QrVSMovItsNO_EMPRESA: TWideStringField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsCodigo: TIntegerField;
    CkReclasse: TCheckBox;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSPallet01: TmySQLQuery;
    QrVSPallet01Codigo: TIntegerField;
    QrVSPallet01Nome: TWideStringField;
    QrVSPallet01Lk: TIntegerField;
    QrVSPallet01DataCad: TDateField;
    QrVSPallet01DataAlt: TDateField;
    QrVSPallet01UserCad: TIntegerField;
    QrVSPallet01UserAlt: TIntegerField;
    QrVSPallet01AlterWeb: TSmallintField;
    QrVSPallet01Ativo: TSmallintField;
    QrVSPallet01Empresa: TIntegerField;
    QrVSPallet01NO_EMPRESA: TWideStringField;
    QrVSPallet01Status: TIntegerField;
    QrVSPallet01CliStat: TIntegerField;
    QrVSPallet01GraGruX: TIntegerField;
    QrVSPallet01NO_CLISTAT: TWideStringField;
    QrVSPallet01NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet01NO_STATUS: TWideStringField;
    QrVSPallet02: TmySQLQuery;
    QrVSPallet02Codigo: TIntegerField;
    QrVSPallet02Nome: TWideStringField;
    QrVSPallet02Lk: TIntegerField;
    QrVSPallet02DataCad: TDateField;
    QrVSPallet02DataAlt: TDateField;
    QrVSPallet02UserCad: TIntegerField;
    QrVSPallet02UserAlt: TIntegerField;
    QrVSPallet02AlterWeb: TSmallintField;
    QrVSPallet02Ativo: TSmallintField;
    QrVSPallet02Empresa: TIntegerField;
    QrVSPallet02NO_EMPRESA: TWideStringField;
    QrVSPallet02Status: TIntegerField;
    QrVSPallet02CliStat: TIntegerField;
    QrVSPallet02GraGruX: TIntegerField;
    QrVSPallet02NO_CLISTAT: TWideStringField;
    QrVSPallet02NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet02NO_STATUS: TWideStringField;
    QrVSPallet03: TmySQLQuery;
    QrVSPallet03Codigo: TIntegerField;
    QrVSPallet03Nome: TWideStringField;
    QrVSPallet03Lk: TIntegerField;
    QrVSPallet03DataCad: TDateField;
    QrVSPallet03DataAlt: TDateField;
    QrVSPallet03UserCad: TIntegerField;
    QrVSPallet03UserAlt: TIntegerField;
    QrVSPallet03AlterWeb: TSmallintField;
    QrVSPallet03Ativo: TSmallintField;
    QrVSPallet03Empresa: TIntegerField;
    QrVSPallet03NO_EMPRESA: TWideStringField;
    QrVSPallet03Status: TIntegerField;
    QrVSPallet03CliStat: TIntegerField;
    QrVSPallet03GraGruX: TIntegerField;
    QrVSPallet03NO_CLISTAT: TWideStringField;
    QrVSPallet03NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet03NO_STATUS: TWideStringField;
    DsVSPallet01: TDataSource;
    DsVSPallet02: TDataSource;
    DsVSPallet03: TDataSource;
    QrVSPallet04: TmySQLQuery;
    QrVSPallet04Codigo: TIntegerField;
    QrVSPallet04Nome: TWideStringField;
    QrVSPallet04Lk: TIntegerField;
    QrVSPallet04DataCad: TDateField;
    QrVSPallet04DataAlt: TDateField;
    QrVSPallet04UserCad: TIntegerField;
    QrVSPallet04UserAlt: TIntegerField;
    QrVSPallet04AlterWeb: TSmallintField;
    QrVSPallet04Ativo: TSmallintField;
    QrVSPallet04Empresa: TIntegerField;
    QrVSPallet04NO_EMPRESA: TWideStringField;
    QrVSPallet04Status: TIntegerField;
    QrVSPallet04CliStat: TIntegerField;
    QrVSPallet04GraGruX: TIntegerField;
    QrVSPallet04NO_CLISTAT: TWideStringField;
    QrVSPallet04NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet04NO_STATUS: TWideStringField;
    QrVSPallet05: TmySQLQuery;
    QrVSPallet05Codigo: TIntegerField;
    QrVSPallet05Nome: TWideStringField;
    QrVSPallet05Lk: TIntegerField;
    QrVSPallet05DataCad: TDateField;
    QrVSPallet05DataAlt: TDateField;
    QrVSPallet05UserCad: TIntegerField;
    QrVSPallet05UserAlt: TIntegerField;
    QrVSPallet05AlterWeb: TSmallintField;
    QrVSPallet05Ativo: TSmallintField;
    QrVSPallet05Empresa: TIntegerField;
    QrVSPallet05NO_EMPRESA: TWideStringField;
    QrVSPallet05Status: TIntegerField;
    QrVSPallet05CliStat: TIntegerField;
    QrVSPallet05GraGruX: TIntegerField;
    QrVSPallet05NO_CLISTAT: TWideStringField;
    QrVSPallet05NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet05NO_STATUS: TWideStringField;
    QrVSPallet06: TmySQLQuery;
    QrVSPallet06Codigo: TIntegerField;
    QrVSPallet06Nome: TWideStringField;
    QrVSPallet06Lk: TIntegerField;
    QrVSPallet06DataCad: TDateField;
    QrVSPallet06DataAlt: TDateField;
    QrVSPallet06UserCad: TIntegerField;
    QrVSPallet06UserAlt: TIntegerField;
    QrVSPallet06AlterWeb: TSmallintField;
    QrVSPallet06Ativo: TSmallintField;
    QrVSPallet06Empresa: TIntegerField;
    QrVSPallet06NO_EMPRESA: TWideStringField;
    QrVSPallet06Status: TIntegerField;
    QrVSPallet06CliStat: TIntegerField;
    QrVSPallet06GraGruX: TIntegerField;
    QrVSPallet06NO_CLISTAT: TWideStringField;
    QrVSPallet06NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet06NO_STATUS: TWideStringField;
    DsVSPallet04: TDataSource;
    DsVSPallet05: TDataSource;
    DsVSPallet06: TDataSource;
    QrVSPallet07: TmySQLQuery;
    QrVSPallet07Codigo: TIntegerField;
    QrVSPallet07Nome: TWideStringField;
    QrVSPallet07Lk: TIntegerField;
    QrVSPallet07DataCad: TDateField;
    QrVSPallet07DataAlt: TDateField;
    QrVSPallet07UserCad: TIntegerField;
    QrVSPallet07UserAlt: TIntegerField;
    QrVSPallet07AlterWeb: TSmallintField;
    QrVSPallet07Ativo: TSmallintField;
    QrVSPallet07Empresa: TIntegerField;
    QrVSPallet07NO_EMPRESA: TWideStringField;
    QrVSPallet07Status: TIntegerField;
    QrVSPallet07CliStat: TIntegerField;
    QrVSPallet07GraGruX: TIntegerField;
    QrVSPallet07NO_CLISTAT: TWideStringField;
    QrVSPallet07NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet07NO_STATUS: TWideStringField;
    QrVSPallet08: TmySQLQuery;
    QrVSPallet08Codigo: TIntegerField;
    QrVSPallet08Nome: TWideStringField;
    QrVSPallet08Lk: TIntegerField;
    QrVSPallet08DataCad: TDateField;
    QrVSPallet08DataAlt: TDateField;
    QrVSPallet08UserCad: TIntegerField;
    QrVSPallet08UserAlt: TIntegerField;
    QrVSPallet08AlterWeb: TSmallintField;
    QrVSPallet08Ativo: TSmallintField;
    QrVSPallet08Empresa: TIntegerField;
    QrVSPallet08NO_EMPRESA: TWideStringField;
    QrVSPallet08Status: TIntegerField;
    QrVSPallet08CliStat: TIntegerField;
    QrVSPallet08GraGruX: TIntegerField;
    QrVSPallet08NO_CLISTAT: TWideStringField;
    QrVSPallet08NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet08NO_STATUS: TWideStringField;
    QrVSPallet09: TmySQLQuery;
    QrVSPallet09Codigo: TIntegerField;
    QrVSPallet09Nome: TWideStringField;
    QrVSPallet09Lk: TIntegerField;
    QrVSPallet09DataCad: TDateField;
    QrVSPallet09DataAlt: TDateField;
    QrVSPallet09UserCad: TIntegerField;
    QrVSPallet09UserAlt: TIntegerField;
    QrVSPallet09AlterWeb: TSmallintField;
    QrVSPallet09Ativo: TSmallintField;
    QrVSPallet09Empresa: TIntegerField;
    QrVSPallet09NO_EMPRESA: TWideStringField;
    QrVSPallet09Status: TIntegerField;
    QrVSPallet09CliStat: TIntegerField;
    QrVSPallet09GraGruX: TIntegerField;
    QrVSPallet09NO_CLISTAT: TWideStringField;
    QrVSPallet09NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet09NO_STATUS: TWideStringField;
    DsVSPallet07: TDataSource;
    DsVSPallet08: TDataSource;
    DsVSPallet09: TDataSource;
    QrVSPallet10: TmySQLQuery;
    QrVSPallet10Codigo: TIntegerField;
    QrVSPallet10Nome: TWideStringField;
    QrVSPallet10Lk: TIntegerField;
    QrVSPallet10DataCad: TDateField;
    QrVSPallet10DataAlt: TDateField;
    QrVSPallet10UserCad: TIntegerField;
    QrVSPallet10UserAlt: TIntegerField;
    QrVSPallet10AlterWeb: TSmallintField;
    QrVSPallet10Ativo: TSmallintField;
    QrVSPallet10Empresa: TIntegerField;
    QrVSPallet10NO_EMPRESA: TWideStringField;
    QrVSPallet10Status: TIntegerField;
    QrVSPallet10CliStat: TIntegerField;
    QrVSPallet10GraGruX: TIntegerField;
    QrVSPallet10NO_CLISTAT: TWideStringField;
    QrVSPallet10NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet10NO_STATUS: TWideStringField;
    QrVSPallet11: TmySQLQuery;
    QrVSPallet11Codigo: TIntegerField;
    QrVSPallet11Nome: TWideStringField;
    QrVSPallet11Lk: TIntegerField;
    QrVSPallet11DataCad: TDateField;
    QrVSPallet11DataAlt: TDateField;
    QrVSPallet11UserCad: TIntegerField;
    QrVSPallet11UserAlt: TIntegerField;
    QrVSPallet11AlterWeb: TSmallintField;
    QrVSPallet11Ativo: TSmallintField;
    QrVSPallet11Empresa: TIntegerField;
    QrVSPallet11NO_EMPRESA: TWideStringField;
    QrVSPallet11Status: TIntegerField;
    QrVSPallet11CliStat: TIntegerField;
    QrVSPallet11GraGruX: TIntegerField;
    QrVSPallet11NO_CLISTAT: TWideStringField;
    QrVSPallet11NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet11NO_STATUS: TWideStringField;
    QrVSPallet12: TmySQLQuery;
    QrVSPallet12Codigo: TIntegerField;
    QrVSPallet12Nome: TWideStringField;
    QrVSPallet12Lk: TIntegerField;
    QrVSPallet12DataCad: TDateField;
    QrVSPallet12DataAlt: TDateField;
    QrVSPallet12UserCad: TIntegerField;
    QrVSPallet12UserAlt: TIntegerField;
    QrVSPallet12AlterWeb: TSmallintField;
    QrVSPallet12Ativo: TSmallintField;
    QrVSPallet12Empresa: TIntegerField;
    QrVSPallet12NO_EMPRESA: TWideStringField;
    QrVSPallet12Status: TIntegerField;
    QrVSPallet12CliStat: TIntegerField;
    QrVSPallet12GraGruX: TIntegerField;
    QrVSPallet12NO_CLISTAT: TWideStringField;
    QrVSPallet12NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet12NO_STATUS: TWideStringField;
    DsVSPallet10: TDataSource;
    DsVSPallet11: TDataSource;
    DsVSPallet12: TDataSource;
    QrVSPallet13: TmySQLQuery;
    QrVSPallet13Codigo: TIntegerField;
    QrVSPallet13Nome: TWideStringField;
    QrVSPallet13Lk: TIntegerField;
    QrVSPallet13DataCad: TDateField;
    QrVSPallet13DataAlt: TDateField;
    QrVSPallet13UserCad: TIntegerField;
    QrVSPallet13UserAlt: TIntegerField;
    QrVSPallet13AlterWeb: TSmallintField;
    QrVSPallet13Ativo: TSmallintField;
    QrVSPallet13Empresa: TIntegerField;
    QrVSPallet13NO_EMPRESA: TWideStringField;
    QrVSPallet13Status: TIntegerField;
    QrVSPallet13CliStat: TIntegerField;
    QrVSPallet13GraGruX: TIntegerField;
    QrVSPallet13NO_CLISTAT: TWideStringField;
    QrVSPallet13NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet13NO_STATUS: TWideStringField;
    QrVSPallet14: TmySQLQuery;
    QrVSPallet14Codigo: TIntegerField;
    QrVSPallet14Nome: TWideStringField;
    QrVSPallet14Lk: TIntegerField;
    QrVSPallet14DataCad: TDateField;
    QrVSPallet14DataAlt: TDateField;
    QrVSPallet14UserCad: TIntegerField;
    QrVSPallet14UserAlt: TIntegerField;
    QrVSPallet14AlterWeb: TSmallintField;
    QrVSPallet14Ativo: TSmallintField;
    QrVSPallet14Empresa: TIntegerField;
    QrVSPallet14NO_EMPRESA: TWideStringField;
    QrVSPallet14Status: TIntegerField;
    QrVSPallet14CliStat: TIntegerField;
    QrVSPallet14GraGruX: TIntegerField;
    QrVSPallet14NO_CLISTAT: TWideStringField;
    QrVSPallet14NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet14NO_STATUS: TWideStringField;
    QrVSPallet15: TmySQLQuery;
    QrVSPallet15Codigo: TIntegerField;
    QrVSPallet15Nome: TWideStringField;
    QrVSPallet15Lk: TIntegerField;
    QrVSPallet15DataCad: TDateField;
    QrVSPallet15DataAlt: TDateField;
    QrVSPallet15UserCad: TIntegerField;
    QrVSPallet15UserAlt: TIntegerField;
    QrVSPallet15AlterWeb: TSmallintField;
    QrVSPallet15Ativo: TSmallintField;
    QrVSPallet15Empresa: TIntegerField;
    QrVSPallet15NO_EMPRESA: TWideStringField;
    QrVSPallet15Status: TIntegerField;
    QrVSPallet15CliStat: TIntegerField;
    QrVSPallet15GraGruX: TIntegerField;
    QrVSPallet15NO_CLISTAT: TWideStringField;
    QrVSPallet15NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet15NO_STATUS: TWideStringField;
    DsVSPallet13: TDataSource;
    DsVSPallet14: TDataSource;
    DsVSPallet15: TDataSource;
    PnPallets: TPanel;
    Panel21: TPanel;
    PnTecla: TPanel;
    Panel25: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    LaVSRibCla: TLabel;
    SbPallet01: TSpeedButton;
    EdPallet01: TdmkEditCB;
    CBPallet01: TdmkDBLookupComboBox;
    Panel9: TPanel;
    Panel7: TPanel;
    PnTecla2: TPanel;
    Panel8: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    SBPallet02: TSpeedButton;
    EdPallet02: TdmkEditCB;
    CBPallet02: TdmkDBLookupComboBox;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    SBPallet03: TSpeedButton;
    EdPallet03: TdmkEditCB;
    CBPallet03: TdmkDBLookupComboBox;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    SBPallet04: TSpeedButton;
    EdPallet04: TdmkEditCB;
    CBPallet04: TdmkDBLookupComboBox;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel20: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    SBPallet05: TSpeedButton;
    EdPallet05: TdmkEditCB;
    CBPallet05: TdmkDBLookupComboBox;
    Panel22: TPanel;
    Panel23: TPanel;
    Panel24: TPanel;
    Label25: TLabel;
    Label26: TLabel;
    SBPallet06: TSpeedButton;
    EdPallet06: TdmkEditCB;
    CBPallet06: TdmkDBLookupComboBox;
    Panel26: TPanel;
    Panel27: TPanel;
    Panel28: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    SBPallet07: TSpeedButton;
    EdPallet07: TdmkEditCB;
    CBPallet07: TdmkDBLookupComboBox;
    Panel29: TPanel;
    Panel53: TPanel;
    Panel54: TPanel;
    Label28: TLabel;
    Label29: TLabel;
    SBPallet08: TSpeedButton;
    EdPallet08: TdmkEditCB;
    CBPallet08: TdmkDBLookupComboBox;
    Panel55: TPanel;
    Panel30: TPanel;
    Panel31: TPanel;
    Panel32: TPanel;
    Label4: TLabel;
    Label7: TLabel;
    SBPallet09: TSpeedButton;
    EdPallet09: TdmkEditCB;
    CBPallet09: TdmkDBLookupComboBox;
    Panel33: TPanel;
    Panel34: TPanel;
    Panel35: TPanel;
    Panel36: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    SBPallet10: TSpeedButton;
    EdPallet10: TdmkEditCB;
    CBPallet10: TdmkDBLookupComboBox;
    Panel37: TPanel;
    Panel38: TPanel;
    Panel39: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    SBPallet11: TSpeedButton;
    EdPallet11: TdmkEditCB;
    CBPallet11: TdmkDBLookupComboBox;
    Panel40: TPanel;
    Panel41: TPanel;
    Panel42: TPanel;
    Label14: TLabel;
    Label17: TLabel;
    SBPallet12: TSpeedButton;
    EdPallet12: TdmkEditCB;
    CBPallet12: TdmkDBLookupComboBox;
    Panel43: TPanel;
    Panel44: TPanel;
    Panel45: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    SBPallet13: TSpeedButton;
    EdPallet13: TdmkEditCB;
    CBPallet13: TdmkDBLookupComboBox;
    Panel46: TPanel;
    Panel47: TPanel;
    Panel48: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    SBPallet14: TSpeedButton;
    EdPallet14: TdmkEditCB;
    CBPallet14: TdmkDBLookupComboBox;
    Panel49: TPanel;
    Panel50: TPanel;
    Panel51: TPanel;
    Label24: TLabel;
    Label27: TLabel;
    SBPallet15: TSpeedButton;
    EdPallet15: TdmkEditCB;
    CBPallet15: TdmkDBLookupComboBox;
    Panel52: TPanel;
    QrVSMovItsClientMO: TIntegerField;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbPalletClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPalletRedefinido(Sender: TObject);
    procedure BtReclasifClick(Sender: TObject);
    procedure CkReclasseClick(Sender: TObject);
    procedure SBPallet07Click(Sender: TObject);
    procedure SbPallet01Click(Sender: TObject);
    procedure SBPallet02Click(Sender: TObject);
    procedure SBPallet03Click(Sender: TObject);
    procedure SBPallet04Click(Sender: TObject);
    procedure SBPallet05Click(Sender: TObject);
    procedure SBPallet06Click(Sender: TObject);
    procedure SBPallet08Click(Sender: TObject);
    procedure SBPallet09Click(Sender: TObject);
    procedure SBPallet10Click(Sender: TObject);
    procedure SBPallet11Click(Sender: TObject);
    procedure SBPallet12Click(Sender: TObject);
    procedure SBPallet13Click(Sender: TObject);
    procedure SBPallet14Click(Sender: TObject);
    procedure SBPallet15Click(Sender: TObject);
  private
    { Private declarations }
    //procedure ReopenVSGerArtDst(Controle: Integer);
    procedure ReopenPallets();
    procedure CadastraPallet(EdPallet: TdmkEditCB; CBPallet:
              TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery);
    procedure ReopenVSPallet(QrVSPallet: TmySQLQuery; PalletIncludente: Integer = 0);
  public
    { Public declarations }
    F_Pallet01_ValueVariant: Integer;
    FCodigo, FCacCod, FFornecMO: Integer;
    FMovimID: TEstqMovimID;
  end;

  var
  FmVSRclArtPrpNw3: TFmVSRclArtPrpNw3;

implementation

uses UnMyObjects, Module, DmkDAC_PF, VSPalletAdd, MyDBCheck, ModuleGeral,
MyListas, UMySQLModule, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSRclArtPrpNw3.BtOKClick(Sender: TObject);
var
  Empresa, FornecMO, ClientMO, Fornecedor, Codigo, MovimCod, VSGerRcl, VsMovIts,
  I, Controle,
  LstPal01, LstPal02, LstPal03, LstPal04, LstPal05,
  LstPal06, LstPal07, LstPal08, LstPal09, LstPal10,
  LstPal11, LstPal12, LstPal13, LstPal14, LstPal15,
  BxaGraGruX,
  GraGruX01, GraGruX02, GraGruX03, GraGruX04, GraGruX05,
  GraGruX06, GraGruX07, GraGruX08, GraGruX09, GraGruX10,
  GraGruX11, GraGruX12, GraGruX13, GraGruX14, GraGruX15,
  CtrlSorc01, CtrlSorc02, CtrlSorc03, CtrlSorc04, CtrlSorc05,
  CtrlSorc06, CtrlSorc07, CtrlSorc08, CtrlSorc09, CtrlSorc10,
  CtrlSorc11, CtrlSorc12, CtrlSorc13, CtrlSorc14, CtrlSorc15,
  CtrlDest01, CtrlDest02,CtrlDest03, CtrlDest04, CtrlDest05,
  CtrlDest06, CtrlDest07,CtrlDest08, CtrlDest09, CtrlDest10,
  CtrlDest11, CtrlDest12,CtrlDest13, CtrlDest14, CtrlDest15,
  BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, TipoArea, VSPallet,
  GraGruX0, VSPaRclCab, CacCod, VMI_XXX, BxaMovimID, VSMulFrnCab, StqCenLoc: Integer;
  DtHrIni, DtHrCfgCla, DtHrLibCla: String;
  SrcPallet: Integer;
  IuvpeiInn, IuvpeiBxa: TInsUpdVMIPrcExecID;
  //
begin
  SrcPallet := EdPallet.ValueVariant;
  if MyObjects.FIC(SrcPallet = 0, EdPallet, 'Pallet inválido!') then
    Exit;
  Codigo         := EdCodigo.ValueVariant;
  CacCod         := EdCacCod.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  GraGruX0       := QrVSMovItsGraGruX.Value;
  VSPallet       := QrVSMovItsPallet.Value;
  VSGerRcl       := EdVSGerRcl.ValueVariant;
  VSMovIts       := QrVSMovItsControle.Value;
  Empresa        := QrVSMovItsEmpresa.Value;
  FornecMO       := FFornecMO;
  ClientMO       := QrVSMovItsClientMO.Value;
  Fornecedor     := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  FMovimID       := emidReclasXXUni;
  TipoArea       := RGTipoArea.ItemIndex;

  // Nao permitir duas vezes o mesmo pallet
  LstPal01 := EdPallet01.ValueVariant;
  LstPal02 := EdPallet02.ValueVariant;
  LstPal03 := EdPallet03.ValueVariant;
  LstPal04 := EdPallet04.ValueVariant;
  LstPal05 := EdPallet05.ValueVariant;
  LstPal06 := EdPallet06.ValueVariant;
  LstPal07 := EdPallet07.ValueVariant;
  LstPal08 := EdPallet08.ValueVariant;
  LstPal09 := EdPallet09.ValueVariant;
  LstPal10 := EdPallet10.ValueVariant;
  LstPal11 := EdPallet11.ValueVariant;
  LstPal12 := EdPallet12.ValueVariant;
  LstPal13 := EdPallet13.ValueVariant;
  LstPal14 := EdPallet14.ValueVariant;
  LstPal15 := EdPallet15.ValueVariant;
  //
  GraGruX01 := QrVSPallet01GraGruX.Value;
  GraGruX02 := QrVSPallet02GraGruX.Value;
  GraGruX03 := QrVSPallet03GraGruX.Value;
  GraGruX04 := QrVSPallet04GraGruX.Value;
  GraGruX05 := QrVSPallet05GraGruX.Value;
  GraGruX06 := QrVSPallet06GraGruX.Value;
  GraGruX07 := QrVSPallet07GraGruX.Value;
  GraGruX08 := QrVSPallet08GraGruX.Value;
  GraGruX09 := QrVSPallet09GraGruX.Value;
  GraGruX10 := QrVSPallet10GraGruX.Value;
  GraGruX11 := QrVSPallet11GraGruX.Value;
  GraGruX12 := QrVSPallet12GraGruX.Value;
  GraGruX13 := QrVSPallet13GraGruX.Value;
  GraGruX14 := QrVSPallet14GraGruX.Value;
  GraGruX15 := QrVSPallet15GraGruX.Value;
  //
  if VS_CRC_PF.PalletDuplicado(VAR_CLA_ART_RIB_MAX_BOX_06,
    LstPal01, LstPal02, LstPal03, LstPal04, LstPal05, LstPal06) then
      Exit;
  //
  // Ver se selecionou pelo menos um pallet!
  if MyObjects.FIC((LstPal01 = 0) and (LstPal02 = 0) and (LstPal03 = 0)
  and (LstPal04 = 0) and (LstPal05 = 0) and (LstPal06 = 0), nil,
  'Informe pelo menos um pallet!') then
     Exit;
  //
  //Veificar se a empresa dos pallets selecionados eh a mesma do pallet a reclassificar!
  if MyObjects.FIC((LstPal01 <> 0) and (QrVSPallet01Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 01!') then Exit;
  if MyObjects.FIC((LstPal02 <> 0) and (QrVSPallet02Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 02!') then Exit;
  if MyObjects.FIC((LstPal03 <> 0) and (QrVSPallet03Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 03!') then Exit;
  if MyObjects.FIC((LstPal04 <> 0) and (QrVSPallet04Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 04!') then Exit;
  if MyObjects.FIC((LstPal05 <> 0) and (QrVSPallet05Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 05!') then Exit;
  if MyObjects.FIC((LstPal06 <> 0) and (QrVSPallet06Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 06!') then Exit;
  if MyObjects.FIC((LstPal07 <> 0) and (QrVSPallet07Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 07!') then Exit;
  if MyObjects.FIC((LstPal08 <> 0) and (QrVSPallet08Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 08!') then Exit;
  if MyObjects.FIC((LstPal09 <> 0) and (QrVSPallet09Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 09!') then Exit;
  if MyObjects.FIC((LstPal10 <> 0) and (QrVSPallet10Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 10!') then Exit;
  if MyObjects.FIC((LstPal11 <> 0) and (QrVSPallet11Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 01!') then Exit;
  if MyObjects.FIC((LstPal12 <> 0) and (QrVSPallet12Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 12!') then Exit;
  if MyObjects.FIC((LstPal13 <> 0) and (QrVSPallet13Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 13!') then Exit;
  if MyObjects.FIC((LstPal14 <> 0) and (QrVSPallet14Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 14!') then Exit;
  if MyObjects.FIC((LstPal15 <> 0) and (QrVSPallet15Empresa.Value <> Empresa),
  nil, 'Empresa difere no BOX 15!') then Exit;
  //
  StqCenLoc := EdStqCenLoc.ValueVariant;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local (4)!') then
    Exit;
  DtHrCfgCla     := Geral.FDT(DModG.ObtemAgora(), 109);
  DtHrLibCla     := DtHrCfgCla;
  CacCod :=
    UMyMod.BPGS1I32('vscaccab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, CacCod);
  VSGerRcl :=
    UMyMod.BPGS1I32('vsgerrcla', 'Codigo', '', '', tsPos, ImgTipo.SQLType, VSGerRcl);
  MovimCod :=
    UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  Codigo :=
    UMyMod.BPGS1I32('vsparclcaba', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  //
  if ImgTipo.SQLType = stIns then
    VS_CRC_PF.InsereVSCacCab(CacCod, emidReclasXXUni, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsparclcaba', False, [
  'VSPallet', CO_FLD_TAB_VMI,
  'LstPal01', 'LstPal02', 'LstPal03', 'LstPal04', 'LstPal05',
  'LstPal06', 'LstPal07', 'LstPal08', 'LstPal09', 'LstPal10',
  'LstPal11', 'LstPal12', 'LstPal13', 'LstPal14', 'LstPal15',
  'VSGerRcl',
  'CacCod'], [
  'Codigo'], [
  VSPallet, VSMovIts,
  LstPal01, LstPal02, LstPal03, LstPal04, LstPal05,
  LstPal06, LstPal07, LstPal08, LstPal09, LstPal10,
  LstPal11, LstPal12, LstPal13, LstPal14, LstPal15,
  VSGerRcl,
  CacCod], [
  Codigo], True) then
  begin
    VSPaRclCab := Codigo;
    if ImgTipo.SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidReclasXXUni, Codigo);
    //
    Codigo := VSGerRcl;
    UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsgerrcla', False, [
    'MovimCod', 'GraGruX', 'VSPallet',
    (*'Nome',*) 'Empresa', 'DtHrLibCla',
    'DtHrCfgCla', (*'DtHrFimCla',*) 'TipoArea',
    'CacCod'], [
    'Codigo'], [
    MovimCod, GraGruX0, VSPallet,
    (*Nome,*) Empresa, DtHrLibCla,
    DtHrCfgCla, (*DtHrFimCla,*) TipoArea,
    CacCod], [
    Codigo], True);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspalleta', False, [
    'GerRclCab'], ['Codigo'], [
    VSGerRcl], [VSPallet], True);
    //
    BxaGraGruX   := QrVSMovItsGraGruX.Value;
    BxaMovimNiv  := QrVSMovItsMovimNiv.Value;
    BxaSrcNivel1 := QrVSMovItsCodigo.Value;
    BxaSrcNivel2 := QrVSMovItsControle.Value;
    BxaMovimID   := QrVSMovItsMovimID.Value;
    //
    VMI_XXX     := QrVSMovItsControle.Value;
    //
    IuvpeiInn := iuvpei046(*Reclassificação de artigo de ribeira (UNI) 3/4*);
    IuvpeiBxa := iuvpei047(*Baixa de artigo de ribeira em reclassificação (UNI) 3/4*);
    //
    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      01, LstPal01, GraGruX01, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc01, CtrlDest01);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      02, LstPal02, GraGruX02, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc02, CtrlDest02);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      03, LstPal03, GraGruX03, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc03, CtrlDest03);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      04, LstPal04, GraGruX04, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc04, CtrlDest04);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      05, LstPal05, GraGruX05, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc05, CtrlDest05);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      06, LstPal06, GraGruX06, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc06, CtrlDest06);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      07, LstPal07, GraGruX07, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc07, CtrlDest07);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      08, LstPal08, GraGruX08, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc08, CtrlDest08);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      09, LstPal09, GraGruX09, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc09, CtrlDest09);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      10, LstPal10, GraGruX10, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc10, CtrlDest10);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      11, LstPal11, GraGruX11, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc11, CtrlDest11);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      12, LstPal12, GraGruX12, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc12, CtrlDest12);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      13, LstPal13, GraGruX13, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc13, CtrlDest13);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      14, LstPal14, GraGruX14, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc14, CtrlDest14);

    VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab, FMovimID, Codigo, MovimCod,
      BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VMI_XXX,
      15, LstPal15, GraGruX15, SrcPallet, StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc15, CtrlDest15);

    //
    FCodigo := VSPaRclCab;
    FCacCod := CacCod;
    Close;
  end;
end;

procedure TFmVSRclArtPrpNw3.BtReclasifClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSGeraArt(0, 0);
  //ReopenVSGerArtDst(0);
  ReopenPallets();
end;

procedure TFmVSRclArtPrpNw3.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSRclArtPrpNw3.CadastraPallet(EdPallet: TdmkEditCB;
  CBPallet: TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery);
const
  GraGruX = 0;
begin
  VS_CRC_PF.CadastraPalletRibCla(QrVSMovItsEmpresa.Value, QrVSMovItsClientMO.Value,
    EdPallet, CBPallet, QrVSPallet, emidClassArtXXUni, GraGruX);
end;

procedure TFmVSRclArtPrpNw3.CkReclasseClick(Sender: TObject);
begin
  ReopenPallets();
end;

procedure TFmVSRclArtPrpNw3.EdPalletRedefinido(Sender: TObject);
begin
  SbPallet.Enabled := EdPallet.ValueVariant > 0;
end;

procedure TFmVSRclArtPrpNw3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSRclArtPrpNw3.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo := 0;
  FCacCod := 0;
  ReopenPallets();
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmVSRclArtPrpNw3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSRclArtPrpNw3.ReopenPallets();
var
  SQL_Rcl: String;
begin
  if CkReclasse.Checked then
    SQL_Rcl := ' '
  else
    SQL_Rcl := 'AND vsp.GerRclCab = 0 ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT vmi.Controle, vmi.MovimNiv, vmi.Codigo, ',
  'vmi.Empresa, vmi.ClientMO, vmi.MovimID, vmi.GraGruX, ',
  'vmi.Pallet, vmi.Codigo VMI_Codi, vmi.Controle VMI_Ctrl, ',
  'vmi.MovimNiv VMI_MovimNiv, vmi.Terceiro, vmi.Ficha, ',
  'MAX(DataHora) DataHora, SUM(vmi.Pecas) Pecas,   ',
  'IF(MIN(AreaM2) > 0.01, 0, SUM(vmi.AreaM2)) AreaM2,   ',
  'IF(MIN(AreaP2) > 0.01, 0, SUM(vmi.AreaP2)) AreaP2,   ',
  'IF(MIN(PesoKg) > 0.001, 0, SUM(vmi.PesoKg)) PesoKg,   ',
  'CONCAT(gg1.Nome,    ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),    ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))    ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,    ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,   ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,   ',
  'vmi.VSMulFrnCab ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi    ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX    ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet    ',
  'LEFT JOIN entidades ent ON ent.Codigo=vmi.Empresa  ',
  'LEFT JOIN entidades frn ON frn.Codigo=vmi.Terceiro   ',
  'WHERE vmi.Pecas > 0 ',
  //'AND vmi.Empresa=-11  ',
  'AND vmi.Pallet > 0 ',
  'AND vmi.SdoVrtPeca > 0 ',
  SQL_Rcl,
  'AND vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidPreReclasse)),
  'GROUP BY vmi.Pallet, vmi.GraGruX, vmi.Empresa ',
  'ORDER BY NO_PRD_TAM_COR ',
  ' ']);
end;

procedure TFmVSRclArtPrpNw3.ReopenVSPallet(QrVSPallet: TmySQLQuery; PalletIncludente: Integer);
begin
  if PalletIncludente > 0 then
    VS_CRC_PF.ReopenVSPallet(QrVSPallet, QrVSMovItsEmpresa.Value,
    QrVSMovItsClientMO.Value, 0, '', [PalletIncludente])
  else
    VS_CRC_PF.ReopenVSPallet(QrVSPallet, QrVSMovItsEmpresa.Value,
    QrVSMovItsClientMO.Value, 0, '', []);
end;

procedure TFmVSRclArtPrpNw3.SbPallet01Click(Sender: TObject);
begin
  CadastraPallet(EdPallet01, CBPallet01, QrVSPallet01);
end;

procedure TFmVSRclArtPrpNw3.SBPallet02Click(Sender: TObject);
begin
  CadastraPallet(EdPallet02, CBPallet02, QrVSPallet02);
end;

procedure TFmVSRclArtPrpNw3.SBPallet03Click(Sender: TObject);
begin
  CadastraPallet(EdPallet03, CBPallet03, QrVSPallet03);
end;

procedure TFmVSRclArtPrpNw3.SBPallet04Click(Sender: TObject);
begin
  CadastraPallet(EdPallet04, CBPallet04, QrVSPallet04);
end;

procedure TFmVSRclArtPrpNw3.SBPallet05Click(Sender: TObject);
begin
  CadastraPallet(EdPallet05, CBPallet05, QrVSPallet05);
end;

procedure TFmVSRclArtPrpNw3.SBPallet06Click(Sender: TObject);
begin
  CadastraPallet(EdPallet06, CBPallet06, QrVSPallet06);
end;

procedure TFmVSRclArtPrpNw3.SBPallet07Click(Sender: TObject);
begin
  CadastraPallet(EdPallet07, CBPallet07, QrVSPallet07);
end;

procedure TFmVSRclArtPrpNw3.SBPallet08Click(Sender: TObject);
begin
  CadastraPallet(EdPallet08, CBPallet08, QrVSPallet08);
end;

procedure TFmVSRclArtPrpNw3.SBPallet09Click(Sender: TObject);
begin
  CadastraPallet(EdPallet09, CBPallet09, QrVSPallet09);
end;

procedure TFmVSRclArtPrpNw3.SBPallet10Click(Sender: TObject);
begin
  CadastraPallet(EdPallet10, CBPallet10, QrVSPallet10);
end;

procedure TFmVSRclArtPrpNw3.SBPallet11Click(Sender: TObject);
begin
  CadastraPallet(EdPallet11, CBPallet11, QrVSPallet11);
end;

procedure TFmVSRclArtPrpNw3.SBPallet12Click(Sender: TObject);
begin
  CadastraPallet(EdPallet12, CBPallet12, QrVSPallet12);
end;

procedure TFmVSRclArtPrpNw3.SBPallet13Click(Sender: TObject);
begin
  CadastraPallet(EdPallet13, CBPallet13, QrVSPallet13);
end;

procedure TFmVSRclArtPrpNw3.SBPallet14Click(Sender: TObject);
begin
  CadastraPallet(EdPallet14, CBPallet14, QrVSPallet14);
end;

procedure TFmVSRclArtPrpNw3.SBPallet15Click(Sender: TObject);
begin
  CadastraPallet(EdPallet15, CBPallet15, QrVSPallet15);
end;

procedure TFmVSRclArtPrpNw3.SbPalletClick(Sender: TObject);
begin
  if MyObjects.FIC(EdStqCenLoc.ValueVariant = 0, EdStqCenLoc, 'Informe o local! (5)') then
    Exit;
  if MyObjects.FIC(EdPallet.ValueVariant = 0, EdPallet,
  'Informe o pallet de artigo de ribeira!') then
    Exit
  else
  begin
    //
    PnPartida.Enabled := False;
    PnPallets.Visible := True;
    BtOK.Enabled      := True;
    //
    ReopenVSPallet(QrVSPallet01, F_Pallet01_ValueVariant);
    ReopenVSPallet(QrVSPallet02);
    ReopenVSPallet(QrVSPallet03);
    ReopenVSPallet(QrVSPallet04);
    ReopenVSPallet(QrVSPallet05);
    ReopenVSPallet(QrVSPallet06);
    ReopenVSPallet(QrVSPallet07);
    ReopenVSPallet(QrVSPallet08);
    ReopenVSPallet(QrVSPallet09);
    ReopenVSPallet(QrVSPallet10);
    ReopenVSPallet(QrVSPallet11);
    ReopenVSPallet(QrVSPallet12);
    ReopenVSPallet(QrVSPallet13);
    ReopenVSPallet(QrVSPallet14);
    ReopenVSPallet(QrVSPallet15);
  end;
end;

end.
