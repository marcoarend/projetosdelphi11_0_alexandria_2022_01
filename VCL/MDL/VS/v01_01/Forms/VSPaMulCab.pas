unit VSPaMulCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker, Variants,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, dmkDBGridZTO,
  UnProjGroup_Consts, UnGrl_Consts, UnAppEnums;

type
  TFmVSPaMulCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSPaMulCab: TmySQLQuery;
    DsVSPaMulCab: TDataSource;
    QrVSPaMulIts: TmySQLQuery;
    DsVSPaMulIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSPaMulCabCodigo: TIntegerField;
    QrVSPaMulCabMovimCod: TIntegerField;
    QrVSPaMulCabEmpresa: TIntegerField;
    QrVSPaMulCabDataHora: TDateTimeField;
    QrVSPaMulCabPecas: TFloatField;
    QrVSPaMulCabPesoKg: TFloatField;
    QrVSPaMulCabAreaM2: TFloatField;
    QrVSPaMulCabAreaP2: TFloatField;
    QrVSPaMulCabLk: TIntegerField;
    QrVSPaMulCabDataCad: TDateField;
    QrVSPaMulCabDataAlt: TDateField;
    QrVSPaMulCabUserCad: TIntegerField;
    QrVSPaMulCabUserAlt: TIntegerField;
    QrVSPaMulCabAlterWeb: TSmallintField;
    QrVSPaMulCabAtivo: TSmallintField;
    QrVSPaMulCabNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TdmkDBGridZTO;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    frxDsVSPaMulIts: TfrxDBDataset;
    frxDsVSPaMulCab: TfrxDBDataset;
    N2: TMenuItem;
    Fichas1: TMenuItem;
    QrVSGerArtNew: TmySQLQuery;
    QrVSGerArtNewCodigo: TIntegerField;
    QrVSGerArtNewControle: TIntegerField;
    QrVSGerArtNewMovimCod: TIntegerField;
    QrVSGerArtNewEmpresa: TIntegerField;
    QrVSGerArtNewMovimID: TIntegerField;
    QrVSGerArtNewGraGruX: TIntegerField;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewLk: TIntegerField;
    QrVSGerArtNewDataCad: TDateField;
    QrVSGerArtNewDataAlt: TDateField;
    QrVSGerArtNewUserCad: TIntegerField;
    QrVSGerArtNewUserAlt: TIntegerField;
    QrVSGerArtNewAlterWeb: TSmallintField;
    QrVSGerArtNewAtivo: TSmallintField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewSrcMovID: TIntegerField;
    QrVSGerArtNewSrcNivel1: TIntegerField;
    QrVSGerArtNewSrcNivel2: TIntegerField;
    QrVSGerArtNewPallet: TIntegerField;
    QrVSGerArtNewNO_PALLET: TWideStringField;
    QrVSGerArtNewSdoVrtArM2: TFloatField;
    QrVSGerArtNewSdoVrtPeca: TFloatField;
    QrVSGerArtNewObserv: TWideStringField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewMovimTwn: TIntegerField;
    QrVSGerArtNewCustoMOKg: TFloatField;
    QrVSGerArtNewMovimNiv: TIntegerField;
    QrVSGerArtNewTerceiro: TIntegerField;
    QrVSGerArtNewCliVenda: TIntegerField;
    QrVSGerArtNewLnkNivXtr1: TIntegerField;
    QrVSGerArtNewFicha: TIntegerField;
    QrVSGerArtNewLnkNivXtr2: TIntegerField;
    QrVSGerArtNewDataHora: TDateTimeField;
    QrVSGerArtNewMisturou: TSmallintField;
    QrVSGerArtNewCustoMOTot: TFloatField;
    QrVSGerArtNewSdoVrtPeso: TFloatField;
    QrVSGerArtNewValorMP: TFloatField;
    QrVSGerArtNewDstMovID: TIntegerField;
    QrVSGerArtNewDstNivel1: TIntegerField;
    QrVSGerArtNewDstNivel2: TIntegerField;
    QrVSGerArtNewQtdGerPeca: TFloatField;
    QrVSGerArtNewQtdGerPeso: TFloatField;
    QrVSGerArtNewQtdGerArM2: TFloatField;
    QrVSGerArtNewQtdGerArP2: TFloatField;
    QrVSGerArtNewQtdAntPeca: TFloatField;
    QrVSGerArtNewQtdAntPeso: TFloatField;
    QrVSGerArtNewQtdAntArM2: TFloatField;
    QrVSGerArtNewQtdAntArP2: TFloatField;
    QrVSGerArtNewNO_FORNECE: TWideStringField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    QrVSGerArtNewIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField;
    DsVSGerArtNew: TDataSource;
    PnPartida: TPanel;
    Panel13: TPanel;
    LaVSRibCad: TLabel;
    SbIMEI: TSpeedButton;
    EdIMEI: TdmkEditCB;
    CBIMEI: TdmkDBLookupComboBox;
    Panel17: TPanel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    EdCacCod: TdmkEdit;
    QrVSPaMulCabVMI_Sorc: TIntegerField;
    QrVSMovSrc: TmySQLQuery;
    DsVSMovSrc: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label9: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label36: TLabel;
    DBEdit5: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit21: TDBEdit;
    Label11: TLabel;
    DBEdSdoVrtPeca: TDBEdit;
    Label22: TLabel;
    DBEdSdoVrtArM2: TDBEdit;
    QrSrc: TmySQLQuery;
    QrSrcControle: TIntegerField;
    QrSrcSrcNivel2: TIntegerField;
    QrVSPaMulCabCacCod: TIntegerField;
    Label12: TLabel;
    DBEdit22: TDBEdit;
    QrVSPaMulCabVSGerArt: TIntegerField;
    ClassesGeradas1: TMenuItem;
    BtClassesGeradas: TBitBtn;
    N1: TMenuItem;
    Baixaresidual1: TMenuItem;
    FichasCOMnomedoPallet1: TMenuItem;
    FichasSEMnomedoPallet1: TMenuItem;
    QrVSPaMulCabTemIMEIMrt: TIntegerField;
    QrVSPaMulItsCodigo: TLargeintField;
    QrVSPaMulItsControle: TLargeintField;
    QrVSPaMulItsMovimCod: TLargeintField;
    QrVSPaMulItsMovimNiv: TLargeintField;
    QrVSPaMulItsMovimTwn: TLargeintField;
    QrVSPaMulItsEmpresa: TLargeintField;
    QrVSPaMulItsTerceiro: TLargeintField;
    QrVSPaMulItsCliVenda: TLargeintField;
    QrVSPaMulItsMovimID: TLargeintField;
    QrVSPaMulItsDataHora: TDateTimeField;
    QrVSPaMulItsPallet: TLargeintField;
    QrVSPaMulItsGraGruX: TLargeintField;
    QrVSPaMulItsPecas: TFloatField;
    QrVSPaMulItsPesoKg: TFloatField;
    QrVSPaMulItsAreaM2: TFloatField;
    QrVSPaMulItsAreaP2: TFloatField;
    QrVSPaMulItsValorT: TFloatField;
    QrVSPaMulItsSrcMovID: TLargeintField;
    QrVSPaMulItsSrcNivel1: TLargeintField;
    QrVSPaMulItsSrcNivel2: TLargeintField;
    QrVSPaMulItsSrcGGX: TLargeintField;
    QrVSPaMulItsSdoVrtPeca: TFloatField;
    QrVSPaMulItsSdoVrtPeso: TFloatField;
    QrVSPaMulItsSdoVrtArM2: TFloatField;
    QrVSPaMulItsObserv: TWideStringField;
    QrVSPaMulItsSerieFch: TLargeintField;
    QrVSPaMulItsFicha: TLargeintField;
    QrVSPaMulItsMisturou: TLargeintField;
    QrVSPaMulItsFornecMO: TLargeintField;
    QrVSPaMulItsCustoMOKg: TFloatField;
    QrVSPaMulItsCustoMOM2: TFloatField;
    QrVSPaMulItsCustoMOTot: TFloatField;
    QrVSPaMulItsValorMP: TFloatField;
    QrVSPaMulItsDstMovID: TLargeintField;
    QrVSPaMulItsDstNivel1: TLargeintField;
    QrVSPaMulItsDstNivel2: TLargeintField;
    QrVSPaMulItsDstGGX: TLargeintField;
    QrVSPaMulItsQtdGerPeca: TFloatField;
    QrVSPaMulItsQtdGerPeso: TFloatField;
    QrVSPaMulItsQtdGerArM2: TFloatField;
    QrVSPaMulItsQtdGerArP2: TFloatField;
    QrVSPaMulItsQtdAntPeca: TFloatField;
    QrVSPaMulItsQtdAntPeso: TFloatField;
    QrVSPaMulItsQtdAntArM2: TFloatField;
    QrVSPaMulItsQtdAntArP2: TFloatField;
    QrVSPaMulItsNotaMPAG: TFloatField;
    QrVSPaMulItsNO_PALLET: TWideStringField;
    QrVSPaMulItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPaMulItsNO_TTW: TWideStringField;
    QrVSPaMulItsID_TTW: TLargeintField;
    QrVSPaMulItsNO_FORNECE: TWideStringField;
    QrVSPaMulItsNO_SerieFch: TWideStringField;
    QrVSPaMulItsReqMovEstq: TLargeintField;
    QrVSMovSrcCodigo: TLargeintField;
    QrVSMovSrcControle: TLargeintField;
    QrVSMovSrcMovimCod: TLargeintField;
    QrVSMovSrcMovimNiv: TLargeintField;
    QrVSMovSrcMovimTwn: TLargeintField;
    QrVSMovSrcEmpresa: TLargeintField;
    QrVSMovSrcTerceiro: TLargeintField;
    QrVSMovSrcCliVenda: TLargeintField;
    QrVSMovSrcMovimID: TLargeintField;
    QrVSMovSrcDataHora: TDateTimeField;
    QrVSMovSrcPallet: TLargeintField;
    QrVSMovSrcGraGruX: TLargeintField;
    QrVSMovSrcPecas: TFloatField;
    QrVSMovSrcPesoKg: TFloatField;
    QrVSMovSrcAreaM2: TFloatField;
    QrVSMovSrcAreaP2: TFloatField;
    QrVSMovSrcValorT: TFloatField;
    QrVSMovSrcSrcMovID: TLargeintField;
    QrVSMovSrcSrcNivel1: TLargeintField;
    QrVSMovSrcSrcNivel2: TLargeintField;
    QrVSMovSrcSrcGGX: TLargeintField;
    QrVSMovSrcSdoVrtPeca: TFloatField;
    QrVSMovSrcSdoVrtPeso: TFloatField;
    QrVSMovSrcSdoVrtArM2: TFloatField;
    QrVSMovSrcObserv: TWideStringField;
    QrVSMovSrcSerieFch: TLargeintField;
    QrVSMovSrcFicha: TLargeintField;
    QrVSMovSrcMisturou: TLargeintField;
    QrVSMovSrcFornecMO: TLargeintField;
    QrVSMovSrcCustoMOKg: TFloatField;
    QrVSMovSrcCustoMOM2: TFloatField;
    QrVSMovSrcCustoMOTot: TFloatField;
    QrVSMovSrcValorMP: TFloatField;
    QrVSMovSrcDstMovID: TLargeintField;
    QrVSMovSrcDstNivel1: TLargeintField;
    QrVSMovSrcDstNivel2: TLargeintField;
    QrVSMovSrcDstGGX: TLargeintField;
    QrVSMovSrcQtdGerPeca: TFloatField;
    QrVSMovSrcQtdGerPeso: TFloatField;
    QrVSMovSrcQtdGerArM2: TFloatField;
    QrVSMovSrcQtdGerArP2: TFloatField;
    QrVSMovSrcQtdAntPeca: TFloatField;
    QrVSMovSrcQtdAntPeso: TFloatField;
    QrVSMovSrcQtdAntArM2: TFloatField;
    QrVSMovSrcQtdAntArP2: TFloatField;
    QrVSMovSrcNotaMPAG: TFloatField;
    QrVSMovSrcStqCenLoc: TLargeintField;
    QrVSMovSrcReqMovEstq: TLargeintField;
    QrVSMovSrcGraGru1: TLargeintField;
    QrVSMovSrcNO_EstqMovimID: TWideStringField;
    QrVSMovSrcNO_MovimID: TWideStringField;
    QrVSMovSrcNO_MovimNiv: TWideStringField;
    QrVSMovSrcNO_PALLET: TWideStringField;
    QrVSMovSrcNO_PRD_TAM_COR: TWideStringField;
    QrVSMovSrcNO_FORNECE: TWideStringField;
    QrVSMovSrcNO_SerieFch: TWideStringField;
    QrVSMovSrcNO_TTW: TWideStringField;
    QrVSMovSrcID_TTW: TLargeintField;
    QrVSMovSrcNO_LOC_CEN: TWideStringField;
    QrVSPaMulItsStqCenLoc: TLargeintField;
    QrVSMovSrcClientMO: TLargeintField;
    Label23: TLabel;
    DBEdit24: TDBEdit;
    Atualizavalordeestoque1: TMenuItem;
    QrVSMovSrcNFeSer: TLargeintField;
    QrVSMovSrcNFeNum: TLargeintField;
    QrVSMovSrcVSMulNFeCab: TLargeintField;
    EdSerieRem: TdmkEdit;
    Label57: TLabel;
    EdNFeRem: TdmkEdit;
    QrVSPaMulCabSerieRem: TSmallintField;
    QrVSPaMulCabNFeRem: TIntegerField;
    QrVSPaMulItsDtCorrApo: TDateTimeField;
    QrVSPaMulItsIxxMovIX: TLargeintField;
    QrVSPaMulItsIxxFolha: TLargeintField;
    QrVSPaMulItsIxxLinha: TLargeintField;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label28: TLabel;
    Label29: TLabel;
    DBEdit26: TDBEdit;
    DBEdit28: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSPaMulCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPaMulCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSPaMulCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSPaMulCabBeforeClose(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxWET_RECUR_010_01GetValue(const VarName: string;
      var Value: Variant);
    procedure BtClassesGeradasClick(Sender: TObject);
    procedure ClassesGeradas1Click(Sender: TObject);
    procedure Baixaresidual1Click(Sender: TObject);
    procedure FichasCOMnomedoPallet1Click(Sender: TObject);
    procedure FichasSEMnomedoPallet1Click(Sender: TObject);
    procedure Atualizavalordeestoque1Click(Sender: TObject);
    procedure DBEdSdoVrtArM2Change(Sender: TObject);
    procedure DBEdSdoVrtPecaChange(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSPaMulIts(SQLType: TSQLType);
    procedure ReopenSrc();
    function  ZeraEstoquePalletOrigem(): Boolean;
    procedure ImprimeTodosPallets(InfoNO_PALLET: Boolean);
    procedure ColoreArea();
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure AtualizaNFeItens();
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSPaMulIts(Controle: Integer);

  end;

var
  FmVSPaMulCab: TFmVSPaMulCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSPaMulIts, ModuleGeral,
  Principal, VSMovImp, UnVS_PF, UnVS_CRC_PF, CreateVS;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSPaMulCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSPaMulCab.MostraFormVSPaMulIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSPaMulIts, FmVSPaMulIts, afmoNegarComAviso) then
  begin
    FmVSPaMulIts.ImgTipo.SQLType := SQLType;
    FmVSPaMulIts.FQrCab := QrVSPaMulCab;
    FmVSPaMulIts.FDsCab := DsVSPaMulCab;
    FmVSPaMulIts.FQrIts := QrVSPaMulIts;
    FmVSPaMulIts.FDataHora := QrVSPaMulCabDataHora.Value;
    FmVSPaMulIts.FEmpresa  := QrVSPaMulCabEmpresa.Value;
    FmVSPaMulIts.ReopenOrig(QrVSPaMulCabVMI_Sorc.Value);
    FmVSPaMulIts.FSrcValorT := QrVSMovSrcValorT.Value;
    FmVSPaMulIts.FSrcAreaM2 := QrVSMovSrcAreaM2.Value;
    FmVSPaMulIts.FSrcPecas  := QrVSMovSrcPecas.Value;
    FmVSPaMulIts.FVSGerArt  := QrVSPaMulCabVSGerArt.Value; //QrVSGerArtNewCodigo.Value;
    FmVSPaMulIts.FClientMO  := QrVSMovSrcClientMO.Value;

    if SQLType = stIns then
    begin
      //FmVSPaMulIts.EdCPF1.ReadOnly := False
    end else
    begin
      ReopenSrc();
      FmVSPaMulIts.EdCtrlGera.ValueVariant := QrVSPaMulItsControle.Value;
      FmVSPaMulIts.EdCtrlBaix.ValueVariant := QrSrcControle.Value;
      FmVSPaMulIts.EdMovimTwn.ValueVariant := QrVSPaMulItsMovimTwn.Value;
      //
      FmVSPaMulIts.EdGragruX.ValueVariant    := QrVSPaMulItsGraGruX.Value;
      FmVSPaMulIts.CBGragruX.KeyValue        := QrVSPaMulItsGraGruX.Value;
      //FmVSPaMulIts.EdFornecedor.ValueVariant := QrVSPaMulItsTerceiro.Value;
      //FmVSPaMulIts.CBFornecedor.KeyValue     := QrVSPaMulItsTerceiro.Value;
      //FmVSPaMulIts.EdSerieFch.ValueVariant   := QrVSPaMulItsSerieFch.Value;
      //FmVSPaMulIts.CBSerieFch.KeyValue       := QrVSPaMulItsSerieFch.Value;
      //FmVSPaMulIts.EdFicha.ValueVariant      := QrVSPaMulItsFicha.Value;
      FmVSPaMulIts.EdPallet.ValueVariant     := QrVSPaMulItsPallet.Value;
      FmVSPaMulIts.CBPallet.KeyValue         := QrVSPaMulItsPallet.Value;
      FmVSPaMulIts.EdPecas.ValueVariant      := QrVSPaMulItsPecas.Value;
      FmVSPaMulIts.EdPesoKg.ValueVariant     := QrVSPaMulItsPesoKg.Value;
      FmVSPaMulIts.EdAreaM2.ValueVariant     := QrVSPaMulItsAreaM2.Value;
      FmVSPaMulIts.EdAreaP2.ValueVariant     := QrVSPaMulItsAreaP2.Value;
      FmVSPaMulIts.EdValorMP.ValueVariant    := QrVSPaMulItsValorMP.Value;
      FmVSPaMulIts.EdCustoMOKg.ValueVariant  := QrVSPaMulItsCustoMOKg.Value;
      FmVSPaMulIts.EdFornecMO.ValueVariant   := QrVSPaMulItsFornecMO.Value;
      FmVSPaMulIts.CBFornecMO.KeyValue       := QrVSPaMulItsFornecMO.Value;
      FmVSPaMulIts.EdObserv.ValueVariant     := QrVSPaMulItsObserv.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
      FmVSPaMulIts.EdCustoMOTot.ValueVariant := QrVSPaMulItsCustoMOTot.Value;
      FmVSPaMulIts.EdValorT.ValueVariant     := QrVSPaMulItsValorT.Value;
      // Tambem no final!
      FmVSPaMulIts.ReopenVsCacItsA(QrVSPaMulCabCacCod.Value);
      FmVSPaMulIts.EdCACI.ValueVariant       := FmVSPaMulIts.QrVSCacItsAControle.Value;
      FmVSPaMulIts.EdDigitador.ValueVariant  := FmVSPaMulIts.QrVSCacItsADigitador.Value;
      FmVSPaMulIts.CbDigitador.KeyValue      := FmVSPaMulIts.QrVSCacItsADigitador.Value;
      FmVSPaMulIts.EdRevisor.ValueVariant    := FmVSPaMulIts.QrVSCacItsARevisor.Value;
      FmVSPaMulIts.CBRevisor.KeyValue        := FmVSPaMulIts.QrVSCacItsARevisor.Value;
      //
      FmVSPaMulIts.EdStqCenLoc.ValueVariant  := QrVSPaMulItsStqCenLoc.Value;
      FmVSPaMulIts.CBStqCenLoc.KeyValue      := QrVSPaMulItsStqCenLoc.Value;
      FmVSPaMulIts.EdReqMovEstq.ValueVariant := QrVSPaMulItsReqMovEstq.Value;
      //
      //FmVSPaMulIts.TPData.Date               := Int(QrVSPaMulItsDataHora.Value);
      //FmVSPaMulIts.EdHora.ValueVariant       := QrVSPaMulItsDataHora.Value;
      VS_PF.DefineDataHoraOuDtCorrApoCompos(QrVSPaMulItsDataHora.Value,
        QrVSPaMulItsDtCorrApo.Value, FmVSPaMulIts.TPData, FmVSPaMulIts.EdHora);
      DmkPF.SetArrayOfInt(FmVSPaMulIts.FPallOnEdit, [QrVSPaMulItsPallet.Value], True);
      FmVSPaMulIts.FPallOnEdit[0]              := QrVSPaMulItsPallet.Value;
      FmVSPaMulIts.RGIxxMovIX.ItemIndex        := QrVSPaMulItsIxxMovIX.Value;
      FmVSPaMulIts.EdIxxFolha.ValueVariant     := QrVSPaMulItsIxxFolha.Value;
      FmVSPaMulIts.EdIxxLinha.ValueVariant     := QrVSPaMulItsIxxLinha.Value;
    end;
    VS_PF.ReopenVSPallet(FmVSPaMulIts.QrVSPallet, FmVSPaMulIts.FEmpresa,
      FmVSPaMulIts.FClientMO, 0, '', FmVSPaMulIts.FPallOnEdit);
    if (SQLType = stUpd) and (FmVSPaMulIts.EdPallet.ValueVariant = 0) then
    begin
      FmVSPaMulIts.EdPallet.ValueVariant     := QrVSPaMulItsPallet.Value;
      FmVSPaMulIts.CBPallet.KeyValue         := QrVSPaMulItsPallet.Value;
    end;
    FmVSPaMulIts.ShowModal;
    FmVSPaMulIts.Destroy;
  end;
end;

procedure TFmVSPaMulCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSPaMulCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSPaMulCab, QrVSPaMulIts);
  //
  VS_PF.HabilitaMenuItensVSAberto(QrVSPaMulCab, QrVSPaMulCabDataHora.Value, [CabAltera1(*, CabExclui1*)]);
end;

procedure TFmVSPaMulCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSPaMulCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSPaMulIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSPaMulIts);
  //
  VS_PF.HabilitaComposVSAtivo(QrVSPaMulItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmVSPaMulCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSPaMulCabCodigo.Value, LaRegistro.Caption[2]);
end;

function TFmVSPaMulCab.ZeraEstoquePalletOrigem(): Boolean;
var
  BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2, BxaValorT: Double;
begin
(*
  Result := True;
  //
  if QrSumTSdoVrtPeca.Value > 0  then
  if Geral.MB_Pergunta(
  'Deseja zerar o estoque residual de ' + FloatToStr(QrSumTSdoVrtPeca.Value) +
  ' pe�as do pallet de origem ' + Geral.FF0(QrVSPaRclCabVSPallet.Value) + '?') =
  ID_YES then
  begin
    BxaPecas    := QrSumTSdoVrtPeca.Value;
    BxaPesoKg   := 0;
    //
    BxaAreaM2 := QrSumTSdoVrtArM2.Value;
    BxaAreaP2 := Geral.ConverteArea(QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    //
    BxaValorT   := 0; // Ver o que fazer!
    Result := VS_PF.ZeraEstoquePalletOrigemReclas(QrVSPaRclCabVSPallet.Value,
      QrVSPaRclCabCodigo.Value, QrVSPaRclCabMovimCod.Value,
      QrVSPaRclCabEmpresa.Value, BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2,
      BxaValorT);
  end;
*)
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSPaMulCab.DefParams;
begin
  VAR_GOTOTABELA := 'vspamulcaba';
  VAR_GOTOMYSQLTABLE := QrVSPaMulCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ');
  VAR_SQLx.Add('FROM vspamulcaba wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSPaMulCab.Estoque1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSPaMulCab.ImprimeTodosPallets(InfoNO_PALLET: Boolean);
const
  VSLstPalBox = '';
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
  TempTab: String;
begin
  TempTab :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp4, DModG.QrUpdPID1,
      False, 1, '_vsmovimp4_' + Self.Name);
  Empresa := QrVSPaMulCabEmpresa.Value;
  N := 0;
  QrVSPaMulIts.First;
  while not QrVSPaMulIts.Eof do
  begin
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrVSPaMulItsPallet.Value;
    //
    QrVSPaMulIts.Next;
  end;
  if N > 0 then
    VS_PF.ImprimePallet_Varios(Empresa, MyArr, TempTab, InfoNO_PALLET)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSPaMulCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSPaMulIts(stUpd);
end;

procedure TFmVSPaMulCab.CabExclui1Click(Sender: TObject);
const
  Pergunta = 'Confirma a exclus�o da desta classe?';
var
  Codigo: Integer;
begin
  Codigo := QrVSPaMulCabCodigo.Value;
  if UMyMod.ExcluiRegistroInt1(Pergunta, 'vspamulcaba', 'Codigo', Codigo,
  Dmod.MyDB) = ID_YES then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSPaMulCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSPaMulCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSPaMulCab.ItsExclui1Click(Sender: TObject);
var
  Codigo, MovimCod, CtrlBaix, SrcNivel2: Integer;
var
  Qry: TmySQLQuery;
  CacCod, VMI_Sorc, VMI_Dest, VMI_Baix: Integer;
  CacItsA: Int64;
begin
  ReopenSrc();
  //
  Codigo    := QrVSPaMulItsCodigo.Value;
  MovimCod  := QrVSPaMulItsMovimCod.Value;
  CtrlBaix  := QrSrcControle.Value;
  SrcNivel2 := QrVSPaMulCabVMI_Sorc.Value;
  //
  CacCod   := QrVSPaMulCabCacCod.Value;
  VMI_Sorc := QrVSPaMulCabVMI_Sorc.Value;
  VMI_Dest := QrVSPaMulItsControle.Value;
  VMI_Baix := QrSrcControle.Value;
  //
  if VS_PF.ExcluiControleVSMovIts(QrVSPaMulIts, TIntegerField(QrVSPaMulItsMovimCod),
  QrVSPaMulItsControle.Value, CtrlBaix, SrcNivel2, False,
  Integer(TEstqMotivDel.emtdWetCurti060)) then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      // Excluir Tamb�m da CacItsA!!!
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM vscacitsa ',
      'WHERE CacCod=' + Geral.FF0(CacCod),
      'AND VMI_Sorc=' + Geral.FF0(VMI_Sorc),
      'AND VMI_Baix=' + Geral.FF0(VMI_Baix),
      'AND VMI_Dest=' + Geral.FF0(VMI_Dest),
      '']);
      //
      CacItsA := Geral.I64(Qry.FieldByName('Controle').AsString);
      if CacItsA <> 0 then
        UMyMod.ExcluiRegistroTxt1('', 'vscacitsa', 'Controle',
          Qry.FieldByName('Controle').AsString, '', Dmod.MyDB);
    finally
      Qry.Free;
    end;
    VS_PF.AtualizaTotaisVSXxxCab('vspamulcaba', MovimCod);
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSPaMulCab.ReopenSrc();
  procedure Abre(Tabela: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSrc, Dmod.MyDb, [
    'SELECT Controle, SrcNivel2 ',
    'FROM ' + Tabela,
    'WHERE MovimCod=' + Geral.FF0(QrVSPaMulItsMovimCod.Value),
    'AND MovimTwn=' + Geral.FF0(QrVSPaMulItsMovimTwn.Value),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcClass)), //1
    '']);
  end;
begin
  Abre(CO_SEL_TAB_VMI);
(*
  if QrSrc.recordCount = 0 then
    Abre(CO_SEL_TAB_VMB);
*)
end;

procedure TFmVSPaMulCab.ReopenVSPaMulIts(Controle: Integer);
var
{
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
}
  TemIMEIMrt, MovimCod: Integer;
begin
  TemIMEIMrt := QrVSPaMulCabTemIMEIMrt.Value;
  MovimCod   := QrVSPaMulCabMovimCod.Value;
{
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSPaMulCabMovimCod.Value),
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)), //2
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaMulIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  QrVSPaMulIts.Locate('Controle', Controle, []);
}
  VS_PF.ReopenVSPaMulIts(QrVSPaMulIts, TemIMEIMrt, MovimCod, Controle);
end;

procedure TFmVSPaMulCab.DBEdSdoVrtPecaChange(Sender: TObject);
begin
  ColoreArea();
end;

procedure TFmVSPaMulCab.DBEdSdoVrtArM2Change(Sender: TObject);
begin
  ColoreArea();
end;

procedure TFmVSPaMulCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSPaMulCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSPaMulCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSPaMulCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSPaMulCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSPaMulCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPaMulCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSPaMulCabCodigo.Value;
  Close;
end;

procedure TFmVSPaMulCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSPaMulIts(stIns);
end;

{
procedure TFmVSPaMulCab.CabAltera1Click(Sender: TObject);
(*
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSPaMulCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vspamulcaba');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSPaMulCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
*)
var
  I, Codigo, MovimCod, MovimID(*, Controle*): Integer;
  Data, Hora: TDateTime;
  DataHora: String;
begin
  //Controle := QrLct.FieldByName('Controle').AsInteger;
  //
  Hora := QrVSPaMulCabDataHora.Value - Int(QrVSPaMulCabDataHora.Value);
  if not DBCheck.ObtemData(QrVSPaMulCabDataHora.Value, Data, 0, Hora, True) then
    Exit;
  //
  DataHora := Geral.FDT(Data, 109);

  // Pr� classe
(*
  Codigo   := QrVSMovSrcCodigo.Value;
  MovimCod := QrVSMovSrcMovimCod.Value;
  MovimID  := Integer(emidPreReclasse); // 15
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  CO_DATA_HORA_VMI], [
  'Codigo', 'MovimCod', 'MovimID'], [DataHora], [
  Codigo, MovimCod, MovimID], True);
*)

  // Itens classe
  Codigo   := QrVSPaMulCabCodigo.Value;
  MovimCod := QrVSPaMulCabMovimCod.Value;
  MovimID  := Integer(emidClassArtXXMul);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  CO_DATA_HORA_VMI], [
  'Codigo', 'MovimCod', 'MovimID'], [DataHora], [
  Codigo, MovimCod, MovimID], True) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspamulcaba', False, [
    CO_DATA_HORA_GRL], [
    'Codigo'], [
    DataHora], [
    Codigo], True) then
    begin
      AtualizaNFeItens();
      LocCod(Codigo, Codigo);
    end;
  end;
end;
}
procedure TFmVSPaMulCab.CabAltera1Click(Sender: TObject);
var
  DtHr: TDateTime;
  SerieRem, NFeRem: Integer;
  I, Codigo, MovimCod, MovimID(*, Controle*): Integer;
  Data, Hora: TDateTime;
  DataHora: String;
begin
  if VS_PF.ObtemDataHoraSerNumNFe(QrVSPaMulCabDataHora.Value,
  QrVSPaMulCabSerieRem.Value, QrVSPaMulCabNFeRem.Value, DtHr, SerieRem, NFeRem)
  then
  begin
    DataHora := Geral.FDT(DtHr, 109);
    // Pr� classe
  (*
    Codigo   := QrVSMovSrcCodigo.Value;
    MovimCod := QrVSMovSrcMovimCod.Value;
    MovimID  := Integer(emidPreReclasse); // 15
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    CO_DATA_HORA_VMI], [
    'Codigo', 'MovimCod', 'MovimID'], [DataHora], [
    Codigo, MovimCod, MovimID], True);
  *)

    // Itens classe
    Codigo   := QrVSPaMulCabCodigo.Value;
    MovimCod := QrVSPaMulCabMovimCod.Value;
    MovimID  := Integer(emidClassArtXXMul);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    CO_DATA_HORA_VMI], [
    'Codigo', 'MovimCod', 'MovimID'], [DataHora], [
    Codigo, MovimCod, MovimID], True) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspamulcaba', False, [
      CO_DATA_HORA_GRL, 'SerieRem', 'NFeRem'], [
      'Codigo'], [
      DataHora, SerieRem, NFeRem], [
      Codigo], True) then
      begin
        LocCod(Codigo, Codigo);
        //
        if EdNFeRem.ValueVariant <> 0 then
          AtualizaNFeItens();
        //
        LocCod(Codigo, Codigo);
      end;
    end;
  end;
end;
procedure TFmVSPaMulCab.BtClassesGeradasClick(Sender: TObject);
begin
  VS_PF.ImprimeClassIMEI(QrVSMovSrcControle.Value, QrVSPaMulCabVSGerArt.Value,
  QrVSPaMulCabMovimCod.Value);
end;

procedure TFmVSPaMulCab.BtConfirmaClick(Sender: TObject);
var
  DataHora, DtHrCfgCla: String;
  VSGerArt, CacCod, Codigo, MovimCod, Empresa, Terceiro, VMI_Sorc, SerieRem,
  NFeRem: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
  SQLType: TSQLType;
begin
  SerieRem       := EdSerieRem.ValueVariant;
  NFeRem         := EdNFeRem.ValueVariant;
  VSGerArt       := QrVSGerArtNewCodigo.Value;
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' +
                    Geral.FDT(EdDataHora.ValueVariant, 100);
  VMI_Sorc       := EdIMEI.ValueVariant;
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if not VS_PF.ValidaCampoNF(3, EdNFeRem, True) then Exit;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(VMI_Sorc = 0, EdIMEI, 'Defina o IME-I de origem!') then Exit;
  if MyObjects.FIC(TPDataHora.DateTime < 2, TPDataHora, 'Defina a data!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vspamulcaba', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if SQLType = stIns then
  begin
    CacCod := UMyMod.BPGS1I32('vscaccab', 'Codigo', '', '', tsPos, SQLType, CacCod);
    VS_PF.InsereVSCacCab(CacCod, emidClassArtXXMul, Codigo);
  end else
    CacCod := QrVSPaMulCabCacCod.Value;
  //
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vspamulcaba', False, [
  'MovimCod', 'Empresa', CO_DATA_HORA_GRL,
  'VMI_Sorc', 'CacCod'
  (*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT',*),
  'VSGerArt', 'SerieRem', 'NFeRem'], [
  'Codigo'], [
  MovimCod, Empresa, DataHora,
  VMI_Sorc, CacCod
  (*, Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*),
  VSGerArt, SerieRem, NFeRem], [
  Codigo], True) then
  begin
    if SQLType = stIns then
    begin
      VS_PF.InsereVSMovCab(MovimCod, emidClassArtXXMul, Codigo);
      // 2015-07-10
      VSGerArt := QrVSGerArtNewCodigo.Value;
      DtHrCfgCla := DataHora;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
      'DtHrCfgCla', 'CacCod'
      ], ['Codigo'], [
      DtHrCfgCla, CacCod
      ], [VSGerArt], True);
      // Fim 2015-07-10
    end else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', CO_DATA_HORA_VMI], ['MovimCod'], [
      Empresa, DataHora], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSPaMulCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vspamulcaba', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vspamulcaba', 'Codigo');
end;

procedure TFmVSPaMulCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSPaMulCab.AtualizaNFeItens();
var
  SrcNivel2, NFeSer, NFeNum, VSMulNFeCab: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    NFeSer      := QrVSPaMulCabSerieRem.Value;
    NFeNum      := QrVSPaMulCabNFeRem.Value;
    VSMulNFeCab := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SrcNivel2 ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE MovimCod=' + Geral.FF0(QrVSMovSrcMovimCod.Value),
    'AND MovimID=' + Geral.FF0(Integer(emidClassArtXXMul)),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcClass)),
    '']);
    SrcNivel2 := Qry.FieldByName('SrcNivel2').AsInteger;
    if SrcNivel2 <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT NFeSer, NFeNum, VSMulNFeCab ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(SrcNivel2),
      '']);
      if (NFeSer = 0) and (NFeNum = 0) then
      begin
        NFeSer      := Qry.FieldByName('NFeSer').AsInteger;
        NFeNum      := Qry.FieldByName('NFeNum').AsInteger;
      end;
      VSMulNFeCab := Qry.FieldByName('VSMulNFeCab').AsInteger;
    end;
    VS_PF.AtualizaSerieNFeVMC(QrVSPaMulCabMovimCod.Value, NFeSer, NFeNum,
      VSMulNFeCab);
    VS_CRC_PF.AtualizaSerieNFeVMI(QrVSPaMulCabVMI_Sorc.Value, NFeSer, NFeNum,
      VSMulNFeCab);
    //
    QrVSPaMulIts.First;
    while not QrVSPaMulIts.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE SrcNivel2=' + Geral.FF0(QrVSPaMulItsControle.Value),
      '']);
      Qry.First;
      while not Qry.Eof do
      begin
        //if (Qry.FieldByName('NFeNum').AsInteger = 0)
        //or (Qry.FieldByName('VSMulNFeCab').AsInteger = 0) then
          VS_PF.AtualizaSerieNFeVMC(QrVSPaMulCabMovimCod.Value, NFeSer,
          NFeNum, VSMulNFeCab);
        //
        Qry.Next;
      end;
      //
      QrVSPaMulIts.Next;
    end;
  finally
    Qry.free;
  end;
end;

procedure TFmVSPaMulCab.Atualizavalordeestoque1Click(Sender: TObject);
var
  AreaTot, AreaAtu, ValrTot, ValrAtu, ValorT: Double;
  Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    AreaTot := QrVSMovSrcAreaM2.Value;
    ValrTot := QrVSMovSrcValorT.Value;
    //
    QrVSPaMulIts.First;
    while not QrVSPaMulIts.Eof do
    begin
      AreaAtu := QrVSPaMulItsAreaM2.Value;
      if ValrTot > 0 then
        ValrAtu := AreaAtu / AreaTot * ValrTot
      else
        ValrAtu := 0;
      //
      ValorT := ValrAtu;
      Controle := QrVSPaMulItsControle.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'ValorT'], ['Controle'], [
      ValorT], [Controle], True) then
      ;
      //
      QrVSPaMulIts.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSPaMulCab.Baixaresidual1Click(Sender: TObject);
begin
  ZeraEstoquePalletOrigem();
end;

procedure TFmVSPaMulCab.BtCabClick(Sender: TObject);
begin
  VS_PF.HabilitaMenuInsOuAllVSAberto(QrVSPaMulCab, QrVSPaMulCabDataHora.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSPaMulCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmVSPaMulCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSPaMulCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSPaMulCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSPaMulCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSPaMulCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSPaMulCabCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSPaMulCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSPaMulCab.QrVSPaMulCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSPaMulCab.QrVSPaMulCabAfterScroll(DataSet: TDataSet);
const
  Campo = 'Controle';
  CtrlLoc = 0;
var
  IMEI, TemIMEIMrt: Integer;
begin
  IMEI := QrVSPaMulCabVMI_Sorc.Value;
  TemIMEIMrt := QrVSPaMulCabTemIMEIMrt.Value;
  //
  ReopenVSPaMulIts(0);
  VS_PF.ReopenVSMovXXX(QrVSMovSrc, Campo, IMEI, TemIMEIMrt, CtrlLoc);
end;

procedure TFmVSPaMulCab.FichasCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(True);
end;

procedure TFmVSPaMulCab.FichasSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(False);
end;

procedure TFmVSPaMulCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSPaMulCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSPaMulCab.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSPaMulCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vspamulcaba', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSPaMulCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPaMulCab.frxWET_RECUR_010_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmVSPaMulCab.CabInclui1Click(Sender: TObject);
const
  Ficha = 0;
  Controle = 0;
  SoNaoZerados = True;
var
  Agora: TDateTime;
begin
  EdIMEI.ValueVariant := 0;
  CBIMEI.KeyValue := Null;
  //
  VS_PF.ReopenVSGerArtDst_ToClassPorFicha(QrVSGerArtNew, Ficha, Controle, SoNaoZerados);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSPaMulCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vspamulcaba');
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDataHora.Date := Agora;
  EdDataHora.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDataHora.SetFocus;
end;

procedure TFmVSPaMulCab.ClassesGeradas1Click(Sender: TObject);
begin
  VS_PF.ImprimeClassIMEI(QrVSMovSrcControle.Value, QrVSPaMulCabVSGerArt.Value,
  QrVSPaMulCabMovimCod.Value);
end;

procedure TFmVSPaMulCab.ColoreArea();
var
  Pecas, AreaM2: Double;
begin
  Pecas  := Geral.DMV(DBEdSdoVrtPeca.Text);
  AreaM2 := Geral.DMV(DBEdSdoVrtArM2.Text);
  //
  if (Pecas = 0) and (AreaM2 <> 0) then
  begin
    DBEdSdoVrtArM2.Font.Color := clRed;
    DBEdSdoVrtArM2.Font.Style := DBEdSdoVrtArM2.Font.Style + [fsBold];
  end else
  begin
    DBEdSdoVrtArM2.Font.Color := clWindowText;
    DBEdSdoVrtArM2.Font.Style := DBEdSdoVrtArM2.Font.Style - [fsBold];
  end;
end;

procedure TFmVSPaMulCab.QrVSPaMulCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSPaMulIts.Close;
  QrVSMovSrc.Close;
end;

procedure TFmVSPaMulCab.QrVSPaMulCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSPaMulCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

