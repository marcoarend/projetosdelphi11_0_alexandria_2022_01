unit VSMovItbAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditDateTimePicker,
  mySQLDbTables, dmkEdit, UnProjGroup_Consts;

type
  TFmVSMovItbAdd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPesquisa: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    QrIMEIs: TmySQLQuery;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    TPDataLimite: TdmkEditDateTimePicker;
    SpeedButton1: TSpeedButton;
    EdIMEILimite: TdmkEdit;
    Label2: TLabel;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    EdIMEIsCopiados: TdmkEdit;
    Label3: TLabel;
    EdCaCsCopiados: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdIMEIsExcluidos: TdmkEdit;
    EdCaCsExcluidos: TdmkEdit;
    Label7: TLabel;
    EdIMEIsPesquisados: TdmkEdit;
    EdCaCsPesquisados: TdmkEdit;
    BtExecuta: TBitBtn;
    QrCaCs: TmySQLQuery;
    BtAtualiza: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure TPDataLimiteChange(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtExecutaClick(Sender: TObject);
    procedure BtAtualizaClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaTemVSMorCab();
  public
    { Public declarations }
  end;

  var
  FmVSMovItbAdd: TFmVSMovItbAdd;

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, DmkDAC_PF, UMySQLModule,
  UnVS_PF, ModVS;

{$R *.DFM}

procedure TFmVSMovItbAdd.AtualizaTemVSMorCab();
begin
  VS_PF.AtualizaTemIMEIMrt1('vsajscab', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vsbxacab', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vsdvlcab', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vsexbcab', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vsgerarta', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vsgerrcla', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vsinncab', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vsmovdif', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vsopecab', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vsoutcab', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vsoutnfi', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vspaclacaba', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vspaclaitsa', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vspalleta', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vspamulcaba', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vspamulcabr', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vspamulitsa', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vspamulitsr', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vsparclbxaa', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vsparclcaba', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vsparclitsa', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vsplccab', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vsprepalcab', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vspwecab', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('vsrclcab', LaAviso1, LaAviso2);  Nao usa para nada???
  VS_PF.AtualizaTemIMEIMrt1('vsrtbcab', LaAviso1, LaAviso2);
  VS_PF.AtualizaTemIMEIMrt1('vssubprdcab', LaAviso1, LaAviso2);
  //VS_PF.AtualizaTemIMEIMrt1('', LaAviso1, LaAviso2);
  //
  VS_PF.AtualizaTemIMEIMrt1('vsmovcab', LaAviso1, LaAviso2, 'Codigo');
  //VS_PF.AtualizaTemIMEIMrt1('vscaccab', LaAviso1, LaAviso2); ??? Como fazer???

(*
vsajscab
vsbxacab
vsdvlcab
vsexbcab
vsgerarta
vsgerrcla
vsinncab
vsopecab
vsoutcab
vspamulcaba
vspamulcabr
vsplccab
vspwecab
vsrtbcab
vssubprdcab

SELECT 1  Tipo, Codigo FROM vsajscab WHERE TemIMEIMrt = 1 UNION
SELECT 2  Tipo, Codigo FROM vsbxacab WHERE TemIMEIMrt = 1 UNION
SELECT 3  Tipo, Codigo FROM vsdvlcab WHERE TemIMEIMrt = 1 UNION
SELECT 4  Tipo, Codigo FROM vsexbcab WHERE TemIMEIMrt = 1 UNION
SELECT 5  Tipo, Codigo FROM vsgerarta WHERE TemIMEIMrt = 1 UNION
SELECT 6  Tipo, Codigo FROM vsgerrcla WHERE TemIMEIMrt = 1 UNION
SELECT 7  Tipo, Codigo FROM vsinncab WHERE TemIMEIMrt = 1 UNION
SELECT 8  Tipo, Codigo FROM vsopecab WHERE TemIMEIMrt = 1 UNION
SELECT 9  Tipo, Codigo FROM vsoutcab WHERE TemIMEIMrt = 1 UNION
SELECT 10  Tipo, Codigo FROM vspamulcaba WHERE TemIMEIMrt = 1 UNION
SELECT 11  Tipo, Codigo FROM vspamulcabr WHERE TemIMEIMrt = 1 UNION
SELECT 12  Tipo, Codigo FROM vsplccab WHERE TemIMEIMrt = 1 UNION
SELECT 13  Tipo, Codigo FROM vspwecab WHERE TemIMEIMrt = 1 UNION
SELECT 14  Tipo, Codigo FROM vsrtbcab WHERE TemIMEIMrt = 1 UNION
SELECT 15  Tipo, Codigo FROM vssubprdcab WHERE TemIMEIMrt = 1 UNION
SELECT 16  Tipo, Codigo FROM vsmovcab WHERE TemIMEIMrt = 1

vsbxacab
vsdvlcab
vsexbcab
vsgerarta
vsgerrcla
vsinncab
vsopecab
vsoutcab
vspamulcaba
vspamulcabr
vsplccab
vspwecab
vsrtbcab
vssubprdcab
*)

end;

procedure TFmVSMovItbAdd.BtAtualizaClick(Sender: TObject);
begin
  AtualizaTemVSMorCab();
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSMovItbAdd.BtExecutaClick(Sender: TObject);
var
  DtaLimite, IMEILimite, TabOrig, TabDest, Campos: String;
  VSMorCab, QtdReg1: Integer;
var
  Nome, DataHora: String;
  Codigo: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  Codigo         := 0;
  Nome           := '';
  DataHora       := Geral.FDT(DModG.ObtemAgora(), 109);
  DtaLimite      := Geral.FDT(TPDataLimite.Date + 1, 1);
  IMEILimite     :=  Geral.FF0(EdIMEILimite.ValueVariant);
  //
  Codigo := UMyMod.BPGS1I32('vsmorcab', 'Codigo', '', '', tsPos, stIns, 0);
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmorcab', False, [
  'Nome', CO_DATA_HORA_GRL, 'DtaLimite',
  'IMEILimite'], [
  'Codigo'], [
  Nome, DataHora, DtaLimite,
  IMEILimite], [
  Codigo], True) then
    Exit;
  //
  VSMorCab := Codigo;
  ///
  //////////////////////////////////////////////////////////////////////////////
  ///  IMEIS >> VS Mov Its para VS Mov Itb
  //////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Transferindo dados para arquivo morto: IME-Is');
  TabOrig := CO_DEL_TAB_VMI;
  TabDest := 'vsmovitb';

  Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabDest, '', QtdReg1);
  Campos := Geral.Substitui(Campos,
    ', VSMorCab', ', ' + FormatFloat('0', VSMorCab) + ' VSMorCab');
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'INSERT INTO ' + TabDest,
  'SELECT ' + Campos,
  'FROM ' + TabOrig,
  'WHERE Controle IN (',
  '  SELECT Controle ',
  '  FROM ' + VAR_MyPID_DB_NOME + '. ' + CO_TAB_VM_MOV + '  ',
  ') ',
  'ON DUPLICATE KEY UPDATE VSMorCab=' + Geral.FF0(VSMorCab),
  '']);
  EdIMEIsCopiados.ValueVariant := Dmod.QrUpd.RowsAffected;
  ///
  //////////////////////////////////////////////////////////////////////////////
  ///  CaCs >> VSCaCItsA para VSCaCItsB
  //////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Transferindo dados para arquivo morto: CaCs');
  TabOrig := 'vscacitsa';
  TabDest := 'vscacitsb';
  //VSMorCab := definido acima
  Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabDest, '', QtdReg1);
  Campos := Geral.Substitui(Campos,
    ', VSMorCab', ', ' + FormatFloat('0', VSMorCab) + ' VSMorCab');
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'INSERT INTO ' + TabDest,
  'SELECT ' + Campos,
  'FROM ' + TabOrig,
  'WHERE VSPallet > 0  ',
  'AND (VSPallet IN ( ',
  '  SELECT Pallet ',
  '  FROM ' + VAR_MyPID_DB_NOME + '. ' + CO_TAB_VM_MOV + '  ',
  '  ) ',
  ') ',
  'ON DUPLICATE KEY UPDATE VSMorCab=' + Geral.FF0(VSMorCab),
  '']);
  EdCaCsCopiados.ValueVariant := Dmod.QrUpd.RowsAffected;
  //
  //
  ///
  //////////////////////////////////////////////////////////////////////////////
  ///  IMEIS >> Excluir dados do VS Mov Its movidos para VS Mov Itb
  //////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpamndo dados do arquivo ativo: IME-Is');
  TabOrig := CO_DEL_TAB_VMI;
  TabDest := 'vsmovitb';
  //VSMorCab := definido acima
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DELETE FROM ' + TabOrig,
  'WHERE Controle IN (',
  '  SELECT Controle ',
  '  FROM ' + TabDest,
  '  WHERE VSMorCab=' + Geral.FF0(VSMorCab),
  ') ',
  '']);
  EdIMEIsExcluidos.ValueVariant := Dmod.QrUpd.RowsAffected;
  ///
  //////////////////////////////////////////////////////////////////////////////
  ///  CaCs >> Excluir dados do VSCaCItsA movidos para VSCaCItsB
  //////////////////////////////////////////////////////////////////////////////
  ///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpamndo dados do arquivo ativo: CaCs');
  TabOrig := 'vscacitsa';
  TabDest := 'vscacitsb';
  //VSMorCab := definido acima
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DELETE FROM ' + TabOrig,
  'WHERE Controle IN (',
  '  SELECT Controle ',
  '  FROM ' + TabDest,
  '  WHERE VSMorCab=' + Geral.FF0(VSMorCab),
  ') ',
  '']);
  EdCaCsExcluidos.ValueVariant := Dmod.QrUpd.RowsAffected;
  //
  AtualizaTemVSMorCab();
(*
vsajscab
vsbxacab
vsdvlcab
vsexbcab
vsgerarta
vsgerrcla
vsinncab
vsmovdif
vsopecab
vsoutcab
vsoutnfi
vspaclacaba
vspaclaitsa
vspalleta
vspamulcaba
vspamulcabr
vspamulitsa
vspamulitsr
vsparclbxaa
vsparclcaba
vsparclitsa
vsplccab
vsprepalcab
vspwecab
vsrtbcab
vssubprdcab


????
vscgicab
vscgiits
vsdsnits
vspedcab
vspedits
vseqzits
vsrclcab
*)
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  BtPesquisa.Enabled := False;
  BtExecuta.Enabled := False;
end;

procedure TFmVSMovItbAdd.BtPesquisaClick(Sender: TObject);
const
  GraGruX = 0;
  Empresa = -11;
  TemIMEiMrt = 0;
  MargemErro = 0.01;
  Avisa = True;
  ForcaMostrarForm = True;
  SelfCall = False;
var
  DtaLimite, IMEILimite: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando inconsistencias que impedem o arquivamento');
  if DmModVS.QtdPosNegSameReg(TemIMEIMrt, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
  if DmModVS.RedConfltGerA(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2) then Exit;
  if DmModVS.RedConfltBaixa(ForcaMostrarForm, SelfCall, False, LaAviso1, LaAviso2) then Exit;
  if DmModVS.DstGGXDiferentesDeGraGruX(LaAviso1, LaAviso2) then Exit;
  if DmModVS.SrcGGXDiferentesDePreClasseParaClasseEReclasse(False, False, False,
    LaAviso1, LaAviso2) then Exit;
  if DmModVS.CustosDiferentesOrigemParaDestino2(Empresa, GraGruX, MargemErro,
    False, False, False, TemIMEiMrt, LaAviso1, LaAviso2) then Exit;
  if VS_PF.RegistrosComProblema(Empresa, GraGruX, LaAviso1, LaAviso2) then Exit;
  if DmModVS.CustosDifPreRecl(Empresa, GraGruX, MargemErro,
    Avisa, ForcaMostrarForm, SelfCall, TemIMEiMrt, nil, nil) then Exit;
  if DmModVS.VlrPosNegSameReg(TemIMEIMrt, Avisa,
    ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
  if DmModVS.CustosDifReclasse(Empresa, GraGruX, MargemErro,
    Avisa, ForcaMostrarForm, SelfCall, TemIMEiMrt, nil, nil) then Exit;
  if DmModVS.CustosDiferentesSaldosVirtuais3(Empresa, GraGruX, TemIMEIMrt,
     0.0005, Avisa, ForcaMostrarForm, SelfCall, LaAviso1, LaAviso2) then Exit;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando / pesquisando');
  //
  DtaLimite  := Geral.FDT(TPDataLimite.Date + 1, 1);
  IMEILimite :=  Geral.FF0(EdIMEILimite.ValueVariant);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIS, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS  ' + CO_TAB_VM_MOV + ' ; ',
  'CREATE TABLE  ' + CO_TAB_VM_MOV + '  ',
  'SELECT Controle, Pallet ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  'WHERE ',
  '  (Controle <= ' + IMEILimite + ') ',
  'AND ',
  //'  (SdoVrtPeca <= 0) ', N�o mandar saldo negativo para o arquivo morto!!!
  '  (SdoVrtPeca = 0) ',
  'AND ',
  '  (DataHora < "' + DtaLimite + '") ',
  'AND ',
  '  (NOT Pallet IN ',
  '    ( ',
  '      SELECT Pallet ',
  '      FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  '      WHERE ( ',
  '        SdoVrtPeca > 0 ',
  '        OR ',
  '        DataHora >= "' + DtaLimite + '" ',
  '      ) ',
  '      AND Pallet <> 0 ',
  '    ) ',
  '  ) ',
  '; ',
  'SELECT * FROM  ' + CO_TAB_VM_MOV + ' ; ',
  '']);
  EdIMEIsPesquisados.ValueVariant := QrIMEIS.RecordCount;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCaCS, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  'WHERE VSPallet > 0  ',
  'AND (VSPallet IN ( ',
  '  SELECT Pallet ',
  '  FROM ' + VAR_MyPID_DB_NOME + '. ' + CO_TAB_VM_MOV + '  ',
  '  ) ',
  ') ',
  '']);
  EdCaCsPesquisados.ValueVariant := QrCaCs.RecordCount;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  BtExecuta.Enabled := QrIMEIs.RecordCount > 0;
end;

procedure TFmVSMovItbAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMovItbAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMovItbAdd.FormCreate(Sender: TObject);
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
begin
  ImgTipo.SQLType := stLok;
  //
  Data := DmodG.ObtemAgora();
  Data := IncMonth(Data, - 5);
  TPDataLimite.Date := Geral.UltimoDiaDoMes(Data);
end;

procedure TFmVSMovItbAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMovItbAdd.SpeedButton1Click(Sender: TObject);
begin
  if DBCheck.LiberaPelaSenhaAdmin() then
  begin
    TPDataLimite.Enabled := True;
    TPDataLimite.SetFocus;
  end;
end;

procedure TFmVSMovItbAdd.TPDataLimiteChange(Sender: TObject);
var
  Qry: TmySQLQuery;
  Data: String;
begin
  Data := Geral.FDT(TPDataLimite.Date + 1, 1);
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Controle) Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE DataHora <= "' + Data + '" ',
    '']);
    //
    EdIMEILimite.ValueVariant := Qry.FieldByName('Controle').AsInteger;
  finally
    Qry.Free;
  end;
end;

end.
