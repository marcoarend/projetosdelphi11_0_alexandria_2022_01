object DfVSMod: TDfVSMod
  Left = 0
  Top = 0
  Caption = 'Modulo VS'
  ClientHeight = 519
  ClientWidth = 925
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 925
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 877
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 829
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 133
        Height = 32
        Caption = 'Form Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 133
        Height = 32
        Caption = 'Form Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 133
        Height = 32
        Caption = 'Form Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 392
    Width = 925
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 921
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 921
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 449
    Width = 925
    Height = 70
    Align = alBottom
    TabOrder = 2
    Visible = False
    object PnSaiDesis: TPanel
      Left = 779
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 777
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 925
    Height = 344
    Align = alClient
    TabOrder = 3
  end
  object QrVsiDest: TMySQLQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 60
    object QrVsiDestMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVsiDestMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVsiDestCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVsiDestControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVsiDestGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVsiDestMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVsiDestEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVsiDestTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
  end
  object QrVsiSorc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 108
    object QrVsiSorcGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVsiSorcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVsiSorcValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVsiSorcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVsiSorcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVsiSorcFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVsiSorcSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVsiSorcMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVsiSorcMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object QrSorces: TMySQLQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 156
    object QrSorcesPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSorcesAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSorcesAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSorcesVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
  end
  object QrSumSorc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 204
    object QrSumSorcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumSorcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumSorcAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object QrVSPallet: TMySQLQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 256
    object QrVSPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrVMI_Sorc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT VMI_Sorc, SUM(Pecas) Pecas, SUM(AreaM2) AreaM2'
      'FROM vscacitsa'
      'WHERE VSPaClaIts=14'
      'GROUP BY VMI_Sorc')
    Left = 112
    Top = 60
    object QrVMI_SorcVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVMI_SorcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMI_SorcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object QrVMI_Baix: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT VMI_Baix, SUM(Pecas) Pecas, SUM(AreaM2) AreaM2'
      'FROM vscacitsa'
      'WHERE VSPaClaIts=14'
      'GROUP BY VMI_Baix')
    Left = 112
    Top = 108
    object QrVMI_BaixVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVMI_BaixPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMI_BaixAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object QrFI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FrmaIns, COUNT(FrmaIns) ITENS'
      'FROM vscacitsa'
      'WHERE CacCod=70'
      'GROUP BY FrmaIns'
      '')
    Left = 180
    Top = 60
    object QrFIFrmaIns: TSmallintField
      FieldName = 'FrmaIns'
    end
    object QrFIITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object QrOrfaos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '    SELECT *'
      '    FROM vscacitsa'
      '    WHERE CacCod=70'
      '    AND VSPaRclIts=21'
      '    AND VMI_Sorc=0'
      '    ORDER BY Controle')
    Left = 180
    Top = 204
  end
  object QrSrc0: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2,'
      'SUM(AreaP2) AreaP2 '
      'FROM vscacitsa'
      'WHERE CacCod=70'
      'AND VSPaRclIts=21')
    Left = 180
    Top = 156
  end
  object QrSrcVMI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 180
    Top = 108
    object QrSrcVMIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrcVMISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrSrcVMISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
  end
  object QrMulRecl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT CacCod, VSPaRclIts'
      'FROM vscacitsa'
      'WHERE VSPallet=247'
      'ORDER BY CacCod, VSPaRclIts')
    Left = 32
    Top = 308
    object QrMulReclCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMulReclCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrMulReclVSPaRclIts: TIntegerField
      FieldName = 'VSPaRclIts'
    end
    object QrMulReclVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrMulReclVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
  end
  object QrMulti: TMySQLQuery
    Database = Dmod.MyDB
    Left = 180
    Top = 256
    object QrMultiITENS: TFloatField
      FieldName = 'ITENS'
    end
  end
  object QrIDs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 184
    Top = 308
    object QrIDsCacID: TIntegerField
      FieldName = 'CacID'
    end
  end
  object QrBoxesPal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT DISTINCT prc.Codigo, prc.CacCod, pri.Controle VSPaRclIts,' +
        ' '
      '0.000 VSPallet, pri.VMI_Dest, 7.000 CacID '
      'FROM vspaclaitsa pri  '
      'LEFT JOIN vspaclacaba prc ON prc.Codigo=pri.Codigo '
      'LEFT JOIN vscacitsa cia ON pri.Controle=cia.VSPaRclIts '
      'WHERE pri.VSPallet=?'
      ' '
      'UNION '
      ' '
      
        'SELECT DISTINCT prc.Codigo, prc.CacCod, pri.Controle VSPaRclIts,' +
        ' '
      'prc.VSPallet + 0.000 VSPallet, pri.VMI_Dest, 8.000 CacID '
      'FROM vsparclitsa pri  '
      'LEFT JOIN vsparclcaba prc ON prc.Codigo=pri.Codigo '
      'LEFT JOIN vscacitsa cia ON pri.Controle=cia.VSPaRclIts '
      'WHERE pri.VSPallet=?')
    Left = 256
    Top = 60
    object QrBoxesPalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBoxesPalCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrBoxesPalVSPaXXXIts: TIntegerField
      FieldName = 'VSPaXXXIts'
      Required = True
    end
    object QrBoxesPalVSPallet: TFloatField
      FieldName = 'VSPallet'
    end
    object QrBoxesPalVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
      Required = True
    end
    object QrBoxesPalCacID: TFloatField
      FieldName = 'CacID'
      Required = True
    end
    object QrBoxesPalVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrBoxesPalVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrBoxesPalTecla: TIntegerField
      FieldName = 'Tecla'
    end
  end
  object QrSumDest1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 256
    Top = 108
  end
  object QrSumSorc1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 256
    Top = 156
  end
  object QrVMISorc1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 256
    Top = 204
  end
  object QrPalSorc1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 256
    Top = 252
  end
  object QrVSXxxCab: TMySQLQuery
    Database = Dmod.MyDB
    Left = 352
    Top = 188
    object QrVSXxxCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSXxxCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object QrAsce: TMySQLQuery
    Database = Dmod.MyDB
    Left = 424
    Top = 60
    object QrAsceAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrAsceValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object QrDesc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 424
    Top = 104
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 243
  end
end
