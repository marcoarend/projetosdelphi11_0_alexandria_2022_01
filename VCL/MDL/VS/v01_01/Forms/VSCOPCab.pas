unit VSCOPCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, UnProjGroup_Vars,
  AppListas;

type
  TFmVSCOPCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrVSCOPCab: TmySQLQuery;
    DsVSCOPCab: TDataSource;
    QrVSCOPIts: TmySQLQuery;
    DsVSCOPIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrVSCOPCabNO_MovimID: TWideStringField;
    QrVSCOPCabNO_Operacoes: TWideStringField;
    QrVSCOPCabCodigo: TIntegerField;
    QrVSCOPCabNome: TWideStringField;
    QrVSCOPCabMovimID: TIntegerField;
    QrVSCOPCabEmpresa: TIntegerField;
    QrVSCOPCabGraGruX: TIntegerField;
    QrVSCOPCabGGXDst: TIntegerField;
    QrVSCOPCabClienteMO: TIntegerField;
    QrVSCOPCabTipoArea: TSmallintField;
    QrVSCOPCabForneceMO: TIntegerField;
    QrVSCOPCabStqCenLoc: TIntegerField;
    QrVSCOPCabPedItsLib: TIntegerField;
    QrVSCOPCabOperacoes: TIntegerField;
    QrVSCOPCabLk: TIntegerField;
    QrVSCOPCabDataCad: TDateField;
    QrVSCOPCabDataAlt: TDateField;
    QrVSCOPCabUserCad: TIntegerField;
    QrVSCOPCabUserAlt: TIntegerField;
    QrVSCOPCabAlterWeb: TSmallintField;
    QrVSCOPCabAtivo: TSmallintField;
    QrVSCOPCabNO_PRD_TAM_COR_INN: TWideStringField;
    QrVSCOPCabNO_PRD_TAM_COR_DST: TWideStringField;
    QrVSCOPCabNO_Empresa: TWideStringField;
    QrVSCOPCabNO_CLIENTEMO: TWideStringField;
    QrVSCOPCabNO_FORNECEMO: TWideStringField;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    RGTipoArea: TdmkRadioGroup;
    DsGraGruX: TDataSource;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrGGXDst: TmySQLQuery;
    QrGGXDstGraGru1: TIntegerField;
    QrGGXDstControle: TIntegerField;
    QrGGXDstNO_PRD_TAM_COR: TWideStringField;
    QrGGXDstSIGLAUNIDMED: TWideStringField;
    QrGGXDstCODUSUUNIDMED: TIntegerField;
    QrGGXDstNOMEUNIDMED: TWideStringField;
    DsGGXDst: TDataSource;
    Label3: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGGXDst: TdmkEditCB;
    Label54: TLabel;
    CBGGXDst: TdmkDBLookupComboBox;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    QrOperacoes: TmySQLQuery;
    DsOperacoes: TDataSource;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    Label35: TLabel;
    EdForneceMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label55: TLabel;
    EdOperacoes: TdmkEditCB;
    CBOperacoes: TdmkDBLookupComboBox;
    EdPedItsLib: TdmkEditCB;
    LaPedItsLib: TLabel;
    CBPedItsLib: TdmkDBLookupComboBox;
    EdClienteMO: TdmkEditCB;
    Label62: TLabel;
    CBClienteMO: TdmkDBLookupComboBox;
    QrVSCOPItsGraGru1: TIntegerField;
    QrVSCOPItsControle: TIntegerField;
    QrVSCOPItsNO_PRD_TAM_COR: TWideStringField;
    QrVSCOPItsSIGLAUNIDMED: TWideStringField;
    QrVSCOPItsCODUSUUNIDMED: TIntegerField;
    QrVSCOPItsNOMEUNIDMED: TWideStringField;
    QrVSCOPItsCodigo: TIntegerField;
    QrVSCOPItsGraGruX: TIntegerField;
    QrVSCOPItsLk: TIntegerField;
    QrVSCOPItsDataCad: TDateField;
    QrVSCOPItsDataAlt: TDateField;
    QrVSCOPItsUserCad: TIntegerField;
    QrVSCOPItsUserAlt: TIntegerField;
    QrVSCOPItsAlterWeb: TSmallintField;
    QrVSCOPItsAtivo: TSmallintField;
    EdMovimID: TdmkEditCB;
    Label4: TLabel;
    CBMovimID: TdmkDBLookupComboBox;
    QrVSMovimID: TmySQLQuery;
    DsVSMovimID: TDataSource;
    QrVSMovimIDCodigo: TIntegerField;
    QrVSMovimIDNome: TWideStringField;
    QrOperacoesCodigo: TIntegerField;
    QrOperacoesNome: TWideStringField;
    QrVSPedIts: TmySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    DsVSPedIts: TDataSource;
    Label10: TLabel;
    dmkEditCB1: TdmkEditCB;
    dmkDBLookupComboBox1: TdmkDBLookupComboBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    QrVSCOPCabNO_LOC_CEN: TWideStringField;
    DBEdit17: TDBEdit;
    QrVSCOPCabNO_PRD_TAM_COR_PED: TWideStringField;
    DBEdit18: TDBEdit;
    QrVSCOPCabVSPMOCab: TIntegerField;
    QrVSCOPCabNO_VSPMOCab: TWideStringField;
    EdVSPMOCab: TdmkEditCB;
    Label5: TLabel;
    CBVSPMOCab: TdmkDBLookupComboBox;
    QrVSPMOCab: TmySQLQuery;
    DsVSPMOCab: TDataSource;
    QrVSPMOCabCodigo: TIntegerField;
    QrVSPMOCabNome: TWideStringField;
    Label18: TLabel;
    DBEdit20: TDBEdit;
    DBEdit19: TDBEdit;
    QrVSArtCab: TMySQLQuery;
    QrVSArtCabCodigo: TIntegerField;
    QrVSArtCabNome: TWideStringField;
    DsVSArtCab: TDataSource;
    Label66: TLabel;
    EdVSArtCab: TdmkEditCB;
    CBVSArtCab: TdmkDBLookupComboBox;
    Label6: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    QrVSCOPCabVSArtCab: TIntegerField;
    QrVSCOPCabNO_VSArtCab: TWideStringField;
    SbGraGruX: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SbVSArtCab: TSpeedButton;
    CkCpermitirCrust: TCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSCOPCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSCOPCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSCOPCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSCOPCabBeforeClose(DataSet: TDataSet);
    procedure SbGraGruXClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SbVSArtCabClick(Sender: TObject);
    procedure CkCpermitirCrustClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraVSCOPIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure ReopenVSCOPIts(Controle: Integer);
    procedure ReabreGGX();
    procedure ReopenGraGruX();
  end;

var
  FmVSCOPCab: TFmVSCOPCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSCOPIts, ModuleGeral,
  UnVS_CRC_PF, VSPMOCab, UnVS_PF, UnGrade_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSCOPCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSCOPCab.MostraVSCOPIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSCOPIts, FmVSCOPIts, afmoNegarComAviso) then
  begin
    FmVSCOPIts.ImgTipo.SQLType := SQLType;
    FmVSCOPIts.FQrCab := QrVSCOPCab;
    FmVSCOPIts.FDsCab := DsVSCOPCab;
    FmVSCOPIts.FQrIts := QrVSCOPIts;
    if SQLType = stIns then
      //
    else
    begin
      FmVSCOPIts.EdControle.ValueVariant := QrVSCOPItsControle.Value;
      //
      FmVSCOPIts.EdGraGruX.ValueVariant := QrVSCOPItsGraGruX.Value;
      FmVSCOPIts.CBGraGruX.KeyValue     := QrVSCOPItsGraGruX.Value;
    end;
    FmVSCOPIts.ShowModal;
    FmVSCOPIts.Destroy;
  end;
end;

procedure TFmVSCOPCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSCOPCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSCOPCab, QrVSCOPIts);
end;

procedure TFmVSCOPCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSCOPCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSCOPIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSCOPIts);
end;

procedure TFmVSCOPCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSCOPCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSCOPCab.DefParams;
begin
  VAR_GOTOTABELA := 'vscopcab';
  VAR_GOTOMYSQLTABLE := QrVSCOPCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ');
  VAR_SQLx.Add('mid.Nome NO_MovimID, pmo.Nome NO_VSPMOCab, ');
  VAR_SQLx.Add('ope.Nome NO_Operacoes, vcc.*,  ');
  VAR_SQLx.Add('CONCAT(gg1p.Nome,  ');
  VAR_SQLx.Add('IF(gtip.PrintTam=0, "", CONCAT(" ", gtip.Nome)),  ');
  VAR_SQLx.Add('IF(gccp.PrintCor=0,"", CONCAT(" ", gccp.Nome)))  ');
  VAR_SQLx.Add('NO_PRD_TAM_COR_PED,  ');
  VAR_SQLx.Add('CONCAT(gg1i.Nome,  ');
  VAR_SQLx.Add('IF(gtii.PrintTam=0, "", CONCAT(" ", gtii.Nome)),  ');
  VAR_SQLx.Add('IF(gcci.PrintCor=0,"", CONCAT(" ", gcci.Nome)))  ');
  VAR_SQLx.Add('NO_PRD_TAM_COR_INN,  ');
  VAR_SQLx.Add('CONCAT(gg1d.Nome,  ');
  VAR_SQLx.Add('IF(gtid.PrintTam=0, "", CONCAT(" ", gtid.Nome)),  ');
  VAR_SQLx.Add('IF(gccd.PrintCor=0,"", CONCAT(" ", gccd.Nome)))  ');
  VAR_SQLx.Add('NO_PRD_TAM_COR_DST,  ');
  VAR_SQLx.Add('vac.Nome NO_VSArtCab, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_Empresa,  ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,  ');
  VAR_SQLx.Add('IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FORNECEMO  ');
  VAR_SQLx.Add('  ');
  VAR_SQLx.Add('FROM vscopcab vcc  ');
  VAR_SQLx.Add('LEFT JOIN entidades  emp  ON emp.Codigo=vcc.Empresa  ');
  VAR_SQLx.Add('LEFT JOIN entidades  cli  ON cli.Codigo=vcc.ClienteMO  ');
  VAR_SQLx.Add('LEFT JOIN entidades  fmo  ON fmo.Codigo=vcc.ForneceMO  ');
  VAR_SQLx.Add('  ');
  VAR_SQLx.Add('LEFT JOIN vspedits   vpi  ON vcc.PedItsLib=vpi.Controle');
  VAR_SQLx.Add('LEFT JOIN gragrux    ggxp ON ggxp.Controle=vpi.GraGruX  ');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggcp ON ggcp.Controle=ggxp.GraGruC  ');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gccp ON gccp.Codigo=ggcp.GraCorCad  ');
  VAR_SQLx.Add('LEFT JOIN gratamits  gtip ON gtip.Controle=ggxp.GraTamI  ');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1p ON gg1p.Nivel1=ggxp.GraGru1  ');
  VAR_SQLx.Add('LEFT JOIN unidmed    unmp ON unmp.Codigo=gg1p.UnidMed  ');
  VAR_SQLx.Add('  ');
  VAR_SQLx.Add('LEFT JOIN gragrux    ggxi ON ggxi.Controle=vcc.GraGruX  ');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggci ON ggci.Controle=ggxi.GraGruC  ');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcci ON gcci.Codigo=ggci.GraCorCad  ');
  VAR_SQLx.Add('LEFT JOIN gratamits  gtii ON gtii.Controle=ggxi.GraTamI  ');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1i ON gg1i.Nivel1=ggxi.GraGru1  ');
  VAR_SQLx.Add('LEFT JOIN unidmed    unmi ON unmi.Codigo=gg1i.UnidMed  ');
  VAR_SQLx.Add('  ');
  VAR_SQLx.Add('LEFT JOIN gragrux    ggxd ON ggxd.Controle=vcc.GGXDst  ');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggcd ON ggcd.Controle=ggxd.GraGruC  ');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gccd ON gccd.Codigo=ggcd.GraCorCad  ');
  VAR_SQLx.Add('LEFT JOIN gratamits  gtid ON gtid.Controle=ggxd.GraTamI  ');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1d ON gg1d.Nivel1=ggxd.GraGru1  ');
  VAR_SQLx.Add('LEFT JOIN unidmed    unmd ON unmd.Codigo=gg1d.UnidMed  ');
  VAR_SQLx.Add('  ');
  VAR_SQLx.Add('LEFT JOIN stqcenloc  scl ON scl.Controle=vcc.StqCenLoc  ');
  VAR_SQLx.Add('LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo  ');
  VAR_SQLx.Add('LEFT JOIN operacoes  ope ON ope.Codigo=vcc.Operacoes  ');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('LEFT JOIN vsmovimid  mid ON vcc.MovimID=mid.Codigo');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('LEFT JOIN vspmocab  pmo ON vcc.VSPMOCab=pmo.Codigo');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('LEFT JOIN vsartcab   vac ON vac.Codigo=vcc.VSArtCab');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE vcc.Codigo > 0');
  //
  VAR_SQL1.Add('AND vcc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vcc.Nome Like :P0');
  //
  //Geral.MB_SQL(Self, QrVSCOPCab);
end;

procedure TFmVSCOPCab.ItsAltera1Click(Sender: TObject);
begin
  MostraVSCOPIts(stUpd);
end;

procedure TFmVSCOPCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmVSCOPCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSCOPCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSCOPCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'VSCOPIts', 'Controle', QrVSCOPItsControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSCOPIts,
      QrVSCOPItsControle, QrVSCOPItsControle.Value);
    ReopenVSCOPIts(Controle);
  end;
}
end;

procedure TFmVSCOPCab.ReabreGGX;
begin
  if CkCpermitirCrust.Checked then
    VS_CRC_PF.AbreGraGruXY(QrGGXDst,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_6144_VSFinCla))
  else
    VS_CRC_PF.AbreGraGruXY(QrGGXDst,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_3072_VSRibCla));
end;

procedure TFmVSCOPCab.ReopenGraGruX();
begin
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_5120_VSWetEnd));
end;

procedure TFmVSCOPCab.ReopenVSCOPIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCOPIts, Dmod.MyDB, [
  'SELECT ggx.GraGru1, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,  ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, ',
  'vci.* ',
  'FROM vscopits vci ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vci.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE vci.Codigo=' + Geral.FF0(QrVSCOPCabCodigo.Value),
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle  ',
  '']);
  //
  QrVSCOPIts.Locate('Controle', Controle, []);
end;


procedure TFmVSCOPCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSCOPCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSCOPCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSCOPCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSCOPCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSCOPCab.SpeedButton5Click(Sender: TObject);
var
  GGYDst: Integer;
begin
  VAR_CADASTRO2 := 0;
  if CkCpermitirCrust.Checked then
    GGYDst := CO_GraGruY_6144_VSFinCla
  else
    GGYDst := CO_GraGruY_3072_VSRibCla;
  Grade_Jan.MostraFormGraGruY(GGYDst, 0, '');
  ReabreGGX();
  if VAR_CADASTRO2 <> 0 then
  begin
    EdGGXDst.ValueVariant := VAR_CADASTRO2;
    CBGGXDst.KeyValue     := VAR_CADASTRO2;
  end;
end;

procedure TFmVSCOPCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCOPCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSCOPCabCodigo.Value;
  Close;
end;

procedure TFmVSCOPCab.ItsInclui1Click(Sender: TObject);
begin
  MostraVSCOPIts(stIns);
end;

procedure TFmVSCOPCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSCOPCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vscopcab');
  DModG.SetaFilialdeEntidade(QrVSCOPCabEmpresa.Value, EdEmpresa, CBEmpresa);
end;

procedure TFmVSCOPCab.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Filial, Codigo, MovimID, Empresa, GraGruX, GGXDst, ClienteMO, TipoArea,
  ForneceMO, StqCenLoc, PedItsLib, Operacoes, VSPMOCab, VSArtCab: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  MovimID        := EdMovimID.ValueVariant;
  Filial         := EdEmpresa.ValueVariant;
  Empresa        := DModG.QrEmpresasCodigo.Value;
  GraGruX        := EdGraGruX.ValueVariant;
  GGXDst         := EdGGXDst.ValueVariant;
  ClienteMO      := EdClienteMO.ValueVariant;
  TipoArea       := RGTipoArea.ItemIndex;
  VSPMOCab       := EdVSPMOCab.ValueVariant;
  ForneceMO      := EdForneceMO.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  PedItsLib      := EdPedItsLib.ValueVariant;
  Operacoes      := EdOperacoes.ValueVariant;
  VSArtCab       := EdVSArtCab.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Defina a empresa!') then Exit;
  if MyObjects.FIC(MovimID = 0, EdEmpresa, 'Defina o ID do movimento!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vscopcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscopcab', False, [
  'Nome', 'MovimID', 'Empresa',
  'GraGruX', 'GGXDst', 'ClienteMO',
  'TipoArea', 'VSPMOCab',
  'ForneceMO', 'StqCenLoc', 'PedItsLib',
  'Operacoes', 'VSArtCab'], [
  'Codigo'], [
  Nome, MovimID, Empresa,
  GraGruX, GGXDst, ClienteMO,
  TipoArea, VSPMOCab,
  ForneceMO, StqCenLoc, PedItsLib,
  Operacoes, VSArtCab], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSCOPCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vscopcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vscopcab', 'Codigo');
end;

procedure TFmVSCOPCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSCOPCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSCOPCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  //UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  ReopenGraGruX();
  UnDmkDAC_PF.AbreQuery(QrGGXDst, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  UnDmkDAC_PF.AbreQuery(QrOperacoes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSMovimID, Dmod.MyDB);
  VS_CRC_PF.ReopenPedItsXXX(QrVSPedIts, 0, 0);
  UnDmkDAC_PF.AbreQuery(QrVSPMOCab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSArtCab, Dmod.MyDB);
end;

procedure TFmVSCOPCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSCOPCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSCOPCab.SbGraGruXClick(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  Grade_Jan.MostraFormGraGruY(CO_GraGruY_5120_VSWetEnd, 0, '');
  ReopenGraGruX();
  if VAR_CADASTRO2 <> 0 then
  begin
    EdGraGruX.ValueVariant := VAR_CADASTRO2;
    CBGraGruX.KeyValue     := VAR_CADASTRO2;
  end;
end;

procedure TFmVSCOPCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSCOPCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrVSCOPCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSCOPCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSCOPCab.QrVSCOPCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSCOPCab.QrVSCOPCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSCOPIts(0);
end;

procedure TFmVSCOPCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSCOPCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSCOPCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrVSCOPCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vscopcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSCOPCab.SbVSArtCabClick(Sender: TObject);
var
  Codigo, GraGruX: Integer;
begin
  Codigo  := EdVSArtCab.ValueVariant;
  GraGruX := 0;
  VAR_CADASTRO := 0;
  VS_PF.MostraFormVSArtCab(Codigo, GraGruX);
  UnDmkDAC_PF.AbreQuery(QrVSArtCab, Dmod.MyDB);
  if VAR_CADASTRO <> 0 then
  begin
    EdVSArtCab.ValueVariant := VAR_CADASTRO;
    CBVSArtCab.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmVSCOPCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCOPCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSCOPCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'vscopcab');
end;

procedure TFmVSCOPCab.CkCpermitirCrustClick(Sender: TObject);
begin
  ReabreGGX();
end;

procedure TFmVSCOPCab.QrVSCOPCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSCOPIts.Close;
end;

procedure TFmVSCOPCab.QrVSCOPCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSCOPCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

