unit VSMOEnvEVMI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditCalc;

type
  TFmVSMOEnvEVMI = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    Panel6: TPanel;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    EdPesoKg: TdmkEdit;
    LaPeso: TLabel;
    Label5: TLabel;
    EdControle: TdmkEdit;
    EdGraGruX: TdmkEdit;
    Label10: TLabel;
    EdNO_PRD_TAM_COR: TdmkEdit;
    Label1: TLabel;
    EdPallet: TdmkEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FTabVMIQtdEnvEnv: String;
  end;

  var
  FmVSMOEnvEVMI: TFmVSMOEnvEVMI;

implementation

uses UnMyObjects, UMySQLModule, ModuleGeral;

{$R *.DFM}

procedure TFmVSMOEnvEVMI.BtOKClick(Sender: TObject);
var
  NO_PRDA_TAM_COR: String;
  Controle, GraGruX: Integer;
  Pecas, PesoKg, AreaM2, AreaP2: Double;
  SQLType: TSQLType;
begin
  SQLType         := ImgTipo.SQLType;
  Controle        := EdControle.ValueVariant;
  GraGruX         := EdGraGruX.ValueVariant;
  Pecas           := EdPecas.ValueVariant;
  PesoKg          := EdPesoKg.ValueVariant;
  AreaM2          := EdAreaM2.ValueVariant;
  AreaP2          := EdAreaP2.ValueVariant;
  NO_PRDA_TAM_COR := EdNO_PRD_TAM_COR.Text;
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, SQLType, FTabVMIQtdEnvEnv, False, [
  'GraGruX', 'Pecas',
  'PesoKg', 'AreaM2', 'AreaP2',
  'NO_PRDA_TAM_COR'], [
  'Controle'], [
  GraGruX, Pecas,
  PesoKg, AreaM2, AreaP2,
  NO_PRDA_TAM_COR], [
  Controle], False) then
  begin
    Close;
  end;
end;

procedure TFmVSMOEnvEVMI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMOEnvEVMI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMOEnvEVMI.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSMOEnvEVMI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
