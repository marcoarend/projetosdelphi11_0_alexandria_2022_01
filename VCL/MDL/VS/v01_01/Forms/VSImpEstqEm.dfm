object FmVSImpEstqEm: TFmVSImpEstqEm
  Left = 0
  Top = 0
  Caption = 'WET-CURTI-129 :: Estoque VS Em...'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object QrOpePWE: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      '')
    Left = 56
    Top = 52
    object QrOpePWEPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrOpePWEAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrOpePWEPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrOpePWEMID: TIntegerField
      FieldName = 'MID'
    end
    object QrOpePWECod: TIntegerField
      FieldName = 'Cod'
    end
  end
  object mySQLQuery1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      '')
    Left = 236
    Top = 60
    object IntegerField1: TIntegerField
      FieldName = 'MovimID'
    end
    object IntegerField2: TIntegerField
      FieldName = 'MovimCod'
    end
    object FloatField1: TFloatField
      FieldName = 'Pecas'
    end
    object FloatField2: TFloatField
      FieldName = 'AreaM2'
    end
    object FloatField3: TFloatField
      FieldName = 'AreaP2'
    end
    object FloatField4: TFloatField
      FieldName = 'PesoKg'
    end
    object FloatField5: TFloatField
      FieldName = 'ValorT'
    end
  end
end
