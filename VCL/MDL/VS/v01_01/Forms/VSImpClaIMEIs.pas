unit VSImpClaIMEIs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  frxClass, frxDBSet, UnProjGroup_Consts, UnAppEnums, dmkDBLookupComboBox,
  dmkEditCB, ModProd;

type
  TFmVSImpClaIMEIs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrVSCacIts: TmySQLQuery;
    frxWET_CURTI_036_001: TfrxReport;
    frxDsVSCacIts: TfrxDBDataset;
    QrVSCacSum: TmySQLQuery;
    QrVSCacSumPecas: TFloatField;
    QrVSCacSumAreaM2: TFloatField;
    QrVSCacSumAreaP2: TFloatField;
    QrVSGerArtOld: TmySQLQuery;
    frxDSVSGerArtOld: TfrxDBDataset;
    QrVSGerArtOldFicha: TLargeintField;
    QrVSGerArtOldSerieFch: TLargeintField;
    QrVSGerArtOldGraGruX: TLargeintField;
    QrVSGerArtOldNotaMPAG: TFloatField;
    QrVSGerArtOldPecas: TFloatField;
    QrVSGerArtOldPesoKg: TFloatField;
    QrVSGerArtOldObserv: TWideStringField;
    QrVSGerArtOldNO_SerieFch: TWideStringField;
    QrVSGerArtOldNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtOldNO_PALLET: TWideStringField;
    QrVSGerArtOldNO_FORNECE: TWideStringField;
    QrVSCacItsPecas: TFloatField;
    QrVSCacItsAreaM2: TFloatField;
    QrVSCacItsAreaP2: TFloatField;
    QrVSCacItsNO_PRD_TAM_COR: TWideStringField;
    QrVSCacItsPercM2: TFloatField;
    QrVSCacItsPercPc: TFloatField;
    QrVSCacItsGraGruX: TLargeintField;
    QrVSCacItsGraGru1: TLargeintField;
    QrMCs: TmySQLQuery;
    QrMCsMovimCod: TLargeintField;
    QrVSCacItsPreco: TFloatField;
    QrVSCacItsValor: TFloatField;
    QrGraCusPrc: TMySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    DsGraCusPrc: TDataSource;
    Label61: TLabel;
    EdGraCusPrc: TdmkEditCB;
    CBGraCusPrc: TdmkDBLookupComboBox;
    QrGraGruVal: TMySQLQuery;
    QrGraGruValCustoPreco: TFloatField;
    frxWET_CURTI_036_002: TfrxReport;
    SbGraCusPrc: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_036_001GetValue(const VarName: string;
      var Value: Variant);
    procedure QrVSCacItsCalcFields(DataSet: TDataSet);
    procedure SbGraCusPrcClick(Sender: TObject);
  private
    { Private declarations }
    //
    function  ListaIMEIs(): String;
    function  ListaMovimCods(): String;
    procedure ReopenVSGerArtSrc(Qry: TmySQLQuery; EstqMovimNiv: TEstqMovimNiv);
    procedure ReopenVSCacIts();
    procedure ReopenVSCacSum();
  public
    { Public declarations }
    FIMEIs, FMortos: array of Integer;
    //FVSGerArt: array of Integer;
    //
    procedure ImprimeClassIMEI();
  end;

  var
  FmVSImpClaIMEIs: TFmVSImpClaIMEIs;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UnVS_CRC_PF, UMySQLModule,
  UnGrade_Jan;

{$R *.DFM}

const
  F_vs037_cia_its_ = '_vs037_cia_its_';

procedure TFmVSImpClaIMEIs.BtOKClick(Sender: TObject);
begin
   ImprimeClassIMEI();
end;

procedure TFmVSImpClaIMEIs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpClaIMEIs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpClaIMEIs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrGraCusPrc, Dmod.MyDB);
end;

procedure TFmVSImpClaIMEIs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpClaIMEIs.frxWET_CURTI_036_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_IMEI' then
    Value := Geral.Substitui(Geral.Substitui(ListaIMEIs(), #13, ' '), #10, '')
  else
end;

procedure TFmVSImpClaIMEIs.ImprimeClassIMEI();
var
  QtdM, GraCusPrc: Integer;
begin
  GraCusPrc := EdGraCusPrc.ValueVariant;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo artigos de origem');
  ReopenVSGerArtSrc(QrVSGerArtOld, eminSorcCurtiXX);
  // Deve ser antes
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo Somas de Classes');
  ReopenVSCacSum();
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo itens de Classes');
  ReopenVSCacIts();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando relatórios');
  MyObjects.frxDefineDataSets(frxWET_CURTI_036_001, [
  DModG.frxDsDono,
  frxDsVSCacIts,
  //frxDsVSGerArtNew,
  frxDsVSGerArtOld
  ]);
  if GraCusPrc <> 0 then
    MyObjects.frxMostra(frxWET_CURTI_036_002, 'Romaneio de Artigo de Ribeira Precificado')
  else
    MyObjects.frxMostra(frxWET_CURTI_036_001, 'Romaneio de Artigo de Ribeira');
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

function TFmVSImpClaIMEIs.ListaIMEIs(): String;
var
  I, N: Integer;
begin
  Result := '';
  N := High(FIMEIs);
  for I := Low(FIMEIs) to N do
  begin
    Result := Result + sLineBreak + Geral.FF0(FIMEIs[I]);
    if I < N then
      Result := Result + ',';
  end;
end;

function TFmVSImpClaIMEIs.ListaMovimCods(): String;
begin
  Result := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMCs, Dmod.MyDB, [
  'SELECT CAST(MovimCod AS SIGNED) MovimCod ',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle IN (' + ListaIMEIs() + ') ',
  '',
  'UNION',
  '',
  'SELECT CAST(MovimCod AS SIGNED) MovimCod ',
  'FROM ' + CO_TAB_VMB,
  'WHERE Controle IN (' + ListaIMEIs() + ') ',
  '']);
  //
  while not QrMCs.Eof do
  begin
    Result := Result + sLineBreak + Geral.FF0(QrMCsMovimCod.Value);
    if QrMCs.RecNo < QrMCs.RecordCount then
      Result := Result + ',';
    //
    QrMCs.Next;
  end;
end;

procedure TFmVSImpClaIMEIs.QrVSCacItsCalcFields(DataSet: TDataSet);
begin
  if QrVSCacSumAreaM2.Value > 0 then
    QrVSCacItsPercM2.Value := QrVSCacItsAreaM2.Value / QrVSCacSumAreaM2.Value * 100
  else
    QrVSCacItsPercM2.Value := 0;
  //
  if QrVSCacSumPecas.Value > 0 then
    QrVSCacItsPercPc.Value := QrVSCacItsPecas.Value / QrVSCacSumPecas.Value * 100
  else
    QrVSCacItsPercPc.Value := 0;
end;

procedure TFmVSImpClaIMEIs.ReopenVSCacIts();
  function GeraSQLVSMovItx(TabMov, TabCac: TTabToWork): String;
  var
    Une: Boolean;
  begin
    Une := not ((TabMov = ttwA) and (TabCac = ttwA));
    Result := Geral.ATS([
    'SELECT',
     VS_CRC_PF.SQL_NO_GGX(),
    'CAST(vsp.GraGruX AS SIGNED) GraGruX, ',
    'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
    // ini 2020-08-16
    '999999999999.9999 Preco, 999999999999.9999 Valor, ',
    // fim 2020-08-16
    'SUM(cia.Pecas) Pecas, ',
    'SUM(cia.AreaM2) AreaM2, ',
    'SUM(cia.AreaP2) AreaP2, ',
    '1 Ativo ',
    'FROM ' + TMeuDB + '.' + VS_CRC_PF.TabCacVS_Tab(TabCac) + ' cia ',
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=cia.VSPallet ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE cia.vmi_Sorc IN (' + ListaIMEIs() + ') ',
    'GROUP BY GraGruX',
    Geral.ATS_If(Une, ['', 'UNION']),
    '']);
  end;
var
  GraGruX, GraCusPrc: Integer;
  Preco, Valor: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacIts, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS ' + F_vs037_cia_its_ + '; ',
  'CREATE TABLE ' + F_vs037_cia_its_ + ' ',
  '',
  GeraSQLVSMovItx(ttwB, ttwB),
  GeraSQLVSMovItx(ttwB, ttwA),
  GeraSQLVSMovItx(ttwA, ttwB),
  GeraSQLVSMovItx(ttwA, ttwA),
  ';',
  'SELECT',
  'CAST(GraGruX AS SIGNED) GraGruX, ',
  'CAST(GraGru1 AS SIGNED) GraGru1, ',
  // ini 2020-08-16
  'Preco, Sum(Valor) Valor, ',
  // fim 2020-08-16
  'SUM(Pecas) Pecas, ',
  'SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'NO_PRD_TAM_COR ',
  'FROM ' + F_vs037_cia_its_,
  'GROUP BY GraGruX',
  'ORDER BY AreaM2 DESC;',
  '']);
  //
  // ini 2020-08-16
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Precificando itens de Classes');
  GraCusPrc := EdGraCusPrc.ValueVariant;
  if GraCusPrc <> 0 then
  begin
    QrVSCacIts.First;
    while not QrVSCacIts.Eof do
    begin
      GraGruX := QrVSCacItsGraGruX.Value;
      //
      //UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruVal, DModG.MyPID_DB, [
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruVal, DMod.MyDB, [
      'SELECT CustoPreco ',
      'FROM gragruval ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      'AND GraCusPrc=' + Geral.FF0(GraCusPrc),
      'ORDER BY DataIni DESC ',
      'LIMIT 0, 1 ',
      '']);
      //
      Preco := QrGraGruValCustoPreco.Value;
      Valor := Preco * QrVSCacItsAreaM2.Value;
      //
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, F_vs037_cia_its_, False, [
      'Preco', 'Valor'], ['GraGruX'], [
      Preco, Valor], [GraGruX], False);
      //
      QrVSCacIts.Next;
    end;
  end else
  begin
    Preco := 0;
    Valor := 0;
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, F_vs037_cia_its_, False, [
    'Preco', 'Valor'], ['Ativo'], [
    Preco, Valor], [1], False);
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Reabrindo itens de Classes precificados');
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacIts, DModG.MyPID_DB, [
  'SELECT',
  'CAST(GraGruX AS SIGNED) GraGruX, ',
  'CAST(GraGru1 AS SIGNED) GraGru1, ',
  'Preco, Sum(Valor) Valor, ',
  'SUM(Pecas) Pecas, ',
  'SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, ',
  'NO_PRD_TAM_COR ',
  'FROM ' + F_vs037_cia_its_,
  'GROUP BY GraGruX',
  'ORDER BY AreaM2 DESC;',
  '']);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, '...');
  // fim 2020-08-16
  //
end;

procedure TFmVSImpClaIMEIs.ReopenVSCacSum();
  function GeraSQLVSMovItx(TabMov, TabCac: TTabToWork): String;
  var
    Une: Boolean;
  begin
    Une := not ((TabMov = ttwA) and (TabCac = ttwA));
    Result := Geral.ATS([

    'SELECT SUM(cia.Pecas) Pecas,  ',
    'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2  ',
    'FROM ' + TMeuDB + '.' + VS_CRC_PF.TabCacVS_Tab(TabCac) + ' cia ',
    'WHERE cia.vmi_Sorc IN (' + ListaIMEIs() + ') ',
    Geral.ATS_If(Une, ['', 'UNION']),
    '']);
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacSum, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _vs037_cia_sum_; ',
  'CREATE TABLE _vs037_cia_sum_ ',
  '',
  GeraSQLVSMovItx(ttwB, ttwB),
  GeraSQLVSMovItx(ttwB, ttwA),
  GeraSQLVSMovItx(ttwA, ttwB),
  GeraSQLVSMovItx(ttwA, ttwA),
  '; ',
  'SELECT SUM(Pecas) Pecas,  ',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2  ',
  'FROM _vs037_cia_sum_; ',
  '']);
end;

procedure TFmVSImpClaIMEIs.ReopenVSGerArtSrc(Qry: TmySQLQuery;
  EstqMovimNiv: TEstqMovimNiv);
var
  MCs: String;
  //
  function GeraSQLVSMovItx(Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
  begin
    if VS_CRC_PF.GeraSQLTabMov(Result, Tab, TemIMEIMrt) and (MCs <> '') then
      Result := Geral.ATS([
      'SELECT ',
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
      'CAST(vmi.Ficha AS SIGNED) Ficha, ',
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, ',
      'CAST(vmi.NotaMPAG AS DECIMAL(15,8)) NotaMPAG, ',
      'CAST(vmi.Pecas AS DECIMAL(15,3)) Pecas, ',
      'CAST(vmi.PesoKg AS DECIMAL(15,3)) PesoKg, ',
      'CAST(vmi.Observ AS CHAR) Observ,',
      'CAST(vsf.Nome AS CHAR) NO_SerieFch, ',
      VS_CRC_PF.SQL_NO_GGX(),
      'vsp.Nome NO_Pallet,',
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
      'FROM ' + VS_CRC_PF.TabMovVS_Tab(tab) + ' vmi ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
      'WHERE vmi.MovimCod IN (' + MCs + ') ',
      'AND vmi.MovimNiv=' + Geral.FF0(Integer(EstqMovimNiv)),
      Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
      '']);
  end;
const
  TemIMEIMrt = 1;
begin
  MCs := Trim(ListaMovimCods());
  //
  if MCs = '' then
  begin
    Geral.MB_Info('Nenhum IME-C encontrado!');
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtOld, Dmod.MyDB, [
  GeraSQLVSMovItx(ttwB, TemIMEIMrt),
  GeraSQLVSMovItx(ttwA),
  //'ORDER BY NO_Pallet, Controle ',
  '']);
end;

procedure TFmVSImpClaIMEIs.SbGraCusPrcClick(Sender: TObject);
var
  Nivel1, TabePr: Integer;
  TipoLista: TTipoListaPreco;
  //MostraSubForm: TSubForm = tsfNenhum);
begin
  Nivel1    := 0;
  TabePr    := EdGraCusPrc.ValueVariant;
  TipoLista := TTipoListaPreco.tpGraCusPrc;
  //MostraSubForm: TSubForm = tsfNenhum);
  Grade_Jan.MostraFormGraGruN(Nivel1, TabePr, TipoLista(*, MostraSubForm: TSubForm = tsfNenhum*));
end;

end.
