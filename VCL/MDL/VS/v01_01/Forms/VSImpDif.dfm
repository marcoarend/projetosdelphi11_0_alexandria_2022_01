object FmVSImpDif: TFmVSImpDif
  Left = 0
  Top = 0
  Caption = 'WET-CURTI-086 :: Impress'#227'o de Diferen'#231'as de Couros VS'
  ClientHeight = 641
  ClientWidth = 837
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGMovDif: TDBGrid
    Left = 0
    Top = 0
    Width = 837
    Height = 641
    Align = alClient
    DataSource = DsMovDif
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'DifPecas'
        Title.Caption = 'Dif Pe'#231'as'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_FORNECE'
        Title.Caption = 'Nome Fornecedor'
        Width = 140
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Controle'
        Title.Caption = 'IME-I'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_SerieFch'
        Title.Caption = 'S'#233'rie RMP'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Ficha'
        Title.Caption = 'Ficha RMP'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GraGruX'
        Title.Caption = 'Reduzido'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_PRD_TAM_COR'
        Title.Caption = 'Mat'#233'ria-prima'
        Width = 200
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'RendKgm2_TXT'
        Title.Alignment = taCenter
        Title.Caption = 'Rend. Kg/m'#178
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Misturou_TXT'
        Title.Caption = 'Mistura'
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NotaMPAG_TXT'
        Title.Alignment = taCenter
        Title.Caption = 'Nota MPAG'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pecas'
        Title.Caption = 'Pe'#231'as'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PesoKg'
        Title.Caption = 'Peso kg'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ValorT'
        Title.Caption = 'Valor total'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoVrtPeca'
        Title.Caption = 'Sdo virtual p'#231
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QtdGerArM2'
        Title.Caption = 'm'#178' gerados'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Observ'
        Title.Caption = 'Observa'#231#245'es'
        Width = 300
        Visible = True
      end>
  end
  object QrMovDif: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DATE_FORMAT(vmi.DataHora, "%d/%m/%Y") DATA_TXT,'
      'vmd.InfPecas, vmd.InfPesoKg, vmd.InfAreaM2, '
      'vmd.infAreaP2, vmd.infValorT, vmd.DifPecas, vmd.DifPesoKg, '
      'vmd.DifAreaM2, vmd.DifAreaP2, vmd.DifValorT, '
      ''
      ''
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, vmi.ValorT, '
      'vmi.Controle, vmi.GraGruX, vmi.Terceiro, vmi.SerieFch, '
      'vmi.Ficha, vmi.Observ, vmi.DataHora,'
      ''
      ''
      ''
      ''
      ''
      'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,'
      'IF(vmi.SdoVrtPeca > 0, 0,'
      '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2,'
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT('
      '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3),'
      '  ",", ""), ".", ",")) RendKgm2_TXT,'
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE('
      'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT,'
      'IF(vmi.Misturou = 1, "SIM", "N'#195'O") Misturou_TXT,'
      'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,'
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT('
      '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3),'
      '  ",", ""), ".", ",")) m2_CouroTXT,'
      'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE '
      'FROM vsmovdif vmd'
      'LEFT JOIN vsmovits vmi ON vmi.Controle=vmd.Controle'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN entidades ent ON ent.Codigo=vmi.Terceiro '
      'WHERE vmd.DifPecas <> 0')
    Left = 52
    Top = 8
    object QrMovDifControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrMovDifGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrMovDifTerceiro: TLargeintField
      FieldName = 'Terceiro'
    end
    object QrMovDifSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrMovDifFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrMovDifDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrMovDifDATA_TXT: TWideStringField
      FieldName = 'DATA_TXT'
      Size = 10
    end
    object QrMovDifInfPecas: TFloatField
      FieldName = 'InfPecas'
    end
    object QrMovDifInfPesoKg: TFloatField
      FieldName = 'InfPesoKg'
    end
    object QrMovDifInfAreaM2: TFloatField
      FieldName = 'InfAreaM2'
    end
    object QrMovDifinfAreaP2: TFloatField
      FieldName = 'infAreaP2'
    end
    object QrMovDifinfValorT: TFloatField
      FieldName = 'infValorT'
    end
    object QrMovDifDifPecas: TFloatField
      FieldName = 'DifPecas'
    end
    object QrMovDifDifPesoKg: TFloatField
      FieldName = 'DifPesoKg'
    end
    object QrMovDifDifAreaM2: TFloatField
      FieldName = 'DifAreaM2'
    end
    object QrMovDifDifAreaP2: TFloatField
      FieldName = 'DifAreaP2'
    end
    object QrMovDifDifValorT: TFloatField
      FieldName = 'DifValorT'
    end
    object QrMovDifObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrMovDifNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrMovDifNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrMovDifNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrMovDifRendKgm2: TFloatField
      FieldName = 'RendKgm2'
    end
    object QrMovDifRendKgm2_TXT: TWideStringField
      FieldName = 'RendKgm2_TXT'
      Size = 58
    end
    object QrMovDifNotaMPAG_TXT: TWideStringField
      FieldName = 'NotaMPAG_TXT'
      Size = 53
    end
    object QrMovDifMisturou_TXT: TWideStringField
      FieldName = 'Misturou_TXT'
      Required = True
      Size = 3
    end
    object QrMovDifSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrMovDifNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrMovDifm2_CouroTXT: TWideStringField
      FieldName = 'm2_CouroTXT'
      Size = 59
    end
    object QrMovDifKgMedioCouro: TFloatField
      FieldName = 'KgMedioCouro'
    end
    object QrMovDifNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrMovDifPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMovDifPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrMovDifAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrMovDifAreaP2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AreaP2'
      Calculated = True
    end
    object QrMovDifValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object DsMovDif: TDataSource
    DataSet = QrMovDif
    Left = 52
    Top = 56
  end
  object frxDsMovDif: TfrxDBDataset
    UserName = 'frxDsMovDif'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'GraGruX=GraGruX'
      'Terceiro=Terceiro'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'DataHora=DataHora'
      'DATA_TXT=DATA_TXT'
      'InfPecas=InfPecas'
      'InfPesoKg=InfPesoKg'
      'InfAreaM2=InfAreaM2'
      'infAreaP2=infAreaP2'
      'infValorT=infValorT'
      'DifPecas=DifPecas'
      'DifPesoKg=DifPesoKg'
      'DifAreaM2=DifAreaM2'
      'DifAreaP2=DifAreaP2'
      'DifValorT=DifValorT'
      'Observ=Observ'
      'NO_SerieFch=NO_SerieFch'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'RendKgm2=RendKgm2'
      'RendKgm2_TXT=RendKgm2_TXT'
      'NotaMPAG_TXT=NotaMPAG_TXT'
      'Misturou_TXT=Misturou_TXT'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'NOMEUNIDMED=NOMEUNIDMED'
      'm2_CouroTXT=m2_CouroTXT'
      'KgMedioCouro=KgMedioCouro'
      'NO_FORNECE=NO_FORNECE'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT')
    DataSet = QrMovDif
    BCDToCurrency = False
    Left = 52
    Top = 104
  end
  object frxWET_CURTI_018_08_A: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 42056.709331932900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_018_08_AGetValue
    Left = 188
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMovDif
        DataSetName = 'frxDsMovDif'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 105.826830240000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 975.118740000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 188.976500000000000000
          Top = 18.897650000000000000
          Width = 593.385826771653500000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Diferen'#231'as de Movimento de Couros - [VARF_TIPO_MOV]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 782.362710000000000000
          Top = 18.897650000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 442.205010000000000000
          Top = 90.708720000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Top = 64.252010000000000000
          Width = 971.339210000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo4: TfrxMemoView
          Top = 68.031540000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 71.811070000000000000
          Top = 68.031540000000000000
          Width = 891.969080000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_TERCEIRO_1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 393.071120000000000000
          Top = 90.708720000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 498.897960000000000000
          Top = 90.708720000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 362.834880000000000000
          Top = 90.708720000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 68.031540000000000000
          Top = 90.708720000000000000
          Width = 113.385851180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 604.724800000000000000
          Top = 90.708720000000000000
          Width = 79.370029920000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 771.024120000000000000
          Top = 90.708720000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Info. ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 721.890230000000000000
          Top = 90.708720000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Info. m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 827.717070000000000000
          Top = 90.708720000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Info. Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 684.094930000000000000
          Top = 90.708720000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Info. p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 876.850960000000000000
          Top = 90.708720000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Info item $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 548.031850000000000000
          Top = 90.708720000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo item $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 90.708720000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 181.417440000000000000
          Top = 90.708720000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 298.582870000000000000
          Top = 90.708720000000000000
          Width = 64.251973390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'S'#233'rie/ficha')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 933.543910000000000000
          Top = 90.708720000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Dif. Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 230.551330000000000000
          Top = 90.708720000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 222.992270000000000000
        Width = 971.339210000000000000
        DataSet = frxDsMovDif
        DataSetName = 'frxDsMovDif'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 442.205010000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 393.071120000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 498.897960000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 362.834880000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 68.031540000000000000
          Width = 113.385851180000000000
          Height = 15.118110240000000000
          DataField = 'NO_FORNECE'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovDif."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 604.724800000000000000
          Width = 79.370029920000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovDif."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 771.024120000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'infAreaP2'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."infAreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 721.890230000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'InfAreaM2'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."InfAreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 827.717070000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'InfPesoKg'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."InfPesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 684.094930000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'InfPecas'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."InfPecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 876.850960000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'infValorT'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."infValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovDif."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 181.417440000000000000
          Width = 49.133853390000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMovDif."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 298.582870000000000000
          Width = 64.251973390000000000
          Height = 15.118110240000000000
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."NO_SerieFch"] [frxDsMovDif."Ficha"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 548.031850000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 230.551330000000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovDif."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 933.543910000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'DifPecas'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."DifPecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 381.732530000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 600.945270000000000000
          Width = 370.393940000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 185.196970000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsMovDif."DATA_TXT"'
        object Me_GH0: TfrxMemoView
          Width = 967.559680000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMovDif."DATA_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 260.787570000000000000
        Width = 971.339210000000000000
        object Me_FT0: TfrxMemoView
          Width = 933.543690310000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu0ValorT: TfrxMemoView
          Left = 933.543910000000000000
          Width = 37.795251180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMovDif."DifPecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 336.378170000000000000
        Width = 971.339210000000000000
        object Memo2: TfrxMemoView
          Top = 3.779530000000000000
          Width = 933.543690310000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Total de diferen'#231'as: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 933.543910000000000000
          Top = 3.779530000000000000
          Width = 37.795251180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMovDif."DifPecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxWET_CURTI_018_08_B: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 42056.709331932900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_018_08_AGetValue
    Left = 188
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMovDif
        DataSetName = 'frxDsMovDif'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 113.385885350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315009450000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.952804720000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Diferen'#231'as de Movimento de Couros - [VARF_TIPO_MOV]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo4: TfrxMemoView
          Top = 68.031540000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 71.811070000000000000
          Top = 68.031540000000000000
          Width = 600.945270000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_TERCEIRO_1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 328.819110000000000000
          Top = 90.708720000000000000
          Width = 94.488188980000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 105.826840000000000000
          Top = 90.708720000000000000
          Width = 222.992125984252000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Top = 90.708720000000000000
          Width = 105.826771653543300000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          Left = 612.283860000000000000
          Top = 90.708720000000000000
          Width = 68.031496060000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Dif. Pe'#231'as')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 423.307360000000000000
          Top = 90.708720000000000000
          Width = 188.976377950000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 22.677165350000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        DataSet = frxDsMovDif
        DataSetName = 'frxDsMovDif'
        RowCount = 0
        object Memo12: TfrxMemoView
          Left = 328.819110000000000000
          Width = 94.488188980000000000
          Height = 22.677165350000000000
          DataField = 'PesoKg'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."PesoKg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 105.826840000000000000
          Width = 222.992125984252000000
          Height = 22.677165354330710000
          DataField = 'NO_FORNECE'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovDif."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Width = 105.826771653543300000
          Height = 22.677165354330710000
          DataField = 'DataHora'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovDif."DataHora"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 423.307360000000000000
          Width = 188.976377950000000000
          Height = 22.677165354330710000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovDif."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 22.677165354330710000
          DataField = 'DifPecas'
          DataSet = frxDsMovDif
          DataSetName = 'frxDsMovDif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovDif."DifPecas"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 321.260050000000000000
          Width = 370.393940000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677165350000000000
        Top = 192.756030000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsMovDif."DATA_TXT"'
        object Me_GH0: TfrxMemoView
          Width = 676.535870000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMovDif."DATA_TXT"]')
          ParentFont = False
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677165350000000000
        Top = 283.464750000000000000
        Width = 680.315400000000000000
        object Me_FT0: TfrxMemoView
          Width = 608.504110310000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          ParentFont = False
        end
        object MeSu0ValorT: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMovDif."DifPecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 34.015755350000000000
        Top = 366.614410000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          Top = 7.559060000000000000
          Width = 298.582650310000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Total de diferen'#231'as: ')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 612.283860000000000000
          Top = 7.559060000000000000
          Width = 68.031496060000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMovDif."DifPecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object Qr08MovAjustes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.Codigo, vmi.MovimCod, vmi.Controle, '
      'vmi.GraGruX, vmi.Terceiro, vmi.SerieFch, vmi.Ficha, '
      'vmi.DataHora, vmi.MovimNiv, vmi.MovimID, '
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, vmi.AreaP2, vmi.ValorT,'
      
        ' ELT(vmi.MovimID + 1,"[ND]","Compra","Venda","Reclasse","Baixa",' +
        '"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado",' +
        '"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","P' +
        'r'#233' reclasse","Compra de Classificado","Baixa extra","Saldo anter' +
        'ior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub Pr' +
        'oduto","Reclasse Mul.") NO_MovimID,'
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto") NO_MovimNiv,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, '
      'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) VSP_CliStat, '
      'IF(vsp.Status IS NULL, 0, vsp.Status) VSP_STATUS, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, '
      'vps.Nome NO_STATUS, '
      
        'vsf.Nome NO_SerieFch, CONCAT(vsf.Nome, " ", vmi.Ficha) NO_SERIE_' +
        'FICHA, '
      'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, '
      'DATE_FORMAT(vmi.DataHora, "%Y/%m/%d") Data_TXT, '
      'DATE_FORMAT(vmi.DataHora, "%d/%m/%Y") Data_BR, '
      'IF(vmi.Pecas < 0, -1.000, 1.000) SentidoMov, '
      'IF(vmi.Pecas < 0, "Sa'#237'da", "Entrada") SentidoMov_TXT, '
      'ggx.GraGruY, ggy.Nome NO_GGY '
      'FROM bluederm_2_cialeather.vsmovits vmi '
      
        'LEFT JOIN bluederm_2_cialeather.vspalleta  vsp ON vsp.Codigo=vmi' +
        '.Pallet   '
      
        'LEFT JOIN bluederm_2_cialeather.gragrux    ggx ON ggx.Controle=I' +
        'F(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) '
      
        'LEFT JOIN bluederm_2_cialeather.gragruxcou xco ON xco.GraGruX=gg' +
        'x.Controle  '
      
        'LEFT JOIN bluederm_2_cialeather.gragruy    ggy ON ggy.Codigo=ggx' +
        '.GraGruY '
      
        'LEFT JOIN bluederm_2_cialeather.gragruc    ggc ON ggc.Controle=g' +
        'gx.GraGruC '
      
        'LEFT JOIN bluederm_2_cialeather.gracorcad  gcc ON gcc.Codigo=ggc' +
        '.GraCorCad '
      
        'LEFT JOIN bluederm_2_cialeather.gratamits  gti ON gti.Controle=g' +
        'gx.GraTamI '
      
        'LEFT JOIN bluederm_2_cialeather.gragru1    gg1 ON gg1.Nivel1=ggx' +
        '.GraGru1 '
      
        'LEFT JOIN bluederm_2_cialeather.entidades  ent ON ent.Codigo=vmi' +
        '.Terceiro '
      
        'LEFT JOIN bluederm_2_cialeather.entidades  emp ON emp.Codigo=vmi' +
        '.Empresa '
      
        'LEFT JOIN bluederm_2_cialeather.vspalsta   vps ON vps.Codigo=vsp' +
        '.Status  '
      
        'LEFT JOIN bluederm_2_cialeather.couniv2    cn2 ON cn2.Codigo=xco' +
        '.CouNiv2'
      
        'LEFT JOIN bluederm_2_cialeather.couniv1    cn1 ON cn1.Codigo=xco' +
        '.CouNiv1'
      
        'LEFT JOIN bluederm_2_cialeather.vsserfch   vsf ON vsf.Codigo=vmi' +
        '.SerieFch '
      'WHERE vmi.DataHora  >= "2015-09-01"'
      'AND ('
      '(MovimID=0) '
      ' OR '
      '(MovimID=9) '
      ' OR '
      '(MovimID=12) '
      ' OR '
      '(MovimID=13) '
      ' OR '
      '(MovimID=17) '
      ') '
      'ORDER BY SentidoMov, GraGruY, DataHora, Controle ')
    Left = 528
    Top = 40
    object Qr08MovAjustesCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object Qr08MovAjustesMovimCod: TLargeintField
      FieldName = 'MovimCod'
    end
    object Qr08MovAjustesControle: TLargeintField
      FieldName = 'Controle'
    end
    object Qr08MovAjustesGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object Qr08MovAjustesTerceiro: TLargeintField
      FieldName = 'Terceiro'
    end
    object Qr08MovAjustesSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object Qr08MovAjustesFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object Qr08MovAjustesDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object Qr08MovAjustesMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
    end
    object Qr08MovAjustesMovimID: TLargeintField
      FieldName = 'MovimID'
    end
    object Qr08MovAjustesPecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr08MovAjustesPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object Qr08MovAjustesAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr08MovAjustesAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object Qr08MovAjustesValorT: TFloatField
      FieldName = 'ValorT'
    end
    object Qr08MovAjustesNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object Qr08MovAjustesNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object Qr08MovAjustesNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr08MovAjustesNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object Qr08MovAjustesVSP_CliStat: TLargeintField
      FieldName = 'VSP_CliStat'
    end
    object Qr08MovAjustesVSP_STATUS: TLargeintField
      FieldName = 'VSP_STATUS'
    end
    object Qr08MovAjustesNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object Qr08MovAjustesNO_CliStat: TWideStringField
      FieldName = 'NO_CliStat'
      Size = 100
    end
    object Qr08MovAjustesNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object Qr08MovAjustesNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object Qr08MovAjustesNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object Qr08MovAjustesNO_SERIE_FICHA: TWideStringField
      FieldName = 'NO_SERIE_FICHA'
      Size = 72
    end
    object Qr08MovAjustesInteiros: TFloatField
      FieldName = 'Inteiros'
    end
    object Qr08MovAjustesData_TXT: TWideStringField
      FieldName = 'Data_TXT'
      Size = 10
    end
    object Qr08MovAjustesData_BR: TWideStringField
      FieldName = 'Data_BR'
      Size = 10
    end
    object Qr08MovAjustesSentidoMov: TFloatField
      FieldName = 'SentidoMov'
      Required = True
    end
    object Qr08MovAjustesSentidoMov_TXT: TWideStringField
      FieldName = 'SentidoMov_TXT'
      Required = True
      Size = 7
    end
    object Qr08MovAjustesGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object Qr08MovAjustesNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object Qr08MovAjustesPallet: TLargeintField
      FieldKind = fkCalculated
      FieldName = 'Pallet'
      Calculated = True
    end
  end
  object Ds08MovAjustes: TDataSource
    DataSet = Qr08MovAjustes
    Left = 528
    Top = 88
  end
  object frxDs08MovAjustes: TfrxDBDataset
    UserName = 'frxDs08MovAjustes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle'
      'GraGruX=GraGruX'
      'Terceiro=Terceiro'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'DataHora=DataHora'
      'MovimNiv=MovimNiv'
      'MovimID=MovimID'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'NO_MovimID=NO_MovimID'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'VSP_CliStat=VSP_CliStat'
      'VSP_STATUS=VSP_STATUS'
      'NO_FORNECE=NO_FORNECE'
      'NO_CliStat=NO_CliStat'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_STATUS=NO_STATUS'
      'NO_SerieFch=NO_SerieFch'
      'NO_SERIE_FICHA=NO_SERIE_FICHA'
      'Inteiros=Inteiros'
      'Data_TXT=Data_TXT'
      'Data_BR=Data_BR'
      'SentidoMov=SentidoMov'
      'SentidoMov_TXT=SentidoMov_TXT'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'Pallet=Pallet')
    DataSet = Qr08MovAjustes
    BCDToCurrency = False
    Left = 528
    Top = 136
  end
  object frxWET_CURTI_018_08_01_A: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42126.704835439810000000
    ReportOptions.LastChange = 42126.704835439810000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_018_08_01_AGetValue
    Left = 724
    Top = 128
    Datasets = <
      item
        DataSet = frxDs08MovAjustes
        DataSetName = 'frxDs08MovAjustes'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 52.913410240000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Shape3: TfrxShapeView
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 994.016390000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 188.976500000000000000
          Top = 18.897650000000000000
          Width = 631.181510000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 820.158010000000000000
          Top = 18.897650000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 650.079160000000000000
          Top = 37.795300000000000000
          Width = 132.283427950000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitCodi: TfrxMemoView
          Left = 551.811380000000000000
          Top = 37.795300000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPecas: TfrxMemoView
          Left = 952.441560000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 49.133890000000000000
          Top = 37.795300000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 132.283550000000000000
          Top = 37.795300000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Codigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 502.677490000000000000
          Top = 37.795300000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 181.417440000000000000
          Top = 37.795300000000000000
          Width = 83.149574570000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Processo ou opera'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 264.567100000000000000
          Top = 37.795300000000000000
          Width = 132.283354720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#237'vel do processo / opera'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 600.945270000000000000
          Top = 37.795300000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 895.748610000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 396.850650000000000000
          Top = 37.795300000000000000
          Width = 105.826796060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 839.055660000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 782.362710000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 419.527830000000000000
        Width = 1009.134510000000000000
        object Memo93: TfrxMemoView
          Width = 472.441250000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 631.181510000000000000
          Width = 377.953000000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 472.441250000000000000
          Width = 158.740196540000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Impresso em [VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 207.874150000000000000
        Width = 1009.134510000000000000
        DataSet = frxDs08MovAjustes
        DataSetName = 'frxDs08MovAjustes'
        RowCount = 0
        object MeValCodi: TfrxMemoView
          Left = 551.811380000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 49.133890000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValNome: TfrxMemoView
          Left = 650.079160000000000000
          Width = 132.283427950000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs08MovAjustes."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 952.441560000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 132.283550000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 502.677490000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'MovimCod'
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."MovimCod"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 200.315090000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs08MovAjustes."NO_MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 283.464750000000000000
          Width = 113.385704720000000000
          Height = 15.118110240000000000
          DataField = 'NO_MovimNiv'
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs08MovAjustes."NO_MovimNiv"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 181.417440000000000000
          Width = 18.897454720000000000
          Height = 15.118110240000000000
          DataField = 'MovimID'
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 264.567100000000000000
          Width = 18.897454720000000000
          Height = 15.118110240000000000
          DataField = 'MovimNiv'
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."MovimNiv"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 600.945270000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 895.748610000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 396.850650000000000000
          Width = 105.826796060000000000
          Height = 15.118110240000000000
          DataField = 'NO_SERIE_FICHA'
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs08MovAjustes."NO_SERIE_FICHA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 782.362710000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 839.055660000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 132.283550000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDs08MovAjustes."GraGruY"'
        object Me_GH1: TfrxMemoView
          Width = 1009.134314720000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs08MovAjustes."NO_GGY"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 291.023810000000000000
        Width = 1009.134510000000000000
        object Memo8: TfrxMemoView
          Left = 952.441560000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."Inteiros">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT1: TfrxMemoView
          Width = 782.362514720000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."NO_GGY"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 895.748610000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."Pecas">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 782.362710000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."AreaM2">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 839.055660000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."PesoKg">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 170.078850000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDs08MovAjustes."GraGruY"'
        object Me_GH2: TfrxMemoView
          Left = 64.252010000000000000
          Width = 944.882304720000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs08MovAjustes."NO_GGY"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_02: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 245.669450000000000000
        Width = 1009.134510000000000000
        object Memo6: TfrxMemoView
          Left = 952.441560000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."Inteiros">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT2: TfrxMemoView
          Left = 64.252010000000000000
          Width = 718.110504720000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs08MovAjustes."NO_GGY"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 895.748610000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."Pecas">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 782.362710000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."AreaM2">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 839.055660000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."PesoKg">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 374.173470000000000000
        Width = 1009.134510000000000000
        object Memo23: TfrxMemoView
          Left = 952.441560000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."Inteiros">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 3.779530000000000000
          Width = 782.362514720000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL PESQUISADO: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 895.748610000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."Pecas">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 782.362710000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."AreaM2">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 839.055660000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs08MovAjustes
          DataSetName = 'frxDs08MovAjustes'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs08MovAjustes."PesoKg">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
