object FmVSClaPalPrpQnz: TFmVSClaPalPrpQnz
  Left = 339
  Top = 185
  Caption = 
    'WET-CURTI-207 :: Configura'#231#227'o de Classifica'#231#227'o de Artigo de Ribe' +
    'ira por Pallet'
  ClientHeight = 613
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 738
        Height = 32
        Caption = 'Configura'#231#227'o de Classifica'#231#227'o de Artigo de Ribeira por Pallet'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 738
        Height = 32
        Caption = 'Configura'#231#227'o de Classifica'#231#227'o de Artigo de Ribeira por Pallet'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 738
        Height = 32
        Caption = 'Configura'#231#227'o de Classifica'#231#227'o de Artigo de Ribeira por Pallet'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 451
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 451
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 451
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 434
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object PnPartida: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 125
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel13: TPanel
              Left = 0
              Top = 0
              Width = 729
              Height = 125
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object LaVSRibCad: TLabel
                Left = 116
                Top = 80
                Width = 31
                Height = 13
                Caption = 'IME-I :'
                Enabled = False
              end
              object SbIMEI: TSpeedButton
                Left = 684
                Top = 96
                Width = 21
                Height = 21
                Caption = '>'
                Enabled = False
                OnClick = SbIMEIClick
              end
              object Label33: TLabel
                Left = 48
                Top = 0
                Width = 83
                Height = 13
                Caption = 'C'#243'digo de barras:'
                Enabled = False
              end
              object Label34: TLabel
                Left = 644
                Top = 0
                Width = 46
                Height = 13
                Caption = 'DV IME-I:'
              end
              object Label49: TLabel
                Left = 48
                Top = 40
                Width = 60
                Height = 13
                Caption = 'Localiza'#231#227'o:'
              end
              object Label36: TLabel
                Left = 48
                Top = 80
                Width = 29
                Height = 13
                Caption = 'Pallet:'
              end
              object EdIMEI: TdmkEditCB
                Left = 116
                Top = 96
                Width = 64
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdIMEIChange
                OnRedefinido = EdIMEIRedefinido
                DBLookupComboBox = CBIMEI
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBIMEI: TdmkDBLookupComboBox
                Left = 184
                Top = 96
                Width = 497
                Height = 21
                KeyField = 'Controle'
                ListField = 'IMEI_NO_PRD_TAM_COR_FICHA'
                ListSource = DsVSESCCabNew
                TabOrder = 6
                dmkEditCB = EdIMEI
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdDVIMEI: TdmkEdit
                Left = 644
                Top = 16
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 3
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object MeLeitura: TMemo
                Left = 48
                Top = 16
                Width = 593
                Height = 21
                Enabled = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                OnChange = MeLeituraChange
              end
              object EdStqCenLoc: TdmkEditCB
                Left = 48
                Top = 56
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'StqCenLoc'
                UpdCampo = 'StqCenLoc'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBStqCenLoc
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBStqCenLoc: TdmkDBLookupComboBox
                Left = 103
                Top = 56
                Width = 598
                Height = 21
                KeyField = 'Controle'
                ListField = 'NO_LOC_CEN'
                ListSource = DsStqCenLoc
                TabOrder = 3
                dmkEditCB = EdStqCenLoc
                UpdType = utYes
                LocF7NameFldName = 'Nome'
                LocF7SQLMasc = '$#'
              end
              object EdPallet: TdmkEdit
                Left = 48
                Top = 96
                Width = 65
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdPalletChange
                OnRedefinido = EdPalletRedefinido
              end
            end
            object Panel17: TPanel
              Left = 729
              Top = 0
              Width = 275
              Height = 125
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              ExplicitLeft = 728
              ExplicitTop = 4
              ExplicitWidth = 276
              ExplicitHeight = 121
              object Label30: TLabel
                Left = 20
                Top = 0
                Width = 47
                Height = 13
                Caption = 'ID Config:'
                Enabled = False
              end
              object Label31: TLabel
                Left = 104
                Top = 0
                Width = 32
                Height = 13
                Caption = 'IME-C:'
                Enabled = False
              end
              object Label32: TLabel
                Left = 188
                Top = 0
                Width = 18
                Height = 13
                Caption = 'OC:'
                Enabled = False
              end
              object EdCodigo: TdmkEdit
                Left = 20
                Top = 16
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdMovimCod: TdmkEdit
                Left = 104
                Top = 16
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdCacCod: TdmkEdit
                Left = 188
                Top = 16
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
          object PnPallets: TPanel
            Left = 0
            Top = 125
            Width = 1004
            Height = 309
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            Visible = False
            object Panel21: TPanel
              Left = 0
              Top = 0
              Width = 1004
              Height = 309
              Align = alClient
              TabOrder = 0
              object PnTecla: TPanel
                Left = 1
                Top = 1
                Width = 500
                Height = 307
                Align = alLeft
                TabOrder = 0
                object Panel25: TPanel
                  Left = 1
                  Top = 1
                  Width = 498
                  Height = 44
                  Align = alTop
                  BevelOuter = bvNone
                  Caption = 'Panel25'
                  TabOrder = 0
                  object Panel6: TPanel
                    Left = 43
                    Top = 0
                    Width = 455
                    Height = 44
                    Align = alClient
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label1: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object LaVSRibCla: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SbPallet01: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SbPallet01Click
                    end
                    object EdPallet01: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet01
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet01: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet01
                      TabOrder = 1
                      dmkEditCB = EdPallet01
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel9: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 44
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '01'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel7: TPanel
                  Left = 1
                  Top = 45
                  Width = 498
                  Height = 44
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object PnTecla2: TPanel
                    Left = 43
                    Top = 0
                    Width = 455
                    Height = 44
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Panel8: TPanel
                      Left = 0
                      Top = 0
                      Width = 480
                      Height = 44
                      Align = alLeft
                      BevelOuter = bvNone
                      Caption = 'Panel6'
                      TabOrder = 0
                      object Label5: TLabel
                        Left = 4
                        Top = 0
                        Width = 29
                        Height = 13
                        Caption = 'Pallet:'
                      end
                      object Label6: TLabel
                        Left = 60
                        Top = 0
                        Width = 186
                        Height = 13
                        Caption = 'Nome do Artigo de Ribeira Classificado:'
                      end
                      object SBPallet02: TSpeedButton
                        Left = 428
                        Top = 16
                        Width = 23
                        Height = 22
                        Caption = '...'
                        OnClick = SBPallet02Click
                      end
                      object EdPallet02: TdmkEditCB
                        Left = 4
                        Top = 16
                        Width = 56
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '-2147483647'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                        DBLookupComboBox = CBPallet02
                        IgnoraDBLookupComboBox = False
                        AutoSetIfOnlyOneReg = setregOnlyManual
                      end
                      object CBPallet02: TdmkDBLookupComboBox
                        Left = 59
                        Top = 16
                        Width = 368
                        Height = 21
                        KeyField = 'Codigo'
                        ListField = 'NO_PRD_TAM_COR'
                        ListSource = DsVSPallet02
                        TabOrder = 1
                        dmkEditCB = EdPallet02
                        UpdType = utYes
                        LocF7SQLMasc = '$#'
                      end
                    end
                  end
                  object Panel10: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 44
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '02'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel11: TPanel
                  Left = 1
                  Top = 89
                  Width = 498
                  Height = 44
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 2
                  object Panel12: TPanel
                    Left = 43
                    Top = 0
                    Width = 480
                    Height = 44
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label10: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label11: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet03: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet03Click
                    end
                    object EdPallet03: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet03
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet03: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet03
                      TabOrder = 1
                      dmkEditCB = EdPallet03
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel14: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 44
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '03'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel15: TPanel
                  Left = 1
                  Top = 133
                  Width = 498
                  Height = 43
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 3
                  object Panel16: TPanel
                    Left = 43
                    Top = 0
                    Width = 480
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label15: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label16: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet04: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet04Click
                    end
                    object EdPallet04: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet04
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet04: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet04
                      TabOrder = 1
                      dmkEditCB = EdPallet04
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel18: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '04'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel19: TPanel
                  Left = 1
                  Top = 176
                  Width = 498
                  Height = 43
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 4
                  object Panel20: TPanel
                    Left = 43
                    Top = 0
                    Width = 480
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label20: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label21: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet05: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet05Click
                    end
                    object EdPallet05: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet05
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet05: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet05
                      TabOrder = 1
                      dmkEditCB = EdPallet05
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel22: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '05'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel23: TPanel
                  Left = 1
                  Top = 219
                  Width = 498
                  Height = 43
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 5
                  object Panel24: TPanel
                    Left = 43
                    Top = 0
                    Width = 480
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label25: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label26: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet06: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet06Click
                    end
                    object EdPallet06: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet06
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet06: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet06
                      TabOrder = 1
                      dmkEditCB = EdPallet06
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel26: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '06'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel27: TPanel
                  Left = 1
                  Top = 262
                  Width = 498
                  Height = 43
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 6
                  object Panel28: TPanel
                    Left = 43
                    Top = 0
                    Width = 480
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label2: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label3: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet07: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet07Click
                    end
                    object EdPallet07: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet07
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet07: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet07
                      TabOrder = 1
                      dmkEditCB = EdPallet07
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel29: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '07'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel53: TPanel
                  Left = 1
                  Top = 305
                  Width = 498
                  Height = 43
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 7
                  object Panel54: TPanel
                    Left = 43
                    Top = 0
                    Width = 480
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label28: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label29: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet08: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet08Click
                    end
                    object EdPallet08: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet08
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet08: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet08
                      TabOrder = 1
                      dmkEditCB = EdPallet08
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel55: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '08'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
              end
              object Panel30: TPanel
                Left = 501
                Top = 1
                Width = 502
                Height = 307
                Align = alClient
                TabOrder = 1
                object Label35: TLabel
                  Left = 52
                  Top = 308
                  Width = 34
                  Height = 13
                  Caption = 'Senha:'
                end
                object Panel31: TPanel
                  Left = 1
                  Top = 1
                  Width = 500
                  Height = 44
                  Align = alTop
                  BevelOuter = bvNone
                  Caption = 'Panel25'
                  TabOrder = 0
                  object Panel32: TPanel
                    Left = 43
                    Top = 0
                    Width = 457
                    Height = 44
                    Align = alClient
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label4: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label7: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet09: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet09Click
                    end
                    object EdPallet09: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet09
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet09: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet09
                      TabOrder = 1
                      dmkEditCB = EdPallet09
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel33: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 44
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '09'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel34: TPanel
                  Left = 1
                  Top = 45
                  Width = 500
                  Height = 44
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Panel35: TPanel
                    Left = 43
                    Top = 0
                    Width = 457
                    Height = 44
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Panel36: TPanel
                      Left = 0
                      Top = 0
                      Width = 480
                      Height = 44
                      Align = alLeft
                      BevelOuter = bvNone
                      Caption = 'Panel6'
                      TabOrder = 0
                      object Label8: TLabel
                        Left = 4
                        Top = 0
                        Width = 29
                        Height = 13
                        Caption = 'Pallet:'
                      end
                      object Label9: TLabel
                        Left = 60
                        Top = 0
                        Width = 186
                        Height = 13
                        Caption = 'Nome do Artigo de Ribeira Classificado:'
                      end
                      object SBPallet10: TSpeedButton
                        Left = 428
                        Top = 16
                        Width = 23
                        Height = 22
                        Caption = '...'
                        OnClick = SBPallet10Click
                      end
                      object EdPallet10: TdmkEditCB
                        Left = 4
                        Top = 16
                        Width = 56
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '-2147483647'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                        DBLookupComboBox = CBPallet10
                        IgnoraDBLookupComboBox = False
                        AutoSetIfOnlyOneReg = setregOnlyManual
                      end
                      object CBPallet10: TdmkDBLookupComboBox
                        Left = 59
                        Top = 16
                        Width = 368
                        Height = 21
                        KeyField = 'Codigo'
                        ListField = 'NO_PRD_TAM_COR'
                        ListSource = DsVSPallet10
                        TabOrder = 1
                        dmkEditCB = EdPallet10
                        UpdType = utYes
                        LocF7SQLMasc = '$#'
                      end
                    end
                  end
                  object Panel37: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 44
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '10'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel38: TPanel
                  Left = 1
                  Top = 89
                  Width = 500
                  Height = 44
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 2
                  object Panel39: TPanel
                    Left = 43
                    Top = 0
                    Width = 480
                    Height = 44
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label12: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label13: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet11: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet11Click
                    end
                    object EdPallet11: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet11
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet11: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet11
                      TabOrder = 1
                      dmkEditCB = EdPallet11
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel40: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 44
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '11'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel41: TPanel
                  Left = 1
                  Top = 133
                  Width = 500
                  Height = 43
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 3
                  object Panel42: TPanel
                    Left = 43
                    Top = 0
                    Width = 480
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label14: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label17: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet12: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet12Click
                    end
                    object EdPallet12: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet12
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet12: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet12
                      TabOrder = 1
                      dmkEditCB = EdPallet12
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel43: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '12'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel44: TPanel
                  Left = 1
                  Top = 176
                  Width = 500
                  Height = 43
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 4
                  object Panel45: TPanel
                    Left = 43
                    Top = 0
                    Width = 480
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label18: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label19: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet13: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet13Click
                    end
                    object EdPallet13: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet13
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet13: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet13
                      TabOrder = 1
                      dmkEditCB = EdPallet13
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel46: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '13'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel47: TPanel
                  Left = 1
                  Top = 219
                  Width = 500
                  Height = 43
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 5
                  object Panel48: TPanel
                    Left = 43
                    Top = 0
                    Width = 480
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label22: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label23: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet14: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet14Click
                    end
                    object EdPallet14: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet14
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet14: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet14
                      TabOrder = 1
                      dmkEditCB = EdPallet14
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel49: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '14'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object Panel50: TPanel
                  Left = 1
                  Top = 262
                  Width = 500
                  Height = 43
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 6
                  object Panel51: TPanel
                    Left = 43
                    Top = 0
                    Width = 480
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = 'Panel6'
                    TabOrder = 0
                    object Label24: TLabel
                      Left = 4
                      Top = 0
                      Width = 29
                      Height = 13
                      Caption = 'Pallet:'
                    end
                    object Label27: TLabel
                      Left = 60
                      Top = 0
                      Width = 186
                      Height = 13
                      Caption = 'Nome do Artigo de Ribeira Classificado:'
                    end
                    object SBPallet15: TSpeedButton
                      Left = 428
                      Top = 16
                      Width = 23
                      Height = 22
                      Caption = '...'
                      OnClick = SBPallet15Click
                    end
                    object EdPallet15: TdmkEditCB
                      Left = 4
                      Top = 16
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPallet15
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPallet15: TdmkDBLookupComboBox
                      Left = 59
                      Top = 16
                      Width = 368
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'NO_PRD_TAM_COR'
                      ListSource = DsVSPallet15
                      TabOrder = 1
                      dmkEditCB = EdPallet15
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel52: TPanel
                    Left = 0
                    Top = 0
                    Width = 43
                    Height = 43
                    Align = alLeft
                    BevelOuter = bvNone
                    Caption = '15'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = 10485760
                    Font.Height = -32
                    Font.Name = 'Arial Black'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object EdVSPwdDdClas: TEdit
                  Left = 52
                  Top = 324
                  Width = 421
                  Height = 22
                  Font.Charset = SYMBOL_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Wingdings'
                  Font.Style = []
                  ParentFont = False
                  PasswordChar = 'l'
                  TabOrder = 7
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 499
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 543
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtReclasif: TBitBtn
        Tag = 421
        Left = 400
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Gera artigos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtReclasifClick
      end
    end
  end
  object QrVSPallet01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 300
    Top = 45
    object QrVSPallet01Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet01Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet01Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet01DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet01DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet01UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet01UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet01AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet01Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet01Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet01NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet01Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet01CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet01GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet01NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet01NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet01NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet01: TDataSource
    DataSet = QrVSPallet01
    Left = 300
    Top = 89
  end
  object QrVSESCCabNew: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet ')
    Left = 808
    Top = 121
    object QrVSESCCabNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSESCCabNewControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSESCCabNewMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSESCCabNewEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSESCCabNewMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSESCCabNewGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSESCCabNewPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSESCCabNewPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSESCCabNewAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSESCCabNewAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSESCCabNewLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSESCCabNewDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSESCCabNewDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSESCCabNewUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSESCCabNewUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSESCCabNewAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSESCCabNewAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSESCCabNewNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSESCCabNewSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSESCCabNewSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSESCCabNewSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSESCCabNewPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSESCCabNewNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSESCCabNewSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSESCCabNewSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSESCCabNewObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSESCCabNewValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSESCCabNewMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSESCCabNewCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrVSESCCabNewMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSESCCabNewTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSESCCabNewCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSESCCabNewLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSESCCabNewFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSESCCabNewLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSESCCabNewDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSESCCabNewMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSESCCabNewCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSESCCabNewSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSESCCabNewValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSESCCabNewDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSESCCabNewDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSESCCabNewDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSESCCabNewQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSESCCabNewQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSESCCabNewQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSESCCabNewQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSESCCabNewQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSESCCabNewQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSESCCabNewQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSESCCabNewQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSESCCabNewNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSESCCabNewNO_FICHA: TWideStringField
      FieldName = 'NO_FICHA'
    end
    object QrVSESCCabNewCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSESCCabNewCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSESCCabNewIMEI_NO_PRD_TAM_COR_FICHA: TWideStringField
      FieldName = 'IMEI_NO_PRD_TAM_COR_FICHA'
      Size = 255
    end
    object QrVSESCCabNewVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSESCCabNewClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSESCCabNewGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
  object DsVSESCCabNew: TDataSource
    DataSet = QrVSESCCabNew
    Left = 808
    Top = 169
  end
  object QrVSPallet02: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 364
    Top = 45
    object QrVSPallet02Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet02Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet02Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet02DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet02DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet02UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet02UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet02AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet02Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet02Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet02NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet02Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet02CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet02GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet02NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet02NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet02NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet02: TDataSource
    DataSet = QrVSPallet02
    Left = 364
    Top = 89
  end
  object QrVSPallet03: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 428
    Top = 45
    object QrVSPallet03Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet03Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet03Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet03DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet03DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet03UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet03UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet03AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet03Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet03Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet03NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet03Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet03CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet03GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet03NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet03NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet03NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet03: TDataSource
    DataSet = QrVSPallet03
    Left = 428
    Top = 89
  end
  object QrVSPallet04: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 300
    Top = 141
    object QrVSPallet04Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet04Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet04Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet04DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet04DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet04UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet04UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet04AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet04Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet04Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet04NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet04Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet04CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet04GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet04NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet04NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet04NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet04: TDataSource
    DataSet = QrVSPallet04
    Left = 300
    Top = 185
  end
  object QrVSPallet05: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 364
    Top = 141
    object QrVSPallet05Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet05Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet05Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet05DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet05DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet05UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet05UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet05AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet05Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet05Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet05NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet05Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet05CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet05GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet05NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet05NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet05NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet05: TDataSource
    DataSet = QrVSPallet05
    Left = 364
    Top = 185
  end
  object QrVSPallet06: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 428
    Top = 141
    object QrVSPallet06Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet06Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet06Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet06DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet06DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet06UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet06UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet06AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet06Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet06Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet06NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet06Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet06CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet06GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet06NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet06NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet06NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet06: TDataSource
    DataSet = QrVSPallet06
    Left = 428
    Top = 185
  end
  object QrVSPallet07: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 300
    Top = 229
    object QrVSPallet07Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet07Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet07Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet07DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet07DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet07UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet07UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet07AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet07Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet07Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet07NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet07Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet07CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet07GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet07NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet07NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet07NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet07: TDataSource
    DataSet = QrVSPallet07
    Left = 300
    Top = 273
  end
  object QrVSPallet08: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 364
    Top = 229
    object QrVSPallet08Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet08Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet08Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet08DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet08DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet08UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet08UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet08AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet08Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet08Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet08NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet08Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet08CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet08GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet08NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet08NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet08NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet08: TDataSource
    DataSet = QrVSPallet08
    Left = 364
    Top = 273
  end
  object QrVSPallet09: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 432
    Top = 229
    object QrVSPallet09Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet09Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet09Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet09DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet09DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet09UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet09UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet09AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet09Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet09Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet09NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet09Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet09CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet09GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet09NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet09NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet09NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet09: TDataSource
    DataSet = QrVSPallet09
    Left = 432
    Top = 273
  end
  object QrVSPallet10: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 300
    Top = 317
    object QrVSPallet10Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet10Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet10Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet10DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet10DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet10UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet10UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet10AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet10Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet10Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet10NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet10Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet10CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet10GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet10NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet10NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet10NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet10: TDataSource
    DataSet = QrVSPallet10
    Left = 300
    Top = 361
  end
  object QrVSPallet11: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 364
    Top = 317
    object QrVSPallet11Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet11Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet11Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet11DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet11DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet11UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet11UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet11AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet11Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet11Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet11NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet11Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet11CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet11GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet11NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet11NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet11NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet11: TDataSource
    DataSet = QrVSPallet11
    Left = 364
    Top = 361
  end
  object QrVSPallet12: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 432
    Top = 317
    object QrVSPallet12Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet12Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet12Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet12DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet12DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet12UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet12UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet12AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet12Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet12Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet12NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet12Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet12CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet12GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet12NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet12NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet12NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet12: TDataSource
    DataSet = QrVSPallet12
    Left = 432
    Top = 361
  end
  object QrVSPallet13: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 300
    Top = 405
    object QrVSPallet13Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet13Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet13Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet13DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet13DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet13UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet13UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet13AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet13Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet13Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet13NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet13Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet13CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet13GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet13NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet13NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet13NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet13: TDataSource
    DataSet = QrVSPallet13
    Left = 300
    Top = 449
  end
  object QrVSPallet14: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 364
    Top = 405
    object QrVSPallet14Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet14Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet14Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet14DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet14DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet14UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet14UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet14AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet14Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet14Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet14NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet14Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet14CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet14GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet14NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet14NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet14NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet14: TDataSource
    DataSet = QrVSPallet14
    Left = 364
    Top = 449
  end
  object QrVSPallet15: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 432
    Top = 405
    object QrVSPallet15Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet15Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet15Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet15DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet15DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet15UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet15UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet15AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet15Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet15Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet15NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPallet15Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet15CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet15GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet15NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet15NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet15NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsVSPallet15: TDataSource
    DataSet = QrVSPallet15
    Left = 432
    Top = 449
  end
  object QrStqCenLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 880
    Top = 112
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 880
    Top = 160
  end
end
