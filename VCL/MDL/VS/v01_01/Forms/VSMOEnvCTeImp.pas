unit VSMOEnvCTeImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  frxClass, Vcl.Mask, mySQLDbTables, frxDBSet, UnProjGroup_Consts;

type
  TFmVSMOEnvCTeImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    frxWET_CURTI_229_A: TfrxReport;
    QrCTe: TmySQLQuery;
    QrCTeEmpresa: TIntegerField;
    QrCTeTerceiro: TIntegerField;
    QrCTeSerCT: TIntegerField;
    QrCTenCT: TIntegerField;
    QrCTeNO_Transp: TWideStringField;
    QrCTePecas: TFloatField;
    QrCTePesoKg: TFloatField;
    QrCTeAreaM2: TFloatField;
    QrCTeAreaP2: TFloatField;
    QrCTePesTrKg: TFloatField;
    QrCTeCusTrKg: TFloatField;
    QrCTeValorT: TFloatField;
    DsCTe: TDataSource;
    QrCTeIts: TmySQLQuery;
    QrCTeItsDataCad: TDateField;
    QrCTeItsTipoFrete: TWideStringField;
    QrCTeItsTabela: TWideStringField;
    QrCTeItsEmpresa: TIntegerField;
    QrCTeItsTerceiro: TIntegerField;
    QrCTeItsSerCT: TIntegerField;
    QrCTeItsnCT: TIntegerField;
    QrCTeItsPecas: TFloatField;
    QrCTeItsPesoKg: TFloatField;
    QrCTeItsAreaM2: TFloatField;
    QrCTeItsAreaP2: TFloatField;
    QrCTeItsPesTrKg: TFloatField;
    QrCTeItsCusTrKg: TFloatField;
    QrCTeItsValorT: TFloatField;
    DsCTeIts: TDataSource;
    Panel5: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label3: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label4: TLabel;
    DBEdit7: TDBEdit;
    Label5: TLabel;
    DBEdit8: TDBEdit;
    Label6: TLabel;
    DBEdit9: TDBEdit;
    Label7: TLabel;
    DBEdit10: TDBEdit;
    Label8: TLabel;
    DBEdit11: TDBEdit;
    Label9: TLabel;
    DBEdit12: TDBEdit;
    Label10: TLabel;
    DBEdit13: TDBEdit;
    DBGrid2: TDBGrid;
    QrCTeNO_Empresa: TWideStringField;
    frxDsCTe: TfrxDBDataset;
    frxDsCTeIts: TfrxDBDataset;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FVSMOEnvCTeImp: String;
    //
    procedure ReopenCTe();
  public
    { Public declarations }
    FEmpresa, FTerceiro, FSerCTe, FnCTe: Integer;
    //
    procedure PesquisaCTe();
  end;

  var
  FmVSMOEnvCTeImp: TFmVSMOEnvCTeImp;

implementation

uses UnMyObjects, ModuleGeral, CreateVS, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSMOEnvCTeImp.BtOKClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxWET_CURTI_229_A, [
  DModG.frxDsDono,
  frxDsCTe,
  frxDsCTeIts
  ]);
  //
  MyObjects.frxMostra(frxWET_CURTI_229_A, 'AUTORIZAÇÃO DE CARREGAMENTO E TRANSPORTE');
end;

procedure TFmVSMOEnvCTeImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSMOEnvCTeImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMOEnvCTeImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSMOEnvCTeImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMOEnvCTeImp.PesquisaCTe();
begin
  FVSMOEnvCTeImp :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSMOEnvCTeGer, DModG.QrUpdPID1, False,
    1, '_vs_mo_env_cte_imp');
(*
RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;  //
*)
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FVSMOEnvCTeImp,
  '',
  'SELECT DataCad, "Simples" TipoFrete, ',
  '"' + CO_TAB_VSMOEnvavu + '" Tabela, CFTMA_Empresa Empresa, ',
  'CFTMA_Terceiro Terceiro, CFTMA_SerCT SerCT, CFTMA_nCT nCT, ',
  'CFTMA_Pecas Pecas, CFTMA_PesoKg PesoKg,  ',
  'CFTMA_AreaM2 AreaM2, CFTMA_AreaP2 AreaP2,  ',
  'CFTMA_PesTrKg PesTrKg, CFTMA_CusTrKg CusTrKg,  ',
  'CFTMA_ValorT ValorT  ',
  'FROM ' + TMeuDB + '.vsmoenvavu ',
  'WHERE CFTMA_Empresa=' + Geral.FF0(FEmpresa),
  'AND CFTMA_Terceiro=' + Geral.FF0(FTerceiro),
  'AND CFTMA_SerCT=' + Geral.FF0(FSerCTe),
  'AND CFTMA_nCT=' + Geral.FF0(FnCTe),
  ' ',
  'UNION ',
  ' ',
  'SELECT DataCad, "Envio" TipoFrete, ',
  '"' + CO_TAB_VSMOEnvenv + '" Tabela, CFTMP_Empresa Empresa, ',
  'CFTMP_Terceiro Terceiro, CFTMP_SerCT SerCT, CFTMP_nCT nCT, ',
  'CFTMP_Pecas Pecas, CFTMP_PesoKg PesoKg, ',
  'CFTMP_AreaM2 AreaM2, CFTMP_AreaP2 AreaP2, ',
  'CFTMP_PesTrKg PesTrKg, CFTMP_CusTrKg CusTrKg, ',
  'CFTMP_ValorT ValorT ',
  'FROM ' + TMeuDB + '.vsmoenvenv ',
  'WHERE CFTMP_Empresa=' + Geral.FF0(FEmpresa),
  'AND CFTMP_Terceiro=' + Geral.FF0(FTerceiro),
  'AND CFTMP_SerCT=' + Geral.FF0(FSerCTe),
  'AND CFTMP_nCT=' + Geral.FF0(FnCTe),
  ' ',
  'UNION ',
  ' ',
  'SELECT DataCad, "Retorno" TipoFrete, ',
  '"' + CO_TAB_VSMOEnvret + '" Tabela, CFTPA_Empresa Empresa, ',
  'CFTPA_Terceiro Terceiro, CFTPA_SerCT SerCT, CFTPA_nCT nCT, ',
  'CFTPA_Pecas Pecas, CFTPA_PesoKg PesoKg, ',
  'CFTPA_AreaM2 AreaM2, CFTPA_AreaP2 AreaP2, ',
  'CFTPA_PesTrKg PesTrKg, CFTPA_CusTrKg CusTrKg, ',
  'CFTPA_ValorT ValorT ',
  'FROM ' + TMeuDB + '.vsmoenvret ',
  'WHERE CFTPA_Empresa=' + Geral.FF0(FEmpresa),
  'AND CFTPA_Terceiro=' + Geral.FF0(FTerceiro),
  'AND CFTPA_SerCT=' + Geral.FF0(FSerCTe),
  'AND CFTPA_nCT=' + Geral.FF0(FnCTe),
  ' ',
  'ORDER BY DataCad DESC, SerCT DESC, nCT DESC ',
  ' ',
  '']);
  //
  ReopenCTe();
end;

procedure TFmVSMOEnvCTeImp.ReopenCTe();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTe, DModG.MyPID_DB, [
  'SELECT ecg.Empresa, ecg.Terceiro,',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_Empresa,',
  'IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_Transp,',
  'ecg.SerCT, ecg.nCT,',
  'SUM(ecg.Pecas) Pecas, SUM(ecg.PesoKg) PesoKg,',
  'SUM(ecg.AreaM2) AreaM2, SUM(ecg.AreaP2) AreaP2,',
  'SUM(ecg. PesTrKg)  PesTrKg, SUM(ecg.CusTrKg) CusTrKg,',
  'SUM(ecg.ValorT) ValorT',
  'FROM ' + FVSMOEnvCTeImp + ' ecg',
  'LEFT JOIN ' + TMeuDB + '.entidades emp ON emp.Codigo=ecg.Empresa',
  'LEFT JOIN ' + TMeuDB + '.entidades tra ON tra.Codigo=ecg.Terceiro',
  'GROUP BY Empresa, Terceiro, SerCT, nCT',
  'ORDER BY ecg.DataCad DESC',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIts, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FVSMOEnvCTeImp,
  'WHERE Empresa=' + Geral.FF0(QrCTeEmpresa.Value),
  'AND Terceiro=' + Geral.FF0(QrCTeTerceiro.Value),
  'AND SerCT=' + Geral.FF0(QrCTeSerCT.Value),
  'AND nCT=' + Geral.FF0(QrCTenCT.Value),
  '']);
end;

end.
