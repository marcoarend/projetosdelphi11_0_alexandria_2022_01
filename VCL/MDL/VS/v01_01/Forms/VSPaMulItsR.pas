unit VSPaMulItsR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, dmkCheckBox, UnProjGroup_Consts,
  Vcl.ComCtrls, dmkEditDateTimePicker, UnGrl_Vars, UnAppEnums;

type
  TFmVSPaMulItsR = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    Panel3: TPanel;
    Label5: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdMovimCod: TdmkDBEdit;
    Label3: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    Label9: TLabel;
    QrOrig: TmySQLQuery;
    DsOrig: TDataSource;
    QrOrigCodigo: TIntegerField;
    QrOrigControle: TIntegerField;
    QrOrigMovimCod: TIntegerField;
    QrOrigMovimNiv: TIntegerField;
    QrOrigMovimTwn: TIntegerField;
    QrOrigEmpresa: TIntegerField;
    QrOrigTerceiro: TIntegerField;
    QrOrigCliVenda: TIntegerField;
    QrOrigMovimID: TIntegerField;
    QrOrigLnkNivXtr1: TIntegerField;
    QrOrigLnkNivXtr2: TIntegerField;
    QrOrigDataHora: TDateTimeField;
    QrOrigPallet: TIntegerField;
    QrOrigGraGruX: TIntegerField;
    QrOrigPecas: TFloatField;
    QrOrigPesoKg: TFloatField;
    QrOrigAreaM2: TFloatField;
    QrOrigAreaP2: TFloatField;
    QrOrigValorT: TFloatField;
    QrOrigSrcMovID: TIntegerField;
    QrOrigSrcNivel1: TIntegerField;
    QrOrigSrcNivel2: TIntegerField;
    QrOrigSrcGGX: TIntegerField;
    QrOrigSdoVrtPeca: TFloatField;
    QrOrigSdoVrtPeso: TFloatField;
    QrOrigSdoVrtArM2: TFloatField;
    QrOrigObserv: TWideStringField;
    QrOrigSerieFch: TIntegerField;
    QrOrigFicha: TIntegerField;
    QrOrigMisturou: TSmallintField;
    QrOrigFornecMO: TIntegerField;
    QrOrigCustoMOKg: TFloatField;
    QrOrigCustoMOTot: TFloatField;
    QrOrigValorMP: TFloatField;
    QrOrigDstMovID: TIntegerField;
    QrOrigDstNivel1: TIntegerField;
    QrOrigDstNivel2: TIntegerField;
    QrOrigDstGGX: TIntegerField;
    QrOrigQtdGerPeca: TFloatField;
    QrOrigQtdGerPeso: TFloatField;
    QrOrigQtdGerArM2: TFloatField;
    QrOrigQtdGerArP2: TFloatField;
    QrOrigQtdAntPeca: TFloatField;
    QrOrigQtdAntPeso: TFloatField;
    QrOrigQtdAntArM2: TFloatField;
    QrOrigQtdAntArP2: TFloatField;
    QrOrigAptoUso: TSmallintField;
    QrOrigNotaMPAG: TFloatField;
    QrOrigMarca: TWideStringField;
    QrOrigLk: TIntegerField;
    QrOrigDataCad: TDateField;
    QrOrigDataAlt: TDateField;
    QrOrigUserCad: TIntegerField;
    QrOrigUserAlt: TIntegerField;
    QrOrigAlterWeb: TSmallintField;
    QrOrigAtivo: TSmallintField;
    QrOrigTpCalcAuto: TIntegerField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrVSRibCla: TmySQLQuery;
    QrVSRibClaGraGruX: TIntegerField;
    QrVSRibClaNO_PRD_TAM_COR: TWideStringField;
    QrVSRibClaPrevPcPal: TIntegerField;
    DsVSRibCla: TDataSource;
    QrVSRibClaGraGruY: TIntegerField;
    QrVSPallet: TmySQLQuery;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    QrVSPalletLk: TIntegerField;
    QrVSPalletDataCad: TDateField;
    QrVSPalletDataAlt: TDateField;
    QrVSPalletUserCad: TIntegerField;
    QrVSPalletUserAlt: TIntegerField;
    QrVSPalletAlterWeb: TSmallintField;
    QrVSPalletAtivo: TSmallintField;
    QrVSPalletEmpresa: TIntegerField;
    QrVSPalletNO_EMPRESA: TWideStringField;
    QrVSPalletStatus: TIntegerField;
    QrVSPalletCliStat: TIntegerField;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletNO_CLISTAT: TWideStringField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletNO_STATUS: TWideStringField;
    DsVSPallet: TDataSource;
    Panel5: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label8: TLabel;
    EdValorMP: TdmkEdit;
    Label12: TLabel;
    EdCustoMOKg: TdmkEdit;
    Label13: TLabel;
    EdCustoMOTot: TdmkEdit;
    Label14: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    Label7: TLabel;
    EdValorT: TdmkEdit;
    Label16: TLabel;
    EdObserv: TdmkEdit;
    SBPallet: TSpeedButton;
    SBNewPallet: TSpeedButton;
    DBEdCacCod: TdmkDBEdit;
    Label15: TLabel;
    Label6: TLabel;
    EdCtrlGera: TdmkEdit;
    EdCtrlBaix: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    EdMovimTwn: TdmkEdit;
    Label17: TLabel;
    EdCACI: TdmkEdit;
    QrRevisores: TmySQLQuery;
    QrRevisoresCodigo: TIntegerField;
    QrRevisoresNOMEENTIDADE: TWideStringField;
    DsRevisores: TDataSource;
    QrDigitadores: TmySQLQuery;
    QrDigitadoresCodigo: TIntegerField;
    QrDigitadoresNOMEENTIDADE: TWideStringField;
    DsDigitadores: TDataSource;
    CBDigitador: TdmkDBLookupComboBox;
    EdDigitador: TdmkEditCB;
    Label71: TLabel;
    Label70: TLabel;
    EdRevisor: TdmkEditCB;
    CBRevisor: TdmkDBLookupComboBox;
    QrVSCacItsA: TmySQLQuery;
    QrVSCacItsACacCod: TIntegerField;
    QrVSCacItsACacID: TIntegerField;
    QrVSCacItsACodigo: TIntegerField;
    QrVSCacItsAControle: TLargeintField;
    QrVSCacItsAClaAPalOri: TIntegerField;
    QrVSCacItsARclAPalOri: TIntegerField;
    QrVSCacItsARclAPalDst: TIntegerField;
    QrVSCacItsAVSPaClaIts: TIntegerField;
    QrVSCacItsAVSPaRclIts: TIntegerField;
    QrVSCacItsAVSPallet: TIntegerField;
    QrVSCacItsAVMI_Sorc: TIntegerField;
    QrVSCacItsAVMI_Baix: TIntegerField;
    QrVSCacItsAVMI_Dest: TIntegerField;
    QrVSCacItsABox: TIntegerField;
    QrVSCacItsAPecas: TFloatField;
    QrVSCacItsAAreaM2: TFloatField;
    QrVSCacItsAAreaP2: TFloatField;
    QrVSCacItsARevisor: TIntegerField;
    QrVSCacItsADigitador: TIntegerField;
    QrVSCacItsADataHora: TDateTimeField;
    QrVSCacItsASumido: TSmallintField;
    QrVSCacItsAAlterWeb: TSmallintField;
    QrVSCacItsAAtivo: TSmallintField;
    QrVSCacItsAMartelo: TIntegerField;
    QrVSCacItsAFrmaIns: TSmallintField;
    Label18: TLabel;
    DBEdit3: TDBEdit;
    DataSource1: TDataSource;
    CkEncerraPallet: TdmkCheckBox;
    SbValorT: TSpeedButton;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Panel7: TPanel;
    Label49: TLabel;
    Label50: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    QrOrigVSMulFrnCab: TIntegerField;
    QrOrigClientMO: TIntegerField;
    TPData: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdHora: TdmkEdit;
    QrOrigNFeSer: TSmallintField;
    QrOrigNFeNum: TIntegerField;
    QrOrigVSMulNFeCab: TIntegerField;
    Panel9: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    RGIxxMovIX: TRadioGroup;
    EdIxxFolha: TdmkEdit;
    EdIxxLinha: TdmkEdit;
    SBNewByInfo: TSpeedButton;
    RGModoContinuarInserindo: TRadioGroup;
    Panel8: TPanel;
    CkContinuar: TCheckBox;
    SbStqCenLoc: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBPalletClick(Sender: TObject);
    procedure EdValorMPChange(Sender: TObject);
    procedure EdPesoKgChange(Sender: TObject);
    procedure EdCustoMOKgChange(Sender: TObject);
    procedure SBNewPalletClick(Sender: TObject);
    procedure EdAreaM2Change(Sender: TObject);
    procedure EdPalletKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbValorTClick(Sender: TObject);
    procedure SBNewByInfoClick(Sender: TObject);
    procedure EdPalletRedefinido(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure SbStqCenLocClick(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    GGX_Auto: Boolean;
    //
    //procedure ReopenVSPallet();
    procedure ReopenVSPaMulIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure CalculaCustos();
    procedure InsereCacItsA(SQLType: TSQLType; VSPallet, VMI_Sorc,
              VMI_Baix, VMI_Dest: Integer; Pecas, AreaM2, AreaP2: Double);
    procedure EncerraPallet(Pallet: Integer);
  public
    { Public declarations }
    FTabela: String;
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO, FVSGerArt: Integer;
    FSrcValorT, FSrcAreaM2: Double;
    FPallOnEdit: TArrSelIdxInt1;
    //
    procedure ReopenOrig(IMEI: Integer);
    procedure ReopenVsCacItsA(CacCod: Integer);
    procedure ReopenVSRibCla();
    procedure CalculaValorT();
  end;

  var
  FmVSPaMulItsR: TFmVSPaMulItsR;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  VSPaMulCabR, Principal, UnVS_CRC_PF, ModuleGeral, AppListas, VSPallet;

{$R *.DFM}

procedure TFmVSPaMulItsR.BtOKClick(Sender: TObject);
const
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  //SrcMovID   = TEstqMovimID(0);
  //DstMovID   = TEstqMovimID(0);
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  //MovimTwn   = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  //SrcGGX     = 0;
  //DstGGX     = 0;
  //
  CustoMOM2  = 0;
  TpCalcAuto = -1;
  //
  EdPalletMP = nil;
  EdAreaM2MP = nil;
  EdFicha    = nil;
  //
  PedItsLib = 0;
  PedItsVda = 0;
  PedItsFin = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe    = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  //
  JmpMovID: TEstqMovimID = emidAjuste;
  JmpNivel1: Integer = 0;
  JmpNivel2: Integer = 0;
  JmpGGX: Integer = 0;
  RmsMovID: TEstqMovimID = emidAjuste;
  RmsNivel1: Integer = 0;
  RmsNivel2: Integer = 0;
  RmsGGX: Integer = 0;
  GSPJmpMovID: TEstqMovimID = emidAjuste;
  GSPJmpNiv2: Integer = 0;
  MovCodPai: Integer = 0;
var
  SrcMovID, DstMovID: TEstqMovimID;
  DataHora, Observ, Marca, DtHrFimCla: String;
  Pallet, Codigo, (*Controle,*) MovimCod, Empresa, GraGruX, Terceiro, Ficha,
  GraGruY, FornecMO, SerieFch, SrcNivel1, SrcNivel2, SrcGGX, DstGGX, DstNivel1,
  DstNivel2, CtrlGera, CtrlBaix, MovimTwn, StqCenLoc, ReqMovEstq,
  VSMulFrnCab, ClientMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, CustoMOKg, CustoMOTot, ValorMP, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  EdPalletX, EdAreaM2X: TdmkEdit;
  SQLType: TSQLType;
  IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  //Controle       := 0;//EdControle.ValueVariant;
  CtrlGera       := EdCtrlGera.ValueVariant;
  CtrlBaix       := EdCtrlBaix.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClientMO       := QrOrigClientMO.Value;
  Terceiro       := QrOrigTerceiro.Value;
  VSMulFrnCab    := QrOrigVSMulFrnCab.Value;
  //DataHora       := Geral.FDT(FDataHora, 109);
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  MovimID        := emidReclasXXMul;
  Pallet         := EdPallet.ValueVariant;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  CustoMOKg      := EdCustoMOKg.ValueVariant;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  ValorMP        := EdValorMP.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  Ficha          := QrOrigFicha.Value;
  GraGruY        := QrVSRibClaGraGruY.Value;
  FornecMO       := EdFornecMO.ValueVariant;
  SerieFch       := QrOrigSerieFch.Value;
  Marca          := QrOrigMarca.Value;
  MovimTwn       := EdMovimTwn.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  case GraGruY of
    CO_GraGruY_0512_VSSubPrd,
    CO_GraGruY_0683_VSPSPPro,
    CO_GraGruY_0853_VSPSPEnd,
    CO_GraGruY_1024_VSNatCad,
    CO_GraGruY_1072_VSNatInC,
    CO_GraGruY_1088_VSNatCon,
    CO_GraGruY_2048_VSRibCad:
    begin
      EdPalletX := nil;
      EdAreaM2X := nil;
    end;
    CO_GraGruY_3072_VSRibCla:
    begin
      EdPalletX := EdPallet;
      EdAreaM2X := EdAreaM2;
    end;
    CO_GraGruY_4096_VSRibOpe,
    CO_GraGruY_5120_VSWetEnd,
    CO_GraGruY_6144_VSFinCla:
    begin
      EdPalletX := nil;
      EdAreaM2X := EdAreaM2;
    end;
    else
    begin
      Geral.MB_Erro('"GraGruY" n�o implementado em "FmVSPaMulIts.BtOKClick()"');
    end;
  end;
  //
(*
  if VS_CRC_PF.FichaErro(EdSerieFch, Empresa, Controle, Ficha) then
    Exit;
*)
  //
  MovimNiv := eminDestClass;
  if ValorT = 0 then
    ValorT := 0.01;
  //
  //
  if MyObjects.FIC(FornecMO = 0, EdFornecMO,
    'Informe o fornecedor da m�o de obra!') then Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc,
    'Informe o local do estoque!') then Exit;
  //
  IxxMovIX       := TEstqMovInfo(RGIxxMovIX.ItemIndex);
  IxxFolha       := EdIxxFolha.ValueVariant;
  IxxLinha       := EdIxxLinha.ValueVariant;
  if VS_CRC_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha) then
    Exit;
  if MyObjects.FIC_MCI(CkContinuar, RGModoContinuarInserindo) then
    Exit;
  //
  if VS_CRC_PF.VSFic(GraGruX, Empresa, ClientMO, Terceiro, Pallet, Ficha, Pecas,
    AreaM2, PesoKg, ValorT, EdGraGruX, EdPalletX, EdFicha, EdPecas, EdAreaM2X,
    EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca, EdStqCenLoc)
  then
    Exit;
  //
  MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos,
    SQLType, MovimTwn);
  CtrlGera := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos,
    SQLType, CtrlGera);
  CtrlBaix := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos,
    SQLType, CtrlBaix);
  // Novo Artigo
  SrcMovID  := TEstqMovimID(0);
  SrcNivel1 := Codigo;
  SrcNivel2 := 0;
  SrcGGX    := 0;
  //
  DstMovID  := TEstqMovimID(QrOrigMovimID .Value);
  DstNivel1 := QrOrigCodigo.Value;
  DstNivel2 := QrOrigControle.Value;
  DstGGX    := QrOrigGraGruX.Value;
  //
  if VS_CRC_PF.InsUpdVSMovIts3(SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, CtrlGera, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2,
  RmsGGX, GSPJmpMovID, GSPJmpNiv2, MovCodPai,  CO_0_VmiPai,
  IxxMovIX, IxxFolha, IxxLinha,
(*
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
*)
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei051(*Baixa de artigo de ribeira em reclassifica��o (MUL)*)) then
  begin
    //  Baixa
    //
    DstMovID  := MovimID;
    DstNivel1 := Codigo;
    DstNivel2 := CtrlGera;
    DstGGX    := GraGruX;
    //
    SrcMovID  := TEstqMovimID(QrOrigMovimID .Value);
    SrcNivel1 := QrOrigCodigo.Value;
    SrcNivel2 := QrOrigControle.Value;
    SrcGGX    := QrOrigGraGruX.Value;
    GraGruX   := SrcGGX;
    //
    MovimNiv := eminSorcClass;
    //
    //StqCenLoc      := 0;
    ReqMovEstq     := 0;
    //
    if VS_CRC_PF.InsUpdVSMovIts3(SQLType, Codigo, MovimCod, MovimTwn, Empresa,
    Terceiro, MovimID, MovimNiv, Pallet, GraGruX, -Pecas, -PesoKg, -AreaM2, -AreaP2,
    ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
    LnkNivXtr2, CliVenda, (*Controle*)CtrlBaix, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot,
    ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
    QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    JmpMovID, JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2,
    RmsGGX, GSPJmpMovID, GSPJmpNiv2, MovCodPai, CO_0_VmiPai,
    IxxMovIX, IxxFolha, IxxLinha,
(*
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
*)
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iuvpei050(*Reclassifica��o de artigo de ribeira (MUL)*)) then
    begin
      InsereCacItsA(SQLType, Pallet, SrcNivel2, CtrlBaix, CtrlGera, Pecas,
        AreaM2, AreaP2);
      VS_CRC_PF.AtualizaTotaisVSXxxCab('vspamulcaba', MovimCod);
      VS_CRC_PF.AtualizaSaldoIMEI(CtrlGera, True);
      VS_CRC_PF.AtualizaSaldoIMEI(QrOrigControle.Value, True);
      FmVSPaMulCabR.AtualizaNFeItens();
      if CkEncerraPallet.Checked then
        EncerraPallet(Pallet);
      // 2015-07-10
      DtHrFimCla := Geral.FDT(DmodG.ObtemAgora(), 109);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
      'DtHrFimCla'
      ], ['Codigo'], [
      DtHrFimCla
      ], [FVSGerArt], True);
      // Fim 2015-07-10
      FmVSPaMulCabR.LocCod(Codigo, Codigo);
      FmVSPaMulCabR.ReopenVSPaMulIts(CtrlGera);
      if CkContinuar.Checked then
      begin
        GGX_Auto := False;
        EdValorT.Enabled           := False;
        ImgTipo.SQLType            := stIns;
        EdCtrlGera.ValueVariant    := 0;
        EdCtrlBaix.ValueVariant    := 0;
        if not VS_CRC_PF.ConfigContinuarInserindoFolhaLinha(
          RGModoContinuarInserindo, EdIxxFolha, EdIxxLinha) then
        begin
          EdGraGruX.ValueVariant     := 0;
          CBGraGruX.KeyValue         := Null;
          //EdFornecedor.ValueVariant  := 0;
          //CBFornecedor.KeyValue      := Null;
          //EdSerieFch.ValueVariant    := 0;
          //CBSerieFch.KeyValue        := Null;
          //EdFicha.ValueVariant       := 0;
          EdPallet.ValueVariant      := 0;
          CBPallet.KeyValue          := Null;
          EdFornecMO.ValueVariant    := 0;
          CBFornecMO.KeyValue        := Null;
        end;
        EdValorT.ValueVariant      := 0;
        EdCustoMOTot.ValueVariant  := 0;
        EdPecas.ValueVariant       := 0;
        EdPesoKg.ValueVariant      := 0;
        EdAreaM2.ValueVariant      := 0;
        EdAreaP2.ValueVariant      := 0;
        EdCustoMOKg.ValueVariant   := 0;
        EdObserv.Text              := '';
        //
        EdValorT.ValueVariant      := 0;
        EdValorMP.ValueVariant     := 0;
        EdCustoMOTot.ValueVariant  := 0;
        //
        SBNewPallet.Enabled := True;
        //
        EdGraGruX.SetFocus;
      end else
        Close;
    end;
  end;
end;

procedure TFmVSPaMulItsR.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPaMulItsR.CalculaCustos();
var
  ValorMP, CustoMOKg, CustoMOTot, ValorT, PesoKg: Double;
begin
  ValorMP      := EdValorMP.ValueVariant;
  CustoMOKg    := EdCustoMOKg.ValueVariant;
  PesoKg       := EdPesoKg.ValueVariant;
  //
  CustoMOTot := CustoMOKg * PesoKg;
  EdCustoMOTot.ValueVariant := CustoMOTot;
  ValorT := ValorMP + CustoMOTot;
  EdValorT.ValueVariant := ValorT;
end;

procedure TFmVSPaMulItsR.CalculaValorT();
var
  CustoM2, ValorT: Double;
begin
  if ImgTipo.SQLType <> stIns then
    Exit;
  //
  if FSrcAreaM2 <> 0 then
    CustoM2 := FSrcValorT / FSrcAreaM2
  else
    CustoM2 := 0;
  //
  ValorT := EdAreaM2.ValueVariant * CustoM2;
  EdValorT.ValueVariant := ValorT;
end;

procedure TFmVSPaMulItsR.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSPaMulItsR.EdAreaM2Change(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSPaMulItsR.EdCustoMOKgChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSPaMulItsR.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSPaMulItsR.EdGraGruXRedefinido(Sender: TObject);
begin
  if EdGraGruX.Focused or CBGraGruX.Focused then
    GGX_Auto := False;
end;

procedure TFmVSPaMulItsR.EdPalletKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Pallet, GraGruX: Integer;
begin
  if Key = VK_INSERT then
  begin
    if SBNewPallet.Enabled then
      SBNewPalletClick(Self);
  end else
  if (Key = Ord('d')) or (Key = Ord('D')) then
    if Shift = [ssCtrl] then
    begin
      Pallet  := EdPallet.ValueVariant;
      GraGruX := EdGraGruX.ValueVariant;
      if VS_CRC_PF.DesfazEncerramentoPallet(Pallet, GraGruX) then
      begin
        VS_CRC_PF.ReopenVSPallet(QrVSPallet, FEmpresa, FClientMO, 0, '', FPallOnEdit);
        EdPallet.ValueVariant := Pallet;
        CBPallet.KeyValue     := Pallet;
      end;
    end;
end;

procedure TFmVSPaMulItsR.EdPalletRedefinido(Sender: TObject);
begin
  if (EdPallet.ValueVariant <> 0) then
  begin
    if GGX_Auto or (EdGraGruX.ValueVariant = 0) then
    begin
      GGX_Auto := True;
      EdGraGruX.ValueVariant := QrVSPalletGraGruX.Value;
      CBGraGruX.KeyValue     := QrVSPalletGraGruX.Value;
    end;
  end;
end;

procedure TFmVSPaMulItsR.EdPesoKgChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSPaMulItsR.EdValorMPChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSPaMulItsR.EncerraPallet(Pallet: Integer);
const
  EncerrandoTodos = False;
  FromRcl  = False;
  Pergunta = True;
  Encerra  = True;
  OC       = 0;
var
  FromBox: Variant;
begin
  FromBox := Null;
(*
  VS_CRC_PF.EncerraPalletOld(Pallet, OC, EncerrandoTodos, FromBox, emidAjuste,
    Pergunta, Encerra, FromRcl);
*)
  VS_CRC_PF.EncerraPalletNew(Pallet, Pergunta);
  VS_CRC_PF.ReopenVSPallet(QrVSPallet, FEmpresa, FClientMO, 0, '', FPallOnEdit);
end;

procedure TFmVSPaMulItsR.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdCacCod.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSPaMulItsR.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  SetLength(FPallOnEdit, 0);
  //
  //UnDmkDAC_PF.AbreQuery(QrVSRibCla, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrRevisores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDigitadores, Dmod.MyDB);
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0,
    TEstqMovimID.emidReclasXXMul);
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  GGX_Auto := False;
end;

procedure TFmVSPaMulItsR.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPaMulItsR.InsereCacItsA(SQLType: TSQLType; VSPallet, VMI_Sorc,
VMI_Baix, VMI_Dest: Integer; Pecas, AreaM2, AreaP2: Double);
var
  DataHora: String;
  CacCod, CacID, Codigo, ClaAPalOri, RclAPalOri, RclAPalDst, VSPaClaIts,
  //VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest, , Pecas, AreaM2, AreaP2
  VSPaRclIts, Box, Revisor, Digitador,
  Sumido, Martelo, FrmaIns: Integer;
  Controle: Int64;
begin
  CacCod         := Geral.IMV(DBEdCacCod.Text);
  CacID          := Integer(TEstqMovimID.emidReclasXXMul);  // 14
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdCACI.ValueVariant;
  ClaAPalOri     := 0;
  RclAPalOri     := 0;
  RclAPalDst     := 0;
  VSPaClaIts     := 0;
  VSPaRclIts     := 0;
  (*VSPallet       := EdPallet.ValueVariant;
  VMI_Sorc       := ;
  VMI_Baix       := ;
  VMI_Dest       := ;*)
  Box            := -1;
  (*Pecas          := ;
  AreaM2         := ;
  AreaP2         := ;*)
  Revisor        := EdRevisor.ValueVariant;
  Digitador      := EdDigitador.ValueVariant;
  // Nao tem xxxxCad e xxxxAlt
  //if SQLType = stIns then
    DataHora     := Geral.FDT(DModG.ObtemAgora(), 109);
  //else
    //DataHora     := Geral.FDT(QrVSCacItsADataHora.Value, 109);
  Sumido         := 0;
  Martelo        := 0;
  FrmaIns        := 3; // Manual
  //
  Controle :=
    UMyMod.BPGS1I64('vscacitsa', 'Controle', '', '', tsPos, SQLType, Controle);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscacitsa', False, [
  'CacCod', 'CacID', 'Codigo',
  'ClaAPalOri', 'RclAPalOri', 'RclAPalDst',
  'VSPaClaIts', 'VSPaRclIts', 'VSPallet',
  'VMI_Sorc', 'VMI_Baix', 'VMI_Dest',
  'Box', 'Pecas', 'AreaM2',
  'AreaP2', 'Revisor', 'Digitador',
  CO_DATA_HORA_GRL, 'Sumido', 'Martelo',
  'FrmaIns'], [
  'Controle'], [
  CacCod, CacID, Codigo,
  ClaAPalOri, RclAPalOri, RclAPalDst,
  VSPaClaIts, VSPaRclIts, VSPallet,
  VMI_Sorc, VMI_Baix, VMI_Dest,
  Box, Pecas, AreaM2,
  AreaP2, Revisor, Digitador,
  DataHora, Sumido, Martelo,
  FrmaIns], [
  Controle], VAR_InsUpd_AWServerID);
end;

procedure TFmVSPaMulItsR.ReopenOrig(IMEI: Integer);
  procedure Abre(Tabela: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrOrig, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + Tabela,
    'WHERE Controle=' + Geral.FF0(IMEI),
    '']);
  end;
begin
  Abre(CO_SEL_TAB_VMI);
  if QrOrig.RecordCount = 0 then
    Abre(CO_TAB_VMB);
end;

procedure TFmVSPaMulItsR.ReopenVsCacItsA(CacCod: Integer);
var
  VMI_Sorc, VMI_Dest, VMI_Baix: Integer;
begin
  //CacCod   := Geral.IMV(DBEdCacCod.Text);
  VMI_Sorc := QrOrigControle.Value;
  VMI_Dest := EdCtrlGera.ValueVariant;
  VMI_Baix := EdCtrlBaix.ValueVariant;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrVSCacItsA, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  'WHERE CacCod=' + Geral.FF0(CacCod),
  'AND VMI_Sorc=' + Geral.FF0(VMI_Sorc),
  'AND VMI_Baix=' + Geral.FF0(VMI_Baix),
  'AND VMI_Dest=' + Geral.FF0(VMI_Dest),
  '']);
end;

procedure TFmVSPaMulItsR.ReopenVSPaMulIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSPaMulItsR.ReopenVSRibCla();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSRibCla, Dmod.MyDB, [
  'SELECT wmp.GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, cou.PrevPcPal, ggx.GraGruY ',
  'FROM ' + FTabela + ' wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

(*
procedure TFmVSPaMulIts.ReopenVSPallet();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM vspalleta ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
end;
*)

procedure TFmVSPaMulItsR.SBNewByInfoClick(Sender: TObject);
begin
  VS_CRC_PF.CadastraPalletInfo(FEmpresa, FClientMO, EdGraGruX.ValueVariant,
  QrVSRibClaGraGruY.Value, QrVSPallet, EdPallet, CBPallet, SBNewPallet,
  EdPecas);
end;

procedure TFmVSPaMulItsR.SBNewPalletClick(Sender: TObject);
var
  Nome, DtHrEndAdd: String;
  Codigo, Empresa, ClientMO, Status, CliStat, GraGruX, MovimIDGer, QtdPrevPc: Integer;
begin
  Codigo         := 0;
  Nome           := '';
  //(*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  Status         := 0; //EdStatus.ValueVariant;
  CliStat        := 0; //EdCliStat.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  DtHrEndAdd     := Geral.FDT(0, 109); //DModG.ObtemAgora(), 109);
  MovimIDGer     := 0; // Altera depois?
  QtdPrevPc      := 0;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('VSPalleta', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'VSPalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'DtHrEndAdd',
  'MovimIDGer', 'QtdPrevPc', 'ClientMO'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, DtHrEndAdd,
  MovimIDGer, QtdPrevPc, ClientMO], [
  Codigo], True) then
  begin
    VS_CRC_PF.AtualizaStatPall(Codigo);
    VS_CRC_PF.ReopenVSPallet(QrVSPallet, FEmpresa, FClientMO, 0, '', FPallOnEdit);
    EdPallet.ValueVariant := Codigo;
    CBPallet.KeyValue := Codigo;
    //
    SBNewPallet.Enabled := False;
  end;
end;

procedure TFmVSPaMulItsR.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_CRC_PF.MostraFormVSPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPallet, CBPallet, QrVSPallet, VAR_CADASTRO);
end;

procedure TFmVSPaMulItsR.SbStqCenLocClick(Sender: TObject);
begin
  VS_CRC_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, TEstqMovimID.emidReclasXXMul);
end;

procedure TFmVSPaMulItsR.SbValorTClick(Sender: TObject);
begin
  EdValorT.Enabled := True;
  EdValorT.SetFocus;
end;

procedure TFmVSPaMulItsR.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
