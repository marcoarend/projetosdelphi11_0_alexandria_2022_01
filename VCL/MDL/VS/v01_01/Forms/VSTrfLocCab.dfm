object FmVSTrfLocCab: TFmVSTrfLocCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-131 :: Transfer'#234'ncia de Local de Estoque VS'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 245
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 476
        Top = 16
        Width = 91
        Height = 13
        Caption = 'Data / hora venda:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 780
        Top = 16
        Width = 97
        Height = 13
        Caption = 'Data / hora entrega:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 56
        Width = 42
        Height = 13
        Caption = 'Terceiro:'
      end
      object Label5: TLabel
        Left = 504
        Top = 56
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object LaPecas: TLabel
        Left = 16
        Top = 96
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object LaAreaM2: TLabel
        Left = 104
        Top = 96
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object LaAreaP2: TLabel
        Left = 192
        Top = 96
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object LaPeso: TLabel
        Left = 280
        Top = 96
        Width = 27
        Height = 13
        Caption = 'Peso:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 932
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label28: TLabel
        Left = 368
        Top = 96
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label29: TLabel
        Left = 400
        Top = 96
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object LaCliente: TLabel
        Left = 72
        Top = 56
        Width = 13
        Height = 13
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtVenda: TdmkEditDateTimePicker
        Left = 476
        Top = 32
        Width = 108
        Height = 21
        Time = 0.639644131944805800
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtVenda'
        UpdCampo = 'DtVenda'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtVenda: TdmkEdit
        Left = 584
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtVenda'
        UpdCampo = 'DtVenda'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtViagem: TdmkEditDateTimePicker
        Left = 628
        Top = 32
        Width = 108
        Height = 21
        Time = 0.639644131944805800
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtViagem'
        UpdCampo = 'FimVisPrv'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtViagem: TdmkEdit
        Left = 736
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtEntrega: TdmkEditDateTimePicker
        Left = 780
        Top = 32
        Width = 108
        Height = 21
        Time = 0.639644131944805800
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtEntrega'
        UpdCampo = 'DtEntrega'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtEntrega: TdmkEdit
        Left = 888
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 8
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtEntrega'
        UpdCampo = 'DtEntrega'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 428
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCliente
        TabOrder = 11
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTransporta: TdmkEditCB
        Left = 504
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Transporta'
        UpdCampo = 'Transporta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransporta: TdmkDBLookupComboBox
        Left = 560
        Top = 72
        Width = 428
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransporta
        TabOrder = 13
        dmkEditCB = EdTransporta
        QryCampo = 'Transporta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPecas: TdmkEdit
        Left = 16
        Top = 112
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaM2: TdmkEditCalc
        Left = 104
        Top = 112
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 15
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 192
        Top = 112
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 16
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPesoKg: TdmkEdit
        Left = 280
        Top = 112
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 17
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 932
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 337
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object Edide_serie: TdmkEdit
        Left = 368
        Top = 112
        Width = 28
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_serie'
        UpdCampo = 'ide_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edide_nNF: TdmkEdit
        Left = 400
        Top = 112
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 19
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_nNF'
        UpdCampo = 'ide_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 137
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 476
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora fatura:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 780
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 16
        Top = 56
        Width = 42
        Height = 13
        Caption = 'Terceiro:'
      end
      object Label15: TLabel
        Left = 504
        Top = 56
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object Label16: TLabel
        Left = 16
        Top = 96
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label17: TLabel
        Left = 104
        Top = 96
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object Label18: TLabel
        Left = 192
        Top = 96
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
      end
      object Label19: TLabel
        Left = 280
        Top = 96
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label20: TLabel
        Left = 368
        Top = 96
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label21: TLabel
        Left = 400
        Top = 96
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSTrfLocCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSTrfLocCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 341
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSTrfLocCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 476
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtVenda'
        DataSource = DsVSTrfLocCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 628
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtViagem'
        DataSource = DsVSTrfLocCab
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 780
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtEntrega'
        DataSource = DsVSTrfLocCab
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsVSTrfLocCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsVSTrfLocCab
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 72
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_CLIENTE'
        DataSource = DsVSTrfLocCab
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 508
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Transporta'
        DataSource = DsVSTrfLocCab
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 564
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_TRANSPORTA'
        DataSource = DsVSTrfLocCab
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 16
        Top = 112
        Width = 84
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSTrfLocCab
        TabOrder = 11
      end
      object DBEdit12: TDBEdit
        Left = 280
        Top = 112
        Width = 84
        Height = 21
        DataField = 'PesoKg'
        DataSource = DsVSTrfLocCab
        TabOrder = 12
      end
      object DBEdit13: TDBEdit
        Left = 104
        Top = 112
        Width = 84
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVSTrfLocCab
        TabOrder = 13
      end
      object DBEdit14: TDBEdit
        Left = 192
        Top = 112
        Width = 84
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVSTrfLocCab
        TabOrder = 14
      end
      object DBEdit15: TDBEdit
        Left = 368
        Top = 112
        Width = 28
        Height = 21
        DataField = 'ide_serie'
        DataSource = DsVSTrfLocCab
        TabOrder = 15
      end
      object DBEdit16: TDBEdit
        Left = 400
        Top = 112
        Width = 64
        Height = 21
        DataField = 'ide_nNF'
        DataSource = DsVSTrfLocCab
        TabOrder = 16
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 260
        Top = 15
        Width = 746
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 613
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 421
          Left = 126
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtVSOutNFeCab: TBitBtn
          Tag = 456
          Left = 248
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&NFe Cab'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtVSOutNFeCabClick
        end
        object BtTribIncIts: TBitBtn
          Tag = 502
          Left = 492
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tributos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtTribIncItsClick
        end
        object BtVSOutNFeIts: TBitBtn
          Tag = 575
          Left = 370
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'N&Fe Itm'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtVSOutNFeItsClick
        end
      end
    end
    object GBIts: TGroupBox
      Left = 0
      Top = 137
      Width = 1008
      Height = 228
      Align = alTop
      Caption = ' Itens da sa'#237'da: '
      TabOrder = 2
      object PCItens: TPageControl
        Left = 2
        Top = 15
        Width = 1004
        Height = 211
        ActivePage = TsRetornoMO
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Por Pallet'
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 996
            Height = 183
            Align = alClient
            DataSource = DsPallets
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = DBGrid1DrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Tabela'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SEQ'
                Title.Caption = 'Item'
                Width = 27
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Title.Caption = 'ID Pallet'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TXT_ARTIGO'
                Title.Caption = 'Artigo'
                Width = 280
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ClasseImp'
                Title.Caption = 'Classe'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Width = 82
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MediaM2'
                Title.Caption = 'M'#233'dia m'#178'/pc'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'FaixaMediaM2'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'Faixa m'#233'dia'
                Width = 60
                Visible = True
              end>
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Por IME-I'
          ImageIndex = 1
          object Label9: TLabel
            Left = 0
            Top = 111
            Width = 140
            Height = 13
            Align = alBottom
            Caption = 'Baixas do IME-I selecionado: '
          end
          object DGDados: TDBGrid
            Left = 0
            Top = 0
            Width = 996
            Height = 111
            Align = alClient
            DataSource = DsIMEIDest
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DGDadosDblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Tabela'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IxxMovIX'
                Title.Caption = 'IXX'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IxxFolha'
                Title.Caption = 'Folha'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IxxLinha'
                Title.Caption = 'Lin'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ItemNFe'
                Title.Caption = 'NFi'
                Width = 22
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ReqMovEstq'
                Title.Caption = 'N'#176' RME'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Title.Caption = 'Ficha RMP'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Title.Caption = 'ID Pallet'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PALLET'
                Title.Caption = 'Pallet'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FORNECE'
                Title.Caption = 'Fornecedor'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 82
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMP'
                Title.Caption = 'Valor MP'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CusFrtAvuls'
                Title.Caption = 'Frete simples '
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CusFrtMOEnv'
                Title.Caption = 'Frete ida'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CusFrtMORet'
                Title.Caption = 'Frete volta'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Valor total'
                Width = 68
                Visible = True
              end>
          end
          object DBGrid3: TDBGrid
            Left = 0
            Top = 124
            Width = 996
            Height = 59
            Align = alBottom
            DataSource = DsIMEISrc
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Tabela'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end>
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Itens NF'
          ImageIndex = 2
          object DBGrid2: TDBGrid
            Left = 205
            Top = 0
            Width = 569
            Height = 183
            Align = alClient
            DataSource = DsVSOutNFeIts
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'ItemNFe'
                Title.Caption = 'NFi'
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Artigo / tamanho / cor'
                Width = 254
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorU'
                Title.Caption = 'Valor Unit.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Valor Total'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
          object GroupBox3: TGroupBox
            Left = 774
            Top = 0
            Width = 222
            Height = 183
            Align = alRight
            Caption = ' Impostos do Item: '
            TabOrder = 1
            object DBGrid4: TDBGrid
              Left = 2
              Top = 15
              Width = 218
              Height = 166
              Align = alClient
              DataSource = DsTribIncIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_Tributo'
                  Title.Caption = 'Tributo'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Percent'
                  Title.Caption = '%'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValrTrib'
                  Title.Caption = 'Valor '
                  Width = 72
                  Visible = True
                end>
            end
          end
          object DBGrid5: TDBGrid
            Left = 0
            Top = 0
            Width = 205
            Height = 183
            Align = alLeft
            DataSource = DsVSOutNFeCab
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ide_serie'
                Title.Caption = 'S'#233'rie'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ide_nNF'
                Title.Caption = 'N'#186' NFe'
                Visible = True
              end>
          end
        end
        object TsFrCompr: TTabSheet
          Caption = 'Dados frete simples envio'
          ImageIndex = 3
          object PnFrCompr: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 183
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGVSMOEnvAvu: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 529
              Height = 183
              Align = alLeft
              DataSource = DsVSMOEnvAvu
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_nCT'
                  Title.Caption = 'N'#186' CT'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_AreaM2'
                  Title.Caption = #193'rea m'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_PesoKg'
                  Title.Caption = 'Peso kg'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_ValorT'
                  Title.Caption = 'Valor frete'
                  Width = 72
                  Visible = True
                end>
            end
            object DBGVSMOEnvAVMI: TdmkDBGridZTO
              Left = 529
              Top = 0
              Width = 467
              Height = 183
              Align = alClient
              DataSource = DsVSMOEnvAVMI
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VSMovIts'
                  Title.Caption = 'IME-I'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorFrete'
                  Title.Caption = '$ Frete'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso kg'
                  Width = 72
                  Visible = True
                end>
            end
          end
        end
        object TsEnvioMO: TTabSheet
          Caption = ' Dados do envio para retorno'
          ImageIndex = 4
          object PnEnvioMO: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 183
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitWidth = 1000
            ExplicitHeight = 92
            object DBGVSMOEnvEnv: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 529
              Height = 183
              Align = alLeft
              DataSource = DsVSMOEnvEnv
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFEMP_nNF'
                  Title.Caption = 'N'#186' NF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMP_nCT'
                  Title.Caption = 'N'#186' CT'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMP_Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMP_AreaM2'
                  Title.Caption = #193'rea m'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMP_PesoKg'
                  Title.Caption = 'Peso kg'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMP_ValorT'
                  Title.Caption = 'Valor frete'
                  Width = 72
                  Visible = True
                end>
            end
            object DBGVSMOEnvEVmi: TdmkDBGridZTO
              Left = 529
              Top = 0
              Width = 467
              Height = 183
              Align = alClient
              DataSource = DsVSMOEnvEVMI
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VSMovIts'
                  Title.Caption = 'IME-I'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorFrete'
                  Title.Caption = '$ Frete'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso kg'
                  Width = 72
                  Visible = True
                end>
            end
          end
        end
        object TsRetornoMO: TTabSheet
          Caption = ' Retorno do envio'
          ImageIndex = 5
          object PnRetornoMO: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 183
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGVSMOEnvRet: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 661
              Height = 183
              Align = alLeft
              DataSource = DsVSMOEnvRet
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTPA_nCT'
                  Title.Caption = 'N'#186' CT'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTPA_AreaM2'
                  Title.Caption = 'Area m'#178
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTPA_PesoKg'
                  Title.Caption = 'Peso kg'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTPA_ValorT'
                  Title.Caption = 'Valor frete'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTPA_Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFRMP_SerNF'
                  Title.Caption = 'S'#233'r. Ret'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFRMP_nNF'
                  Title.Caption = 'NF Retorno'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFCMO_SerNF'
                  Title.Caption = 'S'#233'r. M.O.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFCMO_nNF'
                  Title.Caption = 'NF M.O.'
                  Visible = True
                end>
            end
            object PnItensRetMO: TPanel
              Left = 661
              Top = 0
              Width = 335
              Height = 183
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Splitter2: TSplitter
                Left = 0
                Top = 68
                Width = 335
                Height = 5
                Cursor = crVSplit
                Align = alBottom
                ExplicitLeft = 585
                ExplicitTop = 1
                ExplicitWidth = 208
              end
              object GroupBox8: TGroupBox
                Left = 0
                Top = 0
                Width = 335
                Height = 68
                Align = alClient
                Caption = ' Itens de NFes enviados para M.O.: '
                TabOrder = 0
                object DBGVSMOEnvRVmi: TdmkDBGridZTO
                  Left = 2
                  Top = 15
                  Width = 331
                  Height = 51
                  TabStop = False
                  Align = alClient
                  DataSource = DsVSMOEnvRVmi
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NFEMP_SerNF'
                      Title.Caption = 'S'#233'r. NF'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NFEMP_nNF'
                      Title.Caption = 'NF'
                      Width = 65
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Pecas'
                      Title.Caption = 'Pe'#231'as'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaM2'
                      Title.Caption = #193'rea m'#178
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PesoKg'
                      Title.Caption = 'Peso kg'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaP2'
                      Title.Caption = #193'rea ft'#178
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorT'
                      Title.Caption = 'Valor Total'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorFrete'
                      Title.Caption = 'Valor Frete'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Title.Caption = 'ID Item'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VSMOEnvEnv'
                      Title.Caption = 'ID Envio'
                      Visible = True
                    end>
                end
              end
              object GroupBox9: TGroupBox
                Left = 0
                Top = 73
                Width = 335
                Height = 110
                Align = alBottom
                Caption = ' IME-Is de couros prontos retornados: '
                TabOrder = 1
                object DBGVSMOEnvGVmi: TdmkDBGridZTO
                  Left = 2
                  Top = 15
                  Width = 331
                  Height = 93
                  TabStop = False
                  Align = alClient
                  DataSource = DsVSMOEnvGVmi
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Title.Caption = 'ID Item'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VSMovIts'
                      Title.Caption = 'IME-I'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Pecas'
                      Title.Caption = 'Pe'#231'as'
                      Width = 62
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaM2'
                      Title.Caption = #193'rea m'#178
                      Width = 62
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PesoKg'
                      Title.Caption = 'Peso kg'
                      Width = 62
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorFrete'
                      Visible = True
                    end>
                end
              end
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 464
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 464
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 464
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 64
    Top = 56
  end
  object QrVSTrfLocCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSTrfLocCabBeforeOpen
    AfterOpen = QrVSTrfLocCabAfterOpen
    BeforeClose = QrVSTrfLocCabBeforeClose
    AfterScroll = QrVSTrfLocCabAfterScroll
    SQL.Strings = (
      'SELECT wic.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA'
      'FROM vsinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.transporta')
    Left = 312
    Top = 293
    object QrVSTrfLocCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSTrfLocCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSTrfLocCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSTrfLocCabDtVenda: TDateTimeField
      FieldName = 'DtVenda'
    end
    object QrVSTrfLocCabDtViagem: TDateTimeField
      FieldName = 'DtViagem'
    end
    object QrVSTrfLocCabDtEntrega: TDateTimeField
      FieldName = 'DtEntrega'
    end
    object QrVSTrfLocCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSTrfLocCabTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrVSTrfLocCabPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrVSTrfLocCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrVSTrfLocCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSTrfLocCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSTrfLocCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSTrfLocCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSTrfLocCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSTrfLocCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSTrfLocCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSTrfLocCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSTrfLocCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSTrfLocCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSTrfLocCabNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrVSTrfLocCabNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
    object QrVSTrfLocCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSTrfLocCabide_serie: TSmallintField
      FieldName = 'ide_serie'
    end
    object QrVSTrfLocCabide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
  end
  object DsVSTrfLocCab: TDataSource
    DataSet = QrVSTrfLocCab
    Left = 312
    Top = 337
  end
  object QrIMEIDest: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrIMEIDestAfterOpen
    BeforeClose = QrIMEIDestBeforeClose
    AfterScroll = QrIMEIDestAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 396
    Top = 293
    object QrIMEIDestCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIMEIDestControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrIMEIDestMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrIMEIDestMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrIMEIDestMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrIMEIDestEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrIMEIDestTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrIMEIDestCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrIMEIDestMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrIMEIDestDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrIMEIDestPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrIMEIDestGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrIMEIDestPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIDestPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIDestAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrIMEIDestSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrIMEIDestSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrIMEIDestSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrIMEIDestSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrIMEIDestSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIDestSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrIMEIDestSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrIMEIDestFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrIMEIDestMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrIMEIDestFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrIMEIDestCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrIMEIDestDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrIMEIDestDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrIMEIDestDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrIMEIDestQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrIMEIDestQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIDestQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrIMEIDestQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIDestQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrIMEIDestPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrIMEIDestMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEIDestStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrIMEIDestNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrIMEIDestNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrIMEIDestNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEIDestNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrIMEIDestID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrIMEIDestReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrIMEIDestIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
    end
    object QrIMEIDestIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
    end
    object QrIMEIDestIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
    end
    object QrIMEIDestCusFrtAvuls: TFloatField
      FieldName = 'CusFrtAvuls'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrIMEIDestCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrIMEIDestCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsIMEIDest: TDataSource
    DataSet = QrIMEIDest
    Left = 396
    Top = 337
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 416
    Top = 568
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      object PorPallet1: TMenuItem
        Caption = 'Por &Pallet'
        OnClick = PorPallet1Click
      end
      object PorIMEI1: TMenuItem
        Caption = 'Por &IME-I'
        OnClick = PorIMEI1Click
      end
      object Porkgsubproduto1: TMenuItem
        Caption = 'Por kg (sub produto)'
        Visible = False
        OnClick = Porkgsubproduto1Click
      end
    end
    object ItsExcluiIMEI1: TMenuItem
      Caption = '&Remove o IME-I'
      Enabled = False
      OnClick = ItsExcluiIMEI1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object AtrelamentoFreteCompra1: TMenuItem
      Caption = 'Atrelamento Frete simples envio'
      object IncluiAtrelamento3: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento3Click
      end
      object AlteraAtrelamento3: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento3Click
      end
      object ExcluiAtrelamento3: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento3Click
      end
    end
    object AtrelamentoNFsdeMO1: TMenuItem
      Caption = 'Atrelamento NFs de envio (para posterior retorno)'
      object IncluiAtrelamento1: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento1Click
      end
      object AlteraAtrelamento1: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento1Click
      end
      object ExcluiAtrelamento1: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento1Click
      end
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object InformaNmerodaRME1: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      OnClick = InformaNmerodaRME1Click
    end
    object InformaItemdeNFe1: TMenuItem
      Caption = '&Informa Item de NFe (NFi)'
    end
    object InformaIEC1: TMenuItem
      Caption = 'Informa P'#225'gina e linha de IEC'
      OnClick = InformaIEC1Click
    end
    object AlteraLocaldeestoque1: TMenuItem
      Caption = 'Altera &Local de estoque'
      OnClick = AlteraLocaldeestoque1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 288
    Top = 576
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
      Visible = False
    end
    object CorrigeFornecedores1: TMenuItem
      Caption = 'Corrige Fornecedores'
      Visible = False
      OnClick = CorrigeFornecedores1Click
    end
  end
  object QrCliente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF'
      'FROM entidades ent'
      'ORDER BY NOMEENTIDADE')
    Left = 112
    Top = 20
    object QrClienteCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 112
    Top = 64
  end
  object QrTransporta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 180
    Top = 20
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 180
    Top = 64
  end
  object PMImprime: TPopupMenu
    Left = 12
    object PackingList1: TMenuItem
      Caption = 'Packing List'
      OnClick = PackingList1Click
    end
  end
  object QrPallets: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPalletsCalcFields
    SQL.Strings = (
      'SELECT IF(cou.ArtigoImp = "", '
      '  CONCAT(gg1.Nome, '
      '  IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      
        '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))), cou.ArtigoImp) ' +
        'TXT_ARTIGO,'
      'cou.ArtigoImp, cou.ClasseImp, '
      'wmi.Pallet, SUM(wmi.Pecas) Pecas, '
      'SUM(wmi.AreaM2) AreaM2, SUM(wmi.AreaP2) AreaP2, '
      
        'IF(SUM(wmi.Pecas) = 0, 0, SUM(wmi.AreaM2) / SUM(wmi.Pecas)) Medi' +
        'aM2, '
      'CASE '
      
        'WHEN SUM(wmi.AreaM2) / SUM(wmi.Pecas) < cou.MediaMinM2 THEN 0.00' +
        '0 '
      
        'WHEN SUM(wmi.AreaM2) / SUM(wmi.Pecas) > cou.MediaMaxM2 THEN 2.00' +
        '0 '
      'ELSE 1.000 END FaixaMediaM2, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle '
      'GROUP BY wmi.Pallet '
      'ORDER BY wmi.Pallet')
    Left = 60
    Top = 292
    object QrPalletsPallet: TLargeintField
      FieldName = 'Pallet'
    end
    object QrPalletsGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrPalletsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrPalletsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPalletsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPalletsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPalletsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPalletsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrPalletsSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrPalletsFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrPalletsNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrPalletsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrPalletsArtigoImp: TWideStringField
      FieldName = 'ArtigoImp'
      Size = 50
    end
    object QrPalletsClasseImp: TWideStringField
      FieldName = 'ClasseImp'
      Size = 30
    end
    object QrPalletsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPalletsMediaM2: TFloatField
      FieldName = 'MediaM2'
      DisplayFormat = '0.00'
    end
    object QrPalletsFaixaMediaM2: TFloatField
      FieldName = 'FaixaMediaM2'
    end
    object QrPalletsTXT_ARTIGO: TWideStringField
      FieldName = 'TXT_ARTIGO'
      Size = 512
    end
  end
  object DsPallets: TDataSource
    DataSet = QrPallets
    Left = 60
    Top = 340
  end
  object frxDsPallets: TfrxDBDataset
    UserName = 'frxDsPallets'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ArtigoImp=ArtigoImp'
      'ClasseImp=ClasseImp'
      'Pallet=Pallet'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'SEQ=SEQ'
      'MediaM2=MediaM2'
      'FaixaMediaM2=FaixaMediaM2'
      'TXT_ARTIGO=TXT_ARTIGO')
    DataSet = QrPallets
    BCDToCurrency = False
    DataSetOptions = []
    Left = 60
    Top = 388
  end
  object frxWET_CURTI_019_00_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxWET_CURTI_019_00_AGetValue
    Left = 60
    Top = 440
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
      end
      item
        DataSet = frxDsVSOutNFeCab
        DataSetName = 'frxDsVSOutNFeCab'
      end
      item
        DataSet = frxDsVSOutNFeIts
        DataSetName = 'frxDsVSOutNFeIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 75.590600000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 34.015770000000010000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 34.015770000000010000
          Width = 468.661720000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Packing List de Transfer'#234'ncia N'#186' [VARF_CODIGO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 253.228314720000000000
          Height = 22.677165350000000000
          DataField = 'TXT_ARTIGO'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."TXT_ARTIGO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 86.929146060000000000
          Height = 22.677165350000000000
          DataField = 'Pallet'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsPallets."Pecas">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsPallets."AreaM2">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Width = 113.385826770000000000
          Height = 22.677165350000000000
          DataField = 'ClasseImp'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."ClasseImp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          DataField = 'SEQ'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."SEQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 653.858690000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 253.228314720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 86.929146060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Width = 113.385826770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Width = 498.897764720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  [COUNT(MD001)] Pallets  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 90.708671180000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsPallets."AreaM2">,MD001,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaP2: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 90.708671180000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsPallets."Pecas">,MD001,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354345350000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSOutNFeCab
        DataSetName = 'frxDsVSOutNFeCab'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 143.622096060000000000
          Height = 22.677165350000000000
          DataField = 'ide_nNF'
          DataSet = frxDsVSOutNFeCab
          DataSetName = 'frxDsVSOutNFeCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutNFeCab."ide_nNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          DataField = 'ide_serie'
          DataSet = frxDsVSOutNFeCab
          DataSetName = 'frxDsVSOutNFeCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutNFeCab."ide_serie"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 22.677180000000020000
          Width = 306.141734720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          DataSet = frxDsVSOutNFeCab
          DataSetName = 'frxDsVSOutNFeCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 22.677180000000020000
          Width = 75.590570710000000000
          Height = 22.677165350000000000
          DataSet = frxDsVSOutNFeCab
          DataSetName = 'frxDsVSOutNFeCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 22.677180000000020000
          Width = 64.251951420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 22.677180000000020000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 22.677180000000020000
          Width = 98.267716535433080000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DD002A: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSOutNFeIts
        DataSetName = 'frxDsVSOutNFeIts'
        RowCount = 0
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          DataField = 'ItemNFe'
          DataSet = frxDsVSOutNFeIts
          DataSetName = 'frxDsVSOutNFeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutNFeIts."ItemNFe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Width = 306.141734720000000000
          Height = 22.677165350000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsVSOutNFeIts
          DataSetName = 'frxDsVSOutNFeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSOutNFeIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 75.590570710000000000
          Height = 22.677165350000000000
          DataField = 'GraGruX'
          DataSet = frxDsVSOutNFeIts
          DataSetName = 'frxDsVSOutNFeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSOutNFeIts."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 64.251951420000000000
          Height = 22.677165350000000000
          DataField = 'Pecas'
          DataSet = frxDsVSOutNFeIts
          DataSetName = 'frxDsVSOutNFeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSOutNFeIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          DataField = 'AreaM2'
          DataSet = frxDsVSOutNFeIts
          DataSetName = 'frxDsVSOutNFeIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSOutNFeIts."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Width = 98.267716535433080000
          Height = 22.677165350000000000
          DataField = 'PesoKg'
          DataSet = frxDsVSOutNFeIts
          DataSetName = 'frxDsVSOutNFeIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSOutNFeIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 49.133890000000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-es')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 26.456709999999990000
          Width = 143.622096060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#250'mero NF-e')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456709999999990000
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 476.220780000000000000
        Width = 680.315400000000000000
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_SUB_TIT]')
          ParentFont = False
        end
        object MeOldC3L0: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 22.574830000000020000
          Width = 192.755907950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
        end
        object MeOldC4L0: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244280000000000000
          Top = 22.574830000000020000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object MeOldC5L0: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 22.574830000000020000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178' WB')
          ParentFont = False
        end
        object MeOldC6L0: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Top = 22.574830000000020000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178' Pronto')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Top = 22.574830000000020000
          Width = 94.488127950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 22.677180000000020000
          Width = 105.826774090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg WB')
          ParentFont = False
        end
      end
      object MD003: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692910000000000
        Top = 540.472790000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 105.826774090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeOldC3L1: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 192.755907950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeOldC4L1: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244280000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object MeOldC5L1: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object MeOldC6L1: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 589.606680000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrVSOutNFeIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSOutNFeItsBeforeClose
    AfterScroll = QrVSOutNFeItsAfterScroll
    SQL.Strings = (
      'SELECT CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, voi.* '
      'FROM vsoutnfi voi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=voi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE voi.Codigo=0')
    Left = 384
    Top = 432
    object QrVSOutNFeItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSOutNFeItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSOutNFeItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSOutNFeItsItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrVSOutNFeItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSOutNFeItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSOutNFeItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrVSOutNFeItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSOutNFeItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSOutNFeItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSOutNFeItsTpCalcVal: TSmallintField
      FieldName = 'TpCalcVal'
    end
    object QrVSOutNFeItsValorU: TFloatField
      FieldName = 'ValorU'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrVSOutNFeItsTribDefSel: TIntegerField
      FieldName = 'TribDefSel'
    end
    object QrVSOutNFeItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSOutNFeItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSOutNFeItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSOutNFeItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSOutNFeItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSOutNFeItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSOutNFeItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSOutNFeIts: TDataSource
    DataSet = QrVSOutNFeIts
    Left = 384
    Top = 480
  end
  object PMVSOutNFeIts: TPopupMenu
    OnPopup = PMVSOutNFeItsPopup
    Left = 672
    Top = 564
    object IncluiitemdeNF1: TMenuItem
      Caption = '&Inclui item de NF'
      OnClick = IncluiitemdeNF1Click
    end
    object AlteraitemdeNF1: TMenuItem
      Caption = '&Altera item de NF'
      OnClick = AlteraitemdeNF1Click
    end
    object ExcluiitemdeNF1: TMenuItem
      Caption = '&Exclui item de NF'
      OnClick = ExcluiitemdeNF1Click
    end
  end
  object QrTribIncIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tii.*, tdd.Nome NO_Tributo  '
      'FROM tribincits tii '
      'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tii.Tributo'
      'WHERE tii.FatID=1003 '
      'AND tii.FatNum=14 '
      'AND tii.FatParcela=851 '
      'ORDER BY tii.Controle ')
    Left = 556
    Top = 296
    object QrTribIncItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrTribIncItsFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrTribIncItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrTribIncItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTribIncItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTribIncItsData: TDateField
      FieldName = 'Data'
    end
    object QrTribIncItsHora: TTimeField
      FieldName = 'Hora'
    end
    object QrTribIncItsValorFat: TFloatField
      FieldName = 'ValorFat'
    end
    object QrTribIncItsBaseCalc: TFloatField
      FieldName = 'BaseCalc'
    end
    object QrTribIncItsValrTrib: TFloatField
      FieldName = 'ValrTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrTribIncItsPercent: TFloatField
      FieldName = 'Percent'
      DisplayFormat = '0.00'
    end
    object QrTribIncItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTribIncItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTribIncItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTribIncItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTribIncItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTribIncItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTribIncItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTribIncItsTributo: TIntegerField
      FieldName = 'Tributo'
    end
    object QrTribIncItsVUsoCalc: TFloatField
      FieldName = 'VUsoCalc'
    end
    object QrTribIncItsOperacao: TSmallintField
      FieldName = 'Operacao'
    end
    object QrTribIncItsNO_Tributo: TWideStringField
      FieldName = 'NO_Tributo'
      Size = 60
    end
    object QrTribIncItsFatorDC: TSmallintField
      FieldName = 'FatorDC'
    end
  end
  object DsTribIncIts: TDataSource
    DataSet = QrTribIncIts
    Left = 556
    Top = 344
  end
  object PMTribIncIts: TPopupMenu
    Left = 796
    Top = 564
    object IncluiTributo1: TMenuItem
      Caption = '&Inclui Tributo'
      OnClick = IncluiTributo1Click
    end
    object AlteraTributoAtual1: TMenuItem
      Caption = '&Altera Tributo Atual'
      OnClick = AlteraTributoAtual1Click
    end
    object ExcluiTributoAtual1: TMenuItem
      Caption = '&Exclui Tributo Atual'
      OnClick = ExcluiTributoAtual1Click
    end
  end
  object QrIMEISorc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 476
    Top = 293
    object QrIMEISorcCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIMEISorcControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrIMEISorcMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrIMEISorcMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrIMEISorcMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrIMEISorcEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrIMEISorcTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrIMEISorcCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrIMEISorcMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrIMEISorcDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrIMEISorcPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrIMEISorcGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrIMEISorcPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEISorcPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEISorcAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrIMEISorcSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrIMEISorcSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrIMEISorcSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrIMEISorcSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrIMEISorcSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEISorcSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrIMEISorcSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrIMEISorcFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrIMEISorcMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrIMEISorcFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrIMEISorcCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrIMEISorcDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrIMEISorcDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrIMEISorcDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrIMEISorcQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrIMEISorcQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEISorcQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrIMEISorcQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEISorcQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrIMEISorcPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrIMEISorcMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEISorcStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrIMEISorcNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrIMEISorcNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrIMEISorcNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEISorcNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrIMEISorcID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrIMEISorcReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsIMEISrc: TDataSource
    DataSet = QrIMEISorc
    Left = 476
    Top = 337
  end
  object QrVSOutNFeCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSOutNFeCabBeforeClose
    AfterScroll = QrVSOutNFeCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM vsoutnfecab'
      'WHERE MovimCod=:P0')
    Left = 276
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSOutNFeCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSOutNFeCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSOutNFeCabide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrVSOutNFeCabide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrVSOutNFeCabOriCod: TIntegerField
      FieldName = 'OriCod'
    end
    object QrVSOutNFeCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSOutNFeCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSOutNFeCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSOutNFeCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSOutNFeCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSOutNFeCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSOutNFeCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSOutNFeCab: TDataSource
    DataSet = QrVSOutNFeCab
    Left = 276
    Top = 480
  end
  object PMVSOutNFeCab: TPopupMenu
    OnPopup = PMVSOutNFeCabPopup
    Left = 544
    Top = 556
    object IncluiSerieNumeroDeNFe1: TMenuItem
      Caption = 'Inclui s'#233'rie/ n'#250'mero de NFe'
      OnClick = IncluiSerieNumeroDeNFe1Click
    end
    object AlteraSerieNumeroDeNFe1: TMenuItem
      Caption = 'Aterai s'#233'rie/ n'#250'mero de NFe'
      OnClick = AlteraSerieNumeroDeNFe1Click
    end
    object ExcluiSerieNumeroDeNFe1: TMenuItem
      Caption = 'Exclui s'#233'rie/ n'#250'mero de NFe'
      OnClick = ExcluiSerieNumeroDeNFe1Click
    end
  end
  object frxDsVSOutNFeCab: TfrxDBDataset
    UserName = 'frxDsVSOutNFeCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'ide_serie=ide_serie'
      'ide_nNF=ide_nNF'
      'OriCod=OriCod'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrVSOutNFeCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 276
    Top = 528
  end
  object frxDsVSOutNFeIts: TfrxDBDataset
    UserName = 'frxDsVSOutNFeIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Codigo=Codigo'
      'Controle=Controle'
      'ItemNFe=ItemNFe'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'TpCalcVal=TpCalcVal'
      'ValorU=ValorU'
      'TribDefSel=TribDefSel'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrVSOutNFeIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 384
    Top = 528
  end
  object QrVSMOEnvAvu: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvAvuBeforeClose
    AfterScroll = QrVSMOEnvAvuAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 660
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvAvuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvAvuCFTMA_FatID: TIntegerField
      FieldName = 'CFTMA_FatID'
    end
    object QrVSMOEnvAvuCFTMA_FatNum: TIntegerField
      FieldName = 'CFTMA_FatNum'
    end
    object QrVSMOEnvAvuCFTMA_Empresa: TIntegerField
      FieldName = 'CFTMA_Empresa'
    end
    object QrVSMOEnvAvuCFTMA_Terceiro: TIntegerField
      FieldName = 'CFTMA_Terceiro'
    end
    object QrVSMOEnvAvuCFTMA_nItem: TIntegerField
      FieldName = 'CFTMA_nItem'
    end
    object QrVSMOEnvAvuCFTMA_SerCT: TIntegerField
      FieldName = 'CFTMA_SerCT'
    end
    object QrVSMOEnvAvuCFTMA_nCT: TIntegerField
      FieldName = 'CFTMA_nCT'
    end
    object QrVSMOEnvAvuCFTMA_Pecas: TFloatField
      FieldName = 'CFTMA_Pecas'
    end
    object QrVSMOEnvAvuCFTMA_PesoKg: TFloatField
      FieldName = 'CFTMA_PesoKg'
    end
    object QrVSMOEnvAvuCFTMA_AreaM2: TFloatField
      FieldName = 'CFTMA_AreaM2'
    end
    object QrVSMOEnvAvuCFTMA_AreaP2: TFloatField
      FieldName = 'CFTMA_AreaP2'
    end
    object QrVSMOEnvAvuCFTMA_PesTrKg: TFloatField
      FieldName = 'CFTMA_PesTrKg'
    end
    object QrVSMOEnvAvuCFTMA_CusTrKg: TFloatField
      FieldName = 'CFTMA_CusTrKg'
    end
    object QrVSMOEnvAvuCFTMA_ValorT: TFloatField
      FieldName = 'CFTMA_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAvuVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvAvuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvAvuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvAvuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvAvuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvAvuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvAvuAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvAvuAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvAvuAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvAvuAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvAvu: TDataSource
    DataSet = QrVSMOEnvAvu
    Left = 660
    Top = 384
  end
  object QrVSMOEnvAVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmoenvevmi'
      'WHERE VSMOEnvEnv=:p0')
    Left = 660
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvAVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvAVMIVSMOEnvAvu: TIntegerField
      FieldName = 'VSMOEnvAvu'
    end
    object QrVSMOEnvAVMIVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvAVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvAVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvAVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvAVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvAVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvAVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvAVMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvAVMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvAVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvAVMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrVSMOEnvAVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSMOEnvAVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSMOEnvAVMI: TDataSource
    DataSet = QrVSMOEnvAVMI
    Left = 660
    Top = 484
  end
  object QrVSMOEnvEnv: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvEnvBeforeClose
    AfterScroll = QrVSMOEnvEnvAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 600
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEnvNFEMP_FatID: TIntegerField
      FieldName = 'NFEMP_FatID'
    end
    object QrVSMOEnvEnvNFEMP_FatNum: TIntegerField
      FieldName = 'NFEMP_FatNum'
    end
    object QrVSMOEnvEnvNFEMP_Empresa: TIntegerField
      FieldName = 'NFEMP_Empresa'
    end
    object QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField
      FieldName = 'NFEMP_Terceiro'
    end
    object QrVSMOEnvEnvNFEMP_nItem: TIntegerField
      FieldName = 'NFEMP_nItem'
    end
    object QrVSMOEnvEnvNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrVSMOEnvEnvNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrVSMOEnvEnvNFEMP_Pecas: TFloatField
      FieldName = 'NFEMP_Pecas'
    end
    object QrVSMOEnvEnvNFEMP_PesoKg: TFloatField
      FieldName = 'NFEMP_PesoKg'
    end
    object QrVSMOEnvEnvNFEMP_AreaM2: TFloatField
      FieldName = 'NFEMP_AreaM2'
    end
    object QrVSMOEnvEnvNFEMP_AreaP2: TFloatField
      FieldName = 'NFEMP_AreaP2'
    end
    object QrVSMOEnvEnvNFEMP_ValorT: TFloatField
      FieldName = 'NFEMP_ValorT'
    end
    object QrVSMOEnvEnvCFTMP_FatID: TIntegerField
      FieldName = 'CFTMP_FatID'
    end
    object QrVSMOEnvEnvCFTMP_FatNum: TIntegerField
      FieldName = 'CFTMP_FatNum'
    end
    object QrVSMOEnvEnvCFTMP_Empresa: TIntegerField
      FieldName = 'CFTMP_Empresa'
    end
    object QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField
      FieldName = 'CFTMP_Terceiro'
    end
    object QrVSMOEnvEnvCFTMP_nItem: TIntegerField
      FieldName = 'CFTMP_nItem'
    end
    object QrVSMOEnvEnvCFTMP_SerCT: TIntegerField
      FieldName = 'CFTMP_SerCT'
    end
    object QrVSMOEnvEnvCFTMP_nCT: TIntegerField
      FieldName = 'CFTMP_nCT'
    end
    object QrVSMOEnvEnvCFTMP_Pecas: TFloatField
      FieldName = 'CFTMP_Pecas'
    end
    object QrVSMOEnvEnvCFTMP_PesoKg: TFloatField
      FieldName = 'CFTMP_PesoKg'
    end
    object QrVSMOEnvEnvCFTMP_AreaM2: TFloatField
      FieldName = 'CFTMP_AreaM2'
    end
    object QrVSMOEnvEnvCFTMP_AreaP2: TFloatField
      FieldName = 'CFTMP_AreaP2'
    end
    object QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField
      FieldName = 'CFTMP_PesTrKg'
    end
    object QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField
      FieldName = 'CFTMP_CusTrKg'
    end
    object QrVSMOEnvEnvCFTMP_ValorT: TFloatField
      FieldName = 'CFTMP_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvEnvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEnvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEnvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEnvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEnvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEnvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEnvAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEnvAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEnvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvEnv: TDataSource
    DataSet = QrVSMOEnvEnv
    Left = 600
    Top = 56
  end
  object QrVSMOEnvEVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmoenvevmi'
      'WHERE VSMOEnvEnv=:p0')
    Left = 684
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvEVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
    object QrVSMOEnvEVMIVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvEVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEVMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEVMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvEVMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrVSMOEnvEVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSMOEnvEVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSMOEnvEVMI: TDataSource
    DataSet = QrVSMOEnvEVMI
    Left = 684
    Top = 56
  end
  object QrVSMOEnvRet: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvRetBeforeClose
    AfterScroll = QrVSMOEnvRetAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvret cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 768
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvRetNFCMO_FatID: TIntegerField
      FieldName = 'NFCMO_FatID'
    end
    object QrVSMOEnvRetNFCMO_FatNum: TIntegerField
      FieldName = 'NFCMO_FatNum'
    end
    object QrVSMOEnvRetNFCMO_Empresa: TIntegerField
      FieldName = 'NFCMO_Empresa'
    end
    object QrVSMOEnvRetNFCMO_nItem: TIntegerField
      FieldName = 'NFCMO_nItem'
    end
    object QrVSMOEnvRetNFCMO_SerNF: TIntegerField
      FieldName = 'NFCMO_SerNF'
    end
    object QrVSMOEnvRetNFCMO_nNF: TIntegerField
      FieldName = 'NFCMO_nNF'
    end
    object QrVSMOEnvRetNFCMO_Pecas: TFloatField
      FieldName = 'NFCMO_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetNFCMO_PesoKg: TFloatField
      FieldName = 'NFCMO_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetNFCMO_AreaM2: TFloatField
      FieldName = 'NFCMO_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_AreaP2: TFloatField
      FieldName = 'NFCMO_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_CusMOM2: TFloatField
      FieldName = 'NFCMO_CusMOM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_CusMOKG: TFloatField
      FieldName = 'NFCMO_CusMOKG'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_ValorT: TFloatField
      FieldName = 'NFCMO_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_Terceiro: TIntegerField
      FieldName = 'NFCMO_Terceiro'
    end
    object QrVSMOEnvRetNFRMP_FatID: TIntegerField
      FieldName = 'NFRMP_FatID'
    end
    object QrVSMOEnvRetNFRMP_FatNum: TIntegerField
      FieldName = 'NFRMP_FatNum'
    end
    object QrVSMOEnvRetNFRMP_Empresa: TIntegerField
      FieldName = 'NFRMP_Empresa'
    end
    object QrVSMOEnvRetNFRMP_nItem: TIntegerField
      FieldName = 'NFRMP_nItem'
    end
    object QrVSMOEnvRetNFRMP_SerNF: TIntegerField
      FieldName = 'NFRMP_SerNF'
    end
    object QrVSMOEnvRetNFRMP_nNF: TIntegerField
      FieldName = 'NFRMP_nNF'
    end
    object QrVSMOEnvRetNFRMP_Pecas: TFloatField
      FieldName = 'NFRMP_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetNFRMP_PesoKg: TFloatField
      FieldName = 'NFRMP_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetNFRMP_AreaM2: TFloatField
      FieldName = 'NFRMP_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_AreaP2: TFloatField
      FieldName = 'NFRMP_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_ValorT: TFloatField
      FieldName = 'NFRMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_Terceiro: TIntegerField
      FieldName = 'NFRMP_Terceiro'
    end
    object QrVSMOEnvRetCFTPA_FatID: TIntegerField
      FieldName = 'CFTPA_FatID'
    end
    object QrVSMOEnvRetCFTPA_FatNum: TIntegerField
      FieldName = 'CFTPA_FatNum'
    end
    object QrVSMOEnvRetCFTPA_Empresa: TIntegerField
      FieldName = 'CFTPA_Empresa'
    end
    object QrVSMOEnvRetCFTPA_Terceiro: TIntegerField
      FieldName = 'CFTPA_Terceiro'
    end
    object QrVSMOEnvRetCFTPA_nItem: TIntegerField
      FieldName = 'CFTPA_nItem'
    end
    object QrVSMOEnvRetCFTPA_SerCT: TIntegerField
      FieldName = 'CFTPA_SerCT'
    end
    object QrVSMOEnvRetCFTPA_nCT: TIntegerField
      FieldName = 'CFTPA_nCT'
    end
    object QrVSMOEnvRetCFTPA_Pecas: TFloatField
      FieldName = 'CFTPA_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetCFTPA_PesoKg: TFloatField
      FieldName = 'CFTPA_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetCFTPA_AreaM2: TFloatField
      FieldName = 'CFTPA_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_AreaP2: TFloatField
      FieldName = 'CFTPA_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_PesTrKg: TFloatField
      FieldName = 'CFTPA_PesTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_CusTrKg: TFloatField
      FieldName = 'CFTPA_CusTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_ValorT: TFloatField
      FieldName = 'CFTPA_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsVSMOEnvRet: TDataSource
    DataSet = QrVSMOEnvRet
    Left = 768
    Top = 56
  end
  object QrVSMOEnvRVmi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vee.NFEMP_SerNF, vee.NFEMP_nNF, mev.* '
      'FROM vsmoenvrvmi mev'
      'LEFT JOIN vsmoenvenv vee ON vee.Codigo=mev.VSMOEnvEnv')
    Left = 852
    Top = 8
    object QrVSMOEnvRVmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvRVmiVSMOEnvRet: TIntegerField
      FieldName = 'VSMOEnvRet'
    end
    object QrVSMOEnvRVmiVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
    object QrVSMOEnvRVmiPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRVmiPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRVmiAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvRVmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvRVmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvRVmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvRVmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvRVmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvRVmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvRVmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvRVmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvRVmiNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrVSMOEnvRVmiNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrVSMOEnvRVmiValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsVSMOEnvRVmi: TDataSource
    DataSet = QrVSMOEnvRVmi
    Left = 852
    Top = 56
  end
  object QrVSMOEnvGVmi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vsmoenvgvmi')
    Left = 940
    Top = 8
    object QrVSMOEnvGVmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvGVmiVSMOEnvRet: TIntegerField
      FieldName = 'VSMOEnvRet'
    end
    object QrVSMOEnvGVmiVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvGVmiPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvGVmiPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvGVmiAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvGVmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvGVmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvGVmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvGVmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvGVmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvGVmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvGVmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvGVmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvGVmiVSMovimCod: TIntegerField
      FieldName = 'VSMovimCod'
    end
  end
  object DsVSMOEnvGVmi: TDataSource
    DataSet = QrVSMOEnvGVmi
    Left = 940
    Top = 56
  end
end
