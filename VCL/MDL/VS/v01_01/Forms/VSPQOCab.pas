unit VSPQOCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmVSPQOCab = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEmitGru: TmySQLQuery;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    DsEmitGru: TDataSource;
    QrListaSetores: TmySQLQuery;
    QrListaSetoresCodigo: TIntegerField;
    QrListaSetoresNome: TWideStringField;
    DsListaSetores: TDataSource;
    Label2: TLabel;
    TPDataB: TdmkEditDateTimePicker;
    Label1: TLabel;
    EdSetor: TdmkEditCB;
    CBSetor: TdmkDBLookupComboBox;
    EdEmitGru: TdmkEditCB;
    Label14: TLabel;
    CBEmitGru: TdmkDBLookupComboBox;
    EdVSMovCod: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo: Integer;
    FPrecisaEmitGru: Boolean;
  end;

  var
  FmVSPQOCab: TFmVSPQOCab;

implementation

uses UnMyObjects, Module, PQx, Principal, ModuleGeral, PQOEdit, MyDBCheck,
  DmkDAC_PF, UnPQ_PF, UMySQLModule;

{$R *.DFM}

procedure TFmVSPQOCab.BtOKClick(Sender: TObject);
const
  Obriga = True;
var
  DataB, Nome: String;
  Codigo, Setor, EmitGru, VSMovCod: Integer;
  //CustoInsumo, CustoTotal: Double;
begin
  if UnPQx.ImpedePeloBalanco(TPDataB.Date) then Exit;
  Codigo         := EdCodigo.ValueVariant;
  VSMovCod       := EdVSMovCod.ValueVariant;
  Setor          := EdSetor.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //CustoInsumo    := ;
  //CustoTotal     := ;
  DataB          := Geral.FDT(TPDataB.Date, 1);
  EmitGru        := EdEmitGru.ValueVariant;
  if MyObjects.FIC(Setor = 0, EdSetor, 'Informe o setor!') then
    Exit;
  if FPrecisaEmitGru then
    if Dmod.ObrigaInformarEmitGru(Obriga, EmitGru, EdEmitGru) then
      Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('pqo', 'Codigo', ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pqo', False, [
  'Setor', (*'CustoInsumo', 'CustoTotal',*)
  'Nome', 'DataB', 'EmitGru', 'VSMovCod'], [
  'Codigo'], [
  Setor, (*CustoInsumo, CustoTotal,*)
  Nome, DataB, EmitGru, VSMovCod], [
  Codigo], True) then
  begin
    FCodigo := Codigo;
    PQ_PF.PQO_AtualizaCusto(Codigo);
    Close;
  end;
end;

procedure TFmVSPQOCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPQOCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSPQOCab.FormCreate(Sender: TObject);
begin
  FCodigo := 0;
  FPrecisaEmitGru := True;
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrListaSetores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
  TPDataB.Date := DModG.ObtemAgora();
end;

procedure TFmVSPQOCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
