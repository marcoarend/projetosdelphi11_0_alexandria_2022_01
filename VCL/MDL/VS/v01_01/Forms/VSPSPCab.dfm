object FmVSPSPCab: TFmVSPSPCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-173 :: Processamento de Sub-produto'
  ClientHeight = 759
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 663
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita2: TGroupBox
      Left = 0
      Top = 153
      Width = 1008
      Height = 180
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object Label3: TLabel
        Left = 16
        Top = 16
        Width = 95
        Height = 13
        Caption = 'Artigo em opera'#231#227'o:'
      end
      object Label10: TLabel
        Left = 696
        Top = 96
        Width = 56
        Height = 13
        Caption = '$ Total MO:'
      end
      object LaPecas: TLabel
        Left = 792
        Top = 96
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object LaPeso: TLabel
        Left = 868
        Top = 96
        Width = 27
        Height = 13
        Caption = 'Peso:'
        Enabled = False
      end
      object Label12: TLabel
        Left = 16
        Top = 96
        Width = 193
        Height = 13
        Caption = 'Observa'#231#227'o sobre o artigo em opera'#231#227'o:'
      end
      object Label19: TLabel
        Left = 944
        Top = 96
        Width = 56
        Height = 13
        Caption = 'ID G'#234'meos:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label35: TLabel
        Left = 16
        Top = 56
        Width = 240
        Height = 13
        Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
      end
      object Label49: TLabel
        Left = 444
        Top = 56
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object Label50: TLabel
        Left = 908
        Top = 56
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label54: TLabel
        Left = 552
        Top = 16
        Width = 105
        Height = 13
        Caption = 'Artigo a ser produzido:'
      end
      object Label55: TLabel
        Left = 400
        Top = 96
        Width = 50
        Height = 13
        Caption = 'Opera'#231#227'o:'
        Enabled = False
      end
      object SpeedButton5: TSpeedButton
        Left = 672
        Top = 112
        Width = 22
        Height = 22
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label66: TLabel
        Left = 16
        Top = 136
        Width = 88
        Height = 13
        Caption = 'Grupo de emiss'#227'o:'
      end
      object EdGraGruX: TdmkEditCB
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 72
        Top = 32
        Width = 477
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 1
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCustoMOTot: TdmkEdit
        Left = 696
        Top = 112
        Width = 94
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPecas: TdmkEdit
        Left = 792
        Top = 112
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPesoKg: TdmkEdit
        Left = 868
        Top = 112
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdObserv: TdmkEdit
        Left = 16
        Top = 112
        Width = 381
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMovimTwn: TdmkEdit
        Left = 944
        Top = 112
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdFornecMO: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecMO: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 369
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsPrestador
        TabOrder = 5
        dmkEditCB = EdFornecMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 444
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 500
        Top = 72
        Width = 405
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_LOC_CEN'
        ListSource = DsStqCenLoc
        TabOrder = 7
        dmkEditCB = EdStqCenLoc
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdReqMovEstq: TdmkEdit
        Left = 908
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdGGXDst: TdmkEditCB
        Left = 552
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GGXDst'
        UpdCampo = 'GGXDst'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGGXDst
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGGXDst: TdmkDBLookupComboBox
        Left = 608
        Top = 32
        Width = 392
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGGXDst
        TabOrder = 3
        dmkEditCB = EdGGXDst
        QryCampo = 'GGXDst'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdOperacoes: TdmkEditCB
        Left = 400
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBOperacoes
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBOperacoes: TdmkDBLookupComboBox
        Left = 456
        Top = 112
        Width = 217
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOperacoes
        TabOrder = 11
        dmkEditCB = EdOperacoes
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmitGru: TdmkEditCB
        Left = 16
        Top = 151
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmitGru
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmitGru: TdmkDBLookupComboBox
        Left = 72
        Top = 151
        Width = 925
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEmitGru
        TabOrder = 17
        dmkEditCB = EdEmitGru
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBEdita1: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 97
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label53: TLabel
        Left = 560
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 712
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 147
        Height = 13
        Caption = 'Observa'#231#227'o sobre a opera'#231#227'o:'
      end
      object Label56: TLabel
        Left = 448
        Top = 56
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label57: TLabel
        Left = 644
        Top = 56
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 425
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPData: TdmkEditDateTimePicker
        Left = 560
        Top = 32
        Width = 108
        Height = 21
        Date = 44879.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdHora: TdmkEdit
        Left = 668
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 712
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 429
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrVSGerArt'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGTipoArea: TdmkRadioGroup
        Left = 776
        Top = 16
        Width = 57
        Height = 77
        Caption = ' '#193'rea:'
        ItemIndex = 0
        Items.Strings = (
          'm'#178
          'ft'#178)
        TabOrder = 10
        QryCampo = 'TipoArea'
        UpdCampo = 'TipoArea'
        UpdType = utYes
        OldValor = 0
      end
      object EdLPFMO: TdmkEdit
        Left = 448
        Top = 72
        Width = 193
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNFeRem: TdmkEdit
        Left = 676
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSerieRem: TdmkEdit
        Left = 644
        Top = 72
        Width = 29
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 600
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 4
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 16
        Width = 120
        Height = 41
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBVSPedIts: TGroupBox
      Left = 0
      Top = 333
      Width = 1008
      Height = 64
      Align = alTop
      TabOrder = 3
      object LaPedItsLib: TLabel
        Left = 16
        Top = 12
        Width = 114
        Height = 13
        Caption = 'Item de pedido atrelado:'
      end
      object Label62: TLabel
        Left = 456
        Top = 12
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object EdPedItsLib: TdmkEditCB
        Left = 16
        Top = 28
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPedItsLib
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPedItsLib: TdmkDBLookupComboBox
        Left = 72
        Top = 28
        Width = 381
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsVSPedIts
        TabOrder = 1
        dmkEditCB = EdPedItsLib
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdClientMO: TdmkEditCB
        Left = 456
        Top = 28
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClientMO: TdmkDBLookupComboBox
        Left = 512
        Top = 28
        Width = 489
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientMO
        TabOrder = 3
        dmkEditCB = EdClientMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBConfig: TGroupBox
      Left = 0
      Top = 97
      Width = 1008
      Height = 56
      Align = alTop
      TabOrder = 0
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 28
        Height = 13
        Caption = 'IME-I:'
      end
      object Label61: TLabel
        Left = 100
        Top = 16
        Width = 123
        Height = 13
        Caption = 'Configura'#231#227'o de inclus'#227'o:'
      end
      object SbVSCOPCabCad: TSpeedButton
        Left = 956
        Top = 32
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbVSCOPCabCadClick
      end
      object SbVSCOPCabCpy: TSpeedButton
        Left = 976
        Top = 32
        Width = 21
        Height = 21
        Caption = '>'
        OnClick = SbVSCOPCabCpyClick
      end
      object EdControle: TdmkEdit
        Left = 16
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdVSCOPCab: TdmkEditCB
        Left = 100
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVSCOPCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBVSCOPCab: TdmkDBLookupComboBox
        Left = 156
        Top = 32
        Width = 797
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 2
        dmkEditCB = EdVSCOPCab
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 663
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 513
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alBottom
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 201
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label1: TLabel
        Left = 940
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        FocusControl = DBEdit0
      end
      object Label2: TLabel
        Left = 80
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit1
      end
      object Label11: TLabel
        Left = 592
        Top = 16
        Width = 107
        Height = 13
        Caption = 'Liberado p/ classificar:'
        FocusControl = DBEdit3
      end
      object Label5: TLabel
        Left = 16
        Top = 56
        Width = 174
        Height = 13
        Caption = 'Observa'#231#227'o sobre a industrializa'#231#227'o:'
        FocusControl = DBEdit4
      end
      object Label22: TLabel
        Left = 440
        Top = 16
        Width = 25
        Height = 13
        Caption = #193'rea:'
        FocusControl = DBEdit14
      end
      object Label23: TLabel
        Left = 476
        Top = 16
        Width = 94
        Height = 13
        Caption = 'Data/hora abertura:'
        FocusControl = DBEdit15
      end
      object Label32: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = dmkDBEdit1
      end
      object Label33: TLabel
        Left = 708
        Top = 16
        Width = 94
        Height = 13
        Caption = 'Configur. p/ classif.:'
        FocusControl = DBEdit25
      end
      object Label34: TLabel
        Left = 824
        Top = 16
        Width = 67
        Height = 13
        Caption = 'Fim opera'#231#227'o:'
        FocusControl = DBEdit26
      end
      object Label58: TLabel
        Left = 680
        Top = 56
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label59: TLabel
        Left = 888
        Top = 56
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label60: TLabel
        Left = 492
        Top = 56
        Width = 50
        Height = 13
        Caption = 'Opera'#231#227'o:'
        FocusControl = DBEdit42
      end
      object Label63: TLabel
        Left = 16
        Top = 160
        Width = 88
        Height = 13
        Caption = 'Grupo de emiss'#227'o:'
      end
      object DBEdit0: TdmkDBEdit
        Left = 940
        Top = 32
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        DataSource = DsVSPSPCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 7
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 80
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSPSPCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 136
        Top = 32
        Width = 301
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSPSPCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 592
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrLibOpe'
        DataSource = DsVSPSPCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 72
        Width = 473
        Height = 21
        DataField = 'Nome'
        DataSource = DsVSPSPCab
        TabOrder = 4
      end
      object DBEdit14: TDBEdit
        Left = 440
        Top = 32
        Width = 32
        Height = 21
        DataField = 'NO_TIPO'
        DataSource = DsVSPSPCab
        TabOrder = 5
      end
      object DBEdit15: TDBEdit
        Left = 476
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtHrAberto'
        DataSource = DsVSPSPCab
        TabOrder = 6
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSPSPCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit25: TDBEdit
        Left = 708
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrCfgOpe'
        DataSource = DsVSPSPCab
        TabOrder = 8
      end
      object DBEdit26: TDBEdit
        Left = 824
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrFimOpe'
        DataSource = DsVSPSPCab
        TabOrder = 9
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 96
        Width = 245
        Height = 61
        Caption = ' Origem:'
        TabOrder = 10
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label36: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label37: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label38: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit27: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasINI'
            DataSource = DsVSPSPCab
            TabOrder = 0
          end
          object DBEdit28: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgINI'
            DataSource = DsVSPSPCab
            TabOrder = 1
          end
          object DBEdit29: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaINIM2'
            DataSource = DsVSPSPCab
            TabOrder = 2
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 264
        Top = 96
        Width = 245
        Height = 61
        Caption = 'Destino:'
        TabOrder = 11
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label40: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label41: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label42: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit31: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasDst'
            DataSource = DsVSPSPCab
            TabOrder = 0
          end
          object DBEdit32: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgDst'
            DataSource = DsVSPSPCab
            TabOrder = 1
          end
          object DBEdit33: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaDstM2'
            DataSource = DsVSPSPCab
            TabOrder = 2
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 760
        Top = 96
        Width = 245
        Height = 61
        Caption = 'Saldo em opera'#231#227'o:'
        TabOrder = 12
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label44: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label45: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label46: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit35: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasSdo'
            DataSource = DsVSPSPCab
            TabOrder = 0
          end
          object DBEdit36: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgSdo'
            DataSource = DsVSPSPCab
            TabOrder = 1
          end
          object DBEdit37: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaSdoM2'
            DataSource = DsVSPSPCab
            TabOrder = 2
          end
        end
      end
      object GroupBox6: TGroupBox
        Left = 512
        Top = 96
        Width = 245
        Height = 61
        Caption = 'Baixas normais:'
        TabOrder = 13
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label39: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label43: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label47: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit30: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasBxa'
            DataSource = DsVSPSPCab
            TabOrder = 0
          end
          object DBEdit34: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgBxa'
            DataSource = DsVSPSPCab
            TabOrder = 1
          end
          object DBEdit38: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaBxaM2'
            DataSource = DsVSPSPCab
            TabOrder = 2
          end
        end
      end
      object DBEdit44: TDBEdit
        Left = 680
        Top = 72
        Width = 205
        Height = 21
        DataField = 'LPFMO'
        DataSource = DsVSPSPCab
        TabOrder = 14
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 920
        Top = 72
        Width = 81
        Height = 21
        TabStop = False
        DataField = 'NFeRem'
        DataSource = DsVSPSPCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 15
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit42: TDBEdit
        Left = 492
        Top = 72
        Width = 186
        Height = 21
        DataField = 'NO_Operacoes'
        DataSource = DsVSPSPCab
        TabOrder = 16
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 888
        Top = 72
        Width = 29
        Height = 21
        TabStop = False
        DataField = 'SerieRem'
        DataSource = DsVSPSPCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 17
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit43: TDBEdit
        Left = 16
        Top = 176
        Width = 56
        Height = 21
        DataField = 'EmitGru'
        DataSource = DsVSPSPCab
        TabOrder = 18
      end
      object DBEdit46: TDBEdit
        Left = 72
        Top = 176
        Width = 925
        Height = 21
        DataField = 'NO_EmitGru'
        DataSource = DsVSPSPCab
        TabOrder = 19
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 599
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 212
        Top = 15
        Width = 794
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 661
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 296
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Em Opera'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtOri: TBitBtn
          Tag = 421
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Origem'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtOriClick
        end
        object BtDst: TBitBtn
          Tag = 447
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Destino'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtDstClick
        end
        object BtPesagem: TBitBtn
          Tag = 194
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pesagem'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtPesagemClick
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 201
      Width = 1008
      Height = 96
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label13: TLabel
        Left = 16
        Top = 16
        Width = 28
        Height = 13
        Caption = 'IME-I:'
      end
      object Label14: TLabel
        Left = 100
        Top = 16
        Width = 95
        Height = 13
        Caption = 'Artigo em opera'#231#227'o:'
      end
      object Label15: TLabel
        Left = 576
        Top = 16
        Width = 62
        Height = 13
        Caption = '$/kg M.obra:'
      end
      object Label16: TLabel
        Left = 672
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object Label17: TLabel
        Left = 748
        Top = 16
        Width = 67
        Height = 13
        Caption = 'Peso (origem):'
        Enabled = False
      end
      object Label20: TLabel
        Left = 836
        Top = 16
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object Label21: TLabel
        Left = 916
        Top = 16
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object Label24: TLabel
        Left = 192
        Top = 56
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        FocusControl = DBEdit16
      end
      object Label26: TLabel
        Left = 16
        Top = 56
        Width = 91
        Height = 13
        Caption = 'S'#233'rie / Ficha RMP:'
        FocusControl = DBEdit18
      end
      object Label51: TLabel
        Left = 492
        Top = 56
        Width = 148
        Height = 13
        Caption = 'Local do (e) centro de estoque:'
        FocusControl = DBEdit39
      end
      object Label52: TLabel
        Left = 932
        Top = 56
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        FocusControl = DBEdit41
      end
      object Label18: TLabel
        Left = 532
        Top = 16
        Width = 39
        Height = 13
        Caption = 'Arquivo:'
        FocusControl = DBEdit11
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 32
        Width = 81
        Height = 21
        DataField = 'Controle'
        DataSource = DsVSPSPAtu
        TabOrder = 0
      end
      object DBEdit6: TDBEdit
        Left = 100
        Top = 32
        Width = 57
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsVSPSPAtu
        TabOrder = 1
      end
      object DBEdit7: TDBEdit
        Left = 156
        Top = 32
        Width = 373
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVSPSPAtu
        TabOrder = 2
      end
      object DBEdit8: TDBEdit
        Left = 576
        Top = 32
        Width = 94
        Height = 21
        DataField = 'CustoMOKg'
        DataSource = DsVSPSPAtu
        TabOrder = 3
      end
      object DBEdit9: TDBEdit
        Left = 672
        Top = 32
        Width = 72
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSPSPAtu
        TabOrder = 4
      end
      object DBEdit10: TDBEdit
        Left = 748
        Top = 32
        Width = 84
        Height = 21
        DataField = 'QtdAntPeso'
        DataSource = DsVSPSPAtu
        TabOrder = 5
      end
      object DBEdit12: TDBEdit
        Left = 836
        Top = 32
        Width = 76
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVSPSPAtu
        TabOrder = 6
      end
      object DBEdit13: TDBEdit
        Left = 916
        Top = 32
        Width = 84
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVSPSPAtu
        TabOrder = 7
      end
      object DBEdit16: TDBEdit
        Left = 192
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Terceiro'
        DataSource = DsVSPSPAtu
        TabOrder = 8
      end
      object DBEdit17: TDBEdit
        Left = 248
        Top = 72
        Width = 240
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsVSPSPAtu
        TabOrder = 9
      end
      object DBEdit18: TDBEdit
        Left = 16
        Top = 72
        Width = 173
        Height = 21
        DataField = 'NO_FICHA'
        DataSource = DsVSPSPAtu
        TabOrder = 10
      end
      object DBEdit40: TDBEdit
        Left = 492
        Top = 72
        Width = 56
        Height = 21
        DataField = 'StqCenLoc'
        DataSource = DsVSPSPAtu
        TabOrder = 11
      end
      object DBEdit39: TDBEdit
        Left = 548
        Top = 72
        Width = 380
        Height = 21
        DataField = 'NO_LOC_CEN'
        DataSource = DsVSPSPAtu
        TabOrder = 12
      end
      object DBEdit41: TDBEdit
        Left = 932
        Top = 72
        Width = 68
        Height = 21
        DataField = 'ReqMovEstq'
        DataSource = DsVSPSPAtu
        TabOrder = 13
      end
      object DBEdit11: TDBEdit
        Left = 532
        Top = 32
        Width = 40
        Height = 21
        DataField = 'NO_TTW'
        DataSource = DsVSPSPAtu
        TabOrder = 14
      end
    end
    object PnDst: TPanel
      Left = 0
      Top = 518
      Width = 1008
      Height = 81
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object DGDadosDst: TDBGrid
        Left = 0
        Top = 0
        Width = 652
        Height = 81
        Align = alClient
        DataSource = DsVSPSPDst
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_TTW'
            Title.Caption = 'Tabela'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ReqMovEstq'
            Title.Caption = 'N'#176' RME'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_LOC_CEN'
            Title.Caption = 'Local e centro de estoque '
            Width = 137
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PALLET'
            Title.Caption = 'Hist'#243'rico'
            Width = 145
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_SerieFch'
            Title.Caption = 'S'#233'rie RMP'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ficha'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigos destino da opera'#231#227'o'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NotaMPAG'
            Title.Caption = 'Nota MPAG'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso kg'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = 'm'#178' gerado'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_FORNECE'
            Title.Caption = 'Fornecedor'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Observ'
            Title.Caption = 'Observa'#231#245'es'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataHora'
            Title.Caption = 'Data / Hora'
            Visible = True
          end>
      end
      object DBGrid1: TDBGrid
        Left = 652
        Top = 0
        Width = 356
        Height = 81
        Align = alRight
        DataSource = DsVSPSPBxa
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = 'm'#178' gerado'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso kg'
            Visible = True
          end>
      end
    end
    object Panel11: TPanel
      Left = 0
      Top = 297
      Width = 1008
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object Label48: TLabel
        Left = 644
        Top = 16
        Width = 164
        Height = 13
        Caption = 'Observa'#231#227'o sobre o artigo gerado:'
      end
      object DBEdit45: TDBEdit
        Left = 644
        Top = 32
        Width = 356
        Height = 21
        DataField = 'Observ'
        DataSource = DsVSPSPAtu
        TabOrder = 0
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 641
        Height = 60
        Align = alLeft
        Caption = ' Valores: '
        TabOrder = 1
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 637
          Height = 43
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label25: TLabel
            Left = 12
            Top = 0
            Width = 62
            Height = 13
            Caption = '$/kg M.obra:'
          end
          object Label27: TLabel
            Left = 116
            Top = 0
            Width = 53
            Height = 13
            Caption = 'Total M.O.:'
            FocusControl = DBEdit20
          end
          object Label28: TLabel
            Left = 324
            Top = 0
            Width = 50
            Height = 13
            Caption = 'Valor total:'
            FocusControl = DBEdit21
          end
          object Label29: TLabel
            Left = 220
            Top = 0
            Width = 49
            Height = 13
            Caption = 'Valor M.P.'
            FocusControl = DBEdit22
          end
          object Label30: TLabel
            Left = 428
            Top = 0
            Width = 39
            Height = 13
            Caption = 'R$ / m'#178':'
            FocusControl = DBEdit23
          end
          object Label31: TLabel
            Left = 532
            Top = 0
            Width = 37
            Height = 13
            Caption = 'R$ / ft'#178':'
            FocusControl = DBEdit24
          end
          object DBEdit19: TDBEdit
            Left = 12
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CustoMOKg'
            DataSource = DsVSPSPAtu
            TabOrder = 0
          end
          object DBEdit20: TDBEdit
            Left = 116
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CustoMOTot'
            DataSource = DsVSPSPAtu
            TabOrder = 1
          end
          object DBEdit21: TDBEdit
            Left = 324
            Top = 16
            Width = 100
            Height = 21
            DataField = 'ValorT'
            DataSource = DsVSPSPAtu
            TabOrder = 2
          end
          object DBEdit22: TDBEdit
            Left = 220
            Top = 16
            Width = 100
            Height = 21
            DataField = 'ValorMP'
            DataSource = DsVSPSPAtu
            TabOrder = 3
          end
          object DBEdit23: TDBEdit
            Left = 428
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CUSTO_M2'
            DataSource = DsVSPSPAtu
            TabOrder = 4
          end
          object DBEdit24: TDBEdit
            Left = 532
            Top = 16
            Width = 100
            Height = 21
            DataField = 'CUSTO_P2'
            DataSource = DsVSPSPAtu
            TabOrder = 5
          end
        end
      end
    end
    object PCPSPOri: TPageControl
      Left = 0
      Top = 357
      Width = 1008
      Height = 88
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 5
      object TabSheet1: TTabSheet
        Caption = 'IME-Is'
        object DGDadosOri: TDBGrid
          Left = 0
          Top = 0
          Width = 1002
          Height = 63
          Align = alClient
          DataSource = DsVSPSPOriIMEI
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'N'#176' RME'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos origem da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaMPAG'
              Title.Caption = 'Nota MPAG'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdGerArM2'
              Title.Caption = 'm'#178' gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSMulFrnCab'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Outras baixas de insumos'
        ImageIndex = 1
        object DBGrid7: TDBGrid
          Left = 0
          Top = 0
          Width = 517
          Height = 60
          Align = alLeft
          DataSource = DsPQO
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataB'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoTotal'
              Title.Caption = 'Custo'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EmitGru'
              Title.Caption = 'Grupo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EMITGRU'
              Title.Caption = 'Nome Grupo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESETOR'
              Title.Caption = 'Setor'
              Width = 90
              Visible = True
            end>
        end
        object DBGIts: TDBGrid
          Left = 517
          Top = 0
          Width = 483
          Height = 60
          Align = alClient
          DataSource = DsPQOIts
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'OrigemCtrl'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Insumo'
              Title.Alignment = taCenter
              Title.Caption = 'C'#243'digo'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliDest'
              Title.Caption = 'Empresa'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPQ'
              Title.Caption = 'Mercadoria'
              Width = 366
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Title.Alignment = taRightJustify
              Title.Caption = 'Quantidade'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor total'
              Width = 92
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 383
        Height = 32
        Caption = 'Processamento de Sub-produto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 383
        Height = 32
        Caption = 'Processamento de Sub-produto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 383
        Height = 32
        Caption = 'Processamento de Sub-produto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSPSPCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSPSPCabBeforeOpen
    AfterOpen = QrVSPSPCabAfterOpen
    BeforeClose = QrVSPSPCabBeforeClose
    AfterScroll = QrVSPSPCabAfterScroll
    OnCalcFields = QrVSPSPCabCalcFields
    SQL.Strings = (
      'SELECT '
      'IF(PecasMan<>0, PecasMan, PecasSrc) PecasINI,'
      'IF(AreaManM2<>0, AreaManM2, AreaSrcM2) AreaINIM2,'
      'IF(AreaManP2<>0, AreaManP2, AreaSrcM2) AreaINIP2,'
      'IF(PesoKgMan<>0, PesoKgMan, PesoKgSrc) PesoKgINI,'
      'voc.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA'
      'FROM vsopecab voc'
      'LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa'
      'WHERE voc.Codigo > 0')
    Left = 120
    Top = 461
    object QrVSPSPCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'vsopecab.Codigo'
    end
    object QrVSPSPCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Origin = 'vsopecab.MovimCod'
    end
    object QrVSPSPCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'vsopecab.Empresa'
    end
    object QrVSPSPCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPSPCabDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
      Origin = 'vsopecab.DtHrAberto'
    end
    object QrVSPSPCabDtHrLibOpe: TDateTimeField
      FieldName = 'DtHrLibOpe'
      Origin = 'vsopecab.DtHrLibOpe'
    end
    object QrVSPSPCabDtHrCfgOpe: TDateTimeField
      FieldName = 'DtHrCfgOpe'
      Origin = 'vsopecab.DtHrCfgOpe'
    end
    object QrVSPSPCabDtHrFimOpe: TDateTimeField
      FieldName = 'DtHrFimOpe'
      Origin = 'vsopecab.DtHrFimOpe'
    end
    object QrVSPSPCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'vsopecab.Nome'
      Size = 100
    end
    object QrVSPSPCabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'vsopecab.Lk'
    end
    object QrVSPSPCabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'vsopecab.DataCad'
    end
    object QrVSPSPCabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'vsopecab.DataAlt'
    end
    object QrVSPSPCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'vsopecab.UserCad'
    end
    object QrVSPSPCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'vsopecab.UserAlt'
    end
    object QrVSPSPCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'vsopecab.AlterWeb'
    end
    object QrVSPSPCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'vsopecab.Ativo'
    end
    object QrVSPSPCabPecasMan: TFloatField
      FieldName = 'PecasMan'
      Origin = 'vsopecab.PecasMan'
    end
    object QrVSPSPCabAreaManM2: TFloatField
      FieldName = 'AreaManM2'
      Origin = 'vsopecab.AreaManM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabAreaManP2: TFloatField
      FieldName = 'AreaManP2'
      Origin = 'vsopecab.AreaManP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabTipoArea: TSmallintField
      FieldName = 'TipoArea'
      Origin = 'vsopecab.TipoArea'
    end
    object QrVSPSPCabNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSPSPCabNO_DtHrFimOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimOpe'
      Calculated = True
    end
    object QrVSPSPCabNO_DtHrLibOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibOpe'
      Calculated = True
    end
    object QrVSPSPCabNO_DtHrCfgOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgOpe'
      Calculated = True
    end
    object QrVSPSPCabCacCod: TIntegerField
      FieldName = 'CacCod'
      Origin = 'vsopecab.CacCod'
    end
    object QrVSPSPCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'vsopecab.GraGruX'
    end
    object QrVSPSPCabCustoManMOKg: TFloatField
      FieldName = 'CustoManMOKg'
      Origin = 'vsopecab.CustoManMOKg'
    end
    object QrVSPSPCabCustoManMOTot: TFloatField
      FieldName = 'CustoManMOTot'
      Origin = 'vsopecab.CustoManMOTot'
    end
    object QrVSPSPCabValorManMP: TFloatField
      FieldName = 'ValorManMP'
      Origin = 'vsopecab.ValorManMP'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabValorManT: TFloatField
      FieldName = 'ValorManT'
      Origin = 'vsopecab.ValorManT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabPecasSrc: TFloatField
      FieldName = 'PecasSrc'
      Origin = 'vsopecab.PecasSrc'
    end
    object QrVSPSPCabAreaSrcM2: TFloatField
      FieldName = 'AreaSrcM2'
      Origin = 'vsopecab.AreaSrcM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabAreaSrcP2: TFloatField
      FieldName = 'AreaSrcP2'
      Origin = 'vsopecab.AreaSrcP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabPecasDst: TFloatField
      FieldName = 'PecasDst'
      Origin = 'vsopecab.PecasDst'
    end
    object QrVSPSPCabAreaDstM2: TFloatField
      FieldName = 'AreaDstM2'
      Origin = 'vsopecab.AreaDstM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabAreaDstP2: TFloatField
      FieldName = 'AreaDstP2'
      Origin = 'vsopecab.AreaDstP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabPecasSdo: TFloatField
      FieldName = 'PecasSdo'
      Origin = 'vsopecab.PecasSdo'
    end
    object QrVSPSPCabAreaSdoM2: TFloatField
      FieldName = 'AreaSdoM2'
      Origin = 'vsopecab.AreaSdoM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabAreaSdoP2: TFloatField
      FieldName = 'AreaSdoP2'
      Origin = 'vsopecab.AreaSdoP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabPesoKgSrc: TFloatField
      FieldName = 'PesoKgSrc'
      Origin = 'vsopecab.PesoKgSrc'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPSPCabPesoKgMan: TFloatField
      FieldName = 'PesoKgMan'
      Origin = 'vsopecab.PesoKgMan'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPSPCabPesoKgDst: TFloatField
      FieldName = 'PesoKgDst'
      Origin = 'vsopecab.PesoKgDst'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPSPCabPesoKgSdo: TFloatField
      FieldName = 'PesoKgSdo'
      Origin = 'vsopecab.PesoKgSdo'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPSPCabValorTMan: TFloatField
      FieldName = 'ValorTMan'
      Origin = 'vsopecab.ValorTMan'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabValorTSrc: TFloatField
      FieldName = 'ValorTSrc'
      Origin = 'vsopecab.ValorTSrc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabValorTSdo: TFloatField
      FieldName = 'ValorTSdo'
      Origin = 'vsopecab.ValorTSdo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabPecasINI: TFloatField
      FieldName = 'PecasINI'
      Required = True
    end
    object QrVSPSPCabAreaINIM2: TFloatField
      FieldName = 'AreaINIM2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabAreaINIP2: TFloatField
      FieldName = 'AreaINIP2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabPesoKgINI: TFloatField
      FieldName = 'PesoKgINI'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPSPCabPesoKgBxa: TFloatField
      FieldName = 'PesoKgBxa'
      Origin = 'vsopecab.PesoKgBxa'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPSPCabPecasBxa: TFloatField
      FieldName = 'PecasBxa'
      Origin = 'vsopecab.PecasBxa'
    end
    object QrVSPSPCabAreaBxaM2: TFloatField
      FieldName = 'AreaBxaM2'
      Origin = 'vsopecab.AreaBxaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabAreaBxaP2: TFloatField
      FieldName = 'AreaBxaP2'
      Origin = 'vsopecab.AreaBxaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabValorTBxa: TFloatField
      FieldName = 'ValorTBxa'
      Origin = 'vsopecab.ValorTBxa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPSPCabNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrVSPSPCabLPFMO: TWideStringField
      FieldName = 'LPFMO'
      Size = 30
    end
    object QrVSPSPCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSPSPCabGGXDst: TIntegerField
      FieldName = 'GGXDst'
    end
    object QrVSPSPCabOperacoes: TIntegerField
      FieldName = 'Operacoes'
    end
    object QrVSPSPCabNO_Operacoes: TWideStringField
      FieldName = 'NO_Operacoes'
      Size = 30
    end
    object QrVSPSPCabSerieRem: TSmallintField
      FieldName = 'SerieRem'
    end
    object QrVSPSPCabVSCOPCab: TIntegerField
      FieldName = 'VSCOPCab'
    end
    object QrVSPSPCabNO_VSCOPCab: TWideStringField
      FieldName = 'NO_VSCOPCab'
      Size = 60
    end
    object QrVSPSPCabEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrVSPSPCabNO_EmitGru: TWideStringField
      FieldName = 'NO_EmitGru'
      Size = 60
    end
  end
  object DsVSPSPCab: TDataSource
    DataSet = QrVSPSPCab
    Left = 120
    Top = 509
  end
  object QrVSPSPAtu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 204
    Top = 461
    object QrVSPSPAtuCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPSPAtuControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPSPAtuMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPSPAtuMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPSPAtuMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPSPAtuEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPSPAtuTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPSPAtuCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPSPAtuMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPSPAtuDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPSPAtuPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPSPAtuGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPSPAtuPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPAtuPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPAtuAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPSPAtuSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPSPAtuSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPSPAtuSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPSPAtuSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPSPAtuSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPAtuSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPSPAtuSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPSPAtuFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPSPAtuMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPSPAtuFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPSPAtuCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPSPAtuDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPSPAtuDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPSPAtuDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPSPAtuQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPSPAtuQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPAtuQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPSPAtuQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPAtuQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPAtuNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPSPAtuNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPSPAtuNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPSPAtuNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPSPAtuID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPSPAtuNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPSPAtuReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPSPAtuCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSPSPAtuCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSPSPAtuNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSPSPAtuMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSPSPAtuPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVSPSPAtuStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSPSPAtuNO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
    object QrVSPSPAtuClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
  end
  object DsVSPSPAtu: TDataSource
    DataSet = QrVSPSPAtu
    Left = 200
    Top = 509
  end
  object PMOri: TPopupMenu
    OnPopup = PMOriPopup
    Left = 632
    Top = 516
    object ItsIncluiOri: TMenuItem
      Caption = '&Adiciona artigo de origem'
      Enabled = False
      object porIMEI1: TMenuItem
        Caption = 'por &IME-I Total'
        OnClick = porIMEI1Click
      end
      object porIMEIParcial1: TMenuItem
        Caption = 'por IM&E-I Parcial'
        OnClick = porIMEIParcial1Click
      end
    end
    object ItsExcluiOriIMEI: TMenuItem
      Caption = '&Remove IMEI de origem'
      Enabled = False
      OnClick = ItsExcluiOriIMEIClick
    end
    object InformaNmerodaRME1: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      Enabled = False
      OnClick = InformaNmerodaRME1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 632
    Top = 468
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CabLibera1: TMenuItem
      Caption = '&Libera classifica'#231#227'o'
      OnClick = CabLibera1Click
    end
    object CabReeditar1: TMenuItem
      Caption = '&Volta a editar'
      Enabled = False
      OnClick = CabReeditar1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object AtualizaestoqueEmOperao1: TMenuItem
      Caption = 'Atualiza estoque "Em &Processo"'
      OnClick = AtualizaestoqueEmOperao1Click
    end
    object Definiodequantidadesmanualmente1: TMenuItem
      Caption = '&Defini'#231#227'o de quantidades manualmente'
      OnClick = Definiodequantidadesmanualmente1Click
    end
    object Corrigirfornecedor1: TMenuItem
      Caption = 'Corrigir &Fornecedor'
      OnClick = Corrigirfornecedor1Click
    end
    object AlteraLocaldoestoque2: TMenuItem
      Caption = 'Altera Local do esto&que'
      OnClick = AlteraLocaldoestoque2Click
    end
    object Reabreitensteste1: TMenuItem
      Caption = 'Reabre itens teste'
      OnClick = Reabreitensteste1Click
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 876
    Top = 96
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 880
    Top = 144
  end
  object QrVSPSPOriIMEI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 288
    Top = 461
    object QrVSPSPOriIMEICodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPSPOriIMEIControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPSPOriIMEIMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPSPOriIMEIMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPSPOriIMEIMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPSPOriIMEIEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPSPOriIMEITerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPSPOriIMEICliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPSPOriIMEIMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPSPOriIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrVSPSPOriIMEIPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPSPOriIMEIGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPSPOriIMEIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPOriIMEIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPOriIMEIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEIValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEISrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPSPOriIMEISrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPSPOriIMEISrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPSPOriIMEISrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPSPOriIMEISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPSPOriIMEISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPOriIMEISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPSPOriIMEISerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPSPOriIMEIFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPSPOriIMEIMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPSPOriIMEIFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPSPOriIMEICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEIDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPSPOriIMEIDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPSPOriIMEIDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPSPOriIMEIDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPSPOriIMEIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPSPOriIMEIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPOriIMEIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPSPOriIMEIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPOriIMEIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriIMEINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPSPOriIMEINO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPSPOriIMEINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPSPOriIMEINO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPSPOriIMEIID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPSPOriIMEINO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrVSPSPOriIMEINO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPSPOriIMEIReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPSPOriIMEIVSMulFrnCab: TLargeintField
      FieldName = 'VSMulFrnCab'
    end
  end
  object DsVSPSPOriIMEI: TDataSource
    DataSet = QrVSPSPOriIMEI
    Left = 288
    Top = 509
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 420
    Top = 56
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 420
    Top = 100
  end
  object PMNumero: TPopupMenu
    Left = 100
    Top = 65532
    object PeloCdigo1: TMenuItem
      Caption = 'Pela &OO (Ordem de Opera'#231#227'o)'
      OnClick = PeloCdigo1Click
    end
    object PeloIMEC1: TMenuItem
      Caption = 'Pelo I&ME-C'
      OnClick = PeloIMEC1Click
    end
    object PeloIMEI1: TMenuItem
      Caption = 'Pelo IME-&I'
      OnClick = PeloIMEI1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 36
    Top = 12
    object IMEIArtigodeRibeiragerado1: TMenuItem
      Caption = '&IME-I  Ordem de Opera'#231#227'o'
      object Somenteinfo1: TMenuItem
        Caption = '&Somente info'
        OnClick = Somenteinfo1Click
      end
      object Apreencher1: TMenuItem
        Caption = '&A preencher'
        OnClick = Apreencher1Click
      end
    end
    object Ordensdeoperaoemaberto1: TMenuItem
      Caption = 'Ordens de opera'#231#227'o em &aberto'
      OnClick = Ordensdeoperaoemaberto1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Outrasimpresses1: TMenuItem
      Caption = '&Outras impress'#245'es (outra janela)'
      OnClick = Outrasimpresses1Click
    end
  end
  object DsVSPSPDst: TDataSource
    DataSet = QrVSPSPDst
    Left = 464
    Top = 509
  end
  object PMDst: TPopupMenu
    OnPopup = PMDstPopup
    Left = 632
    Top = 564
    object ItsIncluiDst: TMenuItem
      Caption = '&Inclui artigo de destino'
      OnClick = ItsIncluiDstClick
    end
    object Alteraartigodedestino1: TMenuItem
      Caption = '&Altera artigo de destino'
      OnClick = Alteraartigodedestino1Click
    end
    object ItsExcluiDst: TMenuItem
      Caption = '&Remove artigo de destino'
      OnClick = ItsExcluiDstClick
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object InformaNmerodaRME2: TMenuItem
      Caption = 'Informa &N'#250'mero da RME'
      OnClick = InformaNmerodaRME2Click
    end
    object AlteraLocaldoestoque1: TMenuItem
      Caption = 'Altera &Local do estoque'
      OnClick = AlteraLocaldoestoque1Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Corrigeartigodedestino1: TMenuItem
      Caption = 'Corrige artigo de destino'
      object Oitemselecionado1: TMenuItem
        Caption = 'O item selecionado'
        OnClick = Oitemselecionado1Click
      end
      object odosapartirdoselecionado1: TMenuItem
        Caption = 'Todos a partir do selecionado'
        OnClick = odosapartirdoselecionado1Click
      end
    end
  end
  object QrTwn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 780
    Top = 436
    object QrTwnControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrVSPSPBxa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 548
    Top = 465
    object QrVSPSPBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPSPBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPSPBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPSPBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPSPBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPSPBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPSPBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPSPBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPSPBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPSPBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPSPBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPSPBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPSPBxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPBxaPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPBxaAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPSPBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPSPBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPSPBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPSPBxaSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPSPBxaSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPBxaSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPSPBxaSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPSPBxaFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPSPBxaMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPSPBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPSPBxaCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPSPBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPSPBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPSPBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPSPBxaQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPSPBxaQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPBxaQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPSPBxaQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPBxaQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPBxaNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPSPBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPSPBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPSPBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPSPBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPSPBxaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPSPBxaNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPSPBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsVSPSPBxa: TDataSource
    DataSet = QrVSPSPBxa
    Left = 544
    Top = 513
  end
  object QrVSPSPOriPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  "Morto" NO_TTW, CAST(1 AS UNSIGNED) ID_TTW, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      ''
      'vsp.Nome NO_Pallet, '
      
        'CAST(IF(COUNT(DISTINCT vmi.SerieFch, vmi.Ficha) <> 1, "0", "1") ' +
        'AS CHAR) Series_E_Fichas, '
      
        'CAST(IF(COUNT(DISTINCT vmi.SerieFch) <> 1, 0, vmi.SerieFch) AS S' +
        'IGNED) SerieFch, '
      
        'CAST(IF(COUNT(DISTINCT vmi.Ficha) <> 1, 0, vmi.Ficha) AS SIGNED)' +
        ' Ficha, '
      
        'CAST(IF(COUNT(DISTINCT vmi.Terceiro) <> 1, 0, vmi.Terceiro) AS S' +
        'IGNED) Terceiro, '
      
        'CAST(IF(COUNT(DISTINCT vmi.Marca) <> 1, "", vmi.Marca) AS CHAR) ' +
        'Marca, '
      'CAST(IF(COUNT(DISTINCT(vmi.Terceiro))<>1, "", '
      'IF((vmi.Terceiro=0) OR (vmi.Terceiro IS NULL), "",   '
      
        'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome))) AS CHAR) NO_FORNECED' +
        'OR,  '
      'CAST(IF(COUNT(DISTINCT(vmi.SerieFch))<>1, "", '
      'IF((vmi.SerieFch=0) OR (vmi.SerieFch IS NULL), "",   '
      'vsf.Nome)) AS CHAR) NO_SerieFch,  '
      'CAST(vmi.Pallet AS SIGNED) Pallet, '
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, '
      'CAST(SUM(vmi.Pecas) AS DECIMAL) Pecas, '
      'CAST(SUM(vmi.PesoKg) AS DECIMAL) PesoKg, '
      'CAST(SUM(vmi.AreaM2) AS DECIMAL) AreaM2, '
      'CAST(SUM(vmi.AreaP2) AS DECIMAL) AreaP2, '
      'CAST(SUM(vmi.ValorT) AS DECIMAL) ValorT, '
      'CAST(SUM(vmi.SdoVrtPeca) AS DECIMAL) SdoVrtPeca, '
      'CAST(SUM(vmi.SdoVrtPeso) AS DECIMAL) SdoVrtPeso, '
      'CAST(SUM(vmi.SdoVrtArM2) AS DECIMAL) SdoVrtArM2 '
      ''
      ''
      'FROM vsmovitb vmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      ''
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro '
      ''
      ''
      'WHERE vmi.MovimCod=659'
      'AND vmi.MovimNiv=7'
      ''
      ''
      'UNION'
      ''
      ''
      ''
      'SELECT  "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      ''
      'vsp.Nome NO_Pallet, '
      
        'CAST(IF(COUNT(DISTINCT vmi.SerieFch, vmi.Ficha) <> 1, "0", "1") ' +
        'AS CHAR) Series_E_Fichas, '
      
        'CAST(IF(COUNT(DISTINCT vmi.SerieFch) <> 1, 0, vmi.SerieFch) AS S' +
        'IGNED) SerieFch, '
      
        'CAST(IF(COUNT(DISTINCT vmi.Ficha) <> 1, 0, vmi.Ficha) AS SIGNED)' +
        ' Ficha, '
      
        'CAST(IF(COUNT(DISTINCT vmi.Terceiro) <> 1, 0, vmi.Terceiro) AS S' +
        'IGNED) Terceiro, '
      
        'CAST(IF(COUNT(DISTINCT vmi.Marca) <> 1, "", vmi.Marca) AS CHAR) ' +
        'Marca, '
      'CAST(IF(COUNT(DISTINCT(vmi.Terceiro))<>1, "", '
      'IF((vmi.Terceiro=0) OR (vmi.Terceiro IS NULL), "",   '
      
        'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome))) AS CHAR) NO_FORNECED' +
        'OR,  '
      'CAST(IF(COUNT(DISTINCT(vmi.SerieFch))<>1, "", '
      'IF((vmi.SerieFch=0) OR (vmi.SerieFch IS NULL), "",   '
      'vsf.Nome)) AS CHAR) NO_SerieFch,  '
      'CAST(vmi.Pallet AS SIGNED) Pallet, '
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, '
      'CAST(SUM(vmi.Pecas) AS DECIMAL) Pecas, '
      'CAST(SUM(vmi.PesoKg) AS DECIMAL) PesoKg, '
      'CAST(SUM(vmi.AreaM2) AS DECIMAL) AreaM2, '
      'CAST(SUM(vmi.AreaP2) AS DECIMAL) AreaP2, '
      'CAST(SUM(vmi.ValorT) AS DECIMAL) ValorT, '
      'CAST(SUM(vmi.SdoVrtPeca) AS DECIMAL) SdoVrtPeca, '
      'CAST(SUM(vmi.SdoVrtPeso) AS DECIMAL) SdoVrtPeso, '
      'CAST(SUM(vmi.SdoVrtArM2) AS DECIMAL) SdoVrtArM2 '
      ''
      ''
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      ''
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro '
      ''
      ''
      'WHERE vmi.MovimCod=659'
      'AND vmi.MovimNiv=7'
      ''
      'GROUP BY Pallet '
      'ORDER BY Pallet ')
    Left = 376
    Top = 461
    object QrVSPSPOriPalletPallet: TLargeintField
      FieldName = 'Pallet'
    end
    object QrVSPSPOriPalletGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSPSPOriPalletPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSPSPOriPalletAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriPalletAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPOriPalletPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPOriPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPSPOriPalletNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrVSPSPOriPalletSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrVSPSPOriPalletFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrVSPSPOriPalletNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrVSPSPOriPalletID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrVSPSPOriPalletSeries_E_Fichas: TWideStringField
      FieldName = 'Series_E_Fichas'
      Size = 1
    end
    object QrVSPSPOriPalletTerceiro: TLargeintField
      FieldName = 'Terceiro'
    end
    object QrVSPSPOriPalletMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSPSPOriPalletNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPSPOriPalletNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPSPOriPalletValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSPSPOriPalletSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPSPOriPalletSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPOriPalletSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsVSPSPOriPallet: TDataSource
    DataSet = QrVSPSPOriPallet
    Left = 376
    Top = 509
  end
  object QrForcados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmovits'
      'WHERE MovimID=9 '
      'AND SrcMovID=11'
      'AND SrcNivel1=5'
      '')
    Left = 908
    Top = 420
    object QrForcadosCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrForcadosControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrForcadosMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrForcadosMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrForcadosMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrForcadosEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrForcadosTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrForcadosCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrForcadosMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrForcadosDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrForcadosPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrForcadosGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrForcadosPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrForcadosSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrForcadosSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrForcadosSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrForcadosSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrForcadosSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrForcadosSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrForcadosFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrForcadosMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrForcadosFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrForcadosCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrForcadosDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrForcadosDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrForcadosDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrForcadosQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrForcadosQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrForcadosQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrForcadosNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrForcadosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrForcadosNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrForcadosID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object DsForcados: TDataSource
    DataSet = QrForcados
    Left = 908
    Top = 468
  end
  object QrVSPedIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.VSWetEnd=0')
    Left = 708
    Top = 468
    object QrVSPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrVSPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsVSPedIts: TDataSource
    DataSet = QrVSPedIts
    Left = 708
    Top = 512
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 344
    Top = 56
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 344
    Top = 104
  end
  object PMNovo: TPopupMenu
    Left = 68
    Top = 56
    object CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem
      Caption = '&Corrige Fornecedores destino a partir desta opera'#231#227'o'
      OnClick = CorrigeFornecedoresdestinoapartirdestaoperao1Click
    end
  end
  object QrVSPSPDst: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPSPDstBeforeClose
    AfterScroll = QrVSPSPDstAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 464
    Top = 461
    object QrVSPSPDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPSPDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPSPDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPSPDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPSPDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPSPDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPSPDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPSPDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPSPDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPSPDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPSPDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPSPDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPSPDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPSPDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPSPDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPSPDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPSPDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPSPDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPSPDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPSPDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPSPDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPSPDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPSPDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPSPDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPSPDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPSPDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPSPDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPSPDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPSPDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPSPDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPSPDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPSPDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPSPDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPSPDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPSPDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPSPDstNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrVSPSPDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPSPDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPSPDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSPSPDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSPSPDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSPSPDstNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
  end
  object QrClientMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 40
    Top = 464
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 40
    Top = 512
  end
  object QrGGXDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 960
    Top = 96
    object QrGGXDstGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXDstControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXDstSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXDstCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXDstNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXDst: TDataSource
    DataSet = QrGGXDst
    Left = 960
    Top = 144
  end
  object QrOperacoes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM operacoes'
      'ORDER BY Nome')
    Left = 320
    Top = 188
    object QrOperacoesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOperacoesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsOperacoes: TDataSource
    DataSet = QrOperacoes
    Left = 320
    Top = 240
  end
  object QrVSCOPCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM operacoes'
      'ORDER BY Nome')
    Left = 400
    Top = 188
    object QrVSCOPCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCOPCabNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSCOPCab: TDataSource
    DataSet = QrVSCOPCab
    Left = 400
    Top = 240
  end
  object PMPesagem: TPopupMenu
    OnPopup = PMPesagemPopup
    Left = 540
    Top = 580
    object Emiteoutrasbaixas1: TMenuItem
      Caption = '&Outras baixas'
      object Novogrupo1: TMenuItem
        Caption = 'Nova pesagem'
        OnClick = Novogrupo1Click
      end
      object Novoitem1: TMenuItem
        Caption = 'Novo item'
        OnClick = Novoitem1Click
      end
      object MenuItem1: TMenuItem
        Caption = '-'
      end
      object Excluiitem1: TMenuItem
        Caption = 'Exclui item'
        OnClick = Excluiitem1Click
      end
      object Excluigrupo1: TMenuItem
        Caption = 'Exclui toda pesagem'
        OnClick = Excluigrupo1Click
      end
    end
    object Recalculacusto1: TMenuItem
      Caption = 'Recalcula custo'
      OnClick = Recalculacusto1Click
    end
  end
  object QrPQO: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPQOBeforeClose
    AfterScroll = QrPQOAfterScroll
    SQL.Strings = (
      'SELECT lse.Nome NOMESETOR,'
      'emg.Nome NO_EMITGRU, pqo.*'
      'FROM pqo pqo'
      'LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor'
      'LEFT JOIN emitgru emg ON emg.Codigo=pqo.EmitGru'
      'WHERE pqo.Codigo > 0'
      '')
    Left = 784
    Top = 505
    object QrPQOCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQOSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrPQOCustoInsumo: TFloatField
      FieldName = 'CustoInsumo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQOCustoTotal: TFloatField
      FieldName = 'CustoTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQODataB: TDateField
      FieldName = 'DataB'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQOLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQODataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQODataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQOUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQOUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQONOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrPQONO_EMITGRU: TWideStringField
      FieldName = 'NO_EMITGRU'
      Size = 60
    end
    object QrPQOAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQOAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQOEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrPQONome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQO: TDataSource
    DataSet = QrPQO
    Left = 784
    Top = 549
  end
  object QrPQOIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico, '
      'pqg.Nome NOMEGRUPO'
      'FROM pqx pqx'
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo'
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ'
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico'
      'WHERE pqx.Tipo=190'
      'AND pqx.OrigemCodi=:P0')
    Left = 832
    Top = 509
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQOItsDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQOItsTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPQOItsCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQOItsCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQOItsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQOItsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQOItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQOItsOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQOItsOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQOItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQOItsGrupoQuimico: TIntegerField
      FieldName = 'GrupoQuimico'
    end
    object QrPQOItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
  end
  object DsPQOIts: TDataSource
    DataSet = QrPQOIts
    Left = 832
    Top = 557
  end
  object QrEmitGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM emitgru'
      'WHERE Ativo=1'
      'ORDER BY Nome'
      '')
    Left = 576
    Top = 57
    object QrEmitGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEmitGru: TDataSource
    DataSet = QrEmitGru
    Left = 576
    Top = 101
  end
end
