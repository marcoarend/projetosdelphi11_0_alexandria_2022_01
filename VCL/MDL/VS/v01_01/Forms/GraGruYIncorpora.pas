unit GraGruYIncorpora;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkDBGridZTO, UnAppPF;

type
  TFmGraGruYIncorpora = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    DBGGGX1: TdmkDBGridZTO;
    QrGG1Orfaos: TmySQLQuery;
    DsGG1Orfaos: TDataSource;
    QrGG1OrfaosNivel1: TIntegerField;
    QrGG1OrfaosNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FGraGruY, FPrdGrupTip: Integer;
    procedure ReopenGG1Orfaos();
  end;

  var
  FmGraGruYIncorpora: TFmGraGruYIncorpora;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmGraGruYIncorpora.BtOKClick(Sender: TObject);
var
  I: Integer;
  iNiveis1: array of Integer;
begin
  if DBGGGX1.SelectedRows.Count > 0 then
  begin
//    if Geral.MB_Pergunta('Deseja excluir TODAS as receitas selecionadas (tamb�m seus itens e fluxo)?') <> ID_YES then
//      Exit;
    //
    SetLength(iNiveis1, DBGGGX1.SelectedRows.Count);
    with DBGGGX1.DataSource.DataSet do
    for I := 0 to DBGGGX1.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGGGX1.SelectedRows.Items[I]));
      GotoBookmark(DBGGGX1.SelectedRows.Items[I]);
      //
      iNiveis1[I] := QrGG1OrfaosNivel1.Value;
      //Numero := Qry.FieldByName('ID').AsInteger;
      //
      //PQ_PF.ExcluiReceita(Numero);
    end;
    AppPF.ConfiguraGGXsDeGGY(FGraGruY, iNiveis1);
    ReopenGG1Orfaos();
  end else
    Geral.MB_Aviso('Nenhum grupo de reduzidos foi selecionado!');
end;

procedure TFmGraGruYIncorpora.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruYIncorpora.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruYIncorpora.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmGraGruYIncorpora.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruYIncorpora.ReopenGG1Orfaos();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGG1Orfaos, Dmod.MyDB, [
  'SELECT DISTINCT gg1.Nivel1, gg1.Nome',
  'FROM gragrux ggx',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN gragruxcou gxc ON gxc.GraGruX=ggx.Controle',
  'WHERE gg1.PrdGrupTip=' + Geral.FF0(FPrdGrupTip),
  'AND gxc.GraGruX IS NULL ',
  '']);
  if QrGG1Orfaos.RecordCount > 0 then
    Geral.MB_Info(QrGG1Orfaos.SQL.Text);
end;

end.
