object FmVSImpPrevia: TFmVSImpPrevia
  Left = 0
  Top = 0
  Caption = 'WET-CURTI-026 :: Impress'#227'o de Pallet VS'
  ClientHeight = 211
  ClientWidth = 294
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object frxWET_CURTI_026_02: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41608.425381412000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Imprime: Boolean;'
      '    '
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Line3.Visible := Imprime;                         '
      
        '  Imprime := not Imprime;                                       ' +
        '                                                                ' +
        '                  '
      'end;'
      ''
      'begin'
      '  Imprime := True;                                  '
      'end.')
    OnGetValue = frxWET_CURTI_026_02GetValue
    Left = 56
    Top = 8
    Datasets = <
      item
        DataSet = frxDs07PrevPal
        DataSetName = 'frxDs07PrevPal'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 559.370078740000100000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        DataSet = frxDs07PrevPal
        DataSetName = 'frxDs07PrevPal'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 37.897650000000000000
          Width = 680.315400000000000000
          Height = 438.425480000000000000
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          Left = 192.756030000000000000
          Top = 491.441250000000000000
          Width = 487.559370000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Artigo de Ribeira')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 498.897960000000000000
          Top = 536.795610000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Top = 536.795610000000000000
          Width = 177.637846540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Item [Line#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 64.543290000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'PALLET [frxDs07PrevPal."VSPallet"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 112.322820000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 1.118120000000000000
          Top = 108.763760000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 491.441098660000000000
          Width = 188.976377950000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 188.976377950000000000
          Top = 204.196818660000000000
          Width = 491.338582680000000000
          Height = 37.795275590000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 162.322820000000000000
          Width = 120.944881890000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 340.157480310000000000
          Top = 351.598444720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Top = 351.598444720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 510.236220470000000000
          Top = 351.598444720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 170.078740160000000000
          Top = 351.598444720000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 514.118256689999900000
          Width = 188.976377950000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 188.976377950000000000
          Top = 241.992096690000000000
          Width = 491.338582680000000000
          Height = 109.606345590000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -48
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 120.944881890000000000
          Top = 162.322820000000000000
          Width = 559.370078740000100000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDs07PrevPal."GraGruX"] - [frxDs07PrevPal."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 340.157480310000000000
          Top = 389.393722760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs07PrevPal."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 389.393722760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDs07PrevPal."' +
              'Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 510.236220470000000000
          Top = 389.393722760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 170.078740160000000000
          Top = 389.393722760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs07PrevPal."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 120.944881890000000000
          Top = 116.543290000000000000
          Width = 559.370078740000100000
          Height = 40.000000000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 116.543290000000000000
          Width = 120.944881890000000000
          Height = 40.000000000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fabricado por: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 177.637910000000000000
          Top = 536.795610000000000000
          Width = 321.260050000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 226.771800000000000000
          Top = 64.354360000000000000
          Width = 434.645510630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs07PrevPal."Nome"]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 559.370103150000000000
          Width = 680.315400000000000000
          OnBeforePrint = 'Line3OnBeforePrint'
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Line2: TfrxLineView
          Top = 487.559370000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object Qr07PrevPal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cia.VSPallet,  '
      'SUM(cia.Pecas) Pecas,  '
      'SUM(cia.AreaM2) AreaM2, '
      'SUM(cia.AreaP2) AreaP2, '
      'CONCAT(gg1.Nome,   '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),  '
      'IF(pal.Nome <> "", CONCAT(" (", pal.Nome, ")"), ""))   '
      'NO_PRD_TAM_COR, pal.*   '
      'FROM vscacitsa cia '
      'LEFT JOIN vspalleta pal ON pal.Codigo=cia.VSPallet '
      'LEFT JOIN gragrux ggx ON ggx.Controle=pal.GraGruX   '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'WHERE cia.VSPallet BETWEEN 221 AND 245 '
      'GROUP BY cia.VSPallet '
      'ORDER BY cia.VSPallet DESC ')
    Left = 52
    Top = 56
    object Qr07PrevPalVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object Qr07PrevPalPecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr07PrevPalAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr07PrevPalAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr07PrevPalNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 220
    end
    object Qr07PrevPalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr07PrevPalNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object Qr07PrevPalEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr07PrevPalStatus: TIntegerField
      FieldName = 'Status'
    end
    object Qr07PrevPalCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object Qr07PrevPalGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr07PrevPalDtHrEndAdd: TDateTimeField
      FieldName = 'DtHrEndAdd'
    end
    object Qr07PrevPalGerRclCab: TIntegerField
      FieldName = 'GerRclCab'
    end
    object Qr07PrevPalDtHrFimRcl: TDateTimeField
      FieldName = 'DtHrFimRcl'
    end
    object Qr07PrevPalMovimIDGer: TIntegerField
      FieldName = 'MovimIDGer'
    end
    object Qr07PrevPalLk: TIntegerField
      FieldName = 'Lk'
    end
    object Qr07PrevPalDataCad: TDateField
      FieldName = 'DataCad'
    end
    object Qr07PrevPalDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object Qr07PrevPalUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object Qr07PrevPalUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object Qr07PrevPalAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object Qr07PrevPalAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr07PrevPalQtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object frxDs07PrevPal: TfrxDBDataset
    UserName = 'frxDs07PrevPal'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VSPallet=VSPallet'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Codigo=Codigo'
      'Nome=Nome'
      'Empresa=Empresa'
      'Status=Status'
      'CliStat=CliStat'
      'GraGruX=GraGruX'
      'DtHrEndAdd=DtHrEndAdd'
      'GerRclCab=GerRclCab'
      'DtHrFimRcl=DtHrFimRcl'
      'MovimIDGer=MovimIDGer'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'QtdPrevPc=QtdPrevPc')
    DataSet = Qr07PrevPal
    BCDToCurrency = False
    Left = 52
    Top = 108
  end
end
