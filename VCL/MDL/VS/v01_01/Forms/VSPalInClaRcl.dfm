object FmVSPaInClaRcl: TFmVSPaInClaRcl
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-062 :: Pallet em Classifica'#231#227'o / Reclassifica'#231#227'o'
  ClientHeight = 338
  ClientWidth = 611
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 611
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 563
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 515
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 493
        Height = 32
        Caption = 'Pallet em Classifica'#231#227'o / Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 493
        Height = 32
        Caption = 'Pallet em Classifica'#231#227'o / Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 493
        Height = 32
        Caption = 'Pallet em Classifica'#231#227'o / Reclassifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 611
    Height = 176
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 611
      Height = 176
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 611
        Height = 176
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 4
        ExplicitTop = 4
        ExplicitWidth = 812
        ExplicitHeight = 467
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 607
          Height = 159
          Align = alClient
          DataSource = DsPalClaRcl
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_ClaRcl'
              Title.Caption = 'Onde'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CacCod'
              Title.Caption = 'OC'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSMovIts'
              Title.Caption = 'IME-I'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSPallet'
              Title.Caption = 'Pallet origem'
              Width = 100
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 224
    Width = 611
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 607
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 268
    Width = 611
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 465
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 463
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
  object QrPalClaRcl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT 1 ClaRcl, "Classifica'#231#227'o" NO_ClaRcl,'
      'CacCod, VSMovIts, 0.000 VSPallet  '
      'FROM vspaclacaba '
      'WHERE DtHrFimCla<"1900-01-01"  '
      'AND (715 IN (LstPal01, LstPal02, LstPal03, '
      '             LstPal04, LstPal05, LstPal06) '
      '     ) '
      ' '
      'UNION '
      ' '
      'SELECT 2 ClaRcl, "Reclassifica'#231#227'o" NO_ClaRcl,'
      'CacCod, VSMovIts, VSPallet  '
      'FROM vsparclcaba '
      'WHERE DtHrFimCla<"1900-01-01"  '
      'AND (715 IN (LstPal01, LstPal02, LstPal03, '
      '             LstPal04, LstPal05, LstPal06) '
      '     ) ')
    Left = 228
    Top = 220
    object QrPalClaRclClaRcl: TLargeintField
      FieldName = 'ClaRcl'
      Required = True
    end
    object QrPalClaRclNO_ClaRcl: TWideStringField
      FieldName = 'NO_ClaRcl'
      Required = True
      Size = 15
    end
    object QrPalClaRclCacCod: TIntegerField
      FieldName = 'CacCod'
      Required = True
    end
    object QrPalClaRclVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
      Required = True
    end
    object QrPalClaRclVSPallet: TFloatField
      FieldName = 'VSPallet'
      Required = True
    end
  end
  object DsPalClaRcl: TDataSource
    DataSet = QrPalClaRcl
    Left = 228
    Top = 268
  end
end
