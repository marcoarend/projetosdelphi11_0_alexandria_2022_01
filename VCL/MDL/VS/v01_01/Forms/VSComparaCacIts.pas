unit VSComparaCacIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  dmkDBGridZTO, dmkCheckBox;

type
  TFmVSComparaCacIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel64: TPanel;
    Label62: TLabel;
    Label63: TLabel;
    EdPalletA: TdmkEdit;
    EdPalletB: TdmkEdit;
    QrSumA: TmySQLQuery;
    QrSumAAreaM2: TFloatField;
    QrSumAItens: TLargeintField;
    QrItensA: TmySQLQuery;
    QrItensAPecas: TFloatField;
    QrItensAAreaM2: TFloatField;
    QrSumB: TmySQLQuery;
    QrSumBAreaM2: TFloatField;
    QrSumBItens: TLargeintField;
    QrItensB: TmySQLQuery;
    QrItensBPecas: TFloatField;
    QrItensBAreaM2: TFloatField;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrComparaCacIts: TmySQLQuery;
    QrComparaCacItsAreaM2: TFloatField;
    QrComparaCacItsPalletA: TIntegerField;
    QrComparaCacItsPalletB: TIntegerField;
    DsComparaCacIts: TDataSource;
    CkAgregar: TdmkCheckBox;
    procedure BtOKClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    FVSComparaCacIts: String;
  public
    { Public declarations }
  end;

  var
  FmVSComparaCacIts: TFmVSComparaCacIts;

implementation

uses UnMyObjects, Module, DmkDAC_PF, CreateVS, ModuleGeral, UMySQLModule;

{$R *.DFM}

procedure TFmVSComparaCacIts.BtOKClick(Sender: TObject);
  procedure AbreSum(Query: TmySQLQuery; Pallet: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQUery0(Query, Dmod.MyDB, [
    'SELECT DISTINCT(AreaM2) AreaM2, ',
    'COUNT(Controle) Itens ',
    'FROM vscacitsa ',
    'WHERE VSPallet=' + Geral.FF0(Pallet),
    'GROUP BY AreaM2 ',
    'ORDER BY AreaM2 ',
(*
    'SELECT Pecas, AreaM2 ',
    'FROM vscacitsa ',
    'WHERE VSPallet=' + Geral.FF0(Pallet),
    'GROUP BY AreaM2 ',
    'ORDER BY AreaM2 ',
*)
    '']);
  end;
  //
  procedure InsereSum(Query: TmySQLQuery; Campo: String);
  var
    Area: Double;
    Itens: Integer;
  begin
    Query.First;
    while not Query.Eof do
    begin
      Area   := Query.FieldByName('AreaM2').AsFloat;
      Itens  := Query.FieldByName('Itens').AsInteger;
      //
      UMyMod.SQLIns_ON_DUPLICATE_KEY(DModG.QrUpdPID1, FVSComparaCacIts, False, [
      Campo], ['AreaM2'], [Campo], [
      Itens], [Area], [Itens], False);
      //
      Query.Next;
    end;
  end;
var
  PalletA, PalletB, ItensOk, AllItens, AtuItens: Integer;
  Percentual: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    PalletA := EdPalletA.ValueVariant;
    PalletB := EdPalletB.ValueVariant;
    //
    if MyObjects.FIC(PalletA = 0, EdPalletA, 'Informe o Pallet "A"!') then Exit;
    if MyObjects.FIC(PalletB = 0, EdPalletB, 'Informe o Pallet "B"!') then Exit;
    //
    AbreSum(QrSumA, PalletA);
    AbreSum(QrSumB, PalletB);
    //
    if not CkAgregar.Checked then
      FVSComparaCacIts := UnCreateVS.RecriaTempTableNovo(ntrttVSComparaCacIts,
        DModG.QrUpdPID1, False);
    //
    InsereSum(QrSumA, 'PalletA');
    InsereSum(QrSumB, 'PalletB');
    //
    UnDmkDAC_PF.AbreMySQLQUery0(QrComparaCacIts, DModG.MyPid_DB, [
    'SELECT * ',
    'FROM ' + FVSComparaCacIts,
    'ORDER BY AreaM2 ',
    '']);
    ItensOk  := 0;
    AllItens := 0;
    QrComparaCacIts.First;
    while not QrComparaCacIts.Eof do
    begin
      AtuItens := QrComparaCacItsPalletA.Value + QrComparaCacItsPalletB.Value;
      AllItens := AllItens + AtuItens;
      if QrComparaCacItsPalletA.Value = QrComparaCacItsPalletB.Value then
        ItensOk := ItensOk + AtuItens;
      //
      QrComparaCacIts.Next;
    end;
    //
    Percentual := 0;
    if AllItens > 0 then
      Percentual := ItensOk / AllItens * 100;
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'Couros com mesma area: ' + Geral.FFT(Percentual, 2, siNegativo) + ' %');
    //
    CkAgregar.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSComparaCacIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSComparaCacIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSComparaCacIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSComparaCacIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
