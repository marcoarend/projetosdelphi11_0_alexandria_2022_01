unit VSGerArtCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, UnProjGroup_Consts,
  UnGrl_Consts, UnGrl_Geral, UnAppEnums, dmkDBGridZTO;

type
  TFmVSGerArtCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita1: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdit0: TdmkDBEdit;
    QrVSGerArt: TmySQLQuery;
    DsVSGerArt: TDataSource;
    QrVSGerArtNew: TmySQLQuery;
    DsVSGerArtNew: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSGerArtCodigo: TIntegerField;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label53: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label4: TLabel;
    QrVSGerArtEmpresa: TIntegerField;
    QrVSGerArtDtHrAberto: TDateTimeField;
    QrVSGerArtNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    QrVSGerArtNome: TWideStringField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    QrVSGerArt015: TmySQLQuery;
    DsVSGerArt015: TDataSource;
    QrVSGerArtMovimCod: TIntegerField;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBEdita2: TGroupBox;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label3: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCustoMOKg: TdmkEdit;
    Label12: TLabel;
    EdObserv: TdmkEdit;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    N1: TMenuItem;
    CabLibera1: TMenuItem;
    QrVSGerArtLk: TIntegerField;
    QrVSGerArtDataCad: TDateField;
    QrVSGerArtDataAlt: TDateField;
    QrVSGerArtUserCad: TIntegerField;
    QrVSGerArtUserAlt: TIntegerField;
    QrVSGerArtAlterWeb: TSmallintField;
    QrVSGerArtAtivo: TSmallintField;
    QrVSGerArtDtHrLibCla: TDateTimeField;
    QrVSGerArtPecasMan: TFloatField;
    QrVSGerArtAreaManM2: TFloatField;
    QrVSGerArtAreaManP2: TFloatField;
    RGTipoArea: TdmkRadioGroup;
    QrVSGerArtTipoArea: TSmallintField;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label23: TLabel;
    QrVSGerArtNO_TIPO: TWideStringField;
    QrVSGerArtNO_DtHrFimCla: TWideStringField;
    dmkDBEdit1: TdmkDBEdit;
    Label32: TLabel;
    QrVSGerArtDtHrFimCla: TDateTimeField;
    DBEdit25: TDBEdit;
    Label33: TLabel;
    QrVSGerArtNO_DtHrLibCla: TWideStringField;
    CabReeditar1: TMenuItem;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    QrVSGerArtDtHrCfgCla: TDateTimeField;
    QrVSGerArtNO_DtHrCfgCla: TWideStringField;
    Label35: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    IMEIArtigodeRibeiragerado1: TMenuItem;
    N2: TMenuItem;
    Outrasimpresses1: TMenuItem;
    QrVSGerArtCacCod: TIntegerField;
    QrVSGerArtGraGruX: TIntegerField;
    QrVSGerArtCustoManMOKg: TFloatField;
    QrVSGerArtCustoManMOTot: TFloatField;
    QrVSGerArtValorManMP: TFloatField;
    QrVSGerArtValorManT: TFloatField;
    Classesgeradas1: TMenuItem;
    PMQuery: TPopupMenu;
    Pelaobservaosobreaaindustrializao1: TMenuItem;
    Pesquisasgeral1: TMenuItem;
    BtClassesGeradas: TBitBtn;
    N3: TMenuItem;
    Removeforado1: TMenuItem;
    QrTwn: TmySQLQuery;
    QrTwnControle: TIntegerField;
    QrTwnMovimNiv: TIntegerField;
    QrTwnSrcNivel2: TIntegerField;
    PB1: TProgressBar;
    QrTeste: TmySQLQuery;
    QrTesteCodigo: TIntegerField;
    QrTesteControle: TIntegerField;
    QrTesteMovimCod: TIntegerField;
    QrTesteMovimNiv: TIntegerField;
    QrTesteMovimTwn: TIntegerField;
    QrTesteEmpresa: TIntegerField;
    QrTesteTerceiro: TIntegerField;
    QrTesteCliVenda: TIntegerField;
    QrTesteMovimID: TIntegerField;
    QrTesteLnkNivXtr1: TIntegerField;
    QrTesteLnkNivXtr2: TIntegerField;
    QrTesteDataHora: TDateTimeField;
    QrTestePallet: TIntegerField;
    QrTesteGraGruX: TIntegerField;
    QrTestePecas: TFloatField;
    QrTestePesoKg: TFloatField;
    QrTesteAreaM2: TFloatField;
    QrTesteAreaP2: TFloatField;
    QrTesteValorT: TFloatField;
    QrTesteSrcMovID: TIntegerField;
    QrTesteSrcNivel1: TIntegerField;
    QrTesteSrcNivel2: TIntegerField;
    QrTesteSdoVrtPeca: TFloatField;
    QrTesteSdoVrtArM2: TFloatField;
    QrTesteObserv: TWideStringField;
    QrTesteLk: TIntegerField;
    QrTesteDataCad: TDateField;
    QrTesteDataAlt: TDateField;
    QrTesteUserCad: TIntegerField;
    QrTesteUserAlt: TIntegerField;
    QrTesteAlterWeb: TSmallintField;
    QrTesteAtivo: TSmallintField;
    QrTesteFicha: TIntegerField;
    QrTesteMisturou: TSmallintField;
    QrTesteCustoMOKg: TFloatField;
    QrTesteCustoMOTot: TFloatField;
    QrTesteNO_PRD_TAM_COR: TWideStringField;
    QrTesteSdoVrtPeso: TFloatField;
    QrTesteSerieFch: TIntegerField;
    QrTesteNO_SerieFch: TWideStringField;
    QrTesteMarca: TWideStringField;
    QrTesteVSRibCad: TIntegerField;
    QrTesteMeios: TFloatField;
    QrTesteFatorVNC: TFloatField;
    QrTesteFatorVRC: TFloatField;
    QrTesteFatorInt: TFloatField;
    FluxodoIMEIGerado1: TMenuItem;
    EdGGXAnterior: TdmkEdit;
    Label39: TLabel;
    QrNiv14: TmySQLQuery;
    QrNiv15: TmySQLQuery;
    QrNiv14Codigo: TIntegerField;
    QrNiv14Controle: TIntegerField;
    QrNiv14MovimCod: TIntegerField;
    QrNiv14MovimNiv: TIntegerField;
    QrNiv14MovimTwn: TIntegerField;
    QrNiv14Empresa: TIntegerField;
    QrNiv14Terceiro: TIntegerField;
    QrNiv14CliVenda: TIntegerField;
    QrNiv14MovimID: TIntegerField;
    QrNiv14LnkNivXtr1: TIntegerField;
    QrNiv14LnkNivXtr2: TIntegerField;
    QrNiv14DataHora: TDateTimeField;
    QrNiv14Pallet: TIntegerField;
    QrNiv14GraGruX: TIntegerField;
    QrNiv14Pecas: TFloatField;
    QrNiv14PesoKg: TFloatField;
    QrNiv14AreaM2: TFloatField;
    QrNiv14AreaP2: TFloatField;
    QrNiv14ValorT: TFloatField;
    QrNiv14SrcMovID: TIntegerField;
    QrNiv14SrcNivel1: TIntegerField;
    QrNiv14SrcNivel2: TIntegerField;
    QrNiv14SrcGGX: TIntegerField;
    QrNiv14SdoVrtPeca: TFloatField;
    QrNiv14SdoVrtPeso: TFloatField;
    QrNiv14SdoVrtArM2: TFloatField;
    QrNiv14Observ: TWideStringField;
    QrNiv14SerieFch: TIntegerField;
    QrNiv14Ficha: TIntegerField;
    QrNiv14Misturou: TSmallintField;
    QrNiv14FornecMO: TIntegerField;
    QrNiv14CustoMOKg: TFloatField;
    QrNiv14CustoMOTot: TFloatField;
    QrNiv14ValorMP: TFloatField;
    QrNiv14DstMovID: TIntegerField;
    QrNiv14DstNivel1: TIntegerField;
    QrNiv14DstNivel2: TIntegerField;
    QrNiv14DstGGX: TIntegerField;
    QrNiv14QtdGerPeca: TFloatField;
    QrNiv14QtdGerPeso: TFloatField;
    QrNiv14QtdGerArM2: TFloatField;
    QrNiv14QtdGerArP2: TFloatField;
    QrNiv14QtdAntPeca: TFloatField;
    QrNiv14QtdAntPeso: TFloatField;
    QrNiv14QtdAntArM2: TFloatField;
    QrNiv14QtdAntArP2: TFloatField;
    QrNiv14AptoUso: TSmallintField;
    QrNiv14NotaMPAG: TFloatField;
    QrNiv14Marca: TWideStringField;
    QrNiv14Lk: TIntegerField;
    QrNiv14DataCad: TDateField;
    QrNiv14DataAlt: TDateField;
    QrNiv14UserCad: TIntegerField;
    QrNiv14UserAlt: TIntegerField;
    QrNiv14AlterWeb: TSmallintField;
    QrNiv14Ativo: TSmallintField;
    QrNiv14TpCalcAuto: TIntegerField;
    QrNiv14Zerado: TSmallintField;
    QrNiv14EmFluxo: TSmallintField;
    QrNiv14NotFluxo: TIntegerField;
    QrNiv14FatNotaVNC: TFloatField;
    QrNiv14FatNotaVRC: TFloatField;
    QrNiv14LnkIDXtr: TIntegerField;
    QrNiv14PedItsLib: TIntegerField;
    QrNiv14PedItsFin: TIntegerField;
    QrNiv14PedItsVda: TIntegerField;
    QrNiv15Codigo: TIntegerField;
    QrNiv15Controle: TIntegerField;
    QrNiv15MovimCod: TIntegerField;
    QrNiv15MovimNiv: TIntegerField;
    QrNiv15MovimTwn: TIntegerField;
    QrNiv15Empresa: TIntegerField;
    QrNiv15Terceiro: TIntegerField;
    QrNiv15CliVenda: TIntegerField;
    QrNiv15MovimID: TIntegerField;
    QrNiv15LnkNivXtr1: TIntegerField;
    QrNiv15LnkNivXtr2: TIntegerField;
    QrNiv15DataHora: TDateTimeField;
    QrNiv15Pallet: TIntegerField;
    QrNiv15GraGruX: TIntegerField;
    QrNiv15Pecas: TFloatField;
    QrNiv15PesoKg: TFloatField;
    QrNiv15AreaM2: TFloatField;
    QrNiv15AreaP2: TFloatField;
    QrNiv15ValorT: TFloatField;
    QrNiv15SrcMovID: TIntegerField;
    QrNiv15SrcNivel1: TIntegerField;
    QrNiv15SrcNivel2: TIntegerField;
    QrNiv15SrcGGX: TIntegerField;
    QrNiv15SdoVrtPeca: TFloatField;
    QrNiv15SdoVrtPeso: TFloatField;
    QrNiv15SdoVrtArM2: TFloatField;
    QrNiv15Observ: TWideStringField;
    QrNiv15SerieFch: TIntegerField;
    QrNiv15Ficha: TIntegerField;
    QrNiv15Misturou: TSmallintField;
    QrNiv15FornecMO: TIntegerField;
    QrNiv15CustoMOKg: TFloatField;
    QrNiv15CustoMOTot: TFloatField;
    QrNiv15ValorMP: TFloatField;
    QrNiv15DstMovID: TIntegerField;
    QrNiv15DstNivel1: TIntegerField;
    QrNiv15DstNivel2: TIntegerField;
    QrNiv15DstGGX: TIntegerField;
    QrNiv15QtdGerPeca: TFloatField;
    QrNiv15QtdGerPeso: TFloatField;
    QrNiv15QtdGerArM2: TFloatField;
    QrNiv15QtdGerArP2: TFloatField;
    QrNiv15QtdAntPeca: TFloatField;
    QrNiv15QtdAntPeso: TFloatField;
    QrNiv15QtdAntArM2: TFloatField;
    QrNiv15QtdAntArP2: TFloatField;
    QrNiv15AptoUso: TSmallintField;
    QrNiv15NotaMPAG: TFloatField;
    QrNiv15Marca: TWideStringField;
    QrNiv15Lk: TIntegerField;
    QrNiv15DataCad: TDateField;
    QrNiv15DataAlt: TDateField;
    QrNiv15UserCad: TIntegerField;
    QrNiv15UserAlt: TIntegerField;
    QrNiv15AlterWeb: TSmallintField;
    QrNiv15Ativo: TSmallintField;
    QrNiv15TpCalcAuto: TIntegerField;
    QrNiv15Zerado: TSmallintField;
    QrNiv15EmFluxo: TSmallintField;
    QrNiv15NotFluxo: TIntegerField;
    QrNiv15FatNotaVNC: TFloatField;
    QrNiv15FatNotaVRC: TFloatField;
    QrNiv15LnkIDXtr: TIntegerField;
    QrNiv15PedItsLib: TIntegerField;
    QrNiv15PedItsFin: TIntegerField;
    QrNiv15PedItsVda: TIntegerField;
    FluxodaFichaRMPOrigemitemselecionado1: TMenuItem;
    Editaantigo1: TMenuItem;
    Quantidades1: TMenuItem;
    InNaturadeOrigem1: TMenuItem;
    QrVSGerArt014: TmySQLQuery;
    DsVSGerArt014: TDataSource;
    AlteraDatas1: TMenuItem;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    BtTribIncIts: TBitBtn;
    PMTribIncIts: TPopupMenu;
    IncluiTributo1: TMenuItem;
    AlteraTributoAtual1: TMenuItem;
    ExcluiTributoAtual1: TMenuItem;
    QrTribIncIts: TmySQLQuery;
    QrTribIncItsFatID: TIntegerField;
    QrTribIncItsFatNum: TIntegerField;
    QrTribIncItsFatParcela: TIntegerField;
    QrTribIncItsEmpresa: TIntegerField;
    QrTribIncItsControle: TIntegerField;
    QrTribIncItsData: TDateField;
    QrTribIncItsHora: TTimeField;
    QrTribIncItsValorFat: TFloatField;
    QrTribIncItsBaseCalc: TFloatField;
    QrTribIncItsValrTrib: TFloatField;
    QrTribIncItsPercent: TFloatField;
    QrTribIncItsLk: TIntegerField;
    QrTribIncItsDataCad: TDateField;
    QrTribIncItsDataAlt: TDateField;
    QrTribIncItsUserCad: TIntegerField;
    QrTribIncItsUserAlt: TIntegerField;
    QrTribIncItsAlterWeb: TSmallintField;
    QrTribIncItsAtivo: TSmallintField;
    QrTribIncItsTributo: TIntegerField;
    QrTribIncItsVUsoCalc: TFloatField;
    QrTribIncItsOperacao: TSmallintField;
    QrTribIncItsNO_Tributo: TWideStringField;
    QrTribIncItsFatorDC: TSmallintField;
    DsTribIncIts: TDataSource;
    Panel8: TPanel;
    Panel9: TPanel;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit41: TDBEdit;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label25: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label18: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit11: TDBEdit;
    QrVSGerArt014Codigo: TLargeintField;
    QrVSGerArt014Controle: TLargeintField;
    QrVSGerArt014MovimCod: TLargeintField;
    QrVSGerArt014MovimNiv: TLargeintField;
    QrVSGerArt014MovimTwn: TLargeintField;
    QrVSGerArt014Empresa: TLargeintField;
    QrVSGerArt014Terceiro: TLargeintField;
    QrVSGerArt014CliVenda: TLargeintField;
    QrVSGerArt014MovimID: TLargeintField;
    QrVSGerArt014DataHora: TDateTimeField;
    QrVSGerArt014Pallet: TLargeintField;
    QrVSGerArt014GraGruX: TLargeintField;
    QrVSGerArt014Pecas: TFloatField;
    QrVSGerArt014PesoKg: TFloatField;
    QrVSGerArt014AreaM2: TFloatField;
    QrVSGerArt014AreaP2: TFloatField;
    QrVSGerArt014ValorT: TFloatField;
    QrVSGerArt014SrcMovID: TLargeintField;
    QrVSGerArt014SrcNivel1: TLargeintField;
    QrVSGerArt014SrcNivel2: TLargeintField;
    QrVSGerArt014SrcGGX: TLargeintField;
    QrVSGerArt014SdoVrtPeca: TFloatField;
    QrVSGerArt014SdoVrtPeso: TFloatField;
    QrVSGerArt014SdoVrtArM2: TFloatField;
    QrVSGerArt014Observ: TWideStringField;
    QrVSGerArt014SerieFch: TLargeintField;
    QrVSGerArt014Ficha: TLargeintField;
    QrVSGerArt014Misturou: TLargeintField;
    QrVSGerArt014FornecMO: TLargeintField;
    QrVSGerArt014CustoMOKg: TFloatField;
    QrVSGerArt014CustoMOM2: TFloatField;
    QrVSGerArt014CustoMOTot: TFloatField;
    QrVSGerArt014ValorMP: TFloatField;
    QrVSGerArt014DstMovID: TLargeintField;
    QrVSGerArt014DstNivel1: TLargeintField;
    QrVSGerArt014DstNivel2: TLargeintField;
    QrVSGerArt014DstGGX: TLargeintField;
    QrVSGerArt014QtdGerPeca: TFloatField;
    QrVSGerArt014QtdGerPeso: TFloatField;
    QrVSGerArt014QtdGerArM2: TFloatField;
    QrVSGerArt014QtdGerArP2: TFloatField;
    QrVSGerArt014QtdAntPeca: TFloatField;
    QrVSGerArt014QtdAntPeso: TFloatField;
    QrVSGerArt014QtdAntArM2: TFloatField;
    QrVSGerArt014QtdAntArP2: TFloatField;
    QrVSGerArt014NotaMPAG: TFloatField;
    QrVSGerArt014NO_PALLET: TWideStringField;
    QrVSGerArt014NO_PRD_TAM_COR: TWideStringField;
    QrVSGerArt014NO_TTW: TWideStringField;
    QrVSGerArt014ID_TTW: TLargeintField;
    QrVSGerArt014NO_FORNECE: TWideStringField;
    QrVSGerArt014NO_SerieFch: TWideStringField;
    QrVSGerArt014ReqMovEstq: TLargeintField;
    QrVSGerArtTemIMEIMrt: TIntegerField;
    QrVSGerArt015Codigo: TLargeintField;
    QrVSGerArt015Controle: TLargeintField;
    QrVSGerArt015MovimCod: TLargeintField;
    QrVSGerArt015MovimNiv: TLargeintField;
    QrVSGerArt015MovimTwn: TLargeintField;
    QrVSGerArt015Empresa: TLargeintField;
    QrVSGerArt015Terceiro: TLargeintField;
    QrVSGerArt015CliVenda: TLargeintField;
    QrVSGerArt015MovimID: TLargeintField;
    QrVSGerArt015DataHora: TDateTimeField;
    QrVSGerArt015Pallet: TLargeintField;
    QrVSGerArt015GraGruX: TLargeintField;
    QrVSGerArt015Pecas: TFloatField;
    QrVSGerArt015PesoKg: TFloatField;
    QrVSGerArt015AreaM2: TFloatField;
    QrVSGerArt015AreaP2: TFloatField;
    QrVSGerArt015ValorT: TFloatField;
    QrVSGerArt015SrcMovID: TLargeintField;
    QrVSGerArt015SrcNivel1: TLargeintField;
    QrVSGerArt015SrcNivel2: TLargeintField;
    QrVSGerArt015SrcGGX: TLargeintField;
    QrVSGerArt015SdoVrtPeca: TFloatField;
    QrVSGerArt015SdoVrtPeso: TFloatField;
    QrVSGerArt015SdoVrtArM2: TFloatField;
    QrVSGerArt015Observ: TWideStringField;
    QrVSGerArt015SerieFch: TLargeintField;
    QrVSGerArt015Ficha: TLargeintField;
    QrVSGerArt015Misturou: TLargeintField;
    QrVSGerArt015FornecMO: TLargeintField;
    QrVSGerArt015CustoMOKg: TFloatField;
    QrVSGerArt015CustoMOM2: TFloatField;
    QrVSGerArt015CustoMOTot: TFloatField;
    QrVSGerArt015ValorMP: TFloatField;
    QrVSGerArt015DstMovID: TLargeintField;
    QrVSGerArt015DstNivel1: TLargeintField;
    QrVSGerArt015DstNivel2: TLargeintField;
    QrVSGerArt015DstGGX: TLargeintField;
    QrVSGerArt015QtdGerPeca: TFloatField;
    QrVSGerArt015QtdGerPeso: TFloatField;
    QrVSGerArt015QtdGerArM2: TFloatField;
    QrVSGerArt015QtdGerArP2: TFloatField;
    QrVSGerArt015QtdAntPeca: TFloatField;
    QrVSGerArt015QtdAntPeso: TFloatField;
    QrVSGerArt015QtdAntArM2: TFloatField;
    QrVSGerArt015QtdAntArP2: TFloatField;
    QrVSGerArt015NotaMPAG: TFloatField;
    QrVSGerArt015NO_PALLET: TWideStringField;
    QrVSGerArt015NO_PRD_TAM_COR: TWideStringField;
    QrVSGerArt015NO_TTW: TWideStringField;
    QrVSGerArt015ID_TTW: TLargeintField;
    QrVSGerArt015NO_FORNECE: TWideStringField;
    QrVSGerArt015NO_SerieFch: TWideStringField;
    QrVSGerArt015ReqMovEstq: TLargeintField;
    QrVSGerArtNewCodigo: TLargeintField;
    QrVSGerArtNewControle: TLargeintField;
    QrVSGerArtNewMovimCod: TLargeintField;
    QrVSGerArtNewMovimNiv: TLargeintField;
    QrVSGerArtNewMovimTwn: TLargeintField;
    QrVSGerArtNewEmpresa: TLargeintField;
    QrVSGerArtNewTerceiro: TLargeintField;
    QrVSGerArtNewCliVenda: TLargeintField;
    QrVSGerArtNewMovimID: TLargeintField;
    QrVSGerArtNewDataHora: TDateTimeField;
    QrVSGerArtNewPallet: TLargeintField;
    QrVSGerArtNewGraGruX: TLargeintField;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewSrcMovID: TLargeintField;
    QrVSGerArtNewSrcNivel1: TLargeintField;
    QrVSGerArtNewSrcNivel2: TLargeintField;
    QrVSGerArtNewSrcGGX: TLargeintField;
    QrVSGerArtNewSdoVrtPeca: TFloatField;
    QrVSGerArtNewSdoVrtPeso: TFloatField;
    QrVSGerArtNewSdoVrtArM2: TFloatField;
    QrVSGerArtNewObserv: TWideStringField;
    QrVSGerArtNewSerieFch: TLargeintField;
    QrVSGerArtNewFicha: TLargeintField;
    QrVSGerArtNewMisturou: TLargeintField;
    QrVSGerArtNewFornecMO: TLargeintField;
    QrVSGerArtNewCustoMOKg: TFloatField;
    QrVSGerArtNewCustoMOM2: TFloatField;
    QrVSGerArtNewCustoMOTot: TFloatField;
    QrVSGerArtNewValorMP: TFloatField;
    QrVSGerArtNewDstMovID: TLargeintField;
    QrVSGerArtNewDstNivel1: TLargeintField;
    QrVSGerArtNewDstNivel2: TLargeintField;
    QrVSGerArtNewDstGGX: TLargeintField;
    QrVSGerArtNewQtdGerPeca: TFloatField;
    QrVSGerArtNewQtdGerPeso: TFloatField;
    QrVSGerArtNewQtdGerArM2: TFloatField;
    QrVSGerArtNewQtdGerArP2: TFloatField;
    QrVSGerArtNewQtdAntPeca: TFloatField;
    QrVSGerArtNewQtdAntPeso: TFloatField;
    QrVSGerArtNewQtdAntArM2: TFloatField;
    QrVSGerArtNewQtdAntArP2: TFloatField;
    QrVSGerArtNewNotaMPAG: TFloatField;
    QrVSGerArtNewNO_PALLET: TWideStringField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewNO_TTW: TWideStringField;
    QrVSGerArtNewID_TTW: TLargeintField;
    QrVSGerArtNewNO_FORNECE: TWideStringField;
    QrVSGerArtNewReqMovEstq: TLargeintField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    QrVSGerArtNewNO_LOC_CEN: TWideStringField;
    QrVSGerArtNewMarca: TWideStringField;
    QrVSGerArtNewPedItsLib: TLargeintField;
    QrVSGerArtNewStqCenLoc: TLargeintField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    N4: TMenuItem;
    GBEdita: TGroupBox;
    EdValorMP: TdmkEdit;
    Label31: TLabel;
    Label44: TLabel;
    EdCustoMOTot: TdmkEdit;
    EdPecas: TdmkEdit;
    LaPecas: TLabel;
    EdValorT: TdmkEdit;
    Label43: TLabel;
    EdPesoKg: TdmkEdit;
    LaPeso: TLabel;
    EdQtdAntPeso: TdmkEdit;
    Label45: TLabel;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label46: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    QrVSGerArtNewClientMO: TLargeintField;
    AdicionadoCurimento1: TMenuItem;
    AdicionadoCurimentoMltiplo1: TMenuItem;
    BtVSOutNFeCab: TBitBtn;
    BtVSOutNFeIts: TBitBtn;
    PMVSOutNFeCab: TPopupMenu;
    IncluiSerieNumeroDeNFe1: TMenuItem;
    AlteraSerieNumeroDeNFe1: TMenuItem;
    ExcluiSerieNumeroDeNFe1: TMenuItem;
    PMVSOutNFeIts: TPopupMenu;
    IncluiitemdeNF1: TMenuItem;
    AlteraitemdeNF1: TMenuItem;
    ExcluiitemdeNF1: TMenuItem;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    DGDados: TDBGrid;
    TabSheet2: TTabSheet;
    QrVSOutNFeCab: TmySQLQuery;
    QrVSOutNFeCabCodigo: TIntegerField;
    QrVSOutNFeCabMovimCod: TIntegerField;
    QrVSOutNFeCabide_serie: TIntegerField;
    QrVSOutNFeCabide_nNF: TIntegerField;
    QrVSOutNFeCabOriCod: TIntegerField;
    QrVSOutNFeCabLk: TIntegerField;
    QrVSOutNFeCabDataCad: TDateField;
    QrVSOutNFeCabDataAlt: TDateField;
    QrVSOutNFeCabUserCad: TIntegerField;
    QrVSOutNFeCabUserAlt: TIntegerField;
    QrVSOutNFeCabAlterWeb: TSmallintField;
    QrVSOutNFeCabAtivo: TSmallintField;
    DsVSOutNFeCab: TDataSource;
    QrVSOutNFeIts: TmySQLQuery;
    QrVSOutNFeItsNO_PRD_TAM_COR: TWideStringField;
    QrVSOutNFeItsCodigo: TIntegerField;
    QrVSOutNFeItsControle: TIntegerField;
    QrVSOutNFeItsItemNFe: TIntegerField;
    QrVSOutNFeItsGraGruX: TIntegerField;
    QrVSOutNFeItsPecas: TFloatField;
    QrVSOutNFeItsPesoKg: TFloatField;
    QrVSOutNFeItsAreaM2: TFloatField;
    QrVSOutNFeItsAreaP2: TFloatField;
    QrVSOutNFeItsValorT: TFloatField;
    QrVSOutNFeItsTpCalcVal: TSmallintField;
    QrVSOutNFeItsValorU: TFloatField;
    QrVSOutNFeItsTribDefSel: TIntegerField;
    QrVSOutNFeItsLk: TIntegerField;
    QrVSOutNFeItsDataCad: TDateField;
    QrVSOutNFeItsDataAlt: TDateField;
    QrVSOutNFeItsUserCad: TIntegerField;
    QrVSOutNFeItsUserAlt: TIntegerField;
    QrVSOutNFeItsAlterWeb: TSmallintField;
    QrVSOutNFeItsAtivo: TSmallintField;
    DsVSOutNFeIts: TDataSource;
    DBGrid5: TDBGrid;
    DBGrid2: TDBGrid;
    GroupBox3: TGroupBox;
    DBGrid3: TDBGrid;
    DBEdit24: TDBEdit;
    Label47: TLabel;
    DBEdit33: TDBEdit;
    Label48: TLabel;
    Alterapesokg1: TMenuItem;
    QrVSGerArtNewDtCorrApo: TDateTimeField;
    Atualizacustodescendentesdoitemselecionado1: TMenuItem;
    SbStqCenLoc: TSpeedButton;
    PCSubItens: TPageControl;
    TabSheet4: TTabSheet;
    Panel7: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    QrVSMOEnvRet: TmySQLQuery;
    QrVSMOEnvRetCodigo: TIntegerField;
    QrVSMOEnvRetNFCMO_FatID: TIntegerField;
    QrVSMOEnvRetNFCMO_FatNum: TIntegerField;
    QrVSMOEnvRetNFCMO_Empresa: TIntegerField;
    QrVSMOEnvRetNFCMO_nItem: TIntegerField;
    QrVSMOEnvRetNFCMO_SerNF: TIntegerField;
    QrVSMOEnvRetNFCMO_nNF: TIntegerField;
    QrVSMOEnvRetNFCMO_Pecas: TFloatField;
    QrVSMOEnvRetNFCMO_PesoKg: TFloatField;
    QrVSMOEnvRetNFCMO_AreaM2: TFloatField;
    QrVSMOEnvRetNFCMO_AreaP2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOM2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOKG: TFloatField;
    QrVSMOEnvRetNFCMO_ValorT: TFloatField;
    QrVSMOEnvRetNFRMP_FatID: TIntegerField;
    QrVSMOEnvRetNFRMP_FatNum: TIntegerField;
    QrVSMOEnvRetNFRMP_Empresa: TIntegerField;
    QrVSMOEnvRetNFRMP_nItem: TIntegerField;
    QrVSMOEnvRetNFRMP_SerNF: TIntegerField;
    QrVSMOEnvRetNFRMP_nNF: TIntegerField;
    QrVSMOEnvRetNFRMP_Pecas: TFloatField;
    QrVSMOEnvRetNFRMP_PesoKg: TFloatField;
    QrVSMOEnvRetNFRMP_AreaM2: TFloatField;
    QrVSMOEnvRetNFRMP_AreaP2: TFloatField;
    QrVSMOEnvRetNFRMP_ValorT: TFloatField;
    QrVSMOEnvRetNFCMO_Terceiro: TIntegerField;
    QrVSMOEnvRetNFRMP_Terceiro: TIntegerField;
    QrVSMOEnvRetCFTPA_FatID: TIntegerField;
    QrVSMOEnvRetCFTPA_FatNum: TIntegerField;
    QrVSMOEnvRetCFTPA_Empresa: TIntegerField;
    QrVSMOEnvRetCFTPA_Terceiro: TIntegerField;
    QrVSMOEnvRetCFTPA_nItem: TIntegerField;
    QrVSMOEnvRetCFTPA_SerCT: TIntegerField;
    QrVSMOEnvRetCFTPA_nCT: TIntegerField;
    QrVSMOEnvRetCFTPA_Pecas: TFloatField;
    QrVSMOEnvRetCFTPA_PesoKg: TFloatField;
    QrVSMOEnvRetCFTPA_AreaM2: TFloatField;
    QrVSMOEnvRetCFTPA_AreaP2: TFloatField;
    QrVSMOEnvRetCFTPA_PesTrKg: TFloatField;
    QrVSMOEnvRetCFTPA_CusTrKg: TFloatField;
    QrVSMOEnvRetCFTPA_ValorT: TFloatField;
    DsVSMOEnvRet: TDataSource;
    AtrelamentoNFsdeMO1: TMenuItem;
    Incluiatrelamento1: TMenuItem;
    Alteraatrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    TsRetornoMO: TTabSheet;
    QrVSMOEnvRVmi: TmySQLQuery;
    DsVSMOEnvRVmi: TDataSource;
    QrVSMOEnvGVmi: TmySQLQuery;
    DsVSMOEnvGVmi: TDataSource;
    QrVSMOEnvGVmiCodigo: TIntegerField;
    QrVSMOEnvGVmiVSMOEnvRet: TIntegerField;
    QrVSMOEnvGVmiVSMovIts: TIntegerField;
    QrVSMOEnvGVmiPecas: TFloatField;
    QrVSMOEnvGVmiPesoKg: TFloatField;
    QrVSMOEnvGVmiAreaM2: TFloatField;
    QrVSMOEnvGVmiAreaP2: TFloatField;
    QrVSMOEnvGVmiValorFrete: TFloatField;
    QrVSMOEnvGVmiLk: TIntegerField;
    QrVSMOEnvGVmiDataCad: TDateField;
    QrVSMOEnvGVmiDataAlt: TDateField;
    QrVSMOEnvGVmiUserCad: TIntegerField;
    QrVSMOEnvGVmiUserAlt: TIntegerField;
    QrVSMOEnvGVmiAlterWeb: TSmallintField;
    QrVSMOEnvGVmiAWServerID: TIntegerField;
    QrVSMOEnvGVmiAWStatSinc: TSmallintField;
    QrVSMOEnvGVmiAtivo: TSmallintField;
    QrVSMOEnvGVmiVSMovimCod: TIntegerField;
    QrVSMOEnvRVmiCodigo: TIntegerField;
    QrVSMOEnvRVmiVSMOEnvRet: TIntegerField;
    QrVSMOEnvRVmiVSMOEnvEnv: TIntegerField;
    QrVSMOEnvRVmiPecas: TFloatField;
    QrVSMOEnvRVmiPesoKg: TFloatField;
    QrVSMOEnvRVmiAreaM2: TFloatField;
    QrVSMOEnvRVmiAreaP2: TFloatField;
    QrVSMOEnvRVmiValorFrete: TFloatField;
    QrVSMOEnvRVmiLk: TIntegerField;
    QrVSMOEnvRVmiDataCad: TDateField;
    QrVSMOEnvRVmiDataAlt: TDateField;
    QrVSMOEnvRVmiUserCad: TIntegerField;
    QrVSMOEnvRVmiUserAlt: TIntegerField;
    QrVSMOEnvRVmiAlterWeb: TSmallintField;
    QrVSMOEnvRVmiAWServerID: TIntegerField;
    QrVSMOEnvRVmiAWStatSinc: TSmallintField;
    QrVSMOEnvRVmiAtivo: TSmallintField;
    QrVSMOEnvRVmiNFEMP_SerNF: TIntegerField;
    QrVSMOEnvRVmiNFEMP_nNF: TIntegerField;
    QrVSMOEnvRVmiValorT: TFloatField;
    PnRetornoMO: TPanel;
    DBGVSMOEnvRet: TdmkDBGridZTO;
    PnItensRetMO: TPanel;
    Splitter1: TSplitter;
    GroupBox7: TGroupBox;
    DBGVSMOEnvRVmi: TdmkDBGridZTO;
    GroupBox8: TGroupBox;
    DBGVSMOEnvGVmi: TdmkDBGridZTO;
    DBEdit34: TDBEdit;
    Label54: TLabel;
    QrVSGerArtNewCusFrtMORet: TFloatField;
    QrVSGerArt015CusFrtMORet: TFloatField;
    QrVSGerArtNewGrandeza: TIntegerField;
    Destagerao1: TMenuItem;
    Devriasgeraes1: TMenuItem;
    Label55: TLabel;
    EdCredPereImposto: TdmkEdit;
    Label56: TLabel;
    EdCredValrImposto: TdmkEdit;
    QrVSGerArtNewCredPereImposto: TFloatField;
    AdicionadoCurimentoMltiplodeMltiplo1: TMenuItem;
    AdicionadoCurimentoIMECnico1: TMenuItem;
    QrVSGerArt015CustoKgInn: TFloatField;
    N5: TMenuItem;
    Corrigeartigogerado1: TMenuItem;
    QrVSGerArt015Marca: TWideStringField;
    PMNovo: TPopupMenu;
    CorrigesaldosdeorigemenotaMPAG1: TMenuItem;
    Corrigevalorderecuperaodeimpostos1: TMenuItem;
    QrVSGerArt015CustoAreaMP: TFloatField;
    QrVSGerArt014CustoAreaCur: TFloatField;
    Label57: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    Label58: TLabel;
    QrVSGerArtNewCustoPQ: TFloatField;
    QrVSGerArt015CustoPQ: TFloatField;
    QrNiv14CustoPQ: TFloatField;
    CkMedEClas: TCheckBox;
    QrVSGerArtNewMedEClas: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSGerArtAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSGerArtBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSGerArtAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSGerArtBeforeClose(DataSet: TDataSet);
    procedure CabLibera1Click(Sender: TObject);
    procedure QrVSGerArtCalcFields(DataSet: TDataSet);
    procedure CabReeditar1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure IMEIArtigodeRibeiragerado1Click(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure Pesquisasgeral1Click(Sender: TObject);
    procedure Pelaobservaosobreaaindustrializao1Click(Sender: TObject);
    procedure BtClassesGeradasClick(Sender: TObject);
    procedure Removeforado1Click(Sender: TObject);
    procedure FluxodoIMEIGerado1Click(Sender: TObject);
    procedure FluxodaFichaRMPOrigemitemselecionado1Click(Sender: TObject);
    procedure Editaantigo1Click(Sender: TObject);
    procedure Quantidades1Click(Sender: TObject);
    procedure QrVSGerArt015BeforeClose(DataSet: TDataSet);
    procedure QrVSGerArt015AfterScroll(DataSet: TDataSet);
    procedure InNaturadeOrigem1Click(Sender: TObject);
    procedure AlteraDatas1Click(Sender: TObject);
    procedure BtTribIncItsClick(Sender: TObject);
    procedure IncluiTributo1Click(Sender: TObject);
    procedure AlteraTributoAtual1Click(Sender: TObject);
    procedure ExcluiTributoAtual1Click(Sender: TObject);
    procedure QrVSGerArtNewBeforeClose(DataSet: TDataSet);
    procedure QrVSGerArtNewAfterScroll(DataSet: TDataSet);
    procedure EdValorMPRedefinido(Sender: TObject);
    procedure EdCustoMOTotRedefinido(Sender: TObject);
    procedure EdCustoMOKgRedefinido(Sender: TObject);
    procedure EdQtdAntPesoRedefinido(Sender: TObject);
    procedure AdicionadoCurimento1Click(Sender: TObject);
    procedure AdicionadoCurimentoMltiplo1Click(Sender: TObject);
    procedure BtVSOutNFeCabClick(Sender: TObject);
    procedure BtVSOutNFeItsClick(Sender: TObject);
    procedure IncluiSerieNumeroDeNFe1Click(Sender: TObject);
    procedure AlteraSerieNumeroDeNFe1Click(Sender: TObject);
    procedure ExcluiSerieNumeroDeNFe1Click(Sender: TObject);
    procedure IncluiitemdeNF1Click(Sender: TObject);
    procedure AlteraitemdeNF1Click(Sender: TObject);
    procedure ExcluiitemdeNF1Click(Sender: TObject);
    procedure QrVSOutNFeCabAfterScroll(DataSet: TDataSet);
    procedure QrVSOutNFeCabBeforeClose(DataSet: TDataSet);
    procedure Alterapesokg1Click(Sender: TObject);
    procedure Atualizacustodescendentesdoitemselecionado1Click(Sender: TObject);
    procedure SbStqCenLocClick(Sender: TObject);
    procedure Incluiatrelamento1Click(Sender: TObject);
    procedure Alteraatrelamento1Click(Sender: TObject);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure QrVSMOEnvRetBeforeClose(DataSet: TDataSet);
    procedure QrVSMOEnvRetAfterScroll(DataSet: TDataSet);
    procedure Destagerao1Click(Sender: TObject);
    procedure Devriasgeraes1Click(Sender: TObject);
    procedure EdCredPereImpostoRedefinido(Sender: TObject);
    procedure AdicionadoCurimentoMltiplodeMltiplo1Click(Sender: TObject);
    procedure AdicionadoCurimentoIMECnico1Click(Sender: TObject);
    procedure QrVSGerArt015CalcFields(DataSet: TDataSet);
    procedure Corrigeartigogerado1Click(Sender: TObject);
    procedure CorrigesaldosdeorigemenotaMPAG1Click(Sender: TObject);
    procedure Corrigevalorderecuperaodeimpostos1Click(Sender: TObject);
  private
    procedure CalculaValorMO();
    procedure CalculaValorT();
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraVSGArtItsBar(SQLType: TSQLType);
    procedure MostraVSGArtItsCurUni(SQLType: TSQLType);
    procedure MostraVSGArtItsCurMul(SQLType: TSQLType);
    procedure MostraVSGArtItsCurMulDeMul(SQLType: TSQLType);
    procedure MostraVSGArtItsCurIMEC(SQLType: TSQLType);
    procedure InsereArtigoRibeira(Codigo, MovimCod, Empresa, GraGruX, ClientMO:
              Integer; DataHora: String);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure AlteraIMEI_Old();
    procedure RemoveItem();
    procedure LiberaClassificacao(SQLType: TSQLType);
    procedure ZZZEditaNovo();
    procedure ReopenVSGerArt14();
    procedure ReopenVSGerArt15(Controle: Integer);
    procedure ReopenTribIncIts(Controle: Integer);
    procedure ReopenVSOutNFeCab(Codigo: Integer);
    procedure ReopenVSOutNFeIts(Controle: Integer);
    procedure SelecionaFormGerArtIts();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //procedure ReopenVSGerArtDst(Qry: TmySQLQuery; MovimCod, Controle: Integer);
    //procedure ReopenVSGerArtSrc(Qry: TmySQLQuery; MovimCod, Controle: Integer;
      //        SQL_Limit: String);
    procedure AtualizaNFeItens();

  end;

var
  FmVSGerArtCab: TFmVSGerArtCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSGerArtItsBar, ModuleGeral,
  VSGerArtEnc, UnVS_CRC_PF, VSMovItsAlt, AppListas, VSGerArtAltQtd, VSGerArtAltInn,
  VSGerArtDatas, VSGerArtItsCurUni, VSGerArtItsCurMul, VSGerArtItsCurMulDeMul,
  ModVS_CRC, VSGerArtItsCurIMEC,
{$IfDef sAllVS} UnVS_PF, UnTributos_PF, {$EndIf}
  GetValor, VSMod, UnVS_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

const
  FGerArX2 = True;

procedure TFmVSGerArtCab.LiberaClassificacao(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  Codigo := QrVSGerArtCodigo.Value;
  //colocar metragem e encerrar!
  if DBCheck.CriaFm(TFmVSGerArtEnc, FmVSGerArtEnc, afmoNegarComAviso) then
  begin
    FmVSGerArtEnc.ImgTipo.SQLType := stUpd;
    //
    FmVSGerArtEnc.EdCodigo.ValueVariant := Codigo;
    FmVSGerArtEnc.EdMovimCod.ValueVariant := QrVSGerArtNewMovimCod.Value;
    FmVSGerArtEnc.EdControle.ValueVariant := QrVSGerArtNewControle.Value;
    //
    FmVSGerArtEnc.EdPecas.ValueVariant      := QrVSGerArtNewPecas.Value;
    FmVSGerArtEnc.EdAreaM2.ValueVariant     := QrVSGerArtNewAreaM2.Value;
    FmVSGerArtEnc.EdAreaP2.ValueVariant     := QrVSGerArtNewAreaP2.Value;
    FmVSGerArtEnc.EdCustoMOKg.ValueVariant  := QrVSGerArtNewCustoMOKg.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
    FmVSGerArtEnc.EdCustoMOTot.ValueVariant := QrVSGerArtNewCustoMOTot.Value;
    FmVSGerArtEnc.EdValorMP.ValueVariant    := QrVSGerArtNewValorMP.Value;
    FmVSGerArtEnc.EdValorT.ValueVariant     := QrVSGerArtNewValorT.Value;

    //
    FmVSGerArtEnc.EdPecasMan.ValueVariant      := QrVSGerArtPecasMan.Value;
    FmVSGerArtEnc.EdAreaManM2.ValueVariant     := QrVSGerArtAreaManM2.Value;
    FmVSGerArtEnc.EdAreaManP2.ValueVariant     := QrVSGerArtAreaManP2.Value;
    //
    FmVSGerArtEnc.EdCustoManMOKg.ValueVariant  := QrVSGerArtCustoManMOKg.Value;
    FmVSGerArtEnc.EdCustoManMOTot.ValueVariant := QrVSGerArtCustoManMOTot.Value;
    FmVSGerArtEnc.EdValorManMP.ValueVariant    := QrVSGerArtValorManMP.Value;
    FmVSGerArtEnc.EdValorManT.ValueVariant     := QrVSGerArtValorManT.Value;
    //
    if SQLType = stIns then
    begin
      FmVSGerArtEnc.BtOKClick(FmVSGerArtEnc.BtOK);
    end else
    begin
      FmVSGerArtEnc.ShowModal;
    end;
    //
    LocCod(Codigo, Codigo);
    FmVSGerArtEnc.Destroy;
  end;
end;

procedure TFmVSGerArtCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSGerArtCab.MostraVSGArtItsBar(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSGerArtItsBar, FmVSGerArtItsBar, afmoNegarComAviso) then
  begin
    FmVSGerArtItsBar.ImgTipo.SQLType := SQLType;
    FmVSGerArtItsBar.FQrCab        := QrVSGerArt;
    FmVSGerArtItsBar.FDsCab        := Dsvsgerart;
    FmVSGerArtItsBar.FQrIts        := QrVSGerArt015;
    FmVSGerArtItsBar.FEmpresa      := QrVSGerArtEmpresa.Value;
    FmVSGerArtItsBar.FClientMO     := QrVSGerArtNewClientMO.Value;
    FmVSGerArtItsBar.FFornecMO     := QrVSGerArtNewFornecMO.Value;
    FmVSGerArtItsBar.FTipoArea     := QrVSGerArtTipoArea.Value;
    FmVSGerArtItsBar.FOrigMovimNiv := TEstqMovimNiv(QrVSGerArtNewMovimNiv.Value);
    FmVSGerArtItsBar.FOrigMovimCod := QrVSGerArtNewMovimCod.Value;
    FmVSGerArtItsBar.FOrigCodigo   := QrVSGerArtNewCodigo.Value;
    FmVSGerArtItsBar.FNewGraGruX   := QrVSGerArtNewGraGruX.Value;
    FmVSGerArtItsBar.FCustoMOKg    := QrVSGerArtNewCustoMOKg.Value;
    FmVSGerArtItsBar.FCredPereImposto := QrVSGerArtNewCredPereImposto.Value;
    //
    FmVSGerArtItsBar.EdCodigo.ValueVariant    := QrVSGerArtCodigo.Value;
    FmVSGerArtItsBar.EdMovimCod.ValueVariant  := QrVSGerArtMovimCod.Value;
    //
    FmVSGerArtItsBar.EdSrcMovID.ValueVariant  := QrVSGerArtNewMovimID.Value;
    FmVSGerArtItsBar.EdSrcNivel1.ValueVariant := QrVSGerArtNewCodigo.Value;
    FmVSGerArtItsBar.EdSrcNivel2.ValueVariant := QrVSGerArtNewControle.Value;
    FmVSGerArtItsBar.EdSrcGGX.ValueVariant    := QrVSGerArtNewGraGruX.Value;
    //
    FmVSGerArtItsBar.ReopenItensAptos();
    FmVSGerArtItsBar.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      // N�o tem stIns, mas se e quando tiver...
      FmVSGerArtItsBar.FDataHora               := QrVSGerArtDtHrAberto.Value;
    end else
    begin
      FmVSGerArtItsBar.EdMovimTwn.ValueVariant := QrVSGerArtNewMovimTwn.Value;
{
      FmVSGerArtItsBar.EdControle.ValueVariant := QrVSGerArt015Controle.Value;
      //
      FmVSGerArtItsBar.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSGerArt015CNPJ_CPF.Value);
      FmVSGerArtItsBar.EdNomeEmiSac.Text := QrVSGerArt015Nome.Value;
      FmVSGerArtItsBar.EdCPF1.ReadOnly := True;
}
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSGerArtNewDataHora.Value,
        QrVSGerArtNewDtCorrApo.Value,  FmVSGerArtItsBar.FDataHora);
    end;
    FmVSGerArtItsBar.ShowModal;
    FmVSGerArtItsBar.Destroy;
    //
    if (SQLType = stIns) and (QrVSGerArtDtHrLibCla.Value < 2) then
      LiberaClassificacao(stIns);
  end;
end;

procedure TFmVSGerArtCab.MostraVSGArtItsCurUni(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSGerArtItsCurUni, FmVSGerArtItsCurUni, afmoNegarComAviso) then
  begin
    FmVSGerArtItsCurUni.ImgTipo.SQLType := SQLType;
    FmVSGerArtItsCurUni.FQrCab        := QrVSGerArt;
    FmVSGerArtItsCurUni.FDsCab        := Dsvsgerart;
    FmVSGerArtItsCurUni.FQrIts        := QrVSGerArt015;
    FmVSGerArtItsCurUni.FEmpresa      := QrVSGerArtEmpresa.Value;
    FmVSGerArtItsCurUni.FClientMO     := QrVSGerArtNewClientMO.Value;
    FmVSGerArtItsCurUni.FFornecMO     := QrVSGerArtNewFornecMO.Value;
    FmVSGerArtItsCurUni.FTipoArea     := QrVSGerArtTipoArea.Value;
    FmVSGerArtItsCurUni.FOrigMovimNiv := TEstqMovimNiv(QrVSGerArtNewMovimNiv.Value);
    FmVSGerArtItsCurUni.FOrigMovimCod := QrVSGerArtNewMovimCod.Value;
    FmVSGerArtItsCurUni.FOrigCodigo   := QrVSGerArtNewCodigo.Value;
    FmVSGerArtItsCurUni.FNewGraGruX   := QrVSGerArtNewGraGruX.Value;
    FmVSGerArtItsCurUni.FCustoMOKg    := QrVSGerArtNewCustoMOKg.Value;
    //CustoMOKg??
    //
    FmVSGerArtItsCurUni.EdCodigo.ValueVariant    := QrVSGerArtCodigo.Value;
    FmVSGerArtItsCurUni.EdMovimCod.ValueVariant  := QrVSGerArtMovimCod.Value;
    //
    FmVSGerArtItsCurUni.EdSrcMovID.ValueVariant  := QrVSGerArtNewMovimID.Value;
    FmVSGerArtItsCurUni.EdSrcNivel1.ValueVariant := QrVSGerArtNewCodigo.Value;
    FmVSGerArtItsCurUni.EdSrcNivel2.ValueVariant := QrVSGerArtNewControle.Value;
    FmVSGerArtItsCurUni.EdSrcGGX.ValueVariant    := QrVSGerArtNewGraGruX.Value;
    //
    FmVSGerArtItsCurUni.ReopenItensAptos();
    FmVSGerArtItsCurUni.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      FmVSGerArtItsCurUni.FDataHora                := QrVSGerArtDtHrAberto.Value;
      //
    end else
    begin
      FmVSGerArtItsCurUni.EdMovimTwn.ValueVariant := QrVSGerArtNewMovimTwn.Value;
      //
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSGerArtNewDataHora.Value,
        QrVSGerArtNewDtCorrApo.Value,  FmVSGerArtItsCurUni.FDataHora);
    end;
    FmVSGerArtItsCurUni.ShowModal;
    FmVSGerArtItsCurUni.Destroy;
    //
    if (SQLType = stIns) and (QrVSGerArtDtHrLibCla.Value < 2) then
      LiberaClassificacao(stIns);
  end;
end;

procedure TFmVSGerArtCab.MostraVSGArtItsCurIMEC(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSGerArtItsCurIMEC, FmVSGerArtItsCurIMEC, afmoNegarComAviso) then
  begin
    FmVSGerArtItsCurIMEC.ImgTipo.SQLType := SQLType;
    FmVSGerArtItsCurIMEC.FQrCab        := QrVSGerArt;
    FmVSGerArtItsCurIMEC.FDsCab        := Dsvsgerart;
    FmVSGerArtItsCurIMEC.FQrIts        := QrVSGerArt015;
    FmVSGerArtItsCurIMEC.FEmpresa      := QrVSGerArtEmpresa.Value;
    FmVSGerArtItsCurIMEC.FClientMO     := QrVSGerArtNewClientMO.Value;
    FmVSGerArtItsCurIMEC.FFornecMO     := QrVSGerArtNewFornecMO.Value;
    FmVSGerArtItsCurIMEC.FTipoArea     := QrVSGerArtTipoArea.Value;
    FmVSGerArtItsCurIMEC.FOrigMovimNiv := TEstqMovimNiv(QrVSGerArtNewMovimNiv.Value);
    FmVSGerArtItsCurIMEC.FOrigMovimCod := QrVSGerArtNewMovimCod.Value;
    FmVSGerArtItsCurIMEC.FOrigCodigo   := QrVSGerArtNewCodigo.Value;
    FmVSGerArtItsCurIMEC.FNewGraGruX   := QrVSGerArtNewGraGruX.Value;
    FmVSGerArtItsCurIMEC.FCustoMOKg    := QrVSGerArtNewCustoMOKg.Value;
    //CustoMOKg?
    //
    FmVSGerArtItsCurIMEC.EdCodigo.ValueVariant    := QrVSGerArtCodigo.Value;
    FmVSGerArtItsCurIMEC.EdMovimCod.ValueVariant  := QrVSGerArtMovimCod.Value;
    //
    FmVSGerArtItsCurIMEC.EdSrcMovID.ValueVariant  := QrVSGerArtNewMovimID.Value;
    FmVSGerArtItsCurIMEC.EdSrcNivel1.ValueVariant := QrVSGerArtNewCodigo.Value;
    FmVSGerArtItsCurIMEC.EdSrcNivel2.ValueVariant := QrVSGerArtNewControle.Value;
    FmVSGerArtItsCurIMEC.EdSrcGGX.ValueVariant    := QrVSGerArtNewGraGruX.Value;
    //
    FmVSGerArtItsCurIMEC.CkMedEClas.Checked       := Geral.IntToBool(QrVSGerArtNewMedEClas.Value);
    //
    FmVSGerArtItsCurIMEC.ReopenItensAptos();
    FmVSGerArtItsCurIMEC.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      FmVSGerArtItsCurIMEC.FDataHora              := QrVSGerArtDtHrAberto.Value;
    end else
    begin
      FmVSGerArtItsCurIMEC.EdMovimTwn.ValueVariant := QrVSGerArtNewMovimTwn.Value;
      //
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSGerArtNewDataHora.Value,
        QrVSGerArtNewDtCorrApo.Value,  FmVSGerArtItsCurIMEC.FDataHora);
    end;
    FmVSGerArtItsCurIMEC.ShowModal;
    FmVSGerArtItsCurIMEC.Destroy;
    //
    if (SQLType = stIns) and (QrVSGerArtDtHrLibCla.Value < 2) then
      LiberaClassificacao(stIns);
  end;
end;

procedure TFmVSGerArtCab.MostraVSGArtItsCurMul(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSGerArtItsCurMul, FmVSGerArtItsCurMul, afmoNegarComAviso) then
  begin
    FmVSGerArtItsCurMul.ImgTipo.SQLType := SQLType;
    FmVSGerArtItsCurMul.FQrCab        := QrVSGerArt;
    FmVSGerArtItsCurMul.FDsCab        := Dsvsgerart;
    FmVSGerArtItsCurMul.FQrIts        := QrVSGerArt015;
    FmVSGerArtItsCurMul.FEmpresa      := QrVSGerArtEmpresa.Value;
    FmVSGerArtItsCurMul.FClientMO     := QrVSGerArtNewClientMO.Value;
    FmVSGerArtItsCurMul.FFornecMO     := QrVSGerArtNewFornecMO.Value;
    FmVSGerArtItsCurMul.FTipoArea     := QrVSGerArtTipoArea.Value;
    FmVSGerArtItsCurMul.FOrigMovimNiv := TEstqMovimNiv(QrVSGerArtNewMovimNiv.Value);
    FmVSGerArtItsCurMul.FOrigMovimCod := QrVSGerArtNewMovimCod.Value;
    FmVSGerArtItsCurMul.FOrigCodigo   := QrVSGerArtNewCodigo.Value;
    FmVSGerArtItsCurMul.FNewGraGruX   := QrVSGerArtNewGraGruX.Value;
    FmVSGerArtItsCurMul.FCustoMOKg    := QrVSGerArtNewCustoMOKg.Value;
    //CustoMOKg?
    //
    FmVSGerArtItsCurMul.EdCodigo.ValueVariant    := QrVSGerArtCodigo.Value;
    FmVSGerArtItsCurMul.EdMovimCod.ValueVariant  := QrVSGerArtMovimCod.Value;
    //
    FmVSGerArtItsCurMul.EdSrcMovID.ValueVariant  := QrVSGerArtNewMovimID.Value;
    FmVSGerArtItsCurMul.EdSrcNivel1.ValueVariant := QrVSGerArtNewCodigo.Value;
    FmVSGerArtItsCurMul.EdSrcNivel2.ValueVariant := QrVSGerArtNewControle.Value;
    FmVSGerArtItsCurMul.EdSrcGGX.ValueVariant    := QrVSGerArtNewGraGruX.Value;
    //
    FmVSGerArtItsCurMul.ReopenItensAptos();
    FmVSGerArtItsCurMul.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      FmVSGerArtItsCurMul.FDataHora              := QrVSGerArtDtHrAberto.Value;
    end else
    begin
      FmVSGerArtItsCurMul.EdMovimTwn.ValueVariant := QrVSGerArtNewMovimTwn.Value;
      //
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSGerArtNewDataHora.Value,
        QrVSGerArtNewDtCorrApo.Value,  FmVSGerArtItsCurMul.FDataHora);
    end;
    FmVSGerArtItsCurMul.ShowModal;
    FmVSGerArtItsCurMul.Destroy;
    //
    if (SQLType = stIns) and (QrVSGerArtDtHrLibCla.Value < 2) then
      LiberaClassificacao(stIns);
  end;
end;

procedure TFmVSGerArtCab.MostraVSGArtItsCurMulDeMul(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSGerArtItsCurMulDeMul, FmVSGerArtItsCurMulDeMul, afmoNegarComAviso) then
  begin
    FmVSGerArtItsCurMulDeMul.ImgTipo.SQLType := SQLType;
    FmVSGerArtItsCurMulDeMul.FQrCab        := QrVSGerArt;
    FmVSGerArtItsCurMulDeMul.FDsCab        := Dsvsgerart;
    FmVSGerArtItsCurMulDeMul.FQrIts        := QrVSGerArt015;
    FmVSGerArtItsCurMulDeMul.FEmpresa      := QrVSGerArtEmpresa.Value;
    FmVSGerArtItsCurMulDeMul.FClientMO     := QrVSGerArtNewClientMO.Value;
    FmVSGerArtItsCurMulDeMul.FFornecMO     := QrVSGerArtNewFornecMO.Value;
    FmVSGerArtItsCurMulDeMul.FTipoArea     := QrVSGerArtTipoArea.Value;
    FmVSGerArtItsCurMulDeMul.FOrigMovimNiv := TEstqMovimNiv(QrVSGerArtNewMovimNiv.Value);
    FmVSGerArtItsCurMulDeMul.FOrigMovimCod := QrVSGerArtNewMovimCod.Value;
    FmVSGerArtItsCurMulDeMul.FOrigCodigo   := QrVSGerArtNewCodigo.Value;
    FmVSGerArtItsCurMulDeMul.FNewGraGruX   := QrVSGerArtNewGraGruX.Value;
    FmVSGerArtItsCurMulDeMul.FCustoMOKg    := QrVSGerArtNewCustoMOKg.Value;
    //CustoMOKg?
    //
    FmVSGerArtItsCurMulDeMul.EdCodigo.ValueVariant    := QrVSGerArtCodigo.Value;
    FmVSGerArtItsCurMulDeMul.EdMovimCod.ValueVariant  := QrVSGerArtMovimCod.Value;
    //
    FmVSGerArtItsCurMulDeMul.EdSrcMovID.ValueVariant  := QrVSGerArtNewMovimID.Value;
    FmVSGerArtItsCurMulDeMul.EdSrcNivel1.ValueVariant := QrVSGerArtNewCodigo.Value;
    FmVSGerArtItsCurMulDeMul.EdSrcNivel2.ValueVariant := QrVSGerArtNewControle.Value;
    FmVSGerArtItsCurMulDeMul.EdSrcGGX.ValueVariant    := QrVSGerArtNewGraGruX.Value;
    //
    FmVSGerArtItsCurMulDeMul.ReopenItensAptos();
    FmVSGerArtItsCurMulDeMul.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      FmVSGerArtItsCurMulDeMul.FDataHora              := QrVSGerArtDtHrAberto.Value;
    end else
    begin
      FmVSGerArtItsCurMulDeMul.EdMovimTwn.ValueVariant := QrVSGerArtNewMovimTwn.Value;
      //
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSGerArtNewDataHora.Value,
        QrVSGerArtNewDtCorrApo.Value,  FmVSGerArtItsCurMulDeMul.FDataHora);
    end;
    FmVSGerArtItsCurMulDeMul.ShowModal;
    FmVSGerArtItsCurMulDeMul.Destroy;
    //
    if (SQLType = stIns) and (QrVSGerArtDtHrLibCla.Value < 2) then
      LiberaClassificacao(stIns);
  end;
end;

procedure TFmVSGerArtCab.Outrasimpresses1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSGerArtCab.Pelaobservaosobreaaindustrializao1Click(
  Sender: TObject);
begin
  LocCod(QrVSGerArtCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vsgerarta', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmVSGerArtCab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSGerArtCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSGerArtCab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_CRC_PF.LocalizaPeloIMEC(emidIndsXX);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSGerArtCab.PeloIMEI1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_CRC_PF.LocalizaPeloIMEI(emidIndsXX);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSGerArtCab.Pesquisasgeral1Click(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_CRC_PF.MostraFormVSRMPPsq(emidIndsXX, [], stPsq, Codigo, Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    QrVSGerArt015.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSGerArtCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSGerArt);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSGerArt, (*QrVSGerArtNew,*)
  QrVSGerArt015);
  MyObjects.HabilitaMenuItemCabUpd(CabLibera1, QrVSGerArt);
  MyObjects.HabilitaMenuItemCabUpd(CabReeditar1, QrVSGerArt);
  if QrVSGerArtDtHrFimCla.Value > 2 then
  begin
    CabLibera1.Enabled := False;
    CabReeditar1.Enabled := False;
  end else
  if QrVSGerArtDtHrLibCla.Value < 2 then
  begin
    CabReeditar1.Enabled := False;
  end;
end;

procedure TFmVSGerArtCab.PMItsPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSGerArt);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSGerArt015);
  MyObjects.HabilitaMenuItemCabDel(ItsExclui1, QrVSGerArt015, QrVSMOEnvRet);
  if (QrVSGerArtDtHrLibCla.Value > 2) or (QrVSGerArtDtHrFimCla.Value > 2) then
  begin
    //ItsInclui1.Enabled := False;
    //ItsAltera1.Enabled := False;
    ItsExclui1.Enabled := False;
  end;
  AdicionadoCurimento1.Enabled := ItsInclui1.Enabled;
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrVSGerArt015);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrVSMOEnvRet);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrVSMOEnvRet);
  //
end;

procedure TFmVSGerArtCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSGerArtCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSGerArtCab.DefParams;
begin
  VAR_GOTOTABELA := 'vsgerarta';
  VAR_GOTOMYSQLTABLE := QrVSGerArt;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vga.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA');
  VAR_SQLx.Add('FROM vsgerarta vga');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=vga.Empresa');
  VAR_SQLx.Add('WHERE vga.Codigo > 0');
  //
  VAR_SQL1.Add('AND vga.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND vga.Nome Like :P0');
  //
end;

procedure TFmVSGerArtCab.Destagerao1Click(Sender: TObject);
begin
  VS_CRC_PF.ImprimeClassIMEI(QrVSGerArtNewControle.Value, QrVSGerArtCodigo.Value, 0);
end;

procedure TFmVSGerArtCab.Devriasgeraes1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSCGICab(0);
end;

procedure TFmVSGerArtCab.EdCredPereImpostoRedefinido(Sender: TObject);
begin
  CalculaValorMO();
end;

procedure TFmVSGerArtCab.EdCustoMOKgRedefinido(Sender: TObject);
begin
  CalculaValorMO();
end;

procedure TFmVSGerArtCab.EdCustoMOTotRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSGerArtCab.Editaantigo1Click(Sender: TObject);
begin
  AlteraIMEI_Old();
  //



{

// Parei Aqui!

// Encontrar o Gemeo de baixa de In Natura e o controle (SrcNivel2) do In Natura
// Result.Controle = gemeo e Result.SrcNivel2 = In Natura de origem

SELECT * FROM v s m o v i t s
WHERE MovimTwn=4482
AND MovimNiv=15
    // Se alterar Pecas tem que alterar recalcular o saldo do In Natura de origem
    // alterando tambem o IMEI Gemeo MovimID=15
    ... codigo...

    // Se alterar IMEI de origem tem que recalcular o In Natura de Origem
    // anterior, o IMEI novo, o IMEI do Artigo Gerado e TODOS IMEIs descendentes
    // do IMEI de Artigo Gerado!!!!!


  Para os descendentes diretos!

SELECT Codigo, Controle, MovimCod, MovimID,
MovimNiv, MovimTwn, Terceiro, SerieFch, Ficha,
Pallet, GraGruX, Pecas,
SrcMovID, SrcNivel2, DstMovID, DstNivel2
FROM v s m o v i t s
WHERE MovimTwn <> 0
AND MovimTwn IN (
     SELECT MovimTwn
     FROM v s m o v i t s
     WHERE SrcNivel2=11983
)

  Para os descendentes dos descendentes

  depois...
  SELECT *
FROM v s m o v i t s
WHERE SrcNivel2=11993

SELECT *
FROM v s m o v i t s
WHERE MovimCod <> 0
AND MovimCod IN (
  SELECT MovimCod
  FROM v s m o v i t s
  WHERE Controle=12018
)
}

end;

procedure TFmVSGerArtCab.EdQtdAntPesoRedefinido(Sender: TObject);
begin
  CalculaValorMO();
end;

procedure TFmVSGerArtCab.EdValorMPRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmVSGerArtCab.ExcluiAtrelamento1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  DmModVS_CRC.ExcluiAtrelamentoMOEnvRet(QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI);
  //
  Codigo := QrVSGerArtCodigo.Value;
  LocCod(Codigo, Codigo);
end;

procedure TFmVSGerArtCab.ExcluiitemdeNF1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'vsoutnfeIts', 'Controle', QrVSOutNFeItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSOutNFeIts,
      QrVSOutNFeItsControle, QrVSOutNFeItsControle.Value);
    ReopenVSOutNFeIts(Controle);
  end;
end;

procedure TFmVSGerArtCab.ExcluiSerieNumeroDeNFe1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'vsoutnfecab', 'Codigo', QrVSOutNFeCabCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSOutNFeCab,
      QrVSOutNFeCabCodigo, QrVSOutNFeCabCodigo.Value);
    ReopenVSOutNFeCab(Codigo);
  end;
end;

procedure TFmVSGerArtCab.ExcluiTributoAtual1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrTribIncItsControle.Value;
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o deste item de tributo?',
  'tribincits', 'Controle', Controle, DMod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrTribIncIts,
      QrTribIncItsControle, QrTribIncItsControle.Value);
    ReopenTribIncIts(Controle);
  end;
end;

procedure TFmVSGerArtCab.CabLibera1Click(Sender: TObject);
begin
  LiberaClassificacao(stUpd);
end;

procedure TFmVSGerArtCab.CabReeditar1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSGerArtCodigo.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
  'DtHrLibCla'], ['Codigo'], [
  '0000-00-00'], [Codigo], True) then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSGerArtCab.CalculaValorMO();
var
  CustoMOTot, Pecas, QtdAntPeso, CustoMOKg, CredPereImposto, CredValrImposto: Double;
begin
  QtdAntPeso      := EdQtdAntPeso.ValueVariant;
  CustoMOKg       := EdCustoMOKg.ValueVariant;
  CredPereImposto := EdCredPereImposto.ValueVariant;
  CustoMOTot      := QtdAntPeso * CustoMOKg;
  CredValrImposto := CustoMOTot * CredPereImposto / 100;
  EdCustoMOTot.ValueVariant := CustoMOTot;
  EdCredValrImposto.ValueVariant := CredValrImposto;
end;

procedure TFmVSGerArtCab.CalculaValorT();
var
  ValorMP, CustoMOTot, ValorT, CredValrImposto: Double;
begin
  ValorMP         := EdValorMP.ValueVariant;
  CustoMOTot      := EdCustoMOTot.ValueVariant;
  CredValrImposto := EdCredValrImposto.ValueVariant;
  ///
  EdValorT.ValueVariant := ValorMP + CustoMOTot - CredValrImposto;
end;

procedure TFmVSGerArtCab.Corrigeartigogerado1Click(Sender: TObject);
var
  GGXOri_TXT, SQL: String;
  Itens: Integer;
begin
  GGXOri_TXT := Geral.FF0(QrVSGerArtNewGraGruX.Value);
  Itens := 0;
  QrVSGerArt015.First;
  while not QrVSGerArt015.Eof do
  begin
    if QrVSGerArt015GraGruX.Value <> QrVSGerArtNewGraGruX.Value then
    begin
      itens := Itens + 1;
      SQL := 'UPDATE vsmovits SET DstGGX=' + GGXOri_TXT +
      ' WHERE Controle=' + Geral.FF0(QrVSGerArt015Controle.Value);
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL);
    end;
    //
    QrVSGerArt015.Next;
  end;
  if Itens > 0 then
    ReopenVSGerArt15(0);
  case Itens of
    0: Geral.MB_Info('Nenhum item precisou ser corrigido!');
    1: Geral.MB_Info('Um item foi corrigido!');
    else Geral.MB_Info(Geral.FF0(Itens) + ' foram corrigidos!');
  end;
end;

procedure TFmVSGerArtCab.CorrigesaldosdeorigemenotaMPAG1Click(Sender: TObject);
var
  Qry1, Qry2, Qry3: TmySQLQuery;
  MovimID,
  MovimTwn, MovimNiv, MovimCod, Codigo, Controle, Itens, GraGruX, DstGGX: Integer;
  Gera: Boolean;
  NotaMPAG, Pecas, PesoKg, QtdGerArM2, FatNotaVNC, FatNotaVRC: Double;
begin
  if Geral.MB_Pergunta('Ser� corrigido o saldo de origem e Nota MPAG de todos itens.' +
  sLineBreak + 'Deseja continuar?') <> ID_YES then Exit;
  //
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
(*
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSGerArtCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
*)
  Itens := 0;
  Qry1 := TmySQLQuery.Create(Dmod);
  try
  Qry2 := TmySQLQuery.Create(Dmod);
  try
  Qry3 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE  ',
    '(MovimID = 1 AND MovimNiv = 0) ',
    'OR ',
    //'(MovimID = 6 AND MovimNiv = 15) ',
    //'OR ',
    '(MovimID = 6 AND MovimNiv = 14) ',
    'ORDER BY MovimID DESC ',
    '']);
    if Qry1.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max := Qry1.RecordCount;
      //
      Qry1.First;
      while not Qry1.Eof do
      begin
        MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
        //
        MovimID    := Qry1.FieldByName('MovimID').AsInteger;
        MovimNiv   := Qry1.FieldByName('MovimNiv').AsInteger;
        Controle   := Qry1.FieldByName('Controle').AsInteger;
        MovimCod   := Qry1.FieldByName('MovimCod').AsInteger;
        Codigo     := Qry1.FieldByName('Codigo').AsInteger;
        MovimTwn   := Qry1.FieldByName('MovimTwn').AsInteger;
        //
        if MovimID = 1 then
        begin
          //
          //VS_CRC_PF.DistribuiCustoIndsVS(TEstqMovimNiv(MovimNiv), MovimCod, Codigo, Controle);
          Gera := MovimID = 1;
          VS_CRC_PF.AtualizaSaldoVirtualVSMovIts(Controle, Gera);
          Itens := Itens + 1;
        end else
        begin
          if MovimNiv = 14 then
          begin
            //pegar o 15 do Twn e atualiza-lo
            UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
            'SELECT Controle, GraGruX, DstGGX, ',
            'Pecas, PesoKg, QtdGerArM2 ',
            'FROM ' + CO_SEL_TAB_VMI,
            'WHERE MovimID=6 ',
            'AND MovimNiv=15 ',
            'AND MovimTwn=' + Geral.FF0(MovimTwn),
            '']);
            if Qry2.RecordCount > 0 then
            begin
              Pecas       := Qry2.FieldByName('Pecas').AsInteger;
              PesoKg      := Qry2.FieldByName('PesoKg').AsInteger;
              QtdGerArM2  := Qry2.FieldByName('QtdGerArM2').AsInteger;
              GraGruX     := Qry2.FieldByName('GraGruX').AsInteger;
              //DstGGX      := Qry2.FieldByName('DstGGX').AsInteger;
              FatNotaVNC  := VS_CRC_PF.FatorNotaMP(GraGruX);
              //pegar o 15 do Twn e atualiza-lo
              UnDmkDAC_PF.AbreMySQLQuery0(Qry3, Dmod.MyDB, [
              'SELECT GraGruX ',
              'FROM ' + CO_SEL_TAB_VMI,
              'WHERE MovimCod=' + Geral.FF0(MovimCod),
              'AND MovimNiv=13 ',
              '']);
              if Qry3.RecordCount > 0 then
              begin
                DstGGX      := Qry3.FieldByName('GraGruX').AsInteger;
                //
                FatNotaVRC  := VS_CRC_PF.FatorNotaAR(DstGGX);
                NotaMPAG    := VS_CRC_PF.NotaCouroRibeiraApuca(
                  -Pecas, -PesoKg, -QtdGerArM2, FatNotaVNC, FatNotaVRC);
                VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
                //
                Controle     := Qry2.FieldByName('Controle').AsInteger;
                VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
              end;
              Itens := Itens + 1;
            end;
          end;
        end;
        Qry1.Next;
      end;
      //
      Geral.MB_Info(Geral.FF0(Itens) + ' itens de ' + Geral.FF0(
        Qry1.RecordCount) + ' foram atualizados!');
    end else
      Geral.MB_Info('Nenhum item precisou ser atualizado!');
  finally
    Qry3.Free;
  end;
  finally
    Qry2.Free;
  end;
  finally
    Qry1.Free;
  end;
end;

procedure TFmVSGerArtCab.Corrigevalorderecuperaodeimpostos1Click(
  Sender: TObject);
begin
  if Geral.MB_Pergunta('Ser� corrigido o valor de recupera��o de tributos de todos itens com valor zerado e al�quota diferente de zero.' +
  sLineBreak + 'Deseja continuar?') <> ID_YES then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
  UndmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
  'UPDATE vsmovits  ',
  'SET CredValrImposto =  ',
  '  (CustoMOKg * CredPereImposto / 100) ',
  '  * QtdAntPeso ',
  'WHERE MovimID=6 ',
  'AND MovimNiv=13 ',
  'AND CredPereImposto <> 0 ',
  'AND CredValrImposto = 0 ',
  '']));
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSGerArtCab.IMEIArtigodeRibeiragerado1Click(Sender: TObject);
const
  LPFMO   = '';
  NFeRem = '';
begin
  VS_CRC_PF.ImprimeIMEI([QrVSGerArtNewControle.Value], viikArtigoGerado, LPFMO,
    NFeRem, QrVSGerArt);
end;

procedure TFmVSGerArtCab.Incluiatrelamento1Click(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
{$IfDef sAllVS}
  //PCItens.ActivePageIndex := 2;
  PCItens.ActivePage := TsRetornoMO;
  //
  MovimCod := VS_CRC_PF.ObtemMovimCodDeControle(QrVSGerArt015SrcNivel2.Value);
  //
  VS_PF.MostraFormVSMOEnvRet(stIns, QrVSGerArtNew, QrVSGerArt014, QrVSGerArt015,
  QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI, MovimCod, 0, 0, siPositivo, FGerArX2);
  //
  Codigo := QrVSGerArtCodigo.Value;
  LocCod(Codigo, Codigo);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSGerArtCab.IncluiitemdeNF1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSNFeOutIts(stIns, QrVSOutNfeIts, QrTribIncIts,
    QrVSOutNfeCabCodigo.Value, QrVSOutNfeCabMovimCod.Value,
      QrVSGerArtEmpresa.Value, QrVSGerArtDtHrLibCla.Value,
      siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
      QrVSGerArtMovimCod.Value, TEstqMovimID.emidIndsXX,
      [eminBaixCurtiXX], [eminDestCurtiXX, eminSorcCurtiXX]);
end;

procedure TFmVSGerArtCab.IncluiSerieNumeroDeNFe1Click(Sender: TObject);
const
  Codigo    = 0;
  ide_serie = 0;
  ide_nNF   = 0;
begin
  if VS_CRC_PF.MostraFormVSNFeOutCab(stIns, QrVSOutNfeCab, QrVSGerArtMovimCod.Value,
  QrVSGerArtCodigo.Value, Codigo, ide_serie, ide_nNF) then
    VS_CRC_PF.MostraFormVSNFeOutIts(stIns, QrVSOutNfeIts, QrTribIncIts,
      QrVSOutNfeCabCodigo.Value, QrVSOutNfeCabMovimCod.Value,
      QrVSGerArtEmpresa.Value, QrVSGerArtDtHrLibCla.Value,
      siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSGerArtMovimCod.Value, TEstqMovimID.emidIndsXX,
  [eminBaixCurtiXX], [eminDestCurtiXX, eminSorcCurtiXX]);
end;

procedure TFmVSGerArtCab.IncluiTributo1Click(Sender: TObject);
const
  Controle = 0;
  Operacao = 0;
  Tributo  = 0;
  Percent  = 0.00;
  VUsoCalc = 0.00;
  ValrTrib = 0.00;
var
  FatNum, FatParcela, Empresa: Integer;
  Data, Hora: TDateTime;
  ValorFat, BaseCalc: Double;
begin
  {$IfDef sAllVS}
  ValorFat  := QrVSGerArtNewCustoMOTot.Value;
  BaseCalc  := QrVSGerArtNewCustoMOTot.Value;
  //
  FatNum     := QrVSGerArtNewCodigo.Value;
  FatParcela := QrVSGerArtNewControle.Value;
  Empresa    := QrVSGerArtNewEmpresa.Value;
  Data       := QrVSGerArtNewDataHora.Value;
  Hora       := QrVSGerArtNewDataHora.Value;
  //
  Tributos_PF.MostraFormTribIncIts(stIns, QrTribIncIts, VAR_FATID_1020,
  FatNum, FatParcela, Empresa, Controle, Data, Hora, Operacao, Tributo,
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib, siPositivo);

  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSGerArtCab.InNaturadeOrigem1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVSGerArtAltInn, FmVSGerArtAltInn, afmoNegarComAviso) then
  begin
    FmVSGerArtAltInn.ImgTipo.SQLType := stUpd;
    FmVSGerArtAltInn.FQrCab          := QrVSGerArt;
    FmVSGerArtAltInn.FDsCab          := DsVSGerArt;
    FmVSGerArtAltInn.FQrIts          := QrVSGerArt015;
    FmVSGerArtAltInn.FEmpresa        := QrVSGerArtEmpresa.Value;
    FmVSGerArtAltInn.FClientMO       := QrVSGerArtNewClientMO.Value;
    FmVSGerArtAltInn.FFornecMO       := QrVSGerArtNewFornecMO.Value;
    FmVSGerArtAltInn.FStqCenLoc      := QrVSGerArtNewStqCenLoc.Value;
    FmVSGerArtAltInn.FTipoArea       := QrVSGerArtTipoArea.Value;
    FmVSGerArtAltInn.FOrigMovimNiv   := TEstqMovimNiv(QrVSGerArtNewMovimNiv.Value);
    FmVSGerArtAltInn.FOrigMovimCod   := QrVSGerArtNewMovimCod.Value;
    FmVSGerArtAltInn.FOrigCodigo     := QrVSGerArtNewCodigo.Value;
    FmVSGerArtAltInn.FNewGraGruX     := QrVSGerArtNewGraGruX.Value;
    FmVSGerArtAltInn.FAnt013SerieFch := QrVSGerArtNewSerieFch.Value;
    FmVSGerArtAltInn.FAnt013Ficha    := QrVSGerArtNewFicha.Value;
    FmVSGerArtAltInn.FAnt013Terceiro := QrVSGerArtNewTerceiro.Value;
    FmVSGerArtAltInn.FAntMarca       := QrVSGerArtNewMarca.Value;
    //
    FmVSGerArtAltInn.EdCodigo.ValueVariant    := QrVSGerArtCodigo.Value;
    FmVSGerArtAltInn.EdMovimCod.ValueVariant  := QrVSGerArtMovimCod.Value;
    //
    FmVSGerArtAltInn.EdSrcMovID.ValueVariant  := QrVSGerArtNewMovimID.Value;
    FmVSGerArtAltInn.EdSrcNivel1.ValueVariant := QrVSGerArtNewCodigo.Value;
    FmVSGerArtAltInn.EdSrcNivel2.ValueVariant := QrVSGerArtNewControle.Value;
    FmVSGerArtAltInn.EdSrcGGX.ValueVariant    := QrVSGerArtNewGraGruX.Value;
    //
    FmVSGerArtAltInn.ReopenItensAptos();
    FmVSGerArtAltInn.DefineTipoArea();
    //
    FmVSGerArtAltInn.EdMovimTwn.ValueVariant := QrVSGerArt015MovimTwn.Value;
    //
    begin
      FmVSGerArtAltInn.FDataHora    := QrVSGerArtDtHrAberto.Value;
      //
      if QrVSGerArt014Pecas.Value = 0 then
        FmVSGerArtAltInN.FFator := 1
      else
        FmVSGerArtAltInN.FFator :=
        -QrVSGerArt015Pecas.Value / QrVSGerArt014Pecas.Value;
      FmVSGerArtAltInN.FAntPecas    := QrVSGerArt014Pecas.Value;
      FmVSGerArtAltInN.FAntPesoKg   := QrVSGerArt014PesoKg.Value;
      FmVSGerArtAltInN.FAntValorT   := QrVSGerArt014ValorT.Value;
      FmVSGerArtAltInN.FInnGraGruX  := QrVSGerArt015GraGruX.Value;
      //
      FmVSGerArtAltInN.FControleNew := QrVSGerArtNewControle.Value;
      //
      FmVSGerArtAltInN.FControleInn := QrVSGerArt015SrcNivel2.Value;

      // Baixa
      FmVSGerArtAltInN.FMovimTwn                    := QrVSGerArt015MovimTwn.Value;
      FmVSGerArtAltInN.EdControleBxa.ValueVariant   := QrVSGerArt015Controle.Value;
      FmVSGerArtAltInN.EdPecasBxa.ValueVariant      := -QrVSGerArt015Pecas.Value;
      FmVSGerArtAltInN.EdPesoKgBxa.ValueVariant     := -QrVSGerArt015PesoKg.Value;
      FmVSGerArtAltInN.EdQtdGerArM2Bxa.ValueVariant := 0;
      FmVSGerArtAltInN.EdQtdGerArP2Bxa.ValueVariant := 0;
      FmVSGerArtAltInN.EdObservBxa.ValueVariant     := QrVSGerArt014Observ.Value;
      //
      // Geracao de item niv 14 que soma com outros itens niv 14 e totaliza no niv 13
      FmVSGerArtAltInN.EdControleSrc.ValueVariant   := QrVSGerArt014Controle.Value;
      FmVSGerArtAltInN.EdPecasSrc.ValueVariant      := QrVSGerArt014Pecas.Value;
      FmVSGerArtAltInN.EdPesoKgSrc.ValueVariant     := QrVSGerArt014PesoKg.Value;
      FmVSGerArtAltInN.EdQtdGerArM2Src.ValueVariant := QrVSGerArt014QtdGerArM2.Value;
      FmVSGerArtAltInN.EdQtdGerArP2Src.ValueVariant := QrVSGerArt014QtdGerArP2.Value;
      FmVSGerArtAltInN.EdObservSrc.ValueVariant     := QrVSGerArt014Observ.Value;
      //
    end;
    //
    FmVSGerArtAltInn.ShowModal;
    FmVSGerArtAltInn.Destroy;
    //
    //if (SQLType = stIns) and (QrVSGerArtDtHrLibCla.Value < 2) then
    //LiberaClassificacao(stIns);
  end;
end;

procedure TFmVSGerArtCab.InsereArtigoRibeira(Codigo, MovimCod, Empresa,
GraGruX, ClientMO: Integer; DataHora: String);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = 0;
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  Pallet     = 0;
  AreaM2     = 0;
  AreaP2     = 0;
  CustoMOM2  = 0;
  CustoMOTot = 0;
  ValorT     = 0;
  Fornecedor = 0;
  Pecas      = 0;
  PesoKg     = 0;
  Ficha      = 0;
  SerieFch   = 0;
  //Misturou   = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0; // Calcular depois a cada item
  Marca      = '';
  //
  ExigeFornecedor = False;
  //
  TpCalcAuto  = -1;
  //
  EdPallet    = nil;
  EdAreaM2    = nil;
  EdFicha     = nil;
  EdValorT    = nil;
  //
  PedItsFin   = 0;
  PedItsVda   = 0;
  PedItsLib   = 0;
  //
  GSPSrcMovID  = TEstqMovimID(0);
  GSPSrcNiv2  = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  Observ: String;
  MovimTwn, Controle, FornecMO, StqCenLoc, ReqMovEstq, MedEClas: Integer;
  CustoMOKg, CredPereImposto: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  //Codigo         :=
  Controle       := EdControle.ValueVariant;
  //MovimCod       :=
  //Empresa        :=
  //DataHora       :=
  MovimID        := emidIndsXX;
  MovimNiv       := eminDestCurtiXX;
  //GraGruX        :=
  Observ         := EdObserv.Text;
  CustoMOKg      := EdCustoMOKg.ValueVariant;
  CredPereImposto := EdCredPereImposto.ValueVariant;
  FornecMO       := EdFornecMO.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
(*  N�o adianta aqui! J� emitiu cabe�alho!
  if Dmod.VSFic(GraGruX, Empresa, Fornecedor, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas, EdAreaM2,
  EdPesoKg, EdValorT, ExigeFornecedor) then
    Exit;
*)
  //
  MedEClas       := Geral.BoolToInt(CkMedEClas.Checked);
  //
  // Nao tem Gemeo!!
  //MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
  MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  if VS_CRC_PF.InsUpdVSMovIts4(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, SrcMovID,
  SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
  AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  MedEClas,
  iuvpei018(*Cabe�alho de gera��o de artigo de ribeira*)) then
  begin
    //Dmod.AtualizaTotaisVSXxxCab('vsgerarta', MovimCod);
    if SQLType = stUpd then

      VS_CRC_PF.DistribuiCustoIndsVS(MovimNiv, MovimCod, Codigo, Controle);
    //MovimID = emidIndsXX; (*06*)
    VS_CRC_PF.AtualizaSaldoIMEI(Controle, False);
  end;
end;

procedure TFmVSGerArtCab.ItsAltera1Click(Sender: TObject);
begin
   ZZZEditaNovo();
end;

procedure TFmVSGerArtCab.CabExclui1Click(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  if QrVSGerArtNewPecas.Value > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
    'Artigo gerado possui quantidade de pe�as!');
    Exit;
  end;
  Codigo := QrVSGerArtCodigo.Value;
  //
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o desta gera��o de artigo?',
  'vsgerarta', 'Codigo', Codigo, DMod.MyDB) = ID_YES then
  begin
    // Exclui movimento de artigo novo!
    Controle := QrVSGerArtNewControle.Value;
    VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle,
      Integer(TEstqMotivDel.emtdWetCurti008A), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
    //
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSGerArt,
      QrVSGerArtCodigo, QrVSGerArtCodigo.Value);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSGerArtCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmVSGerArtCab.Quantidades1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVSGerArtAltQtd, FmVSGerArtAltQtd, afmoNegarComAviso) then
  begin
    FmVSGerArtAltQtd.ImgTipo.SQLType := stUpd;
    //
    FmVSGerArtAltQtd.FQrCab        := QrVSGerArt;
    FmVSGerArtAltQtd.FDsCab        := Dsvsgerart;
    FmVSGerArtAltQtd.FQrIts        := QrVSGerArt015;
    FmVSGerArtAltQtd.FEmpresa      := QrVSGerArtEmpresa.Value;
    FmVSGerArtAltQtd.FTipoArea     := QrVSGerArtTipoArea.Value;
    FmVSGerArtAltQtd.FOrigMovimNiv := TEstqMovimNiv(QrVSGerArtNewMovimNiv.Value);
    FmVSGerArtAltQtd.FOrigMovimCod := QrVSGerArtNewMovimCod.Value;
    FmVSGerArtAltQtd.FOrigCodigo   := QrVSGerArtNewCodigo.Value;
    FmVSGerArtAltQtd.FNewGraGruX   := QrVSGerArtNewGraGruX.Value;
    //
    FmVSGerArtAltQtd.EdCodigo.ValueVariant    := QrVSGerArtCodigo.Value;
    FmVSGerArtAltQtd.EdMovimCod.ValueVariant  := QrVSGerArtMovimCod.Value;
    FmVSGerArtAltQtd.TPData.Date              := QrVSGerArt015DataHora.Value;
    FmVSGerArtAltQtd.EdHora.ValueVariant      := QrVSGerArt015DataHora.Value - Int(QrVSGerArt015DataHora.Value);
    //
    FmVSGerArtAltQtd.EdSrcMovID.ValueVariant  := QrVSGerArtNewMovimID.Value;
    FmVSGerArtAltQtd.EdSrcNivel1.ValueVariant := QrVSGerArtNewCodigo.Value;
    FmVSGerArtAltQtd.EdSrcNivel2.ValueVariant := QrVSGerArtNewControle.Value;
    FmVSGerArtAltQtd.EdSrcGGX.ValueVariant    := QrVSGerArtNewGraGruX.Value;
    FmVSGerArtAltQtd.EdSrcValorT.ValueVariant := QrVSGerArtNewValorT.Value;
    //
    //FmVSGerArtAltQtd.ReopenItensAptos();
    FmVSGerArtAltQtd.DefineTipoArea();
    //
    // stIns:
    //FmVSGerArtAltQtd.FDataHora                := QrVSGerArtDtHrAberto.Value;
    // stUpd:
    begin
      //
      if QrVSGerArt014Pecas.Value = 0 then
        FmVSGerArtAltQtd.FFator := 1
      else
        FmVSGerArtAltQtd.FFator :=
        -QrVSGerArt015Pecas.Value / QrVSGerArt014Pecas.Value;
      FmVSGerArtAltQtd.FAntPecas    := QrVSGerArt014Pecas.Value;
      FmVSGerArtAltQtd.FAntPesoKg   := QrVSGerArt014PesoKg.Value;
      FmVSGerArtAltQtd.FAntValorT   := QrVSGerArt014ValorT.Value;
      FmVSGerArtAltQtd.FAntValorMP  := QrVSGerArt014ValorMP.Value;
      FmVSGerArtAltQtd.FCustoMOKg   := QrVSGerArtNewCustoMOKg.Value;

      FmVSGerArtAltQtd.FInnGraGruX  := QrVSGerArt015GraGruX.Value;
      //
      FmVSGerArtAltQtd.FControleInn := QrVSGerArt015SrcNivel2.Value;

      // Baixa
      FmVSGerArtAltQtd.EdControleBxa.ValueVariant   := QrVSGerArt015Controle.Value;
      FmVSGerArtAltQtd.EdPecasBxa.ValueVariant      := -QrVSGerArt015Pecas.Value;
      FmVSGerArtAltQtd.EdPesoKgBxa.ValueVariant     := -QrVSGerArt015PesoKg.Value;
      FmVSGerArtAltQtd.EdQtdGerArM2Bxa.ValueVariant := 0;
      FmVSGerArtAltQtd.EdQtdGerArP2Bxa.ValueVariant := 0;
      FmVSGerArtAltQtd.EdObservBxa.ValueVariant     := QrVSGerArt014Observ.Value;
      //
      // Geracao de item niv 14 que soma com outros itens niv 14 e totaliza no niv 13
      FmVSGerArtAltQtd.EdControleSrc.ValueVariant   := QrVSGerArt014Controle.Value;
      FmVSGerArtAltQtd.EdPecasSrc.ValueVariant      := QrVSGerArt014Pecas.Value;
      FmVSGerArtAltQtd.EdPesoKgSrc.ValueVariant     := QrVSGerArt014PesoKg.Value;
      FmVSGerArtAltQtd.EdQtdGerArM2Src.ValueVariant := QrVSGerArt014QtdGerArM2.Value;
      FmVSGerArtAltQtd.EdQtdGerArP2Src.ValueVariant := QrVSGerArt014QtdGerArP2.Value;
      FmVSGerArtAltQtd.EdQtdGerPeso.ValueVariant    := QrVSGerArt014QtdGerPeso.Value;
      FmVSGerArtAltQtd.EdObservSrc.ValueVariant     := QrVSGerArt014Observ.Value;
      //
    end;
    //
    FmVSGerArtAltQtd.LiberaEdicao();
    //
    FmVSGerArtAltQtd.ShowModal;
    FmVSGerArtAltQtd.Destroy;
  end;
end;

procedure TFmVSGerArtCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSGerArtCab.Removeforado1Click(Sender: TObject);
begin
//  if not DBCheck.LiberaPelaSenhaAdmin then
  //  Exit;
  RemoveItem();
end;

procedure TFmVSGerArtCab.RemoveItem();
var
  MovimNiv, OrigMovimNiv: TEstqMovimNiv;
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimCod, MovimTwn, SrcNivel2,
  SrcMovID: Integer;
  Continua: Boolean;
  OrigMovimCod, OrigCodigo, OrigSrcNivel2: Integer;
begin
  if QrVSGerArtNewSdoVrtPeca.Value < QrVSGerArtNewPecas.Value then
  begin
(*
    Geral.MB_Aviso('Exclus�o de item cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo de Ribeira que foi gerado!');
    Exit;
*)
    Geral.MB_Aviso('Ser� necess�rio senha!' + sLineBreak +
    'J� existe baixa do Artigo de Ribeira que foi gerado!');
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  end;
  //end else
  begin
    MovimTwn := QrVSGerArt015MovimTwn.Value;
    if MovimTwn = 0 then
    begin
      Geral.MB_Aviso('Exclus�o de item cancelada!' +
      sLineBreak + '"MovimTwn" n�o definido!');
      Exit;
    end else
    begin
      Codigo   := QrVSGerArtCodigo.Value;
      CtrlOri  := QrVSGerArt015SrcNivel2.Value;
      MovimCod := QrVSGerArtNewMovimCod.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrTwn, Dmod.MyDB, [
      'SELECT Controle, MovimNiv, SrcNivel2  ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),
      'AND MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimTwn=' + Geral.FF0(MovimTwn),
      'AND MovimNiv IN ('
      // Evitar excluir artigo gerado do cabecalho!!
      //+ Geral.FF0(Integer(eminDestCurtiXX))//=13,
      //+ ', '
      + Geral.FF0(Integer(eminSorcCurtiXX))//=14,
      + ', '
      + Geral.FF0(Integer(eminBaixCurtiXX))//=15,
      + ')',
      '']);
      //
      if QrTwn.RecordCount > 0 then
      begin
        if QrTwn.RecordCount <> 2 then
          Continua := Geral.MB_Pergunta(
          'A quantidade de g�meos do item esperada era 2 mas foi encontrado ' +
          Geral.FF0(QrTwn.RecordCount) + sLineBreak +
          'Confirma assim mesmo a exclus�o do(s) item(ns) assim mesmo?') =
          ID_YES
        else
          Continua := True;
        if Continua then
        begin
          while not QrTwn.Eof do
          begin
            MovimNiv  := TEstqMovimNiv(QrTwnMovimNiv.Value);
            CtrlDel   := QrTwnControle.Value;
            SrcNivel2 := QrTwnSrcNivel2.Value;
            SrcMovID  := QrVSGerArt015SrcMovID.Value;

            if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti008B), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
            begin
              if MovimNiv = TEstqMovimNiv.eminBaixCurtiXX then
              begin
                // Reduzir Saldo do In Natura selecionado
                VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, True);
                if SrcMovID = Integer(TEstqMovimID.emidEmProcCur) then
                VS_CRC_PF.AtualizaSaldoItmCur(SrcNivel2);
              end;
              //
              if MovimNiv = TEstqMovimNiv.eminSorcCurtiXX then
              begin
                // Aumentar Saldo e custo do Artigo de Ribeira gerado
                VS_CRC_PF.DistribuiCustoIndsVS(MovimNiv, MovimCod, Codigo,
                QrVSGerArtNewControle.Value);
              end;
              // Nao se aplica!
              //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
              //
            end;
            QrTwn.Next;
          end;
          MovimCod := QrVSGerArtMovimCod.Value;
          CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSGerArt015,
            TIntegerField(QrVSGerArt015Controle), QrVSGerArt015Controle.Value);
          OrigMovimNiv  := TEstqMovimNiv(QrVSGerArtNewMovimNiv.Value);
          OrigMovimCod  := QrVSGerArtNewMovimCod.Value;
          OrigCodigo    := QrVSGerArtNewCodigo.Value;
          OrigSrcNivel2 := QrVSGerArtNewControle.Value;
          VS_CRC_PF.DistribuiCustoIndsVS(OrigMovimNiv, OrigMovimCod, OrigCodigo, OrigSrcNivel2);
          VS_CRC_PF.AtualizaSaldoItmCur(OrigSrcNivel2);
          VS_CRC_PF.AtualizaTotaisVSCurCab(MovimCod);
          LocCod(Codigo, Codigo);
          //VS_CRC_PF.ReopenVSGerArtSrc(QrVSGerArt015, QrVSGerArtMovimCod.Value,
          //CtrlNxt, QrVSGerArtTemIMEIMrt.Value, '', eminBaixCurtiXX);
          ReopenVSGerArt15(CtrlNxt);
        end;
      end;
    end;
  end;
end;

procedure TFmVSGerArtCab.ReopenTribIncIts(Controle: Integer);
begin
  {$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrTribIncIts, Dmod.MyDB, [
  'SELECT tii.*, tdd.Nome NO_Tributo  ',
  'FROM tribincits tii ',
  'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tii.Tributo',
  'WHERE tii.FatID=' + Geral.FF0(VAR_FATID_1020),
  'AND tii.FatNum=' + Geral.FF0(QrVSGerArtNewCodigo.Value),
  'AND tii.FatParcela=' + Geral.FF0(QrVSGerArtNewControle.Value),
  'ORDER BY tii.Controle DESC ',
  '']);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSGerArtCab.ReopenVSGerArt14();
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //VS_CRC_PF.ReopenVSOpeOriIMEI(QrVSOpeOriIMEI, QrVSGerArtMovimCod.Value, Controle);
  TemIMEIMrt := QrVSGerArtTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'vmi.ValorT / vmi.QtdGerArM2 CustoAreaCur, CustoPQ ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSGerArt015MovimCod.Value),
  'AND vmi.MovimTwn=' + Geral.FF0(QrVSGerArt015MovimTwn.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcCurtiXX)), //14
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArt014, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
  //Geral.MB_Teste(QrVSGerArt014.SQL.Text);
end;

procedure TFmVSGerArtCab.ReopenVSGerArt15(Controle: Integer);
begin
  VS_CRC_PF.ReopenVSGerArtSrc(QrVSGerArt015, QrVSGerArtMovimCod.Value,
  Controle, QrVSGerArtTemIMEIMrt.Value, '', eminBaixCurtiXX);
end;

procedure TFmVSGerArtCab.ReopenVSOutNFeCab(Codigo: Integer);
begin
  {$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOutNFeCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsoutnfecab ',
  'WHERE MovimCod=' + Geral.FF0(QrVSGerArtMovimCod.Value),
  '']);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSGerArtCab.ReopenVSOutNFeIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOutNFeIts, Dmod.MyDB, [
  'SELECT CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, voi.* ',
  'FROM vsoutnfeits voi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=voi.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
  'WHERE voi.Codigo=' + Geral.FF0(QrVSOutNFeCabCodigo.Value),
  '']);
end;

procedure TFmVSGerArtCab.ItsExclui1Click(Sender: TObject);
begin
  //RemoveItem();
end;

procedure TFmVSGerArtCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSGerArtCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSGerArtCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSGerArtCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSGerArtCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSGerArtCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerArtCab.BtTribIncItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTribIncIts, BtTribIncIts);
end;

procedure TFmVSGerArtCab.BtVSOutNFeCabClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMVSOutNFeCab, BtVSOutNFeCab);
end;

procedure TFmVSGerArtCab.BtVSOutNFeItsClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMVSOutNFeIts, BtVSOutNFeIts);
end;

procedure TFmVSGerArtCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSGerArtCodigo.Value;
  Close;
end;

procedure TFmVSGerArtCab.ItsInclui1Click(Sender: TObject);
begin
  if (QrVSGerArtDtHrLibCla.Value > 2) or (QrVSGerArtDtHrFimCla.Value > 2) then
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
  MostraVSGArtItsBar(stIns);
end;

procedure TFmVSGerArtCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  HabilitaEmpresa(stUpd);
  GBEdita.Visible := True;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSGerArt, [PnDados],
  [PnEdita], TPData, ImgTipo, 'vsgerarta');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSGerArtEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
  //
  EdGGXAnterior.ValueVariant := QrVSGerArtNewGraGruX.Value;
  EdControle.ValueVariant    := QrVSGerArtNewControle.Value;
  EdGraGruX.ValueVariant     := QrVSGerArtNewGraGruX.Value;
  CBGraGruX.KeyValue         := QrVSGerArtNewGraGruX.Value;
  EdCustoMOKg.ValueVariant   := QrVSGerArtNewCustoMOKg.Value;
  EdPecas.ValueVariant       := QrVSGerArtNewPecas.Value;
  EdPesoKg.ValueVariant      := QrVSGerArtNewPesoKg.Value;
  EdMovimTwn.ValueVariant    := QrVSGerArtNewMovimTwn.Value;
  EdObserv.ValueVariant      := QrVSGerArtNewObserv.Value;
  EdClientMO.ValueVariant    := QrVSGerArtNewClientMO.Value;
  CBClientMO.KeyValue        := QrVSGerArtNewClientMO.Value;
  EdFornecMO.ValueVariant    := QrVSGerArtNewFornecMO.Value;
  CBFornecMO.KeyValue        := QrVSGerArtNewFornecMO.Value;
  EdStqCenLoc.ValueVariant   := QrVSGerArtNewStqCenLoc.Value;
  CBStqCenLoc.KeyValue       := QrVSGerArtNewStqCenLoc.Value;
  //
  EdQtdAntPeso.ValueVariant  := QrVSGerArtNewQtdAntPeso.Value;
  EdValorMP.ValueVariant     := QrVSGerArtNewValorMP.Value;
  EdCustoMOTot.ValueVariant  := QrVSGerArtNewCustoMOTot.Value;
  EdValorT.ValueVariant      := QrVSGerArtNewValorT.Value;
  //
  EdCredPereImposto.ValueVariant   := QrVSGerArtNewCredPereImposto.Value;
  //EdCredValrImposto.ValueVariant   := QrVSGerArtNewCredValrImposto.Value;
  //
  CkMedEClas.Checked         := Geral.IntToBool(QrVSGerArtNewMedEClas.Value);
end;

procedure TFmVSGerArtCab.BtClassesGeradasClick(Sender: TObject);
begin
  VS_CRC_PF.ImprimeClassIMEI(QrVSGerArtNewControle.Value, QrVSGerArtCodigo.Value, 0);
end;

procedure TFmVSGerArtCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, GraGruX, Empresa, MovimCod, TipoArea, FornecMO, GGXAnterior, StqCenLoc: Integer;
  Nome, DtHrAberto, SQL: String;
  SQLType: TSQLType;
  //
  Controle, DstGGX, GGX15, SrcNivel2, ClientMO: Integer;
  CustoMOTot, CustoMOKg, FatorInt1, FatorInt2, Fator1para2, Pecas, ValorT,
  PesoKg, ValorMP, QtdAntPeso: Double;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  // CUIDADO!!! Mesmo GraGruX vai nas duas tabelas !!!
  GraGruX        := EdGragruX.ValueVariant;
  GGXAnterior    := EdGGXAnterior.ValueVariant;
  DtHrAberto     := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  Nome           := EdNome.Text;
  TipoArea       := RGTipoArea.ItemIndex;
  FornecMO       := EdFornecMO.ValueVariant;
  ClientMO       := EdCLientMO.ValueVariant;
  CustoMOKg      := EdCustoMOKg.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGragruX, 'Informe o Artigo de Ribeira!') then
    Exit;
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Informe o dono do couro!') then
    Exit;
  if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Informe o fornecedor da m�o de obra!') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque') then
    Exit;
  if EdFornecMO.ValueVariant <> EdClientMO.ValueVariant then
    if MyObjects.FIC(CustoMOKg <= 0, EdCustoMOKg,
      '$/kg da m�o de obra n�o informado!') then
       if Geral.MB_Pergunta('Deseja continuar sem informar a m�o de obra?') <>
        ID_YES then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vsgerarta', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsgerarta', False, [
  'MovimCod', 'Empresa', 'DtHrAberto',
  'Nome', 'TipoArea', 'GraGruX'], [
  'Codigo'], [
  MovimCod, Empresa, DtHrAberto,
  Nome, TipoArea, GraGruX], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidIndsXX, Codigo);
    InsereArtigoRibeira(Codigo, MovimCod, Empresa, GraGruX, ClientMO, DtHrAberto);
    if SQLType = stUpd then
    begin
      //if GGXAnterior <> GraGruX then
      begin
        // Abrir os IMEIs de artigos gerados
        UnDmkDAC_PF.AbreMySQLQuery0(QrNiv14, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE MovimCod=' + Geral.FF0(MovimCod),
        'AND MovimNiv=' + Geral.FF0(Integer(eminSorcCurtiXX)), // 14
        '']);
        DstGGX   := GraGruX;
        Controle := QrNiv14Controle.Value;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'GraGruX', 'DstGGX'], ['Controle'], [
        GraGruX, DstGGX], [Controle], True) then
        begin
          CustoMOTot := QrNiv14PesoKg.Value * CustoMOKg;
          Controle   := QrNiv14Controle.Value;
          ValorT     := QrNiv14ValorMP.Value + CustoMOTot;

          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
          'CustoMOKg', 'CustoMOTot', 'ValorT'
          ], ['Controle'], [
          CustoMOKg, CustoMOTot, ValorT
          ], [Controle], True) then
          begin
            while not QrNiv14.Eof do
            begin
              UnDmkDAC_PF.AbreMySQLQuery0(QrNiv15, Dmod.MyDB, [
              'SELECT * ',
              'FROM ' + CO_SEL_TAB_VMI,
              'WHERE MovimCod=' + Geral.FF0(MovimCod),
              'AND MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiXX)),  // 15
              'AND MovimTwn=' + Geral.FF0(QrNiv14MovimTwn.Value),
              '']);
              Controle  := QrNiv15Controle.Value;
              SrcNivel2 := QrNiv15SrcNivel2.Value;
              GGX15     := QrNiv15GraGruX.Value;
              //
              VS_CRC_PF.ObtemFatorIntDeGraGruXeVeSeDifere(GGX15, GraGruX,
              FatorInt1, FatorInt2, Fator1para2);// then
              Pecas      := -QrNiv14Pecas.Value * Fator1para2;
              //
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
              'Pecas'], ['Controle'], [
              Pecas], [Controle], True) then
(*
*)
              begin
                Controle    := EdControle.ValueVariant;
                //
                Pecas       := EdPecas.ValueVariant;
                QtdAntPeso  := EdQtdAntPeso.ValueVariant;
                ValorMP     := EdValorMP.ValueVariant;
                CustoMOKg   := EdCustoMOKg.ValueVariant;
                CustoMOTot  := EdCustoMOTot.ValueVariant;
                ValorT      := EdValorT.ValueVariant;
                //
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
                'Pecas', 'QtdAntPeso', 'ValorMP',
                'CustoMOKg', 'CustoMOTot', 'ValorT'], ['Controle'], [
                Pecas, QtdAntPeso, ValorMP,
                CustoMOKg, CustoMOTot, ValorT], [Controle], True) then
                begin
                  VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, False);
                  VS_CRC_PF.AtualizaVSValorT(Controle);
                end;
              end;
              QrNiv14.Next;
            end;
          end;
        end;
      end;
      // Com o codigo acima nao precisa mais desta procedure aqui!
      //VS_CRC_PF.AtualizaArtigosDePallets(PB1, LaAviso1, LaAviso2);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrVSGerArt015.RecordCount = 0) then
      SelecionaFormGerArtIts();
    if SQLType = stUpd then
    begin
      SQL := 'UPDATE vsmovits SET DstGGX=' + Geral.FF0(QrVSGerArtNewGragruX.Value) +
      ' WHERE MovimCod=' + Geral.FF0(QrVSGerArtNewMovimCod.Value) +
      ' AND MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiXX)); // 15
       //
       UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL);
    end;
  end;
end;

procedure TFmVSGerArtCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsgerarta', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsgerarta', 'Codigo');
end;

procedure TFmVSGerArtCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSGerArtCab.AdicionadoCurimento1Click(Sender: TObject);
begin
{ Habilitar!!! Desabilitado para testes!!!!
  if (QrVSGerArtDtHrLibCla.Value > 2) or (QrVSGerArtDtHrFimCla.Value > 2) then
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
}
  MostraVSGArtItsCurUni(stIns);
end;

procedure TFmVSGerArtCab.AdicionadoCurimentoIMECnico1Click(Sender: TObject);
begin
  MostraVSGArtItsCurIMEC(stIns);
end;

procedure TFmVSGerArtCab.AdicionadoCurimentoMltiplo1Click(Sender: TObject);
begin
  MostraVSGArtItsCurMul(stIns);
end;

procedure TFmVSGerArtCab.AdicionadoCurimentoMltiplodeMltiplo1Click(
  Sender: TObject);
begin
  MostraVSGArtItsCurMuldeMul(stIns);
end;

procedure TFmVSGerArtCab.Alteraatrelamento1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{$IfDef sAllVS}
  PCItens.ActivePageIndex := 2;
  //
  VS_PF.MostraFormVSMOEnvRet(stUpd, QrVSGerArtNew, QrVSGerArt014, QrVSGerArt015,
  QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI, 0, 0, 0, siPositivo, FGerArX2);
  //
  Codigo := QrVSGerArtCodigo.Value;
  LocCod(Codigo, Codigo);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSGerArtCab.AlteraDatas1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSGerArtCodigo.Value;
  //
  VS_Jan.MostraFormVSGerArtDatas(Codigo, QrVSGerArtMovimCod.Value,
  QrVSGerArtDtHrAberto.Value, QrVSGerArtDtHrLibCla.Value,
  QrVSGerArtDtHrCfgCla.Value, QrVSGerArtDtHrFimCla.Value, emidIndsXX);
  //
  LocCod(Codigo, Codigo);
  {
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  Codigo := QrVSGerArtCodigo.Value;
  if DBCheck.CriaFm(TFmVSGerArtDatas, FmVSGerArtDatas, afmoNegarComAviso) then
  begin
    FmVSGerArtDatas.FCodigo := Codigo;
    FmVSGerArtDatas.TPDtHrAberto.Date         := QrVSGerArtDtHrAberto.Value;
    FmVSGerArtDatas.EdDtHrAberto.ValueVariant := QrVSGerArtDtHrAberto.Value;
    FmVSGerArtDatas.TPDtHrLibCla.Date         := QrVSGerArtDtHrLibCla.Value;
    FmVSGerArtDatas.EdDtHrLibCla.ValueVariant := QrVSGerArtDtHrLibCla.Value;
    FmVSGerArtDatas.TPDtHrCfgCla.Date         := QrVSGerArtDtHrCfgCla.Value;
    FmVSGerArtDatas.EdDtHrCfgCla.ValueVariant := QrVSGerArtDtHrCfgCla.Value;
    FmVSGerArtDatas.TPDtHrFimCla.Date         := QrVSGerArtDtHrFimCla.Value;
    FmVSGerArtDatas.EdDtHrFimCla.ValueVariant := QrVSGerArtDtHrFimCla.Value;
    //
    FmVSGerArtDatas.FMovimCod                 := QrVSGerArtMovimCod.Value;
    FmVSGerArtDatas.FMovimID                  := Integer(emidIndsXX);
    //
    FmVSGerArtDatas.ShowModal;
    FmVSGerArtDatas.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
}
end;

procedure TFmVSGerArtCab.AlteraIMEI_Old();
const
  AtualizaSaldoModoGenerico = False;
var
  OrigMovimNiv: TEstqMovimNiv;
  OrigMovimCod, OrigCodigo, OrigControle,
  IMEIOld, IMEINew, IMEISrc, IMEIDst: Integer;
begin
  IMEIOld := QrVSGerArt015Controle.Value;
  IMEINew := QrVSGerArtNewControle.Value;
  IMEISrc := QrVSGerArt015SrcNivel2.Value;
  IMEIDst := QrVSGerArt015DstNivel2.Value;
  //
  OrigMovimNiv := TEstqMovimNiv(QrVSGerArtNewMovimNiv.Value);
  OrigMovimCod := QrVSGerArtNewMovimCod.Value;
  OrigCodigo   := QrVSGerArtNewCodigo.Value;
  OrigControle := QrVSGerArtNewControle.Value;
  //
  VS_CRC_PF.MostraFormVSMovItsAlt(IMEIOld, AtualizaSaldoModoGenerico, [eegbQtdOriginal]);
  //VS_CRC_PF.AtualizaSaldoIMEI(IMEINew, True);
  //VS_CRC_PF.AtualizaSaldoIMEI(IMEIOld, True);
  VS_CRC_PF.AtualizaSaldoIMEI(IMEISrc, True);
  VS_CRC_PF.AtualizaSaldoIMEI(IMEIDst, True);
  // Mudar Saldo e custo do Artigo de Ribeira gerado
  VS_CRC_PF.DistribuiCustoIndsVS(OrigMovimNiv, OrigMovimCod, OrigCodigo,
  OrigControle);
  //
  FmVSGerArtCab.LocCod(OrigCodigo, OrigCodigo);
  //VS_CRC_PF.ReopenVSGerArtSrc(QrVSGerArt015, QrVSGerArtMovimCod.Value, 0,
  //QrVSGerArtTemIMEIMrt.Value, '', eminBaixCurtiXX);
  ReopenVSGerArt15(0);
  //
end;

procedure TFmVSGerArtCab.AlteraitemdeNF1Click(Sender: TObject);
begin
    VS_CRC_PF.MostraFormVSNFeOutIts(stUpd, QrVSOutNfeIts, QrTribIncIts,
    QrVSOutNfeCabCodigo.Value, QrVSOutNfeCabMovimCod.Value,
      QrVSGerArtEmpresa.Value, QrVSGerArtDtHrLibCla.Value,
      siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSGerArtMovimCod.Value, TEstqMovimID.emidIndsXX,
  [eminBaixCurtiXX], [eminDestCurtiXX, eminSorcCurtiXX]);
end;

procedure TFmVSGerArtCab.Alterapesokg1Click(Sender: TObject);
(*var
  ResVar: Variant;
  Qtde, Controle: Integer;*)
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  (*Qtde := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Quantidade', 'Informe a quantidade em kg: ',
  0, ResVar) then
  begin
    Qtde := Geral.IMV(ResVar);
    if Qtde >= 0 then
    begin
      Controle := QrVSGerArtNewControle.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'PesoKg'], [
      'Controle'], [
      Qtde], [
      Controle], True) then
      begin
        VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle,
        QrVSGerArtNewMovimID.Value, QrVSGerArtNewMovimNiv.Value);
        //
        LocCod(QrVSGerArtCodigo.Value, QrVSGerArtCodigo.Value);
      end;
    end else
      Geral.MB_Aviso('Valor inv�lido! N�o pode ser negativo!');
  end;
  //*)
  if VS_CRC_PF.AlteraVMI_PesoKg('PesoKg', QrVSGerArtNewMovimID.Value,
  QrVSGerArtNewMovimNiv.Value, QrVSGerArtNewControle.Value,
  QrVSGerArtNewPesoKg.Value, siPositivo) then
    LocCod(QrVSGerArtCodigo.Value, QrVSGerArtCodigo.Value);
end;

procedure TFmVSGerArtCab.AlteraSerieNumeroDeNFe1Click(Sender: TObject);
var
  Codigo, ide_serie, ide_nNF: Integer;
begin
  Codigo    := QrVSOutNFeCabCodigo.Value;
  ide_serie := QrVSOutNFeCabide_serie.Value;
  ide_nNF   := QrVSOutNFeCabide_nNF.Value;
  //
  VS_CRC_PF.MostraFormVSNFeOutCab(stUpd, QrVSOutNfeCab, QrVSGerArtMovimCod.Value,
  QrVSGerArtCodigo.Value, Codigo, ide_serie, ide_nNF);
end;

procedure TFmVSGerArtCab.AlteraTributoAtual1Click(Sender: TObject);
var
  Operacao, Tributo, FatID, FatNum, FatParcela, Empresa, Controle: Integer;
  Data, Hora: TDateTime;
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib: Double;
begin
  {$IfDef sAllVS}
  FatNum     := QrTribIncItsFatNum.Value;
  FatParcela := QrTribIncItsFatParcela.Value;
  Empresa    := QrTribIncItsEmpresa.Value;
  Data       := QrTribIncItsData.Value;
  Hora       := QrTribIncItsHora.Value;
  //
  Controle   := QrTribIncItsControle.Value;
  Operacao   := QrTribIncItsOperacao.Value;
  Tributo    := QrTribIncItsTributo.Value;
  ValorFat   := QrTribIncItsValorFat.Value;
  BaseCalc   := QrTribIncItsBaseCalc.Value;
  Percent    := QrTribIncItsPercent.Value;
  VUsoCalc   := QrTribIncItsVUsoCalc.Value;
  ValrTrib   := QrTribIncItsValrTrib.Value;
  //
  Tributos_PF.MostraFormTribIncIts(stUpd, QrTribIncIts, VAR_FATID_1020,
  FatNum, FatParcela, Empresa, Controle, Data, Hora, Operacao, Tributo,
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib, siPositivo);

  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSGerArtCab.Atualizacustodescendentesdoitemselecionado1Click(
  Sender: TObject);
begin
  VS_CRC_PF.AtualizaCustosDescendentesGerArtFicha(QrVSGerArt015Empresa.Value,
  QrVSGerArt015SerieFch.Value, QrVSGerArt015Ficha.Value);
  ReopenVSGerArt15(QrVSGerArt015Controle.Value);
end;

procedure TFmVSGerArtCab.AtualizaNFeItens();
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSGerArtMovimCod.Value, TEstqMovimID.emidIndsXX,
  [eminBaixCurtiXX], [eminDestCurtiXX, eminSorcCurtiXX]);
end;

procedure TFmVSGerArtCab.BtCabClick(Sender: TObject);
begin
  VS_CRC_PF.HabilitaMenuInsOuAllVSAberto(QrVSGerArt, QrVSGerArtDtHrAberto.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSGerArtCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  //GBEdita.Align := alClient;
  PnDados.Align := alClient;
  PCItens.Align := alClient;
  PCItens.ActivePageIndex := 0;
  PCSubItens.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
  //
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_2048_VSRibCad));
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0,
    TEstqMovimID.emidIndsXX);
  EdCredPereImposto.ValueVariant :=
    Dmod.QrControleCreTrib_MP_ICMS_ALIQ.Value +
    Dmod.QrControleCreTrib_MP_PIS_ALIQ.Value +
    Dmod.QrControleCreTrib_MP_COFINS_ALIQ.Value;
end;

procedure TFmVSGerArtCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSGerArtCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSGerArtCab.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNovo, SbNovo);
end;

procedure TFmVSGerArtCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmVSGerArtCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSGerArtCab.QrVSGerArtAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSGerArtCab.QrVSGerArtAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSGerArtDst(QrVSGerArtNew, QrVSGerArtMovimCod.Value, 0,
    QrVSGerArtTemIMEIMrt.Value, eminDestCurtiXX);
  //VS_CRC_PF.ReopenVSGerArtSrc(QrVSGerArt015, QrVSGerArtMovimCod.Value, 0,
  //QrVSGerArtTemIMEIMrt.Value, '', eminBaixCurtiXX);
  ReopenVSGerArt15(0);
  ReopenVSOutNFeCab(0);
end;

procedure TFmVSGerArtCab.FluxodaFichaRMPOrigemitemselecionado1Click(
  Sender: TObject);
begin
  {$IfDef sAllVS}
  VS_PF.MostraRelatoroFluxoFichaRMP(QrVSGerArt015SerieFch.Value,
  QrVSGerArt015Ficha.Value);
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSGerArtCab.FluxodoIMEIGerado1Click(Sender: TObject);
begin
  {$IfDef sAllVS}
  VS_PF.MostraRelatoroFluxoIMEI(QrVSGerArtNewControle.Value);
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSGerArtCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSGerArtCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSGerArtCab.SbQueryClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMQuery, SbQuery);
end;

procedure TFmVSGerArtCab.SbStqCenLocClick(Sender: TObject);
begin
  VS_CRC_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, TEstqMovimID.emidIndsXX);
end;

procedure TFmVSGerArtCab.SelecionaFormGerArtIts();
var
  Ordem: Integer;
  Ord_Txt: String;
begin
  Ordem := MyObjects.SelRadioGroup('Janela de Itens',
  'Selecione a origem do couro', [
  'Barraca - Ficha',
  'Curtimento - Item',
  'Curtimento - Ficha',
  'Curtimento - IMEC (Ful�o)'], 1, -1, True);
  case Ordem of
    0: MostraVSGArtItsBar(stIns);
    1: MostraVSGArtItsCurUni(stIns);
    2: MostraVSGArtItsCurMul(stIns);
    3: MostraVSGArtItsCurIMEC(stIns);
  end;
end;

procedure TFmVSGerArtCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerArtCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSGerArtCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  EdGGXAnterior.ValueVariant := 0;
  EdControle.ValueVariant    := 0;
  if QrGraGruX.RecordCount > 1 then
  begin
    EdGraGruX.ValueVariant     := 0;
    CBGraGruX.KeyValue         := 0;
  end;
  EdCustoMOKg.ValueVariant   := 0;
  EdPecas.ValueVariant       := 0;
  EdPesoKg.ValueVariant      := 0;
  EdMovimTwn.ValueVariant    := 0;
  if QrPrestador.RecordCount > 1 then
  begin
    EdFornecMO.ValueVariant    := 0;
    CBFornecMO.KeyValue        := 0;
  end;
  EdObserv.ValueVariant      := '';
  //
  HabilitaEmpresa(stIns);
  GBEdita.Visible := False;
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSGerArt, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vsgerarta');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPData.SetFocus;
end;

procedure TFmVSGerArtCab.QrVSGerArtBeforeClose(
  DataSet: TDataSet);
begin
  QrVSGerArt015.Close;
  QrVSGerArtNew.Close;
  QrVSOutNFeCab.Close;
end;

procedure TFmVSGerArtCab.QrVSGerArtBeforeOpen(DataSet: TDataSet);
begin
  QrVSGerArtCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSGerArtCab.QrVSGerArtCalcFields(DataSet: TDataSet);
begin
  case QrVSGerArtTipoArea.Value of
    0: QrVSGerArtNO_TIPO.Value := 'm�';
    1: QrVSGerArtNO_TIPO.Value := 'ft�';
    else QrVSGerArtNO_TIPO.Value := '???';
  end;
  QrVSGerArtNO_DtHrLibCla.Value := Geral.FDT(QrVSGerArtDtHrLibCla.Value, 106, True);
  QrVSGerArtNO_DtHrFimCla.Value := Geral.FDT(QrVSGerArtDtHrFimCla.Value, 106, True);
  QrVSGerArtNO_DtHrCfgCla.Value := Geral.FDT(QrVSGerArtDtHrCfgCla.Value, 106, True);
end;

procedure TFmVSGerArtCab.QrVSGerArtNewAfterScroll(DataSet: TDataSet);
begin
  ReopenTribIncIts(0);
end;

procedure TFmVSGerArtCab.QrVSGerArtNewBeforeClose(DataSet: TDataSet);
begin
  QrTribIncIts.Close;
end;

procedure TFmVSGerArtCab.QrVSMOEnvRetAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvRVmi(QrVSMOEnvRVmi, QrVSMOEnvRetCodigo.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvGVmi(QrVSMOEnvGVmi, QrVSMOEnvRetCodigo.Value, 0);
end;

procedure TFmVSGerArtCab.QrVSMOEnvRetBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvGVmi.Close;
  QrVSMOEnvRVmi.Close;
end;

procedure TFmVSGerArtCab.QrVSOutNFeCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSOutNFeIts(0);
end;

procedure TFmVSGerArtCab.QrVSOutNFeCabBeforeClose(DataSet: TDataSet);
begin
  QrVSOutNFeIts.Close;
end;

procedure TFmVSGerArtCab.QrVSGerArt015AfterScroll(DataSet: TDataSet);
begin
  ReopenVSGerArt14();
{$IfDef sAllVS}
  VS_CRC_PF.ReopenVSMOEnvRet(QrVSMOEnvRet, QrVSGerArtMovimCod.Value, 0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSGerArtCab.QrVSGerArt015BeforeClose(DataSet: TDataSet);
begin
  QrVSGerArt014.Close;
  QrVSMOEnvRet.Close;
end;

procedure TFmVSGerArtCab.QrVSGerArt015CalcFields(DataSet: TDataSet);
begin
  if QrVSGerArt015QtdGerArM2.Value = 0.00 then
    QrVSGerArt015CustoKgInn.Value := 0.00
  else
    QrVSGerArt015CustoKgInn.Value := QrVSGerArt015ValorT.Value / QrVSGerArt015PesoKg.Value;
end;

procedure TFmVSGerArtCab.ZZZEditaNovo();
begin
{

// Parei Aqui!

// Encontrar o Gemeo de baixa de In Natura e o controle (SrcNivel2) do In Natura
// Result.Controle = gemeo e Result.SrcNivel2 = In Natura de origem

SELECT * FROM v s m o v i t s
WHERE MovimTwn=4482
AND MovimNiv=15
    // Se alterar Pecas tem que alterar recalcular o saldo do In Natura de origem
    // alterando tambem o IMEI Gemeo MovimID=15
    ... codigo...

    // Se alterar IMEI de origem tem que recalcular o In Natura de Origem
    // anterior, o IMEI novo, o IMEI do Artigo Gerado e TODOS IMEIs descendentes
    // do IMEI de Artigo Gerado!!!!!


  Para os descendentes diretos!

SELECT Codigo, Controle, MovimCod, MovimID,
MovimNiv, MovimTwn, Terceiro, SerieFch, Ficha,
Pallet, GraGruX, Pecas,
SrcMovID, SrcNivel2, DstMovID, DstNivel2
FROM v s m o v i t s
WHERE MovimTwn <> 0
AND MovimTwn IN (
     SELECT MovimTwn
     FROM v s m o v i t s
     WHERE SrcNivel2=11983
)

  Para os descendentes dos descendentes

  depois...
  SELECT *
FROM v s m o v i t s
WHERE SrcNivel2=11993

SELECT *
FROM v s m o v i t s
WHERE MovimCod <> 0
AND MovimCod IN (
  SELECT MovimCod
  FROM v s m o v i t s
  WHERE Controle=12018
)
}

end;

end.

