unit VSCfgEstqVSxPQ_EFD;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  dmkDBGridZTO, Vcl.Mask, UnDmkProcFunc, dmkDBLookupComboBox, dmkEditCB,
  AppListas, UnProjGroup_Vars, UnProjGroup_Consts, mySQLDirectQuery,
  dmkCheckGroup, UMySQLDB, Vcl.Menus;

type
  TFmVSCfgEstqVSxPQ_EFD = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PB1: TProgressBar;
    DGPsq: TdmkDBGridZTO;
    DGGer: TdmkDBGridZTO;
    Splitter1: TSplitter;
    Panel5: TPanel;
    BtCorrige: TBitBtn;
    QrPsq: TmySQLQuery;
    DsPsq: TDataSource;
    QrGer: TmySQLQuery;
    DsGer: TDataSource;
    PMCorrige: TPopupMenu;
    QrGerNO_PRD_TAM_COR: TWideStringField;
    QrGerNO_MovimID: TWideStringField;
    QrGerNO_MovimNiv: TWideStringField;
    QrGerIMEIGer: TIntegerField;
    QrGerDataHora: TDateTimeField;
    QrGerDstNivel2: TIntegerField;
    QrGerPecas: TFloatField;
    QrGerPesoKg: TFloatField;
    QrGerAreaM2: TFloatField;
    QrGerMovimID: TIntegerField;
    QrGerMovimNiv: TIntegerField;
    QrGerGraGruX: TIntegerField;
    CorrigeIMEIDestinodagerao1: TMenuItem;
    CorrigeIMEIOrigemdagerao1: TMenuItem;
    RecalculaGeraoesaldo1: TMenuItem;
    BtReopen: TBitBtn;
    QrPsqIMEC: TIntegerField;
    QrPsqPesagem: TIntegerField;
    QrPsqDataEmis: TDateTimeField;
    QrPsqDtaBaixa: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtCorrigeClick(Sender: TObject);
    procedure QrPsqBeforeClose(DataSet: TDataSet);
    procedure QrPsqAfterScroll(DataSet: TDataSet);
    procedure QrGerAfterOpen(DataSet: TDataSet);
    procedure QrGerBeforeClose(DataSet: TDataSet);
    procedure PMCorrigePopup(Sender: TObject);
    procedure CorrigeIMEIOrigemdagerao1Click(Sender: TObject);
    procedure CorrigeIMEIDestinodagerao1Click(Sender: TObject);
    procedure BtReopenClick(Sender: TObject);
    procedure RecalculaGeraoesaldo1Click(Sender: TObject);
  private
    { Private declarations }
    FDataIniDta, FDataFimDta: TDateTime;
    FDataIniTxt, FDataFimTxt: String;
    //
    function  HabilitaCorrecaoPesoGeracaoPsqAtual(): Boolean;
    function  HabilitaCorrecaoPecaEPesoGeracaoPsqAtual(): Boolean;
    procedure RemoveIMEI(IMEIInn: Integer);
    procedure ReopenSdoInn(IMEIInn: Integer);
    procedure ReopenGeradores(IMEIGer: Integer);
    procedure PesquisaErros();
  public
    { Public declarations }
    FImporExpor, FAnoMes, FEmpresa, FPeriApu: Integer;
    //
    procedure PreencheAnoMesPeriodo(AnoMes: Integer);
  end;

  var
  FmVSCfgEstqVSxPQ_EFD: TFmVSCfgEstqVSxPQ_EFD;

implementation

uses UnMyObjects, DmkDAC_PF, Module, ModuleGeral, UMySQLModule, UnVS_CRC_PF,
  UnVS_PF;

{$R *.DFM}

procedure TFmVSCfgEstqVSxPQ_EFD.BtCorrigeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCorrige, BtCorrige);
end;

procedure TFmVSCfgEstqVSxPQ_EFD.BtOKClick(Sender: TObject);
begin
  PesquisaErros();
end;

procedure TFmVSCfgEstqVSxPQ_EFD.BtReopenClick(Sender: TObject);
begin
  ReopenSdoInn(0);
end;

procedure TFmVSCfgEstqVSxPQ_EFD.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCfgEstqVSxPQ_EFD.CorrigeIMEIDestinodagerao1Click(
  Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
var
  Controle: Integer;
  Gerandos, Gerado: Double;
begin
(*
  Controle := QrPsqIMEIInn.Value;
  VS_CRC_PF.MostraFormVSMovItsAlt(Controle, AtualizaSaldoModoGenerico,
    [eegbQtdOriginal]);
  ReopenSdoInn(Controle);
*)
end;

procedure TFmVSCfgEstqVSxPQ_EFD.CorrigeIMEIOrigemdagerao1Click(Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
var
  Controle: Integer;
begin
(*
  Controle := QrGerIMEIGer.Value;
  VS_CRC_PF.MostraFormVSMovItsAlt(Controle, AtualizaSaldoModoGenerico,
    [eegbQtdOriginal]);
  ReopenGeradores(Controle);
*)
end;

procedure TFmVSCfgEstqVSxPQ_EFD.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCfgEstqVSxPQ_EFD.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSCfgEstqVSxPQ_EFD.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmVSCfgEstqVSxPQ_EFD.HabilitaCorrecaoPecaEPesoGeracaoPsqAtual: Boolean;
begin
(*
  Result := (QrPsqSdoPc.Value < 0) and (QrPsqSdoKg.Value < 0) and
  (QrGer.State <> dsInactive) and (QrGer.RecordCount > 0)
  and (QrPsqMovimID.Value = Integer(emidCompra));
*)
end;

function TFmVSCfgEstqVSxPQ_EFD.HabilitaCorrecaoPesoGeracaoPsqAtual(): Boolean;
begin
(*
  Result := (QrGer.State <> dsInactive) and (QrGer.RecordCount > 0)
  and (QrPsqMovimID.Value = Integer(emidCompra)) and (QrPsqSdoPc.Value = 0);
*)
end;

procedure TFmVSCfgEstqVSxPQ_EFD.PesquisaErros();
begin
  Screen.Cursor := crHourGlass;
  try
  //
  FDataIniDta := Geral.AnoMesToData(FAnoMes, 1);
  FDataFimDta := Geral.AnoMesToData(FAnoMes + 1, 1) - 1;
  FDataIniTxt := Geral.FDT(FDataIniDta, 109);
  FDataFimTxt := Geral.FDT(FDataFimDta, 1) + ' 23:59:59';
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '1/2 - Criando tabela temporária de processos x pesagens');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _SPED_DATA_ERR_PQX_1; ',
  'CREATE TABLE _SPED_DATA_ERR_PQX_1 ',
  'SELECT *  ',
  'FROM ' + TMeuDB + '.emit ',
  'WHERE DtaBaixa BETWEEN "' + FDataIniTxt + '" ',
  'AND "' + FDataFimTxt + '" ',
  '; ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '2/2 - Abrindo tabela temporária de datas inválidas');
  ReopenSdoInn(0);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCfgEstqVSxPQ_EFD.PMCorrigePopup(Sender: TObject);
var
  Habilita1, Habilita2: Boolean;
begin
  Habilita1 := (QrPsq.State <> dsInactive) and (QrPsq.RecordCount > 0);
  CorrigeIMEIDestinodagerao1.Enabled := Habilita1;
  RecalculaGeraoesaldo1.Enabled := Habilita1;
  //
  Habilita2 := (QrGer.State <> dsInactive) and (QrGer.RecordCount > 0);
  CorrigeIMEIOrigemdagerao1.Enabled := Habilita2;
  //////////////////////////////////////////////////////////////////////////////
end;

procedure TFmVSCfgEstqVSxPQ_EFD.PreencheAnoMesPeriodo(AnoMes: Integer);
begin
end;

procedure TFmVSCfgEstqVSxPQ_EFD.QrGerAfterOpen(DataSet: TDataSet);
begin
  BtCorrige.Enabled := QrGer.RecordCount > 0;
end;

procedure TFmVSCfgEstqVSxPQ_EFD.QrGerBeforeClose(DataSet: TDataSet);
begin
  BtCorrige.Enabled := False;
end;

procedure TFmVSCfgEstqVSxPQ_EFD.QrPsqAfterScroll(DataSet: TDataSet);
begin
  ReopenGeradores(0);
end;

procedure TFmVSCfgEstqVSxPQ_EFD.QrPsqBeforeClose(DataSet: TDataSet);
begin
  QrGer.Close;
end;

procedure TFmVSCfgEstqVSxPQ_EFD.RecalculaGeraoesaldo1Click(Sender: TObject);
var
  Controle, MovimID, MovimNiv: Integer;
begin
(*
  Controle := QrPsqIMEIInn.Value;
  MovimID  := QrPsqMovimID.Value;
  MovimNiv := QrPsqMovimNiv.Value;
  //
  VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv);
  ReopenSdoInn(Controle);
*)
end;

procedure TFmVSCfgEstqVSxPQ_EFD.RemoveIMEI(IMEIInn: Integer);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM _Gerados_LCT_GER_',
  'WHERE SrcNivel2=' + Geral.FF0(IMEIInn),
  '']);
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM _Gerados_SDO_GER_',
  'WHERE IMEIInn=' + Geral.FF0(IMEIInn),
  '']);
end;

procedure TFmVSCfgEstqVSxPQ_EFD.ReopenGeradores(IMEIGer: Integer);
var
  ATT_MovimID, ATT_MovimNiv: String;
begin
(*
  ATT_MovimID := dmkPF.ArrayToTexto('ger.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('ger.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGer, DModG.MyPID_DB, [
  'SELECT ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR,  ',
  ATT_MovimID,
  ATT_MovimNiv,
  'ger.*  ',
  'FROM _Gerados_LCT_GER_ ger ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=ger.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.Controle  ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'WHERE ger.DstNivel2=' + Geral.FF0(QrPsqIMEIInn.Value),
  'ORDER BY ger.IMEIGer DESC ',
  '']);
  //Geral.MB_Info(QrGer.SQL.Text);
  QrGer.Locate('IMEIGer', IMEIGer, []);
*)
end;

procedure TFmVSCfgEstqVSxPQ_EFD.ReopenSdoInn(IMEIInn: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, DModG.MyPID_DB, [
  'SELECT DISTINCT ep1.VSMovCod IMEC, ep1.Codigo Pesagem,',
  'ep1.DataEmis, DtaBaixa  ',
  'FROM _SPED_DATA_ERR_PQX_1 ep1 ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  '  ON ep1.VSMovCod=vmi.MovimCod ',
  'WHERE vmi.DataHora > "' + FDataFimTxt + '" ',
  ' ']);
//
(*
var
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('sdo.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('sdo.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, DModG.MyPID_DB, [
  'SELECT sdo.*, ',
  ATT_MovimID,
  ATT_MovimNiv,
  'ggx.Controle Reduzido, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR ',
  'FROM _Gerados_SDO_GER_ sdo ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=sdo.GGXInn ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE SdoPc <= 0 ',
  'AND ( ',
  '  SdoM2 <> 0 ',
  '  OR ',
  '  SdoKg <> 0 ',
  ') ',
  'ORDER BY DataHora ',
  '']);
  //Geral.MB_SQL(self, QrPsq);
  QrPsq.Locate('IMEIInn', IMEIInn, []);
*)
end;

end.
