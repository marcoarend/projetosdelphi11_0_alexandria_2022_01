unit VSCustosDifPreRecl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, UnProjGroup_Consts;

type
  THackDBGrid = class(TDBGrid);
  TFmVSCustosDifPreRecl = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DGDados: TdmkDBGridZTO;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXNO_GraGruY: TWideStringField;
    QrGraGruXGrandeza: TSmallintField;
    DsGraGruX: TDataSource;
    PnPesquisa: TPanel;
    Label2: TLabel;
    Label8: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    BtOK: TBitBtn;
    BtTeste2: TBitBtn;
    CkTemIMEiMrt: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure BtTeste2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmVSCustosDifPreRecl: TFmVSCustosDifPreRecl;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModVS, UnVS_PF, ModuleGeral;

{$R *.DFM}

procedure TFmVSCustosDifPreRecl.BtOKClick(Sender: TObject);
var
  sControle, Tabela: String;
  ValorT: Double;
begin
  Screen.Cursor := crHourGlass;
  try
  DmModVS.QrCustosDifPreRecl.First;
  while not DmModVS.QrCustosDifPreRecl.Eof do
  begin
    if DmModVS.QrCustosDifPreReclDstNivel2.Value <> 0 then
    begin
      case DmModVS.QrCustosDifPreReclID_TTW.Value of
        0: Tabela := CO_TAB_VMI;
        1: Tabela := CO_TAB_VMB;
        else Tabela := '???';
      end;
      sControle := Geral.FF0(DmModVS.QrCustosDifPreReclDstNivel2.Value);
      ValorT := DmModVS.QrCustosDifPreReclValorT.Value;
      begin
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
        ' UPDATE ' + Tabela + ' SET ' +
        ' ValorT = ' + Geral.FFT_Dot(ValorT, 10, siNegativo) +
        ' WHERE Controle=' + sControle);
      end;
    end;
    DmModVS.QrCustosDifPreRecl.Next;
  end;
  //DmModVS.QrCustosDifPreRecl.Close;
  //DmModVS.QrCustosDifPreRecl.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCustosDifPreRecl.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCustosDifPreRecl.BtTeste2Click(Sender: TObject);
const
  Avisa = False;
  ForcaMostrarForm = False;
  SelfCall = True;
  MargemErro = 0.001;
var
  Filial, Empresa, GraGruX, TemIMEIMrt: Integer;
begin
  TemIMEIMrt := Geral.BoolToInt(CkTemIMEiMrt.Checked);
  //
  Filial := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  Empresa := DModG.QrEmpresasCodigo.Value;
  GraGruX := EdGraGruX.ValueVariant;
//  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o artigo!') then Exit;
  //
  DmModVS.CustosDifPreRecl(Empresa, GraGruX, MargemErro,
    Avisa, ForcaMostrarForm, SelfCall, TemIMEIMrt, LaAviso1, LaAviso2);
end;

procedure TFmVSCustosDifPreRecl.DGDadosDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  //
  if Campo = 'DstNivel2' then
      VS_PF.MostraFormVSMovIts(DmModVS.QrCustosDifPreReclDstNivel2.Value)
end;

procedure TFmVSCustosDifPreRecl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCustosDifPreRecl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  //
end;

procedure TFmVSCustosDifPreRecl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
