unit VSOutIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.ComCtrls,
  dmkEditDateTimePicker, Vcl.Grids, Vcl.DBGrids, UnProjGroup_Consts;

type
  TFmVSOutIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    QrVSPallet: TmySQLQuery;
    DsVSPallet: TDataSource;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    BtExclui: TBitBtn;
    QrGraGruXGraGruY: TIntegerField;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    Panel6: TPanel;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label52: TLabel;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    EdMovimCod: TdmkEdit;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    Label4: TLabel;
    SBPallet: TSpeedButton;
    Label9: TLabel;
    Label5: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    EdPecas: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    EdPesoKg: TdmkEdit;
    EdPsqPallet: TdmkEditCB;
    CBPsqPallet: TdmkDBLookupComboBox;
    EdObserv: TdmkEdit;
    EdValorT: TdmkEdit;
    Label14: TLabel;
    EdSrcMovID: TdmkEdit;
    Label10: TLabel;
    EdSrcNivel1: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    Label17: TLabel;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsNO_Pallet: TWideStringField;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TIntegerField;
    DsVSMovIts: TDataSource;
    Label11: TLabel;
    EdPsqFicha: TdmkEdit;
    QrVSMovItsSerieFch: TIntegerField;
    EdPsqSerieFch: TdmkEditCB;
    Label2: TLabel;
    CBPsqSerieFch: TdmkDBLookupComboBox;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    EdSrcGGX: TdmkEdit;
    Label12: TLabel;
    QrVSMovItsMarca: TWideStringField;
    EdPsqMarca: TdmkEdit;
    Label13: TLabel;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSMovItsClientMO: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBPalletClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure EdSrcMovIDChange(Sender: TObject);
    procedure EdSrcNivel1Change(Sender: TObject);
    procedure EdSrcNivel2Change(Sender: TObject);
    procedure EdFornecedorChange(Sender: TObject);
    procedure EdPsqPalletRedefinido(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure EdPsqFichaRedefinido(Sender: TObject);
    procedure EdPsqSerieFchRedefinido(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;

    //
    procedure ReopenVSOutIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure HabilitaInclusao();
    procedure ReopenVSMovIts();
    procedure ReopenVSPallet();

  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FAntSrcNivel2: Integer;
    //FDsCab: TDataSource;
  end;

  var
  FmVSOutIts: TFmVSOutIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
Principal, ModuleGeral, UnVS_PF, AppListas, VSOutCab;

{$R *.DFM}

procedure TFmVSOutIts.BtExcluiClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := EdControle.ValueVariant;
  if Geral.MB_Pergunta('Confirme a exclus�o do IME-I' +
  Geral.FF0(Controle) + '?') = ID_YES then
  begin
    VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle,
    Integer(TEstqMotivDel.emtdWetCurti022), Dmod.QrUpd, Dmod.MyDB, '');
    VS_PF.AtualizaSaldoIMEI(Controle, False);
    //
    Close;
  end;
end;

procedure TFmVSOutIts.BtOKClick(Sender: TObject);
const
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  CustoMOTot = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  FornecMO   = 0;
  NotaMPAG   = 0;
  //
  TpCalcAuto = -1;
  //
  EdFicha  = nil;
  EdPallet = nil;
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  StqCenLoc  = 0;
  ItemNFe    = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, SerieFch,
  Ficha, SrcNivel1, SrcNivel2, (*Misturou,*) GraGruY, SrcGGX,
  VSMulFrnCab, ClientMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
begin
  SrcMovID       := EdSrcMovID.ValueVariant;
  SrcNivel1      := EdSrcNivel1.ValueVariant;
  SrcNivel2      := EdSrcNivel2.ValueVariant;
  SrcGGX         := EdSrcGGX.ValueVariant;
  //
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  ClientMO       := QrVSMovItsClientMO.Value;
  Terceiro       := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' + EdDataHora.Text;
  MovimID        := emidForcado;
  MovimNiv       := eminSemNiv;
  Pallet         := QrVSMovItsPallet.Value;
  GraGruX        := QrVSMovItsGraGruX.Value;
  FUltGGX        := GraGruX;
  Pecas          := -EdPecas.ValueVariant;
  PesoKg         := -EdPesoKg.ValueVariant;
  AreaM2         := -EdAreaM2.ValueVariant;
  AreaP2         := -EdAreaP2.ValueVariant;
  ValorT         := -EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  //
  SerieFch       := QrVSMovItsSerieFch.Value;
  Ficha          := QrVSMovItsFicha.Value;
  Marca          := QrVSMovItsMarca.Value;
  //Misturou       := QrVSMovItsMisturou.Value;
  //
  GraGruY        := QrGraGruXGraGruY.Value;
  //

  //
(*
  if VS_PF.FichaErro(EdSerieFch, Empresa, Controle, Ficha) then
    Exit;
*)
  //
  if VS_PF.VSFic(GraGruX, Empresa, 0, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca,
  nil) then
    Exit;
  //
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
(*
  if Dmod.InsUpdVSMovIts_(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle) then
*)
  if VS_PF.InsUpdVSMovIts1(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca,
  QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, CO_0_GGXRcl) then
  begin
    VS_PF.AtualizaSaldoIMEI(FAntSrcNivel2, False);
    VS_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    VS_PF.AtualizaTotaisVSXxxCab('vsoutcab', MovimCod);
    FmVSOutCab.AtualizaNFeItens(FmVSOutCab.
    QrVSOutCabMovimCod.Value);
    //FmVSOutCab.LocCod(Codigo, Codigo);
    ReopenVSOutIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      EdValorT.ValueVariant      := 0;
      EdPsqPallet.ValueVariant   := 0;
      CBPsqPallet.KeyValue       := 0;
      EdPsqFicha.ValueVariant    := 0;
      EdObserv.Text              := '';
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmVSOutIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOutIts.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSOutIts.DBGrid1DblClick(Sender: TObject);
var
  Valor: Double;
begin
  EdSrcMovID.ValueVariant  := QrVSMovItsMovimID.Value;
  EdSrcNivel1.ValueVariant := QrVSMovItsCodigo.Value;
  EdSrcNivel2.ValueVariant := QrVSMovItsControle.Value;
  EdSrcGGX.ValueVariant    := QrVSMovItsGraGruX.Value;

  //
  EdPesoKg.ValueVariant := 0;
  EdAreaP2.ValueVariant := 0;
  // Deve ser depois dos zerados!
  EdPecas.ValueVariant  := QrVSMovItsSdoVrtPeca.Value;
  EdAreaM2.ValueVariant := QrVSMovItsSdoVrtArM2.Value;
  Valor := 0;
  (*
  case TEstqNivCtrl(Dmod.QrControleVSNivGer.Value) of
    encPecas: ;//Valor := 0;
    encArea: ;//Valor := 0;
    encValor:
    begin
  *)
      if (QrVSMovItsSdoVrtArM2.Value > 0) and (QrVSMovItsAreaM2.Value > 0) then
        Valor := QrVSMovItsSdoVrtArM2.Value *
        (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value)
      else
      if QrVSMovItsPecas.Value > 0 then
        Valor := QrVSMovItsSdoVrtPeca.Value *
        (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value);
  (*
    end;
    else
    begin
      //Valor := 0;
      Geral.MB_Aviso('N�vel de controle de estoque n�o implementado! [1]');
    end;
  end;
  *)
  EdValorT.ValueVariant := Valor;
end;

procedure TFmVSOutIts.EdPsqSerieFchRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

{
function TFmVSOutIts.DefineProximoPallet(): String;
var
  Txt: String;
begin
  Result := '';
  Txt := EdPallet.Text;
  if Geral.SoNumero_TT(Txt) = Txt then
  begin
    try
      Result := Geral.FI64(Geral.I64(EdPallet.Text) + 1);
    except
      Result := '';
    end;
  end else
    Result := '';
end;
}

procedure TFmVSOutIts.EdEmpresaRedefinido(Sender: TObject);
begin
  ReopenVSPallet();
end;

procedure TFmVSOutIts.EdFornecedorChange(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSOutIts.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenVSPallet();
  ReopenVSMovIts();
end;

procedure TFmVSOutIts.EdPsqFichaRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutIts.EdPsqPalletRedefinido(Sender: TObject);
begin
  ReopenVSMovIts();
end;

procedure TFmVSOutIts.EdSrcMovIDChange(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSOutIts.EdSrcNivel1Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSOutIts.EdSrcNivel2Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmVSOutIts.FormActivate(Sender: TObject);
begin
{
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdFornecedor.DataSource := FDsCab;
  //
}
  MyObjects.CorIniComponente();
end;

procedure TFmVSOutIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' + CO_GraGruY_ALL_VS + ') ');
  VS_PF.AbreVSSerFch(QrVSSerFch);
  //
  TPDataHora.Date := DModG.ObtemAgora();
  EdDataHora.Text := Geral.FDT(DModG.ObtemAgora(), 100);
end;

procedure TFmVSOutIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOutIts.HabilitaInclusao();
var
  Habilita: Boolean;
begin
  Habilita :=
    (EdSrcMovID.ValueVariant <> 0) and
    (EdSrcNivel1.ValueVariant <> 0) and
    (EdSrcNivel2.ValueVariant <> 0);
  //
  BtOK.Enabled := Habilita;
end;

procedure TFmVSOutIts.ImgTipoChange(Sender: TObject);
begin
  BtExclui.Visible := ImgTipo.SQLType = stUpd;
end;

procedure TFmVSOutIts.ReopenVSMovIts();
var
  GraGruX, Empresa, Pallet, SerieFch, Ficha: Integer;
  Marca, SQL_Pallet, SQL_SerieFch, SQL_Ficha, SQL_Marca: String;
begin
  GraGruX    := EdGraGruX.ValueVariant;
  Empresa    := EdEmpresa.ValueVariant;
  if (GragruX = 0) or (Empresa = 0) then
  begin
    QrVSMovIts.Close;
    Exit;
  end;
  SQL_Pallet := '';
  SQL_Ficha  := '';
  SQL_Marca  := '';
  Pallet     := EdPsqPallet.ValueVariant;
  Ficha      := EdPsqFicha.ValueVariant;
  Marca      := Trim(EdPsqMarca.Text);
  if Pallet <> 0 then
    SQL_Pallet := 'AND vmi.Pallet=' + Geral.FF0(Pallet);
  if SerieFch <> 0 then
    SQL_SerieFch  := 'AND vmi.SerieFch=' + Geral.FF0(SerieFch);
  if Ficha <> 0 then
    SQL_Ficha  := 'AND vmi.Ficha=' + Geral.FF0(Ficha);
  if Marca <> '' then
    SQL_Marca  := 'AND vmi.Marca="' + Marca + '"';
  //
  //
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT pal.Nome NO_Pallet, vmi.*,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vspalleta pal ON pal.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=vmi.Terceiro',
  'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
  'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
  'AND ( ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidCompra)),
  '  OR  ',
  '  vmi.SrcMovID<>0 ',
  ') ',
  'AND (vmi.SdoVrtPeca > 0 ',
  // Precisa?
  //'OR vmi.SdoVrtArM2 > 0 ',
  ') ',
  SQL_Pallet,
  SQL_SerieFch,
  SQL_Ficha,
  SQL_Marca,
  'ORDER BY DataHora, Pallet, Ficha ',
  '']);
  //
end;

procedure TFmVSOutIts.ReopenVSOutIts(Controle: Integer);
begin
  if (FQrIts <> nil)
  and (FQrCab <> nil) then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSOutIts.ReopenVSPallet();
var
  Empresa, GraGruX: Integer;
begin
  Empresa    := EdEmpresa.ValueVariant;
  GraGruX := EdGraGruX.ValueVariant;
  //
  if (Empresa = 0) or (GraGruX = 0) then
  begin
    QrVSPallet.Close;
    Exit;
  end;
  //
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT Codigo, Nome  ',
  'FROM vspalleta ',
  'WHERE Codigo IN ( ',
  '  SELECT DISTINCT vmi.Pallet  ',
  '  FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  '  WHERE vmi.Pecas >=0 ',
  '  AND vmi.SdoVrtPeca > 0 ',
  '  AND vmi.Empresa=' + Geral.FF0(Empresa),
  '  AND vmi.GraGruX=' + Geral.FF0(GraGruX),
  ') ',
  'ORDER BY Nome ',
  ' ']);
end;

procedure TFmVSOutIts.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_PF.MostraFormVSPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPsqPallet, CBPsqPallet, QrVSPallet, VAR_CADASTRO);
end;

procedure TFmVSOutIts.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
