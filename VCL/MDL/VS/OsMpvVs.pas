unit OsMpvVs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, UnProjGroup_Vars;

type
  TFmOsMpvVs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    QrOSs: TMySQLQuery;
    QrOSsCodigo: TIntegerField;
    DsOSs: TDataSource;
    LaPrompt: TLabel;
    EdOS: TdmkEditCB;
    CBOS: TdmkDBLookupComboBox;
    SBCadastro: TSpeedButton;
    QrMPVIts: TMySQLQuery;
    QrMPVItsCodigo: TIntegerField;
    QrMPVItsControle: TIntegerField;
    QrMPVItsMP: TIntegerField;
    QrMPVItsQtde: TFloatField;
    QrMPVItsPreco: TFloatField;
    QrMPVItsValor: TFloatField;
    QrMPVItsTexto: TWideStringField;
    QrMPVItsDesco: TFloatField;
    QrMPVItsEntrega: TDateField;
    QrMPVItsPronto: TDateField;
    QrMPVItsStatus: TIntegerField;
    QrMPVItsFluxo: TIntegerField;
    QrMPVItsClasse: TWideStringField;
    QrMPVItsPedido: TIntegerField;
    QrMPVItsEspesTxt: TWideStringField;
    QrMPVItsCorTxt: TWideStringField;
    QrMPVItsM2Pedido: TFloatField;
    QrMPVItsPrecoPed: TFloatField;
    QrMPVItsValorPed: TFloatField;
    QrMPVItsPecas: TFloatField;
    QrMPVItsUnidade: TIntegerField;
    QrMPVItsAlterWeb: TSmallintField;
    QrMPVItsAtivo: TSmallintField;
    QrMPVItsTipoProd: TSmallintField;
    QrMPVItsCustoPQ: TFloatField;
    QrMPVItsGraGruX: TIntegerField;
    QrMPVItsReceiRecu: TIntegerField;
    QrMPVItsReceiRefu: TIntegerField;
    QrMPVItsReceiAcab: TIntegerField;
    QrMPVItsTxtMPs: TWideStringField;
    QrMPVItsCustoWB: TFloatField;
    QrMPVItsDtaCrust: TDateField;
    QrMPVItsVSArtGGX: TIntegerField;
    QrMPVItsVSArtCab: TIntegerField;
    QrMPVItsAWServerID: TIntegerField;
    QrMPVItsAWStatSinc: TSmallintField;
    QrMPVItsFluxPcpCab: TIntegerField;
    QrMPP: TMySQLQuery;
    QrMPPCodigo: TIntegerField;
    QrMPPCliente: TIntegerField;
    QrMPPVendedor: TIntegerField;
    QrMPPDataF: TDateField;
    QrMPPQtde: TFloatField;
    QrMPPValor: TFloatField;
    QrMPPObz: TWideStringField;
    QrMPPTransp: TIntegerField;
    QrMPPCondicaoPg: TIntegerField;
    QrMPPPedidCli: TWideStringField;
    QrMPVItsObserv: TWideMemoField;
    QrMPVItsDescricao: TWideMemoField;
    QrMPVItsComplementacao: TWideMemoField;
    QrOSsDescricao: TWideStringField;
    QrMPVItsDataCad: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBCadastroClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenStqaLocIts(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmOsMpvVs: TFmOsMpvVs;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnApp_Jan, ModuleGeral, VSPWECab,
  UMySQLModule, UnVS_CRC_PF;

{$R *.DFM}

const
  FThisEmp = -11;

procedure TFmOsMpvVs.BtOKClick(Sender: TObject);
var
  SemiCab, SemiCul: String;
  //
  procedure SeparaLinhasSemi();
  var
    x, s: String;
    n: Integer;
  begin
    SemiCab := '';
    SemiCul := '';
    x := QrMPVItsEspesTxt.Value;
    n := Pos('-', x);
    if n = 0 then
      n := Pos('/', x);
    if n > 0 then
    begin
      s := Copy(x, 1, n-1);
      SemiCul := Geral.SoNumero_TT(s);
      s := Copy(x, n+1);
      SemiCab := Geral.SoNumero_TT(s);
    end;
  end;
var
  MPVIts: Integer;
  //
  Codigo, GraGruX, Empresa, ClientMO, MovimCod, TipoArea, FornecMO, MovimNiv,
  Forma, Quant, Cliente, CliVenda, StqCenLoc, NFeRem, GGXDst, SerieRem,
  EmitGru, VSArtCab, LinCulReb, LinCabReb, LinCulSem, LinCabSem, ReceiRecu,
  ReceiRefu: Integer;
  Nome, DtHrAberto, DataHora, LPFMO, EmitGrLote, DataPed, DataStart, DataCrust,
  DataExped: String;
  SQLType: TSQLType;
  ExigeNFe: Boolean;
  CustoMOM2: Double;
  Classes: String;
  Agora: TDateTime;
  Controle, Conta, VSMovimID, VSMovimCod: Integer;
  Observ: String;
begin
  SQLType := stIns;
  Agora := DModG.ObtemAgora();
  MPVIts := EdOS.ValueVariant;
  if MyObjects.FIC(MPVIts = 0, EdOS, 'Informe a OS!') then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPVIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM mpvits ',
  'WHERE Controle=' + Geral.FF0(MPVIts),
  '']);
  if QrOSs.RecordCount = 0 then
  begin
    Geral.MB_Erro('OS n�o localizada!');
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPP, Dmod.MyDB, [
  'SELECT * ',
  'FROM mpp ',
  'WHERE Codigo=' + Geral.FF0(QrMPVItsPedido.Value),
  '']);
  if QrMPP.RecordCount > 0 then
  begin
    Cliente    := QrMPPCliente.Value;
    EmitGrLote := QrMPPPedidCli.Value;
    DataPed    := Geral.FDT(QrMPPDataF.Value, 1);
  end else
  begin
    Cliente    := 0;
    EmitGrLote := '';
    DataPed    := Geral.FDT(Agora, 1);
  end;
  Empresa    := FThisEmp; //QrMPVItsEmpresa   .Value;
  DtHrAberto := Geral.FDT(Agora, 109);
  Nome       := QrOSsDescricao.Value;
  VAR_NOME_COPCab := QrOSsDescricao.Value;
  Observ       := QrMPVItsObserv.Value;

  TipoArea   := 0; // m2 - QrMPVItsTipoArea  .Value;
  GraGruX    := QrMPVItsGraGruX   .Value;
  LPFMO      := ''; //QrMPVItsLPFMO     .Value;
  NFeRem     := 0; //QrMPVItsNFeRem    .Value;
  SerieRem   := 0; //QrMPVItsSerieRem  .Value;
  GGXDst     := 0; //QrMPVItsGGXDst    .Value;
  EmitGru    := 1; // QrMPVItsEmitGru   .Value;
  VSArtCab   := QrMPVItsVSArtCab  .Value;
  SeparaLinhasSemi();
  LinCulSem  := Geral.IMV(SemiCul); //QrMPVItsLinCulSem .Value;
  LinCabSem  := Geral.IMV(SemiCab); //QrMPVItsLinCabSem .Value;
  DataStart  := Geral.FDT(QrMPVItsDataCad.Value, 1); //QrMPVItsDataStart .Value;
  DataCrust  := Geral.FDT(QrMPVItsDtaCrust.Value, 1);
  DataExped  := Geral.FDT(QrMPVItsEntrega.Value, 1);
  ReceiRecu  := QrMPVItsReceiRecu.Value;
  ReceiRefu  := QrMPVItsReceiRefu.Value;
  Classes    := QrMPVItsClasse.Value;
  //
  Codigo := UMyMod.BPGS1I32('vspwecab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vspwecab', False, [
  'MovimCod', 'Empresa', 'DtHrAberto',
  'Nome', 'TipoArea', 'GraGruX',
  'Cliente', 'LPFMO', 'NFeRem',
  'SerieRem', 'GGXDst', 'EmitGru',
  'VSArtCab', 'EmitGrLote',
  'LinCulReb', 'LinCabReb', 'LinCulSem',
  'LinCabSem', 'DataPed', 'DataStart',
  'DataCrust', 'DataExped', 'ReceiRecu',
  'ReceiRefu', 'Classes', 'MPVIts',
  'Observ'], [
  'Codigo'], [
  MovimCod, Empresa, DtHrAberto,
  Nome, TipoArea, GraGruX,
  Cliente, LPFMO, NFeRem,
  SerieRem, GGXDst, EmitGru,
  VSArtCab, EmitGrLote,
  LinCulReb, LinCabReb, LinCulSem,
  LinCabSem, DataPed, DataStart,
  DataCrust, DataExped, ReceiRecu,
  ReceiRefu, Classes, MPVIts,
  Observ], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidEmProcWE, Codigo);
    //
    FmVSPWECab.LocCod(Codigo, Codigo);
    if FmVSPWECab.QrVSPWECabCodigo.Value = Codigo then
    begin
      if QrMPVItsTipoProd.Value = 1 then
        ClientMO := FThisEmp
      else
        ClientMO := Cliente;
      FornecMO       := FThisEmp;
      StqCenLoc      := 1; //EdStqCenLoc.ValueVariant;
      FmVSPWECab.InsereArtigoEmProcesso(stIns, Codigo, MovimCod, Empresa, ClientMO,
        GraGruX, Cliente, DtHrAberto, FornecMO, StqCenLoc);
      //
      FmVSPWECab.LocCod(Codigo, Codigo);
      if FmVSPWECab.QrVSPWECabCodigo.Value = Codigo then
      begin
        Controle   := MPVIts;
        VSMovimID  := Integer(TEstqMovimID.emidEmProcWE);
        VSMovimCod := FmVSPWECab.QrVSPWECabMovimCod.Value;
        //
        Conta := UMyMod.BPGS1I32('mpvitvs', 'Conta', '', '', tsPos, stIns, 0);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mpvitvs', False, [
        'Controle', 'VSMovimID', 'VSMovimCod'], [
        'Conta'], [
        Controle, VSMovimID, VSMovimCod], [
        Conta], True);
        //
        FmVSPWECab.Altera();
        Close;
      end;
    end;
  end;
end;

procedure TFmOsMpvVs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOsMpvVs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOsMpvVs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenStqaLocIts(0);
end;

procedure TFmOsMpvVs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOsMpvVs.ReopenStqaLocIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSs, Dmod.MyDB, [
  'SELECT mi.Controle Codigo, CONCAT(mi.Texto, " ", mi.CorTxt,',
  '  " ", mi.EspesTxt, " (", ',
  '  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome), ")") Descricao ',
  'FROM mpvits mi ',
  'LEFT JOIN mpp pp ON pp.Codigo=mi.Pedido  ',
  'LEFT JOIN entidades cl ON cl.Codigo=pp.Cliente  ',
  'WHERE mi.Codigo = 0 ',
  //'AND MovimCod = 0 ',
  'ORDER BY Descricao',
  '']);
end;

procedure TFmOsMpvVs.SBCadastroClick(Sender: TObject);
var
  OS: Integer;
begin
  OS := App_Jan.MostraFormOSsAbertaMPV(True);
  if OS <> 0 then
  begin
    EdOS.ValueVariant := OS;
    CBOS.KeyValue     := OS;
  end;
end;

end.
