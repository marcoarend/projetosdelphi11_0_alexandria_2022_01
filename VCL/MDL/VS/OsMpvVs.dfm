object FmOsMpvVs: TFmOsMpvVs
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-265 :: Pesquisa OS MPV para VS'
  ClientHeight = 218
  ClientWidth = 729
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 729
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 681
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 633
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 328
        Height = 32
        Caption = 'Pesquisa OS MPV para VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 328
        Height = 32
        Caption = 'Pesquisa OS MPV para VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 328
        Height = 32
        Caption = 'Pesquisa OS MPV para VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 104
    Width = 729
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 725
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 148
    Width = 729
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 583
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 581
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 729
    Height = 56
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object LaPrompt: TLabel
      Left = 12
      Top = 8
      Width = 18
      Height = 13
      Caption = 'OS:'
    end
    object SBCadastro: TSpeedButton
      Left = 692
      Top = 24
      Width = 23
      Height = 22
      Caption = '?'
      OnClick = SBCadastroClick
    end
    object EdOS: TdmkEditCB
      Left = 12
      Top = 24
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBOS
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBOS: TdmkDBLookupComboBox
      Left = 80
      Top = 24
      Width = 613
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Descricao'
      ListSource = DsOSs
      TabOrder = 1
      dmkEditCB = EdOS
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrOSs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 208
    Top = 66
    object QrOSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 512
    end
  end
  object DsOSs: TDataSource
    DataSet = QrOSs
    Left = 208
    Top = 114
  end
  object QrMPVIts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 384
    Top = 114
    object QrMPVItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMPVItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMPVItsMP: TIntegerField
      FieldName = 'MP'
      Required = True
    end
    object QrMPVItsQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrMPVItsPreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
    object QrMPVItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrMPVItsTexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrMPVItsDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrMPVItsEntrega: TDateField
      FieldName = 'Entrega'
      Required = True
    end
    object QrMPVItsPronto: TDateField
      FieldName = 'Pronto'
      Required = True
    end
    object QrMPVItsStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrMPVItsFluxo: TIntegerField
      FieldName = 'Fluxo'
      Required = True
    end
    object QrMPVItsClasse: TWideStringField
      FieldName = 'Classe'
      Size = 15
    end
    object QrMPVItsPedido: TIntegerField
      FieldName = 'Pedido'
      Required = True
    end
    object QrMPVItsEspesTxt: TWideStringField
      FieldName = 'EspesTxt'
      Size = 10
    end
    object QrMPVItsCorTxt: TWideStringField
      FieldName = 'CorTxt'
      Size = 30
    end
    object QrMPVItsM2Pedido: TFloatField
      FieldName = 'M2Pedido'
      Required = True
    end
    object QrMPVItsPrecoPed: TFloatField
      FieldName = 'PrecoPed'
      Required = True
    end
    object QrMPVItsValorPed: TFloatField
      FieldName = 'ValorPed'
      Required = True
    end
    object QrMPVItsPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrMPVItsUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrMPVItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrMPVItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrMPVItsTipoProd: TSmallintField
      FieldName = 'TipoProd'
      Required = True
    end
    object QrMPVItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
    end
    object QrMPVItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrMPVItsReceiRecu: TIntegerField
      FieldName = 'ReceiRecu'
      Required = True
    end
    object QrMPVItsReceiRefu: TIntegerField
      FieldName = 'ReceiRefu'
      Required = True
    end
    object QrMPVItsReceiAcab: TIntegerField
      FieldName = 'ReceiAcab'
      Required = True
    end
    object QrMPVItsTxtMPs: TWideStringField
      FieldName = 'TxtMPs'
      Size = 255
    end
    object QrMPVItsCustoWB: TFloatField
      FieldName = 'CustoWB'
      Required = True
    end
    object QrMPVItsDtaCrust: TDateField
      FieldName = 'DtaCrust'
      Required = True
    end
    object QrMPVItsVSArtGGX: TIntegerField
      FieldName = 'VSArtGGX'
      Required = True
    end
    object QrMPVItsVSArtCab: TIntegerField
      FieldName = 'VSArtCab'
      Required = True
    end
    object QrMPVItsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrMPVItsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrMPVItsObserv: TWideMemoField
      FieldName = 'Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPVItsDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPVItsComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMPVItsFluxPcpCab: TIntegerField
      FieldName = 'FluxPcpCab'
      Required = True
    end
    object QrMPVItsDataCad: TDateField
      FieldName = 'DataCad'
    end
  end
  object QrMPP: TMySQLQuery
    Database = Dmod.MyDB
    Left = 452
    Top = 116
    object QrMPPCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMPPCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrMPPVendedor: TIntegerField
      FieldName = 'Vendedor'
      Required = True
    end
    object QrMPPDataF: TDateField
      FieldName = 'DataF'
      Required = True
    end
    object QrMPPQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrMPPValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrMPPObz: TWideStringField
      FieldName = 'Obz'
      Size = 255
    end
    object QrMPPTransp: TIntegerField
      FieldName = 'Transp'
      Required = True
    end
    object QrMPPCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
      Required = True
    end
    object QrMPPPedidCli: TWideStringField
      FieldName = 'PedidCli'
      Size = 60
    end
  end
end
