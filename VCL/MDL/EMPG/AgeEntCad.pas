unit AgeEntCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, Vcl.Grids,
  Vcl.DBGrids, dmkDBGridZTO;

type
  TFmAgeEntCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrAgeEntCad: TmySQLQuery;
    DsAgeEntCad: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DsPediPrzCab: TDataSource;
    QrCartEmis: TmySQLQuery;
    QrCartEmisCodigo: TIntegerField;
    QrCartEmisNome: TWideStringField;
    QrCartEmisTipo: TIntegerField;
    DsCartEmis: TDataSource;
    QrContas: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    Panel6: TPanel;
    Panel7: TPanel;
    EdCondicaoPG: TdmkEditCB;
    Label3: TLabel;
    CBCondicaoPG: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdCartEmis: TdmkEditCB;
    CBCartEmis: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdGeneroHon: TdmkEditCB;
    CBGeneroHon: TdmkDBLookupComboBox;
    SBCondicaoPG: TSpeedButton;
    SBCarteira: TSpeedButton;
    SbGeneroHon: TSpeedButton;
    RGUsaQualCrt: TdmkRadioGroup;
    RGDiaPagaHon: TdmkRadioGroup;
    QrAgeEntCadCodigo: TIntegerField;
    QrAgeEntCadCondicaoPG: TIntegerField;
    QrAgeEntCadGeneroHon: TIntegerField;
    QrAgeEntCadDiaPagaHon: TSmallintField;
    QrAgeEntCadLk: TIntegerField;
    QrAgeEntCadDataCad: TDateField;
    QrAgeEntCadDataAlt: TDateField;
    QrAgeEntCadUserCad: TIntegerField;
    QrAgeEntCadUserAlt: TIntegerField;
    QrAgeEntCadAlterWeb: TSmallintField;
    QrAgeEntCadAtivo: TSmallintField;
    QrAgeEntCadUsaQualCrt: TSmallintField;
    QrAgeEntCadCartEmis: TIntegerField;
    QrAgeEntCadNO_CARTEMIS: TWideStringField;
    QrAgeEntCadNO_CONTA: TWideStringField;
    QrAgeEntCadNO_ENTIDADE: TWideStringField;
    QrAgeEntCadNO_CONDICAOPG: TWideStringField;
    Panel9: TPanel;
    QrAgeEntCadEmpresa: TIntegerField;
    QrAgeEntCadNO_EMPRESA: TWideStringField;
    QrAgentes: TmySQLQuery;
    QrAgentesCodigo: TIntegerField;
    QrAgentesNO_AGENTE: TWideStringField;
    DsAgentes: TDataSource;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    DBEdNO_ENTIDADE: TdmkDBEdit;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrAgeEntCadNO_UsaQualCrt: TWideStringField;
    EdControle: TdmkEdit;
    Label7: TLabel;
    Label5: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    QrAgeEntCadControle: TIntegerField;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrCartEmisForneceI: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrAgeEntCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrAgeEntCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SBCondicaoPGClick(Sender: TObject);
    procedure SBCarteiraClick(Sender: TObject);
    procedure SbGeneroHonClick(Sender: TObject);
    procedure QrAgentesAfterScroll(DataSet: TDataSet);
    procedure QrAgentesBeforeClose(DataSet: TDataSet);
    procedure QrAgeEntCadBeforeClose(DataSet: TDataSet);
    procedure QrAgentesAfterOpen(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenAgeEntCad(Empresa: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmAgeEntCad: TFmAgeEntCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnGFat_Jan, UnFinanceiroJan, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmAgeEntCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmAgeEntCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrAgentesCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmAgeEntCad.DefParams;
begin
  VAR_GOTOTABELA := 'entidades';
  VAR_GOTOMYSQLTABLE := QrAgentes;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := VAR_FP_FUNCION;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ent.Codigo,  ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_AGENTE  ');
  VAR_SQLx.Add('FROM entidades ent ');
  VAR_SQLx.Add('WHERE ent.' + VAR_FP_FUNCION);
  VAR_SQLx.Add('AND ent.Codigo > 0');
  //
  VAR_SQL1.Add('AND ent.' + VAR_FP_FUNCION);
  VAR_SQL1.Add('AND ent.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ent.' + VAR_FP_FUNCION);
  VAR_SQLa.Add('AND IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE :P0');
  //
end;

procedure TFmAgeEntCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmAgeEntCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmAgeEntCad.ReopenAgeEntCad(Empresa: Integer);
var
  ATT_UsaQualCrt: String;
begin
  //
  ATT_UsaQualCrt := dmkPF.ArrayToTexto('aec.UsaQualCrt', 'NO_UsaQualCrt',
    pvPos, True, sUsaQualPredef);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgeEntCad, Dmod.MyDB, [
  'SELECT aec.*, car.Nome NO_CARTEMIS, cta.Nome NO_CONTA, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENTIDADE, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  ATT_UsaQualCrt,
  'ppc.Nome NO_CONDICAOPG ',
  'FROM ageentcad aec ',
  'LEFT JOIN entidades ent ON ent.Codigo=aec.Codigo ',
  'LEFT JOIN entidades emp ON emp.Codigo=aec.Empresa ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=aec.CondicaoPg ',
  'LEFT JOIN carteiras car ON car.Codigo=aec.CartEmis ',
  'LEFT JOIN contas cta ON cta.Codigo=aec.GeneroHon ',
  'WHERE aec.Codigo=' + Geral.FF0(QrAgentesCodigo.Value),
  '']);
  //
  if Empresa <> 0 then
    QrAgeEntCad.Locate('Empresa', Empresa, []);
end;

procedure TFmAgeEntCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmAgeEntCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmAgeEntCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmAgeEntCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmAgeEntCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmAgeEntCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAgeEntCad.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrAgeEntCad, [PnDados],
  [PnEdita], EdCondicaoPG, ImgTipo, 'ageentcad');
end;

procedure TFmAgeEntCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrAgentesCodigo.Value;
  Close;
end;

procedure TFmAgeEntCad.BtConfirmaClick(Sender: TObject);
var
  Codigo, CondicaoPG, GeneroHon, DiaPagaHon, UsaQualCrt, CartEmis, Controle,
  Empresa: Integer;
  SQLType: TSQLType;
  Qry: TmySQLQuery;
begin
  SQLType := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text); // Entidade
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Controle       := EdControle.ValueVariant;
  CondicaoPG     := EdCondicaoPG.ValueVariant;
  GeneroHon      := EdGeneroHon.ValueVariant;
  DiaPagaHon     := RGDiaPagaHon.ItemIndex;
  UsaQualCrt     := RGUsaQualCrt.ItemIndex;
  CartEmis       := EdCartEmis.ValueVariant;
  //
  if SQLType = stIns then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM ageentcad ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
      if MyObjects.FIC(Qry.RecordCount > 0, nil,
      'Empresa j� cadastrada para o Agente!' ) then
        Exit;
    finally
      Qry.Free;
    end;
  end;
  //
  if CartEmis <> 0 then
  begin
    if MyObjects.FIC(QrCartEmisForneceI.Value <> Empresa, EdCartEmis,
    'Carteira n�o pertence a empresa selecionada!') then
      Exit;
  end;
  Controle := UMyMod.BPGS1I32('ageentcad', 'Codigo', '', '', tsPos, SQLType,
    Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ageentcad', False, [
  'Codigo', 'Empresa', 'CondicaoPG',
  'GeneroHon', 'DiaPagaHon', 'UsaQualCrt',
  'CartEmis'], [
  'Controle'], [
  Codigo, Empresa, CondicaoPG,
  GeneroHon, DiaPagaHon, UsaQualCrt, CartEmis], [
  Controle], True) then
  //
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    QrAgeEntCad.Locate('Controle', Controle, []);
  end;
end;

procedure TFmAgeEntCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := QrAgentesCodigo.Value;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ageentcad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmAgeEntCad.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrAgeEntCad, [PnDados],
  [PnEdita], EdCondicaoPG, ImgTipo, 'ageentcad');
end;

procedure TFmAgeEntCad.FormCreate(Sender: TObject);
const
  Colunas = 1;
  Default = 0;
begin
  ImgTipo.SQLType := stLok;
  MyObjects.ConfiguraRadioGroup(RGUsaQualCrt, sUsaQualPredef, Colunas, Default);
  //MyObjects.ConfiguraDBRadioGroup(DBRGUsaQualCrt, sUsaQualPredef, Colunas);

  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa);
  //DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;

//ReopenContas();
  UnDmkDAC_PF.AbreMySQLQuery0(QrContas, Dmod.MyDB, [
    'SELECT Codigo, Nome',
    'FROM contas',
    'WHERE Codigo > 0',
    'AND Ativo = 1',
    'AND Debito="V" ',
    'ORDER BY Nome',
  '']);
//ReopenCartEmis(EmprEnti: Integer);
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartEmis, Dmod.MyDB, [
  'SELECT car.Codigo, car.Nome, ',
  'car.Tipo, car.ForneceI ',
  'FROM carteiras car ',
  'WHERE Codigo<>0 ',
  //'AND Tipo=2 ',
  //Geral.ATS_IF(EmprEnti <> 0, ['AND ForneceI=' + Geral.FF0(EmprEnti)]),
  'ORDER BY Nome ',
  '']);
//ReopenPediPrzCab();
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, MaxDesco, ',
  'JurosMes, Parcelas, Percent1, Percent2 ',
  'FROM pediprzcab ',
  //'WHERE Aplicacao & :P0 <> 0 ',
  'WHERE Aplicacao >= 0 ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmAgeEntCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrAgentesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAgeEntCad.SBCarteiraClick(Sender: TObject);
begin
  FinanceiroJan.InsereEDefineCarteira(EdCartEmis.ValueVariant, EdCartEmis,
  CBCartEmis, QrCartEmis);
end;

procedure TFmAgeEntCad.SBCondicaoPGClick(Sender: TObject);
begin
  GFat_Jan.InsereEDefinePediPrzCab(EdCondicaoPG.ValueVariant, EdCondicaoPG,
  CBCondicaoPG, QrPediPrzCab);
end;

procedure TFmAgeEntCad.SbGeneroHonClick(Sender: TObject);
begin
  FinanceiroJan.InsereEDefineConta(EdGeneroHon.ValueVariant, EdGeneroHon,
  CBGeneroHon, QrContas);
end;

procedure TFmAgeEntCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmAgeEntCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrAgentesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAgeEntCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmAgeEntCad.QrAgeEntCadAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrAgeEntCad.RecordCount > 0;
  QueryPrincipalAfterOpen;
end;

procedure TFmAgeEntCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAgeEntCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrAgentesCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ageentcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmAgeEntCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgeEntCad.QrAgeEntCadBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
end;

procedure TFmAgeEntCad.QrAgeEntCadBeforeOpen(DataSet: TDataSet);
begin
  QrAgeEntCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmAgeEntCad.QrAgentesAfterOpen(DataSet: TDataSet);
begin
  BtInclui.Enabled := QrAgentes.RecordCount > 0;
end;

procedure TFmAgeEntCad.QrAgentesAfterScroll(DataSet: TDataSet);
begin
  ReopenAgeEntCad(0);
end;

procedure TFmAgeEntCad.QrAgentesBeforeClose(DataSet: TDataSet);
begin
  BtInclui.Enabled := False;
  QrAgeEntCad.Close;
end;

end.

