unit UnEmpg_PF;


interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  UnDmkProcFunc, UnProjGroup_Vars;

type
  TUnEmpg_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MD5_AtualizaCheckSumAgeEqiCab(Codigo: Integer);
    function  MD5_AtualizaCheckSumOSAge(TabPai, FldPai, TabAge: String; IDPai: Integer): Integer;
    function  MD5_ObtemAgeEqiCab(Tabela, FldID, FldAge: String;
              Codigo: Integer; Database: TmySQLDatabase): Integer;
    function  MD5_ObtemCheckSumAgentes(Tabela, FldID, FldAge: String;
              ValID: Integer; Database: TmySQLDatabase): String;
    //
    procedure MostraFormAgeEqiCab(Codigo: Integer);
    procedure MostraFormAgeEqiCfg(Codigo: Integer);
  end;

var
  Empg_PF: TUnEmpg_PF;

implementation

uses MyDBCheck, DmkDAC_PF, Module, ModuleGeral, UMySQLModule,
  AgeEqiCab, AgeEqiCfg;

{ TUnEmpg_PF }

procedure TUnEmpg_PF.MD5_AtualizaCheckSumAgeEqiCab(Codigo: Integer);
var
  MD5: String;
begin
  MD5 := MD5_ObtemCheckSumAgentes('ageeqiits', 'Codigo', 'Entidade', Codigo,
    Dmod.MyDB);
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ageeqicab', False, [
  'MD5'], ['Codigo'], [MD5], [Codigo], True);
end;

function TUnEmpg_PF.MD5_AtualizaCheckSumOSAge(TabPai, FldPai, TabAge: String; IDPai: Integer): Integer;
  function CriaNovoAgeEqiCab(MD5: String): Integer;
  var
    Nome: String;
    Codigo, Controle, Entidade, EhLider: Integer;
    Qry: TmySQLQuery;
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      Codigo := UMyMod.BPGS1I32('ageeqicab', 'Codigo', '', '', tsPos, stIns, 0);
      //Nome := DModG.ConcatenaPrimeirosNomes(TabAge, FldPai, 'Agente', IDPai);
      Nome := 'Equipe ' + Geral.FFF(Codigo, 3);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ageeqicab', False, [
      'Nome', 'MD5'], [
      'Codigo'], [
      Nome, MD5], [
      Codigo], True) then
      begin
        Result := Codigo;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Agente ',
        'FROM ' + TabAge,
        'WHERE ' + FldPai + '=' + Geral.FF0(IDPai),
        '']);
        EhLider := 0;
        Qry.First;
        while not Qry.Eof do
        begin
          Entidade := Qry.FieldByName('Agente').AsInteger;
          //
          Controle := UMyMod.BPGS1I32('ageeqiits', 'Controle', '', '', tsPos, stIns, 0);
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ageeqiits', False, [
          'Codigo', 'Entidade', 'EhLider'], [
          'Controle'], [
          Codigo, Entidade, EhLider], [
          Controle], True);
          //
          Qry.Next;
        end;
      end else
        Result := 0;
    finally
      Qry.Free;
    end;
  end;
var
  Qry: TmySQLQuery;
  MD5: String;
  AgeEqiCab, Codigo: Integer;
begin
  Result := 0;
  AgeEqiCab := MD5_ObtemAgeEqiCab(TabAge, 'Controle', 'Agente', IDPai, Dmod.MyDB);
  if AgeEqiCab = 0 then
  begin
    MD5 := MD5_ObtemCheckSumAgentes(TabAge, 'Controle', 'Agente', IDPai, Dmod.MyDB);
    if MD5 <> '' then
      AgeEqiCab := CriaNovoAgeEqiCab(MD5);
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabPai, False, [
  'AgeEqiCab'], [FldPai], [AgeEqiCab], [IDPai], True) then
    Result := AgeEqiCab;
end;

function TUnEmpg_PF.MD5_ObtemAgeEqiCab(Tabela, FldID, FldAge: String;
  Codigo: Integer; Database: TmySQLDatabase): Integer;
var
  MD5: String;
  Qry: TmySQLQuery;
begin
  Result := 0;
  MD5 := MD5_ObtemCheckSumAgentes(Tabela, FldID, FldAge, Codigo, Database);
  //
  if MD5 <> '' then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM ageeqicab ',
      'WHERE MD5="' + MD5 + '" ',
      '']);
      Result := Qry.FieldByName('Codigo').AsInteger;
    finally
      Qry.Free;
    end;
  end;
end;

function TUnEmpg_PF.MD5_ObtemCheckSumAgentes(Tabela, FldID, FldAge: String;
  ValID: Integer; Database: TmySQLDatabase): String;
var
  Texto: String;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Result := '';
    Texto := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
    'SELECT ' + FldAge + ' Agente ',
    'FROM ' + Tabela,
    'WHERE ' + FldID + '=' + Geral.FF0(ValID),
    'ORDER BY Agente']);
    Qry.First;
    while not Qry.Eof do
    begin
      Texto := Texto + ',' + Geral.FF0(Qry.FieldByName('Agente').AsInteger);
      //
      Qry.Next;
    end;
    //
    if Texto <> '' then
    begin
      Texto := Copy(Texto, 2);
      Result := DModG.MD5_por_SQL(Texto);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnEmpg_PF.MostraFormAgeEqiCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmAgeEqiCab, FmAgeEqiCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmAgeEqiCab.LocCod(Codigo, Codigo);
    //
    FmAgeEqiCab.ShowModal;
    FmAgeEqiCab.Destroy;
  end;
end;

procedure TUnEmpg_PF.MostraFormAgeEqiCfg(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmAgeEqiCfg, FmAgeEqiCfg, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmAgeEqiCfg.LocCod(Codigo, Codigo);
    //
    FmAgeEqiCfg.ShowModal;
    FmAgeEqiCfg.Destroy;
  end;
end;

end.
