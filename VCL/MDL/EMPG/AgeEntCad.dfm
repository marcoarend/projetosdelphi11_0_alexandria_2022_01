object FmAgeEntCad: TFmAgeEntCad
  Left = 368
  Top = 194
  Caption = 'AGE-ENTID-001 :: Cadastro de Agentes'
  ClientHeight = 476
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 380
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 297
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 42
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label7: TLabel
          Left = 712
          Top = 0
          Width = 14
          Height = 13
          Caption = 'ID:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label5: TLabel
          Left = 16
          Top = 0
          Width = 37
          Height = 13
          Caption = 'Agente:'
          FocusControl = dmkDBEdit2
        end
        object DBEdNO_ENTIDADE: TdmkDBEdit
          Left = 76
          Top = 16
          Width = 633
          Height = 21
          Color = clWhite
          DataField = 'NO_AGENTE'
          DataSource = DsAgentes
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object EdControle: TdmkEdit
          Left = 712
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 16
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsAgentes
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taRightJustify
        end
      end
      object Panel7: TPanel
        Left = 2
        Top = 57
        Width = 780
        Height = 238
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object Label3: TLabel
          Left = 16
          Top = 40
          Width = 119
          Height = 13
          Caption = 'Condi'#231#227'o de pagamento:'
        end
        object Label4: TLabel
          Left = 16
          Top = 80
          Width = 235
          Height = 13
          Caption = 'Carteira: (Pagamento de honor'#225'rios = Espec'#237'fico) '
        end
        object Label15: TLabel
          Left = 16
          Top = 120
          Width = 125
          Height = 13
          Caption = 'Conta do plano de contas:'
        end
        object SBCondicaoPG: TSpeedButton
          Left = 516
          Top = 56
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBCondicaoPGClick
        end
        object SBCarteira: TSpeedButton
          Left = 515
          Top = 96
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBCarteiraClick
        end
        object SbGeneroHon: TSpeedButton
          Left = 515
          Top = 136
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbGeneroHonClick
        end
        object Label2: TLabel
          Left = 16
          Top = 0
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object EdCondicaoPG: TdmkEditCB
          Left = 16
          Top = 56
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CondicaoPG'
          UpdCampo = 'CondicaoPG'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCondicaoPG
          IgnoraDBLookupComboBox = False
        end
        object CBCondicaoPG: TdmkDBLookupComboBox
          Left = 72
          Top = 56
          Width = 440
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsPediPrzCab
          TabOrder = 1
          dmkEditCB = EdCondicaoPG
          QryCampo = 'CondicaoPG'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCartEmis: TdmkEditCB
          Left = 16
          Top = 96
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CartEmis'
          UpdCampo = 'CartEmis'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCartEmis
          IgnoraDBLookupComboBox = False
        end
        object CBCartEmis: TdmkDBLookupComboBox
          Left = 72
          Top = 96
          Width = 440
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCartEmis
          TabOrder = 3
          dmkEditCB = EdCartEmis
          QryCampo = 'CartEmis'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdGeneroHon: TdmkEditCB
          Left = 16
          Top = 136
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'GeneroHon'
          UpdCampo = 'GeneroHon'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBGeneroHon
          IgnoraDBLookupComboBox = False
        end
        object CBGeneroHon: TdmkDBLookupComboBox
          Left = 72
          Top = 136
          Width = 440
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContas
          TabOrder = 5
          dmkEditCB = EdGeneroHon
          QryCampo = 'GeneroHon'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object RGUsaQualCrt: TdmkRadioGroup
          Left = 544
          Top = 8
          Width = 225
          Height = 149
          Caption = ' Carteira para pagamento de honor'#225'rios: '
          Items.Strings = (
            'sUsaQualPredef')
          TabOrder = 6
          QryCampo = 'UsaQualCrt'
          UpdCampo = 'UsaQualCrt'
          UpdType = utYes
          OldValor = 0
        end
        object RGDiaPagaHon: TdmkRadioGroup
          Left = 16
          Top = 160
          Width = 753
          Height = 73
          Caption = 'Dia padr'#227'o do pagamento: '
          Columns = 16
          ItemIndex = 0
          Items.Strings = (
            '00'
            '01'
            '02'
            '03'
            '04'
            '05'
            '06'
            '07'
            '08'
            '09'
            '10'
            '11'
            '12'
            '13'
            '14'
            '15'
            '16'
            '17'
            '18'
            '19'
            '20'
            '21'
            '22'
            '23'
            '24'
            '25'
            '26'
            '27'
            '28'
            '29'
            '30'
            '31')
          TabOrder = 7
          QryCampo = 'DiaPagaHon'
          UpdCampo = 'DiaPagaHon'
          UpdType = utYes
          OldValor = 0
        end
        object EdEmpresa: TdmkEditCB
          Left = 16
          Top = 15
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 72
          Top = 15
          Width = 465
          Height = 21
          Color = clWhite
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 9
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 317
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 380
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 261
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel9: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 46
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 0
          Width = 37
          Height = 13
          Caption = 'Agente:'
          FocusControl = DBEdCodigo
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 16
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsAgentes
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 76
          Top = 16
          Width = 692
          Height = 21
          Color = clWhite
          DataField = 'NO_AGENTE'
          DataSource = DsAgentes
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 2
        Top = 61
        Width = 780
        Height = 198
        Align = alClient
        DataSource = DsAgeEntCad
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_EMPRESA'
            Title.Caption = 'Empresa'
            Width = 132
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CONDICAOPG'
            Title.Caption = 'Condi'#231#227'o de pagamento'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CONTA'
            Title.Caption = 'Conta (plano de contas)'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CARTEMIS'
            Title.Caption = 'Carteira'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DiaPagaHon'
            Title.Caption = 'Dia'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_UsaQualCrt'
            Title.Caption = 'Qual carteira'
            Width = 103
            Visible = True
          end>
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 316
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 256
        Height = 32
        Caption = 'Cadastro de Agentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 256
        Height = 32
        Caption = 'Cadastro de Agentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 256
        Height = 32
        Caption = 'Cadastro de Agentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrAgeEntCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrAgeEntCadBeforeOpen
    AfterOpen = QrAgeEntCadAfterOpen
    BeforeClose = QrAgeEntCadBeforeClose
    SQL.Strings = (
      'SELECT aec.*, car.Nome NO_CARTEMIS, cta.Nome NO_CONTA,  '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENTIDADE, '
      'ppc.Nome NO_CONDICAOPG '
      'FROM ageentcad aec '
      'LEFT JOIN entidades ent ON ent.Codigo=aec.Codigo '
      'LEFT JOIN pediprzcab ppc ON ent.Codigo=aec.CondicaoPg '
      'LEFT JOIN carteiras car ON car.Codigo=aec.CartEmis '
      'LEFT JOIN contas cta ON ent.Codigo=aec.GeneroHon')
    Left = 228
    Top = 44
    object QrAgeEntCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgeEntCadEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrAgeEntCadControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAgeEntCadCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
    end
    object QrAgeEntCadGeneroHon: TIntegerField
      FieldName = 'GeneroHon'
    end
    object QrAgeEntCadDiaPagaHon: TSmallintField
      FieldName = 'DiaPagaHon'
    end
    object QrAgeEntCadLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAgeEntCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAgeEntCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAgeEntCadUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAgeEntCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAgeEntCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAgeEntCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrAgeEntCadUsaQualCrt: TSmallintField
      FieldName = 'UsaQualCrt'
    end
    object QrAgeEntCadCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrAgeEntCadNO_CARTEMIS: TWideStringField
      FieldName = 'NO_CARTEMIS'
      Size = 100
    end
    object QrAgeEntCadNO_CONTA: TWideStringField
      FieldName = 'NO_CONTA'
      Size = 50
    end
    object QrAgeEntCadNO_ENTIDADE: TWideStringField
      FieldName = 'NO_ENTIDADE'
      Size = 100
    end
    object QrAgeEntCadNO_CONDICAOPG: TWideStringField
      FieldName = 'NO_CONDICAOPG'
      Size = 50
    end
    object QrAgeEntCadNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrAgeEntCadNO_UsaQualCrt: TWideStringField
      FieldName = 'NO_UsaQualCrt'
      Size = 30
    end
  end
  object DsAgeEntCad: TDataSource
    DataSet = QrAgeEntCad
    Left = 228
    Top = 92
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 384
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 384
    Top = 92
  end
  object QrCartEmis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome, car.Tipo'
      'FROM carteiras car'
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 308
    Top = 44
    object QrCartEmisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCartEmisNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCartEmisTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCartEmisForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object DsCartEmis: TDataSource
    DataSet = QrCartEmis
    Left = 308
    Top = 92
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome, car.Tipo'
      'FROM carteiras car'
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 464
    Top = 44
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 464
    Top = 92
  end
  object QrAgentes: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrAgentesAfterOpen
    BeforeClose = QrAgentesBeforeClose
    AfterScroll = QrAgentesAfterScroll
    Left = 108
    Top = 298
    object QrAgentesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgentesNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
  end
  object DsAgentes: TDataSource
    DataSet = QrAgentes
    Left = 108
    Top = 346
  end
end
