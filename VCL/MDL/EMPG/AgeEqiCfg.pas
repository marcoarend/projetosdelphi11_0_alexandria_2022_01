unit AgeEqiCfg;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, dmkDBGridZTO, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB,
  dmkCheckBox;

type
  TFmAgeEqiCfg = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrAgeEqiCfg: TmySQLQuery;
    DsAgeEqiCfg: TDataSource;
    QrAgeEqiCab: TmySQLQuery;
    DsAgeEqiCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TdmkDBGridZTO;
    QrAgeEqiCfgCodigo: TIntegerField;
    QrAgeEqiCfgNome: TWideStringField;
    QrAgeEqiCfgLk: TIntegerField;
    QrAgeEqiCfgDataCad: TDateField;
    QrAgeEqiCfgDataAlt: TDateField;
    QrAgeEqiCfgUserCad: TIntegerField;
    QrAgeEqiCfgUserAlt: TIntegerField;
    QrAgeEqiCfgAlterWeb: TSmallintField;
    QrAgeEqiCfgAtivo: TSmallintField;
    QrAgeEqiCabCodigo: TIntegerField;
    QrAgeEqiCabLk: TIntegerField;
    QrAgeEqiCabDataCad: TDateField;
    QrAgeEqiCabDataAlt: TDateField;
    QrAgeEqiCabUserCad: TIntegerField;
    QrAgeEqiCabUserAlt: TIntegerField;
    QrAgeEqiCabAlterWeb: TSmallintField;
    QrAgeEqiCabAtivo: TSmallintField;
    QrAgeEqiCabNome: TWideStringField;
    GroupBox1: TGroupBox;
    EdHrInnDay: TdmkEdit;
    Label3: TLabel;
    EdHrOutDay: TdmkEdit;
    Label4: TLabel;
    GroupBox2: TGroupBox;
    CkTemInterv: TdmkCheckBox;
    PnIntervalo: TPanel;
    Label5: TLabel;
    EdHrOutMid: TdmkEdit;
    EdHrInnMid: TdmkEdit;
    Label6: TLabel;
    QrAgeEqiCfgHrInnDay: TTimeField;
    QrAgeEqiCfgHrOutMid: TTimeField;
    QrAgeEqiCfgHrInnMid: TTimeField;
    QrAgeEqiCfgHrOutDay: TTimeField;
    QrAgeEqiCfgTemInterv: TSmallintField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label13: TLabel;
    Label14: TLabel;
    DBCheckBox1: TDBCheckBox;
    Label8: TLabel;
    Label10: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrAgeEqiCfgAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrAgeEqiCfgBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrAgeEqiCfgAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrAgeEqiCfgBeforeClose(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure CkTemIntervClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraAgeEqiCab(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenAgeEqiCab(Codigo: Integer);

  end;

var
  FmAgeEqiCfg: TFmAgeEqiCfg;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, AgeEqiIts, UnEmpg_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmAgeEqiCfg.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmAgeEqiCfg.MostraAgeEqiCab(SQLType: TSQLType);
begin
(*
  if DBCheck.CriaFm(TFmAgeEqiIts, FmAgeEqiIts, afmoNegarComAviso) then
  begin
    FmAgeEqiIts.ImgTipo.SQLType := SQLType;
    FmAgeEqiIts.FQrCab := QrAgeEqiCfg;
    FmAgeEqiIts.FDsCab := DsAgeEqiCfg;
    FmAgeEqiIts.FQrIts := QrAgeEqiCab;
    if SQLType = stIns then
      //
    else
    begin
      FmAgeEqiIts.EdControle.ValueVariant := QrAgeEqiCabControle.Value;
      //
      FmAgeEqiIts.CkEhLider.Checked := Geral.IntToBool(QrAgeEqiCabEhLider.Value);
      FmAgeEqiIts.EdEntidade.ValueVariant := QrAgeEqiCabEntidade.Value;
      FmAgeEqiIts.EdEntidade.ValueVariant := QrAgeEqiCabEntidade.Value;
      //
    end;
    FmAgeEqiIts.ShowModal;
    FmAgeEqiIts.Destroy;
  end;
*)
end;

procedure TFmAgeEqiCfg.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrAgeEqiCfg);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrAgeEqiCfg, QrAgeEqiCab);
end;

procedure TFmAgeEqiCfg.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrAgeEqiCfg);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrAgeEqiCab);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrAgeEqiCab);
end;

procedure TFmAgeEqiCfg.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrAgeEqiCfgCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmAgeEqiCfg.DefParams;
begin
  VAR_GOTOTABELA := 'ageeqicfg';
  VAR_GOTOMYSQLTABLE := QrAgeEqiCfg;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM ageeqicfg');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmAgeEqiCfg.ItsAltera1Click(Sender: TObject);
begin
  MostraAgeEqiCab(stUpd);
end;

procedure TFmAgeEqiCfg.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmAgeEqiCfg.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmAgeEqiCfg.QueryPrincipalAfterOpen;
begin
end;

procedure TFmAgeEqiCfg.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
(*
  if UMyMod.ExcluiRegistroInt1('Confirma a retirada do agente selecionado?',
  'AgeEqiIts', 'Controle', QrAgeEqiCabControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrAgeEqiCab,
      QrAgeEqiCabControle, QrAgeEqiCabControle.Value);
    ReopenAgeEqiCab(Controle);
  end;
*)
end;

procedure TFmAgeEqiCfg.ReopenAgeEqiCab(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgeEqiCab, Dmod.MyDB, [
  'SELECT *  ',
  'FROM ageeqicab ',
  'WHERE AgeEqiCfg=' + Geral.FF0(QrAgeEqiCfgCodigo.Value),
  'ORDER BY Nome ',
  '']);
  //
  QrAgeEqiCab.Locate('Codigo', Codigo, []);
end;


procedure TFmAgeEqiCfg.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmAgeEqiCfg.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmAgeEqiCfg.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmAgeEqiCfg.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmAgeEqiCfg.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmAgeEqiCfg.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAgeEqiCfg.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrAgeEqiCfgCodigo.Value;
  Close;
end;

procedure TFmAgeEqiCfg.ItsInclui1Click(Sender: TObject);
begin
  MostraAgeEqiCab(stIns);
end;

procedure TFmAgeEqiCfg.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrAgeEqiCfg, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ageeqicfg');
end;

procedure TFmAgeEqiCfg.BtConfirmaClick(Sender: TObject);
var
  Nome, HrInnDay, HrOutMid, HrInnMid, HrOutDay: String;
  Codigo, TemInterv: Integer;
begin
  Codigo    := EdCodigo.ValueVariant;
  Nome      := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then
    Exit;
  HrInnDay       := EdHrInnDay.Text;
  HrOutMid       := EdHrOutMid.Text;
  HrInnMid       := EdHrInnMid.Text;
  HrOutDay       := EdHrOutDay.Text;
  TemInterv      := Geral.BoolToInt(CkTemInterv.Checked);
  if CkTemInterv.Checked then
  begin
    //??? o turno pode passar da meia noite!
  end;
  //
  Codigo := UMyMod.BPGS1I32('ageeqicfg', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ageeqicfg', False, [
  'Nome', 'HrInnDay', 'HrOutMid',
  'HrInnMid', 'HrOutDay', 'TemInterv'], [
  'Codigo'], [
  Nome, HrInnDay, HrOutMid,
  HrInnMid, HrOutDay, TemInterv], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmAgeEqiCfg.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ageeqicfg', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ageeqicfg', 'Codigo');
end;

procedure TFmAgeEqiCfg.BtItsClick(Sender: TObject);
begin
////////////////////////////////////////////////////////////////////////////////
///
///  N�o permitir altera��o!
///  A equipe � cadastrada pelo sistema!
///
////////////////////////////////////////////////////////////////////////////////
//
  //MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
//
end;

procedure TFmAgeEqiCfg.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmAgeEqiCfg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  UnDmkDAC_PF.AbreQuery(QrAgeEqiCfg, Dmod.MyDB);
end;

procedure TFmAgeEqiCfg.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrAgeEqiCfgCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAgeEqiCfg.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmAgeEqiCfg.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmAgeEqiCfg.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  //LaRegistro.Caption := GOTOy.CodUsu(QrAgeEqiCfgCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAgeEqiCfg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmAgeEqiCfg.QrAgeEqiCfgAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmAgeEqiCfg.QrAgeEqiCfgAfterScroll(DataSet: TDataSet);
begin
  ReopenAgeEqiCab(0);
end;

procedure TFmAgeEqiCfg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrAgeEqiCfgCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmAgeEqiCfg.SbQueryClick(Sender: TObject);
begin
  LocCod(QrAgeEqiCfgCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ageeqicfg', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmAgeEqiCfg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgeEqiCfg.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrAgeEqiCfg, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ageeqicfg');
end;

procedure TFmAgeEqiCfg.CkTemIntervClick(Sender: TObject);
begin
  PnIntervalo.Visible := CkTemInterv.Checked;
end;

procedure TFmAgeEqiCfg.QrAgeEqiCfgBeforeClose(
  DataSet: TDataSet);
begin
  QrAgeEqiCab.Close;
end;

procedure TFmAgeEqiCfg.QrAgeEqiCfgBeforeOpen(DataSet: TDataSet);
begin
  QrAgeEqiCfgCodigo.DisplayFormat := FFormatFloat;
end;

end.

