unit UnitOrdProd;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db,
  DbCtrls, Variants, mySQLDbTables, UnDmkEnums, UnDmkProcFunc, DmkDAC_PF,
  UMySQLModule;

type
  TUnitOrdProd = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // Q U E R I E S
    procedure ReopenGrupos(Qry: TmySQLQuery);
    procedure ReopenGraGruX(Qry: TmySQLQuery; _WHERE: String = '');
    procedure ReopenPrestador(Qry: TmySQLQuery);
    procedure ReopenCliente(Qry: TmySQLQuery);
    // F O R M S
    procedure MostraFormOrdProdCab();
  end;

var
  UnOrdProd: TUnitOrdProd;

implementation

uses Module, ModuleGeral, OrdProdCab, MyDBCheck;

{ TUnitOrdProd }

procedure TUnitOrdProd.MostraFormOrdProdCab;
begin
  if DBCheck.CriaFm(TFmOrdProdCab, FmOrdProdCab, afmoSoBoss) then
  begin
    FmOrdProdCab.ShowModal;
    FmOrdProdCab.Destroy;
  end;
end;

procedure TUnitOrdProd.ReopenCliente(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ent.Codigo, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE ',
    'FROM entidades ent ',
    'WHERE ent.Cliente1="V" ',
    'ORDER BY NOMEENTIDADE ',
    '']);
end;

procedure TUnitOrdProd.ReopenGraGruX(Qry: TmySQLQuery; _WHERE: String = '');
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, ',
    'ggx.GraGruY ',
    'FROM gragrux ggx',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
    _WHERE,
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
end;

procedure TUnitOrdProd.ReopenGrupos(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM ordprodgru ',
    'WHERE Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
end;

procedure TUnitOrdProd.ReopenPrestador(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ent.Codigo, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE ',
    'FROM entidades ent ',
    'WHERE ent.Fornece7="V" ',
    'ORDER BY NOMEENTIDADE ',
    '']);
end;

end.
