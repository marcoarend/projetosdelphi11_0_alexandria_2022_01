unit OrdProdCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB;

type
  TFmOrdProdCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPrdProdCab: TmySQLQuery;
    QrPrdProdCabCodigo: TIntegerField;
    QrPrdProdCabNome: TWideStringField;
    DsPrdProdCab: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Label66: TLabel;
    EdGrupo: TdmkEditCB;
    CBGrupo: TdmkDBLookupComboBox;
    EdNome: TdmkEdit;
    Label9: TLabel;
    Label56: TLabel;
    EdLPFMO: TdmkEdit;
    EdSerieRem: TdmkEdit;
    Label57: TLabel;
    EdNFeRem: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label4: TLabel;
    EdHora: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    Label53: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    GBEdita2: TGroupBox;
    Label3: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label63: TLabel;
    CBGGXDst: TdmkDBLookupComboBox;
    EdGGXDst: TdmkEditCB;
    Label35: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    EdStqCenLoc: TdmkEditCB;
    Label49: TLabel;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    Label54: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdObserv: TdmkEdit;
    Label12: TLabel;
    Label48: TLabel;
    EdCustoMOM2: TdmkEdit;
    QrOrdProdGru: TmySQLQuery;
    IntegerField1: TIntegerField;
    DsOrdProdGru: TDataSource;
    QrOrdProdGruNome: TWideStringField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrGGXDst: TmySQLQuery;
    QrGGXDstGraGru1: TIntegerField;
    QrGGXDstControle: TIntegerField;
    QrGGXDstNO_PRD_TAM_COR: TWideStringField;
    QrGGXDstSIGLAUNIDMED: TWideStringField;
    QrGGXDstCODUSUUNIDMED: TIntegerField;
    QrGGXDstNOMEUNIDMED: TWideStringField;
    DsGGXDst: TDataSource;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    DsStqCenLoc: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPrdProdCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPrdProdCabBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmOrdProdCab: TFmOrdProdCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, UnitOrdProd, UnGrade_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOrdProdCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOrdProdCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrdProdCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOrdProdCab.DefParams;
begin
  VAR_GOTOTABELA := 'ordprodcab';
  VAR_GOTOMYSQLTABLE := QrPrdProdCab;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM ordprodcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmOrdProdCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOrdProdCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOrdProdCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmOrdProdCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOrdProdCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOrdProdCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOrdProdCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOrdProdCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOrdProdCab.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPrdProdCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ordprodcab');
end;

procedure TFmOrdProdCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPrdProdCabCodigo.Value;
  Close;
end;

procedure TFmOrdProdCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ordprodcab', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrPrdProdCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'ordprodcab', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
*)
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmOrdProdCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ordprodcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmOrdProdCab.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPrdProdCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ordprodcab');
end;

procedure TFmOrdProdCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBDados.Align   := alClient;
  CriaOForm;
  //
  CBEmpresa.ListSource   := DModG.DsEmpresas;
  CBGrupo.Listsource     := DsOrdProdGru;
  CBGraGruX.ListSource   := DsGraGruX;
  CBGGXDst.ListSource    := DsGGXDst;
  CBFornecMO.ListSource  := DsPrestador;
  CBCliente.ListSource   := DsCliente;
  CBStqCenLoc.ListSource := DsStqCenLoc;
  //
  UnOrdProd.ReopenGrupos(QrOrdProdGru);
  UnOrdProd.ReopenGraGruX(QrGraGruX);
  UnOrdProd.ReopenGraGruX(QrGGXDst);
  UnOrdProd.ReopenPrestador(QrPrestador);
  UnOrdProd.ReopenCliente(QrCliente);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmOrdProdCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPrdProdCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOrdProdCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOrdProdCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPrdProdCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOrdProdCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOrdProdCab.QrPrdProdCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOrdProdCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOrdProdCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPrdProdCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ordprodcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOrdProdCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOrdProdCab.QrPrdProdCabBeforeOpen(DataSet: TDataSet);
begin
  QrPrdProdCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

