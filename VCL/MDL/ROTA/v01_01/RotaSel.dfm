object FmRotaSel: TFmRotaSel
  Left = 339
  Top = 185
  Caption = 'CAD-ROTAS-002 :: Sele'#231#227'o de Rota'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 201
        Height = 32
        Caption = 'Sele'#231#227'o de Rota'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 201
        Height = 32
        Caption = 'Sele'#231#227'o de Rota'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 201
        Height = 32
        Caption = 'Sele'#231#227'o de Rota'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object DBGRota: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 808
          Height = 450
          Align = alClient
          DataSource = DsRotaLatLon
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGRotaDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Distance'
              Title.Caption = 'Dist'#226'ncia km'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STC_Codi'
              Title.Caption = 'Lugar'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STC_Nome'
              Title.Caption = 'Nome do lugar'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Latitude'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Longitude'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Rot_Codi'
              Title.Caption = 'Rota'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Rot_Nome'
              Title.Caption = 'Descri'#231#227'o da rota'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrRotaLatLon: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rll.Codigo Rot_Codi, rll.Nome Rot_Nome, '
      'stc.Codigo STC_Codi, stc.Nome STC_Nome,  '
      'stc.Latitude, stc.Longitude, ( '
      '        acos( '
      '            cos( radians(-23.441828) ) * '
      '            cos( radians( stc.Latitude ) ) * '
      '            cos( radians( stc.Longitude ) - '
      '            radians(-51.923194) ) + '
      '            sin( radians(-23.441828) ) * '
      '            sin( radians( stc.Latitude ) ) '
      '        ) '
      '    ) * 6371 '
      '    AS Distance '
      'FROM siaptercad stc '
      'LEFT JOIN rotalatlon rll ON rll.Codigo=stc.RotaLatLon '
      'WHERE stc.Codigo <> 10310 /* (1310 > Cleide)*/ '
      'AND ( '
      '    stc.Latitude <> 0 '
      '    OR '
      '    stc.Longitude <> 0 ) '
      'ORDER BY Distance ')
    Left = 356
    Top = 192
    object QrRotaLatLonRot_Codi: TIntegerField
      FieldName = 'Rot_Codi'
    end
    object QrRotaLatLonRot_Nome: TWideStringField
      FieldName = 'Rot_Nome'
      Size = 60
    end
    object QrRotaLatLonSTC_Codi: TIntegerField
      FieldName = 'STC_Codi'
    end
    object QrRotaLatLonSTC_Nome: TWideStringField
      FieldName = 'STC_Nome'
      Size = 100
    end
    object QrRotaLatLonLatitude: TFloatField
      FieldName = 'Latitude'
    end
    object QrRotaLatLonLongitude: TFloatField
      FieldName = 'Longitude'
    end
    object QrRotaLatLonDistance: TFloatField
      FieldName = 'Distance'
      DisplayFormat = '#,###,##0.0'
    end
  end
  object DsRotaLatLon: TDataSource
    DataSet = QrRotaLatLon
    Left = 356
    Top = 240
  end
end
