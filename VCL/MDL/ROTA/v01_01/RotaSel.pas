unit RotaSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables,
  Variants;

type
  TFmRotaSel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBGRota: TdmkDBGridZTO;
    QrRotaLatLon: TmySQLQuery;
    DsRotaLatLon: TDataSource;
    QrRotaLatLonRot_Codi: TIntegerField;
    QrRotaLatLonRot_Nome: TWideStringField;
    QrRotaLatLonSTC_Codi: TIntegerField;
    QrRotaLatLonSTC_Nome: TWideStringField;
    QrRotaLatLonLatitude: TFloatField;
    QrRotaLatLonLongitude: TFloatField;
    QrRotaLatLonDistance: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGRotaDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSelecionado: Integer;
    procedure ReopenRotaLatLon(Codigo: Integer; Latitude, Longitude: Double);
  end;

  var
  FmRotaSel: TFmRotaSel;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, MyDBCheck;

{$R *.DFM}

procedure TFmRotaSel.BtOKClick(Sender: TObject);
  procedure InsereRotaLatLon(RotaLatLon: Integer; Nome: String);
  var
    Codigo: Integer;
  begin
    Codigo := QrRotaLatLonSTC_Codi.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'siaptercad', False, [
    'RotaLatLon'], [
    'Codigo'], [
    RotaLatLon], [
    Codigo], True) then
      Geral.MB_Info('O lugar "' + QrRotaLatLonSTC_Nome.Value +
      '" foi adicionado � rota "' + Nome + '".');
  end;
const
  Aviso  = '...';
  Titulo = 'Sele��o de Rota';
  Prompt = 'Informe a rota: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Nome, Endereco: String;
  Codigo, RotaLatLon: Integer;
  Latitude, Longitude: Double;
  AcaoRota: Byte;
  //
  Escolhido: Variant;
  SQL_WHERE, Tabela: String;
begin
  FSelecionado := QrRotaLatLonRot_Codi.Value;
  if FSelecionado = 0 then
  begin
    AcaoRota := MyObjects.SelRadioGroup('Rota',
    'O que deseja fazer?',
    ['Criar nova rota e adicionar o lugar selecionado',
    'Adicionar o lugar selecionado a uma rota existente',
    'N�o quero fazer nada'], -1);
    case AcaoRota of
      0:
      begin
        //if Geral.MB_Pergunta('O lugar selecionado n�o possui rota definida!' +
        //sLineBreak + 'Deseja criar uma nova rota?') = ID_YES then
        //begin
        Codigo         := 0;
        Nome           := '';
        Endereco       := '';
        Latitude       := 0;
        Longitude      := 0;
        //
        if InputQuery('Inclus�o de Nova Rota',
        'Informe o nome da nova rota:', Nome) then
        begin
          Codigo := UMyMod.BPGS1I32('rotalatlon', 'Codigo', '', '', tsPos, stIns, Codigo);
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'rotalatlon', False, [
          'Nome', 'Endereco', 'Latitude',
          'Longitude'], [
          'Codigo'], [
          Nome, Endereco, Latitude,
          Longitude], [
          Codigo], True) then
          begin
            FSelecionado := Codigo;
            RotaLatLon   := Codigo;
            //
            InsereRotaLatLon(RotaLatLon, Nome);
            //
            Close;
          end;
        end;
      end;
      1:
      begin
        Escolhido := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
        'SELECT Codigo, Nome ' + Campo,
        'FROM rotalatlon ',
        'ORDER BY ' + Campo,
        ''], Dmod.MyDB, True);
        if Escolhido <> Null then
        begin
          RotaLatLon := Escolhido;
          FSelecionado := RotaLatLon;
          Nome := VAR_SELNOM;
          //
          InsereRotaLatLon(RotaLatLon, Nome);
          //UnDMkDAC_PF.AbreQuery(QrRotaLatLon, Dmod.MyDB);
          //QrRotaLatLon.Locate('STC_Codi', RotaLatLon);
          Close;
        end;
      end;
      2: ; //nada
      else
        Geral.MB_Erro('Opc�o n�o implementada!');
    end;
  end else
    Close;
end;

procedure TFmRotaSel.BtSaidaClick(Sender: TObject);
begin
  FSelecionado := 0;
  Close;
end;

procedure TFmRotaSel.DBGRotaDblClick(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmRotaSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmRotaSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSelecionado := 0;
end;

procedure TFmRotaSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRotaSel.ReopenRotaLatLon(Codigo: Integer; Latitude, Longitude: Double);
var
  Lat, Lon: String;
begin
  Lat := Geral.FFN_Dot(Latitude);
  Lon := Geral.FFN_Dot(Longitude);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRotaLatLon, Dmod.MyDB, [
  'SELECT rll.Codigo Rot_Codi, rll.Nome Rot_Nome, ',
  'stc.Codigo STC_Codi, stc.Nome STC_Nome,  ',
  'stc.Latitude, stc.Longitude, ( ',
  '        acos( ',
  '            cos( radians(' + Lat + ') ) * ',
  '            cos( radians( stc.Latitude ) ) * ',
  '            cos( radians( stc.Longitude ) - ',
  '            radians(' + Lon + ') ) + ',
  '            sin( radians(' + Lat + ') ) * ',
  '            sin( radians( stc.Latitude ) ) ',
  '        ) ',
  '    ) * 6371 ',
  '    AS Distance ',
  'FROM siaptercad stc ',
  'LEFT JOIN rotalatlon rll ON rll.Codigo=stc.RotaLatLon ',
  'WHERE stc.Codigo <> ' + Geral.FF0(Codigo),
  'AND ( ',
  '    stc.Latitude <> 0 ',
  '    OR ',
  '    stc.Longitude <> 0 ',
  '    ) ',
  'ORDER BY Distance ',
  '']);
  //
  //Geral.MB_SQL(Self, QrRotaLatLon);
end;

end.
