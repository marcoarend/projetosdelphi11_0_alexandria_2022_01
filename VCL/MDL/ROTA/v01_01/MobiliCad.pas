unit MobiliCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, UnDmkEnums;

type
  TFmMobiliCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrMobiliCad: TmySQLQuery;
    DsMobiliCad: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrMobiliCadCodigo: TIntegerField;
    QrMobiliCadNome: TWideStringField;
    QrMobiliCadStqCenCad: TIntegerField;
    QrMobiliCadLk: TIntegerField;
    QrMobiliCadDataCad: TDateField;
    QrMobiliCadDataAlt: TDateField;
    QrMobiliCadUserCad: TIntegerField;
    QrMobiliCadUserAlt: TIntegerField;
    QrMobiliCadAlterWeb: TSmallintField;
    QrMobiliCadAtivo: TSmallintField;
    QrMobiliCadNO_StqCenCad: TWideStringField;
    QrStqCenCad: TmySQLQuery;
    DsStqCenCad: TDataSource;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    Label3: TLabel;
    EdGraG1Veic: TdmkEditCB;
    CBGraG1Veic: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    SbGraG1Veic: TSpeedButton;
    SbStqCenCad: TSpeedButton;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    QrGraG1Veic: TmySQLQuery;
    QrGraG1VeicNivel1: TIntegerField;
    DsGraG1Veic: TDataSource;
    QrMobiliCadGraG1Veic: TIntegerField;
    QrMobiliCadNO_GraG1Veic: TWideStringField;
    QrGraG1VeicNome: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMobiliCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMobiliCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbGraG1VeicClick(Sender: TObject);
    procedure SbStqCenCadClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmMobiliCad: TFmMobiliCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, Principal, ModProd;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMobiliCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMobiliCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMobiliCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMobiliCad.DefParams;
begin
  VAR_GOTOTABELA := 'mobilicad';
  VAR_GOTOMYSQLTABLE := QrMobiliCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT moc.*,');
  VAR_SQLx.Add('gg1.Nome NO_GraG1Veic, scc.Nome NO_StqCenCad');
  VAR_SQLx.Add('FROM mobilicad moc');
  VAR_SQLx.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=moc.GraG1Veic');
  VAR_SQLx.Add('LEFT JOIN stqcencad scc ON scc.Codigo=moc.StqCenCad');
  VAR_SQLx.Add('WHERE moc.Codigo > 0');
  //
  VAR_SQL1.Add('AND moc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND moc.Nome Like :P0');
  //
end;

procedure TFmMobiliCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmMobiliCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmMobiliCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmMobiliCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMobiliCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMobiliCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMobiliCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMobiliCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMobiliCad.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrMobiliCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'mobilicad');
end;

procedure TFmMobiliCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMobiliCadCodigo.Value;
  Close;
end;

procedure TFmMobiliCad.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, GraG1Veic, StqCenCad: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  StqCenCad      := EdStqCenCad.ValueVariant;
  GraG1Veic      := EdGraG1Veic.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  if MyObjects.FIC(StqCenCad = 0, EdNome, 'Defina um centro de estoque!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('mobilicad', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'mobilicad', False, [
  'Nome', 'GraG1Veic', 'StqCenCad'], [
  'Codigo'], [
  Nome, GraG1Veic, StqCenCad], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmMobiliCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'mobilicad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmMobiliCad.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrMobiliCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'mobilicad');
end;

procedure TFmMobiliCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrGraG1Veic, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
end;

procedure TFmMobiliCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMobiliCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmMobiliCad.SbGraG1VeicClick(Sender: TObject);
begin
(*
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormMobiliUni();
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(
      EdMobiliUni, CBMobiliUni, QrMobiliUni, VAR_CADASTRO);
*)
end;

procedure TFmMobiliCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMobiliCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrMobiliCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmMobiliCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMobiliCad.QrMobiliCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmMobiliCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMobiliCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrMobiliCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'mobilicad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmMobiliCad.SbStqCenCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  DmProd.MostraStqCenCad(EdStqCenCad.ValueVariant);
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(
      EdStqCenCad, CBStqCenCad, QrStqCenCad, VAR_CADASTRO);
end;

procedure TFmMobiliCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMobiliCad.QrMobiliCadBeforeOpen(DataSet: TDataSet);
begin
  QrMobiliCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

