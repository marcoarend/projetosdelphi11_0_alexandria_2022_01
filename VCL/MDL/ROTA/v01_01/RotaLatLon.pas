unit RotaLatLon;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmRotaLatLon = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrRotaLatLon: TmySQLQuery;
    QrRotaLatLonCodigo: TIntegerField;
    QrRotaLatLonNome: TWideStringField;
    DsRotaLatLon: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Label8: TLabel;
    EdLatitude: TdmkEdit;
    Label3: TLabel;
    EdLongitude: TdmkEdit;
    SBCoord: TSpeedButton;
    EdEndereco: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    QrRotaLatLonEndereco: TWideStringField;
    QrRotaLatLonLatitude: TFloatField;
    QrRotaLatLonLongitude: TFloatField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrRotaLatLonAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrRotaLatLonBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SBCoordClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmRotaLatLon: TFmRotaLatLon;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnDmkWeb;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmRotaLatLon.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmRotaLatLon.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrRotaLatlonCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmRotaLatLon.DefParams;
begin
  VAR_GOTOTABELA := 'rotalatlon';
  VAR_GOTOMYSQLTABLE := QrRotaLatlon;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM rotalatlon');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmRotaLatLon.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmRotaLatLon.QueryPrincipalAfterOpen;
begin
end;

procedure TFmRotaLatLon.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmRotaLatLon.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmRotaLatLon.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmRotaLatLon.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmRotaLatLon.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmRotaLatLon.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRotaLatLon.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrRotaLatlon, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'rotalatlon');
end;

procedure TFmRotaLatLon.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrRotaLatlonCodigo.Value;
  Close;
end;

procedure TFmRotaLatLon.BtConfirmaClick(Sender: TObject);
var
  Nome, Endereco: String;
  Codigo: Integer;
  Latitude, Longitude: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Endereco       := EdEndereco.ValueVariant;
  Latitude       := EdLatitude.ValueVariant;
  Longitude      := EdLongitude.ValueVariant;
  //
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('rotalatlon', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'rotalatlon', False, [
  'Nome', 'Endereco',
  'Latitude', 'Longitude'], [
  'Codigo'], [
  Nome, Endereco,
  Latitude, Longitude], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmRotaLatLon.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'rotalatlon', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmRotaLatLon.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrRotaLatlon, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'rotalatlon');
end;

procedure TFmRotaLatLon.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmRotaLatLon.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrRotaLatlonCodigo.Value, LaRegistro.Caption);
end;

procedure TFmRotaLatLon.SBCoordClick(Sender: TObject);
var
  Latitude, Longitude: Double;
begin
  if DmkWeb.DefineCoordenadas(
  EdEndereco.Text, Latitude, Longitude, True, False, 0, 0) then
  begin
    EdLatitude.ValueVariant := Latitude;
    EdLongitude.ValueVariant := Longitude;
  end;
end;

procedure TFmRotaLatLon.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmRotaLatLon.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrRotaLatlonCodigo.Value, LaRegistro.Caption);
end;

procedure TFmRotaLatLon.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmRotaLatLon.QrRotaLatLonAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmRotaLatLon.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmRotaLatLon.SbQueryClick(Sender: TObject);
begin
  LocCod(QrRotaLatlonCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'rotalatlon', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmRotaLatLon.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRotaLatLon.QrRotaLatLonBeforeOpen(DataSet: TDataSet);
begin
  QrRotaLatlonCodigo.DisplayFormat := FFormatFloat;
end;

end.

