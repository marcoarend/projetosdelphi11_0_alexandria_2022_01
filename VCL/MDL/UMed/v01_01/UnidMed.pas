unit UnidMed;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
{$IfNDef SemNFe_0000}
    {$IfDef ComSPED_0000}
  SPED_Listas,
  {$EndIf}
{$EndIf}
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, UnDmkProcFunc, dmkImage,
  UnDmkEnums;

type
  TFmUnidMed = class(TForm)
    PainelDados: TPanel;
    DsUnidMed: TDataSource;
    QrUnidMed: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedNome: TWideStringField;
    EdSigla: TdmkEdit;
    Label4: TLabel;
    QrUnidMedSigla: TWideStringField;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    RGGrandeza: TdmkRadioGroup;
    QrUnidMedGrandeza: TSmallintField;
    DBRadioGroup1: TDBRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel4: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrUM: TmySQLQuery;
    QrUMCodigo: TIntegerField;
    QrUMNome: TWideStringField;
    QrUMSigla: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrUnidMedAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrUnidMedBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmUnidMed: TFmUnidMed;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects,
{$IfNDef _tmp_EhLgistic}
//NFe_PF,
{$EndIf}
Module, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmUnidMed.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmUnidMed.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrUnidMedCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmUnidMed.DefParams;
begin
  VAR_GOTOTABELA := 'UnidMed';
  VAR_GOTOMYSQLTABLE := QrUnidMed;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome, Sigla, Grandeza');
  VAR_SQLx.Add('FROM unidmed');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmUnidMed.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'UnidMed', 'CodUsu', [], [],
      stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmUnidMed.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmUnidMed.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmUnidMed.QueryPrincipalAfterOpen;
begin
end;

procedure TFmUnidMed.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmUnidMed.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmUnidMed.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmUnidMed.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmUnidMed.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmUnidMed.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrUnidMed, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'unidmed');
end;

procedure TFmUnidMed.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrUnidMedCodigo.Value;
  Close;
end;

procedure TFmUnidMed.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome, Sigla: String;
begin
  Nome  := EdNome.ValueVariant;
  Sigla := EdSigla.ValueVariant;
  Codigo := EdCodigo.ValueVariant;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Sigla = '', EdSigla, 'Defina uma sigla!') then Exit;
  //
  if (RGGrandeza.ItemIndex = -1) or (RGGrandeza.ItemIndex = RGGrandeza.Items.Count-1) then
  begin
    Geral.MB_Aviso('Defina uma Grandeza v�lida!');
    Exit;
  end;
  if DModG.SiglaDuplicada(Sigla, 'Sigla', 'Codigo', 'Nome', 'unidmed',
  Codigo) then
    Exit;
    //
  Codigo := UMyMod.BuscaEmLivreY_Def('UnidMed', 'Codigo', ImgTipo.SQLType,
    QrUnidMedCodigo.Value);
  //
  {$IfNDef _tmp_EhLgistic}
{
  UnNFe_PF.ReopenUnidMed(Sigla, QrUM, Codigo);
  if MyObjects.FIC(QrUM.RecordCount > 0, nil,
  'Sigla j� cadastrada sob o c�digo ' + Geral.FF0(QrUMCodigo.Value) +
  sLineBreak + 'Chave prim�ria do Registro SPED EFD 0190.') then Exit;
}
  //
{$EndIf}
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmUnidMed, PainelEdit,
    'UnidMed', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmUnidMed.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'UnidMed', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'UnidMed', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'UnidMed', 'Codigo');
end;

procedure TFmUnidMed.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrUnidMed, [PainelDados],
    [PainelEdita], EdSigla, ImgTipo, 'unidmed');
  //
  RGGrandeza.ItemIndex := Integer(TGrandezaUnidMed.gumOutros);
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'UnidMed', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmUnidMed.FormCreate(Sender: TObject);
const
  Colunas = 5;
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
  //
{$IfNDef SemNFe_0000}
{$IfDef comSPED_0000}
  MyObjects.ConfiguraRadioGroup(RGGrandeza, sGRANDEZA_UNIDMED,  Colunas,
    Integer(TGrandezaUnidMed.gumOutros));
{$EndIf}
{$EndIf}
end;

procedure TFmUnidMed.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrUnidMedCodigo.Value, LaRegistro.Caption);
end;

procedure TFmUnidMed.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmUnidMed.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmUnidMed.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrUnidMedCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmUnidMed.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmUnidMed.QrUnidMedAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmUnidMed.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmUnidMed.SbQueryClick(Sender: TObject);
begin
  LocCod(QrUnidMedCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'UnidMed', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmUnidMed.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmUnidMed.QrUnidMedBeforeOpen(DataSet: TDataSet);
begin
  QrUnidMedCodigo.DisplayFormat := FFormatFloat;
end;

{
SELECT DISTINCT gg1.Nivel1, gg1.Nome,
gg1.GerBxaEstq, unm.Sigla, unm.Nome
FROM stqmovitsa smia
LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX
LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1
LEFT JOIN unidmed unm ON unm.Codigo=gg1.UnidMed
WHERE Tipo IN (101,104,107,108)
}

end.

