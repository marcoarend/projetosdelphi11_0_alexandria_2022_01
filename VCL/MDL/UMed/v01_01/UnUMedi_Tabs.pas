unit UnUMedi_Tabs;
{
function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista: TList): Boolean;
      UMedi_Tabs.CarregaListaTabelas(FTabelas);

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    UMedi_Tabs.CarregaListaSQL(Tabela, FListaSQL);

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String; FLIndices: TList): Boolean;
      UMedi_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList; var Temcontrole: TTemControle): Boolean;
      UMedi_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    UMedi_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);

function TMyListas.CriaListaJanelas(FLJanelas: TList): Boolean;
  UMedi_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
}

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) mySQLDBTables, UnMyLinguas, Forms, UnInternalConsts, MyDBCheck,
  dmkGeral, UnDmkEnums, UnProjGroup_Consts;

type
  TUMedi_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Database: TmySQLDatabase;
             Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList;
             DMKID_APP: Integer): Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  UMedi_Tabs: TUMedi_Tabs;

implementation

uses UMySQLModule, Module, MyListas;

function TUMedi_Tabs.CarregaListaTabelas(Database: TmySQLDatabase;
  Lista: TList<TTabelas>): Boolean;
begin
  try
    if Database = Dmod.MyDB then
    begin
      if CO_SIGLA_APP = 'CLRC' then
      begin
        MyLinguas.AdTbLst(Lista, False, Lowercase('UnidMed'), '');
      end else
      begin
        //MyLinguas.AdTbLst(Lista, False, Lowercase('???'), '_lst_sample');
        MyLinguas.AdTbLst(Lista, False, Lowercase('UnidMed'), '');
      end;
      //
    end;
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUMedi_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
  TStringList; DMKID_APP: Integer): Boolean;
begin
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('???') then
  begin
    if FListaSQL.Count = 0 then  // evitar erro no blue derm! 2012-01-16
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"???"');
  end else
  if Uppercase(Tabela) = Uppercase('UnidMed') then
  begin
    if FListaSQL.Count = 0 then  // evitar erro no blue derm! 2012-01-16
      FListaSQL.Add('Codigo|CodUsu|Sequencia|Grandeza|Sigla|Nome');
    //
    if DMKID_APP = 24 then //Bugstrol
    begin
      FListaSQL.Add('1|001|000|000|"P�"|"PE�A"');
      FListaSQL.Add('2|002|000|000|"PCTE"|"PACOTE"');
      FListaSQL.Add('3|003|000|005|"L"|"LITROS"');
      FListaSQL.Add('4|004|000|005|"ML"|"MILILITROS"');
      FListaSQL.Add('5|005|000|000|"BL"|"BLOCO"');
      FListaSQL.Add('6|006|000|002|"G"|"GRAMAS"');
      FListaSQL.Add('7|007|000|000|"UN"|"UNIDADE"');
    end else
    begin
      FListaSQL.Add('-1|-1|000|003|"??"|"? ? ? ? ?"');
      FListaSQL.Add('0|000|000|003|""|""');
    end;
  end else
end;

function TUMedi_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('?????') then
  begin
  end else
end;

function TUMedi_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('UnidMed') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CodUsu';
    FLIndices.Add(FRIndices);
    //
  end else
end;

function TUMedi_Tabs.CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TUMedi_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMed';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUMedi_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('UnidMed') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      // ini 2020-12-05 Alterado Provisoriamente por causa do BidWon importa��o de XLS da Nayr
      FRCampos.Tipo       := 'varchar(6)'; // M�x NF-e
      //FRCampos.Tipo       := 'varchar(10)';
      // fim 2020-12-05 Alterado Provisoriamente por causa do BidWon importa��o de XLS da Nayr
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sequencia';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Grandeza';
      FRCampos.Tipo       := 'tinyint(1)'; // Pe�a, �rea, Peso, ???
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '3';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
  except
    raise;
    Result := False;
  end;
end;

function TUMedi_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // PRD-GERAL-001 :: Cadastro de Unidades de Medida
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-GERAL-001';
  FRJanelas.Nome      := 'FmUnidMed';
  FRJanelas.Descricao := 'Cadastro de Unidades de Medida';
  FRJanelas.Modulo    := 'UMED';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-GERAL-006 :: Defini��o M�ltipla de Unidade de Medida
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-GERAL-006';
  FRJanelas.Nome      := 'FmUnidMedMul';
  FRJanelas.Descricao := 'Defini��o M�ltipla de Unidade de Medida';
  FRJanelas.Modulo    := 'UMED';
  FLJanelas.Add(FRJanelas);
  //
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
