unit UnUMedi_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*)
  // ver!!! Delphi Alexandria
  //DmkCoding,
  // fim// ver!!! Delphi Alexandria
  dmkEdit,
  dmkRadioGroup, dmkMemo, dmkCheckGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu;

type
  TUMedi_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CriaEEscolheUnidMed(Codigo: Integer; EdUnidMed: TdmkEditCB;
              CBUnidMed: TdmkDBLookupComboBox; QrUnidMed: TmySQLQuery);
    function  MostraUnidMed(Codigo: Integer): Boolean;
    function  MostraFormUnidMedMul(SQL: array of String; DB: TmySQLDataBase):
              Boolean;
    procedure CadastraESelecionaUnidMed(VUUnidMed: TdmkValUsu; EdUnidMed:
              TdmkEditCB; CBUnidMed: TdmkDBLookupCombobox; QrUnidMed: TmySQLQuery);
    procedure PesquisaPorCodigo(UnidMed: Integer; EdSigla: TdmkEdit);
    procedure PesquisaPorSigla(Limpa: Boolean; EdSigla: TdmkEdit;
              EdUnidMed: TdmkEditCB; CBUnidMed: TdmkDBLookupComboBox);
  end;

var
  UMedi_PF: TUMedi_PF;

implementation

uses Module, DmkDAC_PF, MyDBCheck, UnidMed,
{$IfDef _tmp_EhLgistic}UnidMedMul, {$EndIf}
UMySQLModule;

{ TUMedi_PF }

procedure TUMedi_PF.CadastraESelecionaUnidMed(VUUnidMed: TdmkValUsu; EdUnidMed:
 TdmkEditCB; CBUnidMed: TdmkDBLookupCombobox; QrUnidMed: TmySQLQuery);
var
  Codigo: Integer;
begin
  Codigo       := VUUnidMed.ValueVariant;
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmUnidMed.LocCod(Codigo, Codigo);
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    { N�o presisa no SetaCodUsoDeCodigo
    QrUnidMed.Close;
    UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
    }
    if VAR_CADASTRO <> 0 then
    begin
(*
      UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
        'Codigo', 'CodUsu');
*)
      UMyMod.SetaCodigoPesquisado(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
        'Codigo');
      CBUnidMed.SetFocus;
    end;
  end;
end;

procedure TUMedi_PF.CriaEEscolheUnidMed(Codigo: Integer; EdUnidMed: TdmkEditCB;
  CBUnidMed: TdmkDBLookupComboBox; QrUnidMed: TmySQLQuery);
begin
  VAR_CADASTRO := 0;
  //
  MostraUnidMed(Codigo);
  //
  { N�o presisa no SetaCodUsoDeCodigo
  QrUnidMed.Close;
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  }
  if VAR_CADASTRO <> 0 then
  begin
(*
    UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
      'Codigo', 'CodUsu');
*)
      UMyMod.SetaCodigoPesquisado(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
        'Codigo');
    if CBUnidMed <> nil then
      CBUnidMed.SetFocus;
  end;
end;

function TUMedi_PF.MostraFormUnidMedMul(SQL: array of String; DB:
  TmySQLDataBase): Boolean;
begin
{$IfDef _tmp_EhLgistic}
  Result := False;
  if DBCheck.CriaFm(TFmUnidMedMul, FmUnidMedMul, afmoNegarComAviso) then
  begin
    FmUnidMedMul.ReopenErrGrandz(Geral.ATS(SQL), DB);
    FmUnidMedMul.ShowModal;
    FmUnidMedMul.Destroy;
    Result := True;
  end;
{$EndIf}
end;

function TUMedi_PF.MostraUnidMed(Codigo: Integer): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmUnidMed.LocCod(Codigo, Codigo);
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    Result := True;
  end;
end;

procedure TUMedi_PF.PesquisaPorCodigo(UnidMed: Integer; EdSigla: TdmkEdit);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Sigla ',
      'FROM unidmed ',
      'WHERE Codigo=' + Geral.FF0(UnidMed),
      '']);
    if Qry.RecordCount > 0 then
    begin
      if EdSigla.ValueVariant <> Qry.FieldByName('Sigla').AsString then
        EdSigla.ValueVariant := Qry.FieldByName('Sigla').AsString;
    end else EdSigla.ValueVariant := '';
  finally
    Qry.Free;
  end;
end;

procedure TUMedi_PF.PesquisaPorSigla(Limpa: Boolean; EdSigla: TdmkEdit;
  EdUnidMed: TdmkEditCB; CBUnidMed: TdmkDBLookupComboBox);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM unidmed ',
      'WHERE Sigla="' + EdSigla.Text + '" ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      if EdUnidMed.ValueVariant <> Qry.FieldByName('Codigo').AsInteger then
        EdUnidMed.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
      if CBUnidMed.KeyValue     <> Qry.FieldByName('Codigo').AsInteger then
        CBUnidMed.KeyValue     := Qry.FieldByName('Codigo').AsInteger;
    end
    else if Limpa then
      EdSigla.ValueVariant := '';
  finally
    Qry.Free;
  end;
end;

end.
