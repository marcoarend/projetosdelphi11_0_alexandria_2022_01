unit UnidMedMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBLookupComboBox,
  dmkEditCB, mySQLDbTables, dmkValUsu, dmkDBGridZTO;

type
  TFmUnidMedMul = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    EdUnidMed: TdmkEditCB;
    Label6: TLabel;
    CBUnidMed: TdmkDBLookupComboBox;
    SBUnidMed: TSpeedButton;
    VUUnidMed: TdmkValUsu;
    DBGErrGrandz: TdmkDBGridZTO;
    QrErrGrandz: TmySQLQuery;
    QrErrGrandzNivel1: TIntegerField;
    QrErrGrandzReduzido: TIntegerField;
    QrErrGrandzNO_GG1: TWideStringField;
    QrErrGrandzSigla: TWideStringField;
    DsErrGrandz: TDataSource;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrErrGrandzGrandeza: TFloatField;
    QrErrGrandzxco_Grandeza: TFloatField;
    QrErrGrandzdif_Grandeza: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBUnidMedClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ReopenErrGrandz(SQL: String; DB: TmySQLDataBase);
  end;

  var
  FmUnidMedMul: TFmUnidMedMul;

implementation

uses UnMyObjects, UnUMedi_PF, DmkDAC_PF, Module, UMySQLModule;

{$R *.DFM}

procedure TFmUnidMedMul.BtNenhumClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGErrGrandz), False);
end;

procedure TFmUnidMedMul.BtOKClick(Sender: TObject);
var
  UnidMed, Nivel1, N: Integer;
begin
  UnidMed := EdUnidMed.ValueVariant;
  if MyObjects.FIC(UnidMed = 0, EdUnidMed, 'Informe a unidade de medida!') then
    Exit;
  if MyObjects.FIC(DBGErrGrandz.SelectedRows.Count = 0, nil,
  'Nenhum item foi selecionado!') then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    with DBGErrGrandz.DataSource.DataSet do
    for N := 0 to DBGErrGrandz.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGErrGrandz.SelectedRows.Items[N]));
      GotoBookmark(DBGErrGrandz.SelectedRows.Items[N]);
      //
      Nivel1 := QrErrGrandzNivel1.Value;
      //
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
      'UnidMed'], ['Nivel1'], [UnidMed], [Nivel1], True);
    end;
    //
    MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGErrGrandz), False);
    ReopenErrGrandz(QrErrGrandz.SQL.Text, QrErrGrandz.Database);
  finally
    Screen.Cursor := crdefault;
  end;
end;

procedure TFmUnidMedMul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmUnidMedMul.BtTodosClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGErrGrandz), True);
end;

procedure TFmUnidMedMul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmUnidMedMul.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
end;

procedure TFmUnidMedMul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmUnidMedMul.ReopenErrGrandz(SQL: String; DB:
  TmySQLDataBase);
begin
  UnDmkDac_PF.AbreMySQLQUery0(QrErrGrandz, DB, [SQL]);
end;

procedure TFmUnidMedMul.SBUnidMedClick(Sender: TObject);
begin
  UMedi_PF.CadastraESelecionaUnidMed(VUUnidMed, EdUnidMed, CBUnidMed, QrUnidMed);
end;

end.
