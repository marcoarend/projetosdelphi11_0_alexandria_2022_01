unit GraGXPIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmGraGXPIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdCodUso: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    CBGraGXOutr: TdmkDBLookupComboBox;
    EdGraGXOutr: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGXOutr: TmySQLQuery;
    DsGraGXOutr: TDataSource;
    VU_Sel_: TdmkValUsu;
    QrGraGXOutrControle: TIntegerField;
    QrGraGXOutrReferencia: TWideStringField;
    QrGraGXOutrNO_GG1: TWideStringField;
    EdReferencia: TdmkEdit;
    QrPesq2: TmySQLQuery;
    QrPesq1: TmySQLQuery;
    QrPesq1Controle: TIntegerField;
    QrPesq2Referencia: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdGraGXOutrChange(Sender: TObject);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdGraGXOutrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdReferenciaExit(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    //procedure Reopen_SorceSel_(Controle: Integer);
    procedure ReopenGraGXPIts(Conta: Integer);
    procedure PesquisaPorReferencia(Limpa: Boolean);
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorNome(Key: Integer);
    procedure ReopenGraGXOutr;
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmGraGXPIts: TFmGraGXPIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, Principal,
DmkDAC_PF, UnMySQLCuringa;

{$R *.DFM}

procedure TFmGraGXPIts.BtOKClick(Sender: TObject);
var
  GraGXPatr, Conta, GraGXOutr: Integer;
begin
  GraGXPatr := FQrCab.FieldByName('GraGruX').AsInteger;
  GraGXOutr := EdGraGXOutr.ValueVariant;
  if MyObjects.FIC(GraGXOutr = 0, EdGraGXOutr, 'Informe o material!') then
    Exit;
  Conta := EdConta.ValueVariant;
  Conta :=
    UMyMod.BPGS1I32('gragxpits', 'Conta', '', '', tsPos, ImgTipo.SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'gragxpits', False, [
  'GraGXPatr', 'GraGXOutr'], ['Conta'], [GraGXPatr, GraGXOutr], [Conta], True) then
  begin
    ReopenGraGXPIts(Conta);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdConta.ValueVariant     := 0;
      EdGraGXOutr.ValueVariant := 0;
      CBGraGXOutr.KeyValue     := Null;
      EdGraGXOutr.SetFocus;
    end else Close;
  end;
end;

procedure TFmGraGXPIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGXPIts.EdGraGXOutrChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    PesquisaPorGraGruX();
end;

procedure TFmGraGXPIts.EdGraGXOutrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaPorNome(Key);
end;

procedure TFmGraGXPIts.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
end;

procedure TFmGraGXPIts.EdReferenciaExit(Sender: TObject);
begin
  PesquisaPorReferencia(True);
end;

procedure TFmGraGXPIts.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaPorNome(Key);
end;

procedure TFmGraGXPIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmGraGXPIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenGraGXOutr;
end;

procedure TFmGraGXPIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGXPIts.FormShow(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := ImgTipo.SQLType = stIns; 
  CkContinuar.Checked := Enab;
  CkContinuar.Visible := Enab;
end;

procedure TFmGraGXPIts.PesquisaPorGraGruX();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxoutr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  cpl.GraGruX=' + Geral.FF0(EdGraGXOutr.ValueVariant),
  'AND NOT (cpl.GraGruX IS NULL) ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
  end else EdReferencia.ValueVariant := '';
end;

procedure TFmGraGXPIts.PesquisaPorNome(Key: Integer);
var
  //Controle
  Nivel1: Integer;
begin
  if Key = VK_F3 then
  begin
    Nivel1 := CuringaLoc.CriaForm('Nivel1', CO_NOME, 'gragru1', Dmod.MyDB, CO_VAZIO);
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM gragrux ',
    'WHERE GraGru1=' + Geral.FF0(Nivel1),
    '']);
    EdGraGXOutr.ValueVariant := Dmod.QrAux.FieldByName('Controle').AsInteger;
  end;
end;

procedure TFmGraGXPIts.PesquisaPorReferencia(Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxoutr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdGraGXOutr.ValueVariant <> QrPesq1Controle.Value then
      EdGraGXOutr.ValueVariant := QrPesq1Controle.Value;
    if CBGraGXOutr.KeyValue     <> QrPesq1Controle.Value then
      CBGraGXOutr.KeyValue     := QrPesq1Controle.Value;
  end else if Limpa then
    EdReferencia.ValueVariant := '';
end;

procedure TFmGraGXPIts.ReopenGraGXOutr;
begin
  Dmod.ReopenOpcoesTRen;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXOutr, Dmod.MyDB, [
  'SELECT ',
  'ggx.Controle, gg1.Referencia, gg1.Nome NO_GG1 ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxoutr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.PrdGrupTip=' + Geral.FF0(Dmod.QrOpcoesTRenGraCodOutr.Value),
  'AND gg1.Nivel1>0 ',
  'AND cpl.Aplicacao <> 0 ',
  '']);
  QrGraGXOutr.Open;
end;

procedure TFmGraGXPIts.ReopenGraGXPIts(Conta: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('GraGruX').AsInteger;
    FQrIts.Open;
    //
    if Conta <> 0 then
      FQrIts.Locate('Conta', Conta, []);
  end;
end;

procedure TFmGraGXPIts.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormGraGXOutr(EdGraGXOutr.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdGraGXOutr, CBGraGXOutr, QrGraGXOutr,
    VAR_CADASTRO, 'Controle');
end;

end.
