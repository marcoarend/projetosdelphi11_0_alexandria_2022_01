object FmLocCPatPri: TFmLocCPatPri
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-002 :: Adi'#231#227'o de Patrim'#244'nio '#224' Loca'#231#227'o'
  ClientHeight = 459
  ClientWidth = 1178
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 287
    Width = 1178
    Height = 32
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object LaSitA1: TLabel
      Left = 375
      Top = 7
      Width = 39
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '???'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clSilver
      Font.Height = -23
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object LaSitA2: TLabel
      Left = 374
      Top = 6
      Width = 39
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '???'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -23
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label14: TLabel
      Left = 231
      Top = 12
      Width = 141
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Situa'#231#227'o do patrim'#244'nio:'
    end
    object CkContinuar: TCheckBox
      Left = 20
      Top = 5
      Width = 144
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 59
    Width = 1178
    Height = 79
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 15
      Top = 25
      Width = 16
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label4: TLabel
      Left = 89
      Top = 25
      Width = 44
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cliente:'
      FocusControl = DBEdCodUso
    end
    object Label3: TLabel
      Left = 192
      Top = 25
      Width = 103
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome do Cliente:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 15
      Top = 44
      Width = 69
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmLocCCon.DsLocCCon
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdCodUso: TDBEdit
      Left = 89
      Top = 44
      Width = 98
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      DataField = 'Cliente'
      DataSource = FmLocCCon.DsLocCCon
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DBEdNome: TDBEdit
      Left = 192
      Top = 44
      Width = 764
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clWhite
      DataField = 'NO_CLIENTE'
      DataSource = FmLocCCon.DsLocCCon
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 138
    Width = 1178
    Height = 149
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Dados do item (Patrim'#244'nio principal): '
    TabOrder = 1
    object Label6: TLabel
      Left = 15
      Top = 20
      Width = 63
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID do item:'
    end
    object Label1: TLabel
      Left = 118
      Top = 20
      Width = 61
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Reduzido:'
    end
    object SpeedButton1: TSpeedButton
      Left = 924
      Top = 39
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label2: TLabel
      Left = 187
      Top = 20
      Width = 69
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Refer'#234'ncia:'
    end
    object Label7: TLabel
      Left = 305
      Top = 20
      Width = 65
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o:'
    end
    object Label21: TLabel
      Left = 954
      Top = 20
      Width = 180
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data / hora da loca'#231#227'o / troca:'
    end
    object EdCtrID: TdmkEdit
      Left = 15
      Top = 39
      Width = 98
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 305
      Top = 39
      Width = 616
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Controle'
      ListField = 'NO_GG1'
      ListSource = DsGraGXPatr
      TabOrder = 3
      dmkEditCB = EdGraGruX
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdGraGruX: TdmkEditCB
      Left = 118
      Top = 39
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraGruXChange
      OnKeyDown = EdGraGruXKeyDown
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdReferencia: TdmkEdit
      Left = 187
      Top = 39
      Width = 119
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdReferenciaChange
      OnExit = EdReferenciaExit
      OnKeyDown = EdReferenciaKeyDown
    end
    object GroupBox3: TGroupBox
      Left = 15
      Top = 69
      Width = 365
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Valores de aluguel: '
      Enabled = False
      TabOrder = 4
      object Panel3: TPanel
        Left = 2
        Top = 18
        Width = 361
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label11: TLabel
          Left = 271
          Top = 0
          Width = 39
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Di'#225'rio:'
          FocusControl = DBEdit4
        end
        object Label10: TLabel
          Left = 182
          Top = 0
          Width = 54
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Semanal'
          FocusControl = DBEdit3
        end
        object Label9: TLabel
          Left = 94
          Top = 0
          Width = 59
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Quinzenal'
          FocusControl = DBEdit2
        end
        object Label8: TLabel
          Left = 5
          Top = 0
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Mensal:'
          FocusControl = DBEdit1
        end
        object DBEdit3: TDBEdit
          Left = 182
          Top = 20
          Width = 84
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'ValorSem'
          DataSource = DsGraGXPatr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 271
          Top = 20
          Width = 83
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'ValorDia'
          DataSource = DsGraGXPatr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object DBEdit2: TDBEdit
          Left = 94
          Top = 20
          Width = 83
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'ValorQui'
          DataSource = DsGraGXPatr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object DBEdit1: TDBEdit
          Left = 5
          Top = 20
          Width = 84
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'ValorMes'
          DataSource = DsGraGXPatr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 384
      Top = 69
      Width = 779
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Patrim'#244'nio Secund'#225'rio: '
      Enabled = False
      TabOrder = 5
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 775
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label12: TLabel
          Left = 10
          Top = 0
          Width = 69
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Refer'#234'ncia:'
          FocusControl = DBEdit5
        end
        object Label13: TLabel
          Left = 123
          Top = 0
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdit6
        end
        object DBEdit5: TDBEdit
          Left = 10
          Top = 20
          Width = 108
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'Referencia'
          DataSource = DsAgrupado
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object DBEdit6: TDBEdit
          Left = 123
          Top = 20
          Width = 646
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'Nome'
          DataSource = DsAgrupado
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
    end
    object TPDataLoc: TdmkEditDateTimePicker
      Left = 954
      Top = 39
      Width = 138
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 41131.724786689820000000
      Time = 41131.724786689820000000
      TabOrder = 6
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdHoraLoc: TdmkEdit
      Left = 1092
      Top = 39
      Width = 71
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 7
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfLong
      HoraFormat = dmkhfLong
      Texto = '00:00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1178
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 1119
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1060
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 458
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Adi'#231#227'o de Patrim'#244'nio '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 458
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Adi'#231#227'o de Patrim'#244'nio '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 458
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Adi'#231#227'o de Patrim'#244'nio '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 319
    Width = 1178
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1174
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 373
    Width = 1178
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 999
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 18
      Width = 997
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DsGraGXPatr: TDataSource
    DataSet = QrGraGXPatr
    Left = 36
    Top = 12
  end
  object VU_Sel_: TdmkValUsu
    dmkEditCB = EdGraGruX
    QryCampo = '_Sel_'
    UpdCampo = '_Sel_'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 64
    Top = 12
  end
  object QrPesq2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  cpl.GraGruX=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 704
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  gg1.Referencia=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 676
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrGraGXPatr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL, '
      'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1, '
      
        'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXIS' +
        'TE,  '
      'cpl.Situacao, cpl.AtualValr, cpl.ValorMes, '
      'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia, '
      'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie, '
      'cpl.Voltagem, cpl.Potencia, cpl.Capacid   '
      'FROM gragrux ggx  '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle  '
      'LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle  '
      'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo  '
      'LEFT JOIN graglsitu ggs ON ggs.Codigo=cpl.Situacao'
      'WHERE  gg1.PrdGrupTip=1 AND (gg1.Nivel1=ggx.GraGru1) '
      'AND cpl.Aplicacao <> 0'
      ''
      'ORDER BY NO_GG1 '
      '')
    Left = 8
    Top = 12
    object QrGraGXPatrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXPatrReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXPatrCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
    end
    object QrGraGXPatrNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXPatrCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
      Required = True
    end
    object QrGraGXPatrSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrGraGXPatrAtualValr: TFloatField
      FieldName = 'AtualValr'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorMes: TFloatField
      FieldName = 'ValorMes'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorQui: TFloatField
      FieldName = 'ValorQui'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorSem: TFloatField
      FieldName = 'ValorSem'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorDia: TFloatField
      FieldName = 'ValorDia'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrAgrupador: TIntegerField
      FieldName = 'Agrupador'
    end
    object QrGraGXPatrMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraGXPatrModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 60
    end
    object QrGraGXPatrSerie: TWideStringField
      FieldName = 'Serie'
      Size = 60
    end
    object QrGraGXPatrVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Size = 30
    end
    object QrGraGXPatrPotencia: TWideStringField
      FieldName = 'Potencia'
      Size = 30
    end
    object QrGraGXPatrCapacid: TWideStringField
      FieldName = 'Capacid'
      Size = 30
    end
    object QrGraGXPatrTXT_MES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_MES'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_QUI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_QUI'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_SEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_SEM'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_DIA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DIA'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrNO_SIT: TWideStringField
      FieldName = 'NO_SIT'
      Size = 30
    end
    object QrGraGXPatrNO_SITAPL: TIntegerField
      FieldName = 'NO_SITAPL'
    end
  end
  object QrAgrupado: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia, gg1.Nome'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE  ggx.Controle=:P0')
    Left = 620
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAgrupadoNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrAgrupadoReferencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
  end
  object DsAgrupado: TDataSource
    DataSet = QrAgrupado
    Left = 648
    Top = 12
  end
  object QrGraGXPIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gpi.GraGXOutr, ggo.Aplicacao, '
      'ggo.ItemValr, ggo.ItemUnid'
      'FROM gragxpits gpi'
      'LEFT JOIN gragxoutr ggo ON ggo.GraGruX=gpi.GraGXOutr'
      'WHERE  gpi.GraGXPatr=:P0'
      'AND ggo.Aplicacao <> 0')
    Left = 592
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGXPItsGraGXOutr: TIntegerField
      FieldName = 'GraGXOutr'
      Origin = 'gragxpits.GraGXOutr'
    end
    object QrGraGXPItsAplicacao: TIntegerField
      FieldName = 'Aplicacao'
      Origin = 'gragxoutr.Aplicacao'
    end
    object QrGraGXPItsItemValr: TFloatField
      FieldName = 'ItemValr'
      Origin = 'gragxoutr.ItemValr'
    end
    object QrGraGXPItsItemUnid: TIntegerField
      FieldName = 'ItemUnid'
    end
  end
end
