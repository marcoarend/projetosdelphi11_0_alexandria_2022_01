unit FixServCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TFmFixServCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrFixServCad: TmySQLQuery;
    QrFixServCadCodigo: TIntegerField;
    QrFixServCadNome: TWideStringField;
    DsFixServCad: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    dmkEdit1: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    QrFixServCadPreco: TFloatField;
    DBEdit1: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFixServCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFixServCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmFixServCad: TFmFixServCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFixServCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFixServCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFixServCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFixServCad.DefParams;
begin
  VAR_GOTOTABELA := 'fixservcad';
  VAR_GOTOMYSQLTABLE := QrFixServCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM fixservcad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmFixServCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFixServCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFixServCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFixServCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFixServCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFixServCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFixServCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFixServCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFixServCad.BtAlteraClick(Sender: TObject);
begin
  if (QrFixServCad.State = dsInactive) or (QrFixServCad.RecordCount = 0) then
    Exit;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFixServCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'fixservcad');
end;

procedure TFmFixServCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFixServCadCodigo.Value;
  Close;
end;

procedure TFmFixServCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('fixservcad', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrFixServCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'fixservcad', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmFixServCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'fixservcad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFixServCad.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrFixServCad.State = dsInactive) or (QrFixServCad.RecordCount = 0) then
    Exit;
  //
  Codigo := QrFixServCadCodigo.Value;
  //Verifica Manuten��es
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM fixgeresrv ',
  'WHERE CodItem=' + Geral.FF0(Codigo),
  '']);
  if Dmod.QrAux.RecordCount = 0 then
  begin
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do registro atual?',
    'fixservcad', 'Codigo', Codigo, Dmod.MyDB) = ID_YES then
    begin
      Va(vpLast);
    end;
  end else
    Geral.MB_Info('Este item n�o pode ser exclu�do!' + #13#10 +
      'Motivo: Ele foi utilizado no conserto ID n�mero ' +
      Geral.FF0(Dmod.QrAux.FieldByName('Codigo').AsInteger));
end;

procedure TFmFixServCad.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFixServCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'fixservcad');
end;

procedure TFmFixServCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmFixServCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFixServCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFixServCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Info('Impress�o n�o implementada nesta janela!');
end;

procedure TFmFixServCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFixServCad.SbNovoClick(Sender: TObject);
begin
  //N�o tem CodUsu - LaRegistro.Caption := GOTOy.CodUsu(QrFixServCadCodigo.Value, LaRegistro.Caption);
  LaRegistro.Caption := GOTOy.Codigo(QrFixServCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFixServCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFixServCad.QrFixServCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFixServCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFixServCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFixServCadCodigo.Value,
    CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'fixservcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFixServCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFixServCad.QrFixServCadBeforeOpen(DataSet: TDataSet);
begin
  QrFixServCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

