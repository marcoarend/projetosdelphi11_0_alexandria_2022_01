unit LocCMateri;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkGeral, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmLocCMateri = class(TForm)
    PainelDados: TPanel;
    DsGraGXOutr: TDataSource;
    QrGraGXOutr: TmySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    SBCadastro: TSpeedButton;
    CBGraGruX: TdmkDBLookupComboBox;
    Label7: TLabel;
    Label2: TLabel;
    EdReferencia: TdmkEdit;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    QrPesq2: TmySQLQuery;
    QrPesq2Referencia: TWideStringField;
    QrPesq1: TmySQLQuery;
    QrPesq1Controle: TIntegerField;
    QrGraGXOutrControle: TIntegerField;
    QrGraGXOutrReferencia: TWideStringField;
    QrGraGXOutrNO_GG1: TWideStringField;
    QrGraGXOutrGraGruX: TIntegerField;
    QrGraGXOutrItemValr: TFloatField;
    QrGraGXOutrItemUnid: TIntegerField;
    CkContinuar: TCheckBox;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure SBCadastroClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdReferenciaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGXOutr();
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorReferencia(Limpa: Boolean);
  public
    { Public declarations }
    FQrLocCPatCns, FQrLocCPatAce, FQrLocCPatUso: TmySQLQuery;
    FTipo, FCodigo, FCtrID: Integer;
  end;

var
  FmLocCMateri: TFmLocCMateri;

implementation

uses UnMyObjects, Module, Principal, LocCCon;

{$R *.DFM}

procedure TFmLocCMateri.BtOKClick(Sender: TObject);
  procedure IncluiAce(Codigo, CtrID: Integer);
  var
    Item, GraGruX: Integer;
    ValBem: Double;
  begin
    GraGruX := QrGraGXOutrGraGruX.Value;
    ValBem  := QrGraGXOutrItemValr.Value;
    //
    Item := UMyMod.BPGS1I32('loccpatace', 'Item', '', '', tsPos, stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatace', False, [
      'Codigo', 'CtrID', 'GraGruX', 'ValBem'], ['Item'], [
      Codigo, CtrID, GraGruX, ValBem], [Item], True)
    then
      FmLocCCon.ReopenLocCPatAce(FQrLocCPatAce, CtrID, Item);
  end;
  procedure IncluiCns(Codigo, CtrID: Integer);
  var
    Item, GraGruX, Unidade: Integer;
    QtdIni, QtdFim, PrcUni, ValUso: Double;
  begin
    GraGruX        := QrGraGXOutrGraGruX.Value;
    Unidade        := QrGraGXOutrItemUnid.Value;
    QtdIni         := 0;
    QtdFim         := 0;
    PrcUni         := QrGraGXOutrItemValr.Value;;
    ValUso         := 0;
    //
    Item := UMyMod.BPGS1I32('loccpatcns', 'Item', '', '', tsPos, stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatcns', False, [
      'Codigo', 'CtrID', 'GraGruX',
      'Unidade', 'QtdIni', 'QtdFim',
      'PrcUni', 'ValUso'], [
      'Item'], [
      Codigo, CtrID, GraGruX,
      Unidade, QtdIni, QtdFim,
      PrcUni, ValUso], [
      Item], True)
    then
      FmLocCCon.ReopenLocCPatCns(FQrLocCPatCns, FCtrID, Item);
  end;
  procedure IncluiUso(Codigo, CtrID: Integer);
  var
    Item, GraGruX, Unidade, AvalIni: Integer;
    AvalFim, PrcUni, ValTot: Double;
  begin
    GraGruX        := QrGraGXOutrGraGruX.Value;
    Unidade        := QrGraGXOutrItemUnid.Value;
    AvalIni        := 0;
    AvalFim        := 0;
    PrcUni         := QrGraGXOutrItemValr.Value;
    ValTot         := 0;
    //
    Item := UMyMod.BPGS1I32('loccpatuso', 'Item', '', '', tsPos, stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatuso', False, [
      'Codigo', 'CtrID', 'GraGruX',
      'Unidade', 'AvalIni', 'AvalFim',
      'PrcUni', 'ValTot'], [
      'Item'], [
      Codigo, CtrID, GraGruX,
      Unidade, AvalIni, AvalFim,
      PrcUni, ValTot], [
      Item], True)
    then
      FmLocCCon.ReopenLocCPatUso(FQrLocCPatUso, CtrID, Item);
  end;
var
  Codigo, CtrID: Integer;
begin
  Codigo := FCodigo;
  CtrID  := FCtrID;
  //
  case FTipo of
    1: IncluiAce(Codigo, CtrID); //1 = Acessório
    2: IncluiUso(Codigo, CtrID); //2 = Uso
    3: IncluiCns(Codigo, CtrID); //3 = Uso e consumo  
  end;
  if CkContinuar.Checked then
    EdGraGruX.SetFocus
  else
    Close;
end;

procedure TFmLocCMateri.BtSaidaClick(Sender: TObject);
begin
  VAR_SELCOD := 0;
  Close;
end;

procedure TFmLocCMateri.EdGraGruXChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    PesquisaPorGraGruX();
end;

procedure TFmLocCMateri.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
end;

procedure TFmLocCMateri.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCMateri.FormShow(Sender: TObject);
begin
  ReopenGraGXOutr;
end;

procedure TFmLocCMateri.PesquisaPorGraGruX;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxoutr ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  ggo.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (ggo.GraGruX IS NULL) ',
  'AND ggo.Aplicacao <> 0 ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
    begin
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
    end;
  end else
    EdReferencia.ValueVariant := '';
end;

procedure TFmLocCMateri.PesquisaPorReferencia(Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxoutr ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
  'AND NOT (ggo.GraGruX IS NULL)',
  'AND ggo.Aplicacao <> 0 ', 
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
    end;
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
    EdReferencia.ValueVariant := '';
end;

procedure TFmLocCMateri.ReopenGraGXOutr;
begin
  QrGraGXOutr.Close;
  QrGraGXOutr.Params[0].AsInteger := FTipo;
  UMyMod.AbreQuery(QrGraGXOutr, Dmod.MyDB);
end;

procedure TFmLocCMateri.SBCadastroClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormGraGXOutr(EdGraGruX.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrGraGXOutr, VAR_CADASTRO, 'Controle');
  PesquisaPorGraGruX();
  EdReferencia.SetFocus;
end;

procedure TFmLocCMateri.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCMateri.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

end.
