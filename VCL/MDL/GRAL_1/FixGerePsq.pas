unit FixGerePsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEdit, dmkEditCB, dmkDBLookupComboBox,
  mySQLDbTables, dmkRadioGroup, Variants, UnDmkEnums;

type
  TFmFixGerePsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtLocaliza: TBitBtn;
    LaTitulo1C: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    Label2: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    EdCliente: TdmkEditCB;
    Label3: TLabel;
    CBCliente: TdmkDBLookupComboBox;
    RGTabela: TdmkRadioGroup;
    QrGraGruX: TmySQLQuery;
    QrGraGruXCNTRL_GGX: TIntegerField;
    QrGraGruXNO_EQUI: TWideStringField;
    DsGraGruX: TDataSource;
    LaEquipamento: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdDefeitoTxt: TdmkEdit;
    QrPsq: TmySQLQuery;
    DBGrid1: TDBGrid;
    DsPsq: TDataSource;
    QrPsqNO_CLI: TWideStringField;
    QrPsqCodigo: TIntegerField;
    QrPsqEmpresa: TIntegerField;
    QrPsqCliente: TIntegerField;
    QrPsqObservacao: TWideStringField;
    QrPsqCustoTota: TFloatField;
    QrPsqCustoServ: TFloatField;
    QrPsqCustoPeca: TFloatField;
    BtPesquisa: TBitBtn;
    EdReferencia: TdmkEdit;
    QrPesq1: TmySQLQuery;
    QrPesq1Controle: TIntegerField;
    QrPesq2: TmySQLQuery;
    QrPesq2Referencia: TWideStringField;
    Label1: TLabel;
    EdObserv: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGTabelaClick(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrPsqBeforeClose(DataSet: TDataSet);
    procedure QrPsqAfterOpen(DataSet: TDataSet);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdDefeitoTxtChange(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdReferenciaExit(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure FechaPesquisa();
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorNome(Key: Integer);
    procedure PesquisaPorReferencia(Limpa: Boolean);
    procedure ConfiguraEquipamentos(Tipo: Integer);
  public
    { Public declarations }
    FSelecionado: Integer;
  end;

  var
  FmFixGerePsq: TFmFixGerePsq;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UnMySQLCuringa;

{$R *.DFM}

procedure TFmFixGerePsq.BtLocalizaClick(Sender: TObject);
begin
  FSelecionado := QrPsqCodigo.Value;
  Close;
end;

procedure TFmFixGerePsq.BtPesquisaClick(Sender: TObject);
var
  Empresa, Cliente, GraGruX: Integer;
  Aviso, DT, DefeitoTxt, Observ, SQLObs: String;
  EQ: Boolean;
begin
  Empresa := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa!') then
    Exit;
  Cliente := EdCliente.ValueVariant;
  GraGruX := EdGraGruX.ValueVariant;
  Observ  := EdObserv.ValueVariant;
  DT := EdDefeitoTxt.Text;
  DefeitoTxt := '%' + DT + '%';
  DT := Trim(DT);
  EQ := (GraGruX <> 0) or (DT <> '') ;
  //
  if Observ <> '' then
    SQLObs := 'AND fgc.Observacao LIKE "%' + Observ + '%"'
  else
    SQLObs := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
    'SELECT ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, ',
    'fgc.* ',
    'FROM fixgerecab fgc ',
    'LEFT JOIN entidades emp ON emp.Codigo=fgc.Empresa ',
    'LEFT JOIN entidades cli ON cli.Codigo=fgc.Cliente ',
    Geral.ATS_If(EQ, ['LEFT JOIN fixgereequ fge ON fge.Codigo=fgc.Codigo']),
    'WHERE fgc.Empresa=' + Geral.FF0(Empresa),
    Geral.ATS_If(Cliente <> 0, ['AND fgc.Cliente=' + Geral.FF0(Cliente)]),
    Geral.ATS_If(GraGruX <> 0, ['AND fge.GraGruX=' + Geral.FF0(GraGruX)]),
    Geral.ATS_If(DT <> '', ['AND fge.DefeitoTxt LIKE "' + DefeitoTxt + '"']),
    SQLObs,
    ' ']);
  if QrPsq.RecordCount > 0 then
    Aviso := Geral.FF0(QrPsq.RecordCount) + ' itens foram localizados!'
  else
    Aviso := 'Nenhum item foi localizado!';
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Aviso);
end;

procedure TFmFixGerePsq.BtSaidaClick(Sender: TObject);
begin
  FSelecionado := 0;
  Close;
end;

procedure TFmFixGerePsq.ConfiguraEquipamentos(Tipo: Integer);
begin
  EdGraGruX.ValueVariant    := 0;
  EdReferencia.ValueVariant := '';
  CBGraGruX.KeyValue        := Null;
  //
  case RGTabela.ItemIndex of
      1:
      begin
        LaEquipamento.Visible := True;
        EdGraGruX.Visible     := True;
        EdReferencia.Visible  := True;
        CBGraGruX.Visible     := True;
        //
        EdReferencia.Left := EdGraGruX.Left + EdGraGruX.Width + 1;
        CBGraGruX.Left    := EdGraGruX.Left + EdGraGruX.Width + EdReferencia.Width + 2;
      end;
      2:
      begin
        LaEquipamento.Visible := True;
        EdGraGruX.Visible     := True;
        EdReferencia.Visible  := False;
        CBGraGruX.Visible     := True;
        //
        CBGraGruX.Left := EdGraGruX.Left + EdGraGruX.Width + 1;
      end;
    else
      begin
        LaEquipamento.Visible := False;
        EdGraGruX.Visible     := False;
        EdReferencia.Visible  := False;
        CBGraGruX.Visible     := False;
      end;
  end;
end;

procedure TFmFixGerePsq.DBGrid1DblClick(Sender: TObject);
begin
  if (QrPsq.State <> dsInactive) and (QrPsq.RecordCount > 0) then
    BtLocalizaClick(Self);
end;

procedure TFmFixGerePsq.EdClienteChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmFixGerePsq.EdDefeitoTxtChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmFixGerePsq.EdEmpresaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmFixGerePsq.EdGraGruXChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    PesquisaPorGraGruX();
  FechaPesquisa();
end;

procedure TFmFixGerePsq.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaPorNome(Key);
end;

procedure TFmFixGerePsq.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
end;

procedure TFmFixGerePsq.EdReferenciaExit(Sender: TObject);
begin
  PesquisaPorReferencia(True);
end;

procedure TFmFixGerePsq.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaPorNome(Key);
end;

procedure TFmFixGerePsq.FechaPesquisa();
begin
  QrPsq.Close;
end;

procedure TFmFixGerePsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFixGerePsq.FormCreate(Sender: TObject);
begin
  FSelecionado    := 0;
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  //
  CBEmpresa.ListSource   := DmodG.DsEmpresas;
  EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
  //
  ConfiguraEquipamentos(0);
end;

procedure TFmFixGerePsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFixGerePsq.PesquisaPorGraGruX;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
    'SELECT gg1.Referencia ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
    'WHERE  cpl.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
    'AND NOT (cpl.GraGruX IS NULL) ',
    '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
    begin
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
    end;
  end else
    EdReferencia.ValueVariant := '';
end;

procedure TFmFixGerePsq.PesquisaPorNome(Key: Integer);
var
  //Controle
  Nivel1: Integer;
begin
  if Key = VK_F3 then
  begin
    Nivel1 := CuringaLoc.CriaForm('Nivel1', CO_NOME, 'gragru1', Dmod.MyDB, CO_VAZIO);
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM gragrux ',
      'WHERE GraGru1=' + Geral.FF0(Nivel1),
      '']);
    EdGraGruX.ValueVariant := Dmod.QrAux.FieldByName('Controle').AsInteger;
  end;
end;

procedure TFmFixGerePsq.PesquisaPorReferencia(Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
    'SELECT ggx.Controle ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
    'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
    'AND NOT (cpl.GraGruX IS NULL)',
    '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
    end;
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
    EdReferencia.ValueVariant := '';
end;

procedure TFmFixGerePsq.QrPsqAfterOpen(DataSet: TDataSet);
begin
  BtLocaliza.Enabled := QrPsq.RecordCount > 0;
end;

procedure TFmFixGerePsq.QrPsqBeforeClose(DataSet: TDataSet);
begin
  BtLocaliza.Enabled := False;
end;

procedure TFmFixGerePsq.RGTabelaClick(Sender: TObject);
begin
  ConfiguraEquipamentos(RGTabela.ItemIndex);
  //
  FechaPesquisa();
  Dmod.ReopenFixEqui(RGTabela.ItemIndex, QrGraGruX);
end;

end.
