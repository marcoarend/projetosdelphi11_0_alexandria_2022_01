unit FixGereCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, dmkEditCB, dmkDBLookupComboBox, dmkMemo, UnDmkEnums,
  dmkCompoStore, Vcl.ComCtrls;

type
  TFmFixGereCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    QrFixGereCab: TmySQLQuery;
    DsFixGereCab: TDataSource;
    QrFixGereEqu: TmySQLQuery;
    DsFixGereEqu: TDataSource;
    PMEqu: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtEqu: TBitBtn;
    DGDados: TDBGrid;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label4: TLabel;
    MeObservacao: TdmkMemo;
    QrFixGereCabNO_EMP: TWideStringField;
    QrFixGereCabNO_CLI: TWideStringField;
    QrFixGereCabCodigo: TIntegerField;
    QrFixGereCabEmpresa: TIntegerField;
    QrFixGereCabCliente: TIntegerField;
    QrFixGereCabObservacao: TWideStringField;
    QrFixGereEquNO_RECE: TWideStringField;
    QrFixGereEquNO_RESP: TWideStringField;
    QrFixGereEquNO_LIBE: TWideStringField;
    QrFixGereEquNO_EQUI: TWideStringField;
    QrFixGereEquCodigo: TIntegerField;
    QrFixGereEquControle: TIntegerField;
    QrFixGereEquTabela: TSmallintField;
    QrFixGereEquGraGruX: TIntegerField;
    QrFixGereEquQuemRece: TIntegerField;
    QrFixGereEquQuemResp: TIntegerField;
    QrFixGereEquQuemLibe: TIntegerField;
    QrFixGereEquDtHrRece: TDateTimeField;
    QrFixGereEquDtHrLibe: TDateTimeField;
    QrFixGereEquCustoPeca: TFloatField;
    QrFixGereEquCustoServ: TFloatField;
    QrFixGereEquCustoTota: TFloatField;
    QrFixGereEquDefeitoTxt: TWideStringField;
    BtSrv: TBitBtn;
    BtPca: TBitBtn;
    GBDados: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrFixGereEquSIGLA_TAB: TWideStringField;
    QrFixGereEquTXT_DtHrLibe: TWideStringField;
    PMPca: TPopupMenu;
    ItsInclui2: TMenuItem;
    ItsAltera2: TMenuItem;
    ItsExclui2: TMenuItem;
    QrFixGerePca: TmySQLQuery;
    DsFixGerePca: TDataSource;
    QrFixGerePcaNO_ITEM: TWideStringField;
    QrFixGerePcaCodigo: TIntegerField;
    QrFixGerePcaControle: TIntegerField;
    QrFixGerePcaConta: TIntegerField;
    QrFixGerePcaCodItem: TIntegerField;
    QrFixGerePcaQtde: TFloatField;
    QrFixGerePcaPreco: TFloatField;
    QrFixGerePcaDesco: TFloatField;
    QrFixGerePcaCusto: TFloatField;
    QrFixGerePcaValor: TFloatField;
    QrFixGerePcaDescri: TWideStringField;
    GBPecServ: TGroupBox;
    DBGrid1: TDBGrid;
    QrFixGereSrv: TmySQLQuery;
    DsFixGereSrv: TDataSource;
    DBGrid2: TDBGrid;
    QrFixGereCabCustoPeca: TFloatField;
    QrFixGereCabCustoServ: TFloatField;
    QrFixGereCabCustoTota: TFloatField;
    Label1: TLabel;
    Label2: TLabel;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    QrFixGereSrvNO_ITEM: TWideStringField;
    QrFixGereSrvCodigo: TIntegerField;
    QrFixGereSrvControle: TIntegerField;
    QrFixGereSrvConta: TIntegerField;
    QrFixGereSrvCodItem: TIntegerField;
    QrFixGereSrvQtde: TFloatField;
    QrFixGereSrvPreco: TFloatField;
    QrFixGereSrvDesco: TFloatField;
    QrFixGereSrvCusto: TFloatField;
    QrFixGereSrvValor: TFloatField;
    QrFixGereSrvDescri: TWideStringField;
    PMSrv: TPopupMenu;
    ItsInclui3: TMenuItem;
    ItsAltera3: TMenuItem;
    ItsExclui3: TMenuItem;
    SpeedButton5: TSpeedButton;
    QrFixGereEquTABELA_TXT: TWideStringField;
    CSTabSheetChamou: TdmkCompoStore;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFixGereCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFixGereCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrFixGereCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtEquClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMEquPopup(Sender: TObject);
    procedure QrFixGereCabBeforeClose(DataSet: TDataSet);
    procedure QrFixGereEquCalcFields(DataSet: TDataSet);
    procedure ItsInclui2Click(Sender: TObject);
    procedure ItsAltera2Click(Sender: TObject);
    procedure ItsExclui2Click(Sender: TObject);
    procedure QrFixGereEquBeforeClose(DataSet: TDataSet);
    procedure QrFixGereEquAfterScroll(DataSet: TDataSet);
    procedure PMPcaPopup(Sender: TObject);
    procedure BtPcaClick(Sender: TObject);
    procedure ItsInclui3Click(Sender: TObject);
    procedure ItsAltera3Click(Sender: TObject);
    procedure ItsExclui3Click(Sender: TObject);
    procedure BtSrvClick(Sender: TObject);
    procedure PMSrvPopup(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFixGereEqu(SQLType: TSQLType);
    procedure MostraFixGerePca(SQLType: TSQLType);
    procedure MostraFixGereSrv(SQLType: TSQLType);
    procedure ReopenFixGereEqu(Controle: Integer);
    procedure ReopenFixGerePca(Conta: Integer);
    procedure ReopenFixGereSrv(Conta: Integer);
    procedure PesquisaComplexa();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure CalculaCustosConsertoEQ(Controle: Integer);

  end;

var
  FmFixGereCab: TFmFixGereCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral,
  FixGereEqu, FixGerePca, FixGereSrv, FixGerePsq, MyGlyfs, Principal;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFixGereCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFixGereCab.MostraFixGereEqu(SQLType: TSQLType);
var
  Tabela: Integer;
begin
  if DBCheck.CriaFm(TFmFixGereEqu, FmFixGereEqu, afmoNegarComAviso) then
  begin
    FmFixGereEqu.ImgTipo.SQLType := SQLType;
    FmFixGereEqu.FQrCab := QrFixGereCab;
    FmFixGereEqu.FDsCab := DsFixGereCab;
    FmFixGereEqu.FQrIts := QrFixGereEqu;
    if SQLType = stIns then
    begin
      Tabela := MyObjects.SelRadioGroup('Inclus�o de Equipamento',
      'Origem do equipamento:', [
        '???',
        'Patrim�nio pr�prio',
        'Equipamento de terceiro'], 1, 0);
      if Tabela < 1 then
      begin
        FmFixGereEqu.Close;
        Geral.MB_Aviso('Nenhuma origem de equipamento foi informada!');
        Screen.Cursor := crDefault;
        Exit;
      end;
      // Deve ser antes!
      FmFixGereEqu.RGTabela.ItemIndex      := Tabela;
      FmFixGereEqu.TPDtHrRece.Date         := Now();
      FmFixGereEqu.EdDtHrRece.ValueVariant := Now();
      //
      if (VAR_USUARIO > 0) or (VAR_USUARIO = -2) then //-2 Boss
      begin
        FmFixGereEqu.EdQuemRece.ValueVariant := VAR_USUARIO;
        FmFixGereEqu.CBQuemRece.KeyValue     := VAR_USUARIO;
        FmFixGereEqu.EdQuemResp.ValueVariant := VAR_USUARIO;
        FmFixGereEqu.CBQuemResp.KeyValue     := VAR_USUARIO;
      end;
      //
      FmFixGereEqu.CkContinuar.Visible := True;
      FmFixGereEqu.CkContinuar.Checked := False;
    end else
    begin
      FmFixGereEqu.EdControle.ValueVariant := QrFixGereEquControle.Value;
      // Deve ser antes!
      FmFixGereEqu.RGTabela.ItemIndex      := QrFixGereEquTabela.Value;
      //
      FmFixGereEqu.EdGraGruX.ValueVariant  := QrFixGereEquGraGruX.Value;
      FmFixGereEqu.CBGraGruX.KeyValue      := QrFixGereEquGraGruX.Value;
      FmFixGereEqu.EdQuemRece.ValueVariant := QrFixGereEquQuemRece.Value;
      FmFixGereEqu.CBQuemRece.KeyValue     := QrFixGereEquQuemRece.Value;
      FmFixGereEqu.TPDtHrRece.Date         := QrFixGereEquDtHrRece.Value;
      FmFixGereEqu.EdDtHrRece.ValueVariant := QrFixGereEquDtHrRece.Value;
      FmFixGereEqu.EdQuemResp.ValueVariant := QrFixGereEquQuemResp.Value;
      FmFixGereEqu.CBQuemResp.KeyValue     := QrFixGereEquQuemResp.Value;
      FmFixGereEqu.EdDefeitoTxt.Text       := QrFixGereEquDefeitoTxt.Value;
      //
      FmFixGereEqu.EdQuemLibe.ValueVariant := QrFixGereEquQuemLibe.Value;
      FmFixGereEqu.CBQuemLibe.KeyValue     := QrFixGereEquQuemLibe.Value;
      FmFixGereEqu.TPDtHrLibe.Date         := QrFixGereEquDtHrLibe.Value;
      FmFixGereEqu.EdDtHrLibe.ValueVariant := QrFixGereEquDtHrLibe.Value;
      //
      FmFixGereEqu.CkContinuar.Visible     := False;
      FmFixGereEqu.CkContinuar.Checked     := False;
    end;
    FmFixGereEqu.ShowModal;
    FmFixGereEqu.Destroy;
  end;
end;

procedure TFmFixGereCab.MostraFixGerePca(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFixGerePca, FmFixGerePca, afmoNegarComAviso) then
  begin
    FmFixGerePca.ImgTipo.SQLType := SQLType;
    FmFixGerePca.FQrCab := QrFixGereCab;
    FmFixGerePca.FDsCab := DsFixGereCab;
    FmFixGerePca.FDsEqu := DsFixGereEqu;
    FmFixGerePca.FQrIts := QrFixGerePca;
    if SQLType = stIns then
    begin
      FmFixGerePca.EdQtde.ValueVariant := 1;
      FmFixGerePca.CkContinuar.Visible := True;
      FmFixGerePca.CkContinuar.Checked := True;
    end else
    begin
      FmFixGerePca.EdConta.ValueVariant := QrFixGerePcaConta.Value;
      //
      FmFixGerePca.EdCodItem.ValueVariant  := QrFixGerePcaCodItem.Value;
      FmFixGerePca.CBCodItem.KeyValue      := QrFixGerePcaCodItem.Value;
      FmFixGerePca.EdDescri.Text           := QrFixGerePcaDescri.Value;
      //
      FmFixGerePca.EdQtde.ValueVariant     := QrFixGerePcaQtde.Value;
      FmFixGerePca.EdPreco.ValueVariant    := QrFixGerePcaPreco.Value;
      FmFixGerePca.EdDesco.ValueVariant    := QrFixGerePcaDesco.Value;
      FmFixGerePca.EdCusto.ValueVariant    := QrFixGerePcaCusto.Value;
      FmFixGerePca.EdValor.ValueVariant    := QrFixGerePcaValor.Value;
      //
      FmFixGerePca.CkContinuar.Visible := False;
      FmFixGerePca.CkContinuar.Checked := False;
    end;
    FmFixGerePca.ShowModal;
    FmFixGerePca.Destroy;
    //
    LocCod(QrFixGereCabCodigo.Value, QrFixGereCabCodigo.Value);
    if VAR_CADASTRO <> 0 then
      QrFixGereEqu.Locate('Controle', VAR_CADASTRO, []);
  end;
end;

procedure TFmFixGereCab.MostraFixGereSrv(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFixGereSrv, FmFixGereSrv, afmoNegarComAviso) then
  begin
    FmFixGereSrv.ImgTipo.SQLType := SQLType;
    FmFixGereSrv.FQrCab := QrFixGereCab;
    FmFixGereSrv.FDsCab := DsFixGereCab;
    FmFixGereSrv.FDsEqu := DsFixGereEqu;
    FmFixGereSrv.FQrIts := QrFixGereSrv;
    if SQLType = stIns then
    begin
      FmFixGereSrv.EdQtde.ValueVariant := 1;
      FmFixGereSrv.CkContinuar.Checked := False;
      FmFixGereSrv.CkContinuar.Visible := True;
    end else
    begin
      FmFixGereSrv.EdConta.ValueVariant   := QrFixGereSrvConta.Value;
      //
      FmFixGereSrv.EdCodItem.ValueVariant := QrFixGereSrvCodItem.Value;
      FmFixGereSrv.CBCodItem.KeyValue     := QrFixGereSrvCodItem.Value;
      FmFixGereSrv.EdDescri.Text          := QrFixGereSrvDescri.Value;
      //
      FmFixGereSrv.EdQtde.ValueVariant    := QrFixGereSrvQtde.Value;
      FmFixGereSrv.EdPreco.ValueVariant   := QrFixGereSrvPreco.Value;
      FmFixGereSrv.EdDesco.ValueVariant   := QrFixGereSrvDesco.Value;
      FmFixGereSrv.EdCusto.ValueVariant   := QrFixGereSrvCusto.Value;
      FmFixGereSrv.EdValor.ValueVariant   := QrFixGereSrvValor.Value;
      //
      FmFixGereSrv.CkContinuar.Checked    := False;
      FmFixGereSrv.CkContinuar.Visible    := False;
    end;
    FmFixGereSrv.ShowModal;
    FmFixGereSrv.Destroy;
    //
    LocCod(QrFixGereCabCodigo.Value, QrFixGereCabCodigo.Value);
    if VAR_CADASTRO <> 0 then
      QrFixGereEqu.Locate('Controle', VAR_CADASTRO, []);
  end;
end;

procedure TFmFixGereCab.PesquisaComplexa();
var
  Selecionado: Integer;
begin
  if DBCheck.CriaFm(TFmFixGerePsq, FmFixGerePsq, afmoNegarComAviso) then
  begin
    FmFixGerePsq.ShowModal;
    Selecionado := FmFixGerePsq.FSelecionado;
    FmFixGerePsq.Destroy;
    if Selecionado <> 0 then
      LocCod(Selecionado, Selecionado);
  end;
end;

procedure TFmFixGereCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrFixGereCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrFixGereCab, QrFixGereEqu);
end;

procedure TFmFixGereCab.PMEquPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrFixGereCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrFixGereEqu);
  MyObjects.HabilitaMenuItemCabDelC1I2(ItsExclui1, QrFixGereEqu, QrFixGerePca, QrFixGereSrv);
end;

procedure TFmFixGereCab.PMPcaPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui2, QrFixGereEqu);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera2, QrFixGerePca);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui2, QrFixGerePca);
end;

procedure TFmFixGereCab.PMSrvPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui3, QrFixGereEqu);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera3, QrFixGereSrv);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui3, QrFixGereSrv);
end;

procedure TFmFixGereCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFixGereCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFixGereCab.DefParams;
begin
  VAR_GOTOTABELA := 'fixgerecab';
  VAR_GOTOMYSQLTABLE := QrFixGereCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI,');
  VAR_SQLx.Add('fgc.*');
  VAR_SQLx.Add('FROM fixgerecab fgc');
  VAR_SQLx.Add('LEFT JOIN enticliint eci ON eci.CodCliInt=fgc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=eci.CodEnti');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=fgc.Cliente');
  VAR_SQLx.Add('WHERE fgc.Codigo <> 0');
  //
  VAR_SQL1.Add('AND fgc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND fgc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND fgc.Nome Like :P0');
  //
end;

procedure TFmFixGereCab.ItsAltera2Click(Sender: TObject);
begin
  MostraFixGerePca(stUpd);
end;

procedure TFmFixGereCab.ItsAltera3Click(Sender: TObject);
begin
  MostraFixGereSrv(stUpd);
end;

procedure TFmFixGereCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFixGereEqu(stUpd);
end;

procedure TFmFixGereCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek:' + #13#10 +
  Caption + #13#10 + TMenuItem(Sender).Name, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmFixGereCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFixGereCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFixGereCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do equipqmento selecionado?',
  'FixGereEqu', 'Controle', QrFixGereEquControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFixGereEqu,
      QrFixGereEquControle, QrFixGereEquControle.Value);
    ReopenFixGereEqu(Controle);
  end;
end;

procedure TFmFixGereCab.ItsExclui2Click(Sender: TObject);
var
  Codigo, Controle, Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a retirada da pe�a selecionada?',
  'FixGerePca', 'Conta', QrFixGerePcaConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo   := QrFixGerePcaCodigo.Value;
    Controle := QrFixGerePcaControle.Value;
    Conta    := GOTOy.LocalizaPriorNextIntQr(QrFixGerePca,
                QrFixGerePcaConta, QrFixGerePcaConta.Value);
    //
    CalculaCustosConsertoEQ(Controle);
    LocCod(Codigo, Codigo);
    QrFixGereEqu.Locate('Controle', Controle, []);
    ReopenFixGerePca(Conta);
  end;
end;

procedure TFmFixGereCab.ItsExclui3Click(Sender: TObject);
var
  Codigo, Controle, Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a retirada do servi�o selecionado?',
  'FixGereSrv', 'Conta', QrFixGereSrvConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo   := QrFixGereSrvCodigo.Value;
    Controle := QrFixGereSrvControle.Value;
    Conta    := GOTOy.LocalizaPriorNextIntQr(QrFixGereSrv,
                QrFixGereSrvConta, QrFixGereSrvConta.Value);
    //
    CalculaCustosConsertoEQ(Controle);
    LocCod(Codigo, Codigo);
    QrFixGereEqu.Locate('Controle', Controle, []);
    ReopenFixGereSrv(Conta);
  end;
end;

procedure TFmFixGereCab.ReopenFixGereEqu(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFixGereEqu, Dmod.MyDB, [
  'SELECT ELT(Tabela, "E", "T", "?") SIGLA_TAB, ',
  'qrc.Login NO_RECE,',
  'qrs.Login NO_RESP,',
  'qlb.Login NO_LIBE,',
  '',
  'if(fge.Tabela=1, gg1.Nome, gxp.DescrTerc) NO_EQUI,',
  '',
  'CASE fge.Tabela ',
  'WHEN 1 THEN "Patrim�nio pr�prio" ',
  'WHEN 2 THEN "Equipamento de terceiro" ',
  'ELSE "???" END TABELA_TXT, ',
  'fge.*',
  '',
  'FROM fixgereequ fge',
  'LEFT JOIN senhas qrc ON qrc.Numero=fge.QuemRece',
  'LEFT JOIN senhas qrs ON qrs.Numero=fge.QuemResp',
  'LEFT JOIN senhas qlb ON qlb.Numero=fge.QuemLibe',
  '',
  'LEFT JOIN gragrux ggx ON ggx.Controle=fge.GraGruX',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  '',
  'LEFT JOIN fixgxpatr gxp ON gxp.GraGrux=fge.GraGruX',
  ' ',
  'WHERE fge.Codigo=' + Geral.FF0(QrFixGereCabCodigo.Value),
  ' ',
  '']);
  //
  QrFixGereEqu.Locate('Controle', Controle, []);
end;


procedure TFmFixGereCab.ReopenFixGerePca(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFixGerePca, Dmod.MyDB, [
  'SELECT fpc.Nome NO_ITEM, fgp.*',
  'FROM fixgerepca fgp',
  'LEFT JOIN fixpecacad fpc ON fpc.Codigo=fgp.CodItem',
  'WHERE fgp.Controle=' + Geral.FF0(QrFixGereEquControle.Value),
  ' ']);
end;

procedure TFmFixGereCab.ReopenFixGereSrv(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFixGereSrv, Dmod.MyDB, [
  'SELECT fpc.Nome NO_ITEM, fgp.*',
  'FROM fixgeresrv fgp',
  'LEFT JOIN fixservcad fpc ON fpc.Codigo=fgp.CodItem',
  'WHERE fgp.Controle=' + Geral.FF0(QrFixGereEquControle.Value),
  ' ']);
end;

procedure TFmFixGereCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFixGereCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFixGereCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFixGereCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFixGereCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFixGereCab.SpeedButton5Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdCliente.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdCliente, CBCliente, QrClientes, VAR_CADASTRO, 'Codigo');
  EdCliente.SetFocus;
end;

procedure TFmFixGereCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFixGereCab.BtSrvClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSrv, BtSrv);
end;

procedure TFmFixGereCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFixGereCabCodigo.Value;
  //
  if TFmFixGereCab(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmFixGereCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFixGereEqu(stIns);
end;

procedure TFmFixGereCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFixGereCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'fixgerecab');
end;

procedure TFmFixGereCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, Empresa, Cliente: Integer;
  Observacao: String;
begin
  Empresa    := EdEmpresa.ValueVariant;
  Cliente    := EdCliente.ValueVariant;
  Observacao := MeObservacao.Text;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a empresa!') then Exit;
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Defina o cliente!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('fixgerecab', 'Codigo', '', '', tsPos,
  ImgTipo.SQLType, QrFixGereCabCodigo.Value);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'fixgerecab', False, [
  'Empresa', 'Cliente', 'Observacao'], [
  'Codigo'], [
  Empresa, Cliente, Observacao], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmFixGereCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'fixgerecab', Codigo);
  //
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'fixgerecab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType, TFmFixGereCab(Self));
end;

procedure TFmFixGereCab.BtEquClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEqu, BtEqu);
end;

procedure TFmFixGereCab.BtPcaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPca, BtPca);
end;

procedure TFmFixGereCab.ItsInclui2Click(Sender: TObject);
begin
  MostraFixGerePca(stIns);
end;

procedure TFmFixGereCab.ItsInclui3Click(Sender: TObject);
begin
  MostraFixGereSrv(stIns);
end;

procedure TFmFixGereCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmFixGereCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBPecServ.Align := alClient;
  CriaOForm;
  FSeq := 0;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDMkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
end;

procedure TFmFixGereCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFixGereCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFixGereCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  PesquisaComplexa();
end;

procedure TFmFixGereCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFixGereCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFixGereCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFixGereCab.QrFixGereCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFixGereCab.QrFixGereCabAfterScroll(DataSet: TDataSet);
begin
  ReopenFixGereEqu(0);
end;

procedure TFmFixGereCab.FormActivate(Sender: TObject);
begin
  if TFmFixGereCab(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrFixGereCabCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmFixGereCab.SbQueryClick(Sender: TObject);
begin
  PesquisaComplexa();
end;

procedure TFmFixGereCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFixGereCab.FormShow(Sender: TObject);
begin
  //FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmFixGereCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFixGereCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'fixgerecab');
  //
  EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
end;

procedure TFmFixGereCab.CalculaCustosConsertoEQ(Controle: Integer);
var
  CustoPeca, CustoServ, CustoTota: Double;
  Qry: TmySQLQuery;
  Codigo: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, SUM(Valor) Valor ',
    'FROM fixgerepca ',
    'WHERE Controle=' + Geral.FF0(Controle),
    'GROUP BY Controle ',
    ' ']);
    CustoPeca := Qry.FieldByName('Valor').AsFloat;
    Codigo    := Qry.FieldByName('Codigo').AsInteger;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Valor) Valor ',
    'FROM fixgeresrv ',
    'WHERE Controle=' + Geral.FF0(Controle),
    'GROUP BY Controle ',
    ' ']);
    CustoServ := Qry.FieldByName('Valor').AsFloat;
    CustoTota := CustoPeca + CustoServ;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fixgereequ', False, [
    'CustoPeca', 'CustoServ', 'CustoTota'], [
    'Controle'], [
    CustoPeca, CustoServ, CustoTota], [
    Controle], True) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(CustoPeca) CustoPeca, ',
      'SUM(CustoServ) CustoServ, ',
      'SUM(CustoTota) CustoTota ',
      'FROM fixgereequ ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'GROUP BY Codigo ',
      ' ']);
      CustoPeca := Qry.FieldByName('CustoPeca').AsFloat;
      CustoServ := Qry.FieldByName('CustoServ').AsFloat;
      CustoTota := Qry.FieldByName('CustoTota').AsFloat;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fixgerecab', False, [
      'CustoPeca', 'CustoServ', 'CustoTota'], [
      'Codigo'], [
      CustoPeca, CustoServ, CustoTota], [
      Codigo], True);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmFixGereCab.QrFixGereCabBeforeClose(
  DataSet: TDataSet);
begin
  QrFixGereEqu.Close;
end;

procedure TFmFixGereCab.QrFixGereCabBeforeOpen(DataSet: TDataSet);
begin
  QrFixGereCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFixGereCab.QrFixGereEquAfterScroll(DataSet: TDataSet);
begin
  ReopenFixGerePca(0);
  ReopenFixGereSrv(0);
end;

procedure TFmFixGereCab.QrFixGereEquBeforeClose(DataSet: TDataSet);
begin
  QrFixGerePca.Close;
  QrFixGereSrv.Close;
end;

procedure TFmFixGereCab.QrFixGereEquCalcFields(DataSet: TDataSet);
begin
  QrFixGereEquTXT_DtHrLibe.Value := Geral.FDT(QrFixGereEquDtHrLibe.Value, 106);
end;

end.

