unit LocCCon;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, dmkEditDateTimePicker,
  dmkValUsu, dmkMemo, NFSe_PF_0000, UnDmkEnums, frxClass, dmkCompoStore,
  Vcl.ComCtrls, Data.DB, UnProjGroup_Consts;

type
  THackDBGrid = class(TDBGrid);
  TAvaliacao = (avalIni, avalFim);
  TFmLocCCon = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrLocCCon: TmySQLQuery;
    DsLocCCon: TDataSource;
    QrLocCPatPri: TmySQLQuery;
    DsLocCPatPri: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrLocCConNO_COMPRADOR: TWideStringField;
    QrLocCConNO_RECEBEU: TWideStringField;
    QrLocCConCodigo: TIntegerField;
    QrLocCConContrato: TIntegerField;
    QrLocCConCliente: TIntegerField;
    QrLocCConDtHrEmi: TDateTimeField;
    QrLocCConDtHrBxa: TDateTimeField;
    QrLocCConVendedor: TIntegerField;
    QrLocCConECComprou: TIntegerField;
    QrLocCConECRetirou: TIntegerField;
    QrLocCConNumOC: TWideStringField;
    QrLocCConValorTot: TFloatField;
    QrLocCConLocalObra: TWideStringField;
    QrLocCConObs0: TWideStringField;
    QrLocCConObs1: TWideStringField;
    QrLocCConObs2: TWideStringField;
    QrLocCConNO_LOGIN: TWideStringField;
    QrLocCConNO_CLIENTE: TWideStringField;
    QrLocCPatPriCodigo: TIntegerField;
    QrLocCPatPriCtrID: TIntegerField;
    QrLocCPatPriGraGruX: TIntegerField;
    QrLocCPatPriValorDia: TFloatField;
    QrLocCPatPriValorSem: TFloatField;
    QrLocCPatPriValorQui: TFloatField;
    QrLocCPatPriValorMes: TFloatField;
    QrLocCPatPriDtHrRetorn: TDateTimeField;
    QrLocCPatPriLibFunci: TIntegerField;
    QrLocCPatPriLibDtHr: TDateTimeField;
    QrLocCPatPriRELIB: TWideStringField;
    QrLocCPatPriHOLIB: TWideStringField;
    QrLocCPatPriNO_GGX: TWideStringField;
    QrLocCPatPriLOGIN: TWideStringField;
    QrLocCPatPriREFERENCIA: TWideStringField;
    QrLocCPatPriTXT_DTA_DEVOL: TWideStringField;
    QrLocCPatPriTXT_DTA_LIBER: TWideStringField;
    QrLocCPatPriTXT_QEM_LIBER: TWideStringField;
    Panel6: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label12: TLabel;
    DBEdit8: TDBEdit;
    Label13: TLabel;
    DBEdit9: TDBEdit;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    Label15: TLabel;
    DBEdit11: TDBEdit;
    GroupBox1: TGroupBox;
    DGDados: TDBGrid;
    Splitter1: TSplitter;
    QrLocCPatSec: TmySQLQuery;
    DsLocCPatSec: TDataSource;
    QrLocCPatSecCodigo: TIntegerField;
    QrLocCPatSecCtrID: TIntegerField;
    QrLocCPatSecItem: TIntegerField;
    QrLocCPatSecGraGruX: TIntegerField;
    QrLocCPatSecNO_GGX: TWideStringField;
    QrLocCPatSecREFERENCIA: TWideStringField;
    QrLocCPatAce: TmySQLQuery;
    DsLocCPatAce: TDataSource;
    QrLocCPatCns: TmySQLQuery;
    DsLocCPatCns: TDataSource;
    QrLocCPatUso: TmySQLQuery;
    DsLocCPatUso: TDataSource;
    QrLocCPatAceCodigo: TIntegerField;
    QrLocCPatAceCtrID: TIntegerField;
    QrLocCPatAceItem: TIntegerField;
    QrLocCPatAceGraGruX: TIntegerField;
    QrLocCPatAceNO_GGX: TWideStringField;
    QrLocCPatAceREFERENCIA: TWideStringField;
    QrLocCPatCnsCodigo: TIntegerField;
    QrLocCPatCnsCtrID: TIntegerField;
    QrLocCPatCnsItem: TIntegerField;
    QrLocCPatCnsGraGruX: TIntegerField;
    QrLocCPatCnsNO_GGX: TWideStringField;
    QrLocCPatCnsREFERENCIA: TWideStringField;
    QrLocCPatUsoCodigo: TIntegerField;
    QrLocCPatUsoCtrID: TIntegerField;
    QrLocCPatUsoItem: TIntegerField;
    QrLocCPatUsoGraGruX: TIntegerField;
    QrLocCPatUsoNO_GGX: TWideStringField;
    QrLocCPatUsoREFERENCIA: TWideStringField;
    QrLocFCab: TmySQLQuery;
    DsLocFCab: TDataSource;
    DsLctFatRef: TDataSource;
    QrLctFatRef: TmySQLQuery;
    QrLctFatRefPARCELA: TIntegerField;
    QrLctFatRefCodigo: TIntegerField;
    QrLctFatRefControle: TIntegerField;
    QrLctFatRefConta: TIntegerField;
    QrLctFatRefLancto: TLargeintField;
    QrLctFatRefValor: TFloatField;
    QrLctFatRefVencto: TDateField;
    QrLocCPatCnsUnidade: TIntegerField;
    QrLocCPatUsoAvalIni: TIntegerField;
    QrLocCPatUsoValTot: TFloatField;
    QrLocCPatCnsQtdIni: TFloatField;
    QrLocCPatCnsQtdFim: TFloatField;
    QrLocCPatCnsPrcUni: TFloatField;
    QrLocCPatCnsValUso: TFloatField;
    Panel11: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    EdCodigo: TdmkEdit;
    DsClientes: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrECComprou: TmySQLQuery;
    QrECComprouControle: TIntegerField;
    QrECComprouNome: TWideStringField;
    DsECComprou: TDataSource;
    QrECRetirou: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsECRetirou: TDataSource;
    EdECComprou: TdmkEditCB;
    CBECComprou: TdmkDBLookupComboBox;
    CBECRetirou: TdmkDBLookupComboBox;
    EdECRetirou: TdmkEditCB;
    EdVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    QrSenhas: TmySQLQuery;
    QrSenhasNumero: TIntegerField;
    QrSenhaslogin: TWideStringField;
    DsSenhas: TDataSource;
    EdNumOC: TdmkEdit;
    EdLocalObra: TdmkEdit;
    QrContratos: TmySQLQuery;
    DsContratos: TDataSource;
    QrContratosCodigo: TIntegerField;
    QrContratosNome: TWideStringField;
    EdObs0: TdmkEdit;
    EdObs1: TdmkEdit;
    EdObs2: TdmkEdit;
    QrLocCConDataEmi: TDateField;
    QrLocCConHoraEmi: TTimeField;
    QrLocCPatUsoUnidade: TIntegerField;
    QrLocCPatUsoSIGLA: TWideStringField;
    QrLocCPatCnsSIGLA: TWideStringField;
    QrLocCPatUsoNO_AVALINI: TWideStringField;
    QrLocCPatUsoNO_AVALFIM: TWideStringField;
    QrLocCPatUsoAvalFim: TIntegerField;
    QrLocCPatCnsQtdUso: TFloatField;
    N1: TMenuItem;
    Libera1: TMenuItem;
    Retorna1: TMenuItem;
    N2: TMenuItem;
    QrLocCPatPriDtHrLocado: TDateTimeField;
    QrLocCPatUsoPrcUni: TFloatField;
    BtFat: TBitBtn;
    QrLocCConDtHrBxa_TXT: TWideStringField;
    Panel7: TPanel;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label21: TLabel;
    TPDataEmi: TdmkEditDateTimePicker;
    EdHoraEmi: TdmkEdit;
    Panel12: TPanel;
    Label20: TLabel;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    QrLocCConEmpresa: TIntegerField;
    QrLocCConNO_EMP: TWideStringField;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    QrLcts: TmySQLQuery;
    QrLocFCabCodigo: TIntegerField;
    QrLocFCabControle: TIntegerField;
    QrLocFCabDtHrFat: TDateTimeField;
    QrLocFCabValLocad: TFloatField;
    QrLocFCabValConsu: TFloatField;
    QrLocFCabValUsado: TFloatField;
    QrLocFCabValFrete: TFloatField;
    QrLocFCabValDesco: TFloatField;
    QrLocFCabValTotal: TFloatField;
    QrLocFCabSerNF: TWideStringField;
    QrLocFCabNumNF: TIntegerField;
    QrLocFCabCondicaoPG: TIntegerField;
    QrLocFCabCartEmis: TIntegerField;
    PMFat: TPopupMenu;
    Faturamentoparcial1: TMenuItem;
    Faturaeencerracontrato1: TMenuItem;
    N3: TMenuItem;
    Excluifaturamento1: TMenuItem;
    Gerabloqueto1: TMenuItem;
    N4: TMenuItem;
    Label7: TLabel;
    EdContrato: TdmkEditCB;
    CBContrato: TdmkDBLookupComboBox;
    Label24: TLabel;
    EdLocalCntat: TdmkEdit;
    Label29: TLabel;
    EdLocalFone: TdmkEdit;
    Label30: TLabel;
    EdEndCobra: TdmkEdit;
    QrLocCConEndCobra: TWideStringField;
    QrLocCConLocalCntat: TWideStringField;
    QrLocCConLocalFone: TWideStringField;
    Label31: TLabel;
    DBEdit14: TDBEdit;
    DBEdit7: TDBEdit;
    Label11: TLabel;
    Label10: TLabel;
    DBEdit6: TDBEdit;
    Label32: TLabel;
    DBEdit15: TDBEdit;
    Label33: TLabel;
    DBEdit16: TDBEdit;
    VUEmpresa: TdmkValUsu;
    SpeedButton5: TSpeedButton;
    QrLocCConFilial: TIntegerField;
    PMAcessorios: TPopupMenu;
    Adicionaacessrio1: TMenuItem;
    Removeacessrio1: TMenuItem;
    PMLocCPatUso: TPopupMenu;
    Adicionamaterialuso1: TMenuItem;
    Removematerialuso1: TMenuItem;
    PMLocCPatCns: TPopupMenu;
    Adicionamaterialdeconsumo1: TMenuItem;
    Removematerialdeconsumo1: TMenuItem;
    Encerracontrato1: TMenuItem;
    Reabrelocao1: TMenuItem;
    EdEndCliente: TdmkEdit;
    Label34: TLabel;
    QrLocCPatPriLibExUsr: TIntegerField;
    ItsExclui2: TMenuItem;
    ItsExclui3: TMenuItem;
    SbImagens: TBitBtn;
    PMImagens: TPopupMenu;
    Visualizarimagemdocliente1: TMenuItem;
    Visualizarimagemdorequisitante1: TMenuItem;
    Visualizarimagemdorecebedor1: TMenuItem;
    ImgCliente: TImage;
    ImgRequisitante: TImage;
    ImgRetirada: TImage;
    N5: TMenuItem;
    BtNFSe: TBitBtn;
    PB1: TProgressBar;
    Visualizarbloquetos1: TMenuItem;
    PMImprime: TPopupMenu;
    Contrato1: TMenuItem;
    Devoluodelocao1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel9: TPanel;
    Splitter3: TSplitter;
    Panel10: TPanel;
    Panel8: TPanel;
    Splitter5: TSplitter;
    GroupBox4: TGroupBox;
    DBGrid3: TDBGrid;
    GroupBox7: TGroupBox;
    DBGLctFatRef: TDBGrid;
    Panel13: TPanel;
    GroupBox6: TGroupBox;
    DBGLocCPatUso: TDBGrid;
    GroupBox2: TGroupBox;
    DBGLocCPatCns: TDBGrid;
    Splitter4: TSplitter;
    GroupBox3: TGroupBox;
    GroupBox5: TGroupBox;
    DBGAcessorios: TDBGrid;
    DBGrid2: TDBGrid;
    Splitter2: TSplitter;
    CSTabSheetChamou: TdmkCompoStore;
    Selecionados1: TMenuItem;
    odos1: TMenuItem;
    QrLoc: TmySQLQuery;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrLocCConAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrLocCConBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrLocCConAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrLocCPatPriCalcFields(DataSet: TDataSet);
    procedure QrLocCConBeforeClose(DataSet: TDataSet);
    procedure QrLocFCabAfterScroll(DataSet: TDataSet);
    procedure QrLocFCabBeforeClose(DataSet: TDataSet);
    procedure QrLocCPatPriAfterScroll(DataSet: TDataSet);
    procedure QrLocCPatPriBeforeClose(DataSet: TDataSet);
    procedure QrLocCPatCnsCalcFields(DataSet: TDataSet);
    procedure QrLctFatRefCalcFields(DataSet: TDataSet);
    procedure EdClienteChange(Sender: TObject);
    procedure QrLocCConCalcFields(DataSet: TDataSet);
    procedure DBGLocCPatCnsDblClick(Sender: TObject);
    procedure Libera1Click(Sender: TObject);
    procedure Retorna1Click(Sender: TObject);
    procedure DBGLocCPatUsoDblClick(Sender: TObject);
    procedure BtFatClick(Sender: TObject);
    procedure PMFatPopup(Sender: TObject);
    procedure Faturamentoparcial1Click(Sender: TObject);
    procedure Faturaeencerracontrato1Click(Sender: TObject);
    procedure Excluifaturamento1Click(Sender: TObject);
    procedure Gerabloqueto1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure DBGAcessoriosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Adicionaacessrio1Click(Sender: TObject);
    procedure Adicionamaterialuso1Click(Sender: TObject);
    procedure Adicionamaterialdeconsumo1Click(Sender: TObject);
    procedure DBGLocCPatUsoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGLocCPatCnsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PMAcessoriosPopup(Sender: TObject);
    procedure PMLocCPatUsoPopup(Sender: TObject);
    procedure PMLocCPatCnsPopup(Sender: TObject);
    procedure Removeacessrio1Click(Sender: TObject);
    procedure Removematerialuso1Click(Sender: TObject);
    procedure Removematerialdeconsumo1Click(Sender: TObject);
    procedure Encerracontrato1Click(Sender: TObject);
    procedure Reabrelocao1Click(Sender: TObject);
    procedure ItsExclui2Click(Sender: TObject);
    procedure ItsExclui3Click(Sender: TObject);
    procedure Visualizarimagemdorequisitante1Click(Sender: TObject);
    procedure Visualizarimagemdorecebedor1Click(Sender: TObject);
    procedure Visualizarimagemdocliente1Click(Sender: TObject);
    procedure SbImagensClick(Sender: TObject);
    procedure PMImagensPopup(Sender: TObject);
    procedure EdECComprouChange(Sender: TObject);
    procedure EdECRetirouChange(Sender: TObject);
    procedure ImgClienteClick(Sender: TObject);
    procedure ImgRequisitanteClick(Sender: TObject);
    procedure ImgRetiradaClick(Sender: TObject);
    procedure BtNFSeClick(Sender: TObject);
    procedure Visualizarbloquetos1Click(Sender: TObject);
    procedure Contrato1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
    procedure odos1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraFormLocCPatPri(SQLType: TSQLType);
    procedure ImprimeDevolucao(Todos: Boolean);
    //
    procedure DefineAvaliacao(Parte: TAvaliacao; Item, Atual: Integer; Reabre:
              Boolean = False);
    procedure MostraFormLocCPatCns();
    procedure MostraFormLocFCab(Encerra: Boolean);
    //
    procedure MostraLocCMateri(Codigo, CtrID, Tipo: Integer);
    procedure AtualizaImagens(Codigo, TipImg: Integer; Image: TImage);
    //
    procedure PesquisaPorCliente();
    procedure VisualizaFoto(Codigo, Entidade, Contato: Integer);
    function  VerificaSeTodosOsItensRetornaram((*QueryLoc: TmySQLQuery;*) Codigo,
                Controle: Integer): Boolean;
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenLocCPatPri(QueryLocCPatPri: TmySQLQuery; Codigo, CtrID: Integer);
    procedure ReopenLocCPatSec(Item: Integer);
    procedure ReopenLocCPatAce(QueryLocCPatAce: TmySQLQuery; CtrID, Item: Integer);
    procedure ReopenLocCPatUso(QueryLocCPatUso: TmySQLQuery; CtrID, Item: Integer);
    procedure ReopenLocCPatCns(QueryLocCPatCns: TmySQLQuery; CtrID, Item: Integer);
    procedure ReopenLocFCab(Controle: Integer);
    procedure ReopenLctFatRef(Conta: Integer);
    //
    procedure ReopenECComprou(Controle: Integer);
    procedure ReopenECRetirou(Controle: Integer);
    function  LiberaItensLocacao(QueryLocCPatPri(*, QueryLoc*): TmySQLQuery): Boolean;
  end;

var
  FmLocCCon: TFmLocCCon;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, LocCPatPri, DmkDAC_PF, SelCod, LocCPatCns,
  ModuleFatura, LocFCab, ModuleGeral, UnBloquetos, ModuleLct2, ModuleFin,
  ContratApp, LocCMateri, MyListas, EntiImagens, Principal, MyGlyfs, UnEntities;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmLocCCon.Libera1Click(Sender: TObject);
var
  DataDef, DataSel, DataMin: TDateTime;
  LibDtHr: String;
  CtrID, LibFunci, UserSelect, LibExUsr, i: Integer;
begin
  if (QrLocCPatPri.State = dsInactive) and (QrLocCPatPri.RecordCount = 0) then Exit;
  //
  DataDef := Now();
  DataMin := Trunc(QrLocCConDtHrEmi.Value);
  DataSel := 0;
  LibExUsr := VAR_USUARIO;
  if not DBCheck.ObtemDataEUser(VAR_USUARIO, UserSelect,
    DataDef, DataSel,dataMin, Time, True) then Exit;
  //
  LibDtHr  := Geral.FDT(DataSel, 109);
  LibFunci := UserSelect;
  //
  CtrID := 0;
  //
  if DGDados.SelectedRows.Count > 1 then
  begin
    with DGDados.DataSource.DataSet do
    for i:= 0 to DGDados.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DGDados.SelectedRows.Items[i]));
      GotoBookmark(DGDados.SelectedRows.Items[i]);
      //
      CtrID := QrLocCPatPriCtrID.Value;
      //
      if not VerificaSeTodosOsItensRetornaram((*QrLoc,*) QrLocCPatPriCodigo.Value, CtrID) then
      begin
        Geral.MB_Info('O patrim�nio ' + QrLocCPatPriNO_GGX.Value +
          ' n�o retornou e por isso n�o pode ser liberado!');
        Exit;
      end;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccpatpri', False, ['LibFunci',
        'LibDtHr', 'LibExUsr'], ['CtrID'], [LibFunci, LibDtHr, LibExUsr],
        [CtrID], True)
      then
        Dmod.VerificaSituacaoPatrimonio(QrLocCPatPriGraGruX.Value);
    end;
  end else
  begin
    CtrID := QrLocCPatPriCtrID.Value;
    //
    if not VerificaSeTodosOsItensRetornaram((*QrLoc,*) QrLocCPatPriCodigo.Value, CtrID) then
    begin
      Geral.MB_Info('O patrim�nio ' + QrLocCPatPriNO_GGX.Value +
        ' n�o retornou e por isso n�o pode ser liberado!');
      Exit;
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccpatpri', False, ['LibFunci',
      'LibDtHr', 'LibExUsr'], ['CtrID'], [LibFunci, LibDtHr, LibExUsr],
      [CtrID], True)
    then
      Dmod.VerificaSituacaoPatrimonio(QrLocCPatPriGraGruX.Value);
  end;
  ReopenLocCPatPri(QrLocCPatPri, QrLocCConCodigo.Value, CtrID);
end;

function TFmLocCCon.LiberaItensLocacao(QueryLocCPatPri(*, QueryLoc*): TmySQLQuery): Boolean;
var
  LibDtHr: String;
  CtrID, LibFunci, LibExUsr: Integer;
  DataMi: TDateTime;  
begin
  Result := False;
  CtrID  := 0;
  DataMi := EncodeDate(1900, 1, 1);
  //
  if (QueryLocCPatPri.State <> dsInactive) and (QueryLocCPatPri.RecordCount > 0) then
  begin
    QueryLocCPatPri.First;
    while not QueryLocCPatPri.Eof do
    begin
      if not VerificaSeTodosOsItensRetornaram((*QueryLoc,*) QueryLocCPatPri.FieldByName('Codigo').AsInteger, CtrID) then
      begin
        Geral.MB_Info('Encerramento cancelado!' + #13#10 +
          'Motivo: O patrim�nio ' + QueryLocCPatPri.FieldByName('NO_GGX').AsString +
          ' n�o retornou e por isso n�o pode ser liberado!');
        Exit;
      end;
      CtrID := QueryLocCPatPri.FieldByName('CtrID').AsInteger;
      //
      if QueryLocCPatPri.FieldByName('LibDtHr').AsDateTime < DataMi then
      begin
        LibFunci := VAR_USUARIO;
        LibDtHr  := Geral.FDT(Now(), 109);
        LibExUsr := VAR_USUARIO;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccpatpri', False, [
          'LibFunci', 'LibDtHr', 'LibExUsr'], ['CtrID'], [LibFunci, LibDtHr,
          LibExUsr], [CtrID], True)
        then
          Dmod.VerificaSituacaoPatrimonio(QueryLocCPatPri.FieldByName('GraGruX').AsInteger);
      end;
      QueryLocCPatPri.Next;
    end;
    ReopenLocCPatPri(QueryLocCPatPri, QueryLocCPatPri.FieldByName('Codigo').AsInteger, CtrID);
    //
    Result := True;
  end;
end;

procedure TFmLocCCon.VisualizaFoto(Codigo, Entidade, Contato: Integer);
begin
  LocCod(Codigo, Codigo);
  //
  if Contato = 0  then
    Entities.MostraEntiImagens(Entidade, 0, 0, True)
  else
    Entities.MostraEntiImagens(Entidade, Contato, 1, True);
end;

procedure TFmLocCCon.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmLocCCon.MostraFormLocCPatCns();
var
  SQLType: TSQLType;
begin
  SQLType := stUpd;
  //
  if DBCheck.CriaFm(TFmLocCPatCns, FmLocCPatCns, afmoNegarComAviso) then
  begin
    FmLocCPatCns.ImgTipo.SQLType := SQLType;
    FmLocCPatCns.FQrCab := QrLocCPatCns;
    FmLocCPatCns.FDsCab := DsLocCPatCns;
    FmLocCPatCns.FQrIts := QrLocCPatCns;
    if SQLType = stIns then
      //?
    else
    begin
      //FmLocCPatCns.EdItem.ValueVariant := QrLocCPatCnsItem.Value;
      FmLocCPatCns.EdQtdIni.ValueVariant := QrLocCPatCnsQtdIni.Value;
      FmLocCPatCns.EdQtdFim.ValueVariant := QrLocCPatCnsQtdFim.Value;
      FmLocCPatCns.EdPrcUni.ValueVariant := QrLocCPatCnsPrcUni.Value;
    end;
    FmLocCPatCns.ShowModal;
    FmLocCPatCns.Destroy;
  end;
end;

procedure TFmLocCCon.MostraFormLocCPatPri(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmLocCPatPri, FmLocCPatPri, afmoNegarComAviso) then
  begin
    FmLocCPatPri.ImgTipo.SQLType := SQLType;
    FmLocCPatPri.FQrCab := QrLocCCon;
    FmLocCPatPri.FDsCab := DsLocCCon;
    FmLocCPatPri.FQrIts := QrLocCPatPri;
    if SQLType = stIns then
      //?
    else
    begin
      FmLocCPatPri.FOldGGX := QrLocCPatPriGraGruX.Value;
      //
      FmLocCPatPri.EdCtrID.ValueVariant := QrLocCPatPriCtrID.Value;
      FmLocCPatPri.EdGraGruX.ValueVariant := QrLocCPatPriGraGruX.Value;
      FmLocCPatPri.CBGraGruX.KeyValue := QrLocCPatPriGraGruX.Value;
      FmLocCPatPri.TPDataLoc.Date := Trunc(QrLocCPatPriDtHrLocado.Value);
      FmLocCPatPri.EdHoraLoc.Text := FormatDateTime('hh:nn:ss', QrLocCPatPriDtHrLocado.Value);
    end;
    FmLocCPatPri.ShowModal;
    FmLocCPatPri.Destroy;
  end;
end;

procedure TFmLocCCon.MostraFormLocFCab(Encerra: Boolean);
const
  SQLType = stIns;
var
  Codigo: Integer;
begin
  Codigo := QrLocCConCodigo.Value;
  //
  if DBCheck.CriaFm(TFmLocFCab, FmLocFCab, afmoNegarComAviso) then
  begin
    FmLocFCab.ImgTipo.SQLType := SQLType;
    FmLocFCab.FQrCab          := QrLocCCon;
    FmLocFCab.FDsCab          := DsLocCCon;
    FmLocFCab.FQrIts          := QrLocFCab;
    FmLocFCab.FQrLocCPatPri   := QrLocCPatPri;
    //FmLocFCab.FQrLoc          := QrLoc;
    FmLocFCab.FEmpresa        := QrLocCConEmpresa.Value;
    FmLocFCab.FFilial         := QrLocCConFilial.Value;
    FmLocFCab.FCliente        := QrLocCConCliente.Value;
    FmLocFCab.FDtHrEmi        := QrLocCConDtHrEmi.Value;
    FmLocFCab.FEncerra        := Encerra;
    //
    if SQLType = stIns then
    begin
      FmLocFCab.TPDataFat.Date := Trunc(Date);
      FmLocFCab.EdHoraFat.Text := FormatDateTime('hh:nn:ss', Now());
    end else
    begin
      FmLocFCab.EdControle.ValueVariant   := QrLocFCabControle.Value;
      FmLocFCab.EdCondicaoPG.ValueVariant := QrLocFCabCondicaoPG.Value;
      FmLocFCab.CBCondicaoPG.KeyValue     := QrLocFCabCondicaoPG.Value;
      FmLocFCab.EdCartEmis.ValueVariant   := QrLocFCabCartEmis.Value;
      FmLocFCab.CBCartEmis.KeyValue       := QrLocFCabCartEmis.Value;
      FmLocFCab.EdValLocad.ValueVariant   := QrLocFCabValLocad.Value;
      FmLocFCab.EdValConsu.ValueVariant   := QrLocFCabValConsu.Value;
      FmLocFCab.EdValUsado.ValueVariant   := QrLocFCabValUsado.Value;
      FmLocFCab.EdValFrete.ValueVariant   := QrLocFCabValFrete.Value;
      FmLocFCab.EdValDesco.ValueVariant   := QrLocFCabValDesco.Value;
      FmLocFCab.EdValTotal.ValueVariant   := QrLocFCabValTotal.Value;
      FmLocFCab.EdSerNF.ValueVariant      := QrLocFCabSerNF.Value;
      FmLocFCab.EdNumNF.ValueVariant      := QrLocFCabNumNF.Value;
      FmLocFCab.TPDataFat.Date            := Trunc(QrLocFCabDtHrFat.Value);
      FmLocFCab.EdHoraFat.Text            := FormatDateTime('hh:nn:ss', QrLocFCabDtHrFat.Value);
    end;
    FmLocFCab.ShowModal;
    FmLocFCab.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmLocCCon.MostraLocCMateri(Codigo, CtrID, Tipo: Integer);
begin
  if Tipo = 0 then Exit;
  //
  if DBCheck.CriaFm(TFmLocCMateri, FmLocCMateri, afmoNegarComAviso) then
  begin
    FmLocCMateri.FTipo         := Tipo;
    FmLocCMateri.FCodigo       := Codigo;
    FmLocCMateri.FCtrID        := CtrID;
    FmLocCMateri.FQrLocCPatCns := QrLocCPatCns;
    FmLocCMateri.FQrLocCPatAce := QrLocCPatAce;
    FmLocCMateri.FQrLocCPatUso := QrLocCPatUso;
    FmLocCMateri.ShowModal;
    FmLocCMateri.Destroy;
  end;
end;

procedure TFmLocCCon.odos1Click(Sender: TObject);
begin
  ImprimeDevolucao(True);
end;

procedure TFmLocCCon.PesquisaPorCliente;
var
  Codigo: Integer;
begin
  Codigo := CuringaLoc.CriaForm('lcc.Codigo',
              'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome)', 'locccon lcc',
              Dmod.MyDB, '', False, 'LEFT JOIN entidades cli ON cli.Codigo=lcc.Cliente');
  //
  LocCod(QrLocCConCodigo.Value, Codigo);
end;

procedure TFmLocCCon.PMAcessoriosPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  if (QrLocCCon.State <> dsInactive) and (QrLocCCon.RecordCount > 0) and
    (Length(QrLocCConDtHrBxa_TXT.Value) > 0) then
  begin
    Adicionaacessrio1.Enabled := False;
    Removeacessrio1.Enabled   := False;
  end else
  begin
    Enab  := (QrLocCPatPri.State <> dsInactive) and (QrLocCPatPri.RecordCount > 0);
    Enab2 := (QrLocCPatAce.State <> dsInactive) and (QrLocCPatAce.RecordCount > 0);
    //
    Adicionaacessrio1.Enabled := Enab;
    Removeacessrio1.Enabled   := Enab and Enab2;
  end;
end;

procedure TFmLocCCon.PMCabPopup(Sender: TObject);
begin
  if (QrLocCCon.State <> dsInactive) and (QrLocCCon.RecordCount > 0) and
    (Length(QrLocCConDtHrBxa_TXT.Value) > 0) then
  begin
    CabAltera1.Enabled := False;
    CabExclui1.Enabled := False;
  end else
  begin
    MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrLocCCon);
    MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrLocCCon, QrLocCPatPri);
  end;
end;

procedure TFmLocCCon.PMFatPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  if (QrLocCCon.State <> dsInactive) and (QrLocCCon.RecordCount > 0) and
    (Length(QrLocCConDtHrBxa_TXT.Value) > 0) then
  begin
    Faturamentoparcial1.Enabled     := False;
    Faturaeencerracontrato1.Enabled := False;
    Encerracontrato1.Enabled        := False;
    //
    Excluifaturamento1.Enabled := True;
    Reabrelocao1.Enabled       := True;    
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT COUNT(CtrID) Itens, ',
    'SUM(IF(YEAR(DtHrRetorn) < 1901, 1, 0)) Abertos ',
    'FROM loccpatpri ',
    'WHERE Codigo=' + Geral.FF0(QrLocCConCodigo.Value),
    '']);
    Faturamentoparcial1.Enabled     := Dmod.QrAux.FieldByName('Abertos').AsFloat > 0.01;
    Faturaeencerracontrato1.Enabled := Dmod.QrAux.FieldByName('Itens').AsFloat > 0.01;
    Encerracontrato1.Enabled        := Dmod.QrAux.FieldByName('Itens').AsFloat > 0.01;
    {
    Gerabloqueto1.Enabled := (QrLocFCab.State <> dsInactive) and
      (QrLocFCab.RecordCount > 0);
    }
    //
    Enab := (QrLocFCab.State <> dsInactive) and (QrLocFCab.RecordCount > 0);
    //
    Excluifaturamento1.Enabled := Enab;
    Reabrelocao1.Enabled       := Enab;    
  end;
  Gerabloqueto1.Enabled := (QrLctFatRef.State <> dsInactive) and
    (QrLctFatRef.RecordCount > 0) and
    (not UBloquetos.VerificaSeBoletoExiste(QrLctFatRefLancto.Value))
end;

procedure TFmLocCCon.PMImagensPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrLocCCon.State <> dsInactive) and (QrLocCCon.RecordCount > 0);
  //
  if Enab then
  begin
    Visualizarimagemdocliente1.Enabled      := Enab and (QrLocCConCliente.Value <> 0);
    Visualizarimagemdorequisitante1.Enabled := Enab and (QrLocCConECComprou.Value <> 0);
    Visualizarimagemdorecebedor1.Enabled    := Enab and (QrLocCConECRetirou.Value <> 0);
  end else
  begin
    Visualizarimagemdocliente1.Enabled      := Enab;
    Visualizarimagemdorequisitante1.Enabled := Enab;
    Visualizarimagemdorecebedor1.Enabled    := Enab;
  end;
end;

procedure TFmLocCCon.PMItsPopup(Sender: TObject);
begin
  if (QrLocCCon.State <> dsInactive) and (QrLocCCon.RecordCount > 0) and
    (Length(QrLocCConDtHrBxa_TXT.Value) > 0) then
  begin
    ItsInclui1.Enabled := False;
    ItsAltera1.Enabled := False;
    //
    Retorna1.Enabled := False;
    Libera1.Enabled := False;
    //
    ItsExclui1.Enabled := False;
    //
    ItsExclui2.Enabled := False;
    ItsExclui3.Enabled := False;
  end else
  begin
    MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrLocCCon);
    MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrLocCPatPri);
    //
    MyObjects.HabilitaMenuItemItsUpd(Retorna1, QrLocCPatPri);
    MyObjects.HabilitaMenuItemItsUpd(Libera1, QrLocCPatPri);
    //
    MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrLocCPatPri);
    //
    MyObjects.HabilitaMenuItemItsDel(ItsExclui2, QrLocCPatPri);
    MyObjects.HabilitaMenuItemItsDel(ItsExclui3, QrLocCPatPri);
  end;
end;

procedure TFmLocCCon.PMLocCPatCnsPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  if (QrLocCCon.State <> dsInactive) and (QrLocCCon.RecordCount > 0) and
    (Length(QrLocCConDtHrBxa_TXT.Value) > 0) then
  begin
    Adicionamaterialdeconsumo1.Enabled := False;
    Removematerialdeconsumo1.Enabled   := False;
  end else
  begin
    Enab  := (QrLocCPatPri.State <> dsInactive) and (QrLocCPatPri.RecordCount > 0);
    Enab2 := (QrLocCPatCns.State <> dsInactive) and (QrLocCPatCns.RecordCount > 0);
    //
    Adicionamaterialdeconsumo1.Enabled := Enab;
    Removematerialdeconsumo1.Enabled   := Enab and Enab2;
  end;
end;

procedure TFmLocCCon.PMLocCPatUsoPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  if (QrLocCCon.State <> dsInactive) and (QrLocCCon.RecordCount > 0) and
    (Length(QrLocCConDtHrBxa_TXT.Value) > 0) then
  begin
    Adicionamaterialuso1.Enabled := False;
    Removematerialuso1.Enabled   := False;
  end else
  begin
    Enab  := (QrLocCPatPri.State <> dsInactive) and (QrLocCPatPri.RecordCount > 0);
    Enab2 := (QrLocCPatUso.State <> dsInactive) and (QrLocCPatUso.RecordCount > 0);
    //
    Adicionamaterialuso1.Enabled := Enab;
    Removematerialuso1.Enabled   := Enab and Enab2;
  end;
end;

procedure TFmLocCCon.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrLocCConCodigo.Value, LaRegistro.Caption[2]);
end;

function TFmLocCCon.VerificaSeTodosOsItensRetornaram((*QueryLoc: TmySQLQuery;*)
  Codigo, Controle: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin

  //False = Existem itens que n�o retornaram
  //True  = Todos os itens retornaram
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT lpp.CtrID');
    Qry.SQL.Add('FROM loccpatpri lpp');
    Qry.SQL.Add('WHERE lpp.Codigo=' + Geral.FF0(Codigo));
    if Controle <> 0 then
      Qry.SQL.Add('AND lpp.CtrID=' + Geral.FF0(Controle));
    Qry.SQL.Add('AND lpp.DtHrRetorn < "1900-01-01"');
    UMyMod.AbreQuery(Qry, Dmod.MyDB);
    if Qry.RecordCount > 0 then
      Result := False
    else
      Result := True;
  finally
    Qry.Free;
  end;
end;

procedure TFmLocCCon.Visualizarbloquetos1Click(Sender: TObject);
begin
  UBloquetos.MostraBloGeren(0, VAR_FATID_3001, 0, QrLocCConCodigo.Value,
    FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPagerNovo);
end;

procedure TFmLocCCon.Visualizarimagemdocliente1Click(Sender: TObject);
begin
  VisualizaFoto(QrLocCConCodigo.Value, QrLocCConCliente.Value, 0);
end;

procedure TFmLocCCon.Visualizarimagemdorecebedor1Click(Sender: TObject);
begin
  VisualizaFoto(QrLocCConCodigo.Value, QrLocCConCliente.Value, QrLocCConECRetirou.Value);
end;

procedure TFmLocCCon.Visualizarimagemdorequisitante1Click(Sender: TObject);
begin
  VisualizaFoto(QrLocCConCodigo.Value, QrLocCConCliente.Value, QrLocCConECComprou.Value);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmLocCCon.DefParams;
begin
  VAR_GOTOTABELA := 'locccon';
  VAR_GOTOMYSQLTABLE := QrLocCCon;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT com.Nome NO_COMPRADOR, ret.Nome NO_RECEBEU, ');
  VAR_SQLx.Add('lcc.*, sen.Login NO_LOGIN, cin.CodCliInt Filial, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE ');
  VAR_SQLx.Add('FROM locccon lcc ');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=lcc.Empresa ');
  VAR_SQLx.Add('LEFT JOIN enticliint cin ON cin.CodEnti=emp.Codigo ');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=lcc.Cliente ');
  VAR_SQLx.Add('LEFT JOIN senhas sen ON sen.Numero=lcc.Vendedor ');
  VAR_SQLx.Add('LEFT JOIN enticontat com ON com.Controle=lcc.ECComprou ');
  VAR_SQLx.Add('LEFT JOIN enticontat ret ON ret.Controle=lcc.ECRetirou ');
  VAR_SQLx.Add(' ');
  VAR_SQLx.Add('WHERE lcc.Codigo > 0');
  //
  VAR_SQL1.Add('AND lcc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmLocCCon.EdClienteChange(Sender: TObject);
begin
  ReopenECComprou(0);
  ReopenECRetirou(0);
  //
  if EdCliente.ValueVariant <> 0 then
    EdEndCliente.Text := GOTOy.EnderecoDeEntidade(EdCliente.ValueVariant, 0);
  //
  AtualizaImagens(EdCliente.ValueVariant, 0, ImgCliente);
end;

procedure TFmLocCCon.EdECComprouChange(Sender: TObject);
begin
  AtualizaImagens(EdECComprou.ValueVariant, 1, ImgRequisitante);
end;

procedure TFmLocCCon.EdECRetirouChange(Sender: TObject);
begin
  AtualizaImagens(EdECRetirou.ValueVariant, 1, ImgRetirada);
end;

procedure TFmLocCCon.Encerracontrato1Click(Sender: TObject);
var
  DataDef, DataSel, DataMin: TDateTime;
  DtHrBxa: String;
  ValTotalFat: Double;
  Codigo: Integer;
  Qry: TmySQLQuery;
begin
  if not LiberaItensLocacao(QrLocCPatPri(*, QrLoc*)) then Exit;
  //
  Codigo  := QrLocCConCodigo.Value;
  DataDef := Now();
  DataMin := 0;
  DataSel := 0;
  if not DBCheck.ObtemData(DataDef, DataSel,dataMin, Time, True) then Exit;
  //
  DtHrBxa := Geral.FDT(DataSel, 109);
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT SUM(lfc.ValTotal) ValTotal');
    Qry.SQL.Add('FROM locfcab lfc');
    Qry.SQL.Add('WHERE lfc.Codigo=' + Geral.FF0(Codigo));
    UMyMod.AbreQuery(Qry, Dmod.MyDB);
    if Qry.RecordCount > 0 then
      ValTotalFat := Qry.FieldByName('ValTotal').AsFloat
    else
      ValTotalFat := 0;
  finally
    Qry.Free;
  end;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'locccon', False, ['ValorTot',
    'DtHrBxa'], ['Codigo'], [ValTotalFat, DtHrBxa], [Codigo], True);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmLocCCon.Gerabloqueto1Click(Sender: TObject);
var
  Entidade, Filial: Integer;
  TabLctA: String;
begin
  try
    Screen.Cursor         := crHourGlass;
    Gerabloqueto1.Enabled := False;
    //
    Entidade := QrLocCConEmpresa.Value;
    Filial   := DmFatura.ObtemFilialDeEntidade(Entidade);
    TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
    //
    UBloquetos.BloquetosParcelasByFatNum(Entidade, VAR_FatID_3001,
      QrLocFCabControle.Value, QrLocCConCodigo.Value, TabLctA, True, PB1);
  finally
    Gerabloqueto1.Enabled := True;
    Screen.Cursor         := crDefault;
  end;
end;

procedure TFmLocCCon.Excluifaturamento1Click(Sender: TObject);
var
  Codigo, Filial, Quitados: Integer;
  TabLctA: String;
begin
  if MyObjects.FIC(QrLocFCabSerNF.Value <> '', nil, 'Exclus�o abortada!' + #13#10 +
    'Motivo: J� foi emitida um Nota Fiscal para este faturamento!') then Exit;
  //
  if MyObjects.FIC((QrLocFCab.State = dsInactive) or (QrLocFCab.RecordCount = 0),
    nil, 'N�o h� nenhum faturamento para ser exclu�do!') then Exit;
  //
  Filial  := DmFatura.ObtemFilialDeEntidade(QrLocCConEmpresa.Value);
  TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  Codigo  := QrLocCConCodigo.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, Dmod.MyDB, [
  'SELECT COUNT(Controle) Itens ',
  'FROM ' + TabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_3001),
  'AND FatNum=' + Geral.FF0(QrLocFCabControle.Value),
  'AND (Sit > 0 OR Compensado > 2) ',
  '']);
  //
  Quitados := DModFin.FaturamentosQuitados(
    TabLctA, VAR_FATID_3001, QrLocFCabControle.Value);
  //
  if Quitados > 0 then
  begin
    Geral.MensagemBox('Existem ' + Geral.FF0(Quitados) +
    ' itens j� quitados que impedem a exclus�o deste faturamento',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Geral.MensagemBox(
  'Confirma a exclus�o do faturamento e de TODOS seus lan�amentos financeiros atrelados?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UBloquetos.DesfazerBoletosFatID(QrLoc, Dmod.MyDB, VAR_FatID_3001,
      QrLocCConCodigo.Value, TabLctA) then
    begin
      Screen.Cursor := crHourGlass;
      try
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
          'DELETE FROM ' + TabLctA,
          'WHERE FatID=' + Geral.FF0(VAR_FATID_3001),
          'AND FatNum=' + Geral.FF0(QrLocFCabControle.Value),
          '']);
        //
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
          'DELETE FROM lctfatref',
          'WHERE Controle=' + Geral.FF0(QrLocFCabControle.Value),
          '']);
        //
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
          'DELETE FROM locfcab ',
          'WHERE Controle=' + Geral.FF0(QrLocFCabControle.Value),
          '']);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'locccon', False, [
          'ValorTot', 'DtHrBxa'], ['Codigo'], [0, ''], [Codigo], True);
        //
        ReopenLocFCab(0);
        LocCod(Codigo, Codigo);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmLocCCon.ItsExclui2Click(Sender: TObject);
var
  CtrID: Integer;
begin
  CtrID := QrLocCPatPriCtrID.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccpatpri', False, ['RetFunci',
    'DtHrRetorn', 'RetExUsr'], ['CtrID'], [0, '', 0], [CtrID], True) then
  begin
    Dmod.VerificaSituacaoPatrimonio(QrLocCPatPriGraGruX.Value);
    ReopenLocCPatPri(QrLocCPatPri, QrLocCConCodigo.Value, CtrID);
    //
    UMyMod.ProximoRegistro(QrLocCPatPri, 'CtrID', QrLocCPatPriCtrID.Value);
  end;
end;

procedure TFmLocCCon.ItsExclui3Click(Sender: TObject);
var
  CtrID: Integer;
begin
  CtrID := QrLocCPatPriCtrID.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccpatpri', False, ['LibFunci',
    'LibDtHr', 'LibExUsr'], ['CtrID'], [0, '', 0], [CtrID], True) then
  begin
    Dmod.VerificaSituacaoPatrimonio(QrLocCPatPriGraGruX.Value);
    ReopenLocCPatPri(QrLocCPatPri, QrLocCConCodigo.Value, CtrID);
    //
    UMyMod.ProximoRegistro(QrLocCPatPri, 'CtrID', QrLocCPatPriCtrID.Value);
  end;
end;

procedure TFmLocCCon.ImgClienteClick(Sender: TObject);
begin
  Entities.MostraEntiImagens(EdCliente.ValueVariant, EdCliente.ValueVariant, 0, True);
  //
  AtualizaImagens(EdCliente.ValueVariant, 0, ImgCliente);
  AtualizaImagens(EdECComprou.ValueVariant, 1, ImgRequisitante);
  AtualizaImagens(EdECRetirou.ValueVariant, 1, ImgRetirada);
end;

procedure TFmLocCCon.ImgRequisitanteClick(Sender: TObject);
begin
  Entities.MostraEntiImagens(EdCliente.ValueVariant, EdECComprou.ValueVariant, 1, True);
  //
  AtualizaImagens(EdCliente.ValueVariant, 0, ImgCliente);
  AtualizaImagens(EdECComprou.ValueVariant, 1, ImgRequisitante);
  AtualizaImagens(EdECRetirou.ValueVariant, 1, ImgRetirada);
end;

procedure TFmLocCCon.ImgRetiradaClick(Sender: TObject);
begin
  Entities.MostraEntiImagens(EdCliente.ValueVariant, EdECRetirou.ValueVariant, 1, True);
  //
  AtualizaImagens(EdCliente.ValueVariant, 0, ImgCliente);
  AtualizaImagens(EdECComprou.ValueVariant, 1, ImgRequisitante);
  AtualizaImagens(EdECRetirou.ValueVariant, 1, ImgRetirada);
end;

procedure TFmLocCCon.ImprimeDevolucao(Todos: Boolean);
var
  CtrID_Txt: String;
  I, CtrID: Integer;
begin
  if not Todos then
  begin
    if DGDados.SelectedRows.Count > 1 then
    begin
      CtrID_Txt := '';
      //
      with DGDados.DataSource.DataSet do
      begin
        for I := 0 to DGDados.SelectedRows.Count - 1 do
        begin
          //GotoBookmark(pointer(DGDados.SelectedRows.Items[i]));
          GotoBookmark(DGDados.SelectedRows.Items[i]);
          //
          CtrID := QrLocCPatPriCtrID.Value;
          //
          if I = DGDados.SelectedRows.Count - 1 then
            CtrID_Txt := CtrID_Txt + Geral.FF0(CtrID)
          else
            CtrID_Txt := CtrID_Txt + Geral.FF0(CtrID) + ', ';
        end;
      end;
    end else
      CtrID_Txt := Geral.FF0(QrLocCPatPriCtrID.Value);
  end else
    CtrID_Txt := '';
  //
  Application.CreateForm(TFmContratApp, FmContratApp);
  //
  FmContratApp.FLocCCon := QrLocCConCodigo.Value;
  //
  FmContratApp.ImprimeDev(CtrID_Txt);
  //
  FmContratApp.Destroy;
end;

procedure TFmLocCCon.ItsAltera1Click(Sender: TObject);
begin
  MostraFormLocCPatPri(stUpd);
end;

procedure TFmLocCCon.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  //Verifica se tem patrim�nios
  if (QrLocCPatPri.State <> dsInactive) and (QrLocCPatPri.RecordCount > 0) then
  begin
    Geral.MensagemBox(
      'Existe(m) patrim�nio(s) cadastradado(s) para esta loca��o!' + #13#10 +
      'Exclua-o(s) e tente novamente.', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //Verifica se tem acess�rios
  if (QrLocCPatAce.State <> dsInactive) and (QrLocCPatAce.RecordCount > 0) then
  begin
    Geral.MensagemBox(
      'Existe(m) acess�rio(s) cadastradado(s) para esta loca��o!' + #13#10 +
      'Exclua-o(s) e tente novamente.', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //Verifica se tem materiais de uso
  if (QrLocCPatUso.State <> dsInactive) and (QrLocCPatUso.RecordCount > 0) then
  begin
    Geral.MensagemBox(
      'Existe(m) material(is) de uso cadastradado(s) para esta loca��o!' +
      #13#10 + 'Exclua-o(s) e tente novamente.', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //Verifica se tem materiais de consumo
  if (QrLocCPatCns.State <> dsInactive) and (QrLocCPatCns.RecordCount > 0) then
  begin
    Geral.MensagemBox(
      'Existe(m) material(is) de consumo cadastradado(s) para esta loca��o!' +
      #13#10 + 'Exclua-o(s) e tente novamente.', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //Verifica se tem faturamento
  if (QrLocFCab.State <> dsInactive) and (QrLocFCab.RecordCount > 0) then
  begin
    if Geral.MensagemBox('Existe faturamento cadastradado para esta loca��o!' +
      #13#10 + 'Deseja excluir este item assim mesmo?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES
    then
      Exit;
  end;
  Codigo := QrLocCConCodigo.Value;
  //
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da loca��o atual?',
  'locccon', 'Codigo', Codigo, Dmod.MyDB) = ID_YES then
  begin
    Va(vpLast);
  end;
end;

procedure TFmLocCCon.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmLocCCon.QueryPrincipalAfterOpen;
begin
end;

procedure TFmLocCCon.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  //Verifica se tem acess�rios
  if (QrLocCPatAce.State <> dsInactive) and (QrLocCPatAce.RecordCount > 0) then
  begin
    Geral.MensagemBox(
      'Existe(m) acess�rio(s) cadastradado(s) para esta loca��o!' + #13#10 +
      'Exclua-o(s) e tente novamente.', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //Verifica se tem materiais de uso
  if (QrLocCPatUso.State <> dsInactive) and (QrLocCPatUso.RecordCount > 0) then
  begin
    Geral.MensagemBox(
      'Existe(m) material(is) de uso cadastradado(s) para esta loca��o!' +
      #13#10 + 'Exclua-o(s) e tente novamente.', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //Verifica se tem materiais de consumo
  if (QrLocCPatCns.State <> dsInactive) and (QrLocCPatCns.RecordCount > 0) then
  begin
    Geral.MensagemBox(
      'Existe(m) material(is) de consumo cadastradado(s) para esta loca��o!' +
      #13#10 + 'Exclua-o(s) e tente novamente.', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if (QrLocFCab.State <> dsInactive) and (QrLocFCab.RecordCount > 0) then
  begin
    if Geral.MensagemBox('Existe faturamento cadastradado para esta loca��o!' +
      #13#10 + 'Deseja excluir este item assim mesmo?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES
    then
      Exit;
  end;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'LocCPatPri', 'CtrID', QrLocCPatPriCtrID.Value, Dmod.MyDB) = ID_YES then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragxpatr', False, [
    'Situacao'], ['GraGruX'], [CO_SIT_PATR_068_D_Disponivel],
    [QrLocCPatPriGraGruX.Value], True);
    //
    Controle := GOTOy.LocalizaPriorNextIntQr(QrLocCPatPri,
      QrLocCPatPriCtrID, QrLocCPatPriCtrID.Value);
    ReopenLocCPatPri(QrLocCPatPri, QrLocCConCodigo.Value, Controle);
  end;
end;

procedure TFmLocCCon.Reabrelocao1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrLocCConCodigo.Value;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'locccon', False, [
  'DtHrBxa'], ['Codigo'], [''], [Codigo], True);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmLocCCon.Removeacessrio1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrLocCPatAce, DBGAcessorios,
    'loccpatace', ['Item'], ['Item'], istPergunta, '');
end;

procedure TFmLocCCon.Removematerialdeconsumo1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrLocCPatCns, DBGLocCPatCns,
    'loccpatcns', ['Item'], ['Item'], istPergunta, '');
end;

procedure TFmLocCCon.Removematerialuso1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrLocCPatUso, DBGLocCPatUso,
    'loccpatuso', ['Item'], ['Item'], istPergunta, '');
end;

procedure TFmLocCCon.ReopenECComprou(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrECComprou, Dmod.MyDB, [
  'SELECT Controle, Nome ',
  'FROM enticontat ',
  'WHERE Codigo=' + Geral.FF0(EdCliente.ValueVariant),
  'ORDER BY Nome ',
  '']);
  //
  QrECComprou.Locate('Controle', Controle, []);
  //
  EdECComprou.ValueVariant := 0;
  CBECComprou.KeyValue := 0;
end;

procedure TFmLocCCon.ReopenECRetirou(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrECRetirou, Dmod.MyDB, [
  'SELECT Controle, Nome ',
  'FROM enticontat ',
  'WHERE Codigo=' + Geral.FF0(EdCliente.ValueVariant),
  'ORDER BY Nome ',
  '']);
  //
  QrECRetirou.Locate('Controle', Controle, []);
  //
  EdECRetirou.ValueVariant := 0;
  CBECRetirou.KeyValue := 0;
end;

procedure TFmLocCCon.ReopenLocCPatAce(QueryLocCPatAce: TmySQLQuery; CtrID, Item: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocCPatAce, Dmod.MyDB, [
    'SELECT lpa.*, gg1.Nome NO_GGX, gg1.REFERENCIA ',
    'FROM loccpatace lpa ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpa.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE lpa.CtrID=' + Geral.FF0(CtrID),
    '']);
  //
  QueryLocCPatAce.Locate('Item', Item, []);
end;

procedure TFmLocCCon.ReopenLocCPatCns(QueryLocCPatCns: TmySQLQuery; CtrID,
  Item: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocCPatCns, Dmod.MyDB, [
    'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,',
    'med.SIGLA ',
    'FROM loccpatcns lpc ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade',
    'WHERE lpc.CtrID=' + Geral.FF0(CtrID),
    '']);
  //
  QueryLocCPatCns.Locate('Item', Item, []);
end;

procedure TFmLocCCon.ReopenLocCPatPri(QueryLocCPatPri: TmySQLQuery;
  Codigo, CtrID: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocCPatPri, Dmod.MyDB, [
  'SELECT lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN ',
  'FROM loccpatpri lpp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
  'WHERE lpp.Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  QueryLocCPatPri.Locate('CtrID', CtrID, []);
end;


procedure TFmLocCCon.ReopenLocCPatSec(Item: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCPatSec, Dmod.MyDB, [
  'SELECT lps.*, gg1.Nome NO_GGX, gg1.REFERENCIA ',
  'FROM loccpatsec lps ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lps.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE lps.CtrID=' + Geral.FF0(QrLocCPatPriCtrID.Value),
  'AND lps.GraGruX <> 0',
  '']);
  //
  QrLocCPatSec.Locate('Item', Item, []);
end;

procedure TFmLocCCon.ReopenLocCPatUso(QueryLocCPatUso: TmySQLQuery; CtrID, Item: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocCPatUso, Dmod.MyDB, [
    'SELECT lpu.*, gg1.Nome NO_GGX, gg1.REFERENCIA, ',
    'med.SIGLA, avi.Nome NO_AVALINI, avf.Nome NO_AVALFIM  ',
    'FROM loccpatuso lpu  ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpu.GraGruX  ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'LEFT JOIN unidmed med ON med.Codigo=lpu.Unidade ',
    'LEFT JOIN graglaval avi ON avi.Codigo=lpu.AvalIni ',
    'LEFT JOIN graglaval avf ON avf.Codigo=lpu.AvalFim ',
    'WHERE lpu.CtrID=' + Geral.FF0(CtrID),
    '']);
  //
  QueryLocCPatUso.Locate('Item', Item, []);
end;

procedure TFmLocCCon.ReopenLocFCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocFCab, Dmod.MyDB, [
  'SELECT lfc.* ',
  'FROM locfcab lfc ',
  'WHERE lfc.Codigo=' + Geral.FF0(QrLocCConCodigo.Value),
  '']);
  //
  QrLocFCab.Locate('Controle', Controle, []);
end;

procedure TFmLocCCon.ReopenLctFatRef(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLctFatRef, Dmod.MyDB, [
  'SELECT lfr.* ',
  'FROM lctfatref lfr ',
  'WHERE lfr.Controle=' + Geral.FF0(QrLocFCabControle.Value),
  'AND FatID IN(0, '+ Geral.FF0(VAR_FATID_3001) + ') ',
  '']);
  //
  QrLctFatRef.Locate('Conta', Conta, []);
end;

procedure TFmLocCCon.Retorna1Click(Sender: TObject);
var
  DataDef, DataSel, DataMin: TDateTime;
  DtHrRetorn: String;
  CtrID, RetFunci, UserSelect, RetExUsr, i: Integer;
begin
  if (QrLocCPatPri.State = dsInactive) and (QrLocCPatPri.RecordCount = 0) then Exit;
  //
  CtrID    := 0;
  DataDef  := Now();
  DataMin  := Trunc(QrLocCConDtHrEmi.Value);
  DataSel  := 0;
  RetExUsr := VAR_USUARIO;
  //
  if not DBCheck.ObtemDataEUser(VAR_USUARIO, UserSelect,
    DataDef, DataSel, dataMin, Time, True) then Exit;
  //
  DtHrRetorn := Geral.FDT(DataSel, 109);
  RetFunci   := UserSelect;
  //
  if DGDados.SelectedRows.Count > 1 then
  begin
    with DGDados.DataSource.DataSet do
    for i:= 0 to DGDados.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DGDados.SelectedRows.Items[i]));
      GotoBookmark(DGDados.SelectedRows.Items[i]);
      //
      CtrID := QrLocCPatPriCtrID.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccpatpri', False, [
      'RetFunci', 'DtHrRetorn', 'RetExUsr'], ['CtrID'], [
      RetFunci, DtHrRetorn, RetExUsr], [CtrID], True) then
      begin
        Dmod.VerificaSituacaoPatrimonio(QrLocCPatPriGraGruX.Value);
      end;
    end;
  end else
  begin
    CtrID := QrLocCPatPriCtrID.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccpatpri', False, [
    'RetFunci', 'DtHrRetorn', 'RetExUsr'], ['CtrID'], [
    RetFunci, DtHrRetorn, RetExUsr], [CtrID], True) then
    begin
      Dmod.VerificaSituacaoPatrimonio(QrLocCPatPriGraGruX.Value);
    end;
  end;
  ReopenLocCPatPri(QrLocCPatPri, QrLocCConCodigo.Value, CtrID);
end;

procedure TFmLocCCon.DBGLocCPatCnsDblClick(Sender: TObject);
var
  Campo: String;
begin
  if (QrLocCPatCns.State = dsInactive) and (QrLocCPatCns.RecordCount = 0) then
    Exit;
  //
  Campo := Uppercase(DBGLocCPatCns.Columns[THackDBGrid(DBGLocCPatCns).Col -1].FieldName);
  if (Uppercase(Campo) = Uppercase('QTDINI'))
  or (Uppercase(Campo) = Uppercase('QTDFIM')) then
    MostraFormLocCPatCns();
end;

procedure TFmLocCCon.DBGLocCPatCnsMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMLocCPatCns, DBGLocCPatCns, X,Y);
end;

procedure TFmLocCCon.DBGLocCPatUsoDblClick(Sender: TObject);
var
  Campo: String;
  Valor: Double;
begin
  if (QrLocCPatUso.State = dsInactive) or (QrLocCPatUso.RecordCount = 0) then
    Exit;
  //  
  Campo := Uppercase(DBGLocCPatUso.Columns[THackDBGrid(DBGLocCPatUso).Col -1].FieldName);
  //
  if Campo = Uppercase('NO_AVALINI') then DefineAvaliacao(
    avalIni, QrLocCPatUsoItem.Value, QrLocCPatUsoAvalIni.Value, True)
  else
  if Campo = Uppercase('NO_AVALFIM') then DefineAvaliacao(
    avalFim, QrLocCPatUsoItem.Value, QrLocCPatUsoAvalFim.Value, True)
  else
  if Campo = Uppercase('VALTOT') then
  begin
    if QrLocCPatUsoValTot.Value > 0 then
      Valor := QrLocCPatUsoValTot.Value
    else
      Valor := QrLocCPatUsoPrcUni.Value;
    if dmkPF.ObtemValorDouble(Valor, 2) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccpatuso', False, [
      'ValTot'], ['Item'], [Valor], [QrLocCPatUsoItem.Value], True) then
        ReopenLocCPatUso(QrLocCPatUso, QrLocCPatPriCtrID.Value, QrLocCPatUsoItem.Value);
    end;
  end
end;

procedure TFmLocCCon.DBGLocCPatUsoMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMLocCPatUso, DBGLocCPatUso, X,Y);
end;

procedure TFmLocCCon.DBGAcessoriosMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMAcessorios, DBGAcessorios, X,Y);
end;

procedure TFmLocCCon.DefineAvaliacao(Parte: TAvaliacao; Item, Atual: Integer;
  Reabre: Boolean);
const
  Aviso   = '...';
  Campo   = 'Descricao';
var
  //Avaliacao: Integer;
  C, P: String;
  AvalFld: String;
  Codigo: Integer;
begin
  case parte of
    avalIni: C := 'Avalia��o Inicial';
    avalFim: C := 'Avalia��o Final';
  end;
  case parte of
    avalIni: P := 'Informe a avalia��o inicial:';
    avalFim: P := 'Informe a avalia��o final:';
  end;
  case parte of
    avalIni: AvalFld := 'AvalIni';
    avalFim: AvalFld := 'AvalFim';
  end;
  {
  Application.CreateForm(TFm SelCod, Fm SelCod);
  Fm SelCod.Caption := C;
  Fm SelCod.LaPrompt.Caption := P;
  Fm SelCod.QrSel.Close;
  Fm SelCod.QrSel.SQL.Clear;
  Fm SelCod.QrSel.SQL.Add('SELECT Nome Descricao, Codigo');
  Fm SelCod.QrSel.SQL.Add('FROM graglaval');
  Fm SelCod.QrSel.SQL.Add('ORDER BY Descricao');
  Fm SelCod.QrSel.Open;
  //
  Fm SelCod.EdSel.ValueVariant := Atual;
  Fm SelCod.CBSel.KeyValue := Atual;
  //
  Fm SelCod.ShowModal;
  Fm SelCod.Destroy;
  if VAR_SELCOD > 0 then
  }
  Codigo := DBCheck.EscolheCodigoUnico(Aviso, C, P, nil, nil, Campo, 0, [
  'SELECT Codigo, Nome ' + Campo,
  'FROM graglaval ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, False);
  if Codigo <> 0 then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccpatuso', False, [
    AvalFld], ['Item'], [VAR_SELCOD], [Item], True) then
      if Reabre then
         ReopenLocCPatUso(QrLocCPatUso, QrLocCPatPriCtrID.Value, Item);
  end;
end;

procedure TFmLocCCon.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmLocCCon.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmLocCCon.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmLocCCon.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmLocCCon.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmLocCCon.SpeedButton5Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdCliente.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdCliente, CBCliente, QrClientes, VAR_CADASTRO, 'Codigo');
  EdCliente.SetFocus;
  //
  AtualizaImagens(EdCliente.ValueVariant, 0, ImgCliente);
  AtualizaImagens(EdECComprou.ValueVariant, 1, ImgRequisitante);
  AtualizaImagens(EdECRetirou.ValueVariant, 1, ImgRetirada);
end;

procedure TFmLocCCon.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCCon.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrLocCConCodigo.Value;
  //
  if TFmLocCCon(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmLocCCon.ItsInclui1Click(Sender: TObject);
begin
  MostraFormLocCPatPri(stIns);
end;

procedure TFmLocCCon.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrLocCCon, [PnDados],
  [PnEdita], EdCliente, ImgTipo, 'locccon');
end;

procedure TFmLocCCon.BtConfirmaClick(Sender: TObject);
var
  DtHrEmi, (*DtHrBxa,*) NumOC, LocalObra, Obs0, Obs1, Obs2, EndCobra,
  LocalCntat, LocalFone: String;
  Empresa, Codigo, Contrato, Cliente, Vendedor, ECComprou, ECRetirou: Integer;
  //ValorTot: Double;
  EhIns: Boolean;
begin
  Empresa        := EdEmpresa.ValueVariant;
  //
  Codigo         := EdCodigo.ValueVariant;
  Contrato       := EdContrato.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  DtHrEmi        := Geral.FDT(Trunc(TPDataEmi.Date), 1) + ' ' + EdHoraEmi.Text;
  //DtHrBxa        := ;
  Vendedor       := EdVendedor.ValueVariant;
  ECComprou      := EdECComprou.ValueVariant;
  ECRetirou      := EdECRetirou.ValueVariant;
  NumOC          := EdNumOC.Text;
  EndCobra       := EdEndCobra.Text;
  LocalObra      := EdLocalObra.Text;
  LocalCntat     := EdLocalCntat.Text;
  LocalFone      := EdLocalFone.Text;
  Obs0           := EdObs0.Text;
  Obs1           := EdObs1.Text;
  Obs2           := EdObs2.Text;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa!') then
    Exit;
{
  if not UMyMod.ObtemCodigoDeCodUsu(EdEmpresa, Empresa, 'Informe a empresa!',
    'Codigo', 'Filial') then Exit;
}
  //
  if MyObjects.FIC(Contrato = 0, EdContrato, 'Informe o contrato!') then
    Exit;
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Informe o cliente!') then
    Exit;
  if MyObjects.FIC(Vendedor = 0, EdVendedor, 'Informe o vendedor!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('locccon', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'locccon', False, [
  'Contrato', 'Cliente', 'DtHrEmi',
  'Empresa', 'Vendedor', 'ECComprou',
  'ECRetirou', 'NumOC', 'EndCobra',
  'LocalObra', 'LocalCntat', 'LocalFone',
  'Obs0', 'Obs1', 'Obs2'], [
  'Codigo'], [
  Contrato, Cliente, DtHrEmi,
  VUEmpresa.ValueVariant, Vendedor, ECComprou,
  ECRetirou, NumOC, EndCobra,
  LocalObra, LocalCntat, LocalFone,
  Obs0, Obs1, Obs2], [
  Codigo], True) then
  begin
    EhIns := ImgTipo.SQLType = stIns;
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    LocCod(Codigo, Codigo);
    if Codigo = QrLocCConCodigo.Value then  // Evitar engano
      if EhIns then
        MostraFormLocCPatPri(stIns);
    GOTOy.BotoesSb(ImgTipo.SQLType);
  end;
end;

procedure TFmLocCCon.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'locccon', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'locccon', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType, TFmLocCCon(Self));
end;

procedure TFmLocCCon.BtFatClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  MyObjects.MostraPopUpDeBotao(PMFat, BtFat);
end;

procedure TFmLocCCon.BtItsClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmLocCCon.BtNFSeClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(False, nil, nil);
end;

procedure TFmLocCCon.Adicionaacessrio1Click(Sender: TObject);
begin
  MostraLocCMateri(QrLocCPatPriCodigo.Value, QrLocCPatPriCtrID.Value, 1); //Acess�rio
end;

procedure TFmLocCCon.Adicionamaterialdeconsumo1Click(Sender: TObject);
begin
  MostraLocCMateri(QrLocCPatPriCodigo.Value, QrLocCPatPriCtrID.Value, 3); //Material de consumo
end;

procedure TFmLocCCon.Adicionamaterialuso1Click(Sender: TObject);
begin
  MostraLocCMateri(QrLocCPatPriCodigo.Value, QrLocCPatPriCtrID.Value, 2); //Material uso
end;

procedure TFmLocCCon.AtualizaImagens(Codigo, TipImg: Integer; Image: TImage);
var
  Img: String;
begin
  if Codigo <> 0 then
  begin
    Img := DModG.ObtemCaminhoImagem(TipImg, Codigo);
    //
    if FileExists(Img) then
    begin
      Image.Picture.LoadFromFile(Img);
      Image.Visible := True;
    end;
  end else
    Image.Visible := False;
end;

procedure TFmLocCCon.BtCabClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmLocCCon.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType    := stLok;
  GBEdita.Align      := alClient;
  DGDados.Align      := alClient;
  PageControl1.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  PageControl1.ActivePageIndex := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrClientes, Dmod.MyDB, [
  'SELECT Codigo, IF(Tipo=0, RazaoSocial, ',
  'Nome) NOMEENTIDADE ',
  'FROM entidades ',
  'WHERE Ativo = 1',
  'ORDER BY NOMEENTIDADE ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSenhas, Dmod.MyDB, [
  'SELECT Numero, Login ',
  'FROM senhas ',
  'WHERE Ativo=1 ',
  'ORDER BY Login ',
  '']);
  //
  {
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartaG, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM cartag ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  }
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratos, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM contratos ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmLocCCon.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrLocCConCodigo.Value, LaRegistro.Caption);
end;

procedure TFmLocCCon.SbImagensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImagens, SbImagens);
end;

procedure TFmLocCCon.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmLocCCon.SbNomeClick(Sender: TObject);
begin
  PesquisaPorCliente();
end;

procedure TFmLocCCon.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrLocCConCodigo.Value, LaRegistro.Caption);
end;

procedure TFmLocCCon.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmLocCCon.QrLocCConAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmLocCCon.QrLocCConAfterScroll(DataSet: TDataSet);
begin
  ReopenLocCPatPri(QrLocCPatPri, QrLocCConCodigo.Value, 0);
  ReopenLocFCab(0);
end;

procedure TFmLocCCon.Faturaeencerracontrato1Click(Sender: TObject);
begin
  MostraFormLocFCab(True);
end;

procedure TFmLocCCon.Faturamentoparcial1Click(Sender: TObject);
begin
  MostraFormLocFCab(False);
end;

procedure TFmLocCCon.FormActivate(Sender: TObject);
begin
  if TFmLocCCon(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end else
  begin
    // Pode ter mudado para DefParams de outo form em Aba!
    DefParams();
  end;
  //
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrLocCConCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmLocCCon.SbQueryClick(Sender: TObject);
begin
  PesquisaPorCliente();
end;

procedure TFmLocCCon.Selecionados1Click(Sender: TObject);
begin
  ImprimeDevolucao(False);
end;

procedure TFmLocCCon.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCCon.FormShow(Sender: TObject);
begin
  (* 2022-07-16 FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);*)
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
end;

procedure TFmLocCCon.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrLocCCon, [PnDados],
  [PnEdita], EdCliente, ImgTipo, 'locccon');
  //
  EdEmpresa.ValueVariant  := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue      := DmodG.QrFiliLogFilial.Value;
  EdContrato.ValueVariant := Dmod.QrOpcoesTRenDefContrat.Value;
  CBContrato.KeyValue := Dmod.QrOpcoesTRenDefContrat.Value;
  TPDataEmi.Date := Trunc(Date);
  EdHoraEmi.Text := FormatDateTime('hh:nn:ss', Now());
  EdVendedor.ValueVariant := VAR_USUARIO;
  CBVendedor.KeyValue := VAR_USUARIO;
  //
  ImgCliente.Visible      := False;
  ImgRequisitante.Visible := False;
  ImgRetirada.Visible     := False;
end;

procedure TFmLocCCon.Contrato1Click(Sender: TObject);
begin
  Application.CreateForm(TFmContratApp, FmContratApp);
  //
  FmContratApp.FLocCCon := QrLocCConCodigo.Value;
  //
  FmContratApp.Imprime();
  //
  FmContratApp.Destroy;
end;

procedure TFmLocCCon.QrLocCConBeforeClose(DataSet: TDataSet);
begin
  QrLocCPatPri.Close;
  QrLocFCab.Close;
end;

procedure TFmLocCCon.QrLocCConBeforeOpen(DataSet: TDataSet);
begin
  QrLocCConCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmLocCCon.QrLocCConCalcFields(DataSet: TDataSet);
begin
  QrLocCConDataEmi.Value := Trunc(QrLocCConDtHrEmi.Value);
  QrLocCConHoraEmi.Value := QrLocCConDtHrEmi.Value - QrLocCConDataEmi.Value;
  QrLocCConDtHrBxa_TXT.Value :=  Geral.FDT(QrLocCConDtHrBxa.Value, 107);
end;

procedure TFmLocCCon.QrLocCPatCnsCalcFields(DataSet: TDataSet);
begin
  QrLocCPatCnsQTDUSO.Value :=
    QrLocCPatCnsQtdFim.Value - QrLocCPatCnsQtdIni.Value;
end;

procedure TFmLocCCon.QrLocCPatPriAfterScroll(DataSet: TDataSet);
begin
  ReopenLocCPatAce(QrLocCPatAce, QrLocCPatPriCtrID.Value, 0);
  ReopenLocCPatCns(QrLocCPatCns, QrLocCPatPriCtrID.Value, 0);
  ReopenLocCPatSec(0);
  ReopenLocCPatUso(QrLocCPatUso, QrLocCPatPriCtrID.Value, 0);
end;

procedure TFmLocCCon.QrLocCPatPriBeforeClose(DataSet: TDataSet);
begin
  QrLocCPatAce.Close;
  QrLocCPatCns.Close;
  QrLocCPatSec.Close;
  QrLocCPatUso.Close;
end;

procedure TFmLocCCon.QrLocCPatPriCalcFields(DataSet: TDataSet);
begin
  QrLocCPatPriTXT_DTA_DEVOL.Value := Geral.FDT(QrLocCPatPriDtHrRetorn.Value, 107);
  QrLocCPatPriTXT_DTA_LIBER.Value := Geral.FDT(QrLocCPatPriLibDtHr.Value, 107);
  QrLocCPatPriTXT_QEM_LIBER.Value := Trim(QrLocCPatPriLOGIN.Value + ' ' +
    QrLocCPatPriRELIB.Value + ' ' +QrLocCPatPriHOLIB.Value);
end;

procedure TFmLocCCon.QrLocFCabAfterScroll(DataSet: TDataSet);
begin
  ReopenLctFatRef(0);
end;

procedure TFmLocCCon.QrLocFCabBeforeClose(DataSet: TDataSet);
begin
  QrLctFatRef.Close;
end;

procedure TFmLocCCon.QrLctFatRefCalcFields(DataSet: TDataSet);
begin
  QrLctFatRefPARCELA.value := QrLctFatRef.RecNo;
end;

end.

