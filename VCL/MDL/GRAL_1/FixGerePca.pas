unit FixGerePca;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFixGerePca = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdControle: TDBEdit;
    DBEdNO_EQUI: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    EdDescri: TdmkEdit;
    Label7: TLabel;
    CBCodItem: TdmkDBLookupComboBox;
    EdCodItem: TdmkEditCB;
    Label1: TLabel;
    SbCodItem: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrFixPecaCad: TmySQLQuery;
    DsFixPecaCad: TDataSource;
    EdPreco: TdmkEdit;
    Label2: TLabel;
    EdDesco: TdmkEdit;
    Label8: TLabel;
    EdCusto: TdmkEdit;
    Label9: TLabel;
    EdValor: TdmkEdit;
    Label11: TLabel;
    EdQtde: TdmkEdit;
    Label10: TLabel;
    QrFixPecaCadCodigo: TIntegerField;
    QrFixPecaCadNome: TWideStringField;
    QrFixPecaCadPreco: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdCodItemChange(Sender: TObject);
    procedure EdQtdeChange(Sender: TObject);
    procedure EdPrecoChange(Sender: TObject);
    procedure EdDescoChange(Sender: TObject);
    procedure SbCodItemClick(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaCusto();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab, FDsEqu: TDataSource;
  end;

  var
  FmFixGerePca: TFmFixGerePca;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  Principal, FixGereCab;

{$R *.DFM}

procedure TFmFixGerePca.BtOKClick(Sender: TObject);
var
  Descri: String;
  Codigo, Controle, Conta, CodItem: Integer;
  Qtde, Preco, Desco, Custo, Valor: Double;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := Geral.IMV(DBEdControle.Text);
  Conta          := EdConta.ValueVariant;
  CodItem        := EdCodItem.ValueVariant;
  Qtde           := EdQtde.ValueVariant;
  Preco          := EdPreco.ValueVariant;
  Desco          := EdDesco.ValueVariant;
  Custo          := EdCusto.ValueVariant;
  Valor          := EdValor.ValueVariant;
  Descri         := EdDescri.Text;
  //
  if MyObjects.FIC(CodItem = 0, EdCodItem, 'Informe o item!') then Exit;
  if MyObjects.FIC(Qtde = 0, EdQtde, 'Informe a quantidade!') then Exit;
  //
  Conta := UMyMod.BPGS1I32('fixgerepca', 'Conta', '', '', tsPos,
    ImgTipo.SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'fixgerepca', False, [
  'Codigo', 'Controle', 'CodItem',
  'Qtde', 'Preco', 'Desco',
  'Custo', 'Valor', 'Descri'], [
  'Conta'], [
  Codigo, Controle, CodItem,
  Qtde, Preco, Desco,
  Custo, Valor, Descri], [
  Conta], True) then
  begin
    FmFixGereCab.CalculaCustosConsertoEQ(Controle);
    VAR_CADASTRO := Controle;
    //
    if CkContinuar.Checked then
    begin
      Geral.MB_Info('Dados salvos com sucesso!');
      //
      ImgTipo.SQLType          := stIns;
      EdConta.ValueVariant     := 0;
      EdCodItem.ValueVariant   := 0;
      CBCodItem.KeyValue       := Null;
      EdDescri.ValueVariant    := '';
      EdQtde.ValueVariant      := 0;
      EdPreco.ValueVariant     := 0;
      EdDesco.ValueVariant     := 0;
      //
      EdCodItem.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmFixGerePca.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Close;
end;

procedure TFmFixGerePca.CalculaCusto();
var
  Qtde, Preco, Desco, Custo, Valor: Double;
begin
  Qtde   := EdQtde.ValueVariant;
  Preco  := EdPreco.ValueVariant;
  Desco  := EdDesco.ValueVariant;
  //
  Custo  := Preco - Desco;
  Valor  := Qtde * Custo;
  //
  EdCusto.ValueVariant := Custo;
  EdValor.ValueVariant := Valor;
end;

procedure TFmFixGerePca.EdCodItemChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdPreco.ValueVariant := QrFixPecaCadPreco.Value;
  end;
end;

procedure TFmFixGerePca.EdDescoChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmFixGerePca.EdPrecoChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmFixGerePca.EdQtdeChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmFixGerePca.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource   := FDsCab;
  DBEdControle.DataSource := FDsEqu;
  DBEdNO_EQUI.DataSource  := FDsEqu;
  MyObjects.CorIniComponente();
end;

procedure TFmFixGerePca.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrFixPecaCad, Dmod.MyDB);
end;

procedure TFmFixGerePca.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFixGerePca.SbCodItemClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.MostraFormFixPecaCad(EdCodItem.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrFixPecaCad, Dmod.MyDB);    
    //
    EdCodItem.ValueVariant := VAR_CADASTRO;
    CBCodItem.KeyValue     := VAR_CADASTRO;
    EdCodItem.SetFocus;
  end;
end;

end.
