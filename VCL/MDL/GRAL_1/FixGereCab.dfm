object FmFixGereCab: TFmFixGereCab
  Left = 368
  Top = 194
  Caption = 'FIX-GEREN-001 :: Conserto - Ordem de Servi'#231'o'
  ClientHeight = 579
  ClientWidth = 993
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 94
    Width = 993
    Height = 485
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 993
      Height = 178
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 20
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 130
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label3: TLabel
        Left = 134
        Top = 43
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label4: TLabel
        Left = 8
        Top = 67
        Width = 184
        Height = 13
        Caption = 'Observa'#231#245'es (m'#225'ximo 255 caracteres):'
        Color = clBtnFace
        ParentColor = False
      end
      object SpeedButton5: TSpeedButton
        Left = 956
        Top = 39
        Width = 21
        Height = 21
        Hint = 'Inclui item de carteira'
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 47
        Top = 16
        Width = 79
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 176
        Top = 16
        Width = 43
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 221
        Top = 16
        Width = 756
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCliente: TdmkEditCB
        Left = 176
        Top = 39
        Width = 43
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 221
        Top = 39
        Width = 733
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientes
        TabOrder = 4
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object MeObservacao: TdmkMemo
        Left = 8
        Top = 82
        Width = 970
        Height = 80
        MaxLength = 255
        TabOrder = 5
        QryCampo = 'Observacao'
        UpdCampo = 'Observacao'
        UpdType = utYes
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 422
      Width = 993
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 855
        Top = 14
        Width = 136
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 118
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 94
    Width = 993
    Height = 485
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 422
      Width = 993
      Height = 63
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 14
        Width = 169
        Height = 48
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 126
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 47
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 171
        Top = 14
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 328
        Top = 14
        Width = 663
        Height = 48
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 532
          Top = 0
          Width = 131
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 118
            Height = 39
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 10090
          Left = 4
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&O.S.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtEqu: TBitBtn
          Tag = 497
          Left = 126
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Equip.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtEquClick
        end
        object BtSrv: TBitBtn
          Tag = 530
          Left = 370
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Servi'#231'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtSrvClick
        end
        object BtPca: TBitBtn
          Tag = 498
          Left = 248
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Pe'#231'a'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtPcaClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 88
      Width = 993
      Height = 173
      Align = alTop
      DataSource = DsFixGereEqu
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID conserto'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtHrRece'
          Title.Caption = 'Recebido'
          Width = 77
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'ID Equip.'
          Width = 35
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Tabela'
          Title.Caption = 'D'
          Width = 10
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_EQUI'
          Title.Caption = 'Nome equipamento'
          Width = 208
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DefeitoTxt'
          Title.Caption = 'Defeito relatado'
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoPeca'
          Title.Caption = '$ Pe'#231'as'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoServ'
          Title.Caption = '$ Servi'#231'os'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoTota'
          Title.Caption = '$ Total'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TXT_DtHrLibe'
          Title.Caption = 'Liberado'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_LIBE'
          Title.Caption = 'Quem liberou'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_RECE'
          Title.Caption = 'Quem Recebeu'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TABELA_TXT'
          Title.Caption = 'Origem'
          Width = 120
          Visible = True
        end>
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 993
      Height = 88
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object Label5: TLabel
        Left = 8
        Top = 20
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 130
        Top = 20
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = 'Empresa:'
      end
      object Label8: TLabel
        Left = 137
        Top = 43
        Width = 35
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cliente:'
      end
      object Label10: TLabel
        Left = 108
        Top = 67
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'Observa'#231#245'es:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label1: TLabel
        Left = 832
        Top = 20
        Width = 42
        Height = 13
        Alignment = taRightJustify
        Caption = '$ Pe'#231'as:'
      end
      object Label2: TLabel
        Left = 824
        Top = 43
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = '$Servi'#231'os:'
      end
      object Label11: TLabel
        Left = 829
        Top = 67
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = '$ Total:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object DBEdit1: TDBEdit
        Left = 47
        Top = 16
        Width = 80
        Height = 21
        DataField = 'Codigo'
        DataSource = DsFixGereCab
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 178
        Top = 16
        Width = 40
        Height = 21
        DataField = 'Empresa'
        DataSource = DsFixGereCab
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 221
        Top = 16
        Width = 571
        Height = 21
        DataField = 'NO_EMP'
        DataSource = DsFixGereCab
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 178
        Top = 39
        Width = 40
        Height = 21
        DataField = 'Cliente'
        DataSource = DsFixGereCab
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 221
        Top = 39
        Width = 571
        Height = 21
        DataField = 'NO_CLI'
        DataSource = DsFixGereCab
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 178
        Top = 63
        Width = 614
        Height = 21
        DataField = 'Observacao'
        DataSource = DsFixGereCab
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 878
        Top = 16
        Width = 103
        Height = 21
        DataField = 'CustoPeca'
        DataSource = DsFixGereCab
        TabOrder = 6
      end
      object DBEdit8: TDBEdit
        Left = 878
        Top = 39
        Width = 103
        Height = 21
        DataField = 'CustoServ'
        DataSource = DsFixGereCab
        TabOrder = 7
      end
      object DBEdit9: TDBEdit
        Left = 878
        Top = 63
        Width = 103
        Height = 21
        DataField = 'CustoTota'
        DataSource = DsFixGereCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
      end
    end
    object GBPecServ: TGroupBox
      Left = 0
      Top = 261
      Width = 993
      Height = 103
      Align = alTop
      Caption = 'Despesas com pe'#231'as e servi'#231'os:'
      TabOrder = 3
      object DBGrid1: TDBGrid
        Left = 2
        Top = 14
        Width = 494
        Height = 88
        Align = alLeft
        DataSource = DsFixGerePca
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Conta'
            Title.Caption = 'ID pe'#231'a'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodItem'
            Title.Caption = 'C'#243'digo'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_ITEM'
            Title.Caption = 'Nome'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descri'
            Title.Caption = 'Complemento'
            Width = 61
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Caption = 'Quantidade'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Custo'
            Title.Caption = 'Custo un.'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Caption = 'Valor itens'
            Width = 48
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 496
        Top = 14
        Width = 495
        Height = 88
        Align = alClient
        DataSource = DsFixGereSrv
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Conta'
            Title.Caption = 'ID pe'#231'a'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodItem'
            Title.Caption = 'C'#243'digo'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_ITEM'
            Title.Caption = 'Nome'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descri'
            Title.Caption = 'Complemento'
            Width = 61
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Caption = 'Quantidade'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Custo'
            Title.Caption = 'Custo un.'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Caption = 'Valor itens'
            Width = 48
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 51
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 213
      Height = 51
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 213
      Top = 0
      Width = 733
      Height = 51
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 336
        Height = 31
        Caption = 'Conserto - Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 336
        Height = 31
        Caption = 'Conserto - Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 336
        Height = 31
        Caption = 'Conserto - Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 993
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 989
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 116
    Top = 52
  end
  object QrFixGereCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFixGereCabBeforeOpen
    AfterOpen = QrFixGereCabAfterOpen
    BeforeClose = QrFixGereCabBeforeClose
    AfterScroll = QrFixGereCabAfterScroll
    SQL.Strings = (
      'SELECT '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI,'
      'fgc.*'
      'FROM fixgerecab fgc'
      'LEFT JOIN entidades emp ON emp.Codigo=fgc.Empresa'
      'LEFT JOIN entidades cli ON cli.Codigo=fgc.Cliente')
    Left = 60
    Top = 53
    object QrFixGereCabNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrFixGereCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrFixGereCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fixgerecab.Codigo'
    end
    object QrFixGereCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'fixgerecab.Empresa'
    end
    object QrFixGereCabCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'fixgerecab.Cliente'
    end
    object QrFixGereCabObservacao: TWideStringField
      FieldName = 'Observacao'
      Origin = 'fixgerecab.Observacao'
      Size = 255
    end
    object QrFixGereCabCustoTota: TFloatField
      FieldName = 'CustoTota'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFixGereCabCustoServ: TFloatField
      FieldName = 'CustoServ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFixGereCabCustoPeca: TFloatField
      FieldName = 'CustoPeca'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsFixGereCab: TDataSource
    DataSet = QrFixGereCab
    Left = 88
    Top = 53
  end
  object QrFixGereEqu: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFixGereEquBeforeClose
    AfterScroll = QrFixGereEquAfterScroll
    OnCalcFields = QrFixGereEquCalcFields
    SQL.Strings = (
      'SELECT ELT(Tabela, "E", "T", "?") SIGLA_TAB, '
      'qrc.Login NO_RECE,'
      'qrs.Login NO_RESP,'
      'qlb.Login NO_LIBE,'
      ''
      'if(fge.Tabela=1, gg1.Nome, gxp.DescrTerc) NO_EQUI,'
      ''
      'CASE fge.Tabela'
      'WHEN 1 THEN "Patrim'#244'nio pr'#243'prio"'
      'WHEN 2 THEN "Equipamento de terceiro"'
      'ELSE "???" END TABELA_TXT,'
      ''
      'fge.*'
      ''
      'FROM fixgereequ fge'
      'LEFT JOIN senhas qrc ON qrc.Numero=fge.QuemRece'
      'LEFT JOIN senhas qrs ON qrs.Numero=fge.QuemResp'
      'LEFT JOIN senhas qlb ON qlb.Numero=fge.QuemLibe'
      ''
      'LEFT JOIN gragrux ggx ON ggx.Controle=fge.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      ''
      'LEFT JOIN fixgxpatr gxp ON gxp.GraGrux=fge.GraGruX')
    Left = 144
    Top = 53
    object QrFixGereEquNO_RECE: TWideStringField
      FieldName = 'NO_RECE'
      Size = 100
    end
    object QrFixGereEquNO_RESP: TWideStringField
      FieldName = 'NO_RESP'
      Size = 100
    end
    object QrFixGereEquNO_LIBE: TWideStringField
      FieldName = 'NO_LIBE'
      Size = 100
    end
    object QrFixGereEquNO_EQUI: TWideStringField
      FieldName = 'NO_EQUI'
      Size = 120
    end
    object QrFixGereEquCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFixGereEquControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFixGereEquTabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrFixGereEquGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrFixGereEquQuemRece: TIntegerField
      FieldName = 'QuemRece'
    end
    object QrFixGereEquQuemResp: TIntegerField
      FieldName = 'QuemResp'
    end
    object QrFixGereEquQuemLibe: TIntegerField
      FieldName = 'QuemLibe'
    end
    object QrFixGereEquDtHrRece: TDateTimeField
      FieldName = 'DtHrRece'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFixGereEquDtHrLibe: TDateTimeField
      FieldName = 'DtHrLibe'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFixGereEquCustoPeca: TFloatField
      FieldName = 'CustoPeca'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFixGereEquCustoServ: TFloatField
      FieldName = 'CustoServ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFixGereEquCustoTota: TFloatField
      FieldName = 'CustoTota'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFixGereEquDefeitoTxt: TWideStringField
      FieldName = 'DefeitoTxt'
      Size = 60
    end
    object QrFixGereEquSIGLA_TAB: TWideStringField
      FieldName = 'SIGLA_TAB'
      Size = 1
    end
    object QrFixGereEquTXT_DtHrLibe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DtHrLibe'
      Size = 30
      Calculated = True
    end
    object QrFixGereEquTABELA_TXT: TWideStringField
      FieldName = 'TABELA_TXT'
      Size = 30
    end
  end
  object DsFixGereEqu: TDataSource
    DataSet = QrFixGereEqu
    Left = 172
    Top = 53
  end
  object PMEqu: TPopupMenu
    OnPopup = PMEquPopup
    Left = 508
    Top = 496
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 384
    Top = 496
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Ativo = 1'
      'ORDER BY NOMEENTIDADE')
    Left = 404
    Top = 68
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 432
    Top = 68
  end
  object PMPca: TPopupMenu
    OnPopup = PMPcaPopup
    Left = 628
    Top = 492
    object ItsInclui2: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui2Click
    end
    object ItsAltera2: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera2Click
    end
    object ItsExclui2: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui2Click
    end
  end
  object QrFixGerePca: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fpc.Nome NO_ITEM, fgp.*'
      'FROM fixgerepca fgp'
      'LEFT JOIN fixpecacad fpc ON fpc.Codigo=fgp.CodItem'
      'WHERE fgp.Controle=0')
    Left = 200
    Top = 53
    object QrFixGerePcaNO_ITEM: TWideStringField
      FieldName = 'NO_ITEM'
      Size = 60
    end
    object QrFixGerePcaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFixGerePcaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFixGerePcaConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFixGerePcaCodItem: TIntegerField
      FieldName = 'CodItem'
    end
    object QrFixGerePcaQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrFixGerePcaPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGerePcaDesco: TFloatField
      FieldName = 'Desco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGerePcaCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGerePcaValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGerePcaDescri: TWideStringField
      FieldName = 'Descri'
      Size = 60
    end
  end
  object DsFixGerePca: TDataSource
    DataSet = QrFixGerePca
    Left = 228
    Top = 53
  end
  object QrFixGereSrv: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fpc.Nome NO_ITEM, fgp.*'
      'FROM fixgerepca fgp'
      'LEFT JOIN fixpecacad fpc ON fpc.Codigo=fgp.CodItem'
      'WHERE fgp.Controle=0')
    Left = 256
    Top = 53
    object QrFixGereSrvNO_ITEM: TWideStringField
      FieldName = 'NO_ITEM'
      Size = 60
    end
    object QrFixGereSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFixGereSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFixGereSrvConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFixGereSrvCodItem: TIntegerField
      FieldName = 'CodItem'
    end
    object QrFixGereSrvQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrFixGereSrvPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGereSrvDesco: TFloatField
      FieldName = 'Desco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGereSrvCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGereSrvValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGereSrvDescri: TWideStringField
      FieldName = 'Descri'
      Size = 60
    end
  end
  object DsFixGereSrv: TDataSource
    DataSet = QrFixGereSrv
    Left = 284
    Top = 53
  end
  object PMSrv: TPopupMenu
    OnPopup = PMSrvPopup
    Left = 748
    Top = 496
    object ItsInclui3: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui3Click
    end
    object ItsAltera3: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera3Click
    end
    object ItsExclui3: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui3Click
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 380
    Top = 243
  end
end
