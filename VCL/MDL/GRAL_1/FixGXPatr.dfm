object FmFixGXPatr: TFmFixGXPatr
  Left = 368
  Top = 194
  Caption = 'FIX-CADAS-003 :: Cadastro de Equipamentos de Terceiros'
  ClientHeight = 632
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 536
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 85
      Align = alTop
      Caption = ' Identifica'#231#227'o: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label6: TLabel
        Left = 320
        Top = 60
        Width = 33
        Height = 13
        Caption = 'Marca:'
        FocusControl = DBEdit4
      end
      object Label8: TLabel
        Left = 524
        Top = 60
        Width = 53
        Height = 13
        Caption = 'Fabricante:'
        FocusControl = DBEdit5
      end
      object Label3: TLabel
        Left = 16
        Top = 60
        Width = 67
        Height = 13
        Caption = 'Complemento:'
        FocusControl = DBEdit1
      end
      object Label25: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label46: TLabel
        Left = 88
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = dmkDBEdit1
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 65
        Height = 21
        TabStop = False
        DataField = 'CONTROLE'
        DataSource = DsFixGXPatr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit4: TDBEdit
        Left = 356
        Top = 56
        Width = 164
        Height = 21
        DataField = 'NO_MARCA'
        DataSource = DsFixGXPatr
        TabOrder = 1
      end
      object DBEdit5: TDBEdit
        Left = 580
        Top = 56
        Width = 193
        Height = 21
        DataField = 'NO_FABR'
        DataSource = DsFixGXPatr
        TabOrder = 2
      end
      object DBEdit1: TDBEdit
        Left = 88
        Top = 56
        Width = 229
        Height = 21
        DataField = 'Complem'
        DataSource = DsFixGXPatr
        TabOrder = 3
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 88
        Top = 32
        Width = 685
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NO_GG1'
        DataSource = DsFixGXPatr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 472
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtCab: TBitBtn
          Tag = 497
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Patrim'#244'nio'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object Panel2: TPanel
          Left = 386
          Top = 0
          Width = 135
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 149
      Width = 1008
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 408
        Height = 64
        Align = alLeft
        Caption = ' Valores atuais: '
        TabOrder = 0
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 404
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label17: TLabel
            Left = 332
            Top = 0
            Width = 19
            Height = 13
            Caption = 'Dia:'
            FocusControl = DBEdit12
          end
          object Label16: TLabel
            Left = 260
            Top = 0
            Width = 42
            Height = 13
            Caption = 'Semana:'
            FocusControl = DBEdit11
          end
          object Label15: TLabel
            Left = 188
            Top = 0
            Width = 48
            Height = 13
            Caption = 'Quinzena:'
            FocusControl = DBEdit10
          end
          object Label14: TLabel
            Left = 116
            Top = 0
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
            FocusControl = DBEdit9
          end
          object Label13: TLabel
            Left = 12
            Top = 0
            Width = 37
            Height = 13
            Caption = 'Venda: '
            FocusControl = DBEdit8
          end
          object DBEdit8: TDBEdit
            Left = 12
            Top = 16
            Width = 100
            Height = 21
            DataField = 'AtualValr'
            DataSource = DsFixGXPatr
            TabOrder = 0
          end
          object DBEdit12: TDBEdit
            Left = 332
            Top = 16
            Width = 68
            Height = 21
            DataField = 'ValorDia'
            DataSource = DsFixGXPatr
            TabOrder = 1
          end
          object DBEdit11: TDBEdit
            Left = 260
            Top = 16
            Width = 68
            Height = 21
            DataField = 'ValorSem'
            DataSource = DsFixGXPatr
            TabOrder = 2
          end
          object DBEdit10: TDBEdit
            Left = 188
            Top = 16
            Width = 68
            Height = 21
            DataField = 'ValorQui'
            DataSource = DsFixGXPatr
            TabOrder = 3
          end
          object DBEdit9: TDBEdit
            Left = 116
            Top = 16
            Width = 68
            Height = 21
            DataField = 'ValorMes'
            DataSource = DsFixGXPatr
            TabOrder = 4
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 408
        Top = 0
        Width = 600
        Height = 64
        Align = alClient
        Caption = 'Caracter'#237'sticas: '
        TabOrder = 1
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 596
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label19: TLabel
            Left = 72
            Top = 0
            Width = 38
            Height = 13
            Caption = 'Modelo:'
            FocusControl = DBEdit14
          end
          object Label20: TLabel
            Left = 196
            Top = 0
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
            FocusControl = DBEdit15
          end
          object Label21: TLabel
            Left = 320
            Top = 0
            Width = 47
            Height = 13
            Caption = 'Voltagem:'
            FocusControl = DBEdit16
          end
          object Label22: TLabel
            Left = 396
            Top = 0
            Width = 45
            Height = 13
            Caption = 'Pot'#234'ncia:'
            FocusControl = DBEdit17
          end
          object Label23: TLabel
            Left = 468
            Top = 0
            Width = 60
            Height = 13
            Caption = 'Capacidade:'
            FocusControl = DBEdit18
          end
          object DBEdit14: TDBEdit
            Left = 72
            Top = 16
            Width = 120
            Height = 21
            DataField = 'Modelo'
            DataSource = DsFixGXPatr
            TabOrder = 0
          end
          object DBEdit15: TDBEdit
            Left = 196
            Top = 16
            Width = 120
            Height = 21
            DataField = 'Serie'
            DataSource = DsFixGXPatr
            TabOrder = 1
          end
          object DBEdit16: TDBEdit
            Left = 320
            Top = 16
            Width = 73
            Height = 21
            DataField = 'Voltagem'
            DataSource = DsFixGXPatr
            TabOrder = 2
          end
          object DBEdit17: TDBEdit
            Left = 396
            Top = 16
            Width = 69
            Height = 21
            DataField = 'Potencia'
            DataSource = DsFixGXPatr
            TabOrder = 3
          end
          object DBEdit18: TDBEdit
            Left = 468
            Top = 16
            Width = 120
            Height = 21
            DataField = 'Capacid'
            DataSource = DsFixGXPatr
            TabOrder = 4
          end
        end
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 85
      Width = 1008
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 457
        Height = 64
        Align = alLeft
        Caption = ' Aquisi'#231#227'o:  '
        TabOrder = 0
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 453
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label5: TLabel
            Left = 12
            Top = 0
            Width = 26
            Height = 13
            Caption = 'Data:'
            FocusControl = DBEdit2
          end
          object Label10: TLabel
            Left = 72
            Top = 0
            Width = 58
            Height = 13
            Caption = 'Documento:'
            FocusControl = DBEdit3
          end
          object Label11: TLabel
            Left = 348
            Top = 0
            Width = 27
            Height = 13
            Caption = 'Valor:'
            FocusControl = DBEdit6
          end
          object DBEdit2: TDBEdit
            Left = 12
            Top = 16
            Width = 56
            Height = 21
            DataField = 'AquisData'
            DataSource = DsFixGXPatr
            TabOrder = 0
          end
          object DBEdit3: TDBEdit
            Left = 72
            Top = 16
            Width = 272
            Height = 21
            DataField = 'AquisDocu'
            DataSource = DsFixGXPatr
            TabOrder = 1
          end
          object DBEdit6: TDBEdit
            Left = 348
            Top = 16
            Width = 100
            Height = 21
            DataField = 'AquisValr'
            DataSource = DsFixGXPatr
            TabOrder = 2
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 457
        Top = 0
        Width = 551
        Height = 64
        Align = alClient
        Caption = ' Venda:  '
        TabOrder = 1
        object Panel11: TPanel
          Left = 2
          Top = 15
          Width = 547
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label32: TLabel
            Left = 12
            Top = 0
            Width = 26
            Height = 13
            Caption = 'Data:'
            FocusControl = DBEdit27
          end
          object Label33: TLabel
            Left = 72
            Top = 0
            Width = 58
            Height = 13
            Caption = 'Documento:'
            FocusControl = DBEdit28
          end
          object Label34: TLabel
            Left = 348
            Top = 0
            Width = 27
            Height = 13
            Caption = 'Valor:'
            FocusControl = DBEdit29
          end
          object Label27: TLabel
            Left = 452
            Top = 0
            Width = 35
            Height = 13
            Caption = 'Cliente:'
            FocusControl = DBEdit22
          end
          object DBEdit27: TDBEdit
            Left = 12
            Top = 16
            Width = 56
            Height = 21
            DataField = 'VENDADATA_TXT'
            DataSource = DsFixGXPatr
            TabOrder = 0
          end
          object DBEdit28: TDBEdit
            Left = 72
            Top = 16
            Width = 272
            Height = 21
            DataField = 'VendaDocu'
            DataSource = DsFixGXPatr
            TabOrder = 1
          end
          object DBEdit29: TDBEdit
            Left = 348
            Top = 16
            Width = 100
            Height = 21
            DataField = 'VendaValr'
            DataSource = DsFixGXPatr
            TabOrder = 2
          end
          object DBEdit22: TDBEdit
            Left = 452
            Top = 16
            Width = 89
            Height = 21
            DataField = 'VendaEnti'
            DataSource = DsFixGXPatr
            TabOrder = 3
          end
        end
      end
    end
    object Panel17: TPanel
      Left = 0
      Top = 213
      Width = 185
      Height = 259
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 4
      object Panel18: TPanel
        Left = 0
        Top = 0
        Width = 185
        Height = 20
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Observa'#231#245'es:'
        TabOrder = 0
      end
      object DBMemo1: TDBMemo
        Left = 0
        Top = 20
        Width = 185
        Height = 239
        Align = alClient
        DataField = 'Observa'
        DataSource = DsFixGXPatr
        TabOrder = 1
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 536
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 85
      Align = alTop
      Caption = ' Identifica'#231#227'o: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 85
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 422
        Top = 60
        Width = 94
        Height = 13
        Caption = 'Fabricante / Marca:'
      end
      object SbMarcas: TSpeedButton
        Left = 855
        Top = 56
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbMarcasClick
      end
      object Label38: TLabel
        Left = 16
        Top = 60
        Width = 67
        Height = 13
        Caption = 'Complemento:'
      end
      object EdGraGruX: TdmkEdit
        Left = 16
        Top = 32
        Width = 65
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 85
        Top = 32
        Width = 790
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdMarca: TdmkEditCB
        Left = 518
        Top = 56
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBMarca
        IgnoraDBLookupComboBox = False
      end
      object CBMarca: TdmkDBLookupComboBox
        Left = 574
        Top = 56
        Width = 277
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_MARCA_FABR'
        ListSource = DsMarcas
        TabOrder = 4
        dmkEditCB = EdMarca
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdComplem: TdmkEdit
        Left = 85
        Top = 56
        Width = 335
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Complem'
        UpdCampo = 'Complem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 473
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 4
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 871
        Top = 15
        Width = 135
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object Panel12: TPanel
      Left = 0
      Top = 85
      Width = 1008
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox5: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 64
        Align = alClient
        Caption = ' Aquisi'#231#227'o:  '
        TabOrder = 0
        object Panel13: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label26: TLabel
            Left = 4
            Top = 0
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object Label28: TLabel
            Left = 120
            Top = 0
            Width = 58
            Height = 13
            Caption = 'Documento:'
          end
          object Label29: TLabel
            Left = 396
            Top = 0
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object TPAquisData: TdmkEditDateTimePicker
            Left = 4
            Top = 16
            Width = 112
            Height = 21
            Date = 41133.774077233790000000
            Time = 41133.774077233790000000
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdAquisDocu: TdmkEdit
            Left = 120
            Top = 16
            Width = 272
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdAquisValr: TdmkEdit
            Left = 396
            Top = 16
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
      end
    end
    object Panel14: TPanel
      Left = 0
      Top = 149
      Width = 1008
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox6: TGroupBox
        Left = 0
        Top = 0
        Width = 408
        Height = 64
        Align = alLeft
        Caption = ' Valores atuais: '
        TabOrder = 0
        object Panel15: TPanel
          Left = 2
          Top = 15
          Width = 404
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label30: TLabel
            Left = 332
            Top = 0
            Width = 19
            Height = 13
            Caption = 'Dia:'
          end
          object Label31: TLabel
            Left = 260
            Top = 0
            Width = 42
            Height = 13
            Caption = 'Semana:'
          end
          object Label35: TLabel
            Left = 188
            Top = 0
            Width = 48
            Height = 13
            Caption = 'Quinzena:'
          end
          object Label36: TLabel
            Left = 116
            Top = 0
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
          end
          object Label37: TLabel
            Left = 12
            Top = 0
            Width = 37
            Height = 13
            Caption = 'Venda: '
          end
          object EdAtualValr: TdmkEdit
            Left = 12
            Top = 16
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdValorMes: TdmkEdit
            Left = 116
            Top = 16
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdValorQui: TdmkEdit
            Left = 188
            Top = 16
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdValorSem: TdmkEdit
            Left = 260
            Top = 16
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdValorDia: TdmkEdit
            Left = 332
            Top = 16
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
      end
      object GroupBox7: TGroupBox
        Left = 408
        Top = 0
        Width = 600
        Height = 64
        Align = alClient
        Caption = 'Caracter'#237'sticas: '
        TabOrder = 1
        object Panel16: TPanel
          Left = 2
          Top = 15
          Width = 596
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label40: TLabel
            Left = 72
            Top = 0
            Width = 38
            Height = 13
            Caption = 'Modelo:'
          end
          object Label41: TLabel
            Left = 196
            Top = 0
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label42: TLabel
            Left = 320
            Top = 0
            Width = 47
            Height = 13
            Caption = 'Voltagem:'
          end
          object Label43: TLabel
            Left = 396
            Top = 0
            Width = 45
            Height = 13
            Caption = 'Pot'#234'ncia:'
          end
          object Label44: TLabel
            Left = 468
            Top = 0
            Width = 60
            Height = 13
            Caption = 'Capacidade:'
          end
          object EdModelo: TdmkEdit
            Left = 72
            Top = 16
            Width = 121
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdSerie: TdmkEdit
            Left = 196
            Top = 16
            Width = 121
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdVoltagem: TdmkEdit
            Left = 320
            Top = 16
            Width = 73
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdPotencia: TdmkEdit
            Left = 396
            Top = 16
            Width = 69
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdCapacid: TdmkEdit
            Left = 468
            Top = 16
            Width = 121
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
      end
    end
    object Panel19: TPanel
      Left = 0
      Top = 213
      Width = 185
      Height = 260
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 3
      object Panel20: TPanel
        Left = 0
        Top = 0
        Width = 185
        Height = 20
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Observa'#231#245'es:'
        TabOrder = 0
      end
      object MeObserva: TdmkMemo
        Left = 0
        Top = 20
        Width = 185
        Height = 240
        TabStop = False
        Align = alClient
        TabOrder = 1
        UpdType = utYes
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 9
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 486
        Height = 32
        Caption = 'Cadastro de Equipamentos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 486
        Height = 32
        Caption = 'Cadastro de Equipamentos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 486
        Height = 32
        Caption = 'Cadastro de Equipamentos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrFixGXPatr: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFixGXPatrBeforeOpen
    AfterOpen = QrFixGXPatrAfterOpen
    OnCalcFields = QrFixGXPatrCalcFields
    SQL.Strings = (
      'SELECT CHAR(cpl.Situacao) SIT_CHR, '
      '/*'
      'ggx.Controle, gg1.Referencia, '
      'gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, '
      '*/'
      'gfm.Nome NO_MARCA, gfc.Nome NO_FABR, '
      'IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, '
      '/*'
      'gg2.Nome NO_GG2, gg3.Nome NO_GG3, '
      'gg4.Nome NO_GG4, gg5.Nome NO_GG5, ggt.Nome NO_GGT, '
      'ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3, ggt.TitNiv4, '
      'ggt.TitNiv5, gg1.PrdGrupTip, gg1.Nivel1, gg1.Nivel2, '
      'gg1.Nivel3, gg1.Nivel4, gg1.Nivel5, gg1.CodUsu, '
      '*/'
      'cpl.* '
      'FROM  fixgxpatr cpl '
      'LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle '
      'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo '
      '')
    Left = 64
    Top = 64
    object QrFixGXPatrGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragxpatr.GraGruX'
    end
    object QrFixGXPatrDescrTerc: TWideStringField
      FieldName = 'DescrTerc'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrFixGXPatrMarca: TIntegerField
      FieldName = 'Marca'
      Origin = 'gragxpatr.Marca'
    end
    object QrFixGXPatrNO_MARCA: TWideStringField
      FieldName = 'NO_MARCA'
      Origin = 'grafabmar.Nome'
      Size = 60
    end
    object QrFixGXPatrNO_FABR: TWideStringField
      FieldName = 'NO_FABR'
      Origin = 'grafabcad.Nome'
      Size = 60
    end
    object QrFixGXPatrComplem: TWideStringField
      FieldName = 'Complem'
      Origin = 'gragxpatr.Complem'
      Size = 60
    end
    object QrFixGXPatrAquisData: TDateField
      FieldName = 'AquisData'
      Origin = 'gragxpatr.AquisData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFixGXPatrAquisDocu: TWideStringField
      FieldName = 'AquisDocu'
      Origin = 'gragxpatr.AquisDocu'
      Size = 60
    end
    object QrFixGXPatrAquisValr: TFloatField
      FieldName = 'AquisValr'
      Origin = 'gragxpatr.AquisValr'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFixGXPatrSituacao: TWordField
      FieldName = 'Situacao'
      Origin = 'gragxpatr.Situacao'
    end
    object QrFixGXPatrAtualValr: TFloatField
      FieldName = 'AtualValr'
      Origin = 'gragxpatr.AtualValr'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFixGXPatrValorMes: TFloatField
      FieldName = 'ValorMes'
      Origin = 'gragxpatr.ValorMes'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFixGXPatrValorQui: TFloatField
      FieldName = 'ValorQui'
      Origin = 'gragxpatr.ValorQui'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFixGXPatrValorSem: TFloatField
      FieldName = 'ValorSem'
      Origin = 'gragxpatr.ValorSem'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFixGXPatrValorDia: TFloatField
      FieldName = 'ValorDia'
      Origin = 'gragxpatr.ValorDia'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFixGXPatrAgrupador: TIntegerField
      FieldName = 'Agrupador'
      Origin = 'gragxpatr.Agrupador'
    end
    object QrFixGXPatrModelo: TWideStringField
      FieldName = 'Modelo'
      Origin = 'gragxpatr.Modelo'
      Size = 60
    end
    object QrFixGXPatrSerie: TWideStringField
      FieldName = 'Serie'
      Origin = 'gragxpatr.Serie'
      Size = 60
    end
    object QrFixGXPatrVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Origin = 'gragxpatr.Voltagem'
      Size = 30
    end
    object QrFixGXPatrPotencia: TWideStringField
      FieldName = 'Potencia'
      Origin = 'gragxpatr.Potencia'
      Size = 30
    end
    object QrFixGXPatrCapacid: TWideStringField
      FieldName = 'Capacid'
      Origin = 'gragxpatr.Capacid'
      Size = 30
    end
    object QrFixGXPatrVendaData: TDateField
      FieldName = 'VendaData'
      Origin = 'gragxpatr.VendaData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFixGXPatrVendaDocu: TWideStringField
      FieldName = 'VendaDocu'
      Origin = 'gragxpatr.VendaDocu'
      Size = 60
    end
    object QrFixGXPatrVendaValr: TFloatField
      FieldName = 'VendaValr'
      Origin = 'gragxpatr.VendaValr'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFixGXPatrVendaEnti: TIntegerField
      FieldName = 'VendaEnti'
      Origin = 'gragxpatr.VendaEnti'
    end
    object QrFixGXPatrObserva: TWideStringField
      FieldName = 'Observa'
      Origin = 'gragxpatr.Observa'
      Size = 255
    end
    object QrFixGXPatrAGRPAT: TWideStringField
      FieldName = 'AGRPAT'
      Origin = 'gragxpatr.AGRPAT'
      Size = 25
    end
    object QrFixGXPatrCLVPAT: TWideStringField
      FieldName = 'CLVPAT'
      Origin = 'gragxpatr.CLVPAT'
      Size = 25
    end
    object QrFixGXPatrVENDADATA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENDADATA_TXT'
      Size = 8
      Calculated = True
    end
    object QrFixGXPatrSIT_CHR: TWideStringField
      FieldName = 'SIT_CHR'
      Size = 4
    end
    object QrFixGXPatrMARPAT: TWideStringField
      FieldName = 'MARPAT'
      Origin = 'gragxpatr.MARPAT'
      Size = 10
    end
    object QrFixGXPatrAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrFixGXPatrCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
    end
    object QrFixGXPatrCONTROLE: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CONTROLE'
      Calculated = True
    end
    object QrFixGXPatrNO_GG1: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_GG1'
      Size = 60
      Calculated = True
    end
  end
  object DsFixGXPatr: TDataSource
    DataSet = QrFixGXPatr
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 176
    Top = 64
  end
  object QrMarcas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gfm.Controle, CONCAT(gfm.Nome, " [", '
      'gfc.Nome, "]") NO_MARCA_FABR'
      'FROM grafabmar gfm '
      'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo'
      'ORDER BY NO_MARCA_FABR')
    Left = 204
    Top = 64
    object QrMarcasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMarcasNO_MARCA_FABR: TWideStringField
      FieldName = 'NO_MARCA_FABR'
      Size = 123
    end
  end
  object DsMarcas: TDataSource
    DataSet = QrMarcas
    Left = 232
    Top = 64
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 520
    Top = 516
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrLocod: TmySQLQuery
    Database = Dmod.MyDB
    Left = 804
    Top = 12
  end
end
