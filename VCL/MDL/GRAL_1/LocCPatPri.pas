unit LocCPatPri;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, Grids, DBGrids, ComCtrls, dmkEditDateTimePicker,
  UnDmkEnums;

type
  TFmLocCPatPri = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdCodUso: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdCtrID: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    DsGraGXPatr: TDataSource;
    VU_Sel_: TdmkValUsu;
    EdReferencia: TdmkEdit;
    QrPesq2: TmySQLQuery;
    QrPesq2Referencia: TWideStringField;
    QrPesq1: TmySQLQuery;
    QrPesq1Controle: TIntegerField;
    Label2: TLabel;
    Label7: TLabel;
    QrGraGXPatr: TmySQLQuery;
    QrGraGXPatrControle: TIntegerField;
    QrGraGXPatrReferencia: TWideStringField;
    QrGraGXPatrCOD_GG1: TIntegerField;
    QrGraGXPatrNO_GG1: TWideStringField;
    QrGraGXPatrCPL_EXISTE: TFloatField;
    QrGraGXPatrSituacao: TWordField;
    QrGraGXPatrAtualValr: TFloatField;
    QrGraGXPatrValorMes: TFloatField;
    QrGraGXPatrValorQui: TFloatField;
    QrGraGXPatrValorSem: TFloatField;
    QrGraGXPatrValorDia: TFloatField;
    QrGraGXPatrAgrupador: TIntegerField;
    QrGraGXPatrMarca: TIntegerField;
    QrGraGXPatrModelo: TWideStringField;
    QrGraGXPatrSerie: TWideStringField;
    QrGraGXPatrVoltagem: TWideStringField;
    QrGraGXPatrPotencia: TWideStringField;
    QrGraGXPatrCapacid: TWideStringField;
    QrGraGXPatrTXT_MES: TWideStringField;
    QrGraGXPatrTXT_QUI: TWideStringField;
    QrGraGXPatrTXT_SEM: TWideStringField;
    QrGraGXPatrTXT_DIA: TWideStringField;
    QrAgrupado: TmySQLQuery;
    QrAgrupadoReferencia: TWideStringField;
    QrAgrupadoNome: TWideStringField;
    GroupBox3: TGroupBox;
    Panel3: TPanel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label11: TLabel;
    Label10: TLabel;
    DBEdit2: TDBEdit;
    Label9: TLabel;
    DBEdit1: TDBEdit;
    Label8: TLabel;
    GroupBox4: TGroupBox;
    Panel5: TPanel;
    Label12: TLabel;
    DBEdit5: TDBEdit;
    DsAgrupado: TDataSource;
    Label13: TLabel;
    DBEdit6: TDBEdit;
    QrGraGXPIts: TmySQLQuery;
    QrGraGXPItsGraGXOutr: TIntegerField;
    QrGraGXPItsAplicacao: TIntegerField;
    QrGraGXPItsItemValr: TFloatField;
    QrGraGXPItsItemUnid: TIntegerField;
    TPDataLoc: TdmkEditDateTimePicker;
    EdHoraLoc: TdmkEdit;
    Label21: TLabel;
    QrGraGXPatrNO_SIT: TWideStringField;
    LaSitA1: TLabel;
    LaSitA2: TLabel;
    Label14: TLabel;
    QrGraGXPatrNO_SITAPL: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdReferenciaExit(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    //procedure Reopen_SorceSel_(Controle: Integer);
    procedure ReopenLocCPatPri(CtrID: Integer);
    procedure ReopenAgrupado();
    procedure PesquisaPorReferencia(Limpa: Boolean);
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorNome(Key: Integer);
    //
    procedure IncluiOutros(Codigo, CtrID: Integer);
    procedure MostraSituacaoPatrimonio();

  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FOldGGX: Integer;
  end;

  var
  FmLocCPatPri: TFmLocCPatPri;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
UnDmkProcFunc, UnMySQLCuringa, Principal;

{$R *.DFM}

procedure TFmLocCPatPri.BtOKClick(Sender: TObject);
var
  Codigo, CtrID, GraGruX: Integer;
  ValorDia, ValorSem, ValorQui, ValorMes: Double;
  DtHrLocado: String;
begin
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
  begin
    if MyObjects.FIC(QrGraGXPatrNO_SITAPL.Value = 0, nil,
      'A situa��o atual deste patrim�nio n�o permite sua loca��o!')
    then
      Exit;
  end;
  //
  Codigo         := FQrCab.FieldByName('Codigo').AsInteger;
  CtrID          := EdCtrID.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  ValorDia       := QrGraGXPatrValorDia.Value;
  ValorSem       := QrGraGXPatrValorSem.Value;
  ValorQui       := QrGraGXPatrValorQui.Value;
  ValorMes       := QrGraGXPatrValorMes.Value;
  DtHrLocado     := Geral.FDT(Trunc(TPDataLoc.Date), 1) + ' ' + EdHoraLoc.Text;
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe um patrim�nio principal!') then
    Exit;
  //
  CtrID := UMyMod.BPGS1I32('loccpatpri', 'CtrID', '', '', tsPos, ImgTipo.SQLType, CtrID);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'loccpatpri', False, [
  'Codigo', 'GraGruX', 'ValorDia',
  'ValorSem', 'ValorQui', 'ValorMes',
  'DtHrLocado'], [
  'CtrID'], [
  Codigo, GraGruX, ValorDia,
  ValorSem, ValorQui, ValorMes,
  DtHrLocado], [
  CtrID], True) then
  begin
    if ImgTipo.SQLType = stIns then
      IncluiOutros(Codigo, CtrID);
    EdCtrID.ValueVariant := 0;
    EdReferencia.Text := '';
    EdGraGruX.ValueVariant := 0;
    CBGraGruX.KeyValue := Null;
    //
    Dmod.VerificaSituacaoPatrimonio(GraGruX);
    if (FOldGGX <> 0) and (FOldGGX <> GraGruX) then
      Dmod.VerificaSituacaoPatrimonio(FOldGGX);
    //
    ReopenLocCPatPri(CtrID);
    if FQrIts <> nil then
    begin
      FQrIts.Close;
      FQrIts.Open;
      FQrIts.Locate('CtrID', CtrID, []);
    end;
    UMyMod.AbreQuery(QrGraGXPatr, Dmod.MyDB);
    if (CkContinuar.Checked) and (CkContinuar.Visible) then
      EdGraGruX.SetFocus
    else
      Close;
  end;
end;

procedure TFmLocCPatPri.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCPatPri.EdGraGruXChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    PesquisaPorGraGruX();
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmLocCPatPri.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaPorNome(Key);
end;

procedure TFmLocCPatPri.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmLocCPatPri.EdReferenciaExit(Sender: TObject);
begin
  PesquisaPorReferencia(True);
end;

procedure TFmLocCPatPri.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaPorNome(Key);
end;

procedure TFmLocCPatPri.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmLocCPatPri.FormCreate(Sender: TObject);
var
  Filtro: String;
begin
  FOldGGx := 0;
  ImgTipo.SQLType := stLok;
  //
  TPDataLoc.Date := Trunc(Date);
  EdHoraLoc.Text := FormatDateTime('hh:nn:ss', Now());
  Dmod.FiltroGrade(gbsLocar, Filtro);
  Filtro := Filtro + ' AND (gg1.Nivel1=ggx.GraGru1)';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPatr, Dmod.MyDB, [
  'SELECT ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL, ggx.Controle, ',
  'gg1.Referencia, gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1,   ',
  'IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE,   ',
  'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,  ',
  'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,  ',
  'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,  ',
  'cpl.Voltagem, cpl.Potencia, cpl.Capacid    ',
  'FROM gragrux ggx   ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle   ',
  'LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle   ',
  'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo   ',
  'LEFT JOIN graglsitu ggs ON ggs.Codigo=cpl.Situacao ',
  'WHERE ' + Filtro,
  'ORDER BY NO_GG1 ',
  '']);
  //
  Dmod.ReopenOpcoesTRen;
end;

procedure TFmLocCPatPri.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCPatPri.FormShow(Sender: TObject);
begin
  CkContinuar.Visible := ImgTipo.SQLType = stIns;
end;

procedure TFmLocCPatPri.IncluiOutros(Codigo, CtrID: Integer);
  procedure IncluiSec();
  var
    Item, GraGruX: Integer;
  begin
    if QrGraGXPatrAgrupador.Value <> EdGraGruX.ValueVariant then
    begin
      GraGruX := QrGraGXPatrAgrupador.Value;
      //
      if GraGruX <> 0 then
      begin
        Item := UMyMod.BPGS1I32('loccpatsec', 'Item', '', '', tsPos, stIns, 0);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'loccpatsec', False, [
          'Codigo', 'CtrID', 'GraGruX'], ['Item'], [
          Codigo, CtrID, GraGruX], [Item], True);
      end;
    end;
  end;
  procedure IncluiAce();
  var
    Item, GraGruX: Integer;
    ValBem: Double;
  begin
    //Item           := 0;
    GraGruX        := QrGraGXPItsGraGXOutr.Value;
    ValBem         := QrGraGXPItsItemValr.Value;
    //
    Item := UMyMod.BPGS1I32('loccpatace', 'Item', '', '', tsPos, stIns, 0);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatace', False, [
    'Codigo', 'CtrID', 'GraGruX', 'ValBem'], ['Item'], [
    Codigo, CtrID, GraGruX, ValBem], [Item], True);
  end;
  procedure IncluiCns();
  var
    Item, GraGruX, Unidade: Integer;
    QtdIni, QtdFim, PrcUni, ValUso: Double;
  begin
    //Item           := 0;
    GraGruX        := QrGraGXPItsGraGXOutr.Value;
    Unidade        := QrGraGXPItsItemUnid.Value;
    QtdIni         := 0;
    QtdFim         := 0;
    PrcUni         := QrGraGXPItsItemValr.Value;;
    ValUso         := 0;
    //
    Item := UMyMod.BPGS1I32('loccpatcns', 'Item', '', '', tsPos, stIns, 0);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatcns', False, [
    'Codigo', 'CtrID', 'GraGruX',
    'Unidade', 'QtdIni', 'QtdFim',
    'PrcUni', 'ValUso'], [
    'Item'], [
    Codigo, CtrID, GraGruX,
    Unidade, QtdIni, QtdFim,
    PrcUni, ValUso], [
    Item], True);
  end;
  procedure IncluiUso();
  var
    Item, GraGruX, Unidade, AvalIni: Integer;
    AvalFim, PrcUni, ValTot: Double;
  begin
    //Item           := 0;
    GraGruX        := QrGraGXPItsGraGXOutr.Value;
    Unidade        := QrGraGXPItsItemUnid.Value;
    AvalIni        := 0;
    AvalFim        := 0;
    PrcUni         := QrGraGXPItsItemValr.Value;
    ValTot         := 0;
    //
    Item := UMyMod.BPGS1I32('loccpatuso', 'Item', '', '', tsPos, stIns, 0);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatuso', False, [
    'Codigo', 'CtrID', 'GraGruX',
    'Unidade', 'AvalIni', 'AvalFim',
    'PrcUni', 'ValTot'], [
    'Item'], [
    Codigo, CtrID, GraGruX,
    Unidade, AvalIni, AvalFim,
    PrcUni, ValTot], [
    Item], True);
  end;
//var
  //GraGruX, Item: Integer;
begin
  IncluiSec();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPIts, Dmod.MyDB, [
  'SELECT gpi.GraGXOutr, ggo.Aplicacao, ggo.ItemValr, ggo.ItemUnid ',
  'FROM gragxpits gpi ',
  'LEFT JOIN gragxoutr ggo ON ggo.GraGruX=gpi.GraGXOutr ',
  'WHERE  gpi.GraGXPatr=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND ggo.Aplicacao <> 0 ',
  '']);
  QrGraGXPIts.First;
  while not QrGraGXPIts.Eof do
  begin
    // 1=Acess�rio, 2=Uso, 3=Consumo
    case QrGraGXPItsAplicacao.Value of
      1: IncluiAce();
      2: IncluiUso();
      3: IncluiCns();
      else Geral.MensagemBox('Aplica��o n�o definida em inclus�o de materiais!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
    //
    QrGraGXPIts.Next;
  end;
end;

procedure TFmLocCPatPri.MostraSituacaoPatrimonio();
var
  Cor: TColor;
begin
  if EdGraGruX.ValueVariant = 0 then
    MyObjects.Informa2(LaSitA1, LaSitA2, False, '? ? ?')
  else
    MyObjects.Informa2(LaSitA1, LaSitA2, False, QrGraGXPatrNO_SIT.Value);
  case QrGraGXPatrSituacao.Value of
    68{D}: Cor := clGreen;
    70{F}: Cor := clBlack;
    73{I}: Cor := clpurple;
    76{L}: Cor := clRed;
    79{O}: Cor := clLaranja;
    82{R}: Cor := clLaranja;
    86{V}: Cor := clGray;
    else Cor := clFuchsia;
  end;
  LaSitA2.Font.Color := Cor;
end;

procedure TFmLocCPatPri.PesquisaPorGraGruX();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  cpl.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (cpl.GraGruX IS NULL) ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
    begin
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
      ReopenAgrupado();
    end;
  end else EdReferencia.ValueVariant := '';
  //
  MostraSituacaoPatrimonio();
end;

procedure TFmLocCPatPri.PesquisaPorNome(Key: Integer);
var
  //Controle
  Nivel1: Integer;
begin
  if Key = VK_F3 then
  begin
    Nivel1 := CuringaLoc.CriaForm('Nivel1', CO_NOME, 'gragru1', Dmod.MyDB, CO_VAZIO);
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM gragrux ',
    'WHERE GraGru1=' + Geral.FF0(Nivel1),
    '']);
    EdGraGruX.ValueVariant := Dmod.QrAux.FieldByName('Controle').AsInteger;
    ReopenAgrupado();
  end;
  //
  MostraSituacaoPatrimonio();
end;

procedure TFmLocCPatPri.PesquisaPorReferencia(Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
      ReopenAgrupado();
    end;
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
    EdReferencia.ValueVariant := '';
  //  
  MostraSituacaoPatrimonio();
end;

procedure TFmLocCPatPri.ReopenAgrupado();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgrupado, Dmod.MyDB, [
  'SELECT gg1.Referencia, gg1.Nome ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE  ggx.Controle=' + Geral.FF0(QrGraGXPatrAgrupador.Value),
  '']);
  //
end;

procedure TFmLocCPatPri.ReopenLocCPatPri(CtrID: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if CtrID <> 0 then
      FQrIts.Locate('CtrID', CtrID, []);
  end;
end;

procedure TFmLocCPatPri.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormGraGXPatr(EdGraGruX.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrGraGXPatr, VAR_CADASTRO, 'Controle');
  PesquisaPorGraGruX();
  EdReferencia.SetFocus;
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

end.
