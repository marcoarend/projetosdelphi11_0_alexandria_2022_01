unit LocFCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkDBEdit, dmkValUsu, NFSe_PF_0201,
  UnDmkEnums, UnAppPF, DmkDAC_PF, dmkRadioGroup, UnAppEnums;

type
  TFmLocFCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    PnEdita: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdCodUso: TDBEdit;
    DBEdNome: TDBEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    Label21: TLabel;
    TPDataFat: TdmkEditDateTimePicker;
    EdHoraFat: TdmkEdit;
    EdValLocad: TdmkEdit;
    Label2: TLabel;
    Label8: TLabel;
    EdValConsu: TdmkEdit;
    EdValUsado: TdmkEdit;
    Label9: TLabel;
    Label10: TLabel;
    EdValFrete: TdmkEdit;
    Label11: TLabel;
    EdValDesco: TdmkEdit;
    Label12: TLabel;
    EdValTotal: TdmkEdit;
    Label7: TLabel;
    EdCartEmis: TdmkEditCB;
    CBCartEmis: TdmkDBLookupComboBox;
    VuCondicaoPG: TdmkValUsu;
    EdNumNF: TdmkEdit;
    Label13: TLabel;
    EdSerNF: TdmkEdit;
    Label14: TLabel;
    SBCondicaoPG: TSpeedButton;
    SBCarteira: TSpeedButton;
    QrLoc: TmySQLQuery;
    SbNFSe: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdValLocadChange(Sender: TObject);
    procedure EdValConsuChange(Sender: TObject);
    procedure EdValUsadoChange(Sender: TObject);
    procedure EdValFreteChange(Sender: TObject);
    procedure EdValDescoChange(Sender: TObject);
    procedure SBCondicaoPGClick(Sender: TObject);
    procedure SBCarteiraClick(Sender: TObject);
    procedure SbNFSeClick(Sender: TObject);
    procedure EdNumNFChange(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValores();
  public
    { Public declarations }
    //FQrLoc,
    FQrCab, FQrIts, FQrLocCPatPri: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FFilial, FCliente: Integer;
    FDtHrEmi: TDateTime;
    FEncerra: Boolean;
  end;

  var
  FmLocFCab: TFmLocFCab;

implementation

uses UnMyObjects, Module, ModuleFatura, UMySQLModule, Principal, LocCCon,
  ModuleGeral, UnFinanceiroJan, UnGFat_Jan;

{$R *.DFM}

procedure TFmLocFCab.BtOKClick(Sender: TObject);
const
  TipoFat = tfatServico;
  FatID = VAR_FATID_3001;
  TipoFatura = tfatServico; {TTipoFatura}
  FaturaDta = dfEncerramento; {TDataFatura}
  Financeiro = tfinCred; {TTipoFinanceiro}
var
  DtHrFat, DtHrBxa, SerNF: String;
  Codigo, Controle, NumNF, CondicaoPG, CartEmis, Conta: Integer;
  ValLocad, ValConsu, ValUsado, ValFrete, ValDesco, ValTotal, ValTotalFat: Double;
var
  Entidade, FatNum, Cliente, IDDuplicata, NumeroNF, TipoCart, Represen: Integer;
  DataAbriu, DataEncer: TDateTime;
  SerieNF: String;
begin
  if FEncerra then
  begin
    if not FmLocCCon.LiberaItensLocacao(FQrLocCPatPri(*, FQrLoc*)) then
      Exit;
  end;
  //
  Codigo     := Geral.IMV(Geral.SoNumero_TT(DBEdCodigo.Text));
  Controle   := EdControle.ValueVariant;
  DtHrFat    := Geral.FDT(Trunc(TPDataFat.Date), 1) + ' ' + EdHoraFat.Text;
  ValLocad   := EdValLocad.ValueVariant;
  ValConsu   := EdValConsu.ValueVariant;
  ValUsado   := EdValUsado.ValueVariant;
  ValFrete   := EdValFrete.ValueVariant;
  ValDesco   := EdValDesco.ValueVariant;
  ValTotal   := EdValTotal.ValueVariant;
  SerNF      := EdSerNF.Text;
  NumNF      := EdNumNF.ValueVariant;
  CondicaoPG := EdCondicaoPG.ValueVariant;
  CartEmis   := EdCartEmis.ValueVariant;
  Conta      := DModG.QrParamsEmpCtaServico.Value;
  //
  if MyObjects.FIC(CondicaoPG = 0, EdCondicaoPG,
    'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(CartEmis = 0, EdCartEmis,
    'Informe a carteira!') then Exit;
  if ValTotal < 0.01 then
  begin
    if FEncerra then
    begin
      if Geral.MensagemBox('Valor inv�lido para faturamento!' + #13#10 +
      'Deseja encerrar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
        Exit;
    end else
    begin
      Geral.MensagemBox('Valor inv�lido para faturamento!',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
      Exit;
    end;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    if ValTotal >= 0.01 then
    begin
      Controle := UMyMod.BPGS1I32('locfcab', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'locfcab', False, [
      'Codigo', 'DtHrFat', 'ValLocad',
      'ValConsu', 'ValUsado', 'ValFrete',
      'ValDesco', 'ValTotal', 'SerNF',
      'NumNF', 'CondicaoPG', 'CartEmis'], [
      'Controle'], [
      Codigo, DtHrFat, ValLocad,
      ValConsu, ValUsado, ValFrete,
      ValDesco, ValTotal, SerNF,
      NumNF, CondicaoPG, CartEmis], [
      Controle], True) then
      begin
        Entidade     := FEmpresa;
        FatNum       := Controle;
        Cliente      := FCliente;
        IDDuplicata  := Controle;
        NumeroNF     := NumNF;
        CartEmis     := CartEmis;
        TipoCart     := DmFatura.QrCartEmisTipo.Value;
        CondicaoPG   := CondicaoPG;
        Represen     := 0;
        DataAbriu    := FDtHrEmi;
        DataEncer    := TPDataFat.Date;
        SerieNF      := SerNF;
        //
        DmFatura.EmiteFaturas(Codigo, FatID, Entidade, FatID, FatNum, Cliente,
          DataAbriu, DataEncer, IDDuplicata, NumeroNF, SerieNF, CartEmis,
          TipoCart,  Conta, CondicaoPG, Represen, TipoFatura, FaturaDta,
          Financeiro, ValTotal);
      end;
    end;
    if FEncerra then
    begin
      QrLoc.Close;
      QrLoc.SQL.Clear;
      QrLoc.SQL.Add('SELECT SUM(lfc.ValTotal) ValTotal');
      QrLoc.SQL.Add('FROM locfcab lfc');
      QrLoc.SQL.Add('WHERE lfc.Codigo=' + Geral.FF0(Codigo));
      UMyMod.AbreQuery(QrLoc, Dmod.MyDB);
      if QrLoc.RecordCount > 0 then
        ValTotalFat := QrLoc.FieldByName('ValTotal').AsFloat
      else
        ValTotalFat := 0;
      //
      DtHrBxa := DtHrFat;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'locccon', False, ['ValorTot',
        'DtHrBxa'], ['Codigo'], [ValTotalFat, DtHrBxa], [Codigo], True);
    end;
    if FQrIts <> nil then
    begin
      UMyMod.AbreQuery(FQrIts, Dmod.MyDB);
      FQrIts.Locate('Controle', Controle, []);
    end;
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocFCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocFCab.CalculaValores();
var
  ValLocad, ValConsu, ValUsado, ValFrete, ValDesco, ValTotal: Double;
begin
  ValLocad := EdValLocad.ValueVariant;
  ValConsu := EdValConsu.ValueVariant;
  ValUsado := EdValUsado.ValueVariant;
  ValFrete := EdValFrete.ValueVariant;
  ValDesco := EdValDesco.ValueVariant;
  ValTotal := ValLocad + ValConsu + ValUsado + ValFrete - ValDesco;
  //
  EdValTotal.ValueVariant := ValTotal;
end;

procedure TFmLocFCab.EdNumNFChange(Sender: TObject);
begin
  SbNFSe.Enabled := EdNumNF.ValueVariant = 0;
end;

procedure TFmLocFCab.EdValConsuChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab.EdValDescoChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab.EdValFreteChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab.EdValLocadChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab.EdValUsadoChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocFCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DmFatura.ReopenPediPrzCab();
  DmFatura.ReopenCartEmis(0);
  CBCondicaoPG.ListSource := DmFatura.DsPediPrzCab;
  CBCartEmis.ListSource := DmFatura.DsCartEmis;
end;

procedure TFmLocFCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocFCab.SBCarteiraClick(Sender: TObject);
begin
  FinanceiroJan.InsereEDefineCarteira(EdCartEmis.ValueVariant, EdCartEmis,
  CBCartEmis, DmFatura.QrCartEmis);
end;

procedure TFmLocFCab.SBCondicaoPGClick(Sender: TObject);
begin
  GFat_Jan.InsereEDefinePediPrzCab(EdCondicaoPG.ValueVariant, EdCondicaoPG,
  CBCondicaoPG, DmFatura.QrPediPrzCab);
end;

procedure TFmLocFCab.SbNFSeClick(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  //Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
var
  Prestador, Tomador: Integer;
  Valor: Double;
  SerieNF: String;
  NumNF: Integer;
begin
  Prestador := FFilial; //DmodG.QrFiliLogFilial.Value;
  Tomador   := FCliente;
  Valor     := EdValTotal.ValueVariant;
  //
  UnNFSe_PF_0201.MostraFormNFSe(SQLType, Prestador, Tomador, Intermediario,
    MeuServico, ItemListSrv, Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico,
    nil, Valor, SerieNF, NumNF, nil);
  //
  if NumNF <> 0  then
  begin
    EdSerNF.ValueVariant := SerieNF;
    EdNumNF.ValueVariant := NumNF;
  end;
end;

end.
