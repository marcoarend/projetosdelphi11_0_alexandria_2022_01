unit LocCPatCns;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmLocCPatCns = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdCodUso: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label1: TLabel;
    EdQtdIni: TdmkEdit;
    EdQtdFim: TdmkEdit;
    Label2: TLabel;
    EdQtdUso: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    EdPrcUni: TdmkEdit;
    Label9: TLabel;
    EdValUso: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdQtdIniChange(Sender: TObject);
    procedure EdQtdFimChange(Sender: TObject);
    procedure EdPrcUniChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenLocCPatCns(Item: Integer);
    procedure Calcula_Uso_e_Val();

  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmLocCPatCns: TFmLocCPatCns;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck;

{$R *.DFM}

procedure TFmLocCPatCns.BtOKClick(Sender: TObject);
var
  //Codigo, CtrID, GraGruX, Unidade
  Item: Integer;
  QtdIni, QtdFim, QtdUso, PrcUni, ValUso: Double;
begin
  Item           := Geral.IMV(Geral.SoNumero_TT(DBEdCodigo.Text));
  QtdIni         := EdQtdIni.ValueVariant;
  QtdFim         := EdQtdFim.ValueVariant;
  QtdUso         := EdQtdUso.ValueVariant;
  PrcUni         := EdPrcUni.ValueVariant;
  ValUso         := EdValUso.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'loccpatcns', False, [
  'QtdIni', 'QtdFim', 'QtdUso', 'PrcUni', 'ValUso'], [
  'Item'], [
  QtdIni, QtdFim, QtdUso, PrcUni, ValUso], [
  Item], True) then
  begin
    ReopenLocCPatCns(Item);
    if FQrIts <> nil then
    begin
      FQrIts.Close;
      FQrIts.Open;
      FQrIts.Locate('Item', Item, []);
    end;
    Close;
  end;
end;

procedure TFmLocCPatCns.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCPatCns.Calcula_Uso_e_Val;
var
  QtdIni, QtdFim, QtdUso, PrcUni, ValUso: Double;
begin
  QtdIni := EdQtdIni.ValueVariant;
  QtdFim := EdQtdFim.ValueVariant;
  QtdUso := QtdFim - QtdIni;
  PrcUni := EdPrcUni.ValueVariant;
  ValUso := QtdUso * PrcUni;
  //
  EdQtdUso.ValueVariant := QtdUso;
  EdValUso.ValueVariant := ValUso;
end;

procedure TFmLocCPatCns.EdPrcUniChange(Sender: TObject);
begin
  Calcula_Uso_e_Val();
end;

procedure TFmLocCPatCns.EdQtdFimChange(Sender: TObject);
begin
  Calcula_Uso_e_Val();
end;

procedure TFmLocCPatCns.EdQtdIniChange(Sender: TObject);
begin
  Calcula_Uso_e_Val();
end;

procedure TFmLocCPatCns.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmLocCPatCns.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmLocCPatCns.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCPatCns.ReopenLocCPatCns(Item: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('CtrID').AsInteger;
    FQrIts.Open;
    //
    if Item <> 0 then
      FQrIts.Locate('Item', Item, []);
  end;
end;

end.
