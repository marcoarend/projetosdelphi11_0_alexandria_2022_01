object FmLocFCab: TFmLocFCab
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-004 :: Faturamento de Loca'#231#227'o'
  ClientHeight = 377
  ClientWidth = 724
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 724
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 676
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 628
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 303
        Height = 32
        Caption = 'Faturamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 303
        Height = 32
        Caption = 'Faturamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 303
        Height = 32
        Caption = 'Faturamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 48
    Width = 724
    Height = 215
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 724
      Height = 215
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 724
        Height = 64
        Align = alTop
        Caption = ' Dados do cabe'#231'alho:'
        Enabled = False
        TabOrder = 0
        object Label5: TLabel
          Left = 12
          Top = 20
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label4: TLabel
          Left = 72
          Top = 20
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          FocusControl = DBEdCodUso
        end
        object Label3: TLabel
          Left = 156
          Top = 20
          Width = 81
          Height = 13
          Caption = 'Nome do Cliente:'
          FocusControl = DBEdNome
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 12
          Top = 36
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = FmLocCCon.DsLocCCon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdCodUso: TDBEdit
          Left = 72
          Top = 36
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'Cliente'
          DataSource = FmLocCCon.DsLocCCon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object DBEdNome: TDBEdit
          Left = 156
          Top = 36
          Width = 556
          Height = 21
          TabStop = False
          Color = clWhite
          DataField = 'NO_CLIENTE'
          DataSource = FmLocCCon.DsLocCCon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 64
        Width = 724
        Height = 265
        Align = alTop
        Caption = ' Dados do item (Patrim'#244'nio principal): '
        TabOrder = 1
        object Label6: TLabel
          Left = 12
          Top = 16
          Width = 51
          Height = 13
          Caption = 'ID do item:'
        end
        object Label1: TLabel
          Left = 268
          Top = 16
          Width = 119
          Height = 13
          Caption = 'Condi'#231#227'o de pagamento:'
        end
        object Label21: TLabel
          Left = 98
          Top = 16
          Width = 149
          Height = 13
          Caption = 'Data / hora da loca'#231#227'o / troca:'
        end
        object Label2: TLabel
          Left = 12
          Top = 102
          Width = 54
          Height = 13
          Caption = '$ Loca'#231#227'o:'
        end
        object Label8: TLabel
          Left = 90
          Top = 102
          Width = 56
          Height = 13
          Caption = '$ Consumo:'
        end
        object Label9: TLabel
          Left = 174
          Top = 102
          Width = 31
          Height = 13
          Caption = '$ Uso:'
        end
        object Label10: TLabel
          Left = 258
          Top = 102
          Width = 36
          Height = 13
          Caption = '$ Frete:'
        end
        object Label11: TLabel
          Left = 342
          Top = 102
          Width = 58
          Height = 13
          Caption = '$ Desconto:'
        end
        object Label12: TLabel
          Left = 426
          Top = 102
          Width = 36
          Height = 13
          Caption = '$ Total:'
        end
        object Label7: TLabel
          Left = 12
          Top = 58
          Width = 39
          Height = 13
          Caption = 'Carteira:'
        end
        object Label13: TLabel
          Left = 586
          Top = 16
          Width = 27
          Height = 13
          Caption = 'S'#233'rie:'
        end
        object Label14: TLabel
          Left = 628
          Top = 16
          Width = 57
          Height = 13
          Caption = 'N'#250'mero NF:'
        end
        object SBCondicaoPG: TSpeedButton
          Left = 560
          Top = 32
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBCondicaoPGClick
        end
        object SBCarteira: TSpeedButton
          Left = 351
          Top = 74
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBCarteiraClick
        end
        object SbNFSe: TSpeedButton
          Left = 691
          Top = 32
          Width = 21
          Height = 21
          Caption = '?'
          OnClick = SbNFSeClick
        end
        object EdControle: TdmkEdit
          Left = 12
          Top = 32
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBCondicaoPG: TdmkDBLookupComboBox
          Left = 326
          Top = 32
          Width = 230
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DmFatura.DsPediPrzCab
          TabOrder = 4
          dmkEditCB = EdCondicaoPG
          QryCampo = 'Cargo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCondicaoPG: TdmkEditCB
          Left = 268
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCondicaoPG
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object TPDataFat: TdmkEditDateTimePicker
          Left = 98
          Top = 32
          Width = 112
          Height = 21
          Date = 41131.000000000000000000
          Time = 0.724786689817847200
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataEmi'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdHoraFat: TdmkEdit
          Left = 210
          Top = 32
          Width = 53
          Height = 21
          TabOrder = 2
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          QryCampo = 'HoraEmi'
          UpdCampo = 'HoraIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdValLocad: TdmkEdit
          Left = 12
          Top = 118
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValLocadChange
        end
        object EdValConsu: TdmkEdit
          Left = 90
          Top = 118
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValConsuChange
        end
        object EdValUsado: TdmkEdit
          Left = 174
          Top = 118
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValUsadoChange
        end
        object EdValFrete: TdmkEdit
          Left = 258
          Top = 118
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValFreteChange
        end
        object EdValDesco: TdmkEdit
          Left = 342
          Top = 118
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValDescoChange
        end
        object EdValTotal: TdmkEdit
          Left = 426
          Top = 118
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 14
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCartEmis: TdmkEditCB
          Left = 12
          Top = 74
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCartEmis
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCartEmis: TdmkDBLookupComboBox
          Left = 70
          Top = 74
          Width = 275
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DmFatura.DsCartEmis
          TabOrder = 8
          dmkEditCB = EdCartEmis
          QryCampo = 'Cargo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdSerNF: TdmkEdit
          Left = 586
          Top = 32
          Width = 36
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdNumNF: TdmkEdit
          Left = 628
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdNumNFChange
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 263
    Width = 724
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 720
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 307
    Width = 724
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 578
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 576
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object VuCondicaoPG: TdmkValUsu
    dmkEditCB = EdCondicaoPG
    Panel = PnEdita
    QryCampo = 'CondicaoPG'
    UpdCampo = 'CondicaoPG'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 12
    Top = 12
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 396
    Top = 12
  end
end
