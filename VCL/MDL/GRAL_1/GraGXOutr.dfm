object FmGraGXOutr: TFmGraGXOutr
  Left = 368
  Top = 194
  Caption = 'PRD-GRUPO-028 :: Cadastro de Material'
  ClientHeight = 383
  ClientWidth = 974
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 94
    Width = 974
    Height = 289
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 226
      Width = 974
      Height = 63
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 89
        Height = 39
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 866
        Top = 14
        Width = 106
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 88
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 974
      Height = 60
      Align = alTop
      Caption = ' Identifica'#231#227'o: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 217
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label39: TLabel
        Left = 75
        Top = 16
        Width = 55
        Height = 13
        Caption = 'Refer'#234'ncia:'
      end
      object EdGraGruX: TdmkEdit
        Left = 16
        Top = 31
        Width = 55
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 217
        Top = 31
        Width = 616
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdReferencia: TdmkEdit
        Left = 75
        Top = 31
        Width = 139
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 60
      Width = 974
      Height = 59
      Align = alTop
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 970
        Height = 42
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitTop = 14
        ExplicitHeight = 44
        object Label37: TLabel
          Left = 16
          Top = -2
          Width = 93
          Height = 13
          Caption = '$ Venda (unidade): '
        end
        object Label3: TLabel
          Left = 118
          Top = -2
          Width = 46
          Height = 13
          Caption = 'Unidade: '
        end
        object SBItemUnid: TSpeedButton
          Left = 281
          Top = 14
          Width = 21
          Height = 20
          Caption = '...'
          OnClick = SBItemUnidClick
        end
        object Label49: TLabel
          Left = 710
          Top = 0
          Width = 27
          Height = 13
          Caption = 'NCM:'
        end
        object SpeedButton6: TSpeedButton
          Left = 808
          Top = 13
          Width = 23
          Height = 21
          Caption = '?'
          OnClick = SpeedButton6Click
        end
        object EdItemValr: TdmkEdit
          Left = 16
          Top = 14
          Width = 98
          Height = 20
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdItemUnid: TdmkEditCB
          Left = 118
          Top = 14
          Width = 56
          Height = 20
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBItemUnid
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBItemUnid: TdmkDBLookupComboBox
          Left = 174
          Top = 14
          Width = 104
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Sigla'
          ListSource = DsUnidMed
          TabOrder = 2
          dmkEditCB = EdItemUnid
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object RGAplicacao: TdmkRadioGroup
          Left = 307
          Top = -2
          Width = 397
          Height = 40
          Caption = ' Aplica'#231#227'o: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Inativo'
            'Acess'#243'rio'
            'Uso'
            'Consumo')
          TabOrder = 3
          UpdType = utYes
          OldValor = 0
        end
        object EdNCM: TdmkEdit
          Left = 710
          Top = 14
          Width = 98
          Height = 20
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 94
    Width = 974
    Height = 289
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 974
      Height = 60
      Align = alTop
      Caption = ' Identifica'#231#227'o: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label2: TLabel
        Left = 75
        Top = 16
        Width = 55
        Height = 13
        Caption = 'Refer'#234'ncia:'
        FocusControl = DBEdNome
      end
      object Label25: TLabel
        Left = 16
        Top = 16
        Width = 48
        Height = 13
        Caption = 'Reduzido:'
      end
      object Label46: TLabel
        Left = 217
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = dmkDBEdit1
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 31
        Width = 55
        Height = 21
        TabStop = False
        DataField = 'Controle'
        DataSource = DsGraGXOutr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 75
        Top = 31
        Width = 139
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Referencia'
        DataSource = DsGraGXOutr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 217
        Top = 31
        Width = 748
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NO_GG1'
        DataSource = DsGraGXOutr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 226
      Width = 974
      Height = 63
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 14
        Width = 169
        Height = 48
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 126
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 47
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 171
        Top = 14
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 586
        Top = 14
        Width = 386
        Height = 48
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 185
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 94
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 279
          Top = 0
          Width = 107
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 89
            Height = 39
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 60
      Width = 974
      Height = 63
      Align = alTop
      TabOrder = 2
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 970
        Height = 46
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 12
          Top = 0
          Width = 93
          Height = 13
          Caption = '$ Venda (unidade): '
        end
        object Label4: TLabel
          Left = 114
          Top = 0
          Width = 46
          Height = 13
          Caption = 'Unidade: '
        end
        object DBEdit1: TDBEdit
          Left = 12
          Top = 16
          Width = 99
          Height = 21
          DataField = 'ItemValr'
          DataSource = DsGraGXOutr
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 114
          Top = 16
          Width = 56
          Height = 21
          DataField = 'ItemUnid'
          DataSource = DsGraGXOutr
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 170
          Top = 16
          Width = 126
          Height = 21
          DataField = 'SIGLA'
          DataSource = DsGraGXOutr
          TabOrder = 2
        end
        object DBRGAplicacao: TDBRadioGroup
          Left = 303
          Top = 0
          Width = 660
          Height = 40
          Caption = ' Aplica'#231#227'o: '
          Columns = 4
          DataField = 'Aplicacao'
          DataSource = DsGraGXOutr
          Items.Strings = (
            'Inativo'
            'Acess'#243'rio'
            'Uso'
            'Consumo')
          TabOrder = 3
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
      end
    end
    object GroupBox8: TGroupBox
      Left = 0
      Top = 123
      Width = 974
      Height = 67
      Align = alTop
      Caption = 'Dados do produto:'
      TabOrder = 3
      object DBText2: TDBText
        Left = 199
        Top = 14
        Width = 187
        Height = 17
        DataField = 'TitNiv5'
        DataSource = DsGraGXOutr
      end
      object DBText3: TDBText
        Left = 392
        Top = 14
        Width = 187
        Height = 17
        DataField = 'TitNiv4'
        DataSource = DsGraGXOutr
      end
      object DBText4: TDBText
        Left = 585
        Top = 14
        Width = 187
        Height = 17
        DataField = 'TitNiv3'
        DataSource = DsGraGXOutr
      end
      object DBText5: TDBText
        Left = 778
        Top = 14
        Width = 187
        Height = 17
        DataField = 'TitNiv2'
        DataSource = DsGraGXOutr
      end
      object Label47: TLabel
        Left = 6
        Top = 14
        Width = 78
        Height = 13
        Caption = 'Tipo de produto:'
      end
      object DBEdit24: TDBEdit
        Left = 6
        Top = 33
        Width = 187
        Height = 21
        DataField = 'NO_GGT'
        DataSource = DsGraGXOutr
        TabOrder = 0
      end
      object DBEdit25: TDBEdit
        Left = 199
        Top = 33
        Width = 187
        Height = 21
        DataField = 'NO_GG5'
        DataSource = DsGraGXOutr
        TabOrder = 1
      end
      object DBEdit26: TDBEdit
        Left = 585
        Top = 33
        Width = 187
        Height = 21
        DataField = 'NO_GG3'
        DataSource = DsGraGXOutr
        TabOrder = 2
      end
      object DBEdit30: TDBEdit
        Left = 392
        Top = 33
        Width = 187
        Height = 21
        DataField = 'NO_GG4'
        DataSource = DsGraGXOutr
        TabOrder = 3
      end
      object DBEdit31: TDBEdit
        Left = 778
        Top = 33
        Width = 187
        Height = 21
        DataField = 'NO_GG2'
        DataSource = DsGraGXOutr
        TabOrder = 4
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 974
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 926
      Top = 0
      Width = 48
      Height = 51
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 213
      Height = 51
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 9
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 213
      Top = 0
      Width = 713
      Height = 51
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 239
        Height = 31
        Caption = 'Cadastro de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 239
        Height = 31
        Caption = 'Cadastro de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 239
        Height = 31
        Caption = 'Cadastro de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 974
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 970
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrGraGXOutr: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGraGXOutrBeforeOpen
    AfterOpen = QrGraGXOutrAfterOpen
    SQL.Strings = (
      'SELECT '
      'ggx.Controle, gg1.Referencia,'
      'gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, '
      'IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, '
      'med.SIGLA, gg1.Nivel1, gg1.Nivel2, gg1.Nivel3, '
      'gg2.Nome NO_GG2, gg3.Nome NO_GG3, '
      'gg4.Nome NO_GG4, gg5.Nome NO_GG5, '
      'ggt.Nome NO_GGT, gg1.Nivel4, gg1.Nivel5, gg1.CodUsu, '
      'gg1.PrdGrupTip, ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3,'
      'ggt.TitNiv4, ggt.TitNiv5, gg1.NCM, '
      'gg5.CodUsu CUNivel5, gg4.CodUsu CUNivel4,'
      'gg3.CodUsu CUNivel3, gg2.CodUsu CUNivel2,'
      'cpl.* '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2'
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3'
      'LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg1.Nivel4'
      'LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5'
      'LEFT JOIN prdgruptip ggt ON ggt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN gragxoutr cpl ON cpl.GraGruX=ggx.Controle'
      'LEFT JOIN unidmed med ON med.Codigo=cpl.ItemUnid'
      ''
      'WHERE  gg1.PrdGrupTip=1'
      'AND gg1.Nivel1>0'
      '')
    Left = 64
    Top = 64
    object QrGraGXOutrGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragxoutr.GraGruX'
    end
    object QrGraGXOutrNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrGraGXOutrItemValr: TFloatField
      FieldName = 'ItemValr'
      Origin = 'gragxoutr.ItemValr'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGraGXOutrControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gragrux.Controle'
    end
    object QrGraGXOutrCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
      Origin = 'gragru1.Nivel1'
      Required = True
    end
    object QrGraGXOutrAplicacao: TIntegerField
      FieldName = 'Aplicacao'
      Origin = 'gragxoutr.Aplicacao'
    end
    object QrGraGXOutrReferencia: TWideStringField
      FieldName = 'Referencia'
      Origin = 'gragru1.Referencia'
      Required = True
      Size = 25
    end
    object QrGraGXOutrItemUnid: TIntegerField
      FieldName = 'ItemUnid'
      Origin = 'gragxoutr.ItemUnid'
    end
    object QrGraGXOutrSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
    object QrGraGXOutrCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
      Required = True
    end
    object QrGraGXOutrNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGXOutrNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGXOutrNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGXOutrNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrGraGXOutrNivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrGraGXOutrCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGXOutrPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGXOutrNO_GG2: TWideStringField
      FieldName = 'NO_GG2'
      Size = 30
    end
    object QrGraGXOutrNO_GG3: TWideStringField
      FieldName = 'NO_GG3'
      Size = 30
    end
    object QrGraGXOutrNO_GG4: TWideStringField
      FieldName = 'NO_GG4'
      Size = 30
    end
    object QrGraGXOutrNO_GG5: TWideStringField
      FieldName = 'NO_GG5'
      Size = 30
    end
    object QrGraGXOutrNO_GGT: TWideStringField
      FieldName = 'NO_GGT'
      Size = 30
    end
    object QrGraGXOutrTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Size = 15
    end
    object QrGraGXOutrTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 15
    end
    object QrGraGXOutrTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 15
    end
    object QrGraGXOutrTitNiv4: TWideStringField
      FieldName = 'TitNiv4'
      Size = 15
    end
    object QrGraGXOutrTitNiv5: TWideStringField
      FieldName = 'TitNiv5'
      Size = 15
    end
    object QrGraGXOutrNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGXOutrCUNivel5: TIntegerField
      FieldName = 'CUNivel5'
    end
    object QrGraGXOutrCUNivel4: TIntegerField
      FieldName = 'CUNivel4'
    end
    object QrGraGXOutrCUNivel3: TIntegerField
      FieldName = 'CUNivel3'
    end
    object QrGraGXOutrCUNivel2: TIntegerField
      FieldName = 'CUNivel2'
    end
  end
  object DsGraGXOutr: TDataSource
    DataSet = QrGraGXOutr
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrMarcas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gfm.Controle, CONCAT(gfm.Nome, " [", '
      'gfc.Nome, "]") NO_MARCA_FABR'
      'FROM grafabmar gfm '
      'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo'
      'ORDER BY NO_MARCA_FABR')
    Left = 152
    Top = 64
    object QrMarcasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMarcasNO_MARCA_FABR: TWideStringField
      FieldName = 'NO_MARCA_FABR'
      Size = 123
    end
  end
  object DsMarcas: TDataSource
    DataSet = QrMarcas
    Left = 180
    Top = 64
  end
  object QrAgrupador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Nome'
      'FROM gragxpatr gxp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=gxp.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'ORDER BY gg1.Nome')
    Left = 212
    Top = 64
    object QrAgrupadorControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gragrux.Controle'
    end
    object QrAgrupadorNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru1.Nome'
      Size = 120
    end
  end
  object DsAgrupador: TDataSource
    DataSet = QrAgrupador
    Left = 240
    Top = 64
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Sigla'
      'FROM unidmed'
      'ORDER BY Sigla')
    Left = 272
    Top = 64
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 300
    Top = 64
  end
  object QrLocod: TMySQLQuery
    Database = Dmod.MyDB
    Left = 548
    Top = 10
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 456
    Top = 224
    object Alteraprodutoatual1: TMenuItem
      Caption = '&Altera produto atual'
      OnClick = Alteraprodutoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Editanveis1: TMenuItem
      Caption = '&Edita n'#237'veis'
      OnClick = Editanveis1Click
    end
    object Editadadosdoproduto1: TMenuItem
      Caption = 'Edita dados do &produto'
      OnClick = Editadadosdoproduto1Click
    end
  end
end
