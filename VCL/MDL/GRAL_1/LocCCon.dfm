object FmLocCCon: TFmLocCCon
  Left = 368
  Top = 194
  ActiveControl = SbQuery
  Caption = 'LOC-PATRI-001 :: Gerenciamento de Loca'#231#227'o'
  ClientHeight = 631
  ClientWidth = 994
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 106
    Width = 994
    Height = 525
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitWidth = 791
    ExplicitHeight = 408
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 994
      Height = 394
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      ExplicitWidth = 791
      object Panel11: TPanel
        Left = 2
        Top = 41
        Width = 990
        Height = 351
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        ExplicitWidth = 787
        object Label16: TLabel
          Left = 8
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label17: TLabel
          Left = 67
          Top = 0
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object Label18: TLabel
          Left = 567
          Top = 0
          Width = 62
          Height = 13
          Caption = 'Requisitante:'
        end
        object Label19: TLabel
          Left = 776
          Top = 0
          Width = 126
          Height = 13
          Caption = 'Respons'#225'vel pela retirada:'
        end
        object Label22: TLabel
          Left = 8
          Top = 86
          Width = 49
          Height = 13
          Caption = 'Vendedor:'
        end
        object Label23: TLabel
          Left = 8
          Top = 126
          Width = 71
          Height = 13
          Caption = 'N'#186' Requisi'#231#227'o:'
        end
        object Label25: TLabel
          Left = 158
          Top = 86
          Width = 88
          Height = 13
          Caption = 'Endere'#231'o da obra:'
        end
        object Label26: TLabel
          Left = 8
          Top = 166
          Width = 70
          Height = 13
          Caption = 'Observa'#231#227'o 1:'
        end
        object Label27: TLabel
          Left = 334
          Top = 166
          Width = 70
          Height = 13
          Caption = 'Observa'#231#227'o 2:'
        end
        object Label28: TLabel
          Left = 662
          Top = 166
          Width = 70
          Height = 13
          Caption = 'Observa'#231#227'o 3:'
        end
        object Label24: TLabel
          Left = 693
          Top = 86
          Width = 79
          Height = 13
          Caption = 'Contato na obra:'
        end
        object Label29: TLabel
          Left = 870
          Top = 86
          Width = 108
          Height = 13
          Caption = 'Telefone contato obra:'
        end
        object Label30: TLabel
          Left = 118
          Top = 126
          Width = 313
          Height = 13
          Caption = 'Endere'#231'o de cobran'#231'a (quando diferente do endere'#231'o do cliente):'
        end
        object SpeedButton5: TSpeedButton
          Left = 539
          Top = 16
          Width = 21
          Height = 21
          Hint = 'Inclui item de carteira'
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object Label34: TLabel
          Left = 8
          Top = 46
          Width = 98
          Height = 13
          Caption = 'Endere'#231'o do cliente:'
        end
        object ImgCliente: TImage
          Left = 8
          Top = 208
          Width = 126
          Height = 126
          Cursor = crHandPoint
          Proportional = True
          Stretch = True
          OnClick = ImgClienteClick
        end
        object ImgRequisitante: TImage
          Left = 150
          Top = 208
          Width = 127
          Height = 126
          Cursor = crHandPoint
          Proportional = True
          Stretch = True
          OnClick = ImgRequisitanteClick
        end
        object ImgRetirada: TImage
          Left = 294
          Top = 208
          Width = 125
          Height = 126
          Cursor = crHandPoint
          Proportional = True
          Stretch = True
          OnClick = ImgRetiradaClick
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 16
          Width = 55
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCliente: TdmkEditCB
          Left = 67
          Top = 16
          Width = 55
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cliente'
          UpdCampo = 'Cliente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdClienteChange
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 122
          Top = 16
          Width = 414
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsClientes
          TabOrder = 2
          dmkEditCB = EdCliente
          QryCampo = 'Cliente'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdECComprou: TdmkEditCB
          Left = 567
          Top = 16
          Width = 55
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ECComprou'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdECComprouChange
          DBLookupComboBox = CBECComprou
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBECComprou: TdmkDBLookupComboBox
          Left = 622
          Top = 16
          Width = 150
          Height = 21
          KeyField = 'Controle'
          ListField = 'Nome'
          ListSource = DsECComprou
          TabOrder = 4
          dmkEditCB = EdECComprou
          QryCampo = 'ECComprou'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBECRetirou: TdmkDBLookupComboBox
          Left = 831
          Top = 16
          Width = 150
          Height = 21
          KeyField = 'Controle'
          ListField = 'Nome'
          ListSource = DsECRetirou
          TabOrder = 6
          dmkEditCB = EdECRetirou
          QryCampo = 'ECRetirou'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdECRetirou: TdmkEditCB
          Left = 776
          Top = 16
          Width = 55
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ECRetirou'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdECRetirouChange
          DBLookupComboBox = CBECRetirou
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdVendedor: TdmkEditCB
          Left = 8
          Top = 102
          Width = 35
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Vendedor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBVendedor
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBVendedor: TdmkDBLookupComboBox
          Left = 43
          Top = 102
          Width = 111
          Height = 21
          KeyField = 'Numero'
          ListField = 'Login'
          ListSource = DsSenhas
          TabOrder = 9
          dmkEditCB = EdVendedor
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNumOC: TdmkEdit
          Left = 8
          Top = 142
          Width = 107
          Height = 20
          TabOrder = 13
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'NumOC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdLocalObra: TdmkEdit
          Left = 158
          Top = 102
          Width = 532
          Height = 21
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'LocalObra'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObs0: TdmkEdit
          Left = 8
          Top = 181
          Width = 323
          Height = 21
          TabOrder = 15
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Obs0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObs1: TdmkEdit
          Left = 334
          Top = 181
          Width = 324
          Height = 21
          TabOrder = 16
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Obs1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObs2: TdmkEdit
          Left = 662
          Top = 181
          Width = 320
          Height = 21
          TabOrder = 17
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Obs2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdLocalCntat: TdmkEdit
          Left = 693
          Top = 102
          Width = 174
          Height = 21
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'LocalCntat'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdLocalFone: TdmkEdit
          Left = 870
          Top = 102
          Width = 111
          Height = 21
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtTelLongo
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'LocalFone'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEndCobra: TdmkEdit
          Left = 118
          Top = 142
          Width = 864
          Height = 20
          TabOrder = 14
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'EndCobra'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEndCliente: TdmkEdit
          Left = 8
          Top = 62
          Width = 974
          Height = 20
          Enabled = False
          ReadOnly = True
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'LocalObra'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 990
        Height = 26
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        ExplicitWidth = 787
        object Label9: TLabel
          Left = 8
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label21: TLabel
          Left = 776
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Emiss'#227'o:'
        end
        object Label7: TLabel
          Left = 496
          Top = 4
          Width = 43
          Height = 13
          Caption = 'Contrato:'
        end
        object EdEmpresa: TdmkEditCB
          Left = 54
          Top = 0
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 98
          Top = 0
          Width = 396
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          QryCampo = 'Filial'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object TPDataEmi: TdmkEditDateTimePicker
          Left = 819
          Top = 0
          Width = 111
          Height = 21
          Date = 41131.000000000000000000
          Time = 0.724786689817847200
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataEmi'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdHoraEmi: TdmkEdit
          Left = 934
          Top = 0
          Width = 48
          Height = 21
          TabOrder = 5
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          QryCampo = 'HoraEmi'
          UpdCampo = 'HoraIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdContrato: TdmkEditCB
          Left = 539
          Top = 0
          Width = 36
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Contrato'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBContrato
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBContrato: TdmkDBLookupComboBox
          Left = 575
          Top = 0
          Width = 198
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContratos
          TabOrder = 3
          dmkEditCB = EdContrato
          QryCampo = 'Contrato'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 464
      Width = 994
      Height = 61
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 347
      ExplicitWidth = 791
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 861
        Top = 15
        Width = 131
        Height = 44
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 658
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 118
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 106
    Width = 994
    Height = 525
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 791
    ExplicitHeight = 408
    object Splitter1: TSplitter
      Left = 0
      Top = 218
      Width = 994
      Height = 11
      Cursor = crVSplit
      Align = alTop
      ExplicitWidth = 993
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 462
      Width = 994
      Height = 63
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 345
      ExplicitWidth = 791
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 169
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 126
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 47
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 171
        Top = 15
        Width = 308
        Height = 46
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
        ExplicitWidth = 105
      end
      object Panel3: TPanel
        Left = 479
        Top = 15
        Width = 513
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        ExplicitLeft = 276
        object Panel2: TPanel
          Left = 382
          Top = 0
          Width = 131
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 3
            Width = 118
            Height = 39
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 499
          Left = 5
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Loca'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 497
          Left = 126
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Patrim'#244'nio'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtFat: TBitBtn
          Tag = 414
          Left = 248
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Faturamento'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFatClick
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 994
      Height = 135
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      ExplicitWidth = 791
      object Panel6: TPanel
        Left = 2
        Top = 41
        Width = 990
        Height = 92
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        ExplicitWidth = 787
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 98
          Top = 4
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          FocusControl = DBEdNome
        end
        object Label3: TLabel
          Left = 473
          Top = 4
          Width = 79
          Height = 13
          Caption = 'Quem requisitou:'
          FocusControl = DBEdit1
        end
        object Label4: TLabel
          Left = 658
          Top = 4
          Width = 73
          Height = 13
          Caption = 'Quem recebeu:'
          FocusControl = DBEdit2
        end
        object Label6: TLabel
          Left = 839
          Top = 4
          Width = 29
          Height = 13
          Caption = 'Baixa:'
          FocusControl = DBEdit4
        end
        object Label8: TLabel
          Left = 4
          Top = 27
          Width = 49
          Height = 13
          Caption = 'Vendedor:'
          FocusControl = DBEdit5
        end
        object Label12: TLabel
          Left = 205
          Top = 27
          Width = 88
          Height = 13
          Caption = 'Endere'#231'o da obra:'
          FocusControl = DBEdit8
        end
        object Label13: TLabel
          Left = 4
          Top = 51
          Width = 70
          Height = 13
          Caption = 'Observa'#231#227'o 1:'
          FocusControl = DBEdit9
        end
        object Label14: TLabel
          Left = 333
          Top = 51
          Width = 70
          Height = 13
          Caption = 'Observa'#231#227'o 2:'
          FocusControl = DBEdit10
        end
        object Label15: TLabel
          Left = 658
          Top = 51
          Width = 70
          Height = 13
          Caption = 'Observa'#231#227'o 3:'
          FocusControl = DBEdit11
        end
        object Label10: TLabel
          Left = 674
          Top = 27
          Width = 40
          Height = 13
          Caption = 'Contato:'
          FocusControl = DBEdit6
        end
        object Label32: TLabel
          Left = 839
          Top = 27
          Width = 27
          Height = 13
          Caption = 'Fone:'
          FocusControl = DBEdit15
        end
        object Label33: TLabel
          Left = 4
          Top = 75
          Width = 112
          Height = 13
          Caption = 'Endere'#231'o da cobran'#231'a:'
          FocusControl = DBEdit16
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 39
          Top = 0
          Width = 55
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsLocCCon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 134
          Top = 0
          Width = 336
          Height = 21
          Color = clWhite
          DataField = 'NO_CLIENTE'
          DataSource = DsLocCCon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit1: TDBEdit
          Left = 556
          Top = 0
          Width = 98
          Height = 21
          DataField = 'NO_COMPRADOR'
          DataSource = DsLocCCon
          TabOrder = 2
        end
        object DBEdit2: TDBEdit
          Left = 738
          Top = 0
          Width = 98
          Height = 21
          DataField = 'NO_RECEBEU'
          DataSource = DsLocCCon
          TabOrder = 3
        end
        object DBEdit4: TDBEdit
          Left = 870
          Top = 0
          Width = 111
          Height = 21
          DataField = 'DtHrBxa_TXT'
          DataSource = DsLocCCon
          TabOrder = 4
        end
        object DBEdit5: TDBEdit
          Left = 55
          Top = 24
          Width = 144
          Height = 21
          DataField = 'NO_LOGIN'
          DataSource = DsLocCCon
          TabOrder = 5
        end
        object DBEdit8: TDBEdit
          Left = 295
          Top = 24
          Width = 375
          Height = 21
          DataField = 'LocalObra'
          DataSource = DsLocCCon
          TabOrder = 6
        end
        object DBEdit9: TDBEdit
          Left = 78
          Top = 47
          Width = 253
          Height = 21
          DataField = 'Obs0'
          DataSource = DsLocCCon
          TabOrder = 7
        end
        object DBEdit10: TDBEdit
          Left = 406
          Top = 44
          Width = 252
          Height = 21
          DataField = 'Obs1'
          DataSource = DsLocCCon
          TabOrder = 8
        end
        object DBEdit11: TDBEdit
          Left = 729
          Top = 47
          Width = 252
          Height = 21
          DataField = 'Obs2'
          DataSource = DsLocCCon
          TabOrder = 9
        end
        object DBEdit6: TDBEdit
          Left = 717
          Top = 24
          Width = 119
          Height = 21
          DataField = 'LocalCntat'
          DataSource = DsLocCCon
          TabOrder = 10
        end
        object DBEdit15: TDBEdit
          Left = 870
          Top = 24
          Width = 111
          Height = 21
          DataField = 'LocalFone'
          DataSource = DsLocCCon
          TabOrder = 11
        end
        object DBEdit16: TDBEdit
          Left = 118
          Top = 71
          Width = 864
          Height = 21
          DataField = 'EndCobra'
          DataSource = DsLocCCon
          TabOrder = 12
        end
      end
      object Panel12: TPanel
        Left = 2
        Top = 15
        Width = 990
        Height = 26
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        ExplicitWidth = 787
        object Label20: TLabel
          Left = 4
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label5: TLabel
          Left = 658
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Emiss'#227'o:'
          FocusControl = DBEdit3
        end
        object Label31: TLabel
          Left = 512
          Top = 4
          Width = 33
          Height = 13
          Caption = 'N'#186' OC:'
          FocusControl = DBEdit14
        end
        object Label11: TLabel
          Left = 815
          Top = 4
          Width = 50
          Height = 13
          Caption = 'Valor total:'
          FocusControl = DBEdit7
        end
        object DBEdit3: TDBEdit
          Left = 701
          Top = 0
          Width = 110
          Height = 21
          DataField = 'DtHrEmi'
          DataSource = DsLocCCon
          TabOrder = 2
        end
        object DBEdit12: TDBEdit
          Left = 114
          Top = 1
          Width = 395
          Height = 21
          DataField = 'NO_EMP'
          DataSource = DsLocCCon
          TabOrder = 1
        end
        object DBEdit13: TDBEdit
          Left = 51
          Top = 1
          Width = 60
          Height = 21
          DataField = 'Filial'
          DataSource = DsLocCCon
          TabOrder = 0
        end
        object DBEdit14: TDBEdit
          Left = 547
          Top = 0
          Width = 107
          Height = 21
          DataField = 'NumOC'
          DataSource = DsLocCCon
          TabOrder = 3
        end
        object DBEdit7: TDBEdit
          Left = 870
          Top = 0
          Width = 111
          Height = 21
          DataField = 'ValorTot'
          DataSource = DsLocCCon
          TabOrder = 4
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 135
      Width = 994
      Height = 83
      Align = alTop
      Caption = ' Patrim'#244'nios principais: '
      TabOrder = 2
      ExplicitWidth = 791
      object DGDados: TDBGrid
        Left = 2
        Top = 15
        Width = 990
        Height = 66
        Align = alClient
        DataSource = DsLocCPatPri
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CtrID'
            Title.Caption = 'ID'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduz.'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'REFERENCIA'
            Title.Caption = 'Refer'#234'ncia'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_GGX'
            Title.Caption = 'Descri'#231#227'o do Patrim'#244'nio'
            Width = 234
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TXT_DTA_DEVOL'
            Title.Caption = 'Devolvido em'
            Width = 67
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TXT_DTA_LIBER'
            Title.Caption = 'Liberado em'
            Width = 67
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TXT_QEM_LIBER'
            Title.Caption = 'Liberado por'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorDia'
            Title.Caption = '$ Dia'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorSem'
            Title.Caption = '$ Semana'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorQui'
            Title.Caption = '$ Quinzena'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorMes'
            Title.Caption = '$ M'#234's'
            Width = 45
            Visible = True
          end>
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 229
      Width = 994
      Height = 221
      ActivePage = TabSheet1
      Align = alTop
      TabHeight = 20
      TabOrder = 3
      ExplicitWidth = 791
      object TabSheet1: TTabSheet
        Caption = 'Geral'
        object Splitter2: TSplitter
          Left = 261
          Top = 0
          Width = 9
          Height = 191
          ExplicitHeight = 193
        end
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 261
          Height = 191
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Splitter3: TSplitter
            Left = 0
            Top = 94
            Width = 261
            Height = 10
            Cursor = crVSplit
            Align = alTop
          end
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 261
            Height = 94
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 261
              Height = 461
              Align = alTop
              Caption = ' Patrim'#244'nios secund'#225'rios: '
              TabOrder = 0
              object DBGrid2: TDBGrid
                Left = 2
                Top = 15
                Width = 257
                Height = 444
                Align = alClient
                DataSource = DsLocCPatSec
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 45
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'REFERENCIA'
                    Title.Caption = 'Refer'#234'ncia'
                    Width = 58
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_GGX'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 236
                    Visible = True
                  end>
              end
            end
          end
          object GroupBox5: TGroupBox
            Left = 0
            Top = 104
            Width = 261
            Height = 87
            Align = alClient
            Caption = ' Acess'#243'rios: '
            TabOrder = 1
            object DBGAcessorios: TDBGrid
              Left = 2
              Top = 15
              Width = 257
              Height = 70
              Align = alClient
              DataSource = DsLocCPatAce
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnMouseUp = DBGAcessoriosMouseUp
              Columns = <
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzifo'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_GGX'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 121
                  Visible = True
                end>
            end
          end
        end
        object Panel13: TPanel
          Left = 270
          Top = 0
          Width = 716
          Height = 191
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitWidth = 513
          object Splitter4: TSplitter
            Left = 0
            Top = 77
            Width = 716
            Height = 9
            Cursor = crVSplit
            Align = alTop
          end
          object GroupBox6: TGroupBox
            Left = 0
            Top = 0
            Width = 716
            Height = 77
            Align = alTop
            Caption = 'Material de uso:'
            TabOrder = 0
            ExplicitWidth = 513
            object DBGLocCPatUso: TDBGrid
              Left = 2
              Top = 15
              Width = 712
              Height = 60
              Align = alClient
              DataSource = DsLocCPatUso
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBGLocCPatUsoDblClick
              OnMouseUp = DBGLocCPatUsoMouseUp
              Columns = <
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'REFERENCIA'
                  Title.Caption = 'Refer'#234'ncia'
                  Width = 58
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_GGX'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 221
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_AVALINI'
                  Title.Caption = 'Avalia. inicial'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_AVALFIM'
                  Title.Caption = 'Avalia. final'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PrcUni'
                  Title.Caption = 'Pre'#231'o'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValTot'
                  Title.Caption = 'Valor'
                  Width = 48
                  Visible = True
                end>
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 86
            Width = 716
            Height = 105
            Align = alClient
            Caption = 'Material de consumo:'
            TabOrder = 1
            ExplicitWidth = 513
            object DBGLocCPatCns: TDBGrid
              Left = 2
              Top = 15
              Width = 712
              Height = 88
              Align = alClient
              DataSource = DsLocCPatCns
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBGLocCPatCnsDblClick
              OnMouseUp = DBGLocCPatCnsMouseUp
              Columns = <
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'REFERENCIA'
                  Title.Caption = 'Refer'#234'ncia'
                  Width = 58
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_GGX'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 221
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SIGLA'
                  Width = 37
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QtdIni'
                  Title.Caption = 'Qtd. inicial'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QtdFim'
                  Title.Caption = 'Qtd. final'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QtdUso'
                  Title.Caption = 'Qtd usado'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValUso'
                  Title.Caption = 'Valor'
                  Width = 45
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Faturamento'
        ImageIndex = 1
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 985
          Height = 187
          Align = alClient
          TabOrder = 0
          object Splitter5: TSplitter
            Left = 1
            Top = 139
            Width = 983
            Height = 5
            Cursor = crVSplit
            Align = alTop
          end
          object GroupBox4: TGroupBox
            Left = 1
            Top = 1
            Width = 983
            Height = 138
            Align = alTop
            Caption = ' Faturamento: '
            TabOrder = 0
            object DBGrid3: TDBGrid
              Left = 2
              Top = 14
              Width = 979
              Height = 122
              Align = alClient
              DataSource = DsLocFCab
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'DtHrFat'
                  Title.Caption = 'Faturado em'
                  Width = 104
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NumNF'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValTotal'
                  Title.Caption = '$ TOTAL'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValConsu'
                  Title.Caption = '$ Consumo'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValUsado'
                  Title.Caption = '$ Uso'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValLocad'
                  Title.Caption = '$ Loca'#231#227'o'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValDesco'
                  Title.Caption = '$ Desconto'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValFrete'
                  Title.Caption = '$ Frete'
                  Width = 59
                  Visible = True
                end>
            end
          end
          object GroupBox7: TGroupBox
            Left = 1
            Top = 144
            Width = 983
            Height = 42
            Align = alClient
            Caption = ' Parcelas do faturamento selecionado: '
            TabOrder = 1
            object DBGLctFatRef: TDBGrid
              Left = 2
              Top = 14
              Width = 979
              Height = 26
              Align = alClient
              DataSource = DsLctFatRef
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'PARCELA'
                  Title.Caption = 'Parc.'
                  Width = 22
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Lancto'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Width = 58
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencto'
                  Width = 45
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 994
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 791
    object GB_R: TGroupBox
      Left = 947
      Top = 0
      Width = 47
      Height = 51
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 744
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 295
      Height = 51
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object SbImagens: TBitBtn
        Tag = 102
        Left = 210
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 5
        OnClick = SbImagensClick
      end
      object BtNFSe: TBitBtn
        Tag = 533
        Left = 252
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtNFSeClick
      end
    end
    object GB_M: TGroupBox
      Left = 295
      Top = 0
      Width = 652
      Height = 51
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 449
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 314
        Height = 31
        Caption = 'Gerenciamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 314
        Height = 31
        Caption = 'Gerenciamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 314
        Height = 31
        Caption = 'Gerenciamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 994
    Height = 55
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitWidth = 791
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 990
      Height = 21
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 787
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 878
        Height = 16
        Caption = 
          'Para adicionar Assess'#243'rios, Materiais de Uso ou Materiais de Con' +
          'sumo clique com o bot'#227'o direito do mouse sobre a grade correspon' +
          'dente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 878
        Height = 16
        Caption = 
          'Para adicionar Assess'#243'rios, Materiais de Uso ou Materiais de Con' +
          'sumo clique com o bot'#227'o direito do mouse sobre a grade correspon' +
          'dente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PB1: TProgressBar
      Left = 2
      Top = 36
      Width = 990
      Height = 17
      Align = alBottom
      TabOrder = 1
      Visible = False
      ExplicitWidth = 787
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 40
  end
  object QrLocCCon: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrLocCConBeforeOpen
    AfterOpen = QrLocCConAfterOpen
    BeforeClose = QrLocCConBeforeClose
    AfterScroll = QrLocCConAfterScroll
    OnCalcFields = QrLocCConCalcFields
    SQL.Strings = (
      'SELECT com.Nome NO_COMPRADOR, ret.Nome NO_RECEBEU,'
      'lcc.*, sen.Login NO_LOGIN, cin.CodCliInt Filial,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE'
      'FROM locccon lcc'
      'LEFT JOIN entidades emp ON emp.Codigo=lcc.Empresa'
      'LEFT JOIN enticliint cin ON cin.CodEnti=emp.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=lcc.Cliente'
      'LEFT JOIN senhas sen ON sen.Numero=lcc.Vendedor'
      'LEFT JOIN enticontat com ON com.Controle=lcc.ECComprou'
      'LEFT JOIN enticontat ret ON ret.Controle=lcc.ECRetirou'
      'WHERE lcc.Codigo > 0')
    Left = 64
    Top = 40
    object QrLocCConEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrLocCConNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrLocCConNO_COMPRADOR: TWideStringField
      FieldName = 'NO_COMPRADOR'
      Size = 30
    end
    object QrLocCConNO_RECEBEU: TWideStringField
      FieldName = 'NO_RECEBEU'
      Size = 30
    end
    object QrLocCConCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCConContrato: TIntegerField
      FieldName = 'Contrato'
    end
    object QrLocCConCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLocCConDtHrEmi: TDateTimeField
      FieldName = 'DtHrEmi'
    end
    object QrLocCConDtHrBxa: TDateTimeField
      FieldName = 'DtHrBxa'
    end
    object QrLocCConVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLocCConECComprou: TIntegerField
      FieldName = 'ECComprou'
    end
    object QrLocCConECRetirou: TIntegerField
      FieldName = 'ECRetirou'
    end
    object QrLocCConNumOC: TWideStringField
      FieldName = 'NumOC'
      Size = 60
    end
    object QrLocCConValorTot: TFloatField
      FieldName = 'ValorTot'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConLocalObra: TWideStringField
      DisplayWidth = 100
      FieldName = 'LocalObra'
      Size = 50
    end
    object QrLocCConObs0: TWideStringField
      FieldName = 'Obs0'
      Size = 50
    end
    object QrLocCConObs1: TWideStringField
      FieldName = 'Obs1'
      Size = 50
    end
    object QrLocCConObs2: TWideStringField
      FieldName = 'Obs2'
      Size = 50
    end
    object QrLocCConNO_LOGIN: TWideStringField
      FieldName = 'NO_LOGIN'
      Required = True
      Size = 30
    end
    object QrLocCConNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrLocCConDataEmi: TDateField
      FieldKind = fkCalculated
      FieldName = 'DataEmi'
      Calculated = True
    end
    object QrLocCConHoraEmi: TTimeField
      FieldKind = fkCalculated
      FieldName = 'HoraEmi'
      Calculated = True
    end
    object QrLocCConDtHrBxa_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DtHrBxa_TXT'
      Calculated = True
    end
    object QrLocCConEndCobra: TWideStringField
      FieldName = 'EndCobra'
      Size = 100
    end
    object QrLocCConLocalCntat: TWideStringField
      FieldName = 'LocalCntat'
      Size = 50
    end
    object QrLocCConLocalFone: TWideStringField
      FieldName = 'LocalFone'
    end
    object QrLocCConFilial: TIntegerField
      FieldName = 'Filial'
    end
  end
  object DsLocCCon: TDataSource
    DataSet = QrLocCCon
    Left = 92
    Top = 40
  end
  object QrLocCPatPri: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLocCPatPriBeforeClose
    AfterScroll = QrLocCPatPriAfterScroll
    OnCalcFields = QrLocCPatPriCalcFields
    SQL.Strings = (
      'SELECT lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN '
      'FROM loccpatpri lpp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci'
      'WHERE lpp.Codigo=:P0'
      '')
    Left = 64
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCPatPriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCPatPriCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocCPatPriGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocCPatPriValorDia: TFloatField
      FieldName = 'ValorDia'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCPatPriValorSem: TFloatField
      FieldName = 'ValorSem'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCPatPriValorQui: TFloatField
      FieldName = 'ValorQui'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCPatPriValorMes: TFloatField
      FieldName = 'ValorMes'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCPatPriDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
    end
    object QrLocCPatPriLibFunci: TIntegerField
      FieldName = 'LibFunci'
    end
    object QrLocCPatPriLibDtHr: TDateTimeField
      FieldName = 'LibDtHr'
    end
    object QrLocCPatPriRELIB: TWideStringField
      FieldName = 'RELIB'
    end
    object QrLocCPatPriHOLIB: TWideStringField
      FieldName = 'HOLIB'
      Size = 5
    end
    object QrLocCPatPriNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocCPatPriLOGIN: TWideStringField
      FieldName = 'LOGIN'
      Required = True
      Size = 30
    end
    object QrLocCPatPriREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
    object QrLocCPatPriTXT_DTA_DEVOL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DTA_DEVOL'
      Calculated = True
    end
    object QrLocCPatPriTXT_DTA_LIBER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DTA_LIBER'
      Size = 30
      Calculated = True
    end
    object QrLocCPatPriTXT_QEM_LIBER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_QEM_LIBER'
      Size = 100
      Calculated = True
    end
    object QrLocCPatPriDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
    end
    object QrLocCPatPriLibExUsr: TIntegerField
      FieldName = 'LibExUsr'
    end
  end
  object DsLocCPatPri: TDataSource
    DataSet = QrLocCPatPri
    Left = 92
    Top = 68
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 656
    Top = 584
    object ItsInclui1: TMenuItem
      Caption = '&Inclui'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Retorna1: TMenuItem
      Caption = '&Retorna'
      Enabled = False
      OnClick = Retorna1Click
    end
    object Libera1: TMenuItem
      Caption = '&Libera'
      Enabled = False
      OnClick = Libera1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ItsExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object ItsExclui2: TMenuItem
      Caption = 'Exclui re&torno do item selecionado'
      Enabled = False
      OnClick = ItsExclui2Click
    end
    object ItsExclui3: TMenuItem
      Caption = 'Exclui li&bera'#231#227'o do item selecionado'
      Enabled = False
      OnClick = ItsExclui3Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 540
    Top = 584
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrLocCPatSec: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lps.*, gg1.Nome NO_GGX, gg1.REFERENCIA'
      'FROM loccpatsec lps'
      'LEFT JOIN gragrux ggx ON ggx.Controle=lps.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE lps.CtrID>0'
      '')
    Left = 120
    Top = 68
    object QrLocCPatSecCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCPatSecCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocCPatSecItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocCPatSecGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocCPatSecNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocCPatSecREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
  end
  object DsLocCPatSec: TDataSource
    DataSet = QrLocCPatSec
    Left = 148
    Top = 68
  end
  object QrLocCPatAce: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lps.*, gg1.Nome NO_GGX, gg1.REFERENCIA'
      'FROM loccpatsec lps'
      'LEFT JOIN gragrux ggx ON ggx.Controle=lps.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE lps.CtrID>0'
      '')
    Left = 176
    Top = 68
    object QrLocCPatAceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCPatAceCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocCPatAceItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocCPatAceGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocCPatAceNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocCPatAceREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
  end
  object DsLocCPatAce: TDataSource
    DataSet = QrLocCPatAce
    Left = 204
    Top = 68
  end
  object QrLocCPatCns: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLocCPatCnsCalcFields
    SQL.Strings = (
      'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,'
      'med.SIGLA '
      'FROM loccpatcns lpc '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade'
      'WHERE lpc.CtrID>0')
    Left = 232
    Top = 68
    object QrLocCPatCnsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCPatCnsCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocCPatCnsItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocCPatCnsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocCPatCnsNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocCPatCnsREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
    object QrLocCPatCnsUnidade: TIntegerField
      FieldName = 'Unidade'
    end
    object QrLocCPatCnsQtdIni: TFloatField
      FieldName = 'QtdIni'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLocCPatCnsQtdFim: TFloatField
      FieldName = 'QtdFim'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLocCPatCnsQtdUso: TFloatField
      FieldName = 'QtdUso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLocCPatCnsPrcUni: TFloatField
      FieldName = 'PrcUni'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrLocCPatCnsValUso: TFloatField
      FieldName = 'ValUso'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocCPatCnsSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
  end
  object DsLocCPatCns: TDataSource
    DataSet = QrLocCPatCns
    Left = 260
    Top = 68
  end
  object QrLocCPatUso: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lpu.*, gg1.Nome NO_GGX, gg1.REFERENCIA,'
      'med.SIGLA, avi.Nome NO_AVALINI, avf.Nome NO_AVALFIM '
      'FROM loccpatuso lpu '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpu.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed med ON med.Codigo=lpu.Unidade'
      'LEFT JOIN graglaval avi ON avi.Codigo=lpu.AvalIni'
      'LEFT JOIN graglaval avf ON avf.Codigo=lpu.AvalFim'
      'WHERE lpu.CtrID>0'
      '')
    Left = 288
    Top = 68
    object QrLocCPatUsoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'loccpatuso.Codigo'
    end
    object QrLocCPatUsoCtrID: TIntegerField
      FieldName = 'CtrID'
      Origin = 'loccpatuso.CtrID'
    end
    object QrLocCPatUsoItem: TIntegerField
      FieldName = 'Item'
      Origin = 'loccpatuso.Item'
    end
    object QrLocCPatUsoGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'loccpatuso.GraGruX'
    end
    object QrLocCPatUsoNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrLocCPatUsoREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Origin = 'gragru1.Referencia'
      Required = True
      Size = 25
    end
    object QrLocCPatUsoAvalIni: TIntegerField
      FieldName = 'AvalIni'
      Origin = 'loccpatuso.AvalIni'
    end
    object QrLocCPatUsoAvalFim: TIntegerField
      FieldName = 'AvalFim'
      Origin = 'loccpatuso.AvalFim'
    end
    object QrLocCPatUsoPrcUni: TFloatField
      FieldName = 'PrcUni'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCPatUsoValTot: TFloatField
      FieldName = 'ValTot'
      Origin = 'loccpatuso.ValTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCPatUsoUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'loccpatuso.Unidade'
    end
    object QrLocCPatUsoSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
    object QrLocCPatUsoNO_AVALINI: TWideStringField
      FieldName = 'NO_AVALINI'
      Origin = 'graglaval.Nome'
      Size = 60
    end
    object QrLocCPatUsoNO_AVALFIM: TWideStringField
      FieldName = 'NO_AVALFIM'
      Origin = 'graglaval.Nome'
      Size = 60
    end
  end
  object DsLocCPatUso: TDataSource
    DataSet = QrLocCPatUso
    Left = 316
    Top = 68
  end
  object QrLocFCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLocFCabBeforeClose
    AfterScroll = QrLocFCabAfterScroll
    SQL.Strings = (
      'SELECT lfc.*'
      'FROM locfcab lfc'
      'WHERE lfc.Codigo>0'
      ''
      '')
    Left = 148
    Top = 40
    object QrLocFCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocFCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocFCabDtHrFat: TDateTimeField
      FieldName = 'DtHrFat'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrLocFCabValLocad: TFloatField
      FieldName = 'ValLocad'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabValConsu: TFloatField
      FieldName = 'ValConsu'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabValUsado: TFloatField
      FieldName = 'ValUsado'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabValFrete: TFloatField
      FieldName = 'ValFrete'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabValDesco: TFloatField
      FieldName = 'ValDesco'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabValTotal: TFloatField
      FieldName = 'ValTotal'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabSerNF: TWideStringField
      FieldName = 'SerNF'
      Size = 3
    end
    object QrLocFCabNumNF: TIntegerField
      FieldName = 'NumNF'
      DisplayFormat = '000000'
    end
    object QrLocFCabCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
    end
    object QrLocFCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
  end
  object DsLocFCab: TDataSource
    DataSet = QrLocFCab
    Left = 176
    Top = 40
  end
  object DsLctFatRef: TDataSource
    DataSet = QrLctFatRef
    Left = 232
    Top = 40
  end
  object QrLctFatRef: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLctFatRefCalcFields
    SQL.Strings = (
      'SELECT lfi.*'
      'FROM locfits lfi'
      'WHERE lfi.Controle>0'
      '')
    Left = 204
    Top = 40
    object QrLctFatRefPARCELA: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'PARCELA'
      Calculated = True
    end
    object QrLctFatRefCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLctFatRefControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctFatRefConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrLctFatRefLancto: TLargeintField
      FieldName = 'Lancto'
    end
    object QrLctFatRefValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctFatRefVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 432
    Top = 68
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Ativo = 1'
      'ORDER BY NOMEENTIDADE')
    Left = 404
    Top = 68
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrECComprou: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM enticontat'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 460
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrECComprouControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrECComprouNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsECComprou: TDataSource
    DataSet = QrECComprou
    Left = 488
    Top = 68
  end
  object QrECRetirou: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM enticontat'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 516
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Controle'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsECRetirou: TDataSource
    DataSet = QrECRetirou
    Left = 544
    Top = 68
  end
  object QrSenhas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, login'
      'FROM senhas'
      'WHERE Ativo=1'
      'ORDER BY Login')
    Left = 572
    Top = 68
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrSenhaslogin: TWideStringField
      FieldName = 'login'
      Required = True
      Size = 30
    end
  end
  object DsSenhas: TDataSource
    DataSet = QrSenhas
    Left = 600
    Top = 68
  end
  object QrContratos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contratos'
      'ORDER BY Nome')
    Left = 628
    Top = 68
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 656
    Top = 68
  end
  object QrLcts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 788
    Top = 292
  end
  object PMFat: TPopupMenu
    OnPopup = PMFatPopup
    Left = 784
    Top = 580
    object Faturamentoparcial1: TMenuItem
      Caption = 'Faturamento parcial'
      Enabled = False
      OnClick = Faturamentoparcial1Click
    end
    object Faturaeencerracontrato1: TMenuItem
      Caption = 'Fatura e encerra contrato'
      Enabled = False
      OnClick = Faturaeencerracontrato1Click
    end
    object Encerracontrato1: TMenuItem
      Caption = 'Encerra contrato'
      Enabled = False
      OnClick = Encerracontrato1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Excluifaturamento1: TMenuItem
      Caption = 'Exclui todo faturamento selecionado e reabre loca'#231#227'o'
      OnClick = Excluifaturamento1Click
    end
    object Reabrelocao1: TMenuItem
      Caption = 'Reabre loca'#231#227'o'
      OnClick = Reabrelocao1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Gerabloqueto1: TMenuItem
      Caption = 'Emite boleto(s) do faturamento selecionado'
      OnClick = Gerabloqueto1Click
    end
    object Visualizarbloquetos1: TMenuItem
      Caption = 'Visualizar bloquetos'
      OnClick = Visualizarbloquetos1Click
    end
  end
  object VUEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PnEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 160
    Top = 104
  end
  object PMAcessorios: TPopupMenu
    OnPopup = PMAcessoriosPopup
    Left = 712
    Top = 16
    object Adicionaacessrio1: TMenuItem
      Caption = '&Adiciona acess'#243'rio'
      OnClick = Adicionaacessrio1Click
    end
    object Removeacessrio1: TMenuItem
      Caption = '&Remove acess'#243'rio'
      OnClick = Removeacessrio1Click
    end
  end
  object PMLocCPatUso: TPopupMenu
    OnPopup = PMLocCPatUsoPopup
    Left = 740
    Top = 16
    object Adicionamaterialuso1: TMenuItem
      Caption = '&Adiciona material uso'
      OnClick = Adicionamaterialuso1Click
    end
    object Removematerialuso1: TMenuItem
      Caption = '&Remove material uso'
      OnClick = Removematerialuso1Click
    end
  end
  object PMLocCPatCns: TPopupMenu
    OnPopup = PMLocCPatCnsPopup
    Left = 768
    Top = 16
    object Adicionamaterialdeconsumo1: TMenuItem
      Caption = '&Adiciona material de consumo'
      OnClick = Adicionamaterialdeconsumo1Click
    end
    object Removematerialdeconsumo1: TMenuItem
      Caption = '&Remove material de consumo'
      OnClick = Removematerialdeconsumo1Click
    end
  end
  object PMImagens: TPopupMenu
    OnPopup = PMImagensPopup
    Left = 796
    Top = 16
    object Visualizarimagemdocliente1: TMenuItem
      Caption = 'Visualizar imagem do &cliente'
      OnClick = Visualizarimagemdocliente1Click
    end
    object Visualizarimagemdorequisitante1: TMenuItem
      Caption = 'Visualizar imagem do &requisitante'
      OnClick = Visualizarimagemdorequisitante1Click
    end
    object Visualizarimagemdorecebedor1: TMenuItem
      Caption = 'Visualizar imagem do r&ecebedor'
      OnClick = Visualizarimagemdorecebedor1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 824
    Top = 16
    object Contrato1: TMenuItem
      Caption = '&Contrato'
      OnClick = Contrato1Click
    end
    object Devoluodelocao1: TMenuItem
      Caption = '&Devolu'#231#227'o de loca'#231#227'o'
      object Selecionados1: TMenuItem
        Caption = '&Selecionado(s)'
        OnClick = Selecionados1Click
      end
      object odos1: TMenuItem
        Caption = '&Todos'
        OnClick = odos1Click
      end
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 404
    Top = 307
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 860
    Top = 284
  end
end
