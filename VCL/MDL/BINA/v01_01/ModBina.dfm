object DmModBina: TDmModBina
  OldCreateOrder = False
  Height = 419
  Width = 598
  object DsBinaLigouA: TDataSource
    DataSet = QrBinaLigouA
    Left = 72
    Top = 112
  end
  object QrBinaLigouA: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = QrBinaLigouACalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM binaligoua '
      'ORDER BY Codigo DESC')
    Left = 72
    Top = 64
    object QrBinaLigouACodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrBinaLigouANome: TWideStringField
      FieldName = 'Nome'
    end
    object QrBinaLigouAxData: TWideStringField
      FieldName = 'xData'
      Size = 10
    end
    object QrBinaLigouAxHora: TWideStringField
      FieldName = 'xHora'
      Size = 10
    end
    object QrBinaLigouAdData: TDateField
      FieldName = 'dData'
    end
    object QrBinaLigouAdHora: TTimeField
      FieldName = 'dHora'
    end
    object QrBinaLigouAUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrBinaLigouATerminal: TIntegerField
      FieldName = 'Terminal'
    end
    object QrBinaLigouAIP: TWideStringField
      FieldName = 'IP'
      Size = 60
    end
    object QrBinaLigouAForcado: TSmallintField
      FieldName = 'Forcado'
    end
    object QrBinaLigouALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBinaLigouADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBinaLigouADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBinaLigouAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBinaLigouAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBinaLigouAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrBinaLigouAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBinaLigouATELEFONE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TELEFONE'
      Size = 30
      Calculated = True
    end
  end
  object QrBinaAB: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 68
    Top = 16
  end
  object IdIPWatch1: TIdIPWatch
    Active = False
    HistoryEnabled = False
    HistoryFilename = 'iphist.dat'
    Left = 68
    Top = 160
  end
end
