unit BinaLigouB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmBinaLigouB = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAtende: TBitBtn;
    DBGrid1: TDBGrid;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    BtReabrir: TBitBtn;
    Panel5: TPanel;
    GroupBox2: TGroupBox;
    CkLimit: TCheckBox;
    EdLimitIni: TdmkEdit;
    EdLimitQtd: TdmkEdit;
    SpeedButton1: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton4: TBitBtn;
    dmkRadioGroup1: TdmkRadioGroup;
    Panel6: TPanel;
    Label1: TLabel;
    EdLogin: TdmkEdit;
    Panel7: TPanel;
    Label2: TLabel;
    EdIP: TdmkEdit;
    QrQtd: TmySQLQuery;
    QrQtdItens: TLargeintField;
    QrBinaLigouB: TmySQLQuery;
    QrBinaLigouBCodigo: TAutoIncField;
    QrBinaLigouBNome: TWideStringField;
    QrBinaLigouBxData: TWideStringField;
    QrBinaLigouBxHora: TWideStringField;
    QrBinaLigouBdData: TDateField;
    QrBinaLigouBdHora: TTimeField;
    QrBinaLigouBUsuario: TIntegerField;
    QrBinaLigouBTerminal: TIntegerField;
    QrBinaLigouBIP: TWideStringField;
    QrBinaLigouBForcado: TSmallintField;
    QrBinaLigouBLk: TIntegerField;
    QrBinaLigouBDataCad: TDateField;
    QrBinaLigouBDataAlt: TDateField;
    QrBinaLigouBUserCad: TIntegerField;
    QrBinaLigouBUserAlt: TIntegerField;
    QrBinaLigouBAlterWeb: TSmallintField;
    QrBinaLigouBAtivo: TSmallintField;
    QrBinaLigouBTELEFONE: TWideStringField;
    QrBinaLigouBDescartou: TSmallintField;
    QrBinaLigouBLogin: TWideStringField;
    QrBinaLigouBTXT_DESCARTOU: TWideStringField;
    DsBinaLigouB: TDataSource;
    Panel8: TPanel;
    Label3: TLabel;
    EdNome: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtAtendeClick(Sender: TObject);
    procedure CkLimitClick(Sender: TObject);
    procedure EdLimitIniChange(Sender: TObject);
    procedure EdLimitQtdChange(Sender: TObject);
    procedure dmkRadioGroup1Click(Sender: TObject);
    procedure EdLoginChange(Sender: TObject);
    procedure EdIPChange(Sender: TObject);
    procedure BtReabrirClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure QrBinaLigouBCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenBinaLigouB();
    procedure FecharPesquisa();
    function  Registros(): Integer;
  public
    { Public declarations }
  end;

  var
  FmBinaLigouB: TFmBinaLigouB;

implementation

uses
{$IfNDef NAO_DIAR} UnDiario_PF, {$EndIf}
UnMyObjects, Module, UMySQLModule, DmkDAC_PF, UnDmkProcFunc;

{$R *.DFM}

procedure TFmBinaLigouB.BtReabrirClick(Sender: TObject);
begin
  ReopenBinaLigouB();
end;

procedure TFmBinaLigouB.BtAtendeClick(Sender: TObject);
var
  xData, xHora, xFone: String;
begin
  //Dmod.AtendeTelefone(QrBinaLigouB);
  xData := QrBinaLigouBxData.Value;
  xHora := QrBinaLigouBxHora.Value;
  xFone := QrBinaLigouBNome.Value;
  Hide;
{$IfNDef NAO_DIAR}
  Diario_PF.MostraDiarioAddByFoneComm(xData, xHora, xFone, False, 0);
{$Else}
  dmkPF.InfoSemModulo(mdlappDiario);
{$EndIf}
  Close;
end;

procedure TFmBinaLigouB.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBinaLigouB.CkLimitClick(Sender: TObject);
begin
  FecharPesquisa();
end;

procedure TFmBinaLigouB.DBGrid1DblClick(Sender: TObject);
begin
  BtAtendeClick(Self);
end;

procedure TFmBinaLigouB.dmkRadioGroup1Click(Sender: TObject);
begin
  FecharPesquisa();
end;

procedure TFmBinaLigouB.EdIPChange(Sender: TObject);
begin
  FecharPesquisa();
end;

procedure TFmBinaLigouB.EdLimitIniChange(Sender: TObject);
begin
  FecharPesquisa();
end;

procedure TFmBinaLigouB.EdLimitQtdChange(Sender: TObject);
begin
  FecharPesquisa();
end;

procedure TFmBinaLigouB.EdLoginChange(Sender: TObject);
begin
  FecharPesquisa();
end;

procedure TFmBinaLigouB.FecharPesquisa;
begin
  QrBinaLigouB.Close;
end;

procedure TFmBinaLigouB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBinaLigouB.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmBinaLigouB.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBinaLigouB.QrBinaLigouBCalcFields(DataSet: TDataSet);
begin
  QrBinaLigouBTELEFONE.Value := Geral.FormataTelefone_TT(QrBinaLigouBNome.Value);
end;

function TFmBinaLigouB.Registros(): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrQtd, Dmod.MyDB, [
  'SELECT COUNT(*) Itens ',
  'FROM binaligoub ',
  '']);
  Result := QrQtdItens.Value;
end;

procedure TFmBinaLigouB.ReopenBinaLigouB();
var
  IP, Login, SQL_IP, SQL_Login, SQL_LIMIT, LimitIni, LimitQtd, SQL_NOme,
  Nome: String;
begin
  IP := Trim(EdIP.Text);
  if IP = '' then
    SQL_IP := ''
  else
    SQL_IP := 'AND blb.IP="' + IP + '" ';

  Login := Trim(EdLogin.Text);
  if Login = '' then
    SQL_Login := ''
  else
    SQL_Login := 'AND sen.Login="' + Login + '" ';
  //
  if CkLimit.Checked then
  begin
    LimitIni := Geral.FF0(EdLimitIni.ValueVariant);
    LimitQtd := Geral.FF0(EdLimitQtd.ValueVariant);

    SQL_LIMIT := 'LIMIT ' + LimitIni + ',' + LimitQtd + ' ';
  end else
    SQL_LIMIT := '';
  //
  Nome := EdNome.Text;
  if Trim(Nome) = '' then
    SQL_Nome := ''
  else
    SQL_Nome := 'AND Nome LIKE"%' + Nome + '%"';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBinaLigouB, Dmod.MyDB, [
  'SELECT blb.*, sen.Login, ',
  'ELT(blb.Descartou + 1, "N�o", "Sim", "???") TXT_DESCARTOU ',
  'FROM binaligoub blb ',
  'LEFT JOIN senhas sen ON sen.Numero=blb.Usuario ',
  'WHERE Codigo <> 0 ',
  SQL_IP,
  SQL_Login,
  SQL_Nome,
  'ORDER BY Codigo Desc ',
  SQL_LIMIT,
  '']);
end;

procedure TFmBinaLigouB.SpeedButton1Click(Sender: TObject);
begin
  EdLimitIni.ValueVariant := 0;
  if CkLimit.Checked then
    ReopenBinaLigouB();
end;

procedure TFmBinaLigouB.SpeedButton2Click(Sender: TObject);
var
  LimitIni: Integer;
begin
  LimitIni := EdLimitIni.ValueVariant - EdLimitQtd.ValueVariant;
  if LimitIni < 0 then
    EdLimitIni.ValueVariant := 0
  else
    EdLimitIni.ValueVariant := LimitIni;
  if CkLimit.Checked then
    ReopenBinaLigouB();
end;

procedure TFmBinaLigouB.SpeedButton3Click(Sender: TObject);
var
  Ini, Max: Integer;
begin
  Max := Registros();
  Ini := EdLimitIni.ValueVariant + EdLimitQtd.ValueVariant;
  if Ini >= Max then
    Ini := Max - EdLimitQtd.ValueVariant;
  if Ini < 0 then
    Ini := 0;
  EdLimitIni.ValueVariant := Ini;
  if CkLimit.Checked then
    ReopenBinaLigouB();
end;

procedure TFmBinaLigouB.SpeedButton4Click(Sender: TObject);
var
  Ini, Max: Integer;
begin
  Max := Registros();
    Ini := Max - EdLimitQtd.ValueVariant;
  if Ini < 0 then
    Ini := 0;
  EdLimitIni.ValueVariant := Ini;
  if CkLimit.Checked then
    ReopenBinaLigouB();
end;

end.
