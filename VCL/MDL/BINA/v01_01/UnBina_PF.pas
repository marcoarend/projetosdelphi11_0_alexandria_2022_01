unit UnBina_PF;

interface

uses
  Windows, Forms, StdCtrls, ComCtrls, ExtCtrls, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, UnMsgInt, dmkGeral, UnInternalConsts,
  DB, mySQLDBTables, UnDmkProcFunc, UnDmkEnums;

type
  TUnBina_PF = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    procedure MostraFormBinaLigouA();
    procedure MostraFormBinaLigouB();
    procedure MostraFormDirectTerminal();
  end;

var
  Bina_PF: TUnBina_PF;

implementation

uses
  UMySQLModule, Module, ModuleGeral, MyDBCheck, UnMyObjects, DmkDAC_PF,
  ModBina, BinaLigouA, BinaLigouB,
  DirectTerminal;

{ TUnBina_PF }

procedure TUnBina_PF.MostraFormBinaLigouA();
begin
  DmModBina.ReopenBinaLigouA();
  case DmModBina.QrBinaLigouA.RecordCount of
    0: Geral.MB_Aviso('N�o h� registro de chamada recebida n�o atendida!');
    1: DmModBina.AtendeTelefone(DmModBina.QrBinaLigouA);
    else
    begin
      if DBCheck.CriaFm(TFmBinaLigouA, FmBinaLigouA, afmoNegarComAviso) then
      begin
        FmBinaLigouA.ShowModal;
        FmBinaLigouA.Destroy;
      end;
    end;
  end;
end;

procedure TUnBina_PF.MostraFormBinaLigouB();
begin
  if DBCheck.CriaFm(TFmBinaLigouB, FmBinaLigouB, afmoNegarComAviso) then
  begin
    FmBinaLigouB.ShowModal;
    FmBinaLigouB.Destroy;
  end;
end;

procedure TUnBina_PF.MostraFormDirectTerminal;
begin
  FmDirectTerminal.Show;
  FmDirectTerminal.EdData.Text := FormatDateTime('mmdd', Date);
  FmDirectTerminal.EdHora.Text := FormatDateTime('hhnn', Time);
  FmDirectTerminal.EdFone.SetFocus;
end;

end.
