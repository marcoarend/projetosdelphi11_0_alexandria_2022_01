unit ModBina;

interface

uses
  Windows, Messages, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons,
  System.SysUtils, System.Classes, IdBaseComponent, IdComponent, IdIPWatch,
  Data.DB, mySQLDbTables, UnInternalConsts, UnDmkEnums;

type
  TDmModBina = class(TDataModule)
    DsBinaLigouA: TDataSource;
    QrBinaLigouA: TmySQLQuery;
    QrBinaLigouACodigo: TAutoIncField;
    QrBinaLigouANome: TWideStringField;
    QrBinaLigouAxData: TWideStringField;
    QrBinaLigouAxHora: TWideStringField;
    QrBinaLigouAdData: TDateField;
    QrBinaLigouAdHora: TTimeField;
    QrBinaLigouAUsuario: TIntegerField;
    QrBinaLigouATerminal: TIntegerField;
    QrBinaLigouAIP: TWideStringField;
    QrBinaLigouAForcado: TSmallintField;
    QrBinaLigouALk: TIntegerField;
    QrBinaLigouADataCad: TDateField;
    QrBinaLigouADataAlt: TDateField;
    QrBinaLigouAUserCad: TIntegerField;
    QrBinaLigouAUserAlt: TIntegerField;
    QrBinaLigouAAlterWeb: TSmallintField;
    QrBinaLigouAAtivo: TSmallintField;
    QrBinaLigouATELEFONE: TWideStringField;
    QrBinaAB: TmySQLQuery;
    IdIPWatch1: TIdIPWatch;
    procedure QrBinaLigouACalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FCamposBinaLigou: String;
    //
    procedure ReopenBinaLigouA();
    procedure AtendeTelefone(Qry: TmySQLQuery);
  end;

var
  DmModBina: TDmModBina;

implementation

uses
{$IfNDef NAO_DIAR} UnDiario_PF, {$EndIf}
DmkGeral, Module, DmkDAC_PF, UMySQLModule, DirectTerminal, UnDmkProcFunc;

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

procedure TDmModBina.AtendeTelefone(Qry: TmySQLQuery);
var
  //Codigo: Integer;
  CodTxt, xData, xHora, xFone, IP: String;
  Usuario, Terminal, Descartou: Integer;
begin
  //Codigo := Qry.FieldByName('Codigo').AsInteger;
  CodTxt := Geral.FF0(Qry.FieldByName('Codigo').AsInteger);
  xData := Qry.FieldByName('xData').AsString;
  xHora := Qry.FieldByName('xHora').AsString;
  xFone := Qry.FieldByName('Nome').AsString;
  //
  Qry.Close;
  //
  DmModBina.QrBinaAB.Close;
  DmModBina.QrBinaAB.Database := Dmod.MyDB;
  //
  UMyMod.ExecutaMySQLQuery1(QrBinaAB, [
    'LOCK TABLES binaligoua WRITE, binaligoub WRITE',
    '']);
  //
  UndmkDAC_PF.AbreMySQLQuery0(QrBinaAB, Dmod.MyDB, [
  'SELECT * ',
  'FROM binaligoua ',
  'WHERE Codigo=' + CodTxt,
  '']);
  //
  if QrBinaAB.RecordCount > 0 then
  begin
    if FCamposBinaLigou = '' then
    begin
      FCamposBinaLigou :=
        UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, 'binaligoub', '');
    end;
    //
    UMyMod.ExecutaMySQLQuery1(QrBinaAB, [
    'INSERT INTO binaligoub ',
    'SELECT ' + FCamposBinaLigou,
    'FROM binaligoua ',
    'WHERE Codigo=' + CodTxt,
    '; ',
    'DELETE FROM binaligoua ',
    'WHERE Codigo=' + CodTxt,
    '; ',
    '']);
    IdIPWatch1.Active := True;
    Usuario           := VAR_USUARIO;
    Terminal          := VAR_TERMINAL;
    Descartou         := 0;
    IP                := IdIPWatch1.LocalIP;
    IdIPWatch1.Active := False;
    //
    UMyMod.SQLInsUpd(QrBinaAB, stUpd, 'binaligoub', False, [
    'Usuario', 'Terminal', 'IP', 'Descartou'], [
    'Codigo'], [
    Usuario, Terminal, IP, Descartou], [
    CodTxt], True);
    //
    UMyMod.ExecutaMySQLQuery1(QrBinaAB, [
    'UNLOCK TABLES; ',
    '']);
    //
    FmDirectTerminal.Hide;
{$IfNDef NAO_DIAR}
    Diario_PF.MostraDiarioAddByFoneComm(xData, xHora, xFone, False, 0);
{$Else}
  dmkPF.InfoSemModulo(mdlappDiario);
{$EndIf}
  end else
  begin
    UMyMod.ExecutaMySQLQuery1(QrBinaAB, [
    'UNLOCK TABLES; ',
    '']);
    Geral.MB_Aviso('A chamada selecionada j� foi atendida!');
  end;
end;

procedure TDmModBina.QrBinaLigouACalcFields(DataSet: TDataSet);
begin
  QrBinaLigouATELEFONE.Value := Geral.FormataTelefone_TT(QrBinaLigouAnome.Value);
end;

procedure TDmModBina.ReopenBinaLigouA;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBinaLigouA, Dmod.MyDB, [
  'SELECT * ',
  'FROM binaligoua ',
  'ORDER BY Codigo DESC ',
  '']);
end;

end.
