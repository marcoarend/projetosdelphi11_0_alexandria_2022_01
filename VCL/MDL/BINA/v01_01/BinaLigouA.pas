unit BinaLigouA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmBinaLigouA = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAtende: TBitBtn;
    DBGrid1: TDBGrid;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    BtArquiva: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtAtendeClick(Sender: TObject);
    procedure BtArquivaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmBinaLigouA: TFmBinaLigouA;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModBina;

{$R *.DFM}

procedure TFmBinaLigouA.BtArquivaClick(Sender: TObject);
  procedure CancelaChamada(Qry: TmySQLQuery);
  var
    //Codigo: Integer;
    CodTxt, xData, xHora, xFone, IP: String;
    Usuario, Terminal, Descartou: Integer;
  begin
    //Codigo := Qry.FieldByName('Codigo').AsInteger;
    CodTxt := Geral.FF0(Qry.FieldByName('Codigo').AsInteger);
    xData := Qry.FieldByName('xData').AsString;
    xHora := Qry.FieldByName('xHora').AsString;
    xFone := Qry.FieldByName('Nome').AsString;
    //
    DmModBina.QrBinaAB.Close;
    DmModBina.QrBinaAB.Database := Dmod.MyDB;
    //
    UMyMod.ExecutaMySQLQuery1(DmModBina.QrBinaAB, [
      'LOCK TABLES binaligoua WRITE, binaligoub WRITE',
      '']);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DmModBina.QrBinaAB, Dmod.MyDB, [
    'SELECT * ',
    'FROM binaligoua ',
    'WHERE Codigo=' + CodTxt,
    '']);
    //
    if DmModBina.QrBinaAB.RecordCount > 0 then
    begin
      //
      if DmModBina.FCamposBinaLigou = '' then
      begin
        DmModBina.FCamposBinaLigou :=
          UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, 'binaligoub', '');
      end;
      UMyMod.ExecutaMySQLQuery1(DmModBina.QrBinaAB, [
      'INSERT INTO binaligoub ',
      'SELECT ' + DmModBina.FCamposBinaLigou,
      'FROM binaligoua ',
      'WHERE Codigo=' + CodTxt,
      '; ',
      'DELETE FROM binaligoua ',
      'WHERE Codigo=' + CodTxt,
      '; ',
      '']);
      //
      Usuario        := VAR_USUARIO;
      Terminal       := VAR_TERMINAL;
      DmModBina.IdIPWatch1.Active := True;
      IP             := DmModBina.IdIPWatch1.LocalIP;
      Descartou      := 1;
      DmModBina.IdIPWatch1.Active := False;
      //
      UMyMod.SQLInsUpd(DmModBina.QrBinaAB, stUpd, 'binaligoub', False, [
      'Usuario', 'Terminal', 'IP', 'Descartou'], [
      'Codigo'], [
      Usuario, Terminal, IP, Descartou], [
      CodTxt], True);
      //
      UMyMod.ExecutaMySQLQuery1(DmModBina.QrBinaAB, [
      'UNLOCK TABLES; ',
      '']);
      //
    end else
    begin
      UMyMod.ExecutaMySQLQuery1(DmModBina.QrBinaAB, [
      'UNLOCK TABLES; ',
      '']);
      Geral.MB_Aviso('A chamada selecionada j� foi atendida!');
    end;
  end;
var
  //PrdGrupTip, Nivel1,
  I: Integer;
begin
  if DBGrid1.SelectedRows.Count > 1 then
  begin
    if Geral.MB_Pergunta('Confirma o n�o atendimento das ' + IntToStr(
    DBGrid1.SelectedRows.Count) + ' chamadas selecionadas?') = ID_YES then
    begin
      with DBGrid1.DataSource.DataSet do
      for I := 0 to DBGrid1.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGrid1.SelectedRows.Items[I]));
        CancelaChamada(DmModBina.QrBinaLigouA);
      end;
    end;
    DmModBina.ReopenBinaLigouA();
  end else
  begin
    if Geral.MB_Pergunta('Confirma o n�o atendimento das ' + IntToStr(
    DBGrid1.SelectedRows.Count) + ' chamadas selecionadas?') = ID_YES then
    begin
      CancelaChamada(DmModBina.QrBinaLigouA);
      DmModBina.ReopenBinaLigouA();
    end;
  end;
end;

procedure TFmBinaLigouA.BtAtendeClick(Sender: TObject);
begin
  Close;
  DmModBina.AtendeTelefone(DmModBina.QrBinaLigouA);
end;

procedure TFmBinaLigouA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBinaLigouA.DBGrid1DblClick(Sender: TObject);
begin
  BtAtendeClick(Self);
end;

procedure TFmBinaLigouA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBinaLigouA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DBGrid1.DataSource := DmModBina.DsBinaLigouA;
end;

procedure TFmBinaLigouA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
