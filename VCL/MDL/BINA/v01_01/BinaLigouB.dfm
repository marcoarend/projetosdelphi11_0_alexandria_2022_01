object FmBinaLigouB: TFmBinaLigouB
  Left = 339
  Top = 185
  Caption = 'TEL-CALID-002 :: Chamadas Atendidas'
  ClientHeight = 535
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 258
        Height = 32
        Caption = 'Chamadas Atendidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 258
        Height = 32
        Caption = 'Chamadas Atendidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 258
        Height = 32
        Caption = 'Chamadas Atendidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 373
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 280
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 373
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 280
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 373
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 280
        object DBGrid1: TDBGrid
          Left = 181
          Top = 15
          Width = 601
          Height = 356
          Align = alClient
          DataSource = DsBinaLigouB
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataCad'
              Title.Caption = 'Data Cad.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'dData'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'xHora'
              Title.Caption = 'Hora'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TELEFONE'
              Title.Caption = 'Telefone'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IP'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TXT_DESCARTOU'
              Title.Caption = 'Descartou?'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 179
          Height = 356
          Align = alLeft
          TabOrder = 1
          ExplicitHeight = 263
          object GroupBox2: TGroupBox
            Left = 1
            Top = 1
            Width = 177
            Height = 104
            Align = alTop
            TabOrder = 0
            object CkLimit: TCheckBox
              Left = 12
              Top = 12
              Width = 53
              Height = 17
              Caption = 'Limitar:'
              Checked = True
              State = cbChecked
              TabOrder = 0
              OnClick = CkLimitClick
            end
            object EdLimitIni: TdmkEdit
              Left = 12
              Top = 32
              Width = 76
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdLimitIniChange
            end
            object EdLimitQtd: TdmkEdit
              Left = 96
              Top = 32
              Width = 76
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '10'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 10
              ValWarn = False
              OnChange = EdLimitQtdChange
            end
            object SpeedButton1: TBitBtn
              Tag = 1
              Left = 12
              Top = 56
              Width = 40
              Height = 40
              Cursor = crHandPoint
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnClick = SpeedButton1Click
            end
            object SpeedButton2: TBitBtn
              Tag = 2
              Left = 52
              Top = 56
              Width = 40
              Height = 40
              Cursor = crHandPoint
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 4
              OnClick = SpeedButton2Click
            end
            object SpeedButton3: TBitBtn
              Tag = 3
              Left = 92
              Top = 56
              Width = 40
              Height = 40
              Cursor = crHandPoint
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 5
              OnClick = SpeedButton3Click
            end
            object SpeedButton4: TBitBtn
              Tag = 4
              Left = 132
              Top = 56
              Width = 40
              Height = 40
              Cursor = crHandPoint
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 6
              OnClick = SpeedButton4Click
            end
          end
          object dmkRadioGroup1: TdmkRadioGroup
            Left = 1
            Top = 105
            Width = 177
            Height = 48
            Align = alTop
            Caption = ' Descatou: '
            Columns = 3
            ItemIndex = 2
            Items.Strings = (
              'N'#227'o'
              'Sim'
              'Ambos')
            TabOrder = 1
            OnClick = dmkRadioGroup1Click
            UpdType = utYes
            OldValor = 0
          end
          object Panel6: TPanel
            Left = 1
            Top = 153
            Width = 177
            Height = 48
            Align = alTop
            TabOrder = 2
            object Label1: TLabel
              Left = 8
              Top = 4
              Width = 29
              Height = 13
              Caption = 'Login:'
            end
            object EdLogin: TdmkEdit
              Left = 8
              Top = 20
              Width = 161
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdLoginChange
            end
          end
          object Panel7: TPanel
            Left = 1
            Top = 201
            Width = 177
            Height = 48
            Align = alTop
            TabOrder = 3
            object Label2: TLabel
              Left = 8
              Top = 4
              Width = 13
              Height = 13
              Caption = 'IP:'
            end
            object EdIP: TdmkEdit
              Left = 8
              Top = 20
              Width = 161
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdIPChange
            end
          end
          object Panel8: TPanel
            Left = 1
            Top = 249
            Width = 177
            Height = 48
            Align = alTop
            TabOrder = 4
            ExplicitLeft = 21
            ExplicitTop = 301
            object Label3: TLabel
              Left = 8
              Top = 4
              Width = 76
              Height = 13
              Caption = 'Parcial telefone:'
            end
            object EdNome: TdmkEdit
              Left = 8
              Top = 20
              Width = 161
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdIPChange
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 421
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 328
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 465
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 372
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAtende: TBitBtn
        Tag = 14
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Atender'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAtendeClick
      end
      object BtReabrir: TBitBtn
        Tag = 18
        Left = 10
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Reabrir'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtReabrirClick
      end
    end
  end
  object QrQtd: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 284
    Top = 84
    object QrQtdItens: TLargeintField
      FieldName = 'Itens'
    end
  end
  object QrBinaLigouB: TmySQLQuery
    Database = DModG.AllID_DB
    OnCalcFields = QrBinaLigouBCalcFields
    SQL.Strings = (
      'SELECT blb.*, sen.Login,'
      'ELT(blb.Descartou + 1, "N'#227'o", "Sim", "???") TXT_DESCARTOU  '
      'FROM binaligoub blb'
      'LEFT JOIN senhas sen ON sen.Numero=blb.Usuario'
      'ORDER BY Codigo Desc'
      'LIMIT 3000, 10')
    Left = 360
    Top = 84
    object QrBinaLigouBCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrBinaLigouBNome: TWideStringField
      FieldName = 'Nome'
    end
    object QrBinaLigouBxData: TWideStringField
      FieldName = 'xData'
      Size = 10
    end
    object QrBinaLigouBxHora: TWideStringField
      FieldName = 'xHora'
      Size = 10
    end
    object QrBinaLigouBdData: TDateField
      FieldName = 'dData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBinaLigouBdHora: TTimeField
      DefaultExpression = '00:00'
      FieldName = 'dHora'
    end
    object QrBinaLigouBUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrBinaLigouBTerminal: TIntegerField
      FieldName = 'Terminal'
    end
    object QrBinaLigouBIP: TWideStringField
      FieldName = 'IP'
      Size = 60
    end
    object QrBinaLigouBForcado: TSmallintField
      FieldName = 'Forcado'
    end
    object QrBinaLigouBLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBinaLigouBDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBinaLigouBDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBinaLigouBUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBinaLigouBUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBinaLigouBAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrBinaLigouBAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBinaLigouBTELEFONE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TELEFONE'
      Size = 30
      Calculated = True
    end
    object QrBinaLigouBDescartou: TSmallintField
      FieldName = 'Descartou'
    end
    object QrBinaLigouBLogin: TWideStringField
      FieldName = 'Login'
      Size = 30
    end
    object QrBinaLigouBTXT_DESCARTOU: TWideStringField
      FieldName = 'TXT_DESCARTOU'
      Size = 3
    end
  end
  object DsBinaLigouB: TDataSource
    DataSet = QrBinaLigouB
    Left = 360
    Top = 132
  end
end
