object FmDirectTerminal: TFmDirectTerminal
  Left = 339
  Top = 185
  Caption = 'ATD-TELEF-000 :: BINA'
  ClientHeight = 631
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 142
        Height = 32
        Caption = 'BINA Direto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 142
        Height = 32
        Caption = 'BINA Direto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 142
        Height = 32
        Caption = 'BINA Direto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 469
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 469
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 469
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 242
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 148
            Height = 242
            Align = alLeft
            Caption = ' Sele'#231#227'o da comunica'#231#227'o: '
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 24
              Width = 54
              Height = 13
              Caption = 'Dispositivo:'
            end
            object nrSemaphore1: TnrSemaphore
              Left = 23
              Top = 97
              Width = 13
              Height = 13
              StretchImage = False
              Blink = False
              BlinkInterval = 100
              Checked = False
              Images = ImageList1
              ImageChecked = 0
              ImageUnChecked = 1
              Pulse = False
              PulseDuration = 1000
              Style3D = nrs3DNone
              Transparent = True
              Color3DCheck = clBlue
              Color3DUnCheck = clRed
              Frame3D = False
            end
            object nrSemaphore2: TnrSemaphore
              Left = 51
              Top = 97
              Width = 13
              Height = 13
              StretchImage = False
              Blink = False
              BlinkInterval = 100
              Checked = False
              Images = ImageList1
              ImageChecked = 0
              ImageUnChecked = 1
              Pulse = False
              PulseDuration = 1000
              Style3D = nrs3DNone
              Transparent = True
              Color3DCheck = clBlue
              Color3DUnCheck = clRed
              Frame3D = False
            end
            object nrSemaphore3: TnrSemaphore
              Left = 79
              Top = 97
              Width = 13
              Height = 13
              StretchImage = False
              Blink = False
              BlinkInterval = 100
              Checked = False
              Images = ImageList1
              ImageChecked = 0
              ImageUnChecked = 1
              Pulse = False
              PulseDuration = 1000
              Style3D = nrs3DNone
              Transparent = True
              Color3DCheck = clBlue
              Color3DUnCheck = clRed
              Frame3D = False
            end
            object nrSemaphore4: TnrSemaphore
              Left = 103
              Top = 97
              Width = 13
              Height = 13
              StretchImage = False
              Blink = False
              BlinkInterval = 100
              Checked = False
              Images = ImageList1
              ImageChecked = 0
              ImageUnChecked = 1
              Pulse = False
              PulseDuration = 1000
              Style3D = nrs3DNone
              Transparent = True
              Color3DCheck = clBlue
              Color3DUnCheck = clRed
              Frame3D = False
            end
            object Label2: TLabel
              Left = 12
              Top = 172
              Width = 26
              Height = 13
              Caption = 'Data:'
            end
            object Label3: TLabel
              Left = 12
              Top = 196
              Width = 26
              Height = 13
              Caption = 'Hora:'
            end
            object Label7: TLabel
              Left = 12
              Top = 220
              Width = 27
              Height = 13
              Caption = 'Fone:'
            end
            object NrDeviceBox1: TnrDeviceBox
              Left = 9
              Top = 41
              Width = 130
              Height = 21
              nrComm = nrComm1
              ResetOnChanged = False
              TabOrder = 0
              OnDblClick = NrDeviceBox1DblClick
            end
            object CkAtivar: TCheckBox
              Left = 9
              Top = 73
              Width = 59
              Height = 14
              Caption = 'Ativar'
              TabOrder = 1
              OnClick = CkAtivarClick
            end
            object Button2: TButton
              Left = 9
              Top = 121
              Width = 128
              Height = 40
              Caption = 'Configurar'
              TabOrder = 2
              OnClick = Button2Click
            end
            object EdData: TdmkEdit
              Left = 40
              Top = 168
              Width = 33
              Height = 21
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdHora: TdmkEdit
              Left = 40
              Top = 192
              Width = 53
              Height = 21
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdFone: TdmkEdit
              Left = 40
              Top = 216
              Width = 97
              Height = 21
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdFoneChange
              OnExit = EdFoneExit
            end
          end
          object GroupBox3: TGroupBox
            Left = 148
            Top = 0
            Width = 280
            Height = 242
            Align = alLeft
            Caption = ' Terminal: '
            TabOrder = 1
            object Memo1: TMemo
              Left = 2
              Top = 61
              Width = 276
              Height = 179
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Terminal'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              OnChange = Memo1Change
            end
            object Panel7: TPanel
              Left = 2
              Top = 15
              Width = 276
              Height = 46
              Align = alTop
              TabOrder = 1
              object Label12: TLabel
                Left = 4
                Top = 4
                Width = 94
                Height = 13
                Caption = 'Comando AT direto:'
              end
              object EdCmd: TEdit
                Left = 7
                Top = 21
                Width = 185
                Height = 21
                TabOrder = 0
                Text = 'AT'
              end
              object BtSendCommand: TButton
                Left = 193
                Top = 17
                Width = 75
                Height = 25
                Caption = 'Envia'
                TabOrder = 1
                OnClick = BtSendCommandClick
              end
            end
          end
          object GroupBox4: TGroupBox
            Left = 428
            Top = 0
            Width = 352
            Height = 242
            Align = alClient
            Caption = ' Logging: '
            TabOrder = 2
            object Memo2: TMemo
              Left = 2
              Top = 15
              Width = 348
              Height = 225
              Align = alClient
              Color = clMenu
              Lines.Strings = (
                ''
                '[05.02.02 12:28:51] The port COM1 succesful opened'
                '[05.02.02 12:28:51] The port succesful closed')
              ReadOnly = True
              TabOrder = 0
            end
          end
        end
        object Memo3: TMemo
          Left = 226
          Top = 257
          Width = 378
          Height = 210
          Align = alClient
          TabOrder = 1
        end
        object GroupBox6: TGroupBox
          Left = 2
          Top = 257
          Width = 224
          Height = 210
          Align = alLeft
          TabOrder = 2
          object Panel6: TPanel
            Left = 2
            Top = 15
            Width = 220
            Height = 82
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label6: TLabel
              Left = 7
              Top = 5
              Width = 203
              Height = 13
              Caption = 'Envio de Arquivo ou arquivos por m'#225'scara:'
            end
            object EdArquivo: TdmkEdit
              Left = 8
              Top = 21
              Width = 176
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'c:\*.bat'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'c:\*.bat'
              ValWarn = False
            end
            object BtSend: TButton
              Left = 7
              Top = 50
              Width = 61
              Height = 20
              Caption = 'Envia'
              Enabled = False
              TabOrder = 1
              OnClick = BtSendClick
            end
            object Button1: TButton
              Left = 186
              Top = 21
              Width = 21
              Height = 20
              Caption = '...'
              TabOrder = 2
              OnClick = Button1Click
            end
            object BtBreak: TButton
              Left = 124
              Top = 50
              Width = 60
              Height = 20
              Caption = 'P'#225'ra'
              Enabled = False
              TabOrder = 3
              OnClick = BtBreakClick
            end
          end
          object GroupBox5: TGroupBox
            Left = 2
            Top = 97
            Width = 220
            Height = 111
            Align = alClient
            Caption = ' Status '
            TabOrder = 1
            object Label4: TLabel
              Left = 2
              Top = 80
              Width = 9
              Height = 13
              Align = alBottom
              Caption = '...'
            end
            object Label5: TLabel
              Left = 2
              Top = 67
              Width = 9
              Height = 13
              Align = alBottom
              Caption = '...'
            end
            object ProgressBar1: TProgressBar
              Left = 2
              Top = 93
              Width = 216
              Height = 16
              Align = alBottom
              TabOrder = 0
            end
            object chkReceive: TCheckBox
              Left = 104
              Top = 17
              Width = 102
              Height = 14
              Alignment = taLeftJustify
              Caption = 'Auto recebimento'
              TabOrder = 1
              OnClick = chkReceiveClick
            end
          end
        end
        object ListBox1: TListBox
          Left = 604
          Top = 257
          Width = 178
          Height = 210
          Align = alRight
          ItemHeight = 13
          Items.Strings = (
            'AT#CID=1'
            'AT#CLS=8#CID=1'
            'AT#CID=2'
            'AT%CCID=1'
            'AT%CCID=2'
            'AT+VCID=1'
            'AT#CC1'
            'AT*ID1')
          TabOrder = 3
          OnDblClick = ListBox1DblClick
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 517
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 561
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Oculta'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label8: TLabel
        Left = 140
        Top = 4
        Width = 54
        Height = 13
        Caption = 'Dispositivo:'
      end
      object Label9: TLabel
        Left = 303
        Top = 4
        Width = 123
        Height = 13
        Caption = 'Comando de inicializa'#231#227'o:'
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Salva (reg)'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object EdDispositivo: TdmkEdit
        Left = 140
        Top = 20
        Width = 157
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdComando: TdmkEdit
        Left = 303
        Top = 20
        Width = 158
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGCalIDServr: TRadioGroup
        Left = 467
        Top = 4
        Width = 163
        Height = 37
        Caption = ' Este '#233' o servidor de Caller ID: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 3
      end
    end
  end
  object nrLogFile1: TnrLogFile
    SizeLimit = 2048
    FileName = 'nrcommlib.log'
    VisualOut = Memo2
    DetailLevel = dlDetail
    Options = [loSafeMode, loAutoFileName]
    AnsiOnly = True
    Left = 240
    Top = 12
  end
  object nrComm1: TnrComm
    Log = nrLogFile1
    Active = False
    BaudRate = 9600
    Parity = pNone
    StopBits = sbOne
    ByteSize = 8
    ComPortNo = 1
    ComPort = cpCOM1
    TraceStates = [tsRxChar, tsRxFlag, tsTxEmpty, tsCTS, tsDSR, tsRLSD, tsBreak, tsErr, tsRing, tsRx80Full, tsEvent1, tsEvent2]
    EventChar = #0
    StreamProtocol = spXonXoff
    DataProcessor = nrZModem1
    BufferInSize = 4096
    BufferOutSize = 4096
    TimeoutRead = 0
    TimeoutWrite = 100
    RS485Mode = False
    EnumPorts = epQuickAll
    UseMainThread = True
    Terminal = Memo1
    TerminalUsage = tuBoth
    TerminalEcho = True
    MonitorCTS = nrSemaphore1
    MonitorDSR = nrSemaphore2
    MonitorRLSD = nrSemaphore4
    MonitorRing = nrSemaphore3
    Left = 208
    Top = 12
  end
  object nrZModem1: TnrZModem
    AbortNoCarrier = False
    Active = True
    ErrorLimit = 255
    DefaultPath = 'c:\'
    Options = [zoCreateWithNewName]
    TimeOut = 10000
    TimeOutRetries = 3
    Log = nrLogFile1
    OnBeforeReceived = nrZModem1BeforeReceived
    OnProgress = nrZModem1Progress
    OnSuccessfulEnd = nrZModem1SuccessfulEnd
    OnAbort = nrZModem1Abort
    Left = 272
    Top = 12
  end
  object ImageList1: TImageList
    Left = 304
    Top = 12
    Bitmap = {
      494C0101020028006C0010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002FC82F0000C2000000C3000000C200002FC82F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002F2FB9000000B2000000B3000000B2002F2FB900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002FC8
      2F0000C3000000C8000000CE000000D0000000CE000000C8000000C300002FC8
      2F00000000000000000000000000000000000000000000000000000000002F2F
      B9000000B4000000BA000000C2000000C4000000C2000000BA000000B4002F2F
      B900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000002FC82F0000C4
      000000CE000000D8000000E1000000E4000000E1000000D8000000CE000000C4
      00002FC82F0000000000000000000000000000000000000000002F2FB9000000
      B5000000C2000000CE000000DA000000DD000000DA000000CE000000C2000000
      B5002F2FB9000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000C3000000CE
      000000DD000000E9000000F3000000F4000000F3000000E9000000DD000000CE
      000000C3000000000000000000000000000000000000000000000000B4000000
      C2000000D5000000E4000000EF000000F1000000EF000000E4000000D5000000
      C2000000B4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000002FC82F0000C8000000D8
      000000E9000000F6000000FD000000FD000000FD000000F6000000E9000000D8
      000000C800002FC82F000000000000000000000000002F2FB9000000BA000000
      CE000000E4000000F3000000FC000000FC000000FC000000F3000000E4000000
      CE000000BA002F2FB90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000C2000000CE000000E1
      000000F3000000FD000000FF000000FF000000FF000000FD000000F3000000E1
      000000CE000000C200000000000000000000000000000000B2000000C2000000
      DA000000EF000000FC000000FF000000FF000000FF000000FC000000EF000000
      DA000000C2000000B20000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000C3000000D0000000E4
      000000F4000000FD000000FF000000FF000000FF000000FD000000F4000000E4
      000000D0000000C300000000000000000000000000000000B3000000C4000000
      DD000000F1000000FC000000FF000000FF000000FF000000FC000000F1000000
      DD000000C4000000B30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000C2000000CE000000E1
      000000F3000000FD000000FF000000FF000000FF000000FD000000F3000000E1
      000000CE000000C200000000000000000000000000000000B2000000C2000000
      DA000000EF000000FC000000FF000000FF000000FF000000FC000000EF000000
      DA000000C2000000B20000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000002FC82F0000C8000000D8
      000000E9000000F6000000FD000000FD000000FD000000F6000000E9000000D8
      000000C800002FC82F000000000000000000000000002F2FB9000000BA000000
      CE000000E4000000F3000000FC000000FC000000FC000000F3000000E4000000
      CE000000BA002F2FB90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000C3000000CE
      000000DD000000E9000000F3000000F4000000F3000000E9000000DD000000CE
      000000C3000000000000000000000000000000000000000000000000B4000000
      C2000000D5000000E4000000EF000000F1000000EF000000E4000000D5000000
      C2000000B4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000002FC82F0000C4
      000000CE000000D8000000E1000000E4000000E1000000D8000000CE000000C4
      00002FC82F0000000000000000000000000000000000000000002F2FB9000000
      B5000000C2000000CE000000DA000000DD000000DA000000CE000000C2000000
      B5002F2FB9000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002FC8
      2F0000C3000000C8000000CE000000D0000000CE000000C8000000C300002FC8
      2F00000000000000000000000000000000000000000000000000000000002F2F
      B9000000B4000000BA000000C2000000C4000000C2000000BA000000B4002F2F
      B900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002FC82F0000C2000000C3000000C200002FC82F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002F2FB9000000B2000000B3000000B2002F2FB900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      F83FF83F00000000E00FE00F00000000C007C00700000000C007C00700000000
      8003800300000000800380030000000080038003000000008003800300000000
      8003800300000000C007C00700000000C007C00700000000E00FE00F00000000
      F83FF83F00000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = Timer1Timer
    Left = 336
    Top = 12
  end
  object QrWake: TmySQLQuery
    Database = Dmod.MyDB
    Left = 76
    Top = 148
  end
end
