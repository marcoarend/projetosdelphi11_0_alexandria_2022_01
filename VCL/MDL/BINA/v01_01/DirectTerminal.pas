unit DirectTerminal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, nrclasses, nrcomm, nrcommbox, nrzmodem, nrlogfile, nrsemaphore,
  ImgList, UnDmkEnums;

type
  TFmDirectTerminal = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    NrDeviceBox1: TnrDeviceBox;
    CkAtivar: TCheckBox;
    Button2: TButton;
    GroupBox3: TGroupBox;
    Memo1: TMemo;
    GroupBox4: TGroupBox;
    Memo2: TMemo;
    Memo3: TMemo;
    nrSemaphore1: TnrSemaphore;
    nrSemaphore2: TnrSemaphore;
    nrSemaphore3: TnrSemaphore;
    nrSemaphore4: TnrSemaphore;
    nrLogFile1: TnrLogFile;
    nrComm1: TnrComm;
    nrZModem1: TnrZModem;
    GroupBox6: TGroupBox;
    Panel6: TPanel;
    Label6: TLabel;
    EdArquivo: TdmkEdit;
    BtSend: TButton;
    Button1: TButton;
    BtBreak: TButton;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    ProgressBar1: TProgressBar;
    chkReceive: TCheckBox;
    ImageList1: TImageList;
    Label2: TLabel;
    EdData: TdmkEdit;
    EdHora: TdmkEdit;
    Label3: TLabel;
    Label7: TLabel;
    EdFone: TdmkEdit;
    Timer1: TTimer;
    Panel7: TPanel;
    Label12: TLabel;
    EdCmd: TEdit;
    BtSendCommand: TButton;
    ListBox1: TListBox;
    Label8: TLabel;
    EdDispositivo: TdmkEdit;
    Label9: TLabel;
    EdComando: TdmkEdit;
    RGCalIDServr: TRadioGroup;
    QrWake: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkAtivarClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure nrZModem1Abort(Sender: TObject; Error: TzAbortType);
    procedure nrZModem1BeforeReceived(Sender: TObject; var FileName: string;
      var flSkip: Boolean);
    procedure nrZModem1SuccessfulEnd(Sender: TObject);
    procedure nrZModem1Progress(Sender: TObject);
    procedure chkReceiveClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtSendClick(Sender: TObject);
    procedure BtBreakClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure EdFoneChange(Sender: TObject);
    procedure EdFoneExit(Sender: TObject);
    procedure BtSendCommandClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure NrDeviceBox1DblClick(Sender: TObject);
  private
    { Private declarations }
    FMSGs, FCalIDServr: Integer;
    FDispositivo, FComando: String;
    //
    procedure FoneTocou(Forcado: Integer);
  public
    { Public declarations }
  end;

  var
  FmDirectTerminal: TFmDirectTerminal;

implementation

uses
{$IfNDef NAO_DIAR} UnDiario_PF, {$EndIf}
UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc;

{$R *.DFM}

procedure TFmDirectTerminal.BtSendCommandClick(Sender: TObject);
begin
  nrComm1.SendString(EdCmd.Text+#13);
  Memo3.Text := NrComm1.ReadString;
end;                           
procedure TFmDirectTerminal.BtBreakClick(Sender: TObject);
begin
  Label5.Caption:='Aborted';
  nrZModem1.Abort;
  BtBreak.Enabled:=false;
end;

procedure TFmDirectTerminal.BtSendClick(Sender: TObject);
begin
  Label5.Caption:='Sending ...';
  nrZModem1.Send(EdArquivo.Text);
  BtSend.Enabled:=False;
  BtBreak.Enabled:=True;
  Label4.Caption:=nrZModem1.FileName;
end;

procedure TFmDirectTerminal.BtOKClick(Sender: TObject);
begin
{
  Imagem := Geral.ReadAppKey(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg', HKEY_LOCAL_MACHINE);
}
  Geral.WriteAppKeyLM('Dispositivo', 'Dermatek\Terminal', EdDispositivo.Text, ktString);
  Geral.WriteAppKeyLM('Comando', 'Dermatek\Terminal', EdComando.Text, ktString);
  Geral.WriteAppKeyLM('CalIDServr', 'Dermatek\Terminal', RGCalIDServr.ItemIndex, ktInteger);
end;

{
procedure TFmDirectTerminal.BtOKClick(Sender: TObject);
  procedure Msg(Texto: String);
  begin
     Memo3.Lines.Add(FormatDateTime('hh:nn:ss', Now()) + ': ' + Texto);
  end;
  function Verifica(Contem, Linha: String; Posicao: Integer): Boolean;
  begin
    Result := pos(Contem, Linha) > 0;
    Msg(Linha);
  end;
  function Envia(Comando: String): Boolean;
  begin
    Msg(Comando);
    nrComm1.SendString(FComando + #13);
  end;
var
  I: Integer;
begin
  Memo3.Lines.Clear;
  try
    Msg('============ IN�CIO DO RASTREAMENTO ===========');
    nrComm1.Terminal := Memo3;
    //nrLogFile1.VisualOut  := nil;
    if NrDeviceBox1.Items.Count = 0 then
    begin
      Msg('Nenhuma porta v�lida foi localizada!');
      Exit;
    end;
    Msg('Portas localizadas: ' + IntToStr(NrDeviceBox1.Items.Count));
    Msg('+++++++++++++++++++++++++++++++++++++++++++++++');
    for I := 0 to NrDeviceBox1.Items.Count - 1 do
    begin
      try
        nrComm1.Terminal := nil;
        NrComm1.Active := False;
        Msg('Porta: ' + NrDeviceBox1.Items[I]);
        //
        NrDeviceBox1.ItemIndex := I;
        nrComm1.SetDeviceIndex(I);
        NrComm1.Active := True;
        Envia('ATZ');
        if Verifica('OK', NrComm1.ReadString, 1) then
        begin
          Envia('AT');
          if Verifica('OK', NrComm1.ReadString, 1) then
          begin
            // AT#CID=1
            Envia('AT#CID=1');
            if Verifica('OK', NrComm1.ReadString, 1) then
              Exit;
            // AT#CLS=8#CID=1
            Envia('AT#CLS=8#CID=1');
            if Verifica('OK', NrComm1.ReadString, 1) then
              Exit;
            // AT+VCID=1
            Envia('AT+VCID=1');
            if Verifica('OK', NrComm1.ReadString, 1) then
              Exit;
            // AT#CID=2
            Envia('AT#CID=2');
            if Verifica('OK', NrComm1.ReadString, 1) then
              Exit;
            // AT%CCID=1
            Envia('AT%CCID=1');
            if Verifica('OK', NrComm1.ReadString, 1) then
              Exit;
            // AT%CCID=2
            Envia('AT%CCID=2');
            if Verifica('OK', NrComm1.ReadString, 1) then
              Exit;
            // AT#CC1
            Envia('AT#CC1');
            if Verifica('OK', NrComm1.ReadString, 1) then
              Exit;
            // AT*ID1
            Envia('AT*ID1');
            if Verifica('OK', NrComm1.ReadString, 1) then
              Exit;
            //
            //
            NrDeviceBox1.ItemIndex := -1;
          end;
        end;
        //
      except
        ; // nada
      end;
    end;
    nrComm1.Terminal := Memo1;
    nrLogFile1.VisualOut  := Memo2;
  finally
      //nrComm1.Terminal := Memo1;
      //nrLogFile1.VisualOut  := Memo2;
      //
      Msg('============ FIM DO RASTREAMENTO ============');
  end;
end;
}

procedure TFmDirectTerminal.BtSaidaClick(Sender: TObject);
begin
  Hide;
end;

procedure TFmDirectTerminal.Button1Click(Sender: TObject);
begin
 MyObjects.DefineArquivo1(Self, EdArquivo);
end;

procedure TFmDirectTerminal.Button2Click(Sender: TObject);
begin
  nrComm1.ConfigDialog;
end;

procedure TFmDirectTerminal.CkAtivarClick(Sender: TObject);
begin
  NrComm1.Active := CkAtivar.Checked;
  if NrComm1.Active then
  begin
{
    AT#CID=1
    AT#CLS=8#CID=1
    AT#CID=2
    AT%CCID=1
    AT%CCID=2
    AT+VCID=1
    AT#CC1
    AT*ID1
}
    nrComm1.SendString(FComando + #13);
    Memo3.Text := NrComm1.ReadString;
  end;
  BtSend.Enabled := NrComm1.Active;
  BtBreak.Enabled := False;
end;

procedure TFmDirectTerminal.EdFoneChange(Sender: TObject);
begin
  if (EdFone.Text <> '') and (not EdFone.Focused) then
    FoneTocou(0);
end;

procedure TFmDirectTerminal.EdFoneExit(Sender: TObject);
begin
  if Length(Trim(EdFone.Text)) > 0 then
    FoneTocou(1);
end;

procedure TFmDirectTerminal.chkReceiveClick(Sender: TObject);
begin
   nrZModem1.Active:=chkReceive.Checked;
end;

procedure TFmDirectTerminal.FoneTocou(Forcado: Integer);
var
  Data, Hora, Fone: String;
begin
  // Evitar erro mysql dormindo
  // mysql server has gone away
  try
    if DMod.MyDB.Connected = False then
      DMod.MyDB.Connect;
    UnDmkDAC_PF.AbreMySQLQuery0(QrWake, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM Controle',
    '']);
    QrWake.Close;
  except
    // Eu espero que o my sql tenha retornado
  end;
  Data := EdData.Text;
  Hora := EdHora.Text;
  Fone := EdFone.Text;
  EdData.Text := '';
  EdHora.Text := '';
  EdFone.Text := '';
  Memo3.Text := '';
  //if FmDirectTerminal.Visible then
  FmDirectTerminal.Hide;
{$IfNDef NAO_DIAR}
  Diario_PF.MostraDiarioAddByFoneComm(Data, Hora, Fone,
    RGCalIDServr.ItemIndex = 1, Forcado);
{$Else}
  dmkPF.InfoSemModulo(mdlappDiario);
{$EndIf}
end;

procedure TFmDirectTerminal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDirectTerminal.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  Fechar: TBasicAction;
begin
  if FmDirectTerminal.Visible then
  begin
    FmDirectTerminal.Hide;
    Fechar := TBasicAction(caNone);
    FmDirectTerminal.Action := Fechar;
  end;
end;

procedure TFmDirectTerminal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  Timer1.Enabled := True;
end;

procedure TFmDirectTerminal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDirectTerminal.ListBox1DblClick(Sender: TObject);
begin
  nrComm1.SendString(ListBox1.Items[ListBox1.ItemIndex]+#13);
  Memo3.Text := NrComm1.ReadString;
end;

procedure TFmDirectTerminal.Memo1Change(Sender: TObject);
var
  N: Integer;
  Texto, Msg: String;
begin
  FMSGs := FMSGs + 1;
  Texto := Memo1.Text;
  if pos('NMBR', Texto) > 0 then
  begin
    Msg := '';
    EdFone.Text := '';
    EdData.Text := '';
    EdHora.Text := '';
    //
    n := pos('DATE', Texto);
    if n > 0 then
      //Msg := Msg + 'Data: ' + Copy(Texto, n + 5, 4) + sLineBreak;
      EdData.Text := Copy(Texto, n + 5, 4);
    //
    n := pos('TIME', Texto);
    if n > 0 then
      //Msg := Msg + 'Hora: ' + Copy(Texto, n + 5, 4) + sLineBreak;
      EdHora.Text := Copy(Texto, n + 5, 4);
    //
    n := pos('NMBR', Texto);
    //Msg := Msg + 'Fone: ' + Copy(Texto, n + 5) + sLineBreak;
    EdFone.Text := Geral.SoNumero_TT(Copy(Texto, n + 5));
    //
    //Show;
  end;
  Memo3.Lines.Add('#' + IntToStr(FMSGs) + sLineBreak + Texto);
  Memo1.Text := '';
end;

procedure TFmDirectTerminal.NrDeviceBox1DblClick(Sender: TObject);
begin
  EdDispositivo.Text := NrDeviceBox1.Items[NrDeviceBox1.ItemIndex];
end;

procedure TFmDirectTerminal.nrZModem1Abort(Sender: TObject; Error: TzAbortType);
begin
  BtSend.Enabled:=true;
end;

procedure TFmDirectTerminal.nrZModem1BeforeReceived(Sender: TObject;
  var FileName: string; var flSkip: Boolean);
begin
  Label5.Caption:='Receiving ...';
  Label4.Caption:=FileName;
  BtSend.Enabled:=false;
end;

procedure TFmDirectTerminal.nrZModem1Progress(Sender: TObject);
begin
//
end;

procedure TFmDirectTerminal.nrZModem1SuccessfulEnd(Sender: TObject);
begin
  Label5.Caption:='Ready';
  BtSend.Enabled:=true;
end;

procedure TFmDirectTerminal.Timer1Timer(Sender: TObject);
var
  I: Integer;
begin
  Timer1.Enabled := False;
  //
  FDispositivo := Geral.ReadAppKeyLM('Dispositivo', 'Dermatek\Terminal', ktString, '');
  EdDispositivo.Text := FDispositivo;
  //
  FComando := Geral.ReadAppKeyLM('Comando', 'Dermatek\Terminal', ktString, '');
  EdComando.Text := FComando;
  //
  FCalIDServr := Geral.ReadAppKeyLM('CalIDServr', 'Dermatek\Terminal', ktInteger, 0);
  RGCalIDServr.ItemIndex := FCalIDServr;
  //
  if Trim(FDispositivo) <> '' then
  begin
    for I := 0 to NrDeviceBox1.Items.Count - 1 do
    begin
      if Uppercase(NrDeviceBox1.Items[I]) = Uppercase(FDispositivo) then
      begin
        NrDeviceBox1.ItemIndex := I;
        NrDeviceBox1.Text := FDispositivo;
        nrComm1.SetDeviceIndex(I);
        //
        CkAtivar.Checked := False;
        CkAtivar.Checked := True;
        CkAtivarClick(Self);
        //
        Break;
      end;
    end;
  end;
end;

end.
