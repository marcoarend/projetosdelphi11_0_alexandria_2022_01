unit UnDmkACBr_ParamsEmp;

interface

(*
================================================================================
Guia GERAL!
================================================================================
object Label473: TLabel
  Left = 8
  Top = 88
  Width = 86
  Height = 13
  Caption = 'Forma de Emiss'#227'o'
end
object cbFormaEmissao: TComboBox
  Left = 8
  Top = 104
  Width = 248
  Height = 21
  TabOrder = 2
end
object Label475: TLabel
  Left = 8
  Top = 126
  Width = 123
  Height = 13
  Caption = 'Modelo Documento Fiscal'
end
object cbModeloDF: TComboBox
  Left = 8
  Top = 142
  Width = 248
  Height = 21
  TabOrder = 6
end
object Label476: TLabel
  Left = 8
  Top = 165
  Width = 121
  Height = 13
  Caption = 'Vers'#227'o Documento Fiscal'
end
object cbVersaoDF: TComboBox
  Left = 8
  Top = 181
  Width = 248
  Height = 21
  TabOrder = 8
end
object Label477: TLabel
  Left = 8
  Top = 299
  Width = 183
  Height = 13
  Caption = 'IdToken/IdCSC (Somente para NFC-e)'
end
object edtIdToken: TEdit
  Left = 8
  Top = 315
  Width = 248
  Height = 21
  TabOrder = 6
end
object Label478: TLabel
  Left = 7
  Top = 339
  Width = 165
  Height = 13
  Caption = 'Token/CSC (Somente para NFC-e)'
end
object edtToken: TEdit
  Left = 7
  Top = 355
  Width = 248
  Height = 21
  TabOrder = 7
end

================================================================================
Guia WebService!
================================================================================

object Label480: TLabel
  Left = 8
  Top = 16
  Width = 126
  Height = 13
  Caption = 'Selecione UF do Emitente:'
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentFont = False
end
object cbUF: TComboBox
  Left = 8
  Top = 32
  Width = 249
  Height = 24
  Style = csDropDownList
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ItemIndex = 15
  ParentFont = False
  TabOrder = 1
  Text = 'PR'
  Items.Strings = (
    'AC'
    'AL'
    'AP'
    'AM'
    'BA'
    'CE'
    'DF'
    'ES'
    'GO'
    'MA'
    'MT'
    'MS'
    'MG'
    'PA'
    'PB'
    'PR'
    'PE'
    'PI'
    'RJ'
    'RN'
    'RS'
    'RO'
    'RR'
    'SC'
    'SP'
    'SE'
    'TO')
end
object rgTipoAmb: TRadioGroup
  Left = 8
  Top = 61
  Width = 249
  Height = 52
  Caption = 'Selecione o Ambiente de Destino'
  Columns = 2
  ItemIndex = 0
  Items.Strings = (
    'Produ'#231#227'o'
    'Homologa'#231#227'o')
  TabOrder = 2
end

================================================================================
Guia Arquivos!
================================================================================
object Label502: TLabel
  Left = 6
  Top = 116
  Width = 94
  Height = 13
  Caption = 'Pasta Arquivos NFe'
end
object edtPathNFe: TEdit
  Left = 6
  Top = 132
  Width = 235
  Height = 21
  TabOrder = 7
end
object sbPathNFe: TSpeedButton
  Left = 247
  Top = 131
  Width = 23
  Height = 24
  Glyph.Data = {
    76010000424D7601000000000000760000002800000020000000100000000100
    04000000000000010000130B0000130B00001000000000000000000000000000
    800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
    333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
    0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
    07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
    07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
    0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
    33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
    B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
    3BB33773333773333773B333333B3333333B7333333733333337}
  NumGlyphs = 2
end
object Label504: TLabel
  Left = 6
  Top = 159
  Width = 108
  Height = 13
  Caption = 'Pasta Arquivos Evento'
end
object edtPathEvento: TEdit
  Left = 6
  Top = 175
  Width = 235
  Height = 21
  TabOrder = 8
end
object sbPathEvento: TSpeedButton
  Left = 247
  Top = 174
  Width = 23
  Height = 24
  Glyph.Data = {
    76010000424D7601000000000000760000002800000020000000100000000100
    04000000000000010000130B0000130B00001000000000000000000000000000
    800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
    333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
    0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
    07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
    07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
    0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
    33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
    B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
    3BB33773333773333773B333333B3333333B7333333733333337}
  NumGlyphs = 2
end
object Label503: TLabel
  Left = 6
  Top = 202
  Width = 127
  Height = 13
  Caption = 'Pasta Arquivos Inutiliza'#231#227'o'
end
object edtPathInu: TEdit
  Left = 6
  Top = 218
  Width = 235
  Height = 21
  TabOrder = 9
end
object sbPathInu: TSpeedButton
  Left = 247
  Top = 217
  Width = 23
  Height = 24
  Glyph.Data = {
    76010000424D7601000000000000760000002800000020000000100000000100
    04000000000000010000130B0000130B00001000000000000000000000000000
    800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
    333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
    0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
    07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
    07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
    0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
    33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
    B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
    3BB33773333773333773B333333B3333333B7333333733333337}
  NumGlyphs = 2
end
object Label505: TLabel
  Left = 6
  Top = 245
  Width = 95
  Height = 13
  Caption = 'Pasta Arquivos PDF'
end
object edtPathPDF: TEdit
  Left = 6
  Top = 261
  Width = 235
  Height = 21
  TabOrder = 10
end
object sbPathPDF: TSpeedButton
  Left = 247
  Top = 260
  Width = 23
  Height = 24
  Glyph.Data = {
    76010000424D7601000000000000760000002800000020000000100000000100
    04000000000000010000130B0000130B00001000000000000000000000000000
    800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
    333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
    0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
    07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
    07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
    0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
    33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
    B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
    3BB33773333773333773B333333B3333333B7333333733333337}
  NumGlyphs = 2
end
*)
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, ExtCtrls, StdCtrls, Data.DB,
  Spin, Buttons, ComCtrls, OleCtrls, SHDocVw,
  //ACBrPosPrinter, ACBrNFeDANFeESCPOS,
  //ACBrDANFCeFortesFr,
  //ACBrNFeDANFeRLClass,
  //ACBrDANFCeFortesFrA4;
  (* 2023-12-19*)
  //ACBrMail, ACBrNFeDANFEClass,
  //ACBrDFeReport, ACBrDFeDANFeReport,
  ACBrBase, ACBrDFe,
  ACBrUtil,
  ACBrIntegrador,
  (**)
  ACBrNFe, ACBrDFeSSL,
  ShellAPI, XMLIntf, XMLDoc, zlib,
  Frm_SelecionarCertificado,
  DmkGeral, UnDmkEnums, UnDmkACBr_PF, UnMyObjects;

type
  TUnDmkACBr_ParamsEmp = class(TObject)
  private
    { Private declarations }
    procedure LoadXML(ACBrNFeX: TACBrNFe; RetWS: String; MyWebBrowser:
              TWebBrowser);
  public
    { Public declarations }
    procedure AtualizarSSLLibsCombo(ACBrNFeX: TACBrNFe);
{$IfNDef sACBr}
    function  ConfigurarComponente(ACBrNFeX: TACBrNFe): Boolean;
{$EndIf}
    procedure FormCreate_ACBr(ACBrNFeX: TACBrNFe
              (*Sender: TObject; ACBrNFeX: TACBrNFe; cbSSLLib:
              TComboBox; cbCryptLib: TComboBox; cbHttpLib: TComboBox;
              cbXmlSignLib: TComboBox; cbSSLType: TComboBox; cbFormaEmissao:
              TComboBox; cbModeloDF: TComboBox; cbVersaoDF: TComboBox; cbxPorta:
              TComboBox; pgRespostas: TPageControl*));
    procedure LerConfiguracao(ACBrNFeX: TACBrNFe);
    procedure DFe_ConsultaSstatusServico(const ACBrNFeX: TACBrNFe; var
              CodStatus: Integer; var TxtStatus: String);
    procedure NumSerie(ACBrNFeX: TACBrNFe);
    procedure SalvarConfiguracao();
    procedure TestarSHA256_Mais_RSA(ACBrNFeX: TACBrNFe);
    procedure TestarHTTPS_SemCertificado(ACBrNFeX: TACBrNFe);

  end;

  var
  DmkACBr_ParamsEmp: TUnDmkACBr_ParamsEmp;


implementation

{ TUnDmkACBr_ParamsEmp }

uses
  strutils, math, TypInfo, DateUtils, synacode, blcksock, FileCtrl, Grids,
  IniFiles, Printers,
  pcnAuxiliar, pcnNFe, pcnConversao, pcnConversaoNFe, pcnNFeRTXT, pcnRetConsReciDFe,
  ACBrDFeConfiguracoes, (*ACBrDFeSSL,*) ACBrDFeOpenSSL, ACBrDFeUtil,
  ACBrNFeNotasFiscais, ACBrNFeConfiguracoes,
  //Frm_Status, Frm_SelecionarCertificado;
  //Frm_ConfiguraSerial;
  ParamsEmp, //DmkACBrNFeSteps_0400,
  ModuleGeral, Module, UMySQLModule;

procedure TUnDmkACBr_ParamsEmp.AtualizarSSLLibsCombo(ACBrNFeX: TACBrNFe);
begin
  with FmParamsEmp do
  begin
    cbSSLLib.ItemIndex     := Integer(ACBrNFeX.Configuracoes.Geral.SSLLib);
    cbCryptLib.ItemIndex   := Integer(ACBrNFeX.Configuracoes.Geral.SSLCryptLib);
    cbHttpLib.ItemIndex    := Integer(ACBrNFeX.Configuracoes.Geral.SSLHttpLib);
    cbXmlSignLib.ItemIndex := Integer(ACBrNFeX.Configuracoes.Geral.SSLXmlSignLib);

    cbSSLType.Enabled := (ACBrNFeX.Configuracoes.Geral.SSLHttpLib in [httpWinHttp, httpOpenSSL]);
  end;
end;

{$IfNDef sACBr}
function TUnDmkACBr_ParamsEmp.ConfigurarComponente(ACBrNFeX: TACBrNFe): Boolean;
var
  Ok: Boolean;
  s: String;
  //Aguardar, Intervalo, Tentativas: String;
  //ACBrNFeX: TACBrNFe;
  _SSLLib: TSSLLib;
begin
  Result := False;
  //
  if DModG.QrParamsACBr.State = dsInactive then
    DModG.ReopenParamsACBr();

  //with FmParamsEmp do
  begin

    ACBrNFeX.Configuracoes.Certificados.URLPFX      := DModG.QrParamsACBrCertificado_URL.Value; //edtURLPFX.Text;
    //ACBrNFeX.Configuracoes.Certificados.ArquivoPFX  := FmParamsEmp.QrPrmsEmpNFeCertDigPfxCam.Value; //edtCaminho.Text;
    ACBrNFeX.Configuracoes.Certificados.ArquivoPFX  := DModG.QrPrmsEmpNFeCertDigPfxCam.Value;
    //ACBrNFeX.Configuracoes.Certificados.Senha       := FmParamsEmp.QrPrmsEmpNFeCertDigPfxPwd.Value; //edtSenha.Text;
    ACBrNFeX.Configuracoes.Certificados.Senha       := DModG.QrPrmsEmpNFeCertDigPfxPwd.Value; //edtSenha.Text;
    if ACBrNFeX.Configuracoes.Certificados.ArquivoPFX = EmptyStr then
      //ACBrNFeX.Configuracoes.Certificados.NumeroSerie := FmParamsEmp.QrPrmsEmpNFeNFeSerNum.Value; //edtNumSerie.Text;
      ACBrNFeX.Configuracoes.Certificados.NumeroSerie := DModG.QrPrmsEmpNFeNfeSerNum.Value; //edtNumSerie.Text;

  // ini dmkMLA
  {
    if cbModeloDF.ItemIndex = 0 then
      ACBrNFeX.DANFE := ACBrNFeDANFeRL1
    else
    begin
      case rgDANFCE.ItemIndex of
        0: ACBrNFeX.DANFE := ACBrNFeDANFCeFortes1;
        1: ACBrNFeX.DANFE := ACBrNFeDANFeESCPOS1;
        2: ACBrNFeX.DANFE := ACBrNFeDANFCeFortesA41;
      end;
    end;
  }
  // fim dmkMLA

    ACBrNFeX.SSL.DescarregarCertificado;

    //_SSLLib     := TSSLLib(DModG.QrParamsACBrCertificado_SSLLib.Value);
    with ACBrNFeX.Configuracoes.Geral do
    begin

      SSLLib        := TSSLLib(DModG.QrParamsACBrCertificado_SSLLib.Value); // TSSLLib(cbSSLLib.ItemIndex);
      SSLCryptLib   := TSSLCryptLib(DModG.QrParamsACBrCertificado_CryptLib.Value); // cbCryptLib.ItemIndex);
      SSLHttpLib    := TSSLHttpLib(DModG.QrParamsACBrCertificado_HttpLib.Value); // TSSLHttpLib(cbHttpLib.ItemIndex);
      SSLXmlSignLib := TSSLXmlSignLib(DModG.QrParamsACBrCertificado_XmlSignLib.Value); // TSSLXmlSignLib(cbXmlSignLib.ItemIndex);

      if MyObjects.ComponentePertenceAoForm(ACBrNFeX, TFmParamsEmp) then
        AtualizarSSLLibsCombo(ACBrNFeX);

      AtualizarXMLCancelado := Geral.IntToBool(DModG.QrParamsACBrGeral_AtualizarXML.Value); // cbxAtualizarXML.Checked;

      Salvar           := Geral.IntToBool(DModG.QrParamsACBrGeral_Salvar.Value); // ckSalvar.Checked;
      ExibirErroSchema := Geral.IntToBool(DModG.QrParamsACBrGeral_ExibirErroSchema.Value); // cbxExibirErroSchema.Checked;
      RetirarAcentos   := Geral.IntToBool(DModG.QrParamsACBrGeral_RetirarAcentos.Value); // cbxRetirarAcentos.Checked;
      FormatoAlerta    := DModG.QrParamsACBrGeral_FormatoAlerta.Value; // edtFormatoAlerta.Text;
      FormaEmissao     := TpcnTipoEmissao.teNormal; //TpcnTipoEmissao(cbFormaEmissao.ItemIndex);
      //ModeloDF         := DmkACBr_PF.DmkToACBr_ModeloDF(FmParamsEmp.QrPrmsEmpNFeide_mod.Value); //TpcnModeloDF(cbModeloDF.ItemIndex);
      ModeloDF         := DmkACBr_PF.DmkToACBr_ModeloDF(DModG.QrPrmsEmpNFeide_mod.Value); //TpcnModeloDF(cbModeloDF.ItemIndex);
      //VersaoDF         := DmkACBr_PF.DmkToACBr_VersaoDF(FmParamsEmp.QrPrmsEmpNFeversao.Value); //TpcnVersaoDF(cbVersaoDF.ItemIndex);
      VersaoDF         := DmkACBr_PF.DmkToACBr_VersaoDF(DModG.QrPrmsEmpNFeversao.Value); //TpcnVersaoDF(cbVersaoDF.ItemIndex);

      //IdCSC            := FmParamsEmp.QrParamsEmpCSCpos.Value; //edtIdToken.Text;
      IdCSC            := DModG.QrPrmsEmpNFeCSCpos.Value; //edtIdToken.Text;
      //CSC              := FmParamsEmp.QrParamsEmpCSC.Value; //edtToken.Text;
      CSC              := DModG.QrPrmsEmpNFeCSC.Value; //edtToken.Text;
      VersaoQRCode     := veqr200;
    end;

    with ACBrNFeX.Configuracoes.WebServices do
    begin
      //UF         := FmParamsEmp.QrPrmsEmpNFeUF_Servico.Value; //cbUF.Text;
      UF         := DModG.QrPrmsEmpNFeUF_Servico.Value; //cbUF.Text;
      //Ambiente   := DmkACBr_PF.DmkToACBr_Ambiente(FmParamsEmp.QrPrmsEmpNFeide_tpAmb.Value); //*) StrToTpAmb(Ok,IntToStr(rgTipoAmb.ItemIndex+1));
      Ambiente   := DmkACBr_PF.DmkToACBr_Ambiente(DModG.QrPrmsEmpNFeide_tpAmb.Value); //*) StrToTpAmb(Ok,IntToStr(rgTipoAmb.ItemIndex+1));
      Visualizar := Geral.IntToBool(DModG.QrParamsACBrWebService_Visualizar.Value); // cbxVisualizar.Checked;
      Salvar     := Geral.IntToBool(DModG.QrParamsACBrWebService_SalvarSOAP.Value); // cbxSalvarSOAP.Checked;


      AjustaAguardaConsultaRet := Geral.IntToBool(DModG.QrParamsACBrWebService_AjustarAut.Value); // cbxAjustarAut.Checked;
      //
{
      Aguardar   := DModG.QrParamsACBrWebService_Aguardar.Value;
      Intervalo  := DModG.QrParamsACBrWebService_Intervalo.Value;
      Tentativas := DModG.QrParamsACBrWebService_Tentativas.Value;
}
      //
      AguardarConsultaRet := DModG.QrParamsACBrWebService_Aguardar.Value;
(*
      if NaoEstaVazio(edtAguardar.Text)then
        AguardarConsultaRet := ifThen(StrToInt(edtAguardar.Text) < 1000, StrToInt(edtAguardar.Text) * 1000, StrToInt(edtAguardar.Text))
      else
        edtAguardar.Text := IntToStr(AguardarConsultaRet);
*)

      Tentativas := DModG.QrParamsACBrWebService_Tentativas.Value;
(*
      if NaoEstaVazio(edtTentativas.Text) then
        Tentativas := StrToInt(edtTentativas.Text)
      else
        edtTentativas.Text := IntToStr(Tentativas);
*)
      IntervaloTentativas := DModG.QrParamsACBrWebService_Intervalo.Value;
(*
      if NaoEstaVazio(edtIntervalo.Text) then
        IntervaloTentativas := ifThen(StrToInt(edtIntervalo.Text) < 1000, StrToInt(edtIntervalo.Text) * 1000, StrToInt(edtIntervalo.Text))
      else
        edtIntervalo.Text := IntToStr(ACBrNFeX.Configuracoes.WebServices.IntervaloTentativas);
*)

    //QrParamsACBrWebService_SSLType: TIntegerField;
      TimeOut   := DModG.QrParamsACBrWebService_TimeOut.Value; // seTimeOut.Value;
      ProxyHost := DModG.QrParamsACBrProxy_Host.Value; //edtProxyHost.Text;
      ProxyPort := DModG.QrParamsACBrProxy_Porta.Value; // edtProxyPorta.Text;
      ProxyUser := DModG.QrParamsACBrProxy_User.Value; // edtProxyUser.Text;
      ProxyPass := DModG.QrParamsACBrProxy_Pass.Value; // edtProxySenha.Text;
    end;

    ACBrNFeX.SSL.SSLType := TSSLType(DModG.QrParamsACBrWebService_SSLType.Value); // TSSLType(cbSSLType.ItemIndex);

    with ACBrNFeX.Configuracoes.Arquivos do
    begin
      Salvar           := Geral.IntToBool(DModG.QrParamsACBrArquivos_Salvar.Value); // cbxSalvarArqs.Checked;
      SepararPorMes    := Geral.IntToBool(DModG.QrParamsACBrArquivos_PastaMensal.Value); // cbxPastaMensal.Checked;
      AdicionarLiteral := Geral.IntToBool(DModG.QrParamsACBrArquivos_AddLiteral.Value); // cbxAdicionaLiteral.Checked;
      EmissaoPathNFe   := Geral.IntToBool(DModG.QrParamsACBrArquivos_EmissaoPathNFe.Value); // cbxEmissaoPathNFe.Checked;
      SalvarEvento     := Geral.IntToBool(DModG.QrParamsACBrArquivos_SalvarPathEvento.Value); // cbxSalvaPathEvento.Checked;
      SepararPorCNPJ   := Geral.IntToBool(DModG.QrParamsACBrArquivos_SepararPorCNPJ.Value); // cbxSepararPorCNPJ.Checked;
      SepararPorModelo := Geral.IntToBool(DModG.QrParamsACBrArquivos_SepararPorModelo.Value); // cbxSepararPorModelo.Checked;
      PathSchemas      := DModG.QrParamsACBrGeral_PathSchemas.Value; // edtPathSchemas.Text;
      //ShowMessage(PathSchemas);
      //////////////////////////////////////////////////////////////////////////
      //s          := FmParamsEmp.QrPrmsEmpNFeDirNFeGer.Value;
      s          := DModG.QrPrmsEmpNFeDirNFeGer.Value;
      if s <> EmptyStr then
      begin
        Geral.VerificaDir(s, '\', 'ACBr', True);
        if s <> EmptyStr then
          PathNFe          := s + 'ACBr'; // edtPathNFe.Text;
      end else
        PathNFe := EmptyStr;
      //////////////////////////////////////////////////////////////////////////
      //s          := FmParamsEmp.QrPrmsEmpNFeDirPedInu.Value;
      s          := DModG.QrPrmsEmpNFeDirPedInu.Value;
      if s <> EmptyStr then
      begin
        Geral.VerificaDir(s, '\', 'ACBr', True);
        if s <> EmptyStr then
          PathInu          := s + 'ACBr'; // edtPathInu.Text;
      end else
        PathInu := EmptyStr;
      if s <> EmptyStr then
      //////////////////////////////////////////////////////////////////////////
      //s       := FmParamsEmp.QrPrmsEmpNFeDirEveEnvLot.Value;
      s       := DModG.QrPrmsEmpNFeDirEveEnvLot.Value;
      if s <> EmptyStr then
      begin
        Geral.VerificaDir(s, '\', 'ACBr', True);
        if s <> EmptyStr then
          PathEvento       := s + 'ACBr'; // edtPathEvento.Text;
      end else
        PathEvento := EmptyStr;
      //////////////////////////////////////////////////////////////////////////
      PathSalvar       := DModG.QrParamsACBrGeral_PathSalvar.Value; // edtPathLogs.Text;
    end;

    if ACBrNFeX.DANFE <> nil then
    begin
      Geral.MB_Aviso('Aparei Aqui! 460')
{
      ACBrNFeX.DANFE.TipoDANFE := StrToTpImp(OK, IntToStr(rgTipoDanfe.ItemIndex + 1));
      ACBrNFeX.DANFE.Logo      := edtLogoMarca.Text;
      ACBrNFeX.DANFE.PathPDF   := edtPathPDF.Text;

      ACBrNFeX.DANFE.MargemDireita  := 7;
      ACBrNFeX.DANFE.MargemEsquerda := 7;
      ACBrNFeX.DANFE.MargemSuperior := 5;
      ACBrNFeX.DANFE.MargemInferior := 5;
}
    end;
    if (ACBrNFeX.Configuracoes.Certificados.ArquivoPFX <> EmptyStr)
    or (ACBrNFeX.Configuracoes.Certificados.NumeroSerie <> EmptyStr) then
      ACBrNFeX.SSL.CarregarCertificado;
  end;
  //
  Result := True;
end;
{$EndIf}

procedure TUnDmkACBr_ParamsEmp.DFe_ConsultaSstatusServico(const ACBrNFeX:
  TACBrNFe; var CodStatus: Integer; var TxtStatus: String);
const
  sProcName = 'TUnDmkACBr_ParamsEmp.DFe_ConsultaSstatusServico()';
begin
  ACBrNFeX.WebServices.StatusServico.Executar;
  CodStatus := ACBrNFeX.WebServices.StatusServico.cStat;
  TxtStatus := ACBrNFeX.WebServices.StatusServico.xMotivo;
  //
  if MyObjects.ComponentePertenceAoForm(ACBrNFeX, TFmParamsEmp) then
  begin
    with FmParamsEmp do
    begin
      MemoResp.Lines.Text := ACBrNFeX.WebServices.StatusServico.RetWS;
      memoRespWS.Lines.Text := ACBrNFeX.WebServices.StatusServico.RetornoWS;
      LoadXML(ACBrNFeX, ACBrNFeX.WebServices.StatusServico.RetornoWS, WBResposta);

      pgRespostas.ActivePageIndex := 1;

      MemoDados.Lines.Add('');
      MemoDados.Lines.Add('Status Servi�o');
      MemoDados.Lines.Add('tpAmb: '    +TpAmbToStr(ACBrNFeX.WebServices.StatusServico.tpAmb));
      MemoDados.Lines.Add('verAplic: ' +ACBrNFeX.WebServices.StatusServico.verAplic);
      MemoDados.Lines.Add('cStat: '    +IntToStr(ACBrNFeX.WebServices.StatusServico.cStat));
      MemoDados.Lines.Add('xMotivo: '  +ACBrNFeX.WebServices.StatusServico.xMotivo);
      MemoDados.Lines.Add('cUF: '      +IntToStr(ACBrNFeX.WebServices.StatusServico.cUF));
      MemoDados.Lines.Add('dhRecbto: ' +DateTimeToStr(ACBrNFeX.WebServices.StatusServico.dhRecbto));
      MemoDados.Lines.Add('tMed: '     +IntToStr(ACBrNFeX.WebServices.StatusServico.TMed));
      MemoDados.Lines.Add('dhRetorno: '+DateTimeToStr(ACBrNFeX.WebServices.StatusServico.dhRetorno));
      MemoDados.Lines.Add('xObs: '     +ACBrNFeX.WebServices.StatusServico.xObs);

      if (ACBrNFeX.Integrador= ACBrIntegrador1) then
      begin
        if (ACBrIntegrador1.ComandoIntegrador.IntegradorResposta.Codigo <> '') then
        begin
          MemoDados.Lines.Add('[Integrador]');
          MemoDados.Lines.Add('Codigo=' + ACBrIntegrador1.ComandoIntegrador.IntegradorResposta.Codigo);
          MemoDados.Lines.Add('Valor=' + ACBrIntegrador1.ComandoIntegrador.IntegradorResposta.Valor);

          ACBrIntegrador1.ComandoIntegrador.IntegradorResposta.Codigo := '';
          ACBrIntegrador1.ComandoIntegrador.IntegradorResposta.Valor := '';
        end;
      end;
    end;
  end else
{
  if MyObjects.ComponentePertenceAoForm(ACBrNFeX, TFmDmkACBrNFeSteps_0400) then
  begin
    // nada!
  end else
  if MyObjects.ComponentePertenceAoForm(ACBrNFeX, TDmNFe_0000) then
  begin
    // nada!
  end else
    Geral.MB_Info('Janela "' + TForm(ACBrNFeX.Owner).Name +
    '" n�o implementada em ' + sProcName);
}
end;

procedure TUnDmkACBr_ParamsEmp.FormCreate_ACBr(ACBrNFeX: TACBrNFe(*Sender: TObject; ACBrNFeX:
  TACBrNFe; cbSSLLib: TComboBox; cbCryptLib: TComboBox; cbHttpLib: TComboBox;
  cbXmlSignLib: TComboBox; cbSSLType: TComboBox; cbFormaEmissao: TComboBox;
  cbModeloDF: TComboBox; cbVersaoDF: TComboBox; cbxPorta: TComboBox;
  pgRespostas: TPageControl*));
var
  T: TSSLLib;
  //I: TpcnTipoEmissao;
  //J: TpcnModeloDF;
  //K: TpcnVersaoDF;
  U: TSSLCryptLib;
  V: TSSLHttpLib;
  X: TSSLXmlSignLib;
  Y: TSSLType;
// ini dmkMLA
  //N: TACBrPosPrinterModelo;
  //O: TACBrPosPaginaCodigo;
// fim dmkMLA
begin
  with FmParamsEmp do
  begin
    cbSSLLib.Items.Clear;
    for T := Low(TSSLLib) to High(TSSLLib) do
      cbSSLLib.Items.Add( GetEnumName(TypeInfo(TSSLLib), integer(T) ) );
    cbSSLLib.ItemIndex := 0;

    cbCryptLib.Items.Clear;
    for U := Low(TSSLCryptLib) to High(TSSLCryptLib) do
      cbCryptLib.Items.Add( GetEnumName(TypeInfo(TSSLCryptLib), integer(U) ) );
    cbCryptLib.ItemIndex := 0;

    cbHttpLib.Items.Clear;
    for V := Low(TSSLHttpLib) to High(TSSLHttpLib) do
      cbHttpLib.Items.Add( GetEnumName(TypeInfo(TSSLHttpLib), integer(V) ) );
    cbHttpLib.ItemIndex := 0;

    cbXmlSignLib.Items.Clear;
    for X := Low(TSSLXmlSignLib) to High(TSSLXmlSignLib) do
      cbXmlSignLib.Items.Add( GetEnumName(TypeInfo(TSSLXmlSignLib), integer(X) ) );
    cbXmlSignLib.ItemIndex := 0;

    cbSSLType.Items.Clear;
    for Y := Low(TSSLType) to High(TSSLType) do
      cbSSLType.Items.Add( GetEnumName(TypeInfo(TSSLType), integer(Y) ) );
    cbSSLType.ItemIndex := 0;

 (*  ini dmkMLA
    cbFormaEmissao.Items.Clear;
    for I := Low(TpcnTipoEmissao) to High(TpcnTipoEmissao) do
       cbFormaEmissao.Items.Add( GetEnumName(TypeInfo(TpcnTipoEmissao), integer(I) ) );
    cbFormaEmissao.ItemIndex := 0;

    cbModeloDF.Items.Clear;
    for J := Low(TpcnModeloDF) to High(TpcnModeloDF) do
       cbModeloDF.Items.Add( GetEnumName(TypeInfo(TpcnModeloDF), integer(J) ) );
    cbModeloDF.ItemIndex := 0;

    cbVersaoDF.Items.Clear;
    for K := Low(TpcnVersaoDF) to High(TpcnVersaoDF) do
       cbVersaoDF.Items.Add( GetEnumName(TypeInfo(TpcnVersaoDF), integer(K) ) );
    cbVersaoDF.ItemIndex := 0;

     cbxModeloPosPrinter.Items.Clear ;
    for N := Low(TACBrPosPrinterModelo) to High(TACBrPosPrinterModelo) do
      cbxModeloPosPrinter.Items.Add( GetEnumName(TypeInfo(TACBrPosPrinterModelo), integer(N) ) ) ;

    cbxPagCodigo.Items.Clear ;
    for O := Low(TACBrPosPaginaCodigo) to High(TACBrPosPaginaCodigo) do
       cbxPagCodigo.Items.Add( GetEnumName(TypeInfo(TACBrPosPaginaCodigo), integer(O) ) ) ;

    cbxPorta.Items.Clear;
    ACBrPosPrinter1.Device.AcharPortasSeriais( cbxPorta.Items );
    ACBrPosPrinter1.Device.AcharPortasRAW( cbxPorta.Items );
     {$IfDef MSWINDOWS}
    cbxPorta.Items.Add('LPT1') ;
    cbxPorta.Items.Add('\\localhost\Epson') ;
    cbxPorta.Items.Add('c:\temp\ecf.txt') ;
    {$EndIf}

    cbxPorta.Items.Add('TCP:192.168.0.31:9100') ;
    {$IfDef LINUX}
    cbxPorta.Items.Add('/dev/ttyS0') ;
    cbxPorta.Items.Add('/dev/ttyUSB0') ;
    cbxPorta.Items.Add('/tmp/ecf.txt') ;
    {$EndIf}
  fim dmkMLA*)

    LerConfiguracao(ACBrNFeX);
  {  ini dmkMLA
    pgRespostas.ActivePageIndex := 0;
    PageControl1.ActivePageIndex := 0;
    PageControl4.ActivePageIndex := 0;
   fim dmkMLA}
  end;
end;

procedure TUnDmkACBr_ParamsEmp.LerConfiguracao(ACBrNFeX: TACBrNFe);

var
  sCodigo: String;
{
  IniFile: String;
  Ini: TIniFile;
  StreamMemo: TMemoryStream;
}
begin
{
  IniFile := ChangeFileExt(ParamStr(0), '.ini');

  Ini := TIniFile.Create(IniFile);
  try
}
  sCodigo := Geral.FF0(DModG.QrParamsEmpCodigo.Value);
  DModG.ReopenParamsACBr();
  if DModG.QrParamsACBr.RecordCount = 0 then
  begin
    Dmod.MyDB.Execute('INSERT INTO paramsacbr SET Codigo=' + sCodigo);
    DModG.ReopenParamsACBr();
  end;
    with FmParamsEmp do
    begin
      cbSSLLib.ItemIndex     := DModG.QrParamsACBrCertificado_SSLLib.Value;     // Ini.ReadInteger('Certificado', 'SSLLib',     0);                                                                 // 4
      cbCryptLib.ItemIndex   := DModG.QrParamsACBrCertificado_CryptLib.Value;   // Ini.ReadInteger('Certificado', 'CryptLib',   0);                                                                 // 3
      cbHttpLib.ItemIndex    := DModG.QrParamsACBrCertificado_HttpLib.Value;    // Ini.ReadInteger('Certificado', 'HttpLib',    0);                                                                 // 2
      cbXmlSignLib.ItemIndex := DModG.QrParamsACBrCertificado_XmlSignLib.Value; // Ini.ReadInteger('Certificado', 'XmlSignLib', 0);                                                                 // 4
      edtURLPFX.Text         := DModG.QrParamsACBrCertificado_URL.Value;        // Ini.ReadString( 'Certificado', 'URL',        '');                                                                // ''
(*
      edtCaminho.Text        := DModG.QrParamsACBrCertificado_Caminho.Value;    // Ini.ReadString( 'Certificado', 'Caminho',    '');                                                                // 'C:\_Sincro\_MLArend\Clientes\Softcouro\certificado\2021\Certificado Digital 2021.pfx'
      edtSenha.Text          := DModG.QrParamsACBrCertificado_Senha.Value;      // Ini.ReadString( 'Certificado', 'Senha',      '');                                                                // argus 123
      edtNumSerie.Text       := DModG.QrParamsACBrCertificado_NumSerie.Value;   // Ini.ReadString( 'Certificado', 'NumSerie',   '');                                                                // ''
*)

      cbxAtualizarXML.Checked     := Geral.IntToBool(DModG.QrParamsACBrGeral_AtualizarXML.Value);     // := Ini.ReadBool(   'Geral', 'AtualizarXML',     True);                                                         // True
      cbxExibirErroSchema.Checked := Geral.IntToBool(DModG.QrParamsACBrGeral_ExibirErroSchema.Value); // := Ini.ReadBool(   'Geral', 'ExibirErroSchema', True);                                                         // True
      edtFormatoAlerta.Text       := DModG.QrParamsACBrGeral_FormatoAlerta.Value;    // := Ini.ReadString( 'Geral', 'FormatoAlerta',    'TAG:%TAGNIVEL% ID:%ID%/%TAG%(%DESCRICAO%) - %MSG%.');         //<-
(*
      cbFormaEmissao.ItemIndex    := DModG.QrParamsACBrGeral_FormaEmissao.Value;     // := Ini.ReadInteger('Geral', 'FormaEmissao',     0);                                                            // 0
      cbModeloDF.ItemIndex        := DModG.QrParamsACBrGeral_ModeloDF.Value;         // := Ini.ReadInteger('Geral', 'ModeloDF',         0);                                                            //----

      cbVersaoDF.ItemIndex      := DModG.QrParamsACBrGeral_VersaoDF.Value; // Ini.ReadInteger('Geral', 'VersaoDF',       0);                                                                // 3
      edtIdToken.Text           := DModG.QrParamsACBrGeral_IdToken.Value; // Ini.ReadString( 'Geral', 'IdToken',        '');                                                               // ''
      edtToken.Text             := DModG.QrParamsACBrGeral_Token.Value; // Ini.ReadString( 'Geral', 'Token',          '');                                                               // ''
*)
      ckSalvar.Checked          := Geral.IntToBool(DModG.QrParamsACBrGeral_Salvar.Value); // Ini.ReadBool(   'Geral', 'Salvar',         True);                                                             // True
      cbxRetirarAcentos.Checked := Geral.IntToBool(DModG.QrParamsACBrGeral_RetirarAcentos.Value); // Ini.ReadBool(   'Geral', 'RetirarAcentos', True);                                                             // True
      edtPathLogs.Text          := DModG.QrParamsACBrGeral_PathSalvar.Value; // Ini.ReadString( 'Geral', 'PathSalvar',     PathWithDelim(ExtractFilePath(Application.ExeName))+'Logs');       // 'C:\ACBr\Exemplos\ACBrDFe\ACBrNFe\Delphi\Logs'
      edtPathSchemas.Text       := DModG.QrParamsACBrGeral_PathSchemas.Value; // Ini.ReadString( 'Geral', 'PathSchemas',    PathWithDelim(ExtractFilePath(Application.ExeName))+'Schemas\'+GetEnumName(TypeInfo(TpcnVersaoDF), integer(cbVersaoDF.ItemIndex) )); // 'C:\ACBr\Exemplos\ACBrDFe\ACBrNFe\Delphi\Schemas\ve200'

(*
      cbUF.ItemIndex := cbUF.Items.IndexOf(Ini.ReadString('WebService', 'UF', 'SP'));                                                            // PR = 15

      rgTipoAmb.ItemIndex   := DModG.QrParamsACBrWebService_Ambiente.Value; // Ini.ReadInteger('WebService', 'Ambiente',   0);                                                                   // 1
*)
      cbxVisualizar.Checked := Geral.IntToBool(DModG.QrParamsACBrWebService_Visualizar.Value); // Ini.ReadBool(   'WebService', 'Visualizar', False);                                                               // False
      cbxSalvarSOAP.Checked := Geral.IntToBool(DModG.QrParamsACBrWebService_SalvarSOAP.Value); // Ini.ReadBool(   'WebService', 'SalvarSOAP', False);                                                               // False
      cbxAjustarAut.Checked := Geral.IntToBool(DModG.QrParamsACBrWebService_AjustarAut.Value); // Ini.ReadBool(   'WebService', 'AjustarAut', False);                                                               // False
      edtAguardar.ValueVariant      := DModG.QrParamsACBrWebService_Aguardar.Value; // Ini.ReadString( 'WebService', 'Aguardar',   '0');                                                                 // '0'
      edtTentativas.ValueVariant    := DModG.QrParamsACBrWebService_Tentativas.Value; // Ini.ReadString( 'WebService', 'Tentativas', '5');                                                                 // '5'
      edtIntervalo.ValueVariant     := DModG.QrParamsACBrWebService_Intervalo.Value; // Ini.ReadString( 'WebService', 'Intervalo',  '0');                                                                 // '0'
      seTimeOut.Value       := DModG.QrParamsACBrWebService_TimeOut.Value; // Ini.ReadInteger('WebService', 'TimeOut',    5000);                                                                // 5000
      cbSSLType.ItemIndex   := DModG.QrParamsACBrWebService_SSLType.Value; // Ini.ReadInteger('WebService', 'SSLType',    0);                                                                   // 5
                                                                                                                                                 // ''
      edtProxyHost.Text  := DModG.QrParamsACBrProxy_Host.Value; // Ini.ReadString('Proxy', 'Host',  '');                                                                                // ''
      edtProxyPorta.Text := DModG.QrParamsACBrProxy_Porta.Value; // Ini.ReadString('Proxy', 'Porta', '');                                                                                // ''
      edtProxyUser.Text  := DModG.QrParamsACBrProxy_User.Value; // Ini.ReadString('Proxy', 'User',  '');                                                                                // ''
      edtProxySenha.Text := DModG.QrParamsACBrProxy_Pass.Value; // Ini.ReadString('Proxy', 'Pass',  '');                                                                                // ''

      cbxSalvarArqs.Checked       := Geral.IntToBool(DModG.QrParamsACBrArquivos_Salvar.Value); // Ini.ReadBool(  'Arquivos', 'Salvar',           false);                                                      // False
      cbxPastaMensal.Checked      := Geral.IntToBool(DModG.QrParamsACBrArquivos_PastaMensal.Value); // Ini.ReadBool(  'Arquivos', 'PastaMensal',      false);                                                      // False
      cbxAdicionaLiteral.Checked  := Geral.IntToBool(DModG.QrParamsACBrArquivos_AddLiteral.Value); // Ini.ReadBool(  'Arquivos', 'AddLiteral',       false);                                                      // False
      cbxEmissaoPathNFe.Checked   := Geral.IntToBool(DModG.QrParamsACBrArquivos_EmissaoPathNFe.Value); // Ini.ReadBool(  'Arquivos', 'EmissaoPathNFe',   false);                                                      // False
      cbxSalvaPathEvento.Checked  := Geral.IntToBool(DModG.QrParamsACBrArquivos_SalvarPathEvento.Value); // Ini.ReadBool(  'Arquivos', 'SalvarPathEvento', false);                                                      // False
      cbxSepararPorCNPJ.Checked   := Geral.IntToBool(DModG.QrParamsACBrArquivos_SepararPorCNPJ.Value); // Ini.ReadBool(  'Arquivos', 'SepararPorCNPJ',   false);                                                      // False
      cbxSepararPorModelo.Checked := Geral.IntToBool(DModG.QrParamsACBrArquivos_SepararPorModelo.Value); // Ini.ReadBool(  'Arquivos', 'SepararPorModelo', false);                                                      // False
{
      edtPathNFe.Text             := DModG.QrParamsACBrArquivos_PathNFe.Value; // Ini.ReadString('Arquivos', 'PathNFe',          '');                                                         // ''
      edtPathInu.Text             := DModG.QrParamsACBrArquivos_PathInu.Value; // Ini.ReadString('Arquivos', 'PathInu',          '');                                                         // ''
      edtPathEvento.Text          := DModG.QrParamsACBrArquivos_PathEvento.Value; // Ini.ReadString('Arquivos', 'PathEvento',       '');                                                         // ''
      edtPathPDF.Text             := DModG.QrParamsACBrArquivos_PathPDF.Value; // Ini.ReadString('Arquivos', 'PathPDF',          '');                                                         // ''

      edtEmitCNPJ.Text       := Ini.ReadString('Emitente', 'CNPJ',        '');                                                                   //
      edtEmitIE.Text         := Ini.ReadString('Emitente', 'IE',          '');                                                                   //
      edtEmitRazao.Text      := Ini.ReadString('Emitente', 'RazaoSocial', '');                                                                   //
      edtEmitFantasia.Text   := Ini.ReadString('Emitente', 'Fantasia',    '');                                                                   //
      edtEmitFone.Text       := Ini.ReadString('Emitente', 'Fone',        '');                                                                   //
      edtEmitCEP.Text        := Ini.ReadString('Emitente', 'CEP',         '');                                                                   //
      edtEmitLogradouro.Text := Ini.ReadString('Emitente', 'Logradouro',  '');                                                                   //
      edtEmitNumero.Text     := Ini.ReadString('Emitente', 'Numero',      '');                                                                   //
      edtEmitComp.Text       := Ini.ReadString('Emitente', 'Complemento', '');                                                                   //
      edtEmitBairro.Text     := Ini.ReadString('Emitente', 'Bairro',      '');                                                                   //
      edtEmitCodCidade.Text  := Ini.ReadString('Emitente', 'CodCidade',   '');                                                                   //
      edtEmitCidade.Text     := Ini.ReadString('Emitente', 'Cidade',      '');                                                                   //
      edtEmitUF.Text         := Ini.ReadString('Emitente', 'UF',          '');                                                                   //

      cbTipoEmpresa.ItemIndex := Ini.ReadInteger('Emitente', 'CRT', 2);                                                                          //

      edtSmtpHost.Text     := Ini.ReadString('Email', 'Host',    '');                                                                            //
      edtSmtpPort.Text     := Ini.ReadString('Email', 'Port',    '');                                                                            //
      edtSmtpUser.Text     := Ini.ReadString('Email', 'User',    '');                                                                            //
      edtSmtpPass.Text     := Ini.ReadString('Email', 'Pass',    '');                                                                            //
      edtEmailAssunto.Text := Ini.ReadString('Email', 'Assunto', '');                                                                            //
      cbEmailSSL.Checked   := Ini.ReadBool(  'Email', 'SSL',     False);                                                                         // False

      StreamMemo := TMemoryStream.Create;
      Ini.ReadBinaryStream('Email', 'Mensagem', StreamMemo);
      mmEmailMsg.Lines.LoadFromStream(StreamMemo);                                                                                               // ''
      StreamMemo.Free;

      rgTipoDanfe.ItemIndex := Ini.ReadInteger('DANFE', 'Tipo',       0);                                                                        // 0
      edtLogoMarca.Text     := Ini.ReadString( 'DANFE', 'LogoMarca',  '');                                                                       // ''
      rgDANFCE.ItemIndex    := Ini.ReadInteger('DANFE', 'TipoDANFCE', 0);                                                                        // 0
}

  {  ini dmkMLA
      cbxModeloPosPrinter.ItemIndex := INI.ReadInteger('PosPrinter', 'Modelo',            Integer(ACBrPosPrinter1.Modelo));
      cbxPorta.Text                 := INI.ReadString( 'PosPrinter', 'Porta',             ACBrPosPrinter1.Porta);
      cbxPagCodigo.ItemIndex        := INI.ReadInteger('PosPrinter', 'PaginaDeCodigo',    Integer(ACBrPosPrinter1.PaginaDeCodigo));
      seColunas.Value               := INI.ReadInteger('PosPrinter', 'Colunas',           ACBrPosPrinter1.ColunasFonteNormal);
      seEspLinhas.Value             := INI.ReadInteger('PosPrinter', 'EspacoLinhas',      ACBrPosPrinter1.EspacoEntreLinhas);
      seLinhasPular.Value           := INI.ReadInteger('PosPrinter', 'LinhasEntreCupons', ACBrPosPrinter1.LinhasEntreCupons);
      cbCortarPapel.Checked         := Ini.ReadBool(   'PosPrinter', 'CortarPapel',       True);

      ACBrPosPrinter1.Device.ParamsString := INI.ReadString('PosPrinter', 'ParamsString', '');

  fim dmkMLA }
      edtAguardar.Text   := Geral.FF0(DModG.QrParamsACBrWebService_Aguardar.Value);
      edtTentativas.Text := Geral.FF0(DModG.QrParamsACBrWebService_Tentativas.Value);
      edtIntervalo.Text  := Geral.FF0(DModG.QrParamsACBrWebService_Intervalo.Value);
      //
      ConfigurarComponente(ACBrNFeX);
      //
    end;
(*
    ConfigurarEmail;
*)
{
  finally
    Ini.Free;
  end;
}

end;

procedure TUnDmkACBr_ParamsEmp.LoadXML(ACBrNFeX: TACBrNFe; RetWS: String;
  MyWebBrowser: TWebBrowser);
begin
  with FmParamsEmp do
  begin
    ACBrUtil.WriteToTXT(PathWithDelim(ExtractFileDir(application.ExeName)) + 'temp.xml',
                        ACBrUtil.ConverteXMLtoUTF8(RetWS), False, False);

    MyWebBrowser.Navigate(PathWithDelim(ExtractFileDir(application.ExeName)) + 'temp.xml');

    if ACBrNFeX.NotasFiscais.Count > 0then
      MemoResp.Lines.Add('Empresa: ' + ACBrNFeX.NotasFiscais.Items[0].NFe.Emit.xNome);
  end;
end;

procedure TUnDmkACBr_ParamsEmp.NumSerie(ACBrNFeX: TACBrNFe);
var
  I: Integer;
//  ASerie: String;
  AddRow: Boolean;
begin
  with FmParamsEmp do
  begin
    ACBrNFeX.SSL.LerCertificadosStore;
    AddRow := False;
    Application.CreateForm(TfrmSelecionarCertificado, frmSelecionarCertificado);
    with frmSelecionarCertificado.StringGrid1 do
    begin
      ColWidths[0] := 220;
      ColWidths[1] := 250;
      ColWidths[2] := 120;
      ColWidths[3] := 80;
      ColWidths[4] := 150;

      Cells[0, 0] := 'Num.S�rie';
      Cells[1, 0] := 'Raz�o Social';
      Cells[2, 0] := 'CNPJ';
      Cells[3, 0] := 'Validade';
      Cells[4, 0] := 'Certificadora';
    end;

    for I := 0 to ACBrNFeX.SSL.ListaCertificados.Count-1 do
    begin
      with ACBrNFeX.SSL.ListaCertificados[I] do
      begin
  //      ASerie := NumeroSerie;

        if (CNPJ <> '') then
        begin
          with frmSelecionarCertificado.StringGrid1 do
          begin
            if Addrow then
              RowCount := RowCount + 1;

            Cells[0, RowCount-1] := NumeroSerie;
            Cells[1, RowCount-1] := RazaoSocial;
            Cells[2, RowCount-1] := CNPJ;
            Cells[3, RowCount-1] := FormatDateBr(DataVenc);
            Cells[4, RowCount-1] := Certificadora;

            AddRow := True;
          end;
        end;
      end;
    end;

    frmSelecionarCertificado.ShowModal;

    if frmSelecionarCertificado.ModalResult = mrOK then
      edtNumSerie.Text := frmSelecionarCertificado.StringGrid1.Cells[0, frmSelecionarCertificado.StringGrid1.Row];
    frmSelecionarCertificado.Destroy
  end;
end;

procedure TUnDmkACBr_ParamsEmp.SalvarConfiguracao();
var
  Certificado_URL, Geral_FormatoAlerta, Geral_PathSalvar, Geral_PathSchemas,
  Proxy_Host, Proxy_Porta, Proxy_User, Proxy_Pass: String;
  Codigo, Certificado_SSLLib, Certificado_CryptLib, Certificado_HttpLib,
  Certificado_XmlSignLib, Geral_AtualizarXML, Geral_ExibirErroSchema,
  Geral_Salvar, Geral_RetirarAcentos, WebService_Visualizar,
  WebService_SalvarSOAP, WebService_AjustarAut, WebService_Aguardar,
  WebService_Tentativas, WebService_Intervalo, WebService_TimeOut,
  WebService_SSLType, Arquivos_Salvar, Arquivos_PastaMensal,
  Arquivos_AddLiteral, Arquivos_EmissaoPathNFe, Arquivos_SalvarPathEvento,
  Arquivos_SepararPorCNPJ, Arquivos_SepararPorModelo: Integer;
  SQLType: TSQLType;
begin
  SQLType := stUpd;
  Codigo  := FmParamsEmp.QrParamsEmpCodigo.Value;
  with FmParamsEmp do
  begin
    Certificado_SSLLib        := cbSSLLib.ItemIndex;
    Certificado_CryptLib      := cbCryptLib.ItemIndex;
    Certificado_HttpLib       := cbHttpLib.ItemIndex;
    Certificado_XmlSignLib    := cbXmlSignLib.ItemIndex;
    Certificado_URL           := edtURLPFX.Text;
    Geral_AtualizarXML        := Geral.BoolToInt(cbxAtualizarXML.Checked);
    Geral_ExibirErroSchema    := Geral.BoolToInt(cbxExibirErroSchema.Checked);
    Geral_FormatoAlerta       := edtFormatoAlerta.Text;
    Geral_Salvar              := Geral.BoolToInt(ckSalvar.Checked);
    Geral_RetirarAcentos      := Geral.BoolToInt(cbxRetirarAcentos.Checked);
    Geral_PathSalvar          := edtPathLogs.Text;
    Geral_PathSchemas         := edtPathSchemas.Text;
    WebService_Visualizar     := Geral.BoolToInt(cbxVisualizar.Checked);
    WebService_SalvarSOAP     := Geral.BoolToInt(cbxSalvarSOAP.Checked);
    WebService_AjustarAut     := Geral.BoolToInt(cbxAjustarAut.Checked);
    WebService_Aguardar       := edtAguardar.ValueVariant;
    WebService_Tentativas     := edtTentativas.ValueVariant;
    WebService_Intervalo      := edtIntervalo.ValueVariant;
    WebService_TimeOut        := seTimeOut.Value;
    WebService_SSLType        := cbSSLType.ItemIndex;
    Proxy_Host                := edtProxyHost.Text;
    Proxy_Porta               := edtProxyPorta.Text;
    Proxy_User                := edtProxyUser.Text;
    Proxy_Pass                := edtProxySenha.Text;
    Arquivos_Salvar           := Geral.BoolToInt(cbxSalvarArqs.Checked);
    Arquivos_PastaMensal      := Geral.BoolToInt(cbxPastaMensal.Checked);
    Arquivos_AddLiteral       := Geral.BoolToInt(cbxAdicionaLiteral.Checked);
    Arquivos_EmissaoPathNFe   := Geral.BoolToInt(cbxEmissaoPathNFe.Checked);
    Arquivos_SalvarPathEvento := Geral.BoolToInt(cbxSalvaPathEvento.Checked);
    Arquivos_SepararPorCNPJ   := Geral.BoolToInt(cbxSepararPorCNPJ.Checked);
    Arquivos_SepararPorModelo := Geral.BoolToInt(cbxSepararPorModelo.Checked);
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'paramsacbr', False, [
  'Certificado_SSLLib', 'Certificado_CryptLib', 'Certificado_HttpLib',
  'Certificado_XmlSignLib', 'Certificado_URL', 'Geral_AtualizarXML',
  'Geral_ExibirErroSchema', 'Geral_FormatoAlerta', 'Geral_Salvar',
  'Geral_RetirarAcentos', 'Geral_PathSalvar', 'Geral_PathSchemas',
  'WebService_Visualizar', 'WebService_SalvarSOAP', 'WebService_AjustarAut',
  'WebService_Aguardar', 'WebService_Tentativas', 'WebService_Intervalo',
  'WebService_TimeOut', 'WebService_SSLType', 'Proxy_Host',
  'Proxy_Porta', 'Proxy_User', 'Proxy_Pass',
  'Arquivos_Salvar', 'Arquivos_PastaMensal', 'Arquivos_AddLiteral',
  'Arquivos_EmissaoPathNFe', 'Arquivos_SalvarPathEvento', 'Arquivos_SepararPorCNPJ',
  'Arquivos_SepararPorModelo'], [
  'Codigo'], [
  Certificado_SSLLib, Certificado_CryptLib, Certificado_HttpLib,
  Certificado_XmlSignLib, Certificado_URL, Geral_AtualizarXML,
  Geral_ExibirErroSchema, Geral_FormatoAlerta, Geral_Salvar,
  Geral_RetirarAcentos, Geral_PathSalvar, Geral_PathSchemas,
  WebService_Visualizar, WebService_SalvarSOAP, WebService_AjustarAut,
  WebService_Aguardar, WebService_Tentativas, WebService_Intervalo,
  WebService_TimeOut, WebService_SSLType, Proxy_Host,
  Proxy_Porta, Proxy_User, Proxy_Pass,
  Arquivos_Salvar, Arquivos_PastaMensal, Arquivos_AddLiteral,
  Arquivos_EmissaoPathNFe, Arquivos_SalvarPathEvento, Arquivos_SepararPorCNPJ,
  Arquivos_SepararPorModelo], [
  Codigo], False) then
  begin
    DModG.ReopenParamsACBr();
  end;
end;

procedure TUnDmkACBr_ParamsEmp.TestarHTTPS_SemCertificado(ACBrNFeX: TACBrNFe);
var
  Acao: String;
  OldUseCert: Boolean;
  // CEP Original 18270-170
  sCEP: String;
begin
  sCEP := Geral.FF0(DModG.QrDOnoCEP.Value);
  if sCEP = EmptyStr then
    sCEP := '18270-170';
  with FmParamsEmp do
  begin
    Acao := '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' +
       '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' +
       'xmlns:cli="http://cliente.bean.master.sigep.bsb.correios.com.br/"> ' +
       ' <soapenv:Header/>' +
       ' <soapenv:Body>' +
       ' <cli:consultaCEP>' +
       ' <cep>87005090</cep>' +
       ' </cli:consultaCEP>' +
       ' </soapenv:Body>' +
       ' </soapenv:Envelope>';

    OldUseCert := ACBrNFeX.SSL.UseCertificateHTTP;
    ACBrNFeX.SSL.UseCertificateHTTP := False;

    try
      MemoResp.Lines.Text := ACBrNFeX.SSL.Enviar(Acao, 'https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl', '');
    finally
      ACBrNFeX.SSL.UseCertificateHTTP := OldUseCert;
    end;

    pgRespostas.ActivePageIndex := 0;
  end;
end;

procedure TUnDmkACBr_ParamsEmp.TestarSHA256_Mais_RSA(ACBrNFeX: TACBrNFe);
var
  Ahash: AnsiString;
begin
  with FmParamsEmp do
  begin
    Ahash := ACBrNFeX.SSL.CalcHash(Edit1.Text, dgstSHA256, outBase64, cbAssinar.Checked);
    MemoResp.Lines.Add( Ahash );
    pgRespostas.ActivePageIndex := 0;
  end;
end;

end.
