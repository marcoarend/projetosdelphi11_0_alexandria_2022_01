object FmDmkACBrNFeSteps_0400: TFmDmkACBrNFeSteps_0400
  Left = 339
  Top = 185
  Caption = 'NFe-STEPS-001 :: Passos da  NF-e 4.00'
  ClientHeight = 664
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 497
        Height = 32
        Caption = 'NFe-STEPS-001 :: Passos da  NF-e 4.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 497
        Height = 32
        Caption = 'NFe-STEPS-001 :: Passos da  NF-e 4.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 497
        Height = 32
        Caption = 'NFe-STEPS-001 :: Passos da  NF-e 4.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 550
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 594
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object PnConfirma: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object Panel10: TPanel
        Left = 293
        Top = -4
        Width = 208
        Height = 45
        BevelOuter = bvNone
        TabOrder = 1
        object CkSoLer: TCheckBox
          Left = 0
          Top = 20
          Width = 189
          Height = 17
          Caption = 'Somente ler arquivo j'#225' gravado.'
          Enabled = False
          TabOrder = 0
        end
      end
      object BitBtn1: TBitBtn
        Left = 148
        Top = 16
        Width = 75
        Height = 25
        Caption = 'BitBtn1'
        TabOrder = 2
        Visible = False
        OnClick = BitBtn1Click
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 502
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 269
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaExpiraCertDigital: TLabel
        Left = 0
        Top = 220
        Width = 1008
        Height = 25
        Align = alBottom
        Alignment = taCenter
        Caption = 'Expira'#231#227'o do Certificado Digital'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        ExplicitWidth = 331
      end
      object LaWait: TLabel
        Left = 0
        Top = 245
        Width = 1008
        Height = 24
        Align = alBottom
        Alignment = taCenter
        Caption = '...'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 18
      end
      object PnLoteEnv: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 220
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel15: TPanel
          Left = 0
          Top = 0
          Width = 1008
          Height = 102
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object RGAcao: TRadioGroup
            Left = 113
            Top = 0
            Width = 779
            Height = 102
            Align = alClient
            Caption = ' A'#231#227'o a realizar: '
            Columns = 3
            Enabled = False
            ItemIndex = 0
            Items.Strings = (
              'Status do servi'#231'o'
              'Envio de lote de NF-e ao fisco'
              'Consultar lote enviado'
              'Pedir cancelamento de NF-e'
              'Pedir inutiliza'#231#227'o de n'#250'mero(s) de NF-e'
              'Consultar NF-e'
              'Enviar lote de eventos da NFe'
              'Consulta Cadastro Entidade'
              'Consulta situa'#231#227'o da NFE (inoperante)'
              'Consulta NF-es Destinadas'
              'Download de NF-e(s)'
              'Consulta Distribui'#231#227'o de DFe de Interesse')
            TabOrder = 0
            OnClick = RGAcaoClick
          end
          object PnAbrirXML: TPanel
            Left = 892
            Top = 0
            Width = 116
            Height = 102
            Align = alRight
            BevelOuter = bvNone
            Enabled = False
            TabOrder = 1
            Visible = False
            object BtAbrir: TButton
              Left = 8
              Top = 4
              Width = 100
              Height = 25
              Caption = 'Abrir arquivo XML'
              Enabled = False
              TabOrder = 0
            end
            object Button1: TButton
              Left = 8
              Top = 32
              Width = 100
              Height = 25
              Caption = 'Aviso'
              Enabled = False
              TabOrder = 1
            end
          end
          object RGAmbiente: TRadioGroup
            Left = 0
            Top = 0
            Width = 113
            Height = 102
            Align = alLeft
            Caption = ' Ambiente: '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ItemIndex = 0
            Items.Strings = (
              'Nenhum'
              'Produ'#231#227'o'
              'Homologa'#231#227'o')
            ParentFont = False
            TabOrder = 2
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 102
          Width = 1008
          Height = 118
          Align = alClient
          TabOrder = 1
          object PCAcao: TPageControl
            Left = 1
            Top = 45
            Width = 1006
            Height = 72
            ActivePage = TabSheet11
            Align = alClient
            TabOrder = 0
            object TabSheet7: TTabSheet
              Caption = 'Status'
            end
            object TabSheet8: TTabSheet
              Caption = 'Envio NF'
              ImageIndex = 1
              object Panel8: TPanel
                Left = 0
                Top = 0
                Width = 998
                Height = 44
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object RGIndSinc: TdmkRadioGroup
                  Left = 4
                  Top = 5
                  Width = 188
                  Height = 39
                  Caption = ' Envio da NF-e ao Fisco (NFe 4.00):'
                  Columns = 2
                  ItemIndex = 0
                  Items.Strings = (
                    'Ass'#237'ncrono'
                    'S'#237'ncrono')
                  TabOrder = 0
                  QryCampo = 'IndSinc'
                  UpdCampo = 'IndSinc'
                  UpdType = utYes
                  OldValor = 0
                end
              end
            end
            object TabSheet9: TTabSheet
              Caption = 'Inutiliza'#231#227'o'
              ImageIndex = 2
              object Panel9: TPanel
                Left = 0
                Top = 0
                Width = 998
                Height = 45
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GroupBox1: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 276
                  Height = 45
                  Align = alLeft
                  Caption = ' Intervalo de numera'#231#227'o a ser inutilizado:  '
                  TabOrder = 0
                  object Label10: TLabel
                    Left = 8
                    Top = 20
                    Width = 61
                    Height = 13
                    Caption = 'N'#186' NF inicial:'
                  end
                  object Label11: TLabel
                    Left = 148
                    Top = 20
                    Width = 54
                    Height = 13
                    Caption = 'N'#186' NF final:'
                  end
                  object EdnNFIni: TdmkEdit
                    Left = 72
                    Top = 16
                    Width = 65
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ValMax = '999999999'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object EdnNFFim: TdmkEdit
                    Left = 204
                    Top = 16
                    Width = 65
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ValMax = '999999999'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                end
                object PnJustificativa: TPanel
                  Left = 320
                  Top = 0
                  Width = 678
                  Height = 45
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label7: TLabel
                    Left = 4
                    Top = 0
                    Width = 169
                    Height = 13
                    Caption = 'Justificativa (m'#237'nimo 15 caracteres):'
                  end
                  object SpeedButton2: TSpeedButton
                    Left = 649
                    Top = 16
                    Width = 21
                    Height = 21
                    Caption = '...'
                  end
                  object EdNFeJust: TdmkEditCB
                    Left = 4
                    Top = 16
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBNFeJust
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBNFeJust: TdmkDBLookupComboBox
                    Left = 60
                    Top = 16
                    Width = 589
                    Height = 21
                    KeyField = 'CodUsu'
                    ListField = 'Nome'
                    ListSource = DsNFeJust
                    TabOrder = 1
                    dmkEditCB = EdNFeJust
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                end
                object Panel12: TPanel
                  Left = 276
                  Top = 0
                  Width = 44
                  Height = 45
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 2
                  object Label15: TLabel
                    Left = 9
                    Top = 0
                    Width = 22
                    Height = 13
                    Caption = 'Ano:'
                  end
                  object EdAno: TdmkEdit
                    Left = 9
                    Top = 16
                    Width = 25
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 2
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ValMax = '899'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                end
              end
            end
            object TabSheet10: TTabSheet
              Caption = 'Consulta NFe'
              ImageIndex = 3
              object Panel14: TPanel
                Left = 0
                Top = 0
                Width = 998
                Height = 44
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label8: TLabel
                  Left = 4
                  Top = 2
                  Width = 127
                  Height = 13
                  Caption = 'Chave de acesso da NF-e:'
                end
                object Label16: TLabel
                  Left = 290
                  Top = 2
                  Width = 65
                  Height = 13
                  Caption = 'Controle NFe:'
                end
                object EdchNFe: TEdit
                  Left = 4
                  Top = 18
                  Width = 280
                  Height = 21
                  MaxLength = 255
                  ReadOnly = True
                  TabOrder = 0
                end
                object EdIDCtrl: TdmkEdit
                  Left = 290
                  Top = 17
                  Width = 73
                  Height = 21
                  Alignment = taRightJustify
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 9
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '000000000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
              end
            end
            object TabSheet11: TTabSheet
              Caption = 'Consulta cadastro'
              ImageIndex = 4
              object Panel16: TPanel
                Left = 0
                Top = 0
                Width = 998
                Height = 44
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label22: TLabel
                  Left = 4
                  Top = 4
                  Width = 17
                  Height = 13
                  Caption = 'UF:'
                end
                object Label21: TLabel
                  Left = 39
                  Top = 4
                  Width = 103
                  Height = 13
                  Caption = 'CNPJ do contribuinte:'
                end
                object EdContribuinte_UF: TdmkEdit
                  Left = 4
                  Top = 20
                  Width = 29
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdContribuinte_CNPJ: TdmkEdit
                  Left = 39
                  Top = 20
                  Width = 112
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '899'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
          end
          object Panel13: TPanel
            Left = 1
            Top = 1
            Width = 1006
            Height = 44
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Label20: TLabel
              Left = 6
              Top = 5
              Width = 36
              Height = 13
              Caption = 'Vers'#227'o:'
            end
            object Label5: TLabel
              Left = 48
              Top = 4
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label14: TLabel
              Left = 96
              Top = 4
              Width = 73
              Height = 13
              Caption = 'CNPJ empresa:'
            end
            object Label18: TLabel
              Left = 212
              Top = 4
              Width = 43
              Height = 13
              Caption = 'UF serv.:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label2: TLabel
              Left = 260
              Top = 5
              Width = 44
              Height = 13
              Caption = 'UF (WS):'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label17: TLabel
              Left = 308
              Top = 4
              Width = 127
              Height = 13
              Caption = 'Serial do Certificado digital:'
            end
            object Label12: TLabel
              Left = 572
              Top = 4
              Width = 38
              Height = 13
              Caption = 'Modelo:'
            end
            object Label13: TLabel
              Left = 616
              Top = 4
              Width = 27
              Height = 13
              Caption = 'S'#233'rie:'
            end
            object Label6: TLabel
              Left = 738
              Top = 4
              Width = 37
              Height = 13
              Caption = 'Recibo:'
            end
            object Label4: TLabel
              Left = 655
              Top = 4
              Width = 24
              Height = 13
              Caption = 'Lote:'
            end
            object EdVersaoAcao: TdmkEdit
              Left = 6
              Top = 20
              Width = 41
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '4,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 4.000000000000000000
              ValWarn = False
            end
            object EdEmpresa: TdmkEdit
              Left = 48
              Top = 20
              Width = 45
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdEmpresaChange
            end
            object EdEmitCNPJ: TdmkEdit
              Left = 96
              Top = 20
              Width = 112
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 1
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '899'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdUF_Servico: TdmkEdit
              Left = 212
              Top = 20
              Width = 45
              Height = 21
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnKeyDown = EdUF_ServicoKeyDown
            end
            object CBUF: TComboBox
              Left = 260
              Top = 20
              Width = 45
              Height = 21
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
              Items.Strings = (
                'AC'
                'AL'
                'AM'
                'AP'
                'BA'
                'CE'
                'DF'
                'ES'
                'GO'
                'MA'
                'MG'
                'MS'
                'MT'
                'PA'
                'PB'
                'PE'
                'PI'
                'PR'
                'RJ'
                'RN'
                'RO'
                'RR'
                'RS'
                'SC'
                'SE'
                'SP'
                'TO')
            end
            object EdSerialNumber: TdmkEdit
              Left = 308
              Top = 20
              Width = 260
              Height = 21
              Enabled = False
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object Edide_mod: TdmkEdit
              Left = 572
              Top = 20
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '55'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 55
              ValWarn = False
            end
            object EdSerie: TdmkEdit
              Left = 616
              Top = 20
              Width = 33
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 1
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '899'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdRecibo: TdmkEdit
              Left = 738
              Top = 20
              Width = 204
              Height = 21
              Enabled = False
              ReadOnly = True
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 9
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLote: TdmkEdit
              Left = 655
              Top = 20
              Width = 77
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              ReadOnly = True
              TabOrder = 9
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 9
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 269
      Width = 1008
      Height = 158
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' XML de envio '
        object RETxtEnvio: TMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 130
          Align = alClient
          TabOrder = 0
          WordWrap = False
          OnChange = RETxtEnvioChange
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'XML de envio (Formatado)'
        ImageIndex = 1
        object WBEnvio: TWebBrowser
          Left = 0
          Top = 0
          Width = 1000
          Height = 130
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 776
          ExplicitHeight = 69
          ControlData = {
            4C0000005A670000700D00000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' XML Retornado (Texto) '
        ImageIndex = 2
        object RETxtRetorno: TMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 130
          Align = alClient
          TabOrder = 0
          WantReturns = False
          OnChange = RETxtRetornoChange
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' XML Retornado (Formatado) '
        ImageIndex = 3
        object WBResposta: TWebBrowser
          Left = 0
          Top = 0
          Width = 1000
          Height = 130
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 776
          ExplicitHeight = 69
          ControlData = {
            4C0000005A670000700D00000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Informa'#231#245'es do XML de retorno '
        ImageIndex = 4
        object MeInfo: TMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 130
          Align = alClient
          ScrollBars = ssVertical
          TabOrder = 0
          WordWrap = False
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'Chaves'
        ImageIndex = 5
        object MeChaves: TMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 130
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 427
      Width = 1008
      Height = 75
      Align = alBottom
      Caption = 'Panel1'
      TabOrder = 2
      object Panel11: TPanel
        Left = 1
        Top = 1
        Width = 1006
        Height = 27
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Panel11'
        TabOrder = 0
        object Label19: TLabel
          Left = 8
          Top = 8
          Width = 68
          Height = 13
          Caption = 'Web Service: '
        end
        object EdWebService: TEdit
          Left = 76
          Top = 4
          Width = 693
          Height = 21
          ReadOnly = True
          TabOrder = 0
        end
      end
      object Panel6: TPanel
        Left = 1
        Top = 28
        Width = 1006
        Height = 46
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 268
          Height = 46
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 4
            Width = 88
            Height = 13
            Caption = 'Posi'#231#227'o do cursor:'
          end
          object dmkEdit1: TdmkEdit
            Left = 8
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdit2: TdmkEdit
            Left = 88
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdit3: TdmkEdit
            Left = 180
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object REWarning: TRichEdit
          Left = 268
          Top = 0
          Width = 738
          Height = 46
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = 4227327
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrStqaLocIts: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM stqalocits')
    Left = 260
    Top = 324
  end
  object DsStqaLocIts: TDataSource
    DataSet = QrStqaLocIts
    Left = 260
    Top = 380
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 16
    Top = 8
  end
  object QrNFeCabA1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM nfecaba'
      'WHERE ID=:P0'
      'AND LoteEnv=:P1'
      '')
    Left = 404
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeCabA1FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabA1FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabA1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabA1IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object QrNFeJust: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfejust'
      'WHERE :P0 & Aplicacao > 0'
      'ORDER BY Nome')
    Left = 48
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeJustCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeJustNome: TWideStringField
      FieldName = 'Nome'
      Size = 240
    end
    object QrNFeJustCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrNFeJustAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsNFeJust: TDataSource
    DataSet = QrNFeJust
    Left = 72
    Top = 8
  end
  object QrCabA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT IDCtrl, infProt_ID, infProt_nProt'
      'FROM nfecaba '
      'WHERE Empresa=:P0'
      'AND id=:P1')
    Left = 524
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCabAinfProt_ID: TWideStringField
      FieldName = 'infProt_ID'
      Size = 30
    end
    object QrCabAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
  end
  object QrNFeCabA2: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM nfecaba'
      'WHERE ID=:P0'
      'AND IDCtrl=:P1'
      '')
    Left = 404
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeCabA2FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabA2FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabA2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabA2Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
end
