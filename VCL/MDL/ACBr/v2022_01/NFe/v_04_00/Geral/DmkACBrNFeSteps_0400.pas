unit DmkACBrNFeSteps_0400;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, TypInfo, Variants, UnGrl_Vars,
  OmniXML, OmniXMLUtils,
  dmkDBGridZTO, UnDmkProcFunc, UnDmkACBr_ParamsEmp, UnitWin,
  ACBrNFe, dmkRadioGroup,
  UnXXe_PF, UMySQLModule, dmkDBLookupComboBox, dmkEditCB;

type
  TTipoTagXML = (
     ttx_Id                ,
     ttx_idLote            ,
     ttx_versao            ,
     ttx_tpAmb             ,
     ttx_verAplic          ,
     ttx_cOrgao            ,
     ttx_cStat             ,
     ttx_xMotivo           ,
     ttx_UF                ,
     ttx_cUF               ,
     ttx_dhRecbto          ,
     ttx_chNFe             ,
     ttx_nProt             ,
     ttx_digVal            ,
     ttx_ano               ,
     ttx_CNPJ              ,
     ttx_mod               ,
     ttx_serie             ,
     ttx_nNFIni            ,
     ttx_nNFFin            ,
     ttx_nRec              ,
     ttx_tMed              ,
     ttx_tpEvento          ,
     ttx_xEvento           ,
     ttx_CNPJDest          ,
     ttx_CPFDest           ,
     ttx_emailDest         ,
     ttx_nSeqEvento        ,
     ttx_dhRegEvento       ,
     ttx_dhResp            ,
     ttx_indCont           ,
     ttx_ultNSU            ,
     ttx_maxNSU            ,
     ttx_NSU               ,
     ttx_CPF               ,
     ttx_xNome             ,
     ttx_IE                ,
     ttx_dEmi              ,
     ttx_tpNF              ,
     ttx_vNF               ,
     ttx_cSitNFe           ,
     ttx_cSitConf          ,
     ttx_dhEvento          ,
     ttx_descEvento        ,
     ttx_xCorrecao         ,
     ttx_cJust             ,
     ttx_xJust             ,
     ttx_verEvento         ,
     ttx_schema            ,
     ttx_docZip            ,
     ttx_dhEmi             ,
     ttx_dhCons            ,
     ttx_cSit              ,
     ttx_indCredNFe        ,
     ttx_indCredCTe        ,
     ttx_xRegApur          ,
     ttx_CNAE              ,
     ttx_dIniAtiv          ,
     ttx_dUltSit           ,
     ttx_IEUnica           ,
     ttx_IEAtual           ,
     ttx_xLgr              ,
     ttx_nro               ,
     ttx_xCpl              ,
     ttx_xBairro           ,
     ttx_cMun              ,
     ttx_xMun              ,
     ttx_CEP               ,
     //
     ttx_
     );

  TFmDmkACBrNFeSteps_0400 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PnConfirma: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrStqaLocIts: TMySQLQuery;
    DsStqaLocIts: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    PnLoteEnv: TPanel;
    Panel15: TPanel;
    RGAcao: TRadioGroup;
    PnAbrirXML: TPanel;
    BtAbrir: TButton;
    Button1: TButton;
    RGAmbiente: TRadioGroup;
    Panel5: TPanel;
    LaExpiraCertDigital: TLabel;
    LaWait: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    RETxtEnvio: TMemo;
    TabSheet5: TTabSheet;
    WBEnvio: TWebBrowser;
    TabSheet2: TTabSheet;
    RETxtRetorno: TMemo;
    TabSheet3: TTabSheet;
    WBResposta: TWebBrowser;
    TabSheet4: TTabSheet;
    MeInfo: TMemo;
    TabSheet6: TTabSheet;
    MeChaves: TMemo;
    Panel1: TPanel;
    Panel11: TPanel;
    Label19: TLabel;
    EdWebService: TEdit;
    Panel6: TPanel;
    Panel7: TPanel;
    Label3: TLabel;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    REWarning: TRichEdit;
    PCAcao: TPageControl;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    Panel8: TPanel;
    RGIndSinc: TdmkRadioGroup;
    Panel10: TPanel;
    CkSoLer: TCheckBox;
    Timer1: TTimer;
    QrNFeCabA1: TMySQLQuery;
    QrNFeCabA1FatID: TIntegerField;
    QrNFeCabA1FatNum: TIntegerField;
    QrNFeCabA1Empresa: TIntegerField;
    QrNFeCabA1IDCtrl: TIntegerField;
    TabSheet9: TTabSheet;
    Panel9: TPanel;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    EdnNFIni: TdmkEdit;
    EdnNFFim: TdmkEdit;
    PnJustificativa: TPanel;
    Label7: TLabel;
    SpeedButton2: TSpeedButton;
    EdNFeJust: TdmkEditCB;
    CBNFeJust: TdmkDBLookupComboBox;
    Panel12: TPanel;
    EdAno: TdmkEdit;
    Label15: TLabel;
    Panel13: TPanel;
    Label20: TLabel;
    EdVersaoAcao: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label5: TLabel;
    Label14: TLabel;
    EdEmitCNPJ: TdmkEdit;
    Label18: TLabel;
    EdUF_Servico: TdmkEdit;
    CBUF: TComboBox;
    Label2: TLabel;
    Label17: TLabel;
    EdSerialNumber: TdmkEdit;
    Label12: TLabel;
    Edide_mod: TdmkEdit;
    EdSerie: TdmkEdit;
    Label13: TLabel;
    Label6: TLabel;
    EdRecibo: TdmkEdit;
    QrNFeJust: TMySQLQuery;
    QrNFeJustCodigo: TIntegerField;
    QrNFeJustNome: TWideStringField;
    QrNFeJustCodUsu: TIntegerField;
    QrNFeJustAplicacao: TIntegerField;
    EdLote: TdmkEdit;
    Label4: TLabel;
    DsNFeJust: TDataSource;
    TabSheet10: TTabSheet;
    Panel14: TPanel;
    Label8: TLabel;
    EdchNFe: TEdit;
    EdIDCtrl: TdmkEdit;
    Label16: TLabel;
    QrCabA: TMySQLQuery;
    QrCabAIDCtrl: TIntegerField;
    QrCabAinfProt_ID: TWideStringField;
    QrCabAinfProt_nProt: TWideStringField;
    QrNFeCabA2: TMySQLQuery;
    QrNFeCabA2FatID: TIntegerField;
    QrNFeCabA2FatNum: TIntegerField;
    QrNFeCabA2Empresa: TIntegerField;
    QrNFeCabA2Id: TWideStringField;
    TabSheet11: TTabSheet;
    Panel16: TPanel;
    EdContribuinte_UF: TdmkEdit;
    Label22: TLabel;
    Label21: TLabel;
    EdContribuinte_CNPJ: TdmkEdit;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdUF_ServicoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RETxtEnvioChange(Sender: TObject);
    procedure RETxtRetornoChange(Sender: TObject);
    procedure RGAcaoClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    //FMsg: String;
    //FSiglas_WS: MyArrayLista;
    FPathLoteNFe, FPathLoteEvento(*, FPathLoteDowNFeDes*): String;
    xmlDoc: IXMLDocument;
    xmlNode, xmlSub, xmlChild1: IXMLNode;
    xmlList: IXMLNodeList;
    FAmbiente_Int, FCodigoUF_Int: Integer;
    FAmbiente_Txt, FCodigoUF_Txt: String;
    FWSDL, FURL, FAvisoNSU: String;
    //FultNSU, FNSU: Int64;
    //
    function  AbreArquivoXML(Arq, Ext: String; Assinado: Boolean): Boolean;
    function  DefinechNFe(var chNFe: String): Boolean;
    function  DefineContribuinteCNPJ(var ContribuinteCNPJ: String; var Tipo:
              Integer): Boolean;
    function  DefineContribuinteUF(var UF: String): Boolean;
    function  DefineEmitCNPJ(var EmitCNPJ: String): Boolean;
    function  DefineEmpresa(var Empresa: Integer): Boolean;
    function  DefineIDCtrl(var IDCtrl: Integer): Boolean;
    function  DefineLote(var Lote: Integer): Boolean;
    function  DefineModelo(var Modelo: String): Boolean;
    function  DefinenNFIni(var nNFIni: String): Boolean;
    function  DefinenNFFin(var nNFFim: String): Boolean;
    function  DefineSerie(var Serie: String): Boolean;
    function  DefineXMLDoc(): Boolean;
    procedure HabilitaBotoes(Visivel: Boolean = True);
    procedure MostraTextoRetorno(Texto: String);
    function  TextoArqDefinido(Texto: String): Boolean;
    procedure VerificaCertificadoDigital(Empresa: Integer);
    procedure ReopenNFeJust(Aplicacao: Byte);
    //
    function  LeNoXML(No: IXMLNode; Tipo: TTipoNoXML; Tag: TTipoTagXML; AvisaVersao: Boolean = True): String;
    function  ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
    function  ObtemNomeDaTag(Tag: TTipoTagXML): String;

    //
    procedure ExecutaConsultaLoteNFe();
    procedure ExecutaConsultaNFe();
    procedure ExecutaEnvioDeLoteEvento();
    procedure ExecutaEnvioDeLoteNFe(Sincronia: TXXeIndSinc);
    procedure ExecutaInutilizaNumerosNF();

    procedure LerTextoConsultaCadastro();
    procedure LerTextoConsultaLoteNFe();
    procedure LerTextoConsultaNFe();
    function  LerTextoEnvioLoteEvento(): Boolean;
    function  LerTextoEnvioLoteNFe(): Boolean;
    procedure LerTextoInutilizaNumerosNF();

    procedure ConsultaCadastroContribuinte();
    procedure VerificaStatusServico();

    procedure LerXML_procEventoNFe(Lista: IXMLNodeList);

  public
    { Public declarations }
    //ACBrNFe1: TACBrNFe;
    //Node: TxmlNode;
    FFormChamou: String;
    FXML_LoteNFe, FXML_LoteEvento(*, FXML_LoteDowNFeDes*): String;
    FSegundos, FSecWait: Integer;
    FTextoArq: String;
    FNaoExecutaLeitura: Boolean;
    FCodStausServico: Integer;
    FTxtStausServico: String;
    procedure PreparaConsultaCadastro(Empresa: Integer);
    procedure PreparaConsultaLote(Lote, Empresa: Integer; Recibo: String);
    procedure PreparaConsultaNFe(Empresa, IDCtrl: Integer; ChaveNFe: String);
    function  PreparaEnvioDeLoteEvento(var UF_Servico: String; Lote, Empresa:
              Integer): Boolean;
    function  PreparaEnvioDeLoteNFe(Lote, Empresa: Integer; Sincronia:
              TXXeIndSinc; Modelo: Integer): Boolean;
    procedure PreparaInutilizaNumerosNF(Empresa, Lote, Ano, Modelo, Serie, nNFIni, nNFFim, Justif: Integer);
    procedure PreparaVerificacaoStatusServico(Empresa, Modelo: Integer);


  end;

  var
  FmDmkACBrNFeSteps_0400: TFmDmkACBrNFeSteps_0400;

implementation

uses UnMyObjects, Module, DmkDAC_PF, NFe_PF, ModuleNFe_0000, NFeGeraXML_0400,
  ModuleGeral, NFeLEnU_0400,
    {$IfNDef semNFCe_0000} NFCeLEnU_0400, {$EndIf}
  NFeLEnC_0400,
  //pcnConversaoNFe
  pcnConversao,
  NFeXMLGerencia;

{$R *.DFM}

const
  FXML_Load_Failure = 'Falha ao carregar o XML!';
  CO_Texto_Opt_Sel = 'Clique em "OK" para ler arquivo selecionado, ou em "Abrir" para selecionar um arquivo!';
  CO_Texto_Clk_Sel = 'Configure a forma de consulta e clique em "OK"!';
  CO_Texto_Env_Sel = 'Configure a forma de envio e clique em "OK" para enviar o lote ao fisco!';
  //FVersaoNFe = 400;

  CO_PCAcao_StatusIndex       = 0;
  CO_PCAcao_EnvioNFIndex      = 1;
  CO_PCAcao_InutilizacaoIndex = 2;

var
  FverXML_versao: String;


function TFmDmkACBrNFeSteps_0400.AbreArquivoXML(Arq, Ext: String;
  Assinado: Boolean): Boolean;
var
  Dir, Arquivo: String;
begin
  Result := False;
  FTextoArq := '';
  if not DmNFe_0000.ObtemDirXML(Ext, Dir, Assinado) then
    Exit;
  Arquivo := Dir + Arq + Ext;
  if FileExists(Arquivo) then
  begin
    if dmkPF.CarregaArquivo(Arquivo, FTextoArq) then
    begin
      MostraTextoRetorno(FTextoArq);
      Result := FTextoArq <> '';
      if Result then HabilitaBotoes();
    end;
  end else Geral.MB_Erro('Arquivo n�o localizado "' + Arquivo + '"!');
end;

procedure TFmDmkACBrNFeSteps_0400.BitBtn1Click(Sender: TObject);
const
(*
  Texto =
  '<retConsCad versao="2.00" xmlns="http://www.portalfiscal.inf.br/nfe"><infCons><verAplic>RSa20210809110246</verAplic><cStat>111</cStat><xMotivo>Consulta cadastro com uma ocorrencia</xMotivo><UF>RS</UF><CNPJ>96734892000123</CNPJ><dhCons>2022-02-20T22:' +
  '43:40</dhCons><cUF>43</cUF><infCad><IE>1240007512</IE><CNPJ>96734892000123</CNPJ><UF>RS</UF><cSit>1</cSit><indCredNFe>2</indCredNFe><indCredCTe>4</indCredCTe><xNome>TFL DO BRASIL IND QUIMICA LTDA</xNome><xRegApur>GERAL</xRegApur><CNAE>2099199</CNAE><' +
  'dIniAtiv>1944-09-01</dIniAtiv><dUltSit>1995-06-01</dUltSit><ender><xLgr>RUA SANTO AGOSTINHO</xLgr><nro>1099</nro><xBairro>SAO MIGUEL</xBairro><cMun>4318705</cMun><xMun>São Leopoldo</xMun><CEP>93025700</CEP></ender></infCad></infCons></retConsCad>';
*)
  Texto =
'<retConsCad versao="2.00" xmlns="http://www.portalfiscal.inf.br/nfe"><infCons><verAplic>GO4.0</verAplic><cStat>111</cStat><xMotivo>Consulta cadastro com uma ocorrência</xMotivo><UF>GO</UF><CNPJ>26685958000157</CNPJ><dhCons>2022-02-23T14:54:41</dhCons' +
'><cUF>52</cUF><infCad><IE>102174075</IE><CNPJ>26685958000157</CNPJ><UF>GO</UF><cSit>1</cSit><indCredNFe>4</indCredNFe><indCredCTe>4</indCredCTe><xNome>CURTUME CENTRO OESTE LTDA</xNome><xRegApur>NORMAL</xRegApur><CNAE>1510600</CNAE><dIniAtiv>1990-05-14' +
'</dIniAtiv><ender><xLgr>AREA RURAL</xLgr><nro>S/N</nro><xCpl>MARGENS RODOVIA GO 020</xCpl><xBairro>AREA RURAL DE SENADOR CANEDO</xBairro><cMun>5220454</cMun><xMun>SENADOR CANEDO</xMun><CEP>75264899</CEP></ender></infCad></infCons></retConsCad>';
begin
  FTextoArq := Texto;
  LerTextoConsultaCadastro();
end;

procedure TFmDmkACBrNFeSteps_0400.BtOKClick(Sender: TObject);
const
  CO_Indisponivel = 'N�o dispon�vel na vers�o: 4.00 da NF-e!';
begin
  REWarning.Text  := '';
  RETxtEnvio.Text := '';
  MeInfo.Text     := '';
  //
  MostraTextoRetorno('');
  PageControl1.ActivePageIndex := 0;
  Update;
  Application.ProcessMessages;
  //
  FCodigoUF_Int  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(CBUF.Text);
  FAmbiente_Int  := RGAmbiente.ItemIndex;
  if FAmbiente_Int = 0 then
  begin
    Geral.MB_Aviso('Defina o ambiente no cadastro da filial!');
    Exit;
  end;
  FAmbiente_Txt  := IntToStr(FAmbiente_Int);
  FCodigoUF_Txt  := IntToStr(FCodigoUF_Int);
  FWSDL          := '';
  FURL           := '';
  if FCodigoUF_Int = 0 then
  begin
    Geral.MB_Aviso('Defina a UF no cadastro da filial!');
    Exit;
  end;
  //
  BtOK.Enabled := False;
  case RGAcao.ItemIndex of
    0: VerificaStatusServico();
    1:
    begin
      if CkSoLer.Checked then
        LerTextoEnvioLoteNFe()
      else
        ExecutaEnvioDeLoteNFe(TXXeIndSinc(RGIndSinc.ItemIndex));
    end;
    2:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaLoteNFe()
      else
        ExecutaConsultaLoteNFe();
    end;
    3:
    begin
      Geral.MB_Aviso(CO_Indisponivel);
    end;
    4:
    begin
      if CkSoLer.Checked then
        LerTextoInutilizaNumerosNF()
      else
        ExecutaInutilizaNumerosNF();
    end;
    5:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaNFe()
      else
        ExecutaConsultaNFe();
    end;
    6:
    begin
      if CkSoLer.Checked then
        LerTextoEnvioLoteEvento()
      else
        ExecutaEnvioDeLoteEvento();
    end;
    7:
    begin
      ConsultaCadastroContribuinte();
    end;
{
    8:
    begin
      Geral.MB_Aviso(CO_Indisponivel);
    end;
    9:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaNFeDest(EdultNSU.Text)
      else
        ExecutaConsultaNFDest();
    end;
    10:
    begin
      Geral.MB_Aviso(CO_Indisponivel);
    end;
    11:
    begin
      Geral.MB_Aviso(CO_Indisponivel);
    end;
}
    else
      Geral.MB_Aviso('A a��o "' + RGAcao.Items[RGAcao.ItemIndex] +
        '" n�o est� implementada! AVISE A DERMATEK!');
  end;
  if Trim(REWarning.Text) <> '' then
    dmkPF.LeTexto_Permanente(REWarning.Text, 'Aviso do Web Service');
end;

procedure TFmDmkACBrNFeSteps_0400.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDmkACBrNFeSteps_0400.ConsultaCadastroContribuinte;
var
  Doc, UF: String;
  Tipo: Integer;
begin
  if not DefineContribuinteCNPJ(Doc, Tipo) then Exit;
  if not DefineContribuinteUF(UF) then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
(*
    FTextoArq := FmNFeGeraXML_0400.WS_NFeConsultaCadastro(
      EdUF_Servico.Text, EdContribuinte_UF.Text,
      EdContribuinte_CNPJ.Text, EdSerialNumber.Text, LaAviso1, LaAviso2, RETxtEnvio,
      EdWebService, EdIde_mod.ValueVariant);
*)
    DmNFe_0000.ConfigurarComponenteNFe();
    DmNFe_0000.ACBrNFe1.WebServices.ConsultaCadastro.UF := UF;
    case Tipo of
      0: DmNFe_0000.ACBrNFe1.WebServices.ConsultaCadastro.CNPJ := Doc;
      1: DmNFe_0000.ACBrNFe1.WebServices.ConsultaCadastro.CPF  := Doc;
    end;
    DmNFe_0000.ACBrNFe1.WebServices.ConsultaCadastro.Executar;
    FTextoArq := DmNFe_0000.ACBrNFe1.WebServices.ConsultaCadastro.RetWS;

    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com sucesso!');
    MostraTextoRetorno(FTextoArq);
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end else
      LerTextoConsultaCadastro();
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmDmkACBrNFeSteps_0400.DefinechNFe(var chNFe: String): Boolean;
var
  K: Integer;
begin
  Result := False;
  chNFe := EdchNFe.Text;
  K := Length(chNFe);
  if K <> 44 then
    Geral.MB_Erro('Tamanho da chave difere de 44: tamanho = ' + IntToStr(K))
  else if Geral.SoNumero1a9_TT(chNFe) = '' then
    Geral.MB_Erro('Chave n�o definida!')
  else
    Result := True;
end;

function TFmDmkACBrNFeSteps_0400.DefineContribuinteCNPJ(
  var ContribuinteCNPJ: String; var Tipo: Integer): Boolean;
var
  K: Integer;
begin
  Tipo := -1;
  ContribuinteCNPJ := Geral.SoNumero_TT(EdContribuinte_CNPJ.Text);
  k := Length(ContribuinteCNPJ);
  if K = 14 then
  begin
    Tipo := 0;
    Result := True;
  end
  else if K = 11 then
  begin
    Tipo := 1;
    Result := True;
  end
  else begin
    Result := False;
    Geral.MB_Erro('CNPJ/CPF da emitente com tamanho incorreto!');
  end;
end;

function TFmDmkACBrNFeSteps_0400.DefineContribuinteUF(var UF: String): Boolean;
begin
  UF := EdContribuinte_UF.Text;
  Result := Geral.SiglaUFValida(UF);
end;

function TFmDmkACBrNFeSteps_0400.DefineEmitCNPJ(var EmitCNPJ: String): Boolean;
var
  K: Integer;
begin
  EmitCNPJ := Geral.SoNumero_TT(EdEmitCNPJ.Text);
  k := Length(EmitCNPJ);
  if K = 14 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('CNPJ da empresa emitente com tamanho incorreto!');
  end;
end;

function TFmDmkACBrNFeSteps_0400.DefineEmpresa(var Empresa: Integer): Boolean;
begin
  Empresa := EdEmpresa.ValueVariant;
  if Empresa <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('Empresa n�o definida!');
  end;
end;

function TFmDmkACBrNFeSteps_0400.DefineIDCtrl(var IDCtrl: Integer): Boolean;
begin
  IDCtrl := EdIDCtrl.ValueVariant;
  if IDCtrl <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro(
    'IDCtrl n�o definido! A��o/consulta n�o ser� inclu�da no hist�rico da NF!');
  end;
end;

function TFmDmkACBrNFeSteps_0400.DefineLote(var Lote: Integer): Boolean;
begin
  Lote := EdLote.ValueVariant;
  if Lote <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('Lote n�o definido!');
  end;
end;

function TFmDmkACBrNFeSteps_0400.DefineModelo(var Modelo: String): Boolean;
begin
  Modelo := FormatFloat('00', EdIde_mod.ValueVariant);
  if (Modelo <> '55') and  (Modelo <> '65') then
  begin
    Result := False;
    Geral.MB_Erro('Modelo de NF-e n�o implementado: ' + Modelo);
  end else Result := True;
end;

function TFmDmkACBrNFeSteps_0400.DefinenNFFin(var nNFFim: String): Boolean;
begin
  if (EdnNFFim.ValueVariant = null) or (EdnNFFim.ValueVariant < 0) then
  begin
    Result := False;
    Geral.MB_Erro('Numera��o final de NF-e inv�lida!');
  end else begin
    nNFFim := FormatFloat('0', EdnNFFim.ValueVariant);
    Result := True;
  end;
end;

function TFmDmkACBrNFeSteps_0400.DefinenNFIni(var nNFIni: String): Boolean;
begin
  if (EdnNFIni.ValueVariant = null) or (EdnNFIni.ValueVariant < 0) then
  begin
    Result := False;
    Geral.MB_Erro('Numera��o inicial de NF-e inv�lida!');
  end else begin
    nNFIni := FormatFloat('0', EdnNFIni.ValueVariant);
    Result := True;
  end;
end;

function TFmDmkACBrNFeSteps_0400.DefineSerie(var Serie: String): Boolean;
begin
  if (EdSerie.ValueVariant = null) or
  (EdSerie.ValueVariant < 0) or
  (EdSerie.ValueVariant > 899) then
  begin
    Result := False;
    Geral.MB_Aviso('N�mero de s�rie inv�lido!');
  end else
  begin
    Serie  := FormatFloat('0', EdSerie.ValueVariant);
    Result := True;
  end;
end;

function TFmDmkACBrNFeSteps_0400.DefineXMLDoc: Boolean;
begin
  xmlDoc := TXMLDocument.Create;
  if not XMLLoadFromAnsiString(xmlDoc, FTextoArq) then
  begin
    Result := False;
    dmkPF.LeTexto_Permanente(FTextoArq, FXML_Load_Failure);
  end else Result := True;
end;

procedure TFmDmkACBrNFeSteps_0400.EdEmpresaChange(Sender: TObject);
begin
  DmNFe_0000.ReopenOpcoesNFe(EdEmpresa.ValueVariant, True);
  RGAmbiente.ItemIndex := DmNFe_0000.QrOpcoesNFeide_tpAmb.Value;
end;

procedure TFmDmkACBrNFeSteps_0400.EdUF_ServicoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
{ Precisa?
var
  TitCols: array[0..1] of String;
}
begin
{ Precisa?
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    //
    EdUF_Servico.Text := Geral.SelecionaItem(FSiglas_WS, 1,
      'SEL-LISTA-000 :: Sigla da Web Service', TitCols, Screen.Width)
  end;
}
end;

procedure TFmDmkACBrNFeSteps_0400.ExecutaConsultaLoteNFe();
var
  Recibo, LoteStr: String;
  Empresa, Lote, Modelo: Integer;
begin
  Recibo := EdRecibo.Text;
  //
  if (Recibo <> '')  then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando consulta');
    if not DefineEmpresa(Empresa) then Exit;
    if not DefineLote(Lote) then Exit;
    Modelo := EdIde_mod.ValueVariant;
    //if not DefineModelo(Modelo) then Exit;
    //
    DmNFe_0000.ReopenEmpresa(Empresa);
    //
    FTextoArq     := '';
    Screen.Cursor := CrHourGlass;
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando o servidor do fisco');
      //
(*
      FTextoArq := FmNFeGeraXML_0400.WS_NFeRetRecepcao(EdUF_Servico.Text, FAmbiente_Int,
        FCodigoUF_Int, EdRecibo.Text, LaAviso1, LaAviso2, RETxtEnvio, EdWebService, Modelo);
*)
      DmNFe_0000.ConfigurarComponenteNFe();
      DmNFe_0000.ACBrNFe1.WebServices.Recibo.Recibo := EdRecibo.Text;
      DmNFe_0000.ACBrNFe1.WebServices.Recibo.Executar;

      FTextoArq := DmNFe_0000.ACBrNFe1.WebServices.Recibo.RetWS;
      //memoRespWS.Lines.Text := DmNFe_0000.ACBrNFe1.WebServices.Recibo.RetornoWS;
      //LoadXML(DmNFe_0000.ACBrNFe1.WebServices.Recibo.RetornoWS, WBResposta);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      if Pos('Erros:', FTextoArq) > 0 then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2,False, 'O fisco acusou erros na resposta!');
        Geral.MB_Erro('Resposta recebida com Erros!');
      end else
      begin
        DmNFe_0000.ReopenEmpresa(Empresa);
        //
        LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
        DmNFe_0000.SalvaXML(NFE_EXT_PRO_REC_XML, LoteStr, FTextoArq, RETxtRetorno, False);
        //
        LerTextoConsultaLoteNFe();
        try
          if FFormChamou = 'FmNFeLEnc_0400' then
            FmNFeLEnc_0400.LocCod(Lote, Lote);
          if FFormChamou = 'FmNFeLEnU_0400' then
            FmNFeLEnU_0400.ReabreNFeLEnc(Lote);
        except
          Geral.MB_Aviso('N�o foi poss�vel localizar o lote de NF-e(s) n�mero ' +
          EdLote.Text + '!');
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end
  else
    Geral.MB_Aviso('Recibo n�o informado para consulta...');
end;

procedure TFmDmkACBrNFeSteps_0400.ExecutaConsultaNFe();
var
  Empresa, IDCtrl: Integer;
  chNFe: String;
  Id, Dir, Aviso: String;
begin
  if not DefineEmpresa(Empresa) then Exit;
  if not DefinechNFe(chNFe) then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
(*
    FTextoArq := FmNFeGeraXML_0400.WS_NFeConsultaNF(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, chNFe, LaAviso1, LaAviso2,
      RETxtEnvio, EdWebService, EdIde_mod.ValueVariant);
*)
    DmNFe_0000.ConfigurarComponenteNFe();
    DmNFe_0000.ACBrNFe1.NotasFiscais.Clear;
    DmNFe_0000.ACBrNFe1.WebServices.Consulta.NFeChave := chNFe;
    DmNFe_0000.ACBrNFe1.WebServices.Consulta.Executar;

    FTextoArq := DmNFe_0000.ACBrNFe1.WebServices.Consulta.RetWS;

    MostraTextoRetorno(FTextoArq);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Resposta recebida com Sucesso!');
    //
    DmNFe_0000.SalvaXML(NFE_EXT_SIT_XML, chNFe, FTextoArq, RETxtRetorno, False);
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros!');
    end;
    if not FNaoExecutaLeitura then
    begin
      LerTextoConsultaNFe();
      //
(*
      QrCabA.Close;
      QrCabA.Params[00].AsInteger := EdEmpresa.ValueVariant;
      QrCabA.Params[01].AsString  := chNFe;
      UnDmkDAC_PF.AbreQuery(QrCabA, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
*)

      UnDmkDAC_PF.AbreMySQLQuery0(QrCabA, Dmod.MyDB, [
      'SELECT IDCtrl, infProt_ID, infProt_nProt ',
      'FROM nfecaba ',
      'WHERE Empresa=' + Geral.FF0(Empresa),
      'AND id="' + chNFe + '" ',
      '']);
      //
      IDCtrl := QrCabAIDCtrl.Value;
      //
      if IDCtrl > 0 then
      begin
        Id := QrCabAinfProt_ID.Value;
        //
        if Trim(Id) = '' then
          Id := QrCabAinfProt_nProt.Value;
        //
        Dir   := DModG.QrPrmsEmpNFeDirSit.Value;
        Aviso := '';
        //
        DmNFe_0000.AtualizaXML_No_BD_ConsultaNFe(chNFe, Id, IDCtrl, Dir, Aviso);
        //
        if Aviso <> '' then
          Geral.MB_Aviso('Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);
      end;
    end else
    begin
      LerTextoConsultaNFe();
      Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDmkACBrNFeSteps_0400.ExecutaEnvioDeLoteEvento();
const
  sProcName = 'TFmDmkACBrNFeSteps_0400.ExecutaEnvioDeLoteEvento()';
var
  retWS, envWS, rtfDadosMsg, LoteStr, sMsg, DH_TZD, dhEmiTZD: String;
  Lote, Empresa: Integer;
  ok: Boolean;
  // ini 2023-07-06
  //Evento: TpcnTpEvento;
  Evento: Integer;
  //s: String;
  // fim 2023-07-06
  dhEmi: TDateTime;
  TipoAutor: TpcnTipoAutor;
  TipoNF: TpcnTipoNFe;
begin
  if not DefineLote(Lote) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  //
  if not CkSoLer.Checked then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando lote ao fisco');
    //
    if not FileExists(FPathLoteEvento) then
    begin
      Geral.MB_Erro('O lote "' + FPathLoteEvento + '" n�o foi localizado!');
      Exit;
    end;
    rtfDadosMsg := dmkPF.LoadFileToText(FPathLoteEvento);
    //
    if rtfDadosMsg = '' then
    begin
      Geral.MB_Erro('O lote de eventos "' + FPathLoteEvento +
        '" foi carregado mas est� vazio!');
      Exit;
    end;
    retWS :='';
    FTextoArq :='';
    Screen.Cursor := crHourGlass;
    try
{
      FTextoArq := FmNFeGeraXML_0400.WS_RecepcaoEvento(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, EdSerialNumber.Text, Lote, LaAviso1, LaAviso2, RETxtEnvio,
      EdWebService, EdIde_mod.ValueVariant);
}
      DmNFe_0000.ConfigurarComponenteNFe();
      DmNFe_0000.ACBrNFe1.EventoNFe.Evento.Clear;
      UnDmkDAC_PF.AbreMySQLQuery0(DmNFe_0000.QrNFeEveRCab1, Dmod.MyDB, [
      'SELECT nec.FatID, nec.FatNum, nec.Empresa, nec.Controle, nec.versao,',
      'nec.tpAmb, nec.chNFe, nec.dhEvento, nec.tpEvento, nec.descEvento,',
      'nec.Status, nec.ret_cStat, nec.ret_nProt, nec.ret_dhRegEvento, nec.verEvento,',
      'nec.XML_Eve, nec.XML_retEve, nec.versao, duf.Nome UF_Nome, nec.cOrgao,',
      'IF(nec.CNPJ<>"", nec.CNPJ, nec.CPF) CNPJ_CPF, nec.nSeqEvento',
      'FROM nfeevercab nec',
      'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_ufs duf ON duf.Codigo=nec.cOrgao',
      'WHERE nec.EventoLote=' + Geral.FF0(Lote),
      '']);
      DmNFe_0000.QrNFeEveRCab1.First;

      while not DmNFe_0000.QrNFeEveRCab1.Eof do
      begin
        // ini 2023-06-07
        //s := Geral.FF0(DmNFe_0000.QrNFeEveRCab1tpEvento.Value);
        //Evento := TpcnTpEvento(StrToEnumerado2(ok, s, TpcnTpEventoString));
        Evento := DmNFe_0000.QrNFeEveRCab1tpEvento.Value;
        // fim 2023-06-07
        //
        case Evento of
          //TpcnTpEvento.teCCe:
          NFe_CodEventoCCe: // 110110
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(DmNFe_0000.QrNFeEveRCCe, Dmod.MyDB, [
            'SELECT * FROM nfeevercce ',
            'WHERE FatID=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1FatID.Value),
            'AND FatNum=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1FatNum.Value),
            'AND Empresa=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1Empresa.Value),
            'AND Controle=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1Controle.Value),
            'ORDER BY Conta DESC ',
            '']);
            //
            with DmNFe_0000.ACBrNFe1.EventoNFe.Evento.New do
            begin
              infEvento.chNFe := DmNFe_0000.QrNFeEveRCab1chNFe.Value; //Chave;
              infEvento.CNPJ   := Geral.SoNumero_TT(DmNFe_0000.QrNFeEveRCab1CNPJ_CPF.Value);
              infEvento.dhEvento := DmNFe_0000.QrNFeEveRCab1dhEvento.Value; //now;
              infEvento.tpEvento := TpcnTpEvento.teCCe; //Evento; //teCCe;
              infEvento.nSeqEvento := DmNFe_0000.QrNFeEveRCab1nSeqEvento.Value; //StrToInt(nSeqEvento);
              infEvento.detEvento.xCorrecao := DmNFe_0000.QrNFeEveRCCexCorrecao.Value; //Correcao;
            end;
          end;
          //TpcnTpEvento.teCancelamento:
          NFe_CodEventoCan: // 110111
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(DmNFe_0000.QrNFeEveRCan, Dmod.MyDB, [
            'SELECT * FROM nfeevercan ',
            'WHERE FatID=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1FatID.Value),
            'AND FatNum=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1FatNum.Value),
            'AND Empresa=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1Empresa.Value),
            'AND Controle=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1Controle.Value),
            'ORDER BY Conta DESC ',
            '']);
            //
            with DmNFe_0000.ACBrNFe1.EventoNFe.Evento.New do
            begin
              infEvento.chNFe := DmNFe_0000.QrNFeEveRCab1chNFe.Value; //Chave;
              infEvento.CNPJ   := Geral.SoNumero_TT(DmNFe_0000.QrNFeEveRCab1CNPJ_CPF.Value);
              infEvento.dhEvento := DmNFe_0000.QrNFeEveRCab1dhEvento.Value; //now;
              infEvento.tpEvento := TpcnTpEvento.teCancelamento; //Evento; //teCancelamento;
              infEvento.detEvento.xJust := DmNFe_0000.QrNFeEveRCanxJust.Value; //Justificativa;
              infEvento.detEvento.nProt := Geral.FF0(DmNFe_0000.QrNFeEveRCannProt.Value); //Protocolo;
            end;
          end;
          //TpcnTpEvento.teEPEC:
          NFe_CodEventoEPEC: // 110140
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(DmNFe_0000.QrNFeEveREPEC, Dmod.MyDB, [
            'SELECT * FROM nfeeverepec ',
            'WHERE FatID=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1FatID.Value),
            'AND FatNum=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1FatNum.Value),
            'AND Empresa=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1Empresa.Value),
            'AND Controle=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1Controle.Value),
            'ORDER BY Conta DESC ',
            '']);
            //
            with DmNFe_0000.ACBrNFe1.EventoNFe.Evento.New do
            begin
              infEvento.cOrgao := 91;
              dhEmi  := DmNFe_0000.QrNFeEveREPECdhEmi.Value;
              dhEmiTZD := dmkPF.TZD_UTC_FloatToSignedStr(DmNFe_0000.QrNFeEveREPECdhEmiTZD.Value);
              DH_TZD := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', dhEmi) + dhEmiTZD;
              TipoAutor := StrToTipoAutor(ok, Geral.FF0(DmNFe_0000.QrNFeEveREPECtpAutor.Value));
              //TipoNF:= StrToTipoNFe(ok, Geral.FF0(DmNFe_0000.QrNFeEveREPECtpNF.Value)); //TpcnTipoNFe;
              TipoNF := StrToTpNF(ok, Geral.FF0(DmNFe_0000.QrNFeEveREPECtpNF.Value));
              //
              infEvento.chNFe := DmNFe_0000.QrNFeEveRCab1chNFe.Value; //Chave;
              infEvento.CNPJ   := Geral.SoNumero_TT(DmNFe_0000.QrNFeEveRCab1CNPJ_CPF.Value);
              infEvento.dhEvento := DmNFe_0000.QrNFeEveRCab1dhEvento.Value; //now;
              infEvento.tpEvento := TpcnTpEvento.teEPECNFe; //Evento; //teEPEC;
              //infEvento.detEvento.xJust := DmNFe_0000.QrNFeEveREPECxJust.Value; //Justificativa;
              //infEvento.detEvento.nProt := Geral.FF0(DmNFe_0000.QrNFeEveREPECnProt.Value); //Protocolo;
              infEvento.detEvento.versao := Geral.FFT_Dot(DmNFe_0000.QrNFeEveREPECversao.Value, 2, siPositivo);
              infEvento.detEvento.descEvento := 'EPEC';
              infEvento.detEvento.cOrgaoAutor := DmNFe_0000.QrNFeEveREPECcOrgaoAutor.Value;
              infEvento.detEvento.tpAutor := TipoAutor; // DmNFe_0000.QrNFeEveREPECtpAutor.Value;
              infEvento.detEvento.verAplic := DmNFe_0000.QrNFeEveREPECverAplic.Value;
              infEvento.detEvento.dhEmi := DmNFe_0000.QrNFeEveREPECdhEmi.Value; //DH_TZD;
              infEvento.detEvento.tpNF := TipoNF; //DmNFe_0000.QrNFeEveREPECtpNF.Value;
              infEvento.detEvento.IE := DmNFe_0000.QrNFeEveREPECIE.Value;
              infEvento.detEvento.dest.UF := DmNFe_0000.QrNFeEveREPECdest_UF.Value;
              if DmNFe_0000.QrNFeEveREPECdest_CNPJ.Value <> EmptyStr then
                //infEvento.detEvento.dest.CNPJ := DmNFe_0000.QrNFeEveREPECdest_CNPJ.Value;
                infEvento.detEvento.dest.CNPJCPF := DmNFe_0000.QrNFeEveREPECdest_CNPJ.Value
              else
              if DmNFe_0000.QrNFeEveREPECdest_CPF.Value <> EmptyStr then
                //infEvento.detEvento.dest.CPF := DmNFe_0000.QrNFeEveREPECdest_CPF.Value;
                infEvento.detEvento.dest.CNPJCPF := DmNFe_0000.QrNFeEveREPECdest_CPF.Value
              else
                infEvento.detEvento.dest.idEstrangeiro := DmNFe_0000.QrNFeEveREPECdest_idEstrangeiro.Value;
              infEvento.detEvento.dest.IE := DmNFe_0000.QrNFeEveREPECdest_IE.Value;
              infEvento.detEvento.vNF := DmNFe_0000.QrNFeEveREPECvNF.Value;
              infEvento.detEvento.vICMS := DmNFe_0000.QrNFeEveREPECvICMS.Value;
              infEvento.detEvento.vST := DmNFe_0000.QrNFeEveREPECvST.Value;
            end;
          end;
(*
          TpcnTpEvento.teManifDestConfirmacao,
          TpcnTpEvento.teManifDestCiencia,
          TpcnTpEvento.teManifDestDesconhecimento,
          TpcnTpEvento.teManifDestOperNaoRealizada:
*)
          // Manifesta��o do destinat�rio
          NFe_CodEventoMDeConfirmacao, // = 210200;
          NFe_CodEventoMDeCiencia    , // = 210210;
          NFe_CodEventoMDeDesconhece , // = 210220;
          NFe_CodEventoMDeNaoRealizou: // = 210240;
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(DmNFe_0000.QrNFeEveRMDe, Dmod.MyDB, [
            'SELECT * FROM nfeevermde ',
            'WHERE FatID=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1FatID.Value),
            'AND FatNum=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1FatNum.Value),
            'AND Empresa=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1Empresa.Value),
            'AND Controle=' + Geral.FF0(DmNFe_0000.QrNFeEveRCab1Controle.Value),
            'ORDER BY Conta DESC ',
            '']);
            //
            with DmNFe_0000.ACBrNFe1.EventoNFe.Evento.New do
            begin
              infEvento.chNFe := DmNFe_0000.QrNFeEveRCab1chNFe.Value; //Chave;
              infEvento.CNPJ   := Geral.SoNumero_TT(DmNFe_0000.QrNFeEveRCab1CNPJ_CPF.Value);
              infEvento.dhEvento := DmNFe_0000.QrNFeEveRCab1dhEvento.Value; //now;
              //infEvento.tpEvento := Evento; //te????;
              case Evento of
                NFe_CodEventoMDeConfirmacao: infEvento.tpEvento := TpcnTpEvento.teManifDestConfirmacao; // = 210200;
                NFe_CodEventoMDeCiencia: infEvento.tpEvento := TpcnTpEvento.teManifDestCiencia; // = 210210;
                NFe_CodEventoMDeDesconhece: infEvento.tpEvento := TpcnTpEvento.teManifDestDesconhecimento; // = 210220;
                NFe_CodEventoMDeNaoRealizou: infEvento.tpEvento := TpcnTpEvento.teManifDestOperNaoRealizada; // = 210240;
              end;
              infEvento.nSeqEvento := DmNFe_0000.QrNFeEveRCab1nSeqEvento.Value; //StrToInt(nSeqEvento);
              infEvento.DetEvento.xJust := DmNFe_0000.QrNFeEveRMDexJust.Value;
            end;
          end
  (*      fim 2022-02-16 - N�o tem no ACBr ???? *)
          else
          begin
            sMsg := GetEnumName(TypeInfo(TpcnTpEvento), Integer(VarType(Evento)));
            //sMsg := Geral.FF0(Evento);
            Geral.MB_Erro('Evento ' + sMsg + ' n�o implementado em ' + sProcName);
            Exit;
          end;
        end;
        DmNFe_0000.QrNFeEveRCab1.Next;
      end;

      DmNFe_0000.ACBrNFe1.EnviarEvento(Lote);
      (*
      EnvWS := DmNFe_0000.ACBrNFe1.WebServices.EnvEvento.CabMsg;
      //Geral.MB_Teste(EnvWS);
      EnvWS := DmNFe_0000.ACBrNFe1.WebServices.EnvEvento.DadosMsg;
      //Geral.MB_Teste(EnvWS);
      *)
      //
      FTextoArq := DmNFe_0000.ACBrNFe1.WebServices.EnvEvento.RetWS;
        //
      MyObjects.Informa2(LaAviso1, LaAviso2,False, 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      //
      DmNFe_0000.ReopenEmpresa(Empresa);
      LoteStr := FormatFloat('000000000', Lote);
      DmNFe_0000.SalvaXML(NFE_EXT_EVE_RET_LOT_XML, LoteStr, FTextoArq, RETxtRetorno, False);
      //
      if LerTextoEnvioLoteEvento() then ;//Close;
      //
      if Pos('Erros:', FTextoArq) > 0 then
      begin
        Geral.MB_Erro('Erro na chamada do WS...' + sLineBreak + FTextoArq);
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
      end else
        Close;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmDmkACBrNFeSteps_0400.ExecutaEnvioDeLoteNFe(Sincronia: TXXeIndSinc);
const
  ImprimirACBr = False;
  ZipadoACBr = False;
var
  SincronoACBr: Boolean;
var
  retWS, rtfDadosMsg, LoteStr: String;
  Lote, Empresa: Integer;
begin
  if not DefineLote(Lote) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  //
  SincronoACBr :=  Sincronia = TXXeIndSinc.nisSincrono;
  //
  if not CkSoLer.Checked then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando lote ao fisco');
    if not FileExists(FPathLoteNFe) then
    begin
      Geral.MB_Erro('O lote "' + FPathLoteNFe + '" n�o foi localizado!');
      Exit;
    end;
(*
    if dmkPF.CarregaArquivo(FPathLoteNFe, rtfDadosMsg) then
    begin
      if rtfDadosMsg = '' then
      begin
        Geral.MB_Erro('O lote de NFe "' + FPathLoteNFe +
        '" foi carregado mas est� vazio!');
        Exit;
      end;
      retWS :='';
      FTextoArq :='';
      Screen.Cursor := crHourGlass;
*)
      try
(*
        FTextoArq := FmNFeGeraXML_0400.WS_NFeRecepcaoLote(EdUF_Servico.Text,
        FAmbiente_Int, FCodigoUF_Int, EdSerialNumber.Text, Lote, LaAviso1, LaAviso2, RETxtEnvio,
        EdWebService, Sincronia, EdIde_mod.ValueVariant);
*)

        //Result := EnviarNFe_ACBr(Modelo);
        try
          DmNFe_0000.ACBrNFe1.Enviar(Lote, ImprimirACBr, SincronoACBr, ZipadoACBr);
        except
          on E: Exception do
            Geral.MB_Erro('Mensagem de retorno: ' + sLineBreak +
            E.Message);
        end;
        //if SincronoACBr then
          FTextoArq := DmNFe_0000.ACBrNFe1.WebServices.Enviar.RetWS;
          // ini 2022-03-01
          //http://schemas.xmlsoap.org/soap/envelope/
          //RETxtEnvio.Text := DmNFe_0000.ACBrNFe1.WebServices.Enviar.XMLEnvio;
          // fim 2022-03-01
        //else
          //FTextoArq := DmNFe_0000.ACBrNFe1.WebServices.Retorno.RetWS;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Sucesso!');
        MostraTextoRetorno(FTextoArq);
        //
        DmNFe_0000.ReopenEmpresa(Empresa);
        LoteStr := FormatFloat('000000000', Lote);
        DmNFe_0000.SalvaXML(NFE_EXT_REC_XML, LoteStr, FTextoArq, RETxtRetorno, False);
        //
        Timer1.Enabled := Sincronia = TXXeIndSinc.nisAssincrono;
      finally
        Screen.Cursor := crDefault;
      end;
    //end;
  end;
  if LerTextoEnvioLoteNFe() then ;
end;

procedure TFmDmkACBrNFeSteps_0400.ExecutaInutilizaNumerosNF();
var
  Id, xJust, Modelo, Serie, nNFIni, nNFFin, EmitCNPJ, LoteStr: String;
  Empresa, Lote, K: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados a serem enviados ao servidor do fisco');
  if not DefineEmpresa(Empresa) then Exit;
  if not DefineLote(Lote) then Exit;
  if not DefineModelo(Modelo) then Exit;
  if not DefineSerie(Serie) then Exit;
  if not DefinenNFIni(nNFIni) then Exit;
  if not DefinenNFFin(nNFFin) then Exit;
  if not DefineEmitCNPJ(EmitCNPJ) then Exit;
  //
  xJust := Trim(XXe_PF.ValidaTexto_XML(CBNFeJust.Text, 'xJust', 'xJust'));
  K := Length(xJust);
  if K < 15 then
  begin
    Geral.MB_Aviso('A justificativa deve ter pelo menos 15 caracteres!' +
    sLineBreak + 'O texto "' + xJust + '" tem apenas ' + IntToStr(K)+'.');
    Exit;
  end;
  xJust := Geral.TFD(FloatToStr(QrNFeJustCodigo.Value), 10, siPositivo) + ' - ' + xJust;
  Screen.Cursor := CrHourGlass;
  try
  {
    FTextoArq := FmNFeGeraXML_0400.WS_NFeInutilizacaoNFe(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, EdAno.ValueVariant, Id, emitCNPJ, Modelo,
      Serie, nNFIni, nNFFin, XJust, EdSerialNumber.Text, LaAviso1, LaAviso2, RETxtEnvio,
      EdWebService, EdIde_mod.ValueVariant);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebido com Sucesso!');
    //
 }
    //
    DmkACBr_ParamsEmp.ConfigurarComponente(DmNFe_0000.ACBrNFe1);
    //DmNFe_0000.ACBrNFe1.WebServices.Inutiliza(edtEmitCNPJ.Text, Justificativa, StrToInt(Ano), StrToInt(Modelo), StrToInt(Serie), StrToInt(NumeroInicial), StrToInt(NumeroFinal));
    DmNFe_0000.ACBrNFe1.WebServices.Inutiliza(EmitCNPJ, xJust, EdAno.ValueVariant,  StrToInt(Modelo), StrToInt(Serie), StrToInt(nNFIni), StrToInt(nNFFin));
    FTextoArq := DmNFe_0000.ACBrNFe1.WebServices.Inutilizacao.RetWS;
    // Salva arquivo
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando resposta');
    //
    DmNFe_0000.MontaID_Inutilizacao(FCodigoUF_Txt, EdAno.Text, emitCNPJ, Modelo, Serie,
      nNFIni, nNFFin, Id);
    //
    LoteStr := Id + '_' + FormatFloat('000000000', Lote);
    DmNFe_0000.SalvaXML(NFE_EXT_INU_XML, LoteStr, FTextoArq, RETxtRetorno, False);

     //
    LerTextoInutilizaNumerosNF();
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDmkACBrNFeSteps_0400.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDmkACBrNFeSteps_0400.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //ACBrNFe1 := TACBrNFe.Create(Self);
  //
  //FSiglas_WS  := Geral.SiglasWebService();

  FCodStausServico := 0;
  FTxtStausServico := 'N�o consultado!';
  //FindCont := 1;
  //FultNSU  := 0;
  //FNSU     := 0;
  //
  Self.Height := 730;
  ImgTipo.SQLType := stLok;
  //
  FTextoArq :='';
  FNaoExecutaLeitura := False;
  FSecWait := 15;
  Timer1.Enabled := False;
  FSegundos := 0;
  //
  PageControl1.ActivePageIndex := 0;
  //FSiglas_WS  := Geral.SiglasWebService();
  FFormChamou := '';
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Configurando conforme solicita��o');
  if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then
  begin
    EdLote.Enabled     := True;
    EdEmpresa.Enabled  := True;
    EdRecibo.Enabled   := True;
    //
    BtAbrir.Enabled    := True;
  end;
  //
  LaWait.Visible := True;
  LaWait.Font.Color := clBlue;
  MyObjects.Informa(LaWait, False, '...');
end;

procedure TFmDmkACBrNFeSteps_0400.FormDestroy(Sender: TObject);
begin
  try
    //FreeAndNil(ACBrNFe1);
    //FreeAndNil(ACBrIntegrador1);
  except
    // Nada
  end;
end;

procedure TFmDmkACBrNFeSteps_0400.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDmkACBrNFeSteps_0400.HabilitaBotoes(Visivel: Boolean);
begin
  PnConfirma.Visible := Visivel;
end;

function TFmDmkACBrNFeSteps_0400.LeNoXML(No: IXMLNode; Tipo: TTipoNoXML;
  Tag: TTipoTagXML; AvisaVersao: Boolean): String;
var
  Texto: String;
begin
  Result := '';
  case Tipo of
    tnxTextStr: Result := GetNodeTextStr(No, ObtemNomeDaTag(Tag), '');
    tnxAttrStr: Result := GetNodeAttrStr(No, ObtemNomeDaTag(Tag), '');
    else Result := '???' + ObtemNomeDaTag(Tag) + '???';
  end;
  //
  if (Tag = ttx_Versao) and (Result <> FverXML_versao) then
    if AvisaVersao then
      Geral.MB_Aviso('Vers�o do XML difere do esperado!' +
      sLineBreak + 'Vers�o informada: ' + Result + sLineBreak +
      'Verifique a vers�o do servi�o no cadastro das op��es da filial para a vers�o: '
      + Result);
  if (Tag = ttx_dhRecbto) then XXe_PF.Ajusta_dh_XXe(Result);
  if Result <> '' then
  begin
    case Tag of
      ttx_tpAmb: Texto := Result + ' - ' + XXe_PF.ObtemNomeAmbiente(Result);
      ttx_tMed : Texto := Result + ' segundos';
      ttx_cUF  : Texto := Result + ' - ' +
                 Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(StrToInt(Result));
      else Texto := Result;
    end;
  end;
  //
  MeInfo.Lines.Add(ObtemDescricaoDaTag(Tag) + ' = ' + Texto);
end;

procedure TFmDmkACBrNFeSteps_0400.LerTextoConsultaCadastro();
const
  //
  sProcName = 'TFmDmkACBrNFeSteps_0400.LerTextoConsultaCadastro()';
  //
  function CorrigeDataStringVazio(Data: String): String;
  begin
    if Data = '' then
      Result := '0000-00-00'
    else
      Result := Data;
  end;
var
  versao, verAplic, cStat, xMotivo, UF, cUF, CPF, CNPJ, dhCons: String;
  //
begin
  FCCC_RetrornoUtil := False;
  FCCC_IE           := '';
  FCCC_CNPJ         := '';
  FCCC_CPF          := '';
  FCCC_UF           := '';
  FCCC_cSit         := 0;
  FCCC_indCredNFe   := 0;
  FCCC_indCredCTe   := 0;
  FCCC_xNome        := '';
  FCCC_xRegApur     := '';
  FCCC_CNAE         := 0;
  FCCC_dIniAtiv     := '00000-00-00';
  FCCC_dUltSit      := '00000-00-00';
  FCCC_IEUnica      := '';
  FCCC_IEAtual      := '';
  FCCC_xLgr         := '';
  FCCC_nro          := 0;
  FCCC_xBairro      := '';
  FCCC_xMun         := '';
  FCCC_cMun         := 0;
  FCCC_CEP          := 0;
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConsCad.Value, 2, siNegativo);
(*
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
*)
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    //xmlNode := xmlDoc.SelectSingleNode('/retEnviNFe');
    xmlNode := xmlDoc.SelectSingleNode('/retConsCad');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
(*
  <retConsCad versao="2.00" xmlns="http://www.portalfiscal.inf.br/nfe">
    <infCons>
      <verAplic>RSa20210809110246</verAplic>
      <cStat>111</cStat>
      <xMotivo>Consulta cadastro com uma ocorrencia</xMotivo>
      <UF>RS</UF>
      <CNPJ>96734892000123</CNPJ>
      <dhCons>2022-02-20T22:43:40</dhCons>
      <cUF>43</cUF>
      <infCad>
        <IE>1240007512</IE>
        <CNPJ>96734892000123</CNPJ>
        <UF>RS</UF>
        <cSit>1</cSit>
        <indCredNFe>2</indCredNFe>
        <indCredCTe>4</indCredCTe>
        <xNome>TFL DO BRASIL IND QUIMICA LTDA</xNome>
        <xRegApur>GERAL</xRegApur>
        <CNAE>2099199</CNAE>
        <dIniAtiv>1944-09-01</dIniAtiv>
        <dUltSit>1995-06-01</dUltSit>
        <ender>
          <xLgr>RUA SANTO AGOSTINHO</xLgr>
          <nro>1099</nro>
          <xBairro>SAO MIGUEL</xBairro>
          <cMun>4318705</cMun>
          <xMun>São Leopoldo</xMun>
          <CEP>93025700</CEP>
        </ender>
      </infCad>
    </infCons>
  </retConsCad>
*)
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      xmlNode := xmlDoc.SelectSingleNode('/retConsCad/infCons');
      if assigned(xmlNode) then
      begin
        verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
        cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
        xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
        UF       := LeNoXML(xmlNode, tnxTextStr, ttx_UF);
        CNPJ     := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
        CPF      := LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
        dhCons   := LeNoXML(xmlNode, tnxTextStr, ttx_dhCons); // 2022-02-20T22:43:40
        cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
        //
        FCCC_cStat    := Geral.IMV(cStat);
        FCCC_xMotivo  := xMotivo;


        xmlNode := xmlDoc.SelectSingleNode('/retConsCad/infCons/infCad');
        if assigned(xmlNode) then
        begin
(*==============================================================================
Dados da situa��o cadastral
Esta estrutura existe somente para as consultas
realizadas com sucesso cStat=111, com
possibilidade de m�ltiplas ocorr�ncias (Ex.:
consulta por IE de contribuinte com Inscri��o
�nica � retorno de todos os estabelecimentos
do contribuinte).
================================================================================
      <infCad>
        <IE>1240007512</IE>
        <CNPJ>96734892000123</CNPJ>
        <UF>RS</UF>
        <cSit>1</cSit>
        <indCredNFe>2</indCredNFe>
        <indCredCTe>4</indCredCTe>
        <xNome>TFL DO BRASIL IND QUIMICA LTDA</xNome>
        <xRegApur>GERAL</xRegApur>
        <CNAE>2099199</CNAE>
        <dIniAtiv>1944-09-01</dIniAtiv>
        <dUltSit>1995-06-01</dUltSit>
*)
          FCCC_IE           := LeNoXML(xmlNode, tnxTextStr, ttx_IE);
          FCCC_CNPJ         := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
          FCCC_CPF          := LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
          FCCC_UF           := LeNoXML(xmlNode, tnxTextStr, ttx_UF);
          FCCC_cSit         := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_cSit));
          FCCC_indCredNFe   := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_indCredNFe));
          FCCC_indCredCTe   := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_indCredCTe));
          FCCC_xNome        := LeNoXML(xmlNode, tnxTextStr, ttx_xNome);
          FCCC_xRegApur     := LeNoXML(xmlNode, tnxTextStr, ttx_xRegApur);
          FCCC_CNAE         := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_CNAE));
          FCCC_dIniAtiv     := CorrigeDataStringVazio(LeNoXML(xmlNode, tnxTextStr, ttx_dIniAtiv));
          FCCC_dUltSit      := CorrigeDataStringVazio(LeNoXML(xmlNode, tnxTextStr, ttx_dUltSit));
          FCCC_IEUnica      := LeNoXML(xmlNode, tnxTextStr, ttx_IEUnica);
          FCCC_IEAtual      := LeNoXML(xmlNode, tnxTextStr, ttx_IEAtual);
          //
          FCCC_RetrornoUtil := True;
          //
       end;
        xmlNode := xmlDoc.SelectSingleNode('/retConsCad/infCons/infCad/ender');
        if assigned(xmlNode) then
        begin
(*==============================================================================
  (*
          <ender>
            <xLgr>RUA SANTO AGOSTINHO</xLgr>
            <nro>1099</nro>
            <xBairro>SAO MIGUEL</xBairro>
            <cMun>4318705</cMun>
            <xMun>São Leopoldo</xMun>
            <CEP>93025700</CEP>
          </ender>
  *)
(*
<retConsCad versao='2.00' xmlns='http://www.portalfiscal.inf.br/nfe'>
  <infCons>
    <verAplic>PR-v4_7_49</verAplic>
    <cStat>111</cStat>
    <xMotivo>Consulta cadastro com uma ocorrencia</xMotivo>
    <UF>PR</UF>
    <CNPJ>29402622002848</CNPJ>
    <dhCons>2022-02-21T11:06:04.699-03:00</dhCons>
    <cUF>41</cUF>
    <infCad>
      <IE>9079538660</IE>
      <CNPJ>29402622002848</CNPJ>
      <UF>PR</UF>
      <cSit>1</cSit>
      <indCredNFe>4</indCredNFe>
      <indCredCTe>4</indCredCTe>
      <xNome>YELLOW MOUNTAIN DISTRIBUIDORA DE VEICULOS LTDA</xNome>
      <xRegApur>Normal - Normal</xRegApur>
      <CNAE>4511101</CNAE>
      <dIniAtiv>2018-10-01-03:00</dIniAtiv>
      <dUltSit>2018-11-06-02:00</dUltSit>
      <ender>
        <xLgr>AV COLOMBO</xLgr>
        <nro>2259</nro>
        <xCpl>LTE 93/13-L;</xCpl>
        <xBairro>VILA NOVA</xBairro>
        <cMun>4115200</cMun>
        <xMun>MARINGA</xMun>
        <CEP>87045000</CEP>
      </ender>
    </infCad>
  </infCons>
</retConsCad>
*)

          FCCC_xLgr      := LeNoXML(xmlNode, tnxTextStr, ttx_xLgr);
          FCCC_nro       := Geral.IMV('0' + Geral.SoNumero_TT(LeNoXML(xmlNode, tnxTextStr, ttx_nro)));
          FCCC_xCpl      := LeNoXML(xmlNode, tnxTextStr, ttx_xCpl);
          FCCC_xBairro   := LeNoXML(xmlNode, tnxTextStr, ttx_xBairro);
          FCCC_cMun      := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_cMun));
          FCCC_xMun      := LeNoXML(xmlNode, tnxTextStr, ttx_xMun);
          FCCC_CEP       := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_CEP));
          //
        end;
      end;
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado!');
  end;
end;

procedure TFmDmkACBrNFeSteps_0400.LerTextoConsultaLoteNFe();
var
  Codigo, Controle, FatID, FatNum, Empresa, Status: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, nRec, dhRecbto, tMed,
  infProt_Id, infProt_chNFe, infProt_nProt, infProt_digVal, infProt_tpAmb,
  infProt_verAplic, infProt_dhRecbto, infProt_cStat, infProt_xMotivo: String;
  dhRecbtoTZD, infProt_dhRecbtoTZD: Double;
begin
  //FverXML_versao := verConsReciNFe_Versao;
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConLot.Value, 2, siNegativo);
  dhRecbto       := '0000-00-00';
  dhRecbtoTZD    := 0;
  tMed           := '0';
  //
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de lote de envio
    xmlNode := xmlDoc.SelectSingleNode('/retConsReciNFe');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      nRec     := LeNoXML(xmlNode, tnxTextStr, ttx_nRec);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      //
      Status := DmNFe_0000.stepLoteEnvConsulta();
      Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenm', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelenm', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cStat', 'xMotivo',
        'cUF', 'nRec', 'dhRecbto', 'tMed',
        'Status', 'dhRecbtoTZD'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cStat, xMotivo,
        cUF, nRec, dhRecbto, tMed,
        Status, dhRecbtoTZD
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfelenc', False, [
          'versao', 'tpAmb', 'verAplic',
          'cStat', 'xMotivo', 'cUF',
          'nRec', 'dhRecbto', 'tMed',
          'dhRecbtoTZD'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cStat, xMotivo, cUF,
          nRec, dhRecbto, tMed,
          dhRecbtoTZD
        ], [Codigo], True) then
        begin
          xmlList := xmlDoc.SelectNodes('/retConsReciNFe/protNFe/infProt');
          if xmlList.Length > 0 then
          begin
            while xmlList.Length > 0 do
            begin
              //
              infProt_Id       := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_Id);
              infProt_tpAmb    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_tpAmb);
              infProt_verAplic := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_verAplic);
              infProt_chNFe    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_chNFe);
              infProt_dhRecbto := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_dhRecbto);
              infProt_nProt    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_nProt);
              infProt_digVal   := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_digVal);
              infProt_cStat    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_cStat);
              infProt_xMotivo  := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_xMotivo);
              //
              XXe_PF.Ajusta_dh_XXe_UTC(infProt_dhRecbto, infProt_dhRecbtoTZD);
              //
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                'Status', 'infProt_Id', 'infProt_tpAmb',
                'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
                'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
                'protNFe_versao', 'infProt_dhRecbtoTZD'
              ], ['ID', 'LoteEnv'], [
                infProt_cStat, infProt_Id, infProt_tpAmb,
                infProt_verAplic, infProt_dhRecbto, infProt_nProt,
                infProt_digVal, infProt_cStat, infProt_xMotivo,
                versao, infProt_dhRecbtoTZD
              ], [infProt_chNFe, Codigo], True) then
              begin
                // hist�rico da NF
(*
                QrNFeCabA1.Close;
                QrNFeCabA1.Params[00].AsString  := infProt_chNFe;
                QrNFeCabA1.Params[01].AsInteger := Codigo;
                QrNFeCabA1.Open ;
*)
                UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA1, Dmod.MyDB, [
                'SELECT * ',
                'FROM nfecaba ',
                'WHERE ID="' + infProt_chNFe + '"',
                'AND LoteEnv=' + Geral.FF0(Codigo),
                '']);
                if QrNFeCabA1.RecordCount > 0 then
                begin
                  FatID       := QrNFeCabA1FatID.Value;
                  FatNum      := QrNFeCabA1FatNum.Value;
                  Empresa     := QrNFeCabA1Empresa.Value;
                  //
                  Controle := DModG.BuscaProximoCodigoInt(
                    'nfectrl', 'nfecabamsg', '', 0);
                  //
                  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabamsg', False, [
                  'FatID', 'FatNum', 'Empresa', 'Solicit',
                  'Id', 'tpAmb', 'verAplic',
                  'dhRecbto', 'nProt', 'digVal',
                  'cStat', 'xMotivo', '_Ativo_',
                  'dhRecbtoTZD'], [
                  'Controle'], [
                  FatID, FatNum, Empresa, 100(*autoriza��o*),
                  infProt_Id, infProt_tpAmb, infProt_verAplic,
                  infProt_dhRecbto, infProt_nProt, infProt_digVal,
                  infProt_cStat, infProt_xMotivo, 1,
                  infProt_dhRecbtoTZD], [
                  Controle], True);
                end else Geral.MB_Aviso('A Nota Fiscal de chave "' +
                infProt_chNFe +
                '" n�o foi localizada e ficara sem o hist�rico desta consulta!');
              end;
              //
              xmlList.Remove(xmlList.Item[0]);
              //
            end;
          end
        end;
      end;
      //
      DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
      //
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [6]');
  end;
end;

procedure TFmDmkACBrNFeSteps_0400.LerTextoConsultaNFe();
var
  IDCtrl, Controle, FatID, FatNum, Empresa: Integer;
  tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt,
  chNFe, digVal, cJust, xJust, _Stat, _Motivo, tpEvento, dhEvento: String;
  //
  Status, Evento, nCondUso: Integer;
  infProt_dhRecbtoTZD, dhRecbtoTZD, infCanc_dhRecbtoTZD, dhEventoTZD: Double;
  //
  cOrgao, dhRegEvento, xEvento, nSeqEvento, xCorrecao, verEvento, CNPJ, CPF: String;
  //
  infCCe_verAplic, infCCe_xCorrecao, infCCe_dhRegEvento, infCCe_nProt,
  infCCe_CNPJ, infCCe_CPF, infCCe_chNFe, infCCe_dhEvento: String;
  infCCe_cOrgao, infCCe_tpAmb, infCCe_tpEvento, infCCe_nSeqEvento, infCCe_cStat,
  infCCe_nCondUso: Integer;
  infCCe_verEvento, infCCe_dhRegEventoTZD, infCCe_dhEventoTZD: Double;
  //
  _CNPJ, _CPF, _xCorreca: String;
  _dhEvento: TDateTime;
  _dhEventoTZD, _verEvento: Double;
begin
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConNFe.Value, 2, siNegativo);
  //
  if not DefineEmpresa(Empresa) then Exit;
  if not DefinechNFe(chNFe) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  //
  if DefineXMLDoc() then
  begin
    if DefineIDCtrl(IDCtrl) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA2, Dmod.MyDB, [
      'SELECT * ',
      'FROM nfecaba ',
      'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
      '']);
      if QrNFeCabA2.RecordCount > 0 then
      begin
        // Verifica se � recibo de consulta de NFe
        xmlNode := xmlDoc.SelectSingleNode('/retConsSitNFe');
        if not assigned(xmlNode) then
        begin
          Geral.MB_Aviso(
          'Arquivo XML n�o possui informa��es de Autoriza��o ou Cancelamento de NF-e!');
        end else
        begin
          tpAmb    := '';
          verAplic := '';
          cStat    := '';
          xMotivo  := '';
          cUF      := '';
          chNFe    := '';
          dhRecbto := '';
          nProt    := '';
          digVal   := '';
          cJust    := '';
          xJust    := '';
          //
          PageControl1.ActivePageIndex := 4;
          //
          tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
          cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
          xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
          cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
          chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
          //
          if (Geral.IMV(cStat) in ([100, 101, 110])) then
          begin
            if (chNFe <> QrNFeCabA2Id.Value) then
            begin
              Geral.MB_Erro('Chave da NF-e n�o confere: ' + sLineBreak +
              'No XML: ' + chNFe + sLineBreak +
              'No BD: ' + QrNFeCabA2Id.Value);
              //
              Exit;
            end else
            begin
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                'Status'], ['ID'], [cStat], [chNFe], True);
            end;
            LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
            //
            xmlNode := xmlDoc.SelectSingleNode('/retConsSitNFe/protNFe/infProt');
            //
            if assigned(xmlNode) then
            begin
              //colocar aqui info de 100
              Pagecontrol1.ActivePageIndex := 4;
              Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
              tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
              verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
              _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
              _Motivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
              cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
              //
              if Geral.IMV(_Stat) = 100 then
              begin
                chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
                dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
                //
                XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, infProt_dhRecbtoTZD);
                //
                UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                  'infProt_Id', 'infProt_tpAmb',
                  'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
                  'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
                  'infProt_dhRecbtoTZD'
                ], ['ID'], [
                  Id, tpAmb,
                  verAplic, dhRecbto, nProt,
                  digVal, _Stat, _Motivo,
                  infProt_dhRecbtoTZD
                ], [chNFe], True);
              end;
              //
            end;
            // Cancelamento
            xmlNode := xmlDoc.SelectSingleNode('/retConsSitNFe/retCancNFe/infCanc');
            if assigned(xmlNode) then
            begin
              //colocar aqui info de 101
              Pagecontrol1.ActivePageIndex := 4;
              Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
              tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
              verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
              _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
              _Motivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
              cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
              //
              if Geral.IMV(_Stat) = 101 then
              begin
                chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
                dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
                cJust    := '0' + LeNoXML(xmlNode, tnxTextStr, ttx_cJust);
                xJust    := LeNoXML(xmlNode, tnxTextStr, ttx_xJust);
                //
                XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, infProt_dhRecbtoTZD);
                //
                UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
                  'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
                  'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
                  'infCanc_xJust', 'infCanc_dhRecbtoTZD'
                ], ['ID'], [
                  Id, tpAmb, verAplic,
                  dhRecbto, nProt, digVal,
                  _Stat, _Motivo, cJust,
                  xJust, infProt_dhRecbtoTZD
                ], [chNFe], True);
              end;
            end;

////////////////////////////////////////////////////////////////////////////////
            // E V E N T O S
            xmlList := xmlDoc.SelectNodes('/retConsSitNFe/procEventoNFe');
            LerXML_procEventoNFe(xmlList);
          end else
          begin
            Geral.MB_Erro('C�digo de retorno ' + cStat + ' - ' +
              NFeXMLGeren.Texto_StatusNFe(Geral.IMV(cStat), 0) + sLineBreak +
              'N�o esperado na consulta!');
          end;
        end;
      end else
      begin
        Geral.MB_Aviso('A Nota Fiscal de chave "' + chNFe +
          '" n�o foi localizada e ficar� sem defini��o de Autoriza��o ou Cancelamento DESTA CONSULTA!');
      end;
      //
      if Geral.IMV(cStat) > 0 then
      begin
        if DefineIDCtrl(IDCtrl) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA2, Dmod.MyDB, [
          'SELECT * ',
          'FROM nfecaba ',
          'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
          '']);
            //
          if QrNFeCabA2.RecordCount > 0 then
          begin
            // hist�rico da NF
            FatID   := QrNFeCabA2FatID.Value;
            FatNum  := QrNFeCabA2FatNum.Value;
            Empresa := QrNFeCabA2Empresa.Value;
            dhRecbto    := '0000-00-00 00:00:00';
            dhRecbtoTZD := infProt_dhRecbtoTZD;
            //
            Controle := DModG.BuscaProximoCodigoInt(
              'nfectrl', 'nfecabamsg', '', 0);
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabamsg', False, [
            'FatID', 'FatNum', 'Empresa', 'Solicit',
            'Id', 'tpAmb', 'verAplic',
            'dhRecbto', 'nProt', 'digVal',
            'cStat', 'xMotivo', '_Ativo_',
            'dhRecbtoTZD'], [
            'Controle'], [
            FatID, FatNum, Empresa, 100(*homologa��o*),
            Id, tpAmb, verAplic,
            dhRecbto, nProt, digVal,
            cStat, xMotivo, 1,
            dhRecbtoTZD], [
            Controle], True) then
            begin
              // Mostrar
              //Para evitar erros quando aberto em aba FmNFe_Pesq_0000.PageControl1.ActivePageIndex := 1;
            end;
          end;
        end else
        begin
          Geral.MB_Aviso(
            'O Status retornou zerado e a Nota Fiscal de chave "' + chNFe +
            '" ficar� sem o hist�rico desta consulta!');
        end;
        //
      end else
      begin
        Geral.MB_Aviso(
          'Arquivo XML n�o possui informa��es de Autoriza��o ou Cancelamento de NF-e!');
      end;
    end else
    begin
      Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [7]');
    end;
  end;
end;

function TFmDmkACBrNFeSteps_0400.LerTextoEnvioLoteEvento(): Boolean;
var
  Codigo, Controle, tpAmb, cOrgao, cStat: Integer;
  versao: Double;
  verAplic, xMotivo: String;

  Status, ret_tpAmb, ret_cOrgao, ret_cStat, ret_tpEvento,
  ret_nSeqEvento: Integer;
  ret_versao, ret_TZD_UTC: Double;
  ret_Id, ret_verAplic, ret_xMotivo, ret_chNFe, ret_xEvento,
  ret_CNPJDest, ret_CPFDest, ret_emailDest, ret_nProt: String;

  SubCtrl: Integer;

  eveMDe_tpAmb, eveMDe_cOrgao, eveMDe_cStat,
  eveMDe_tpEvento, eveMDe_nSeqEvento, cSitConf: Integer;
  eveMDe_Id, eveMDe_verAplic, eveMDe_xMotivo,
  eveMDe_chNFe, eveMDe_xEvento, eveMDe_CNPJDest,
  eveMDe_CPFDest, eveMDe_emailDest, eveMDe_nProt, Id, eveMDe_dhRegEvento: String;
  eveMDe_TZD_UTC: Double;

  Empresa, cJust: Integer;
  //tMed, nRec
  idLote: String;
  infEvento_Id, infEvento_tpAmb, infEvento_verAplic, infEvento_cOrgao,
  infEvento_cStat, infEvento_xMotivo, infEvento_chNFe, infEvento_tpEvento,
  infEvento_xEvento, infEvento_CNPJDest, infEvento_CPFDest, infEvento_emailDest,
  infEvento_nSeqEvento, infEvento_dhRegEvento, infEvento_nProt, XML_retEve,
  infEvento_versao, infEvento_xCorrecao: String;

  tpEvento, nSeqEvento: Integer;
  //cSitNFe: Integer;
  xJust: String;
  SQLType: TSQLType;

  CPF: String;
begin
  //FverXML_versao := verEnviEvento_Versao;
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerLotEve.Value, 2, siNegativo);
  Result := False;
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
      versao   := Geral.DMV_Dot(LeNoXML(xmlNode, tnxAttrStr, ttx_versao, False));
      //
      idLote   := LeNoXML(xmlNode, tnxTextStr, ttx_idLote);
      tpAmb    := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb));
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cOrgao   := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao));
      cStat    := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_cStat));
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      //
      ///xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento/retEvento');
      Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverlor', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeeverlor', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cOrgao',
        'cStat', 'xMotivo'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cOrgao,
        cStat, xMotivo
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeeverloe', False, [
          'versao', 'tpAmb', 'verAplic',
          'cOrgao', 'cStat', 'xMotivo'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cOrgao, cStat, xMotivo
        ], [Codigo], True) then
        begin
          xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento');
          //xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento/infEvento');
          if xmlList.Length > 0 then
          begin
            //I := -1;
            while xmlList.Length > 0 do
            begin
              //I := I + 1;
              //testar
              infEvento_versao := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_versao, False);
//{TODO :       Verificar vers�o e conte�do do XML_retEve!
              XML_retEve            := xmlList.Item[0].XML;
              //
              //xmlNode := xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento');
              xmlNode := xmlList.Item[0].FirstChild;
              //xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento', xmlNode);
              //xmlNode := xmlList.Item[0].SelectSingleNode('/retEvento/infEvento');
              infEvento_Id          := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
              //
              infEvento_tpAmb       := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
              infEvento_verAplic    := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
              infEvento_cOrgao      := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
              infEvento_cStat       := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
              infEvento_xMotivo     := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
              infEvento_chNFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
              infEvento_tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
              infEvento_xEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
              infEvento_CNPJDest    := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJDest);
              infEvento_CPFDest     := LeNoXML(xmlNode, tnxTextStr, ttx_CPFDest);
              infEvento_emailDest   := LeNoXML(xmlNode, tnxTextStr, ttx_emailDest);
              infEvento_nSeqEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
              infEvento_dhRegEvento := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
              infEvento_nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
              //
              //
              //infEvento_xCorrecao   := LeNoXML(xmlNode, tnxTextStr, ttx_xCorrecao);
              //
              if infEvento_dhRegEvento = '' then
                infEvento_dhRegEvento := '0000-00-00 00:00:00'
              else
                XXe_PF.Ajusta_dh_XXe_UTC(infEvento_dhRegEvento, ret_TZD_UTC);
              //
              Status     := Geral.IMV(infEvento_cStat);
              tpEvento   := Geral.IMV(infEvento_tpEvento);
              nSeqEvento := Geral.IMV(infEvento_nSeqEvento);
              //N�O LOCALIZA DIREITO
              Controle := DmNFe_0000.EventoObtemCtrl(Codigo, tpEvento,
                nSeqEvento, infEvento_chNFe);
              //
              if Controle <> 0 then
              begin
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeevercab', False, [
                  'XML_retEve',
                  'Status', 'ret_versao', 'ret_Id',
                  'ret_tpAmb', 'ret_verAplic', 'ret_cOrgao',
                  'ret_cStat', 'ret_xMotivo', 'ret_chNFe',
                  'ret_tpEvento', 'ret_xEvento', 'ret_nSeqEvento',
                  'ret_CNPJDest', 'ret_CPFDest', 'ret_emailDest',
                  'ret_dhRegEvento', 'ret_TZD_UTC', 'ret_nProt'], [
                  (*'FatID', 'FatNum', 'Empresa',*) 'Controle'], [
                  Geral.WideStringToSQLString(XML_retEve), Status,
                  (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                  (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                  (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                  (*ret_xMotivo*)infEvento_xMotivo, (*ret_chNFe*)infEvento_chNFe,
                  (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                  (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                  (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                  (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                  (*ret_nProt*)infEvento_nProt], [
                  (*FatID, FatNum, Empresa,*) Controle], True) then
                begin
                  // hist�rico
                  //SubCtrl := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverret', '', 0);
                  DmNFe_0000.EventoObtemSub(Controle, infEvento_Id, SubCtrl, SQLType);

                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfeeverret', False, [
                    'Controle', 'ret_versao', 'ret_Id', 'ret_tpAmb',
                    'ret_verAplic', 'ret_cOrgao', 'ret_cStat',
                    'ret_xMotivo', 'ret_chNFe', 'ret_tpEvento',
                    'ret_xEvento', 'ret_nSeqEvento', 'ret_CNPJDest',
                    'ret_CPFDest', 'ret_emailDest', 'ret_dhRegEvento',
                    'ret_TZD_UTC', 'ret_nProt'], [
                    'SubCtrl'], [
                    Controle, (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                    (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                    (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                    (*ret_xMotivo*)infEvento_xMotivo, (*ret_chNFe*)infEvento_chNFe,
                    (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                    (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                    (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                    (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                    (*ret_nProt*)infEvento_nProt], [
                    SubCtrl], True) then
                  begin
                    case Geral.IMV(infEvento_tpEvento) of
                      NFe_CodEventoCCe: // Carta de corre��o.
                      //A princ�pio n�o faz nada!
                      // Implementado nos aplicativos Dermatek s� na NFe 3.10
                      begin
                        //
                        if infEvento_cStat = '135' then
                        begin
                          if not UnNFe_PF.AtualizaDadosCCeNfeCabA(infEvento_chNFe,
                            verAplic, infEvento_chNFe, infEvento_dhRegEvento,
                            infEvento_nProt, nSeqEvento, cOrgao, tpAmb, tpEvento,
                            cStat, ret_TZD_UTC)
                          then
                            Exit;

(*
// carta de correcao
<descEvento>Carta de Correcao</descEvento>
<xCorrecao>QWF EF EWEW EW WE EWWER</xCorrecao>
<xCondUso>
A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de erro ocorrido na emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique mudanca do remetente ou do destinatario; III - a data de emissao ou de saida.
</xCondUso>
</detEvento>
*)
                        end;
                      end;
                      NFe_CodEventoCan: // Cancelamento
                      begin
                        //
                        if infEvento_cStat = '135' then
                        begin
                          if not UnNFe_PF.AtualizaDadosCanNfeCabA(
                            infEvento_chNFe, infEvento_tpAmb, infEvento_verAplic,
                            infEvento_dhRegEvento, infEvento_nProt, infEvento_versao,
                            ret_TZD_UTC)
                          then
                            Exit;
                        end;
                      end;
                      NFe_CodEventoEPEC: // EPEC - Evento Pr�vio de Emiss�o em Conting�ncia
                      begin
                        //
                        if infEvento_cStat = '136' then
                        begin
                          if not UnNFe_PF.AtualizaDadosEPECNfeCabA(
                          infEvento_chNFe, infEvento_versao, infEvento_verAplic,
                          infEvento_dhRegEvento, infEvento_nProt,
                          infEvento_cOrgao, infEvento_tpAmb,
                          infEvento_cStat, infEvento_xMotivo,
                          //infEvento_dhRegEventoTZD: Double;
                          ret_TZD_UTC)
                          then
                            Exit;
                        end;
                      end;
                      // Manifesta��o do destinat�rio
                      NFe_CodEventoMDeConfirmacao, // = 210200;
                      NFe_CodEventoMDeCiencia    , // = 210210;
                      NFe_CodEventoMDeDesconhece , // = 210220;
                      NFe_CodEventoMDeNaoRealizou: // = 210240;
                      begin
                        if infEvento_cStat = '135' then
                        begin
                          //  Realmente precisa localizar?
                          if not DmNFe_0000.LocalizaNFeInfoMDeEve(
                            infEvento_chNFe, Id, xJust, cJust, tpEvento)

                          then
                            Geral.MB_Erro('Falha ao localizar dados da manifesta��o');
                          //
                          Id                 := infEvento_chNFe;
                          //
                          eveMDe_Id          := infEvento_Id;
                          eveMDe_tpAmb       := Geral.IMV(infEvento_tpAmb);
                          eveMDe_verAplic    := infEvento_verAplic;
                          eveMDe_cOrgao      := Geral.IMV(infEvento_cOrgao);
                          eveMDe_cStat       := Geral.IMV(infEvento_cStat);
                          eveMDe_xMotivo     := infEvento_xMotivo;
                          eveMDe_chNFe       := infEvento_chNFe;
                          eveMDe_tpEvento    := Geral.IMV(infEvento_tpEvento);
                          eveMDe_xEvento     := infEvento_xEvento;
                          eveMDe_nSeqEvento  := Geral.IMV(infEvento_nSeqEvento);
                          eveMDe_CNPJDest    := infEvento_CNPJDest;
                          eveMDe_CPFDest     := infEvento_CPFDest;
                          eveMDe_emailDest   := infEvento_emailDest;
                          eveMDe_dhRegEvento := infEvento_dhRegEvento;
                          eveMDe_TZD_UTC     := ret_TZD_UTC;
                          eveMDe_nProt       := infEvento_nProt;
                          //
                          //cSitNFe  :=  N�o tem aqui!
                          cSitConf := NFeXMLGeren.Obtem_DeManifestacao_de_tpEvento_cSitConf(eveMDe_tpEvento);
                          //
                          if eveMDe_dhRegEvento = '' then
                            eveMDe_dhRegEvento := '0000-00-00 00:00:00';
                          //
                          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                            'eveMDe_Id', 'eveMDe_tpAmb', 'eveMDe_verAplic',
                            'eveMDe_cOrgao', 'eveMDe_cStat', 'eveMDe_xMotivo',
                            'eveMDe_chNFe', 'eveMDe_tpEvento', 'eveMDe_xEvento',
                            'eveMDe_nSeqEvento', 'eveMDe_CNPJDest', 'eveMDe_CPFDest',
                            'eveMDe_emailDest', 'eveMDe_dhRegEvento', 'eveMDe_TZD_UTC',
                            'eveMDe_nProt', 'cSitConf'
                          ], ['Id'], [
                            eveMDe_Id, eveMDe_tpAmb, eveMDe_verAplic,
                            eveMDe_cOrgao, eveMDe_cStat, eveMDe_xMotivo,
                            eveMDe_chNFe, eveMDe_tpEvento, eveMDe_xEvento,
                            eveMDe_nSeqEvento, eveMDe_CNPJDest, eveMDe_CPFDest,
                            eveMDe_emailDest, eveMDe_dhRegEvento, eveMDe_TZD_UTC,
                            eveMDe_nProt, cSitConf
                          ], [Id], True);
                        end;
                      end;
                      else
                      begin
                        Geral.MB_Aviso('Tipo de evento n�o implementado na fun��o:' +
                        sLineBreak + 'TFmNFeSteps_0400.LerTextoEnvioLoteEvento()');
                        Result := False;
                        Exit;
                      end;
                    end;
                  end;
                end;
              end;
              //
              xmlList.Remove(xmlList.Item[0]);
              //
            end;
          end
        end;
      end;
      // N�o precisa! J� Faz direto nas SQL acima!
      //DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
      //
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [2]');
  end;
end;

function TFmDmkACBrNFeSteps_0400.LerTextoEnvioLoteNFe(): Boolean;
const
  sProcName = 'TFmNFeSteps_0400.LerTextoEnvioLoteNFe()';
  //
  function CorrigeDataStringVazio(Data: String): String;
  begin
    if Data = '' then
      Result := '0000-00-00'
    else
      Result := Data;
  end;
var
  Status, Codigo, Controle, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, nRec, dhRecbto, tMed: String;
  //
  FatID, FatNum: Integer;
  infProt_Id, infProt_chNFe, infProt_nProt, infProt_digVal, infProt_tpAmb,
  infProt_verAplic, infProt_dhRecbto, infProt_cStat, infProt_xMotivo: String;
  infProt_dhRecbtoTZD, dhRecbtoTZD: Double;
  Sincronia: TXXeIndSinc;
  Dir, Aviso: String;
  IDCtrl: Integer;
begin
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerEnvLot.Value, 2, siNegativo);
  Result := False;
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    xmlNode := xmlDoc.SelectSingleNode('/retEnviNFe');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
(*
- <retEnviNFe versao="3.10" xmlns="http://www.portalfiscal.inf.br/nfe">
  <tpAmb>2</tpAmb>
  <verAplic>PR-v3_2_1</verAplic>
  <cStat>104</cStat>
  <xMotivo>Lote processado</xMotivo>
  <cUF>41</cUF>
  <dhRecbto>2014-10-22T00:53:41-02:00</dhRecbto>
*)
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      //
(*==============================================================================
      NFe 3.10  - NFe Sincrona
================================================================================
- <protNFe versao="3.10">
- <infProt Id="ID141140001620054">
  <tpAmb>2</tpAmb>
  <verAplic>PR-v3_2_1</verAplic>
  <chNFe>41141002717861000110550010000045811360413650</chNFe>
  <dhRecbto>2014-10-22T00:53:41-02:00</dhRecbto>
  <nProt>141140001620054</nProt>
  <digVal>gzIavxRcIKOO6WbOu2M+HwEH0Cg=</digVal>
  <cStat>100</cStat>
  <xMotivo>Autorizado o uso da NF-e</xMotivo>
  </infProt>
  </protNFe>
  </retEnviNFe>
*)
      xmlNode := xmlDoc.SelectSingleNode('/retEnviNFe/protNFe');
      if assigned(xmlNode) then
      begin
        Sincronia := nisSincrono;
        xmlNode := xmlDoc.SelectSingleNode('/retEnviNFe/protNFe/infProt');
        if assigned(xmlNode) then
        begin
          infProt_Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
          infProt_tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          infProt_verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
          infProt_chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
          infProt_dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
          infProt_nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          infProt_digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
          infProt_cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
          infProt_xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
          //
          XXe_PF.Ajusta_dh_XXe_UTC(infProt_dhRecbto, infProt_dhRecbtoTZD);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
            'Status', 'infProt_Id', 'infProt_tpAmb',
            'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
            'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
            'protNFe_versao', 'infProt_dhRecbtoTZD'
          ], ['ID', 'LoteEnv'], [
            infProt_cStat, infProt_Id, infProt_tpAmb,
            infProt_verAplic, infProt_dhRecbto, infProt_nProt,
            infProt_digVal, infProt_cStat, infProt_xMotivo,
            versao, infProt_dhRecbtoTZD
          ], [infProt_chNFe, Codigo], True) then
          begin
            // hist�rico da NF
(*
            QrNFeCabA1.Close;
            QrNFeCabA1.Database := Dmod.MyDB;
            QrNFeCabA1.Params[00].AsString  := infProt_chNFe;
            QrNFeCabA1.Params[01].AsInteger := Codigo;
            QrNFeCabA1.O p e n;
*)
            UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA1, Dmod.MyDB, [
            'SELECT * ',
            'FROM nfecaba ',
            'WHERE ID="' + infProt_chNFe + '"',
            'AND LoteEnv=' + Geral.FF0(Codigo),
            '']);
            if QrNFeCabA1.RecordCount > 0 then
            begin
              FatID   := QrNFeCabA1FatID.Value;
              FatNum  := QrNFeCabA1FatNum.Value;
              Empresa := QrNFeCabA1Empresa.Value;
              //
              Controle := DModG.BuscaProximoCodigoInt(
                'nfectrl', 'nfecabamsg', '', 0);
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabamsg', False, [
              'FatID', 'FatNum', 'Empresa', 'Solicit',
              'Id', 'tpAmb', 'verAplic',
              'dhRecbto', 'nProt', 'digVal',
              'cStat', 'xMotivo', 'dhRecbtoTZD',
              '_Ativo_'], [
              'Controle'], [
              FatID, FatNum, Empresa, 100(*autoriza��o*),
              infProt_Id, infProt_tpAmb, infProt_verAplic,
              infProt_dhRecbto, infProt_nProt, infProt_digVal,
              infProt_cStat, infProt_xMotivo, infProt_dhRecbtoTZD,
              1], [
              Controle], True);
            end else Geral.MB_Aviso('A Nota Fiscal de chave "' +
            infProt_chNFe +
            '" n�o foi localizada e ficara sem o hist�rico desta consulta!');
            //
            Dir := DModG.QrPrmsEmpNFeDirRec.Value;
            Aviso := '';
            IDCtrl := QrNFeCabA1IDCtrl.Value;
            //DmNFE_0000.AtualizaXML_No_BD_Aut_Sinc(IDCtrl, Codigo, infProt_Id, Dir, Aviso);
            DmNFE_0000.AtualizaXML_No_BD_Aut_Sinc(IDCtrl, Codigo, infProt_chNFe, Dir, Aviso);
            //
            //DmNFE_0000.AtualizaXML_No_BD_NFe(QrNFeCabA1IDCtrl.Value, Dir, Aviso);
            //DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
            //
           if Aviso <> '' then Geral.MB_Aviso(
            'Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);
          end;
          // NFeLEnM
          // Conforme Manual NFe 3.10:
          // C. Processamento S�ncrono
          //No caso de processamento s�ncrono do Lote de NF-e, as valida��es da
          //NF-e ser�o feitas na sequ�ncia, sem a gera��o de um N�mero de Recibo.
          nRec      := 'NFe s�ncrona';
          dhRecbto := infProt_dhRecbto;
          tMed      := '0';
          //
        end else Geral.MB_Erro('Protocolo de NFe sem inform��es!');
      end else
      begin
        Sincronia := nisAssincrono;
(*==============================================================================
      FIM  NFe 3.10  - NFe Sincrona
==============================================================================*)
        xmlNode := xmlDoc.SelectSingleNode('/retEnviNFe/infRec');
        if assigned(xmlNode) then
        begin
          nRec      := LeNoXML(xmlNode, tnxTextStr, ttx_nRec);
          dhRecbto  := CorrigeDataStringVazio(LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto));
          tMed      := LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
        end else begin
          nRec      := '';
          dhRecbto  := CorrigeDataStringVazio('');
          tMed      := '0';
        end;
      end;
      //
      XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
      //
      Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenm', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelenm', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cStat', 'xMotivo',
        'cUF', 'nRec', 'dhRecbto', 'tMed',
        'Status', 'dhRecbtoTZD'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cStat, xMotivo,
        cUF, nRec, dhRecbto, tMed,
        DmNFe_0000.stepLoteEnvEnviado(), dhRecbtoTZD
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfelenc', False, [
          'versao', 'tpAmb', 'verAplic',
          'cStat', 'xMotivo', 'cUF',
          'nRec', 'dhRecbto', 'tMed'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cStat, xMotivo, cUF,
          nRec, dhRecbto, tMed
        ], [Codigo], True) then
        begin
          // CUIDADO!!!!  Somente se for lote Assincrono!!
          // Se fizer no sincrono a NFe mesmo autorizada fimca Status = 40 !!!
          if Sincronia = nisAssincrono then
          begin
            Status := Geral.IMV(cStat);
            //
            if Status <> 103 then
              Status := DmNFe_0000.stepLoteRejeitado;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
              'Status'], ['LoteEnv', 'Empresa'], [
              Status], [Codigo, Empresa], True);
          end;
        end;
      end;
      try
        if FFormChamou = 'FmNFeLEnc_0400' then
          FmNFeLEnc_0400.LocCod(Codigo, Codigo)
        else
        if FFormChamou = 'FmNFeLEnU_0400' then
          FmNFeLEnU_0400.ReabreNFeLEnc(Codigo)
        else
        if FFormChamou = 'FmNFeLEnU_0400' then
        {$IfNDef semNFCe_0000}
          FmNFCeLEnU_0400.ReabreNFeLEnc(Codigo);
        {$Else}
          Geral.MB_Info('NFC-e n�o habilitado para este ERP');
        {$EndIf}
      except
        Geral.MB_Erro('Form que chamou "' + FFormChamou +
        '" difere do esperado!' + sLineBreak + sProcName)
      end;
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado!');
  end;
  Result := True;
end;

procedure TFmDmkACBrNFeSteps_0400.LerTextoInutilizaNumerosNF();
var
  Controle, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt, xJust, Ano,
  CNPJ, Modelo, Serie, nNFIni, nNFFim: String;
  //
  Lote: Integer;
  dhRecbtoTZD: Double;
begin
  //FverXML_versao := verNFeInutNFe_Versao;
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerInuNum.Value, 2, siNegativo);
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de inutiliza��o de n�meros de NF
    xmlNode := xmlDoc.SelectSingleNode('/retInutNFe');
    if assigned(xmlNode) then
    begin
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := '';
      verAplic := '';
      cStat    := '';
      xMotivo  := '';
      cUF      := '';
      xmlNode := xmlDoc.SelectSingleNode('/retInutNFe/infInut');
      if assigned(xmlNode) then
      begin
        Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
        tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
        verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
        cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
        xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
        cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
        if not DefineLote(Lote) then
        begin
          FmNFeGeraXML_0400.DesmontaID_Inutilizacao(
            Id, cUF, Ano, CNPJ, Modelo, Serie, nNFIni, nNFFim);
          DmNFe_0000.QrNFeInut.Close;
          DmNFe_0000.QrNFeInut.Database := Dmod.MyDB; // 2020-08-31
          DmNFe_0000.QrNFeInut.Params[00].AsInteger := Empresa;
          DmNFe_0000.QrNFeInut.Params[01].AsString := cUF;
          DmNFe_0000.QrNFeInut.Params[02].AsString := ano;
          DmNFe_0000.QrNFeInut.Params[03].AsString := CNPJ;
          DmNFe_0000.QrNFeInut.Params[04].AsString := modelo;
          DmNFe_0000.QrNFeInut.Params[05].AsString := Serie;
          DmNFe_0000.QrNFeInut.Params[06].AsString := nNFIni;
          DmNFe_0000.QrNFeInut.Params[07].AsString := nNFFim;
          UnDmkDAC_PF.AbreQuery(DmNFe_0000.QrNFeInut, Dmod.MyDB); // (*2022-02-20 - Antigo . O p e n ; *)
          Lote := DmNFe_0000.QrNFeInutCodigo.Value;
          if Lote = 0 then
          begin
            Geral.MB_Erro('N�o foi poss�vel descobrir o Lote pelo ID!');
            //
            Exit;
          end else
            Geral.MB_Aviso('O Lote foi encontrado pelo ID!');
        end;
        if cStat = '102' then
        begin
          ano      := LeNoXML(xmlNode, tnxTextStr, ttx_ano);
          CNPJ     := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
          modelo   := LeNoXML(xmlNode, tnxTextStr, ttx_mod);
          serie    := LeNoXML(xmlNode, tnxTextStr, ttx_serie);
          nNFIni   := LeNoXML(xmlNode, tnxTextStr, ttx_nNFIni);
          nNFFim   := LeNoXML(xmlNode, tnxTextStr, ttx_nNFFin);
          dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
          nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          //
          XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeinut', False, [
          'versao', 'Id', 'tpAmb',
          'cUF', 'ano', 'CNPJ',
          //ver aqui o que fazer
          'modelo', 'Serie',
          'nNFIni', 'nNFFim', 'xJust',
          'cStat', 'xMotivo', 'dhRecbto',
          'nProt', 'dhRecbtoTZD'], ['Codigo'], [
          versao, Id, tpAmb,
          cUF, ano, CNPJ,
          modelo, Serie,
          nNFIni, nNFFim, xJust,
          cStat, xMotivo, dhRecbto,
          nProt, dhRecbtoTZD], [Lote], True);
        end;
        Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeinutmsg', '', 0);
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeinutmsg', False, [
        'Codigo', 'versao', 'Id',
        'tpAmb', 'verAplic', 'cStat',
        'xMotivo', 'cUF', '_Ativo_'], [
        'Controle'], [
        Lote, versao, Id,
        tpAmb, verAplic, cStat,
        xMotivo, cUF, 1], [
        Controle], True);
      end else Geral.MB_Aviso(
      'Arquivo XML n�o possui informa��es de Inutiliza��o de numera��o de NF-e!');
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [3]');
    //
    DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
    //
  end;
  //FmNFeInut_0000.LocCod(Lote, Lote);
  //Close;
end;

procedure TFmDmkACBrNFeSteps_0400.LerXML_procEventoNFe(Lista: IXMLNodeList);
var
  IDCtrl, Controle, FatID, FatNum, Empresa: Integer;
  tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt,
  chNFe, digVal, xJust, _Stat, _Motivo, tpEvento, dhEvento: String;
  //
  Status, Evento, nCondUso: Integer;
  infProt_dhRecbtoTZD, dhRecbtoTZD, infCanc_dhRecbtoTZD, dhEventoTZD: Double;
  //
  cOrgao, dhRegEvento, xEvento, nSeqEvento, xCorrecao, verEvento, CNPJ, CPF: String;
  //
  infCCe_verAplic, infCCe_xCorrecao, infCCe_dhRegEvento, infCCe_nProt,
  infCCe_CNPJ, infCCe_CPF, infCCe_chNFe, infCCe_dhEvento, infEvento_versao: String;
  infCCe_cOrgao, infCCe_tpAmb, infCCe_tpEvento, infCCe_nSeqEvento, infCCe_cStat,
  infCCe_nCondUso, cJust, infCanc_cStat: Integer;
  infCCe_verEvento, infCCe_dhRegEventoTZD, infCCe_dhEventoTZD: Double;
  //
  _CNPJ, _CPF, _xCorreca: String;
  _dhEvento: TDateTime;
  _dhEventoTZD, _verEvento: Double;
begin
  if Lista.Length > 0 then
  begin
    while Lista.Length > 0 do
    begin
      cOrgao     := '';
      tpAmb      := '';
      CNPJ       := '';
      chNFe      := '';
      dhEvento   := '';
      tpEvento   := '';
      nSeqEvento := '';
      verEvento  := '';
      xCorrecao  := '';  // Carta de correcao
      nProt      := '';  // Cancelamento
      xJust      := '';  // Cancelamento
      //
      //xmlSub  := Lista.Item[0].FirstChild;
      xmlSub  := nil;
      xmlSub  := Lista.Item[0];
      // Dados do envio ao Fisco...
      // ... dados gerais
      // Erro Aqui! Pega sempre o primeiro!
      //xmlNode := xmlSub.SelectSingleNode('/retConsSitNFe/procEventoNFe/evento/infEvento');
      // Nao pega nada!
      //xmlNode := xmlSub.SelectSingleNode('procEventoNFe/infEvento');
      xmlNode := xmlSub.SelectSingleNode('evento/infEvento');
      if assigned(xmlNode) then
      begin
        infEvento_versao := LeNoXML(xmlNode, tnxAttrStr, ttx_versao, False);
        cOrgao           := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
        tpAmb            := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
        CNPJ             := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
        CPF              := LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
        chNFe            := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
        dhEvento         := LeNoXML(xmlNode, tnxTextStr, ttx_dhEvento);
        tpEvento         := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
        nSeqEvento       := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
        verEvento        := LeNoXML(xmlNode, tnxTextStr, ttx_verEvento);
        //
        XXe_PF.Ajusta_dh_XXe_UTC(dhEvento, dhEventoTZD);
        //
      end;
(*
//Cartade correcao
<cOrgao>41</cOrgao>
<tpAmb>2</tpAmb>
<CNPJ>02717861000110</CNPJ>
<chNFe>41141002717861000110550010000046201568000375</chNFe>
<dhEvento>2014-10-24T19:04:22-02:00</dhEvento>
<tpEvento>110110</tpEvento>
<nSeqEvento>1</nSeqEvento>
<verEvento>1.00</verEvento>
<detEvento versao="1.00">
//
// Cancelamento
<cOrgao>41</cOrgao>
<tpAmb>2</tpAmb>
<CNPJ>02717861000110</CNPJ>
<chNFe>41141002717861000110550010000046201568000375</chNFe>
<dhEvento>2014-10-25T14:45:22-02:00</dhEvento>
<tpEvento>110111</tpEvento>
<nSeqEvento>1</nSeqEvento>
<verEvento>1.00</verEvento>
<detEvento versao="1.00">
*)
      // ... dados especificos de cada evento
      //xmlNode := xmlSub.SelectSingleNode('/retConsSitNFe/procEventoNFe/evento/infEvento/detEvento');
      xmlChild1 := xmlNode.SelectSingleNode('detEvento');

(*
// carta de correcao
<descEvento>Carta de Correcao</descEvento>
<xCorrecao>QWF EF EWEW EW WE EWWER</xCorrecao>
<xCondUso>
A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de erro ocorrido na emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique mudanca do remetente ou do destinatario; III - a data de emissao ou de saida.
</xCondUso>
</detEvento>
*)
      if assigned(xmlChild1) then
      begin
        xCorrecao := LeNoXML(xmlChild1, tnxTextStr, ttx_xCorrecao);
        //xCondUso := LeNoXML(xmlNode, tnxTextStr, ttx_xCorrecao);
// Fim Carta de correcao

(*
<descEvento>Cancelamento</descEvento>
<nProt>141140001628294</nProt>
<xJust>POR ESTAR EM DESACORDO COM O PEDIDO</xJust>
</detEvento>
</infEvento>
*)
        nProt     := LeNoXML(xmlChild1, tnxTextStr, ttx_nProt);
        xJust     := LeNoXML(xmlChild1, tnxTextStr, ttx_xJust);
// Fim cancelamento
      //
      //
      end;
      // Dados do retorno da consulta no Fisco
      // erro aqui
      //xmlNode := xmlSub.SelectSingleNode('/retConsSitNFe/procEventoNFe/retEvento/infEvento');
      xmlNode := xmlSub.SelectSingleNode('retEvento/infEvento');
      if assigned(xmlNode) then
      begin
        tpEvento := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
        _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
        Evento := Geral.IMV(tpEvento);
        if (Geral.IMV(_Stat) in ([135, 136])) then
        begin
          tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
          cOrgao    := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
          xMotivo    := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
          chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
          tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
          xEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
          nSeqEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
          dhRegEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
          nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          //
          //
          case Evento of
            NFe_CodEventoCCe: // = 110110;
            begin
              XXe_PF.Ajusta_dh_XXe_UTC(dhRegEvento, infCCe_dhRegEventoTZD);
              //
              if not UnNFe_PF.AtualizaDadosCCeNfeCabA(chNFe, verAplic, chNFe,
                dhRegEvento, nProt, Geral.IMV(nSeqEvento), Geral.IMV(cOrgao),
                Geral.IMV(tpAmb), Geral.IMV(tpEvento), Geral.IMV(_Stat),
                infCCe_dhRegEventoTZD)
              then
                Exit;
            end;
            NFe_CodEventoCan: // = 110111;
            begin
              XXe_PF.Ajusta_dh_XXe_UTC(dhRegEvento, infCanc_dhRecbtoTZD);
              //
              if not UnNFe_PF.AtualizaDadosCanNfeCabA(chNFe, tpAmb, verAplic,
                dhRegEvento, nProt, infEvento_versao, infCanc_dhRecbtoTZD)
              then
               Exit;
            end;
            else Geral.MB_Erro('Evento: ' + tpEvento +
            ' n�o implementado em "TFmNFeSteps_0400.LerTextoConsultaNFe()"');
          end;
        end else
          Geral.MB_Erro('C�digo de retorno de evento ' + _Stat + ' - ' +
          NFeXMLGeren.Texto_StatusNFe(Geral.IMV(_Stat), 0) + sLineBreak +
          'N�o esperado para c�digo de retorno de consulta ' + cStat +
          NFeXMLGeren.Texto_StatusNFe(Geral.IMV(cStat), 0));
      end;
      Lista.Remove(Lista.Item[0]);
    end;
  end;
end;

procedure TFmDmkACBrNFeSteps_0400.MostraTextoRetorno(Texto: String);
begin
  RETxtRetorno.Text := Texto;
end;

function TFmDmkACBrNFeSteps_0400.ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'ID'                    ;
    ttx_idLote      : Result := 'ID do lote'            ;
    ttx_versao      : Result := 'Vers�o XML'            ;
    ttx_tpAmb       : Result := 'Ambiente'              ;
    ttx_verAplic    : Result := 'Vers�o do aplicativo'  ;
    ttx_cOrgao      : Result := 'Org�o'                 ;
    ttx_cStat       : Result := 'Status'                ;
    ttx_xMotivo     : Result := 'Motivo'                ;
    ttx_UF          : Result := 'UF'                    ;
    ttx_cUF         : Result := 'UF'                    ;
    ttx_dhRecbto    : Result := 'Data/hora recibo'      ;
    ttx_chNFe       : Result := 'Chave NF-e'            ;
    ttx_nProt       : Result := 'Protocolo'             ;
    ttx_digVal      : Result := 'Valor "digest"'        ;
    ttx_ano         : Result := 'Ano'                   ;
    ttx_CNPJ        : Result := 'CNPJ'                  ;
    ttx_mod         : Result := 'Modelo NF'             ;
    ttx_serie       : Result := 'S�rie NF'              ;
    ttx_nNFIni      : Result := 'N�mero inicial NF'     ;
    ttx_nNFFin      : Result := 'N�mero final NF'       ;
    ttx_nRec        : Result := 'N�mero do Recibo'      ;
    ttx_tMed        : Result := 'Tempo m�dio'           ;
    ttx_tpEvento    : Result := 'Tipo de evento'        ;
    ttx_xEvento     : Result := 'Descri��o do evento'   ;
    ttx_CNPJDest    : Result := 'CNPJ do destinat�rio'  ;
    ttx_CPFDest     : Result := 'CPF do destinat�rio'   ;
    ttx_emailDest   : Result := 'E-mail do destinat�rio';
    ttx_nSeqEvento  : Result := 'Sequencial do evento'  ;
    ttx_dhRegEvento : Result := 'Data/hora registro'    ;
    ttx_dhResp      : Result := 'Data/hora msg resposta';
    ttx_indCont     : Result := 'Indicador continua��o' ;
    ttx_ultNSU      : Result := '�ltima NSU pesquisada' ;
    ttx_maxNSU      : Result := 'NSU M�xima encontrada' ;
    ttx_NSU         : Result := 'NSU do docum. fiscal'  ;
    ttx_CPF         : Result := 'CPF'                   ;
    ttx_xNome       : Result := 'Raz�o Social ou Nome'  ;
    ttx_IE          : Result := 'Inscri��o Estadual'    ;
    ttx_dEmi        : Result := 'Data da emiss�o'       ;
    ttx_tpNF        : Result := 'Tipo de opera��o NF-e' ;
    ttx_vNF         : Result := 'Valor total da NF-e'   ;
    ttx_cSitNFe     : Result := 'Situa��o da NF-e'      ;
    ttx_cSitConf    : Result := 'Sit. Manifes. Destinat';
    ttx_dhEvento    : Result := 'Data/hora do evento'   ;
    ttx_descEvento  : Result := 'Evento'                ;
    ttx_xCorrecao   : Result := 'xCorrecao'             ;
    ttx_cJust       : Result := 'C�digo da justifica��o';
    ttx_xJust       : Result := 'Texto da justifica��o' ;
    ttx_verEvento   : Result := 'Vers�o do evento'      ;
    ttx_schema      : Result := 'Schema XML'            ;
    ttx_docZip      : Result := 'Resumo do documento'   ;
    ttx_dhEmi       : Result := 'Data / hora de emiss�o';
    ttx_dhCons      : Result := 'Data / hora consulta  ';
    ttx_cSit        : Result := 'Situa��o do contrib.'  ;
    ttx_indCredNFe  : Result := 'Indic. pode emitir NFe';
    ttx_indCredCTe  : Result := 'Indic. pode emitir CTe';
    ttx_xRegApur    : Result := 'Regime de Apur ICMS'   ;
    ttx_CNAE        : Result := 'CNAE'                  ;
    ttx_dIniAtiv    : Result := 'Data in�cio atividades';
    ttx_dUltSit     : Result := '�ltima alter. cadastro';
    ttx_IEUnica     : Result := 'I.E. �nica'            ;
    ttx_IEAtual     : Result := 'I.E. Atual'            ;
    ttx_xLgr        : Result := 'Logradouro'            ;
    ttx_nro         : Result := 'N�mero'                ;
    ttx_xCpl        : Result := 'Complemento'           ;
    ttx_xBairro     : Result := 'Bairro'                ;
    ttx_cMun        : Result := 'C�digo do munic�pio'   ;
    ttx_xMun        : Result := 'Nome do munic�pio'     ;
    ttx_CEP         : Result := 'CEP'                   ;
    //
    else              Result := '? ? ? ? ?';
  end;
end;

function TFmDmkACBrNFeSteps_0400.ObtemNomeDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'Id'                ;
    ttx_idLote      : Result := 'idLote'            ;
    ttx_versao      : Result := 'versao'            ;
    ttx_tpAmb       : Result := 'tpAmb'             ;
    ttx_verAplic    : Result := 'verAplic'          ;
    ttx_cOrgao      : Result := 'cOrgao'            ;
    ttx_cStat       : Result := 'cStat'             ;
    ttx_xMotivo     : Result := 'xMotivo'           ;
    ttx_UF          : Result := 'UF'               ;
    ttx_cUF         : Result := 'cUF'               ;
    ttx_dhRecbto    : Result := 'dhRecbto'          ;
    ttx_chNFe       : Result := 'chNFe'             ;
    ttx_nProt       : Result := 'nProt'             ;
    ttx_digVal      : Result := 'digVal'            ;
    ttx_ano         : Result := 'ano'               ;
    ttx_CNPJ        : Result := 'CNPJ'              ;
    ttx_mod         : Result := 'mod'               ;
    ttx_serie       : Result := 'serie'             ;
    ttx_nNFIni      : Result := 'nNFIni'            ;
    ttx_nNFFin      : Result := 'nNFFin'            ;
    ttx_nRec        : Result := 'nRec'              ;
    ttx_tMed        : Result := 'tMed'              ;
    ttx_tpEvento    : Result := 'tpEvento'          ;
    ttx_xEvento     : Result := 'xEvento'           ;
    ttx_CNPJDest    : Result := 'CNPJDest'          ;
    ttx_CPFDest     : Result := 'CPFDest'           ;
    ttx_emailDest   : Result := 'emailDest'         ;
    ttx_nSeqEvento  : Result := 'nSeqEvento'        ;
    ttx_dhRegEvento : Result := 'dhRegEvento'       ;
    ttx_dhResp      : Result := 'dhResp'            ;
    ttx_indCont     : Result := 'indCont'           ;
    ttx_ultNSU      : Result := 'ultNSU'            ;
    ttx_maxNSU      : Result := 'maxNSU'            ;
    ttx_NSU         : Result := 'NSU'               ;
    ttx_CPF         : Result := 'CPF'               ;
    ttx_xNome       : Result := 'xNome'             ;
    ttx_IE          : Result := 'IE'                ;
    ttx_dEmi        : Result := 'dEmi'              ;
    ttx_tpNF        : Result := 'tpNF'              ;
    ttx_vNF         : Result := 'vNF'               ;
    ttx_cSitNFe     : Result := 'cSitNFe'           ;
    ttx_cSitConf    : Result := 'cSitConf'          ;
    ttx_dhEvento    : Result := 'dhEvento'          ;
    ttx_descEvento  : Result := 'descEvento'        ;
    ttx_xCorrecao   : Result := 'xCorrecao'         ;
    ttx_cJust       : Result := 'cJust'             ;
    ttx_xJust       : Result := 'xJust'             ;
    ttx_verEvento   : Result := 'verEvento'         ;
    ttx_schema      : Result := 'schema'            ;
    ttx_docZip      : Result := 'docZip'            ;
    ttx_dhEmi       : Result := 'dhEmi'             ;
    ttx_dhCons      : Result := 'dhCons'            ;
    ttx_cSit        : Result := 'cSit'              ;
    ttx_indCredNFe  : Result := 'indCredNFe'        ;
    ttx_indCredCTe  : Result := 'indCredCTe'        ;
    ttx_xRegApur    : Result := 'xRegApur'          ;
    ttx_CNAE        : Result := 'CNAE'              ;
    ttx_dIniAtiv    : Result := 'dIniAtiv'          ;
    ttx_dUltSit     : Result := 'dUltSit'           ;
    ttx_IEUnica     : Result := 'IEUnica'           ;
    ttx_IEAtual     : Result := 'IEAtual'           ;
    ttx_xLgr        : Result := 'xLgr'              ;
    ttx_nro         : Result := 'nro'               ;
    ttx_xCpl        : Result := 'xCpl'              ;
    ttx_xBairro     : Result := 'xBairro'           ;
    ttx_cMun        : Result := 'cMun'              ;
    ttx_xMun        : Result := 'xMun'              ;
    ttx_CEP         : Result := 'CEP'               ;
    //
    else           Result :=      '???';
  end;
end;

procedure TFmDmkACBrNFeSteps_0400.PreparaConsultaCadastro(Empresa: Integer);
begin
  FCCC_RetrornoUtil := False;
  //
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmDmkACBrNFeSteps_0400.PreparaConsultaLote(Lote, Empresa: Integer;
  Recibo: String);
var
  LoteStr: String;
begin
  EdLote.ValueVariant      := Lote;
  EdEmpresa.ValueVariant   := Empresa;
  EdRecibo.ValueVariant    := Recibo;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  if CkSoLer.Checked then
  begin
    LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
    if AbreArquivoXML(LoteStr, NFE_EXT_PRO_REC_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmDmkACBrNFeSteps_0400.PreparaConsultaNFe(Empresa, IDCtrl: Integer;
  ChaveNFe: String);
begin
  EdEmpresa.ValueVariant := Empresa;
  EdchNFe.Text           := ChaveNFe;
  EdIDCtrl.ValueVariant  := IDCtrl;
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

function TFmDmkACBrNFeSteps_0400.PreparaEnvioDeLoteEvento(
  var UF_Servico: String; Lote, Empresa: Integer): Boolean;
var
  LoteStr: String;
  Continua: Boolean;
begin
  FPathLoteEvento := '';
  EdLote.ValueVariant    := Lote;
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  // 2022-02-15 porque depois????
  EdUF_Servico.Text := UF_Servico;
  //
  if not CkSoLer.Checked then
  begin
    Continua := FmNFeGeraXML_0400.GerarLoteEvento(Lote, Empresa, FPathLoteEvento, FXML_LoteEvento, LaAviso1, LaAviso2);
    if Continua then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Env_Sel);
    end else
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Envio cancelado!');
  end else
  begin
    LoteStr := FormatFloat('000000000', Lote);
    //
    if AbreArquivoXML(LoteStr, NFE_EXT_EVE_RET_LOT_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end;
  HabilitaBotoes();
  Result := True;
end;

function TFmDmkACBrNFeSteps_0400.PreparaEnvioDeLoteNFe(Lote, Empresa: Integer;
  Sincronia: TXXeIndSinc; Modelo: Integer): Boolean;
var
  LoteStr: String;
  Continua: Boolean;
begin
  FPathLoteNFe := '';
  EdLote.ValueVariant    := Lote;
  EdEmpresa.ValueVariant := Empresa;
  EdIde_mod.ValueVariant := Modelo;
  //
  VerificaCertificadoDigital(Empresa);
  //
  if not CkSoLer.Checked then
  begin
    Continua := FmNFeGeraXML_0400.GerarLoteNFeNovo(Lote, Empresa, FPathLoteNFe, FXML_LoteNFe, LaAviso1, LaAviso2, Sincronia);
    if Continua then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      CO_Texto_Env_Sel);
    end else MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Envio cancelado!');
  end else
  begin
    LoteStr := FormatFloat('000000000', Lote);
    if AbreArquivoXML(LoteStr, NFE_EXT_REC_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      CO_Texto_Opt_Sel);
  end;
  //
  HabilitaBotoes();
  Result := True;
end;

procedure TFmDmkACBrNFeSteps_0400.PreparaInutilizaNumerosNF(Empresa, Lote, Ano,
  Modelo, Serie, nNFIni, nNFFim, Justif: Integer);
var
  LoteStr, cUF, Id: String;
begin
  PCAcao.ActivePageIndex   := CO_PCAcao_InutilizacaoIndex;
  EdEmpresa.ValueVariant   := Empresa;
  EdLote.ValueVariant      := Lote;
  EdAno.ValueVariant       := Ano;
  EdIde_mod.ValueVariant    := Modelo;
  EdSerie.ValueVariant     := Serie;
  EdnNFIni.ValueVariant    := nNFIni;
  EdnNFFim.ValueVariant    := nNFFim;
  EdNFeJust.ValueVariant   := Justif;
  CBNFeJust.KeyValue       := Justif;
  //
  PnJustificativa.Enabled  := False;
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  if CkSoLer.Checked then
  begin
    cUF := IntToStr(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(DmNFe_0000.QrEmpresaNO_UF.Value));
    DmNFe_0000.MontaID_Inutilizacao(cUF, EdAno.Text, EdEmitCNPJ.Text,
      FormatFloat('00', EdIde_mod.ValueVariant),
      FormatFloat('00', EdSerie.ValueVariant),
      FormatFloat('000000000', nNFIni),
      FormatFloat('000000000', nNFFim), Id);
    LoteStr := Id + '_' + FormatFloat('000000000', Lote);
    //
    if AbreArquivoXML(LoteStr, NFE_EXT_INU_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Configure o modo de solicita��o e clique em "OK"!');
  HabilitaBotoes();
end;

procedure TFmDmkACBrNFeSteps_0400.PreparaVerificacaoStatusServico(
  Empresa, MOdelo: Integer);
begin
  EdEmpresa.ValueVariant := Empresa;
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmDmkACBrNFeSteps_0400.ReopenNFeJust(Aplicacao: Byte);
begin
{
  QrNFeJust.Close;
  QrNFeJust.Params[0].AsInteger := Aplicacao;
  QrNFeJust. O p e n ;
  //
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeJust, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfejust ',
  'WHERE ' + Geral.FF0(Aplicacao) + ' & Aplicacao > 0 ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmDmkACBrNFeSteps_0400.RETxtEnvioChange(Sender: TObject);
begin
  DmNFe_0000.LoadXML(RETxtEnvio, WBEnvio, PageControl1, 1);
end;

procedure TFmDmkACBrNFeSteps_0400.RETxtRetornoChange(Sender: TObject);
begin
  DmNFe_0000.LoadXML(RETxtRetorno, WBResposta, PageControl1, 3);
end;

procedure TFmDmkACBrNFeSteps_0400.RGAcaoClick(Sender: TObject);
begin
(*
  if PCAcao.PageCount  >= RGAcao.Items.Count then
    PCAcao.ActivePageIndex := RGAcao.ItemIndex
  else
    PCAcao.ActivePageIndex := 0;
*)
  case RGAcao.ItemIndex of
    0: PCAcao.ActivePageIndex := 0;
    1: PCAcao.ActivePageIndex := 1;
    2: PCAcao.ActivePageIndex := 1;
    4: PCAcao.ActivePageIndex := 2;
    5: PCAcao.ActivePageIndex := 3;
    7: PCAcao.ActivePageIndex := 4;
    //...
    else PCAcao.ActivePageIndex := 0;
  end;
{  PnLote.Visible               := False;
  PnRecibo.Visible             := False;
  PnJustificativa.Visible      := False;
  PnInutiliza.Visible          := False;
  PnChaveNFe.Visible           := False;
  PnProtocolo.Visible          := False;}
  //
  case RGAcao.ItemIndex of
    {0: (*Nada*);
    1:
    begin
      PnLote.Visible               := True;
    end;
    2:
    begin
      PnLote.Visible               := True;
      PnRecibo.Visible             := True;
    end;}
    3:
    begin
      ReopenNFeJust(1);
      {PnLote.Visible               := True;
      //PnCancInutiliza.Visible      := True;
      PnChaveNFe.Visible           := True;
      PnProtocolo.Visible          := True;
      PnJustificativa.Visible      := True;}
    end;
    4: (*Ainda n�o fiz*)
    begin
      ReopenNFeJust(2);
      {PnLote.Visible               := True;
      //PnCancInutiliza.Visible      := True;
      PnInutiliza.Visible          := True;
      PnJustificativa.Visible      := True;}
    end;
    {5:
    begin
      PnChaveNFe.Visible           := True;
    end;
    6:
    begin
      PnLote.Visible               := True;
    end;
    9:
    begin
      PnLote.Visible               := True;
    end;
    }
  end;
  {PnCancInutiliza.Visible := PnRecibo.Visible or PnChaveNFe.Visible or PnProtocolo.Visible;}
end;

function TFmDmkACBrNFeSteps_0400.TextoArqDefinido(Texto: String): Boolean;
begin
  Result := Texto <> '';
  if not Result then
    Geral.MB_Erro('Texto XML n�o definido!  "TFmDmkACBrNFeSteps_0400.TextoArqDefinido()"');
end;

procedure TFmDmkACBrNFeSteps_0400.Timer1Timer(Sender: TObject);
begin
  FSegundos := FSegundos + 1;
  LaWait.Visible := True;
  MyObjects.Informa(LaWait, True, 'Aguarde o tempo m�nimo de resposta ' +
    FormatFloat('0', FSegundos) + ' de ' + FormatFloat('0', FSecWait));
  //
  if LaWait.Font.Color = clGreen then
    LaWait.Font.Color := clBlue
  else
    LaWait.Font.Color := clGreen;
  //
  if FSegundos = FSecWait then
    Close;
end;

procedure TFmDmkACBrNFeSteps_0400.VerificaCertificadoDigital(Empresa: Integer);
begin
  UnNFe_PF.VerificaCertificadoDigital(Empresa, EdEmitCNPJ, EdSerialNumber,
    EdUF_Servico, CBUF, LaExpiraCertDigital);
end;

procedure TFmDmkACBrNFeSteps_0400.VerificaStatusServico();
const
  PerguntaSobreTutorial = True;
  PerguntaSobreOpcoesInternet = True;
  PerguntaSobreSnapIn = False;
var
  Modelo: Integer;
begin
{ Parei Aqui 2022-02-11}
  Screen.Cursor := crHourGlass;
  try
    try
    DmNFe_0000.ConfigurarComponenteNFe();
    DmkACBr_ParamsEmp.DFe_ConsultaSstatusServico(DmNFe_0000.ACBrNFe1, FCodStausServico, FTxtStausServico);
    except
      //XXe_PF.MostraMMC_SnapIn();
      on E: exception do
        UnWin.OpcoesDaInternet(Self, E.Message, PerguntaSobreTutorial,
        PerguntaSobreOpcoesInternet, PerguntaSobreSnapIn);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
