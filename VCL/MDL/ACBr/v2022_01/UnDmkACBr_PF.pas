unit UnDmkACBr_PF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, ExtCtrls, StdCtrls,
  Spin, Buttons, ComCtrls, OleCtrls, SHDocVw,
  //ACBrMail,
  //ACBrPosPrinter, ACBrNFeDANFeESCPOS,
  //ACBrDANFCeFortesFr,
  //ACBrNFeDANFeRLClass,
  //ACBrDANFCeFortesFrA4;
  (* 2023-12-19
  ACBrNFeDANFEClass,
  ACBrDFeReport, ACBrDFeDANFeReport,
  ACBrBase,
  ACBrDFe,
  ACBrIntegrador,
  pcnConversao,
  ACBrNFe, ACBrUtil,
  *)
  pcnConversao,
  pcnConversaoNFe,
  ShellAPI, XMLIntf, XMLDoc, zlib,
{$IfNDef semNFe_0000}
  UnXXe_PF,
{$EndIf}
  DmkGeral, UnDmkEnums, MyDBCheck;

type
  TUnDmkACBr_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function DmkToACBr_ModeloDF(ide_mod: Integer): TpcnModeloDF;
    function DmkToACBr_VersaoDF(versao: Double): TpcnVersaoDF;
    function DmkToACBr_Ambiente(ide_amb: Integer): TpcnTipoAmbiente;
    //
{$IfNDef semNFe_0000}
    function ObtemDadosCEP(CEP: String): String;
    function NFe_StatusServicoCod(const MostraForm: Boolean; var CodStatus:
             Integer; var TxtStatus: String): Integer;
{$EndIf}
end;

var
  DmkACBr_PF: TUnDmkACBr_PF;


implementation

{ TUnDmkACBr_PF }

uses
{$IfNDef semNFe_0000}
  NFe_PF, ModuleNFe_0000, DmkACBrNFeSteps_0400,
{$EndIf}
ModuleGeral;

function TUnDmkACBr_PF.DmkToACBr_Ambiente(ide_amb: Integer): TpcnTipoAmbiente;
var
  OK: Boolean;
begin
  Result := StrToTpAmb(OK, Geral.FF0(ide_Amb));
  if not OK then
    Geral.MB_Erro('Ambientepara emiss�o de NF n�o definido: ' + Geral.FF0(ide_amb));
end;

function TUnDmkACBr_PF.DmkToACBr_ModeloDF(ide_mod: Integer): TpcnModeloDF;
var
  OK: Boolean;
begin
  // TpcnModeloDF = (moNFe, moNFCe);
  Result := StrToModeloDF(ok, Geral.FF0(ide_Mod));
  if not OK then
    Geral.MB_Erro('Modelo de NF n�o definido: ' + Geral.FF0(ide_mod));
end;

function TUnDmkACBr_PF.DmkToACBr_VersaoDF(versao: Double): TpcnVersaoDF;
var
  OK: Boolean;
begin
  // TpcnVersaoDF = (ve200, ve300, ve310, ve400);
  result :=  DblToVersaoDF(OK, versao);
  if not OK then
    Geral.MB_Erro('Vers�o de NF n�o definida: ' + Geral.FFT(versao, 2, siNegativo));
end;

{$IfNDef semNFe_0000}
function TUnDmkACBr_PF.NFe_StatusServicoCod(const MostraForm: Boolean;
  var CodStatus: Integer; var TxtStatus: String): Integer;
var
  VersaoNFe: Integer;
begin
  CodStatus := -2;
  TxtStatus := 'N�o consultado. Erro antes de consultar!';
  VersaoNFe := UnNFe_PF.VersaoNFeEmUso();
  //
  case VersaoNFe of
     400:
    begin
      if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
      begin
        FmDmkACBrNFeSteps_0400.FCodStausServico := -1;
        //
        FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True;
        FmDmkACBrNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerStaSer.Value;
        if MostraForm then
          FmDmkACBrNFeSteps_0400.Show;
        //
        FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvStatusServico); // 0 - Status do servi�o
        FmDmkACBrNFeSteps_0400.PreparaVerificacaoStatusServico(DmodG.QrFiliLogCodigo.Value, 55);
        //
        if FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex = 0 then
        begin
          FmDmkACBrNFeSteps_0400.BtOKClick(FmDmkACBrNFeSteps_0400);
          CodStatus := FmDmkACBrNFeSteps_0400.FCodStausServico;
          TxtStatus := FmDmkACBrNFeSteps_0400.FTxtStausServico;
          //FmNFeSteps_????.Destroy;
          //
        end else
          Geral.MB_Erro(
          'O status do servi�o da NF-e n�o foi consultado! Abortado antes da consulta! RGAcao.ItemIndex <> 0');
        //
        if not MostraForm then
          FmDmkACBrNFeSteps_0400.Destroy;
      end;
    end;
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

function TUnDmkACBr_PF.ObtemDadosCEP(CEP: String): String;
var
  Acao: String;
  OldUseCert: Boolean;
  //ACBrNFe_: TACBrNFe;
begin
  Result := EmptyStr;
  //ACBrNFe_ := TACBrNFe.Create(DmodG);
  DmNFe_0000.ConfigurarComponenteNFe();
  //try
    Acao := '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' +
       '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' +
       'xmlns:cli="http://cliente.bean.master.sigep.bsb.correios.com.br/"> ' +
       ' <soapenv:Header/>' +
       ' <soapenv:Body>' +
       ' <cli:consultaCEP>' +
       //' <cep>18270-170</cep>' +
       ' <cep>' + CEP + '</cep>' +
       ' </cli:consultaCEP>' +
       ' </soapenv:Body>' +
       ' </soapenv:Envelope>';

    OldUseCert := DmNFe_0000.ACBrNFe1.SSL.UseCertificateHTTP;
    DmNFe_0000.ACBrNFe1.SSL.UseCertificateHTTP := False;

    try
      Result := DmNFe_0000.ACBrNFe1.SSL.Enviar(Acao, 'https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl', '');
    finally
      DmNFe_0000.ACBrNFe1.SSL.UseCertificateHTTP := OldUseCert;
    end;
  //finally
    //FreeAndNil(ACBrNFe_);
  //end;
end;
 {$EndIf}

end.
